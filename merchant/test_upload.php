<?php
include('includes/header.php');
require_once('classes/imageupload.php');
?>

<?php
if(isset($_POST['submit'])) {
    $width = '1600';
    $height = '400';
    $id = $_POST['user_id'];
    $file = $_FILES['banner_image'];
    $destination = IMG_PATH;
    $message = "";

    $file_info = getimagesize($file['tmp_name']);
    $uploadWidth  = $file_info[0];
    $uploadHeight = $file_info[1];
    $uploadType = $file_info[2];
    $filesize = $file['size'];
    $fileTmpLoc = $file['tmp_name'];

    $uploadLoc = $destination.$banner;

    if($uploadType == 1) {
        $ext = 'gif';
    }
    else if($uploadType == 2) {
        $ext = 'jpg';
    }
    else if($uploadType == 3) {
        $ext = 'png';
    }

    if ($uploadType != 1 && $uploadType != 2 && $uploadType != 3) {
        $message = "The image you are trying to upload is the wrong format <br />";
    }
    else {
        if($filesize > 1024000) {
            $message = "The file is larger than the allowable file size <br />";
        }
        else {
            /*if($uploadWidth > $width) {
                //resize image
                $scale_ratio = $uploadWidth / $uploadHeight;
                if (($width / $height) > $scale_ratio) {
                    $width = $height * $scale_ratio;
                } else {
                    $height = $width / $scale_ratio;
                }
                $img = "";
                $ext = strtolower($ext);
                if ($ext == "gif"){
                    $img = imagecreatefromgif($file);
                } else if($ext =="png"){
                    $img = imagecreatefrompng($file);
                } else {
                    $img = imagecreatefromjpeg($file);
                }
                $new_banner = imagecreatetruecolor($w, $h);
                // imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
                imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $uploadWidth, $uploadHeight);
                if ($ext == "gif"){
                    imagegif($tci, $file);
                } else if($ext =="png"){
                    imagepng($tci, $file);
                } else {
                    imagejpeg($tci, $file, 84);
                }

                $uploadLoc = $destination.$new_banner;
                $banner_upload = move_uploaded_file($fileTmpLoc, $uploadLoc);
            }*/
            $banner_upload = move_uploaded_file($fileTmpLoc, $uploadLoc);

            if($banner_upload) {
                //insert into the value into db
                $q = mysql_query("SELECT * FROM test_banner WHERE provider_id = '{$provider_id}'");
                $n = mysql_num_rows($q);
                if($n == 0){
                    $add_banner = mysql_query("INSERT INTO test_banner (provider_id, banner) values('$provider_id', '$banner')");
                    if($add_banner) {
                        $message .= "The record was added successfully.";
                    }
                }
                else {
                    $add_banner = mysql_query("UPDATE test_banner SET banner = '{$banner}' WHERE provider_id = '{$provider_id}'");
                    $message .= "The db was updated successfully";
                }
            }
            else {
                $message = "The Banner was not added <br />";
            }

        }
    }

}
?>

<?php
$banner_info = mysql_query("SELECT * FROM test_banner WHERE provider_id = '{$provider_id}'");
$sel_banner = mysql_fetch_object($banner_info);
$num = mysql_num_rows($banner_info);

if(!isset($sel_banner->banner)) {
    $banner = IMG_URL.'banner_images/no_banner.jpg';
}
else {
    $banner = IMG_URL.'banner_images/'.$sel_banner->banner;
}
?>

<div id="content">

    <?php include('includes/sidebar.php') ?>

    <div id="container">

        <h2 class="caps">Test Upload</h2>
        <?php
        if(isset($message) && !empty($message)){
            echo '<div class="alert">'.$message.'</div>';
        }
        ?>
        <div class="main">
            <div class="banner_con">
                <img src="<?php echo $banner ?>" />
            </div>
        </div>

        <div class="main">
            <div class="form_con">

                <form enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">

                    <div class="form_itm filefield">
                        <label>Banner</label>
                        <input type="file" name="banner" id="banner" />
                    </div>

                    <input type="hidden" name="user_id" value="<?php echo $provider_id ?>" />

                    <div class="form_itm">
                        <input type="submit" name="submit" id="submit" class="form_btn" value="Save" />
                    </div>

                </form>

            </div>
        </div>

    </div>

    <br class="clearfix" />

</div>

<?php
include('includes/footer.php')
?>

</body>
</html> */