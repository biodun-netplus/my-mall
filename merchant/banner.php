<?php
include('includes/header.php');
require_once('../includes/classes/upload.php');
?>

<?php

if(isset($_POST['submit'])) {
    if(empty($_FILES['merchant_banner'])) {
        $message = "You did not select any file.";
    }
    else {

        $width = '1600';
        $height = '400';
        $id = $_POST['user_id'];
        $file = $_FILES['merchant_banner'];
        $rand= date('his').rand(10,99);
        $merchant_banner = $rand.$_FILES['merchant_banner']['name'];
        $destination = '../banner_images/';
        $message = "";

        $file_info = getimagesize($file['tmp_name']);
        $uploadWidth  = $file_info[0];
        $uploadHeight = $file_info[1];
        $uploadType = $file_info[2];
        $filesize = $file['size'];
        $fileTmpLoc = $file['tmp_name'];
   /*     echo $merchant_banner;die;*/
        $merchant_banner = preg_replace('#[^a-z.0-9]#i', '', $merchant_banner); // filter the $filename
        $uploadLoc = $destination.$merchant_banner;
        echo $uploadLoc;
        if($uploadType == 1) {
            $ext = 'gif';
        }
        else if($uploadType == 2) {
            $ext = 'jpg';
        }
        else if($uploadType == 3) {
            $ext = 'png';
        }

        if ($uploadType != 1 && $uploadType != 2 && $uploadType != 3) {
            $message = "The image you are trying to upload is the wrong format <br />";
        }
        else {
            if($filesize > 1024000) {
                $message = "The file is larger than the allowable file size <br />";
            }
            else {

                $banner_upload = move_uploaded_file($fileTmpLoc, $uploadLoc);

                if($banner_upload) {
                    //insert into the value into db
                    $q = mysql_query("SELECT * FROM provider_banner WHERE provider_id = '{$provider_id}'");
                    $n = mysql_num_rows($q);
                    if($n == 0){
                        $add_banner = mysql_query("INSERT INTO provider_banner (provider_id, header_banner) values('$provider_id', '$merchant_banner')");
                        if($add_banner) {
                            $message = "The banner was added successfully.";
                        }
                    }
                    else {
                        $add_banner = mysql_query("UPDATE provider_banner SET header_banner = '{$merchant_banner}' WHERE
        provider_id = '{$provider_id}'");
                        $message = "The banner was updated successfully";
                    }
                }
                else {
                    $message = "The Banner was not added <br />";
                }

            }
        }
    }

}

?>

<?php
$banner_info = mysql_query("SELECT * FROM provider_banner WHERE provider_id = '{$provider_id}'");
$sel_banner = mysql_fetch_object($banner_info);
//var_dump($sel_banner);
$num = mysql_num_rows($banner_info);

$bannerArray = explode(',', $sel_banner->header_banner);
if(empty($bannerArray[0]) || ($bannerArray[0] == '-') )
{
    $banner = SITE_URL.'banner_images/no_banner.jpg';
}
else {

    $banner = SITE_URL.'banner_images/'.$bannerArray[0];
}
?>

<div id="content">

    <?php include('includes/sidebar.php') ?>

    <div id="container">

        <h2 class="caps">Update Banner</h2>
        <?php
        if(isset($message) && !empty($message)){
            echo '<div class="alert">'.$message.'</div>';
        }
        ?>
        <div class="main">
            <div class="banner_con" >
                <img src="<?php echo $banner ?>" style="width: 1103.64px;height: 246px"  />
            </div>
        </div>

        <div class="main">
            <div class="form_con">

                <form enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">

                    <div class="form_itm filefield">
                        <label>Merchant Banner</label>
                        <input type="file" name="merchant_banner" id="merchant_banner" />
                    </div>

                    <input type="hidden" name="user_id"
                           value="<?php echo $provider_id ?>" />

                    <div class="form_itm">
                        <input type="submit" name="submit" id="submit" class="form_btn" value="Save" />
                    </div>

                </form>

            </div>
        </div>

    </div>

    <br class="clearfix" />

</div>

<?php
include('includes/footer.php')
?>

</body>
</html>