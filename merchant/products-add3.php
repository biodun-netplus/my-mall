<?php
include('includes/header.php');
?>


<?php
$products = mysql_query("select * from products where pid = '" . $provider_id . "'");
?>

<div id="content" >

    <?php include('includes/sidebar.php') ?>

    <div id="container">

        <h2 class="caps">Add Products</h2>

        <div class="main">
            <div class="tab_nav">
                <ul>
                    <li><a href="#">Step 1</a></li>
                    <li><a href="products-add2.php">Step 2</a></li>
                    <li class="active"><a href="#">Step 3</a></li>
                </ul>
            </div>

            <section class="form_wrap">
                <h2>Update Inventory</h2>

                <div class="form_con float_lft">
                    <div class="errors" id="inventory_error"></div>

                    <form action="" method="post" id="update_inventory">
                        <div class="form_itm">
                            <select name="color" class="selection" id="select_color">
                                <option value="">Select Color</option>
                                <?php
                                if(isset($_SESSION['product_id'])) {
                                    $product_id = $_SESSION['product_id'];
                                    $colored = mysql_query("SELECT * FROM product_color WHERE
                                        pid='{$product_id}'");
                                    while($color_det = mysql_fetch_object($colored)){
                                        ?>
                                        <option value="<?php echo $color_det->id ?>"><?php echo
                                            $color_det->color ?></option>
                                    <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form_itm">
                            <select name="size" class="selection" id="select_size">
                                <option value="0">Select Size</option>
                                <?php
                                if(isset($_SESSION['product_id'])) {
                                    $product_id = $_SESSION['product_id'];
                                    $sized = mysql_query("SELECT * FROM product_size WHERE
                                        pid='{$product_id}'");
                                    while($size_det = mysql_fetch_object($sized)){
                                        ?>
                                        <option value="<?php echo $size_det->id ?>"><?php echo
                                            $size_det->size ?></option>
                                    <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form_itm">
                            <input type="number" name="quantity" id="quantity" class="textfield"
                                   placeholder="Enter Product Quantity" />
                        </div>

                        <input type="hidden" name="product_id" id="curr_prdt_id3"
                               value="<?php if(isset($_SESSION['product_id'])){echo
                               $_SESSION['product_id']; } ?>" />
<div id="inventory_add"></div>
                        <div class="form_itm">
                            <input type="button" name="submit_inventory" id="submit_inventory" class="form_btn"
                                   value="Save" />
                        </div>
                    </form>
                </div>

                <div class="form_con float_rgt">
                    <h5>Product Inventory</h5>
                    <div class="disp_inventory"></div>

                </div>

                <br class="clearfix" />
            </section>

        </div>

        <div class="form_wrap">
            <div class="add_more_btn float_rgt">
                <a href="products.php">Go to Product Index</a>
            </div>
            <br class="clearfix" />
        </div>
    </div>

    <br class="clearfix" />

</div>

<?php
include('includes/footer.php')
?>


</body>
</html>