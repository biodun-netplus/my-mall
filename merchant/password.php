<?php
include('includes/header.php');
?>

<?php
if(isset($_POST['submit'])){

    $currentpassword = md5(strip_tags($_POST['currentpassword']));
    $newpassword = md5(strip_tags($_POST['newpassword']));
    $confirmpassword = md5(strip_tags($_POST['confirmpassword']));

    $pass_info = mysql_fetch_object(mysql_query("select * from users where id = '" . $provider_id . "'"));

    if($newpassword == $confirmpassword) {
        if ($currentpassword == $pass_info->password) {
            $q = mysql_query("UPDATE users SET password='{$newpassword}' WHERE id='{$provider_id}'");

            if ($q) {
                $message = 'Password was successfully updated';
            } else {
                $message = 'An error occurred during the query';
            }
        } else {
            $message = 'The current password you entered is not correct';
        }
    }
    else {
        $message = 'The confirm password does not match the new password';
    }

}
?>

    <div id="content">

        <?php include('includes/sidebar.php') ?>

        <div id="container">

            <h2 class="caps">Password</h2>

            <div class="main">
                <div class="form_con">
                    <?php
                    if(isset($message) && !empty($message)) {
                    ?>
                        <div class="alert" style="margin-bottom: 10px"><?php echo $message ?></div>
                    <?php
                    }
                    ?>
                    <form action="" method="post">

                        <div class="form_itm">
                            <input type="password" name="currentpassword" id="currpassword" class="textfield"
                                   placeholder="Current Password*" autocomplete="off" />
                            <div class="form_itm_alert" id="currpassword_error"></div>
                        </div>

                        <div class="form_itm">
                            <input type="password" name="newpassword" id="newpassword" class="textfield"
                                   placeholder="New Password*" />
                            <div class="form_itm_alert" id="newpassword_error"></div>
                        </div>

                        <div class="form_itm">
                            <input type="password" name="confirmpassword" id="confpassword" class="textfield"
                                   placeholder="Confirm Password*" />
                            <div class="form_itm_alert" id="confpassword_error"></div>
                        </div>

                        <div class="form_itm">
                            <input type="submit" name="submit" id="submit" class="form_btn" value="Save" />
                        </div>

                    </form>

                </div>
            </div>

        </div>

        <br class="clearfix" />

    </div>

<?php
include('includes/footer.php')
?>