<?php
include('includes/header.php');
?>

<?php
if(!isset($_SESSION['provider_id'])) {
    $provider_id = '1612';
    //header("location: ".SITE_URL."/");
}
else {
    $provider_id =  $_SESSION['provider_id'];
}
?>

<?php
if(isset($_POST['submit'])) {
    $title = mysql_real_escape_string(strip_tags($_POST["title"]));
    $meta = mysql_real_escape_string(strip_tags($_POST["meta"]));
    $content = mysql_real_escape_string(strip_tags($_POST["content"]));
    $id = mysql_real_escape_string(strip_tags($_POST["id"]));
    $q = mysql_query("UPDATE title_meta SET title='{$title}', meta='{$meta}', content='{$content}' WHERE id='{$id}'");
    //$num = mysql_affected_rows($q);
    if($q) {
        $message = 'The Meta data was successfully updated.';
    }
    else {
        $message = 'An error occurred. The meta data was not updated.';
    }
}
?>

<?php
$meta = mysql_query("SELECT * FROM title_meta WHERE merchant_id = '{$provider_id}'");
$meta_details = mysql_fetch_object($meta);
?>

<div id="content">

    <?php include('includes/sidebar.php') ?>

    <div id="container">

        <h2 class="caps">Edit Meta Tags</h2>

        <div class="main">
            <div class="form_con">
                <?php

                ?>
                <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">

                    <div class="form_itm">
                        <label>Title</label>
                        <input type="text" name="title" id="title" class="textfield"
                               placeholder="Title"
                               value="<?php if(isset($meta_details->title)){echo $meta_details->title;}?>" />
                    </div>

                    <div class="form_itm">
                        <label>Meta</label>
                        <input type="text" name="meta" id="meta" class="textfield"
                               placeholder="Meta"
                               value="<?php if(isset($meta_details->meta)){echo $meta_details->meta;}?>" />
                    </div>

                    <div class="form_itm">
                        <label>Content</label>
                            <textarea name="content" class="textarea"><?php if(isset($meta_details->content)){echo $meta_details->content;}?>
                            </textarea>
                    </div>

                    <input type="hidden" name="id"
                           value="<?php if(isset($meta_details->id)){echo $meta_details->id;}?>" />

                    <div class="form_itm">

                        <input type="submit" name="submit" id="submit" class="form_btn" value="Save" />

                        <div class="cancel_btn"><a href="meta-tags.php">Cancel</a></div>

                    </div>

                </form>

            </div>
        </div>

    </div>

    <br class="clearfix" />

</div>

<?php
include('includes/footer.php')
?>

</body>
</html>