<?php
include('includes/header.php');
echo $provider_id;
$prof_info = mysql_fetch_object(mysql_query("select * from users where id = '" . $provider_id . "'"));

$banner_info = mysql_query("SELECT * FROM provider_banner WHERE provider_id = '{$provider_id}'");
$sel_banner = mysql_fetch_object($banner_info);
$num = mysql_num_rows($banner_info);
$bannerArray = explode(',', $sel_banner->header_banner);
if(!file_exists('../banner_images/'.$bannerArray[0]) || ($bannerArray[0] == '-') )
{
    $banner = SITE_URL.'banner_images/no_banner.jpg';
}
else {

    $banner = SITE_URL.'banner_images/'.$bannerArray[0];
}

if(!file_exists('../banner_images/'.$sel_banner->logo) || $sel_banner->logo == '-') {
    $img = SITE_URL.'banner_images/_no_image.png';
}
else {
    $img = SITE_URL.'banner_images/'.$sel_banner->logo;
}


?>

    <div id="content">

        <?php include('includes/sidebar.php') ?>

        <div id="container">

            <h2 class="caps">Profile</h2>

            <div class="btn profile_btn"><a href="profile-edit.php">Edit Profile</a></div>

            <div class="main">
                <div class="banner_con">
                    <a href="<?php echo SITE_URL ?>banner.php"><img src="<?php echo $banner ?>" style="width: 1103.64px;height: 246px"/></a>
                </div>

                <div class="cont_detail float_lft">
                    <div class="cont_detail_itm">
                        <span>Business Name:</span> <?php echo $prof_info->first_name ?>
                    </div>

                    <div class="cont_detail_itm">
                        <span>Address:</span> <?php echo $prof_info->address ?>
                    </div>

                    <div class="cont_detail_itm">
                        <span>Telephone:</span> <?php echo $prof_info->phone ?>
                    </div>

                    <div class="cont_detail_itm">
                        <span>Email:</span> <?php echo $prof_info->email ?>
                    </div>

                    <div class="cont_detail_itm">
                        <span>Merchant Pickup Area: </span> <?php echo $prof_info->deliveryarea ?>
                    </div>

                    <div class="cont_detail_itm">
                        <span>Merchant's Bank: </span> <?php echo $prof_info->bank_name ?>
                    </div>

                    <div class="cont_detail_itm">
                        <span>Account Number: </span> <?php echo $prof_info->account_type ?>
                    </div>
                </div>

                <div class="profile_logo float_rgt">
                    <a href="<?php echo SITE_URL ?>change-logo.php"><img src="<?php echo $img ?>" /></a>
                </div>
            </div>

        </div>

        <br class="clearfix" />

    </div>

<?php
include('includes/footer.php')
?>

</body>
</html>