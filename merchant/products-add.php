<?php
include('includes/header.php');
?>


<?php
$products = mysql_query("select * from products where pid = '" . $provider_id . "'");

$merchant = mysql_fetch_object(mysql_query("SELECT * FROM users WHERE id=$provider_id"));
//var_dump($merchant);
?>

<div id="content" >

    <?php include('includes/sidebar.php') ?>

    <div id="container">

        <h2 class="caps">Add Products Step 1</h2>

        <div class="main">
            <div class="tab_nav">
                <ul>
                    <li class="active"><a href="#">Step 1</a></li>
                    <li><a href="#">Step 2</a></li>
                    <li><a href="#">Step 3</a></li>
                </ul>
            </div>

            <section class="form_wrap">
                <h2>Add Product</h2>
                <div class="instruction_btn"><a href="#">Click Here to View Image Upload Instruction</a></div>
                <form action="uploadproducts.php" method="post" enctype="multipart/form-data" id="add_product_form">

                    <div class="form_process" style="color: red"></div>

                    <div class="content" id="form_success">
                        <div class="form_con float_lft">
                            <div class="form_itm">
                                <select name="category_id" class="selection" id="category_id">
                                    <option value="">Select Category</option>
                                    <?php
                                    $main_cat = mysql_query("SELECT * FROM category WHERE parent_id = 0 ORDER BY category_name");
                                    while($main_cat_data = mysql_fetch_object($main_cat)) {
                                        ?>
                                        <option value="<?php echo $main_cat_data->id ?>">
                                            <?php echo $main_cat_data->category_name?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                                <div class="form_itm_error" id="sel_cat_error"></div>
                            </div>

                            <div class="form_itm" id="sub_id">
                                <select name="sub_cat_id"  class="selection" id="sub_category">
                                    <option value="">Select sub category</option>
                                </select>
                            </div>

                            <div class="form_itm" id="extra_id">
                                <select name="sub_sub_category" class="selection" id="sub_sub_category">
                                    <option value="">Select sub sub category</option>
                                </select>
                            </div>

                            <div class="form_itm">
                                <input type="text" name="product_name" id="product_name" class="textfield"
                                       placeholder="Product Name*" />
                                <div class="form_itm_error" id="product_name_error"></div>
                            </div>

                            <div class="form_itm">
                                <input type="text" name="brand" id="brand" class="textfield"
                                       placeholder="Brand*" />
                                <div class="form_itm_error" id="brand_error"></div>
                            </div>
                            <br class="clearfix" />

                        </div>

                        <div class="form_con float_rgt">
                            <div class="form_itm">
                                <input type="text" name="list_price" id="list_price" class="textfield"
                                       placeholder="List Price" />
                                <div class="form_notice">
                                    General Market Price. Please you may leave it blank if you are unsure of the value
                                </div>

                            </div>

                            <div class="form_itm">
                                <input type="text" name="product_price" id="product_price" class="textfield"
                                       placeholder="Product Price*" />
                                <div class="form_notice">
                                    Your Selling Price
                                </div>
                                <div class="form_itm_error" id="product_price_error"></div>
                            </div>

                            <div class="form_itm filefield">
                                <label>Product Image 1*</label>
                                <input type="file" accept="image/*" name="product_image1" id="product_image1" />
                                <div class="form_notice">
                                    The image must be jpeg, jpg, gif or png file less than 200Kb.
                                </div>
                                <div class="form_itm_error" id="product_image1_error"></div>
                            </div>

                            <div class="form_itm filefield">
                                <label>Product Image 2</label>
                                <input type="file" accept="image/*" name="product_image2" id="product_image2" />
                                <div class="form_notice">
                                    The image must be jpeg, jpg, gif or png file less than 200Kb.
                                </div>
                            </div>
                            <?php
//echo $merchant->first_name;
                            if($merchant->first_name == 'Cyber Week')
                            {?>
                                <div class="form_itm">
                                    <select name="merchant_id" class="selection" id="category_id">
                                        <option value="">Merchant</option>
                                        <?php
                                        $main_cat = mysql_query("SELECT * FROM users WHERE isactive = 't' AND first_name!='' ORDER BY first_name ASC");
                                        while($main_cat_data = mysql_fetch_object($main_cat)) {
                                            ?>
                                            <option value="<?php echo $main_cat_data->id ?>">
                                                <?php echo $main_cat_data->first_name?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <div class="form_itm_error" id="sel_cat_error"></div>
                                </div>
                            <?php }
                            ?>
                            <br class="clearfix" />

                        </div>
                        <input type="hidden" name="merchant_name" id="submit_product" class="textfield" value="<?php echo $merchant->first_name;?>" />
                        <div class="form_itm clearfix">
                            <label>Product Details*</label>
                            <textarea name="product_details" class="textarea" id="product_details"
                                      ></textarea>
                            <div class="form_itm_error" id="product_detail_error"></div>
                        </div>



                        <div class="form_itm clearfix">
                            <input type="submit" name="submit" id="submit_product" class="form_btn" value="Save" />
                        </div>

                        <br class="clearfix" />
                    </div>
                </form>
            </section>

        </div>
    </div>

    <br class="clearfix" />

</div>

<?php
include('includes/footer.php')
?>

<script type="text/javascript">
    $(document).ready(function(){
        $('.instruction_btn').click(function(){
            $('#instruction_overlay').css('display', 'block');
            $('#instruction_popup').css('display', 'block');
        });

        $('.close_instruction').click(function(){
            $('#instruction_overlay').css('display', 'none');
            $('#instruction_popup').css('display', 'none');
        });
    })
</script>

<script type="text/javascript" src="<?php echo SITE_URL_MERC; ?>js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">

    tinyMCE.init({
        // General options
        selector: "textarea",
        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft," +
        "justifycenter,justifyright,justifyfull,formatselect,cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,cleanup,help,code,|,insertdate,inserttime,preview",
    });
</script>

</body>
</html>