<?php
include('includes/header.php');
?>


<?php
$products = mysql_query("select * from products where pid = '" . $provider_id . "'");
?>

<div id="content" >

    <?php include('includes/sidebar.php') ?>

    <div id="container">

        <h2 class="caps">Add Products Step 2</h2>

        <div class="main">
            <div class="tab_nav">
                <ul>
                    <li><a href="#">Step 1</a></li>
                    <li class="active"><a href="#">Step 2</a></li>
                    <li><a href="products-add3.php">Step 3</a></li>
                </ul>
            </div>

            <section class="form_wrap">
                <h2>Add Color and Sizes</h2>
                <form action="" method="post">

                    <div class="form_con float_lft">
                        <h5>Available Colors</h5>
                        <div class="errors" id="color_error"></div>

                        <div class="disp_avail_color"></div>
                        <br />

                        <form action="" method="post" id="update_color">
                            <div id="color_loading" ></div>
                            <div class="form_itm" id="hidden_prdt_id">
                                <input type="text" name="color" id="color" class="textfield"
                                       placeholder="Enter Color" />
                                <div class="form_notice">
                                    If your product does not have color classification please skip this.
                                </div>
                            </div>
                            <input type="hidden" name="product_id" id="curr_prdt_id1"
                                   value="<?php if(isset($_SESSION['product_id'])){echo
                                   $_SESSION['product_id']; } ?>" />

                            <div class="form_itm">
                                <input type="button" name="submit_color" id="submit_color" class="form_btn"
                                       value="Save Color" />
                            </div>
                        </form>
                    </div>

                    <div class="form_con float_rgt">
                        <h5>Available Sizes</h5>
                        <div class="errors" id="size_error"></div>

                        <div class="disp_avail_size"></div>
                        <br />

                        <form action="" method="post" id="size_update">
                            <div id="size_loading" ></div>
                            <div class="form_itm">
                                <input type="text" name="size" id="size" class="textfield"
                                       placeholder="Enter Size" />
                                <div class="form_notice">
                                    If your product does not have size classification please skip this.
                                </div>
                            </div>

                            <input type="hidden" name="product_id" id="curr_prdt_id2"
                                   value="<?php if(isset($_SESSION['product_id'])){echo
                                   $_SESSION['product_id']; } ?>" />

                            <div class="form_itm">
                                <input type="button" name="submit_size" id="submit_size" class="form_btn"
                                       value="Save Size" />
                            </div>
                        </form>
                    </div>

                    <br class="clearfix">
                </form>
            </section>

        </div>

        <div class="form_wrap">
            <div class="add_more_btn float_rgt">
                <a href="products-add3.php">Add Inventory to Product</a>
            </div>
            <br class="clearfix" />
        </div>
    </div>

    <br class="clearfix" />

</div>

<?php
include('includes/footer.php')
?>


</body>
</html>
