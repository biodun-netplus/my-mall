<?php
include('includes/header.php');
?>

<?php

?>

    <div id="content">

        <?php include('includes/sidebar.php') ?>

        <div id="container">

            <h2 class="caps">Send Message</h2>

            <div class="main">
                <div class="form_itm2 float_lft">
                    <input type="text" name="message_title" class="textfield" placeholder="To: " />
                </div>

                <div class="form_itm2 float_rgt">
                    <input type="text" name="message_subject" class="textfield" placeholder="Subject" />
                </div>

                <div class="form_itm">
                    <textarea name="message_content" class="textarea2" placeholder="Write a message..."></textarea>
                </div>

                <div class="form_itm">
                    <input type="button" name="submit" class="form_btn" value="Send" />
                </div>

                <br class="clearfix" />
            </div>

        </div>

        <br class="clearfix" />

    </div>

<?php
include('includes/footer.php')
?>

</body>
</html>