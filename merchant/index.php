<?php
include('includes/header.php');
//var_dump($_SESSION);
?>

<?php

$transactions = mysql_query("SELECT `order`.*,
                    `cart`.*,
                    `order`.`status` as `order_status`,
                    `shipping_addres_list`.*,
                    `users`.`id` AS `user_id`,
                    `products`.`id` As
                    `product_id`,
                    `products`.`pid` As
                    `product_user_id` FROM
                    `order` LEFT JOIN
                    `shipping_addres_list` ON
                    `shipping_addres_list`.`id` = `order`.`shipping_add_id`
                    INNER JOIN `cart` ON `cart`.`order_id` = `order`.`order_id`
                    LEFT JOIN `products` ON `products`.`id` = `cart`.`product_id`
                    JOIN `users` ON `users`.`id` = `products`.`pid`
                    WHERE `products`.`pid` = '$provider_id'
                    AND `order`.`status` = 'Completed'
                    GROUP BY `transaction_ref`
                    ");
$transaction_count = mysql_num_rows($transactions);

$all_sales = 0;
while($sales_data = mysql_fetch_object($transactions)) {
    $sales = $sales_data->price*$sales_data->quantity;
    $all_sales += $sales;
}
$total_sales = $all_sales;

//get values for product
$product = mysql_query("SELECT * FROM products WHERE pid = '".$provider_id."'");
$product_count = mysql_num_rows($product);

//get the messages
//$messages = mysql_query("SELECT * FROM message WHERE pid = '{$provider_id}'");
//$message_count = mysql_num_rows($messages);

?>

<div id="content">

    <?php include('includes/sidebar.php') ?>

    <div id="container">

        <h2 class="caps">Dashboard</h2>

        <div class="main">
            <div id="total_sales" class="dashboard_itm">
                <div class="di_ins_rgt">
                    <a href="<?php echo SITE_URL ?>merchant/orders.php">Total Sales</a>
                    <h3 class="white"><a href="<?php echo SITE_URL ?>merchant/orders.php">₦<?php echo number_format($total_sales) ?></a></h3>
                </div>
            </div>

            <div id="total_orders" class="dashboard_itm">
                <div class="di_ins_rgt">
                    <a href="<?php echo SITE_URL ?>merchant/orders.php">Total Orders</a>
                    <h3 class="white"><a href="<?php echo SITE_URL ?>merchant/orders.php"><?php echo $transaction_count ?></a></h3>
                </div>
            </div>

            <div id="product_count" class="dashboard_itm">
                <div class="di_ins_rgt">
                    <a href="<?php echo SITE_URL ?>merchant/products.php">Product Count</a>
                    <h3 class="white"><a href="<?php echo SITE_URL ?>merchant/products.php"><?php echo $product_count ?></a></h3>
                </div>
            </div>

            <div id="pending_payment" class="dashboard_itm">
                <div class="di_ins_rgt">
                    <a href="<?php echo SITE_URL ?>merchant/messages.php">Messages</a>
                    <h3 class="white"><a href="<?php echo SITE_URL ?>merchant/messages.php"><?php echo "0" ?></a></h3>
                </div>
            </div>

            <br class="clearfix" />
        </div>

        <div class="main">
            <div class="box1" id="graph">
                <div class="box_title">Sales Analysis</div>
                <div class="box_ins" style="width:95%">
                    <canvas id="display_graph" height="400" width="600"></canvas>
                </div>
            </div>

            <div class="box1" id="recent_orders">
                <div class="box_title">Last 10 Orders</div>
                <div class="box_ins" style="overflow-y: scroll;">
                    <table width="100%" border="1">
                        <tr>
                            <th width="25%" height="30">Image</th>
                            <th width="25%" height="30">Customer Name</th>
                            <th width="40%">Product  Name</th>
                            <th width="15%">Price</th>
                            <th width="20%">Date</th>
                        </tr>

                        <?php
                        $transact =mysql_query("SELECT `order`.*,
                    `cart`.*,
                    `order`.`status` as `order_status`,
                    `shipping_addres_list`.*,
                    `users`.`id` AS `user_id`,
                    `products`.`id` As
                    `product_id`,
                    `products`.`pid` As
                    `product_user_id` FROM
                    `order` LEFT JOIN
                    `shipping_addres_list` ON
                    `shipping_addres_list`.`id` = `order`.`shipping_add_id`
                    INNER JOIN `cart` ON `cart`.`order_id` = `order`.`order_id`
                    LEFT JOIN `products` ON `products`.`id` = `cart`.`product_id`
                    JOIN `users` ON `users`.`id` = `products`.`pid`
                    WHERE `products`.`pid` = '$provider_id'
                    AND `order`.`status` = 'Completed'

                    ORDER BY `order`.`id`
                    DESC LIMIT 10");
                        while ($TransData = mysql_fetch_object($transact)) {
//                            var_dump($TransData);
                            $pro_id = $TransData->product_id;
                            $prod = mysql_query("SELECT * FROM products WHERE id ='{$pro_id}'") or die(mysql_error());
                            $product_data = mysql_fetch_object($prod);
                        ?>

                        <tr>
                            <td>
                                <?php
                                if (!file_exists('../product_images/' . $product_data->image) || $product_data->image == '') {
                                    ?>
                                    <img src="<?php echo SITE_URL ?>banner_images/_no_image.png"
                                         class="prdt_img"/>

                                    <?php

                                } else {
                                    ?>
                                    <img src="<?php echo SITE_URL ?>product_images/<?php echo $product_data->image ?>" class="prdt_img"/>

                                    <?php
                                } ?>

                            </td>
                            <td><?php echo $TransData->delivery_contact_name ?></td>
                            <td><?php echo $TransData->product_name ?></td>
                            <td>₦<?php echo number_format($TransData->price*$TransData->quantity) ?></td>
                            <td>
                                <?php
                                $date = date('M j, Y', strtotime($TransData->created));
                                echo $date
                                ?>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <br class="clearfix" />

</div>

<?php
include('includes/footer.php')
?>

<script type="text/javascript">
    //var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
    var lineChartData = {
        labels : ["January","February","March","April","May","June","July"],
        datasets : [
            {
                label: "My First dataset",
                fillColor : "rgba(20,20,20,0.2)",
                strokeColor : "rgba(20,20,20,1)",
                pointColor : "rgba(20,20,20,1)",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data: [0, 0, 0, 0, 0, 0, 0]
            }
        ]

    }

    window.onload = function(){
        var ctx = document.getElementById("display_graph").getContext("2d");
        window.myLine = new Chart(ctx).Line(lineChartData, {
            responsive: true,
            scaleBeginAtZero : true,
            scaleShowGridLines : true,
            scaleGridLineColor : "rgba(0,0,0,.05)",
            scaleGridLineWidth : 1,
            scaleShowHorizontalLines: true,
            scaleShowVerticalLines: true

        });
    }

</script>

</body>
</html>