<?php
include('includes/header.php');
?>

<?php
if(isset($_POST['save'])) {
    $id = strip_tags($_POST['id']);
    $quantity = strip_tags($_POST['quantity']);
    $q = mysql_query("UPDATE relation_color_size SET qty ='{$quantity}' WHERE id='{$id}' LIMIT 1");
    if($q) {
        header("location: products.php?msg=success");
    }
    else {
        $message = 'The product was not updated. ';
    }
}
?>

<div id="content">

    <?php include('includes/sidebar.php') ?>

    <div id="container">

        <h2 class="caps">Edit Inventory</h2>


        <div class="main">
            <div class="content">
                <?php
                    if(isset($_GET['id'])) {
                        $id = $_GET['id'];
                        $q = mysql_query("SELECT * FROM relation_color_size WHERE id='{$id}'");
                        $invent_info = mysql_fetch_object($q);
                        echo '<h5 class="light">Color: ' . $invent_info->color . '</h5>';
                        echo '<h5 class="light">Size: ' . $invent_info->size . '</h5>';
                        ?>
                        <br/>

                        <div class="form_con">
                            <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" id="update_inventory">

                                <div class="form_itm">
                                    <input type="number" name="quantity" id="quantity" class="textfield"
                                           value="<?php echo $invent_info->qty ?>" placeholder="Enter Product Quantity"/>
                                </div>

                                <input type="hidden" name="id"
                                       value="<?php if (isset($_GET['id'])) {
                                           echo $_GET['id'];
                                       } ?>"/>

                                <div class="form_itm">
                                    <input type="submit" name="save" class="form_btn"
                                           value="Save"/>

                                    <div class="cancel_btn"><a href="#">Cancel</a></div>
                                </div>
                            </form>
                        </div>
                    <?php
                    }
                    else if(isset($message) && !empty($message)) {
                        echo '<h5 class="light">'.$message.'</h5>';
                    }
                ?>

            </div>

        </div>


    </div>

    <br class="clearfix" />

</div>

<?php
include('includes/footer.php')
?>



<?php
if(isset($_GET['product_id'])){
    $product_id = $_GET['product_id'];
    ?>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.add_more_btn').click(function(){
                $('#popup_overlay').css('display', 'block');
                $('#popup').css('display', 'block');
            });

            $('.close_pop_up').click(function(){
                $('#popup_overlay').css('display', 'none');
                $('#popup').css('display', 'none');
                window.location.href='product-inventory-edit.php?product_id=<?php echo $product_id ?>';
            });
        })
    </script>

    <script type="text/javascript">
        $(document).ready(function(){

            function displayInventory(){
                var product_id = <?php echo $product_id; ?>;
                $.ajax({
                    type:"post",
                    url:"add_product_inventory.php",
                    data:"product_id="+product_id+"&action=displayinventory",
                    success:function(data){
                        $(".show_inventory").html(data);
                    }
                });
            }

            displayInventory();

            $("#edit_inventory").click(function(){
                //alert('submit was clicked');
                var prdt_id = <?php echo $product_id; ?>;
                var size=$("#select_size").val();
                var color=$("#select_color").val();
                var quantity=$("#quantity").val();

                $.ajax({
                    type:"post",
                    url:"add_product_inventory.php",
                    data:"prdt_id="+prdt_id+"&size="+size+"&color="+color+"&quantity="+quantity+"&action=updateinventory",
                    success:function(data){

                        if(data=='failed') {
                            $("#inventory_error2").html("<p style='color:#333;'>An error was encountered</p>");
                        }

                        else {
                            displayInventory();
                            $("#inventory_error2").html("");
                            $("#quantity").val("");
                        }
                    }
                });

            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){

            function displayColor(){
                var prdt_id = <?php echo $product_id; ?>;
                $.ajax({
                    type:"post",
                    url:"add_product_color.php",
                    data:"prdt_id="+prdt_id+"&action=displaycolor",
                    success:function(data){
                        $(".show_avail_color").html(data);
                    }
                });
            }

            displayColor();

            $("#add_color").click(function(){

                var prdt_id1 = <?php echo $product_id; ?>;
                var color = $("#updatecolor").val();

                $.ajax({
                    type:"post",
                    url:"add_product_color.php",
                    data:"prdt_id1="+prdt_id1+"&color="+color+"&action=addcolor",
                    success:function(data){

                        if(data=='failed') {
                            $("#color_error2").html("<p style='color:#333;'>There was an error encountered</p>");
                        }
                        else if(data == 'color_added'){
                            $("#color_error2").html("<p>This color has already been added</p>")
                        }
                        else {
                            displayColor();
                            $("#color_error2").html("");
                            $("#updatecolor").val("");
                        }
                    }
                });

            });

            //
            function displaySize(){
                var product_id2 = <?php echo $product_id; ?>;
                $.ajax({
                    type:"post",
                    url:"add_product_size.php",
                    data:"product_id2="+product_id2+"&action=displaysize",
                    success:function(data){
                        $(".show_avail_size").html(data);
                    }
                });
            }

            displaySize();

            $("#add_size").click(function(){

                var prdt_id2 = <?php echo $product_id; ?>;
                var size = $("#updatesize").val();

                $.ajax({
                    type:"post",
                    url:"add_product_size.php",
                    data:"prdt_id2="+prdt_id2+"&size="+size+"&action=addsize",
                    success:function(data){

                        if(data=='failed') {
                            $("#size_error2").html("<p style='color:#333;'>There was an error encountered</p>");
                        }
                        else if(data == 'size_added'){
                            $("#size_error2").html("<p>This size has already been added</p>")
                        }
                        else {
                            displaySize();
                            $("#size_error2").html("");
                            $("#updatesize").val("");
                        }
                    }
                });

            });
        });
    </script>

<?php
}
?>

</body>
</html>