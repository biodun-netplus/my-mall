<?php
include('includes/header.php');
?>

<?php
$mes = mysql_query("SELECT * FROM message WHERE pid = '{$provider_id}'");
$num = mysql_num_rows($mes);
?>

    <div id="content">

        <?php include('includes/sidebar.php') ?>

        <div id="container">

            <h2 class="caps">Messages</h2>

            <div class="btn profile_btn"><a href="message-send.php">New Message</a></div>


            <div class="main_top">
                <div class="btn float_lft"><a href="">Inbox</a></div>
                <div class="btn float_lft"><a href="">Sent Messages</a></div>
                <br class="clearfix" />
            </div>

            <div class="main">
                <?php
                if($num > 0) {
                    ?>
                    <table width="100%" border="1">
                        <tr>
                            <th width="15%">Sender's Name</th>
                            <th width="20%">Subject</th>
                            <th width="45%">Message</th>
                            <th width="10%">Date</th>
                            <th width="10%">Action</th>
                        </tr>
                        <?php
                        while ($mess_det = mysql_fetch_object($mes)) {
                            ?>
                            <tr>
                                <td><?php echo $mess_det->sender_name ?></td>
                                <td><?php echo $mess_det->subject ?></td>
                                <td><?php echo $mess_det->content ?></td>
                                <td>
                                    <?php
                                    $date = date('M j, Y', strtotime($mess_det->date));
                                    echo $date
                                    ?>
                                </td>
                                <td>
                                    <a href="<?php echo SITE_URL ?>merchant/message-reply.php?id=<?php echo
                                    $mess_det->id ?>">Reply</a>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>

                    </table>
                <?php
                }
                else {
                    echo '<p>There are no messages</p>';
                }
                ?>
            </div>

        </div>

        <br class="clearfix" />

    </div>

<?php
include('includes/footer.php')
?>

</body>
</html>