<?php

function generate_page_links ($cur_page, $num_pages) {
    global $category_id;
    $page_links='';

    //if this page is not the first page, generate the "previous" link
    if ($cur_page>1) {
        $page_links='<a href="' . $_SERVER['PHP_SELF'] . '?page=' . ($cur_page-1) . '"><span class="pagelinks_itm">Previous </span></a>';
    }
    else {
        $page_links='<span class="pagelinks_itm">Previous</span> ';
    }

    //Loop through the pages generating the page number links
    for ($i=1; $i<=$num_pages; $i++) {
        if ($cur_page==$i) {
            $page_links.= ' <span class="pagelinks_itm">' . $i . '</span> ';
        }
        else {
            $page_links.='<a href="' . $_SERVER['PHP_SELF'] . '?page=' . $i . '"><span class="pagelinks_itm">'. ' ' . $i . ' ' . '</span></a>';
        }
    }

    //if this page is  not the last page, generate the "next" link.
    if($cur_page < $num_pages) {
        $page_links .= '<span class="pagelinks_itm"><a href="' . $_SERVER['PHP_SELF'] . '?page=' . ($cur_page+1) . '"> Next</a><span>';
    }
    else {
        $page_links .= ' <span class="pagelinks_itm">Next</span>';
    }
    return $page_links;
}


function generate_page_links2 ($cur_page, $num_pages) {
    global $category_id;
    $page_links='';

    //if this page is not the first page, generate the "previous" link
    if ($cur_page>1) {
        $page_links='<a href="' . $_SERVER['PHP_SELF'] . '?category_id=' . $category_id . '&page=' . ($cur_page-1) . '"><span class="pagelinks_itm">Previous </span></a>';
    }
    else {
        $page_links='<span class="pagelinks_itm">Previous</span> ';
    }

    //Loop through the pages generating the page number links
    for ($i=1; $i<=$num_pages; $i++) {
        if ($cur_page==$i) {
            $page_links.= ' <span class="pagelinks_itm">' . $i . '</span> ';
        }
        else {
            $page_links.='<a href="' . $_SERVER['PHP_SELF'] . '?page=' . $i . '"><span class="pagelinks_itm">'. ' ' . $i . ' ' . '</span></a>';
        }
    }

    //if this page is  not the last page, generate the "next" link.
    if($cur_page < $num_pages) {
        $page_links .= '<span class="pagelinks_itm"><a href="' . $_SERVER['PHP_SELF'] . '?category_id=' . $category_id . '&page=' . ($cur_page+1) . '"> Next</a><span>';
    }
    else {
        $page_links .= ' <span class="pagelinks_itm">Next</span>';
    }
    return $page_links;
}

?>


<!-- -------------------------------------------- -->
<!-- "image_upload_script.php" -->
<!-- -------------------------------------------- -->

<?php
// ----------------------- RESIZE FUNCTION -----------------------
// Function for resizing any jpg, gif, or png image files
function ak_img_resize($target, $newcopy, $w, $h, $ext) {
    list($w_orig, $h_orig) = getimagesize($target);
    $scale_ratio = $w_orig / $h_orig;
    if (($w / $h) > $scale_ratio) {
        $w = $h * $scale_ratio;
    } else {
        $h = $w / $scale_ratio;
    }
    $img = "";
    $ext = strtolower($ext);
    if ($ext == "gif"){
        $img = imagecreatefromgif($target);
    } else if($ext =="png"){
        $img = imagecreatefrompng($target);
    } else {
        $img = imagecreatefromjpeg($target);
    }
    $tci = imagecreatetruecolor($w, $h);
    // imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
    imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
    if ($ext == "gif"){
        imagegif($tci, $newcopy);
    } else if($ext =="png"){
        imagepng($tci, $newcopy);
    } else {
        imagejpeg($tci, $newcopy, 84);
    }
}

// ---------------- THUMBNAIL (CROP) FUNCTION ------------------
// Function for creating a true thumbnail cropping from any jpg, gif, or png image files
function ak_img_thumb($target, $newcopy, $w, $h, $ext) {
    list($w_orig, $h_orig) = getimagesize($target);
    $src_x = ($w_orig / 2) - ($w / 2);
    $src_y = ($h_orig / 2) - ($h / 2);
    $ext = strtolower($ext);
    $img = "";
    if ($ext == "gif"){
        $img = imagecreatefromgif($target);
    } else if($ext =="png"){
        $img = imagecreatefrompng($target);
    } else {
        $img = imagecreatefromjpeg($target);
    }
    $tci = imagecreatetruecolor($w, $h);
    imagecopyresampled($tci, $img, 0, 0, $src_x, $src_y, $w, $h, $w, $h);
    if ($ext == "gif"){
        imagegif($tci, $newcopy);
    } else if($ext =="png"){
        imagepng($tci, $newcopy);
    } else {
        imagejpeg($tci, $newcopy, 84);
    }
}

// ------------------ IMAGE CONVERT FUNCTION -------------------
// Function for converting GIFs and PNGs to JPG upon upload
function ak_img_convert_to_jpg($target, $newcopy, $ext) {
    list($w_orig, $h_orig) = getimagesize($target);
    $ext = strtolower($ext);
    $img = "";
    if ($ext == "gif"){
        $img = imagecreatefromgif($target);
    } else if($ext =="png"){
        $img = imagecreatefrompng($target);
    }
    $tci = imagecreatetruecolor($w_orig, $h_orig);
    imagecopyresampled($tci, $img, 0, 0, 0, 0, $w_orig, $h_orig, $w_orig, $h_orig);
    imagejpeg($tci, $newcopy, 84);
}
?>
	
