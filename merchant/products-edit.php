<?php
include('includes/header.php');
?>

<?php
$destination = IMG_PATH;

    if(isset($_POST['submit'])) {
    $product_id = mysql_real_escape_string($_POST['product_id']);
    $category_id = mysql_real_escape_string($_POST['category_id']);
    $sub_cat_id = mysql_real_escape_string($_POST['sub_cat_id']);
    $sub_sub_cat_id = mysql_real_escape_string($_POST['sub_sub_cat_id']);
    $product_name = mysql_real_escape_string($_POST['product_name']);
    $brand = mysql_real_escape_string($_POST['brand']);
    $list_price = mysql_real_escape_string($_POST['list_price']);
    $product_price = mysql_real_escape_string($_POST['product_price']);
    $product_detail = mysql_real_escape_string($_POST['product_detail']);

//    $sub_sub_cat_id = $_POST['sub_sub_cat_id'];

    $prod_id = $_REQUEST['product_id'];
    //get the status
    $stat_check = mysql_query("SELECT * FROM products WHERE id=$prod_id") or die(mysql_error());
    $stat_fetch = mysql_fetch_object($stat_check);
    if($stat_fetch->isactive =='d')
    {
        //update database table
        if(isset($_POST['merchant_id'])){
            $merchant_id = $_POST['merchant_id'];
            $update_product = mysql_query("UPDATE products SET product_name='{$product_name}', pid='{$provider_id}',
    category='{$category_id}', sub_category='{$sub_cat_id}', sub_sub_category='{$sub_sub_cat_id}',
    product_price='{$product_price}', list_price='{$list_price}', product_detail='{$product_detail}', isactive='p',
    brand='{$brand}',real_merchant_id='{$merchant_id}' WHERE id='{$product_id}'") or die(mysql_error());
        }
        else
        {
            $update_product = mysql_query("UPDATE products SET product_name='{$product_name}', pid='{$provider_id}',
    category='{$category_id}', sub_category='{$sub_cat_id}', sub_sub_category='{$sub_sub_cat_id}',
    product_price='{$product_price}', list_price='{$list_price}', product_detail='{$product_detail}', isactive='p',
    brand='{$brand}' WHERE id='{$product_id}'") or die(mysql_error());
        }



    }
    else
    {
        //update database table
        if(isset($_POST['merchant_id'])){
            $merchant_id = $_POST['merchant_id'];
            $update_product = mysql_query("UPDATE products SET product_name='{$product_name}', pid='{$provider_id}',
    category='{$category_id}', sub_category='{$sub_cat_id}', sub_sub_category='{$sub_sub_cat_id}',
    product_price='{$product_price}', list_price='{$list_price}', product_detail='{$product_detail}',
    brand='{$brand}',real_merchant_id='{$merchant_id}' WHERE id='{$product_id}'") or die(mysql_error());
        }
        else
        {
            $update_product = mysql_query("UPDATE products SET product_name='{$product_name}', pid='{$provider_id}',
    category='{$category_id}', sub_category='{$sub_cat_id}', sub_sub_category='{$sub_sub_cat_id}',
    product_price='{$product_price}', list_price='{$list_price}', product_detail='{$product_detail}',
    brand='{$brand}' WHERE id='{$product_id}'") or die(mysql_error());
        }


    }

    if($update_product) {
        header('location:'.SITE_URL.'merchant/products.php?edit=success');
    }
    else {
        header('location:'.SITE_URL.'merchant/products.php?edit=failure');
    }
}

?>

<div id="content">

    <?php include('includes/sidebar.php') ?>

    <div id="container">

        <h2 class="caps">Edit Product</h2>

        <?php
        if(isset($_GET['product_id'])) {
            $product_id = $_GET['product_id'];
            $q = mysql_query("SELECT * FROM products WHERE id='{$product_id}'");
            $product_info = mysql_fetch_object($q);
//                var_dump($product_info);
            $cat = mysql_query("SELECT * FROM category WHERE id='{$product_info->category}'");
            $show_cat = mysql_fetch_object($cat);

            $subcat = mysql_query("SELECT * FROM category WHERE id='{$product_info->sub_category}'");
            $show_subcat = mysql_fetch_object($subcat);

            $subsubcat = mysql_query("SELECT * FROM category WHERE id='{$product_info->sub_sub_category}'");
            $show_subsubcat = mysql_fetch_object($subsubcat);
        ?>

        <div class="main">
            <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data" id="edit_product_form">
                <div class="form_process"></div>

                <div class="content"></div>

                <div class="content" id="form_success">
                    <div class="form_con float_lft">
                        <div class="form_itm">
                            <label>Select Category*</label>
                            <select name="category_id" class="selection" id="category_id">
                                <option value="<?php echo $product_info->category ?>"><?php
                                    echo $show_cat->category_name ?>
                                </option>
                                <?php
                                $main_cat = mysql_query("SELECT * FROM category WHERE parent_id = 0 ORDER BY category_name");

                                while($main_cat_data = mysql_fetch_object($main_cat)) {
//                                    var_dump($main_cat_data);
                                    ?>
                                            <option <?php if($product_info->category===$main_cat_data->id){ ?> selected="selected" <?php } ?> value="<?php echo $main_cat_data->id ?>" >
                                        <?php echo $main_cat_data->category_name?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form_itm" id="sub_id">
                            <label>Select Sub Category</label>
                            <select name="sub_cat_id" class="selection" id="sub_cat_id" >
                                <option value="">Sub Category</option>
                                <?php
                                $subcat2 = mysql_query("SELECT * FROM category WHERE parent_id ='{$product_info->category}' ORDER BY category_name");
                                while($subcat_data = mysql_fetch_object($subcat2)) {
                                    ?>
                                    <option  <?php if($product_info->sub_category===$subcat_data->id){ ?> selected="selected" <?php } ?> value="<?php echo $subcat_data->id ?>">
                                        <?php echo $subcat_data->category_name?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>

                        <div class="form_itm" id="extra_id">
                            <label>Select Sub Category</label>
                            <select name="sub_sub_cat_id" class="selection" id="sub_sub_cat_id">
                                <option value="<?php echo $product_info->sub_sub_cat_id ?>"><?php echo
                                    $show_subsubcat->category_name ?></option>
                                <?php
                                $subcat3 = mysql_query("SELECT * FROM category WHERE parent_id = '{$product_info->sub_category}' ORDER BY category_name");
                                while($subsubcat_data = mysql_fetch_object($subcat3)) {
                                    ?>
                                    <option value="<?php echo $subsubcat_data->id ?>" <?php if($product_info->sup_sub_category===$subsubcat_data->id){ ?> selected="selected" <?php } ?>>
                                        <?php echo $subsubcat_data->category_name?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                        <input type="hidden" name="product_id" id="product_name" class="textfield"
                               placeholder="Product Name"
                               value="<?php echo $_REQUEST['product_id']; ?>" />
                        <div class="form_itm">
                            <label>Product Name</label>
                            <input type="text" name="product_name" id="product_name" class="textfield"
                                   placeholder="Product Name"
                                   value="<?php if(isset($product_info->product_name)){
                                       echo $product_info->product_name; } ?>" />
                        </div>

                        <br class="clerafix" />
                    </div>

                    <div class="form_con float_rgt">
                        <div class="form_itm">
                            <label>List Price</label>
                            <input type="text" name="list_price" id="list_price" class="textfield"
                                   placeholder="List Price"
                                   value="<?php if(isset($product_info->list_price)){
                                       echo $product_info->list_price; } ?>"/>
                        </div>

                        <div class="form_itm">
                            <label>Product Price</label>
                            <input type="text" name="product_price" id="product_price" class="textfield"
                                   placeholder="Product Price"
                                   value="<?php if(isset($product_info->product_price)){
                                       echo $product_info->product_price; } ?>" />
                        </div>

                        <div class="form_itm">
                            <label>Brand</label>
                            <input type="text" name="brand" id="brand" class="textfield"
                                   placeholder="Brand"
                                   value="<?php if(isset($product_info->brand)){
                                       echo $product_info->brand; } ?>" />
                        </div>
                        <?php
                        //echo $merchant->first_name;
                        $merchant = mysql_fetch_object(mysql_query("SELECT * FROM users WHERE id=$provider_id"));
                        if($merchant->first_name == 'Cyber Week')
                        {?>
                            <div class="form_itm">
                                <select name="merchant_id" class="selection" id="category_id">
                                    <option value="">Merchant</option>
                                    <?php
                                    $main_cat = mysql_query("SELECT * FROM users WHERE isactive = 't' AND first_name!='' ORDER BY first_name ASC");
                                    while($main_cat_data = mysql_fetch_object($main_cat)) {
                                        ?>
                                        <option value="<?php echo $main_cat_data->id ?>">
                                            <?php echo $main_cat_data->first_name?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <div class="form_itm_error" id="sel_cat_error"></div>
                            </div>
                        <?php }
                        ?>

                        <input type="hidden" name="product_id" id="product_id"
                               value="<?php echo $product_id; ?>" />

                        <br class="clearfix" />
                    </div>

                    <br class="clearfix" />
                    <div class="form_itm">
                        <label>Product Detail</label>
                        <textarea name="product_detail" class="textarea" id="product_details"
                                  placeholder="Product Detail"><?php if(isset($product_info->product_detail)){
                                echo $product_info->product_detail; } ?></textarea>
                    </div>

                    <div class="form_itm">
                        <input type="submit" name="submit" id="edit_product" class="form_btn" value="Save" />

                        <div class="cancel_btn"><a href="products.php">Cancel</a></div>
                    </div>

                    <br class="clearfix" />
                </div>
            </form>
        </div>

        <?php
        }
        ?>

    </div>

    <br class="clearfix" />

</div>

<?php
include('includes/footer.php')
?>

<script type="text/javascript" src="<?php echo SITE_URL; ?>merchant/js/tiny_mce/tiny_mce.js"></script>

<script type="text/javascript">
    tinyMCE.init({
        // General options
        mode : "textareas",
        theme : "advanced",
        plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media",

        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft," +
        "justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,cleanup,help,code,|,insertdate,inserttime,preview",
        //theme_advanced_buttons2 : "",

        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        content_css : "css/content.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js",
        media_external_list_url : "lists/media_list.js",

        // Style formats
        style_formats : [
            {title : 'Bold text', inline : 'b'},
            {title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
            {title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
            {title : 'Example 1', inline : 'span', classes : 'example1'},
            {title : 'Example 2', inline : 'span', classes : 'example2'},
            {title : 'Table styles'},
            {title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
        ],

        // Replace values for the template plugin
        template_replace_values : {
            username : "Some User",
            staffid : "991234"
        }
    });
</script>

</body>
</html>