<?php
include('includes/header.php');
?>

<?php
$display = '';
if(isset($_GET['product_id'])) {
    $product_id = $_GET['product_id'];
    $q = mysql_query("SELECT * FROM products WHERE id='{$product_id}'");
    $product_info = mysql_fetch_object($q);
}
?>

    <div id="content">

        <?php include('includes/sidebar.php') ?>

        <div id="container">

            <h2 class="caps">Update Product Inventory</h2>


            <div class="main">
                <div class="content">
                    <h5 class="light"><?php
                        if(isset($_GET['product_id'])) {
                            $product_id = $_GET['product_id'];
                            $q = mysql_query("SELECT * FROM products WHERE id='{$product_id}'");
                            $product_info = mysql_fetch_object($q);
                            echo $product_info->product_name;
                        }
                        ?>
                    </h5><br />

                    <div class="form_con float_lft">
                        <form action="" method="post" id="update_inventory">
                            <h5>Add Product Inventory</h5>

                            <div class="errors" id="inventory_error2"></div>

                            <div class="form_itm">
                                <div class="add_more_btn float_rgt"><a href="#">Add More Colors and Sizes</a></div>
                            </div>

                            <div class="form_itm">
                                <select name="color" class="selection" id="select_color">
                                    <option value="">Select Color</option>
                                    <?php
                                    if(isset($_GET['product_id'])) {
                                        $product_id = $_GET['product_id'];
                                        $colored = mysql_query("SELECT * FROM product_color WHERE
                                            pid=$product_id");
                                        while($color_det = mysql_fetch_object($colored)){
                                            ?>
                                            <option value="<?php echo $color_det->id ?>"><?php echo
                                                $color_det->color ?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form_itm">
                                <select name="size" class="selection" id="select_size">
                                    <option value="">Select Size</option>
                                    <?php
                                    if(isset($_GET['product_id'])) {
                                        $product_id = $_GET['product_id'];
                                        $sized = mysql_query("SELECT * FROM product_size WHERE
                                            pid=$product_id");
                                        while($size_det = mysql_fetch_object($sized)){
                                            ?>
                                            <option value="<?php echo $size_det->id ?>"><?php echo
                                                $size_det->size ?></option>
                                        <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form_itm">
                                <input type="number" name="quantity" id="quantity" class="textfield"
                                       placeholder="Enter Product Quantity" />
                            </div>

                            <input type="hidden" name="product_id" id="curr_prdt_id3"
                                   value="<?php if(isset($_GET['product_id'])){echo
                                   $_GET['product_id']; } ?>" />

                            <div class="form_itm">
                                <input type="button" name="edit_inventory" id="edit_inventory" class="form_btn"
                                       value="Save" />

                                <div class="cancel_btn"><a href="products.php">Cancel</a></div>
                            </div>
                        </form>
                    </div>

                    <div class="form_con float_rgt">

                        <h5>Product Inventory</h5>
                        <div class="show_inventory"></div>
                    </div>
                </div>

            </div>


        </div>

        <br class="clearfix" />

    </div>

<?php
include('includes/footer.php')
?>



<?php
if(isset($_GET['product_id'])){
    $product_id = $_GET['product_id'];
?>

<script type="text/javascript">
    $(document).ready(function(){
        $('.add_more_btn').click(function(){
            $('#popup_overlay').css('display', 'block');
            $('#popup').css('display', 'block');
        });

        $('.close_pop_up').click(function(){
            $('#popup_overlay').css('display', 'none');
            $('#popup').css('display', 'none');
            window.location.href='product-inventory-edit.php?product_id=<?php echo $product_id ?>';
        });
    })
</script>

<script type="text/javascript">
    $(document).ready(function(){

        function displayInventory(){
            var product_id = <?php echo $product_id; ?>;
            $.ajax({
                type:"post",
                url:"add_product_inventory.php",
                data:"product_id="+product_id+"&action=displayinventory",
                success:function(data){
                    $(".show_inventory").html(data);
                }
            });
        }

        displayInventory();

        $("#edit_inventory").click(function(){

            var prdt_id = <?php echo $product_id; ?>;
            var size=$("#select_size").val();
            var color=$("#select_color").val();
            var quantity=$("#quantity").val();

            $.ajax({
                type:"post",
                url:"add_product_inventory.php",
                data:"prdt_id="+prdt_id+"&size="+size+"&color="+color+"&quantity="+quantity+"&action=updateinventory",
                success:function(data){
                    alert(data);
                    if(data=='failed') {
                        $("#inventory_error2").html("<p style='color:#333;'>An error was encountered</p>");
                    }

                    else {
                        displayInventory();
                        $("#inventory_error2").html("");
                        $("#quantity").val("");
                    }
                }
            });

        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){

        function displayColor(){
            var prdt_id = <?php echo $product_id; ?>;
            $.ajax({
                type:"post",
                url:"add_product_color.php",
                data:"prdt_id="+prdt_id+"&action=displaycolor",
                success:function(data){
                    $(".show_avail_color").html(data);
                }
            });
        }

        displayColor();

        $("#add_color").click(function(){

            var prdt_id1 = <?php echo $product_id; ?>;
            var color = $("#updatecolor").val();

            if(color != '')
            {
                $.ajax({
                    type:"post",
                    url:"add_product_color.php",
                    data:"prdt_id1="+prdt_id1+"&color="+color+"&action=addcolor",
                    success:function(data){

                        if(data=='failed') {
                            $("#color_error2").html("<p style='color:#333;'>There was an error encountered</p>");
                        }
                        else if(data == 'color_added'){
                            $("#color_error2").html("<p>This color has already been added</p>")
                        }
                        else {
                            displayColor();
                            $("#color_error2").html("");
                            $("#updatecolor").val("");
                        }
                    }
                });
            }
            else
            {
                alert("Colour Can't Be Empty")
            }


        });

        //
        function displaySize(){
            var product_id2 = <?php echo $product_id; ?>;
            $.ajax({
                type:"post",
                url:"add_product_size.php",
                data:"product_id2="+product_id2+"&action=displaysize",
                success:function(data){
                    $(".show_avail_size").html(data);
                }
            });
        }

        displaySize();

        $("#add_size").click(function(){

            var prdt_id2 = <?php echo $product_id; ?>;
            var size = $("#updatesize").val();
             if(size != '')
             {
                 $.ajax({
                     type:"post",
                     url:"add_product_size.php",
                     data:"prdt_id2="+prdt_id2+"&size="+size+"&action=addsize",
                     success:function(data){

                         if(data=='failed') {
                             $("#size_error2").html("<p style='color:#333;'>There was an error encountered</p>");
                         }
                         else if(data == 'size_added'){
                             $("#size_error2").html("<p>This size has already been added</p>")
                         }
                         else {
                             displaySize();
                             $("#size_error2").html("");
                             $("#updatesize").val("");
                         }
                     }
                 });
             }
             else
             {
                 alert("Size Can't Be Empty")
             }


        });

    });

</script>

<?php
}
?>

</body>
</html>