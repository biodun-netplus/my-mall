<?php
include('includes/header.php');
require_once('../includes/classes/upload.php');
?>

<?php

if(isset($_POST['submit'])) {
    if(empty($_FILES['merchant_logo'])) {
        $message = "You did not select any file.";
    }
    else {
        $width = '200';
        $height = '200';
        $rand= date('his').rand(10,99);
        $id = $_POST['user_id'];
        $file = $_FILES['merchant_logo'];
        $merchant_logo = $rand.$file['name'];
        $destination = '../banner_images/';
        $message = "";

        $file_info = getimagesize($file['tmp_name']);
        $uploadWidth  = $file_info[0];
        $uploadHeight = $file_info[1];
        $uploadType = $file_info[2];
        $filesize = $file['size'];
        $fileTmpLoc = $file['tmp_name'];

        $merchant_logo = preg_replace('#[^a-z.0-9]#i', '', $merchant_logo); // filter the $filename
        $uploadLoc = $destination.$merchant_logo;

        if($uploadType == 1) {
            $ext = 'gif';
        }
        else if($uploadType == 2) {
            $ext = 'jpg';
        }
        else if($uploadType == 3) {
            $ext = 'png';
        }

        if ($uploadType != 1 && $uploadType != 2 && $uploadType != 3) {
            $message = "The image you are trying to upload is the wrong format <br />";
        }
        else {
            if($filesize > 1024000) {
                $message = "The file is larger than the allowable file size <br />";
            }
            else {

                $logo_upload = move_uploaded_file($fileTmpLoc, $uploadLoc);

                if($logo_upload) {

                        $add_logo = mysql_query("UPDATE users SET image = '{$merchant_logo}' WHERE
      id = '{$provider_id}'");
                        $message = "The logo was updated successfully";

                }
                else {
                    $message = "The Logo was not added <br />";
                }

            }
        }
    }

}

?>

<?php

$prof_info = mysql_fetch_object(mysql_query("select * from users where id = '" . $provider_id . "'"));

//$logo_info = mysql_fetch_object(mysql_query("select * from provider_banner where provider_id = '" . $provider_id ."'"));

if(!file_exists('../banner_images/'.$prof_info->image) || $prof_info->image=='-') {
    $img = SITE_URL.'banner_images/_no_image.png';
}
else {
    $img = SITE_URL.'banner_images/'.$prof_info->image;
}
?>

<div id="content">

    <?php include('includes/sidebar.php') ?>

    <div id="container">

        <h2 class="caps">Update Logo</h2>
        <?php
        if(isset($message) && !empty($message)){
            echo '<div class="alert">'.$message.'</div>';
        }
        ?>
        <div class="main">
            <div class="profile_logo">
                <img src="<?php echo $img ?>" />
            </div>
        </div>

        <div class="main">
            <div class="form_con">

                <form enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">

                    <div class="form_itm filefield">
                        <label>Merchant Logo</label>
                        <input type="file" name="merchant_logo" id="merchant_logo" />
                    </div>

                    <input type="hidden" name="user_id"
                           value="<?php echo $provider_id ?>" />

                    <div class="form_itm">
                        <input type="submit" name="submit" id="submit" class="form_btn" value="Save" />
                    </div>

                </form>

            </div>
        </div>

    </div>

    <br class="clearfix" />

</div>

<?php
include('includes/footer.php')
?>

</body>
</html>