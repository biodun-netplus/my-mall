<?php
include('includes/header.php');
?>


<?php
if(isset($_GET['product_id'])) {
    $product_id = $_GET['product_id'];
    $products = mysql_query("select * from products where pid = '{$provider_id}' AND id = '{$product_id}'");
    $product_det = mysql_fetch_object($products);
}

?>

    <div id="content">

        <?php include('includes/sidebar.php') ?>

        <div id="container">

            <h2 class="caps">Product Detail</h2>

            <div class="btn profile_btn">
                <a href="products-edit.php?product_id=<?php echo $product_det->id ?>">Edit Product</a>
            </div>

            <div class="main">
                <div class="form_con float_lft">
                    <h5><?php echo $product_det->product_name ?></h5>

                    <br />
                    <h5 class="light">Product Details:</h5>
                    <p><?php echo $product_det->product_detail ?></p><br /><br />
                    <p><b>Product Price:</b> <?php echo $product_det->product_price ?></p>

                    <p><b>List Price:</b> <?php echo $product_det->list_price ?></p>

                    <p><b>Brand:</b> <?php echo $product_det->brand ?></p>

                    <p><b>Status:</b> <?php echo $product_det->product_price ?></p>
                    <?php
                    $cat_id = $product_det->category;
                    $cat = mysql_query("SELECT * FROM category WHERE id=$cat_id");
                    $cat_det = mysql_fetch_object($cat);
                    ?>
                    <p><b>Category:</b> <?php echo $cat_det->category_name ?></p>
                    <?php
                    $sub_cat_id = $product_det->sub_cat_id;
                    $sub_cat = mysql_query("SELECT * FROM category WHERE id=$sub_cat_id");
                    $sub_cat_det = mysql_fetch_object($sub_cat);
                    ?>
                    <p><b>Sub Category:</b> <?php echo $sub_cat_det->category_name ?></p>
                    <?php
                    $sub_sub_cat_id = $product_det->sub_sub_cat_id;
                    $sub_sub_cat = mysql_query("SELECT * FROM category WHERE id=$sub_sub_cat_id");
                    $sub_sub_cat_det = mysql_fetch_object($sub_sub_cat);
                    ?>
                    <p><b>Sub Sub Category:</b> <?php echo $sub_sub_cat_det->category_name ?></p>
                </div>

                <div class="form_con float_rgt">

                    <div class="product_det_img">
                        <?php
                        if(!file_exists('../product_images/'.$product_det->image) || $product_det->image == '') {
                            ?>
                            <img src="<?php echo SITE_URL ?>banner_images/_no_image.png"
                                 alt="product_image1" />

                            <?php

                        }
                        else {
                           ?>
                            <img src="<?php echo SITE_URL ?>product_images/<?php echo $product_det->image ?>"
                                 alt="product_image1" />

                            <?php
                        }

                        if(!file_exists('../product_images/'.$product_det->image2) || $product_det->image2 == '' ) {

                            ?>
                            <img src="<?php echo SITE_URL ?>banner_images/_no_image.png"
                                 alt="product_image1" />

                            <?php
                        }
                        else {
                            ?>
                            <img src="<?php echo SITE_URL ?>product_images/<?php echo $product_det->image2 ?>"
                                 alt="product_image1" />

                            <?php

                        }
                        ?>


                        <br class="clearfix" />
                    </div>



                </div>

                <br class="clearfix" />
            </div>

            <div class="main border-top">
                <div class="add_more_btn">
                    <a href="products.php">Back to Product Index</a>
                </div>

                <div class="add_more_btn float_rgt">
                    <a href="product-image-edit.php?product_id=<?php echo $product_det->id ?>">Change Product Image</a>
                </div>
            </div>

        </div>

        <br class="clearfix" />

    </div>

<?php
include('includes/footer.php')
?>

</body>
</html>