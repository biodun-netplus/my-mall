<?php
require_once('../includes/classes/env.php');
require_once('../includes/classes/db_connect.php');

if (!isset($_SESSION['userDetail'])) {
    header("location: " . SITE_URL ."merchant/index.php" );
} else {
    $provider_id = $_SESSION['userDetail']->id;
}

?>

<?php
$prof_info = mysql_fetch_object(mysql_query("select * from users where id = '" . $provider_id . "'"));
?>

<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>MyMall Merchant Control Panel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo SITE_URL ?>merchant/css/reset.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL ?>merchant/css/admin.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL ?>merchant/css/jquery.steps.css"  rel="stylesheet" />

    <script type="text/javascript" src="<?php echo SITE_URL; ?>merchant/js/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="<?php echo SITE_URL; ?>merchant/js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="<?php echo SITE_URL ?>merchant/js/Chart.js"></script>

    <script type="text/javascript" src="<?php echo SITE_URL; ?>merchant/js/jquery.cookie-1.3.1.js"></script>

</head>
<body>
<!--http://localhost/mymall_repo/assets/img/admin-logo.png-->
<header>
    <div id="logo">
        <a href="<?php echo SITE_URL ?>"> <img src="<?php echo SITE_URL ?>assets/img/admin-logo.png" style="width: 125px"/></a>
    </div>

    <div id="header_lft">
        <a href="<?php echo SITE_URL ?>merchant/logout.php"> <div class="logout_icon"></div></a>
        <a href="<?php echo SITE_URL ?>merchant/messages.php"> <div class="contact_icon"></div></a>
        <div class="welcome">Welcome, <?php echo $prof_info->first_name; ?></div>
    </div>
</header>