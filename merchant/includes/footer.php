<footer>


</footer>

<div id="popup_overlay"></div>
<div id="popup">
    <div class="popup_content">
        <div class="close_pop_up"></div>
        <div class="content">
            <h4>Add Color and Size</h4>
            <form action="" method="post">
                <div class="form_con float_lft">
                    <h5>Available Colors</h5>
                    <div class="errors" id="color_error2"></div>

                    <div class="show_avail_color"></div>
                    <br />

                    <form action="" method="post" id="update_color">
                        <div class="form_itm" id="hidden_prdt_id">
                            <input type="text" name="color" id="updatecolor" class="textfield"
                                   placeholder="Enter Color" />
                        </div>

                        <div class="form_itm">
                            <input type="button" name="add_color" id="add_color" class="form_btn"
                                   value="Save Color" />
                        </div>
                    </form>
                </div>

                <div class="form_con float_rgt">
                    <h5>Available Sizes</h5>
                    <div class="errors" id="size_error2"></div>

                    <div class="show_avail_size"></div>
                    <br />

                    <form action="" method="post" id="size_update">
                        <div class="form_itm">
                            <input type="text" name="size" id="updatesize" class="textfield"
                                   placeholder="Enter Size" />
                        </div>

                        <div class="form_itm">
                            <input type="button" name="add_size" id="add_size" class="form_btn"
                                   value="Save Size" />
                        </div>
                    </form>
                </div>

                <br class="clearfix">
            </form>
        </div>
    </div>
</div>

<div id="instruction_overlay"></div>
<div id="instruction_popup">
    <div class="instruction_content">
        <div class="close_instruction"></div>
        <div class="content">
            <h2>Product Upload Guide</h2>
            <div class="section"><!--- The Category and sub category option -->
                <div class="section_txt float_lft">
                    <h5 class="light">1. Category and Sub categories</h5>
                    <p>Select the Primary category of the product</p>
                    <p>Select the Sub category and the sub sub-category of the product</p><br />

                    <h5>2. Product Name</h5>
                    <p>Enter the name of the product. Keeep the name of the product as brief as possible but also as
                        descriptive as possible.
                    </p><br />

                    <h5>3. Product Brand</h5>
                    <p>Enter the name of the brand. Most times this will be the name of the manufacturer of the
                        product.</p>
                </div>

                <div class="section_img float_rgt">
                    <img src="<?php echo SITE_URL_MERC ?>img/add_product1.jpg" />
                </div>
                <br class="clearfix" />
            </div>

            <div class="section">
                <div class="section_img float_lft">
                    <img src="<?php echo SITE_URL_MERC ?>img/add_product2.jpg" />
                </div>
                <div class="section_txt float_rgt">
                    <h5 class="light">4. List Price</h5>
                    <p>The List price id the standard market price that the product is being sold for. If you are
                        not sure of what this value is, you can leave this field blank.</p><br />

                    <h5 class="light">5. Product Price</h5>
                    <p>The product price is the price that you as a merchant wish to sell your product.</p>
                </div>
                <br class="clearfix" />
            </div>

            <div class="section">
                <div class="section_img float_rgt">
<<<<<<< HEAD
                    <img src="<?php echo SITE_URL_MERC ?>img/add_product3.jpg" />
=======
                    <img src="<?php echo SITE_URL ?>merchant/img/add_product3.jpg" />
>>>>>>> master
                </div>
                <div class="section_txt float_lft">
                    <h5 class="light">6. Uploading Product Image</h5>
                    <p>The Product image is the most important aspect of a product. People react to visual
                        impressions better than they do with words. Hence, to get the best of results and attract
                        buyers to your store you need to upload the right kind of images.</p>
                </div>
                <br class="clearfix" />
            </div>

            <div class="section">
                <h5 class="light">7. Product Details</h5>
<<<<<<< HEAD
                <img src="<?php echo SITE_URL_MERC ?>img/add_product4.jpg" />
=======
                <img src="<?php echo SITE_URL ?>merchant/img/add_product4.jpg" />
>>>>>>> master
                <p>Please ensure that you provide as much details about the product as possible e.g. For Computers
                    provide information like the sceen size, screen resolution, type of processor, processing speed,
                    ram, type of graphics card, and other unique features it may have.
                </p>
                <p>The idea is for the buyer to have as much information on the product as possible.</p>
            </div>

            <br />
            <h4 class="light center">Image Upload Best Practice</h4>

            <div class="section">
                <div class="section_img float_lft">
                    <h5 class="light">Aspect Ratio</h5>

                    <img src="<?php echo SITE_URL_MERC ?>img/add_product5.jpg" />
                    <p>Please check that the aspect ratio of the image is 1:1. This means that the width and height
                        of the image are equal.</p>
                    <p>For best result, the images should have width and height of 400px each.</p>
                </div>
                <div class="section_img float_rgt">
                    <h5 class="light">Image Visibility and Size</h5>
                    <img src="<?php echo SITE_URL_MERC ?>img/add_product6.jpg" />
                    <p>Please ensure that the visual part of the image is larger than 75% of the image</p>
                </div>
                <br class="clearfix" />
            </div>

            <div class="section">
                <div class="section_img float_lft">
                    <h5 class="light">Cropped Images</h5>
                    <img src="<?php echo SITE_URL_MERC ?>img/add_product7.jpg" />
                    <p>It is not visually appealling for the images to be cropped. Please try to see that your
                        images are clear and properly visible.</p>
                </div>

                <div class="section_img float_rgt">
                    <h5 class="light">White Background for Image</h5>
                    <img src="<?php echo SITE_URL_MERC ?>img/add_product8.jpg" />
                    <p>When the background of an image is white, it is more visually appealing then when the image
                        has a background.</p>
                </div>
                <br class="clearfix" />
            </div>
            <br />


            <div class="section">
                <h4 class="light">Adding Color and Size</h4>
                <p>If the product you want to upload does not come in size or color classification, please skip it
                    and click on add inventory to product.</p>
                <img src="<?php echo SITE_URL_MERC ?>img/add_product9.jpg" />
                <br class="clearfix" />
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    $(document).ready(function() {
        $('#sidebar > ul > li > a').click(function() {
            $('#sidebar li').removeClass('active');
            $(this).closest('li').addClass('active');
            var checkElement = $(this).next();
            if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
                $(this).closest('li').removeClass('active');
                checkElement.slideUp('normal');
            }
            if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
                $('#sidebar ul ul:visible').slideUp('normal');
                checkElement.slideDown('normal');
            }
            if($(this).closest('li').find('ul').children().length == 0) {
                return true;
            } else {
                return false;
            }
        });
    })
</script>


<script type="text/javascript">
    $(document).ready(function(){
        $('.del_disp_color').on('click', function(){
            //alert('you just tried to delete');
        });

        $('.del_disp_size').on('click', function(){
            //alert('you just tried to delete');
        });
    });
</script>


<script type="text/javascript">

    function loadSubCat(){
        //alert('Changed Sub Category');
        var sub_category = $('#sub_cat_id').val();
        $.ajax({
            type: "POST",
            url: "select_sub_category.php",
            data: "sub_category="+sub_category,

            beforeSend:function()
            {
                $('#extra_id').html("<div class='load' ><img src='img/small_loader.gif'></div>");
            },

            success: function(retdata){
                $("#extra_id").html(retdata);
            }
        })
    }

    $(document).ready(function() {
        $("#category_id").change(function() {
            //alert('Changed');
            category_id = $('#category_id').val();
            $.ajax({
                type: "POST",
                url: "select_category.php",
                data: "category_id="+category_id,

                beforeSend:function()
                {
                    $('#sub_id').html("<div class='load' ><img src='img/small_loader.gif'></div>");
                },

                success: function(data){
                    $("#sub_id").html(data);
                }
            })
        });

    });
</script>


<script type="text/javascript">
    function delete_color(id)
    {
        var product_id = <?php echo $product_id; ?>;
        if(confirm('Are you sure you want to delete this colour?'))
        {
            $.ajax({
                type:"post",
                url:"add_product_color.php",
                data:"id="+id+"&prod_id="+product_id+"&action=deleteColour",
                success:function(data){
                    alert("Successful ")
                }
            });
        }

    }

    function delete_size(id)
    {
        var product_id = <?php echo $product_id; ?>;
        if(confirm('Are you sure you want to delete this size?'))
        {
            $.ajax({
                type:"post",
                url:"add_product_size.php",
                data:"id="+id+"&prod_id="+product_id+"&action=deleteSize",
                success:function(data){
//                    alert()
                    alert("Successful")
                }
            });
        }

    }
    function loadInventory(){
        var product_id = <?php echo $product_id; ?>;
        $.ajax({
            type:"post",
            url:"add_product_inventory.php",
            data:"product_id="+product_id+"&action=displayinventory",
            success:function(data){
                $(".show_inventory").html(data);
            }
        });
    }

    function deleteinventory(){
        //alert('Changed Sub Category');
        var id = $('#inventory_id').val();
        $.ajax({
            type: "POST",
            url: "inventory.php",
            data: "id="+id,

            beforeSend:function()
            {
                $('#message').html("<div class='load' ><img src='img/small_loader.gif'></div>");
            },

            success:function(data){

                if(data=='failed') {
                    $("#message").html("<p style='color:#333;'>An error was encountered</p>");
                }

                else {
                    loadInventory();
                    $("#message").html("");
                }
            }
        })
    }
</script>


<script type="text/javascript">
    function validate_product_name() {
        if($("#product_name").val()=='')
        {
            $("#product_name_error").html('Please Enter Product Name');
            return false;
        }
        else
        {
            $("#product_name_error").html('');
            return true;
        }
    }
    function validate_category() {
        if($("#category_id").val()=='')
        {
            $("#sel_cat_error").html('Please Select A Category');
            return false;
        }
        else
        {
            $("#sel_cat_error").html('');
            return true;
        }
    }

    function validate_brand() {
        if($("#brand").val()=='')
        {
            $("#brand_error").html('Please Enter Brand');
            return false;
        }
        else
        {
            $("#brand_error").html('');
            return true;
        }
    }

    function validate_product_price() {
        if($("#product_price").val()=='')
        {
            $("#product_price_error").html('Please Enter Product Price');
            return false;
        }
        else
        {
            $("#product_price_error").html('');
            return true;
        }
    }

    function validate_list_price() {
        if($("#list_price").val()=='')
        {
            $("#list_price_error").html('Please Enter Product Price');
            return false;
        }
        else
        {
            $("#list_price_error").html('');
            return true;
        }
    }

    function validate_product_details() {
        if($("#product_details").val()=='')
        {
            $("#product_detail_error").html('Please Enter Product Detail');
            return false;
        }
        else
        {
            $("#product_detail_error").html('');
            return true;
        }
    }

    function validate_product_image1() {
        if($("#product_image1").val()== '')
        {
            $("#product_image1_error").html('You did not select an image')
        }
        else
        {
            $("#product_image1_error").html('');
            return true;
        }
    }

    $(document).ready(function() {
        $("form#add_product_form").submit(function(event){
            //disable the default form submission
            tinyMCE.triggerSave();
            //alert( $('#product_details').val());

            event.preventDefault();
            //grab all form data
            var formData = new FormData($(this)[0]);

            $("form#add_product_form").serializeArray().forEach(function(field) {
                formData.append(field.name, field.value)
//                console.log(field.name);
//                console.log(field.value);
            })

            if(validate_product_name() & validate_brand() & validate_product_price() & validate_product_image1() & validate_product_details() & validate_category()) {
                $.ajax({
                    url: 'uploadproducts.php',
                    type: 'POST',
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,

                    beforeSend: function () {
                        $('.form_process').html("<h5>Uploading Product...</h5>");
                    },

                    success: function (html) {
//                        alert(html);
                        if (html.indexOf('fail') > 0) {
                            $(".form_process").html("<h5>There was an error encountered</h5>");
                        }
                        else if (html.indexOf('no file') > 0) {
                            $(".form_process").html("<h5>You did not select an image</h5>");
                        }
                        else if (html.indexOf('wrong file') > 0) {
                            $(".form_process").html("<h5>The file you selected is the wrong image file type</h5>");
                        }
                        else if (html.indexOf('large file') > 0) {
                            $(".form_process").html("<h5>The image is too large</h5>");
                        }
                        else if(html.indexOf('success') > 0) {
                            $('#form_success').html("<h5>Product Uploaded Successfully." +
                            "<a href='products-add2.php'>Click here</a> to add " +
                            "color and size to the product.</h5>");
                            $('.form_process').html("");
                        }
                        else {
                            $(".form_process").html("<h5>An error occurred</h5>");
                        }

                    }
                });
            }

            return false;
        });

    });
</script>


<script type="text/javascript">
    $(document).ready(function(){

        function showColor(){
            $.ajax({
                type:"post",
                url:"add_product_color.php",
                data:"action=showcolor",
                success:function(data){
                    $(".disp_avail_color").html(data);
                }
            });
        }

        showColor();

        $("#submit_color").click(function(){

            var prdt_id1=$("#curr_prdt_id1").val();
            var color=$("#color").val();

            $.ajax({
                type:"post",
                url:"add_product_color.php",
                data:"prdt_id1="+prdt_id1+"&color="+color+"&action=addcolor",
                beforeSend: function () {
                    $('#color_loading').html("<h5 style='font-size: 12px;color: black;'>Adding Colour...</h5>");
                },
                success:function(data){

                    if(data=='failed') {
                        $("#color_error").html("<p style='color:#333;'>There was an error encountered</p>");
                    }
                    else if(data == 'color_added'){
                        $("#color_error").html("<p>This color has already been added</p>")
                    }
                    else {
                        showColor();
                        $("#color_error").html("");
                        $("#color").val("");
                        $('#color_loading').html("");
                    }
                }
            });

        });



        //update and show size
        function showSize(){
            $.ajax({
                type:"post",
                url:"add_product_size.php",
                data:"action=showsize",
                success:function(data){
                    $(".disp_avail_size").html(data);
                }
            });
        }

        showSize();

        $("#submit_size").click(function(){

            var prdt_id2=$("#curr_prdt_id2").val();
            var size=$("#size").val();

            $.ajax({
                type:"post",
                url:"add_product_size.php",
                data:"prdt_id2="+prdt_id2+"&size="+size+"&action=addsize",
                beforeSend: function () {
                    $('#size_loading').html("<h5 style='font-size: 12px;color: black;'>Adding Size...</h5>");
                },
                success:function(data){

                    if(data=='failed') {
                        $("#size_error").html("<p style='color:#333;'>There was an error encountered</p>");
                    }
                    else if(data == 'size_added'){
                        $("#size_error").html("<p>This size has already been added</p>")
                    }
                    else {
                        showSize();
                        $("#size_error").html("");
                        $("#size").val("");
                        $('#size_loading').html("");
                    }
                }
            });

        });
    });
</script>


<script type="text/javascript">
    $(document).ready(function(){

        function showInventory(){
            $.ajax({
                type:"post",
                url:"add_product_inventory.php",
                data:"action=showinventory",
                success:function(data){
                    $(".disp_inventory").html(data);
                }
            });
        }

        showInventory();

        $("#submit_inventory").click(function(){
            //alert('submit was clicked');
            var prdt_id3=$("#curr_prdt_id3").val();
            var size=$("#select_size").val();
            var color=$("#select_color").val();
            var quantity=$("#quantity").val();

            $.ajax({
                type:"post",
                url:"add_product_inventory.php",
                data:"prdt_id3="+prdt_id3+"&size="+size+"&color="+color+"&quantity="+quantity+"&action=addinventory",
                beforeSend: function () {
                    $('#inventory_add').html("<h5 style='font-size: 12px;color: black;'>Adding Inventory...</h5>");
                },
                success:function(data){

                    if(data=='failed') {
                        $("#inventory_error").html("<p style='color:#333;'>An error was encountered</p>");
                    }

                    else {
                        showInventory();
                        $("#inventory_error").html("");
                        $("#quantity").val("");
                        $('#inventory_add').html("");
                    }
                }
            });

        });
    });
</script>





