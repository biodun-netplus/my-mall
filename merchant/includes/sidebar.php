<div id="sidebar">
    <ul>
        <li>
            <a href="<?php echo SITE_URL ?>merchant/index.php"><span>Dashboard</span></a>
        </li>

        <li class="has-sub">
            <a href="#"><span>Profile Information</span></a>
            <ul>
                <li><a href="<?php echo SITE_URL ?>merchant/profile.php"><span>Profile</span></a></li>
                <li><a href="<?php echo SITE_URL ?>merchant/banner.php"><span>Merchant Banner</span></a></li>
                <li><a href="<?php echo SITE_URL ?>merchant/change-logo.php"><span>Update Logo</span></a></li>
                <li><a href="<?php echo SITE_URL ?>merchant/meta-tags.php"><span>Meta Tags</span></a></li>
            </ul>
        </li>

        <li>
            <a href="<?php echo SITE_URL ?>merchant/products.php"><span>Products</span></a>
        </li>

        <li>
            <a href="<?php echo SITE_URL ?>merchant/orders.php"><span>My Orders</span></a>
        </li>

        <li>
            <a href="<?php echo SITE_URL ?>merchant/messages.php"><span>Messages</span></a>
        </li>

        <li>
            <a href="<?php echo SITE_URL ?>merchant/password.php"><span>Change Password</span></a>
        </li>

        <li>
            <a href="<?php echo SITE_URL ?>merchant/logout.php"><span>Logout</span></a>
        </li>
    </ul>
</div>