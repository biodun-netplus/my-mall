<?php
include('includes/header.php');
require_once('lib/functions.php');
?>
<script type="text/javascript">
    function changeStatus(id, status) {

        $.ajax({
            type: 'POST',
            url: "delete_category.php?prdid=" + id + "&status=" + status,
            success: function () {

            }
        });
    }
</script>
<?php
//calculate pagination info
$cur_page = !empty($_GET['page']) ? (int)$_GET['page'] : 1;
$results_per_page = 20;
$skip = (($cur_page - 1) * $results_per_page);

//query the first time to get total results
$product1 = mysql_query("select * from products where pid = '" . $provider_id . "' and isactive != 'n'");
$total = mysql_num_rows($product1);
$num_pages = ceil($total / $results_per_page);

//query again to get just the subsets of results
$prod = "select * from products where pid = '" . $provider_id . "'  and isactive != 'n' ORDER BY id DESC LIMIT $skip, $results_per_page ";
//echo $prod;
$products = mysql_query($prod) or die(mysql_error());

?>

<div id="content">

    <?php include('includes/sidebar.php') ?>

    <div id="container">

        <h2 class="caps">Products</h2>

        <div class="btn profile_btn"><a href="products-add.php">Add Product</a></div>
        <?php
        if (isset($_GET['del'])) {
            if ($_GET['del'] == 'success') {
                echo '<div class="notice success">The Product was successfully deleted</div>';
            } else {
                echo '<div class="notice failure">There was an error! The product was not deleted.</div>';
            }
        }
        if (isset($_GET['inventory'])) {
            if ($_GET['inventory'] == 'success') {
                echo '<div class="notice success">The Product inventory was successfully updated</div>';
            } else {
                echo '<div class="notice failure">There was an error! The product inventory was not updated</div>';
            }
        } else if (isset($GET['edit'])) {
            if ($_GET['edit'] == 'success') {
                echo '<div class="notice success">The Product was successfully edited</div>';
            } else {
                echo '<div class="notice failure">There was an error! The product was not edited.</div>';
            }
        } else if (isset($GET['image'])) {
            if ($_GET['image'] == 'success') {
                echo '<div class="notice success">The Product image was successfully changed.</div>';
            } else {
                echo '<div class="notice failure">There was an error! The product image was not edited.</div>';
            }
        }
        ?>
        <div class="main">

            <table width="100%" border="1" style="vertical-align: text-top;">
                <tr>
                    <td width="10%">Image</td>
                    <td width="20%">Name</td>
                    <td width="10%">Price</td>
                    <td width="10%">List Price</td>
                    <td width="10%">Brand</td>
                    <td width="10%">Status</td>
                    <td width="10%">Real Merchant</td>
                    <td width="15%">Inventory</td>
                    <td width="15%">Action</td>
                </tr>
                <?php
                while ($product_info = mysql_fetch_object($products)) {
//                    var_dump($product_info);
                    ?>
                    <tr>
                        <td>
                            <?php
                            if (!file_exists('../product_images/' . $product_info->image) || $product_info->image == '') {
                                ?>
                                <img src="<?php echo SITE_URL ?>banner_images/_no_image.png"
                                     class="prdt_img"/>

                                <?php

                            } else {
                                ?>
                                <img src="<?php echo SITE_URL ?>product_images/<?php echo $product_info->image ?>"
                                     class="prdt_img"/>

                                <?php
                            } ?>

                        </td>

                        <td>
                            <?php echo $product_info->product_name ?>
                        </td>

                        <td>
                            &#x20a6;&nbsp;<?php echo number_format($product_info->product_price) ?>
                        </td>

                        <td>
                            &#x20a6;&nbsp;<?php echo number_format($product_info->list_price) ?>
                        </td>

                        <td>
                            <?php echo $product_info->brand ?>
                        </td>

                        <td>

                        <?php if ($product_info->isactive == 'd'){
                            echo 'Declined';
                            echo '<br/><p style="color:red;">';
                            echo $product_info->reason;
                            echo '</p>';
                        }
                        else if ($product_info->isactive == 'p')
                        {
                            echo 'Pending';
                        }else {
                            ?>
                            <select onchange="javascript:changeStatus('<?php echo $product_info->id; ?>',this.value);">
                                <option value="t" <?php if($product_info->isactive=='t'){ echo 'selected';}?>>Available</option>
                                <option value="f"<?php if($product_info->isactive=='f'){ echo 'selected';}?>>Paused</option>
                            </select>
                            <?php
                        }
                        ?>
                        </td>
                        <td>
                            <?php
                            $merchant_id = $product_info->real_merchant_id;
                            //echo $product_id;

                            $invent = mysql_query("SELECT * FROM users WHERE id ='{$merchant_id}'");
                            $num = mysql_num_rows($invent);

                            $invent_det = mysql_fetch_object($invent);
                                echo $invent_det->first_name;

                            ?>
                        </td>

                        <td>
                            <?php
                            $product_id = $product_info->id;
                            //echo $product_id;
                            ?>
                            <?php
                            $invent = mysql_query("SELECT * FROM relation_color_size WHERE product_id =$product_id");

                            $num = mysql_num_rows($invent);
                            if ($num == 0) {
                                echo '<p>No inventory for this product</p>';
                            } else {
                                ?>
                                <table width="96%" style="margin: 0 auto 5px;border: solid 1px #ddd">
                                    <tr class="center">
                                        <th>Color</th>
                                        <th>Size</th>
                                        <th>Quantity</th>
                                    </tr>
                                    <?php
                                    while ($invent_det = mysql_fetch_object($invent)) {
                                        $size = $invent_det->size_id;
                                        $color = $invent_det->colour_id;

                                        if($size >0)
                                        {
                                            $get_size = mysql_fetch_object(mysql_query("SELECT * FROM product_size WHERE id=$size"));
                                        }
                                        if($color>0)
                                        {
                                            $get_color = mysql_fetch_object(mysql_query("SELECT * FROM product_color WHERE id=$color"));
                                        }
//                                        var_dump($get_size);
//                                        var_dump($get_color);
                                        ?>
                                        <tr class="center">
                                            <td><?php echo $get_color->color ?></td>
                                            <td><?php echo $get_size->size ?></td>
                                            <td><?php echo $invent_det->qty ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                                <?php
                            }
                            ?>
                        </td>

                        <td class="edit_icons">
                            <a href="products-edit.php?product_id=<?php echo $product_id ?>" title="edit product">
                                <img src="img/edit_icon.png"/>
                            </a>

                            <a href="product-inventory-edit.php?product_id=<?php echo $product_id ?>"
                               title="edit inventory"><img src="img/doc_edit.png"/>
                            </a>

                            <a href="product-image-edit.php?product_id=<?php echo $product_id ?>"
                               title="change image"><img src="img/image_icon.png"/>
                            </a>

                            <a href="products-detail.php?product_id=<?php echo $product_id ?>" title="details">
                                <img src="img/details_icon.png"/>
                            </a>

                            <a href="products-delete.php?product_id=<?php echo $product_id ?>" title="delete">
                                <img src="img/delete_icon.png"/>
                            </a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>

        </div>

        <div class="pagination_con">
            <?php
            if ($num_pages > 1) {
                ?>
                <div class="pagelinks">
                    <?php echo generate_page_links($cur_page, $num_pages); ?>
                </div>
                <?php
            }
            ?>
        </div>

    </div>

    <br class="clearfix"/>

</div>

<?php
include('includes/footer.php')
?>

</body>
</html>