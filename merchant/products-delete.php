<?php
include('includes/header.php');
?>

<?php
if(isset($_POST['submit'])) {
    $product_id = $_POST['id'];
    $q = mysql_query("UPDATE products SET isactive = 'n' WHERE id = '{$product_id}'");
    if($q) {
        header("location: products.php?del=success");
    }
    else {
        header("location: products.php?del=fail");
    }
}
?>

<?php
if(isset($_GET['product_id'])) {
    $product_id = $_GET['product_id'];
    $products = mysql_query("select * from products where pid = '{$provider_id}' AND id = '{$product_id}'");
    $product_det = mysql_fetch_object($products);
}

?>

    <div id="content">

        <?php include('includes/sidebar.php') ?>

        <div id="container">

            <h2 class="caps">Delete Product</h2>

            <div class="main">
                <h5>
                    You are about to delete this product. Please note that this action is not reversible. <br />
                    Click delete to complete your action. If this is an error, then click on cancel to go back to
                    products index.
                </h5>
            </div>

            <div class="main">
                <div class="form_con">
                    <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
                        <h5><?php echo $product_det->product_name ?></h5><br />
                        <h5 class="light">Product Details:</h5>
                        <p><?php echo $product_det->product_detail ?></p><br /><br />


                        <input type="hidden" name="id"
                               value="<?php if(isset($product_det->id)){echo $product_det->id;}?>" />

                        <div class="form_itm">
                            <input type="submit" name="submit" id="submit" class="form_btn" value="Delete" />
                            <div class="cancel_btn"><a href="products.php">Cancel</a></div>
                        </div>
                    </form>
                </div>

            </div>

        </div>

        <br class="clearfix" />

    </div>

<?php
include('includes/footer.php')
?>

</body>
</html>