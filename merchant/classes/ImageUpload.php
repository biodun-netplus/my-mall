<?php

class ImageUpload {

    var $message;
    var $uploadWidth;
    var $uploadHeight;
    var $uploadType;
    var $uploadLoc;

    public function upload($destination, $file, $width, $height) {
        // -- get some information about the file
        $file_info = getimagesize($file['tmp_name']);
        $this->uploadWidth  = $file_info[0];
        $this->uploadHeight = $file_info[1];
        $this->uploadType = $file_info[2];
        $filesize = $file['size'];
        $fileTmpLoc = $file['tmp_name']; // File in the PHP tmp folder
        $this->uploadLoc = $destination.$file;

        if($this->uploadType == 1) {
            $ext = 'gif';
        }
        else if($this->uploadType == 2) {
            $ext = 'jpg';
        }
        else if($this->uploadType == 3) {
            $ext = 'png';
        }

        if ($this->uploadType != 1 && $this->uploadType != 2 && $this->uploadType != 3) {
            $this->message = 'The image you are trying to upload is the wrong format';
        }
        else {
            if($filesize > 1024000) {
                $this->message = 'The file is larger than the allowable file size';
            }
            else {
                if($this->uploadWidth > $width) {
                    //resize the image.
                    $file = $this->resize($file, $newfile, $width, $height, $ext);
                    //upload the image;
                    move_uploaded_file($fileTmpLoc, $this->uploadLoc);
                }
                else {
                    //upload the image
                    move_uploaded_file($fileTmpLoc, $this->uploadLoc);
                }
            }
        }
    }

    public function resize($target, $newcopy, $width, $height, $ext) {
        list($uploadWidth, $uploadHeight) = getimagesize($target);
        $scale_ratio = $uploadWidth / $uploadHeight;
        if (($width / $height) > $scale_ratio) {
            $width = $height * $scale_ratio;
        } else {
            $height = $width / $scale_ratio;
        }
        $img = "";
        $ext = strtolower($ext);
        if ($ext == "gif"){
            $img = imagecreatefromgif($target);
        } else if($ext =="png"){
            $img = imagecreatefrompng($target);
        } else {
            $img = imagecreatefromjpeg($target);
        }
        $tci = imagecreatetruecolor($w, $h);
        // imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
        imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
        if ($ext == "gif"){
            imagegif($tci, $newcopy);
        } else if($ext =="png"){
            imagepng($tci, $newcopy);
        } else {
            imagejpeg($tci, $newcopy, 84);
        }
    }


    public function crop($target, $newcopy, $w, $h, $ext) {
        list($w_orig, $h_orig) = getimagesize($target);
        $src_x = ($w_orig / 2) - ($w / 2);
        $src_y = ($h_orig / 2) - ($h / 2);
        $ext = strtolower($ext);
        $img = "";
        if ($ext == "gif"){
            $img = imagecreatefromgif($target);
        } else if($ext =="png"){
            $img = imagecreatefrompng($target);
        } else {
            $img = imagecreatefromjpeg($target);
        }
        $tci = imagecreatetruecolor($w, $h);
        imagecopyresampled($tci, $img, 0, 0, $src_x, $src_y, $w, $h, $w, $h);
        if ($ext == "gif"){
            imagegif($tci, $newcopy);
        } else if($ext =="png"){
            imagepng($tci, $newcopy);
        } else {
            imagejpeg($tci, $newcopy, 84);
        }
    }
}

$imageupload = new ImageUpload();