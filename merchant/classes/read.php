<?php
/**
 * Created by PhpStorm.
 * User: Netplus-Paul
 * Date: 7/21/2015
 * Time: 8:45 AM
 */

class read
{

    public function read_single($table, $options, $limit = 0)
    {

        $query = 'SELECT * FROM ' . $table . ' WHERE ';
        $i = count($options);
        foreach ($options as $key => $val) {
            $i--;
            $query .= "`" . htmlspecialchars($key , ENT_QUOTES) . "` = '" . htmlspecialchars($val , ENT_QUOTES) . "'";
            if ($i != 0) {
                $query .= ' AND ';
            }
        }

        if($limit != 0){
            $query .= 'LIMIT '. $limit;
        }

        //echo $query;

        $localhost = mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);

        mysql_select_db(DB_DATABASE,$localhost);
        //echo $query;
        $fetcher = mysql_query($query,$localhost) or die('jdjdjd'.mysql_error());
        //echo $query;
        $rows = mysql_fetch_array($fetcher);
        $total = mysql_num_rows($fetcher);
        mysql_close($localhost);

        if ($total > 0) {
            return $rows;
        } else {
            return '-1';
        }

    }


    public function read_list($item, $table, $options, $limit = 0,$extras = '')
    {

        $query = "SELECT `" . $item . "` FROM `" . $table . "`";
        $i = count($options);

        if(count($options) > 0) {
            $query .= ' WHERE ';

            foreach ($options as $key => $val) {
                $i--;
                $query .= "`" . htmlspecialchars($key , ENT_QUOTES) . "` = '" . htmlspecialchars($val , ENT_QUOTES) . "'";
                if ($i != 0) {
                    $query .= ' AND ';
                }
            }

        }

        $query .= ' ';
        $query .= $extras;
        $query .= ' ';

        if($limit != 0){
            $query .= 'LIMIT '. $limit;
        }

        //echo $query.'<br>';
        $localhost = mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
        mysql_select_db(DB_DATABASE,$localhost);
        $fetcher = mysql_query($query,$localhost);
        $row_ads_list = mysql_fetch_assoc($fetcher);
        $items = $row_ads_list[$item];
        mysql_close($localhost);

        $list_items = array($items);

        do {
            $addss = $row_ads_list[$item];
            array_push($list_items, $addss);
        } while ($row_ads_list = mysql_fetch_assoc($fetcher));
        unset($list_items[0]);

        return $list_items;

    }


    public function read_list_like($item, $table, $options, $limit = 0)
    {

        $query = "SELECT `" . $item . "` FROM `" . $table . "`";
        $i = count($options);

        if(count($options) > 0) {
            $query .= ' WHERE ';

            foreach ($options as $key => $val) {
                $i--;
                $query .= "`" . htmlspecialchars($key , ENT_QUOTES) . "` LIKE '%" . htmlspecialchars($val , ENT_QUOTES) . "%'";
                if ($i != 0) {
                    $query .= ' AND ';
                }
            }

        }

        if($limit != 0){
            $query .= ' LIMIT '. $limit;
        }


        $localhost = mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
        mysql_select_db(DB_DATABASE,$localhost);
        $fetcher = mysql_query($query,$localhost);
        $row_ads_list = mysql_fetch_assoc($fetcher);
        $items = $row_ads_list[$item];
        mysql_close($localhost);

        $list_items = array($items);

        do {
            $addss = $row_ads_list[$item];
            array_push($list_items, $addss);
        } while ($row_ads_list = mysql_fetch_assoc($fetcher));
        unset($list_items[0]);

        return $list_items;

    }


    public function read_list_like_or($item, $table, $options, $limit = 0)
    {

        $query = "SELECT `" . $item . "` FROM `" . $table . "`";
        $i = count($options);

        if(count($options) > 0) {
            $query .= ' WHERE ';

            foreach ($options as $key => $val) {
                $i--;
                $query .= "`" . htmlspecialchars($key , ENT_QUOTES) . "` LIKE '%" . htmlspecialchars($val , ENT_QUOTES) . "%'";
                if ($i != 0) {
                    $query .= ' OR ';
                }
            }

        }

        if($limit != 0){
            $query .= ' LIMIT '. $limit;
        }

        $localhost = mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
        mysql_select_db(DB_DATABASE,$localhost);
        $fetcher = mysql_query($query,$localhost);
        $row_ads_list = mysql_fetch_assoc($fetcher);
        $items = $row_ads_list[$item];
        mysql_close($localhost);

        $list_items = array($items);

        do {
            $addss = $row_ads_list[$item];
            array_push($list_items, $addss);
        } while ($row_ads_list = mysql_fetch_assoc($fetcher));
        unset($list_items[0]);

        return $list_items;

    }

}

$read = new read();