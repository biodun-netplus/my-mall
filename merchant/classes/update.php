<?php
/**
 * Created by PhpStorm.
 * User: Netplus-Paul
 * Date: 7/21/2015
 * Time: 8:46 AM
 */

class Update {

    public function edit($table, $updates, $options)
    {

        $query = 'UPDATE `'. $table .'` SET ';
        $i = count($updates);
        $j = count($options);
        foreach ($updates as $key => $val) {
            $i--;
            $query .= "`" . $key . "` = '" . htmlspecialchars($val , ENT_QUOTES) . "'";
            if ($i != 0) {
                $query .= ' , ';
            }
        }

        if(count($options) > 0) {
            $query .= ' WHERE ';

            foreach ($options as $key => $val) {
                $j--;
                $query .= "`" . $key . "` = '" . htmlspecialchars($val , ENT_QUOTES) . "'";
                if ($j != 0) {
                    $query .= ' AND ';
                }
            }

        }
        //echo $query;
        $localhost = mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
        mysql_select_db(DB_DATABASE, $localhost);
        mysql_query($query, $localhost);
        mysql_close($localhost);

    }

    public function query($query,$return=false)
    {
        $localhost = mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
        mysql_select_db(DB_DATABASE,$localhost);
        if($return){
            return mysql_query($query,$localhost);
        }else{
            mysql_query($query,$localhost);
        }
        mysql_close($localhost);
    }

}

$update = new Update();