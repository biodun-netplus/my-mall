<?php
include('includes/header.php');
?>

<?php

if(isset($_POST['submit'])) {
    $product_id = $_POST['product_id'];
    $destination = "../product_images/";
    $message = '';

    $file1 = $_FILES['product_image1'];
    $fileTmpLoc1 = $file1['tmp_name'];
    if(!empty($file1['tmp_name'])) {
        $file_info1 = getimagesize($file1['tmp_name']);
        $uploadType1 = $file_info1[2];
        $filesize1 = $file1['size'];
        $product_image1 = $_FILES['product_image1']['name'];
        $product_image1 = time().'_'. preg_replace('#[^a-z.0-9]#i', '', $product_image1);
        $uploadLoc1 = $destination . $product_image1;
        $image_upload1 = move_uploaded_file($fileTmpLoc1, $uploadLoc1);
    }
    else {
        $product_image1 = $_POST['curr_product_image1'];
    }
    //details for image file2
    $file2 = $_FILES['product_image2'];
    $fileTmpLoc2 = $file2['tmp_name'];
    if(!empty($file2['tmp_name'])) {
        $file_info2 = getimagesize($file2['tmp_name']);
        $uploadType2 = $file_info2[2];
        $filesize2 = $file2['size'];
        $product_image2 = $file2['name'];
        $product_image2 = time().'_'.preg_replace('#[^a-z.0-9]#i', '', $product_image2);
        $uploadLoc2 = $destination . $product_image2;
        $image_upload2 = move_uploaded_file($fileTmpLoc2, $uploadLoc2);
        //$message .= $product_image2;
    } else {
        $product_image2 = $_POST['curr_product_image2'];
        //$message .= $product_image2;
    }
//get the status
    $stat = "SELECT * FROM products WHERE id=$product_id";
//    echo $stat;
$stat_check = mysql_query($stat) or die('jdjdjd'.mysql_error());
$stat_fetch = mysql_fetch_object($stat_check);
if($stat_fetch->isactive =='d')
{
    //update database table
    $update_product = mysql_query("UPDATE products SET image='{$product_image1}', image2='{$product_image2}',isactive='p'
     WHERE id=$product_id");
}else
{
    //update database table
    $update_product = mysql_query("UPDATE products SET image='{$product_image1}', image2='{$product_image2}'
     WHERE id='{$product_id}'");
}


    if($update_product) {
        $message = 'Product Image Updated Successfully';
        //header('location:'.SITE_URL.'products.php?image=success');
    }
    else {
        $message = 'Product Image was not updated.';
        //header('location:'.SITE_URL.'products.php?image=failed');
    }
}

?>

<div id="content">

    <?php include('includes/sidebar.php') ?>

    <div id="container">

        <h2 class="caps">Edit Product Image</h2>

        <?php
        if(isset($_GET['product_id'])) {
            $product_id = $_GET['product_id'];
            $q = mysql_query("SELECT * FROM products WHERE id=$product_id") or die(mysql_error());
            $product_info = mysql_fetch_object($q);
//            var_dump($product_info);
//            echo '../product_images/'.$product_info->image;
            if(!file_exists('../product_images/'.$product_info->image) || $product_info->image == '') {
                $product_image1 = SITE_URL.'banner_images/_no_image.png';
            }
            else {
                $product_image1 = '../product_images/'.$product_info->image;
            }

            if(!file_exists('../product_images/'.$product_info->image2) || $product_info->image2 == '' ) {
                $product_image2 = SITE_URL.'banner_images/_no_image.png';
            }
            else {
                $product_image2 = SITE_URL.'product_images/'.$product_info->image2;
            }

            ?>

            <div class="main">

                <?php
                if(isset($message) && !empty($message)) {
                    echo '<div class="notice success">'.$message.'</div>';
                }
                ?>
                <form action="<?php echo $_SERVER['PHP_SELF'] ?>?product_id=<?php echo $product_id ?>" method="post"
                      enctype="multipart/form-data">
                    <div class="form_process"></div>

                    <div class="content"></div>

                    <div class="content" id="form_success">
                        <div class="form_con float_lft">
                            <div class="form_itm filefield">
                                <label>Product Image 1</label>
                                <input type="file" name="product_image1" />
                                <div class="form_itm_error" id="product_image1_error"></div>
                            </div>

                            <input type="hidden" name="curr_product_image1"
                                   value="<?php echo $product_info->image; ?>" />

                            <div class="form_itm filefield">
                                <label>Product Image 2</label>
                                <input type="file" name="product_image2" />
                                <div class="form_itm_error" id="product_image2_error"></div>

                            </div>

                            <input type="hidden" name="curr_product_image2"
                                   value="<?php echo $product_info->image2; ?>" />

                            <input type="hidden" name="product_id" value="<?php echo $product_info->id; ?>" />

                            <div class="form_itm">
                                <input type="submit" name="submit" class="form_btn" value="Save" />

                                <div class="cancel_btn"><a href="products.php">Back</a></div>
                            </div>

                            <br class="clearfix" />
                        </div>

                        <div class="form_con float_rgt">
                            <div class="product_det_img">
                                <img src="<?php echo $product_image1 ?>" />
                                <img src="<?php echo $product_image2 ?>" />
                            </div>
                        </div>


                        <br class="clearfix" />
                    </div>
                </form>
            </div>

        <?php
        }

        else if (isset($message) && !empty($message)) {
            echo '<h5>'.$message.'</h5>';
        }
        ?>

    </div>

    <br class="clearfix" />

</div>

<?php
include('includes/footer.php')
?>


</body>
</html>