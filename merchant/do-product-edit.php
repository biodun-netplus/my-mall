<?php
require_once('../includes/classes/env.php');
require_once('../includes/classes/db_connect.php');
?>

<?php
if ($_SERVER['HTTP_HOST'] == 'localhost') {
    if (!isset($_SESSION['provider_id'])) {
        $provider_id = '1612';
        //header("location: " . SITE_URL . "/");
    } else {
        $provider_id = $_SESSION['provider_id'];
    }
}else {
    if (!isset($_SESSION['provider_id'])) {
        //$provider_id = '1612';
        header("location: " . SITE_URL . "/");
    } else {
        $provider_id = $_SESSION['provider_id'];
    }
}
?>

<?php
$destination = IMG_PATH;

if(isset($_POST['submit'])) {
    $category_id = $_POST['category_id'];
    $sub_cat_id = $_POST['sub_cat_id'];
    $product_name = $_POST['product_name'];
    $brand = $_POST['brand'];
    $list_price = $_POST['list_price'];
    $product_price = $_POST['product_price'];
    $product_detail = $_POST['product_detail'];

    if(isset($_POST['sub_sub_category'])){
        $sub_sub_cat_id = $_POST['sub_sub_category'];
    }
    else {
        $sub_sub_cat_id = null;
    }

    //details for image file 1
    if(empty($_FILES['product_image1'])){
        $product_image1 = $_POST['hidden_image1'];
    }
    else {
        $file1 = $_FILES['product_image1'];
        $fileTmpLoc1 = $file1['tmp_name'];
        $file_info1 = getimagesize($file1['tmp_name']);
        $uploadType1 = $file_info1[2];
        $filesize1 = $file1['size'];
        $product_image1 = $_FILES['product_image1']['name'];
        $product_image1 = preg_replace('#[^a-z.0-9]#i', '', $product_image1);
        $uploadLoc1 = $destination.$product_image1;
        if($uploadType1 == 1) {
            $ext = 'gif';
        }
        else if($uploadType1 == 2) {
            $ext = 'jpg';
        }
        else if($uploadType1 == 3) {
            $ext = 'png';
        }

        if ($uploadType1 != 1 && $uploadType1 != 2 && $uploadType1 != 3) {
            $message = 'wrong file for product_image 1';
        }
        else {
            if ($filesize1 > 1024000) {
                $message = 'The file size of product image 1 is larger than the allowable size';
            }
            else {
                $image_upload1 = move_uploaded_file($fileTmpLoc1, $uploadLoc1);
            }
        }
    }


    //details for image file2
    if(!isset($_FILES['product_image2']) || empty($_FILES['product_image2'])){
        $product_image2 = $_POST['hidden_image2'];
    }
    else {
        $file2 = $_FILES['product_image2'];
        $fileTmpLoc2 = $file2['tmp_name'];
        $file_info2 = getimagesize($file2['tmp_name']);
        $uploadType2 = $file_info2[2];
        $filesize2 = $file2['size'];
        $product_image2 = $_FILES['product_image2']['name'];
        $product_image2 = preg_replace('#[^a-z.0-9]#i', '', $product_image1);
        $uploadLoc2 = $destination.$product_image2;
        if($uploadType2 == 1) {
            $ext = 'gif';
        }
        else if($uploadType2 == 2) {
            $ext = 'jpg';
        }
        else if($uploadType2 == 3) {
            $ext = 'png';
        }

        if (($uploadType2 != 1 && $uploadType2 != 2 && $uploadType2 != 3) || ($filesize1 > 1024000)) {
            $message = 'The file can not be uploaded.';
        }
        else {
            $image_upload2 = move_uploaded_file($fileTmpLoc2, $uploadLoc2);
        }
    }

    //update database table
    $update_product = mysql_query("INSERT INTO products (product_name, pid, category_id, sub_cat_id, sub_sub_cat_id,
    product_price, list_price, product_detail, image, image2, brand)
    values('$product_name', '$provider_id', '$category_id', '$sub_cat_id', '$sub_sub_cat_id','$product_price',
    '$list_price', '$product_detail', '$product_image1', '$product_image2', '$brand')");

    if($update_product) {
        $message = 'Product Updated Successfully';
        header('location:'.SITE_URL.'products.php');
    }
}





