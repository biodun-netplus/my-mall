<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route["cart"]="cart/index";
$route["cart/reset-detail"]="cart/reset_customer_shipping_detail";
$route["cart/new-address"]="cart/add_customer_shipping_address";
$route["cart/customer-detail"]="cart/customer_shipping_detail";

$route["search"]="search/index";
$route["registration"]="registration/index";
$route["product/(:any)"]="product/detail/$1";

$route["home"]="home/index";
$route["v1"]="v1/merchant_login";

$route["profile"]="merchant/profile";
$route["edit-profile"]="merchant/edit_profile";
$route["transaction"]="merchant/transaction";
$route["change-password"]="merchant/change_password";

$route["category/ajax_product_by_subcate"]="category/ajax_product_by_subcate";
$route["category"]="category/index";
$route["category/(:any)"]="category/product_by_category/$1";



/** banner  **/

$route["add-banner"]="banner/add";
$route["banner"]="banner/index";
$route["banner/delete/(:any)"]="banner/delete/$1";


/** product **/
$route["product"]="product/index";
$route["add-product"]="product/add";
$route["edit-product/(:any)"]="product/edit/$1";
$route["delete-product/(:any)"]="product/delete/$1";

$route["product-colour/(:any)"]="product/product_colour/$1";
$route["add-product-colour/(:any)"]="product/add_colour/$1";
$route["edit-product-colour/(:any)"]="product/edit_colour/$1";
$route["delete-product-colour/(:any)"]="product/delete_colour/$1";

$route["product-size/(:any)"]="product/product_size/$1";
$route["add-product-size/(:any)"]="product/add_size/$1";
$route["edit-product-size/(:any)"]="product/edit_size/$1";
$route["delete-product-size/(:any)"]="product/delete_size/$1";

$route["relate-product-size-colour/(:any)"]="product/relate_product_size_colour/$1";
$route["add-product-size-colour/(:any)"]="product/add_product_size_colour/$1";
$route["edit-product-size-colour/(:any)"]="product/edit_product_size_colour/$1";
$route["delete-product-size-colour/(:any)"]="product/delete_product_size_colour/$1";
$route["update_product_qty"]="product/update_product_qty";


/** extra pages **/
$route["terms-of-use"]="miscellaneous/terms_of_use/";
$route["faqs"]="miscellaneous/faqs/";
$route["contact-us"]="miscellaneous/contact_us/";

/**  sellonline **/
$route["sell-online"]="sellonline/index";

/** merchant **/

$route["meet-our-merchants"]="merchant/meet_our_merchants/";
$route["description"]="merchant/merchant_description/";

$route["(:any)"]="merchant/detail/$1";

/** verification **/
$route[".well-known/pki-validation"] = "product/validate";




