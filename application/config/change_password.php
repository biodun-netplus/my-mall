<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('merchant-head');
?>

<section class="background-white">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header1">Change Password</h3> 
      </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
      <?php
		if(isset($msg) && $msg != '')
		{
		?>
      <div class="alert alert-success">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
        <strong><?php echo $msg;?></strong> </div>
      <?php
            }
            else if(isset($error_msg) && $error_msg != '')
            {
            ?>
      <div class="alert alert-danger">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
        <strong><?php echo $error_msg;?></strong> </div>
      <?php
            }
            ?>
    </div>
    </div>
    <div class="border">
      <form role="form" action="<?php echo base_url();?>change-password" id="change-password" method="post" enctype="multipart/form-data">
        <div class="col-lg-6">
          <div class="form-group">
            <label>Old Password</label>
            <input class="validate[required]" type="text" name="txtOldPassword" id="txtOldPassword">
          </div>
          <div class="form-group">
            <label>New Password</label>
            <input class="validate[required]" type="text" name="txtNewPassword" id="txtNewPassword">
          </div>
          <div class="form-group">
            <label>Confirm Password</label>
            <input class="validate[required,equals[password]]" type="text" name="txtConfirmPass" id="txtConfirmPass">
          </div>
        </div>
       <div class="col-lg-12">
          <button type="submit" class="btn btn-default" value="Submit" name="submit">Submit</button>
          <button type="reset" class="btn btn-default">Reset</button>
        </div>
      </form>
    </div>
  </div>
</section>
