<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('merchant-head');
?>

<section class="background-white">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header1">Updat Store Owners Profile</h3> 
      </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
      <?php
		if(isset($msg) && $msg != '')
		{
		?>
      <div class="alert alert-success">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
        <strong><?php echo $msg;?></strong> </div>
      <?php
            }
            else if(isset($error_msg) && $error_msg != '')
            {
            ?>
      <div class="alert alert-danger">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
        <strong><?php echo $error_msg;?></strong> </div>
      <?php
            }
            ?>
    </div>
    </div>
    <div class="border ">
      <form role="form" action="<?php echo base_url();?>description" id="description" method="post" enctype="multipart/form-data">
        <div class="col-lg-6">
          <div class="form-group" style=" margin-bottom: 33px;">
            <label>User Image</label>
            <?php
            if($merchantDetail->user_img != ''){
            ?>
                  <img src="<?php echo base_url()?>provider_images/<?php echo $merchantDetail->user_img;?>" height="100" /><br /><br />
                  <?php 
            }
            ?>
            <input class="validate[funcCall[validateImage[txtUserImg]]" type="file" name="txtUserImg" id="txtUserImg">
            <input type="hidden" name="txtOldUserImg" id="txtOldUserImg" value="<?php echo $merchantDetail->user_img;?>" />
          </div>
          <div class="form-group">
            <label>Detail</label>
            <textarea rows="5" class="form-control" name="txtDescription" id="txtDescription"><?php echo $merchantDetail->description;?></textarea>
          </div>
        </div>
       <div class="col-lg-12">
          <button type="submit" class="btn btn-default" value="Submit" name="submit">Submit</button>
          <button type="reset" class="btn btn-default">Reset</button>
        </div>
      </form>
    </div>
  </div>
</section>
