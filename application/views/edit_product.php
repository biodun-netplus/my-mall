<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('merchant-head'); 
?>

<section class="background-white">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header1"> Edit Product</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <?php
    		if(isset($msg) && $msg != '')
    		{
    		?>
        <div class="alert alert-success">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
          <strong><?php echo $msg;?></strong> </div>
        <?php
            }
            else if(isset($error_msg) && $error_msg != '')
            {
            ?>
        <div class="alert alert-danger">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
          <strong><?php echo $error_msg;?></strong> </div>
        <?php
            }
            ?>
      </div>
    </div>
    <div class="border ">
      <form role="form" action="<?php echo base_url();?>edit-product/<?php echo base64_encode($productdetail->id);?>" id="edit-product" method="post" enctype="multipart/form-data">
        <div class="col-lg-6">
          <div class="form-group">
            <label>Name</label>
            <input class="form-control validate[required]" type="text" name="txtName" id="txtName" value="<?php echo $productdetail->product_name;?>"  />
          </div>
          <div class="form-group" style=" margin-bottom: 33px;">
            <label>Image 1</label>
            <?php
            if($productdetail->image != '')
            {
            ?>
            <br><img src="<?php echo base_url().'product_images/'.$productdetail->image;?>" width="100"><br>
            <?php
            }
            ?>
            <input class="validate[<?php if($productdetail->image == ''){?>required,<?php } ?> funcCall[validateImage[txtImage1]]" type="file" name="txtImage1" id="txtImage1">
            <div class="help-block">The image must be png,jpg,jpeg,gif,bmp.</div>
            <input type="hidden" name="oldImage1" id="oldImage1" value="<?php echo $productdetail->image;?>">
          </div>
          <div class="form-group" style=" margin-bottom: 33px;">
            <label>Image 2</label>
            <?php
            if($productdetail->image2 != '')
            {
            ?>
            <br><img src="<?php echo base_url().'product_images/'.$productdetail->image2;?>" width="100"><br>
            <?php
            }
            ?>
            <input class="validate[funcCall[validateImage[txtImage2]]" type="file" name="txtImage2" id="txtImage2">
            <input type="hidden" name="oldImage2" id="oldImage2" value="<?php echo $productdetail->image2;?>">
            <div class="help-block">The image must be png,jpg,jpeg,gif,bmp.</div>
          </div>
          <div class="form-group" style=" margin-bottom: 33px;">
            <label>Image 3</label>
            <?php
            if($productdetail->image3 != '')
            {
            ?>
            <br><img src="<?php echo base_url().'product_images/'.$productdetail->image3;?>" width="100"><br>
            <?php
            }
            ?>
            <input class="validate[funcCall[validateImage[txtImage3]]" type="file" name="txtImage3" id="txtImage3">
            <input type="hidden" name="oldImage3" id="oldImage3" value="<?php echo $productdetail->image3;?>">
            <div class="help-block">The image must be png,jpg,jpeg,gif,bmp.</div>
          </div>
          <div class="form-group">
            <label>List Price</label>
            <input class="form-control validate[custom[number],funcCall[list_price_ge_than[txtOurPrice]]" type="text" name="txtListPrice" id="txtListPrice" value="<?php echo $productdetail->list_price;?>" />
            <div class="help-block">General Market Price. List price should be greater than cost price.</div>
          </div>

        </div>
        <div class="col-lg-6">
          <div class="form-group">
            <label>Our Price</label>
            <input class="form-control validate[required,custom[number],funcCall[cost_price_less_than[txtListPrice]]" type="text" name="txtOurPrice" id="txtOurPrice" value="<?php echo $productdetail->product_price;?>"  />
            <div class="help-block">Your Selling Price. Cost price should be less than list price.</div>
          </div>
         <?php /*
          <div class="form-group">
            <label>Quantity</label>
            <input class="form-control validate[required,custom[number]]" type="text" name="txtQuantity" id="txtQuantity" value="<?php echo $productdetail->product_quantity;?>"  />
          </div> */?>
          <div class="form-group">
            <label>Brand</label>
            <input class="form-control validate[required]" type="text" name="txtBrand" id="txtBrand" value="<?php echo $productdetail->brand;?>"  />
          </div>
          <div class="form-group">
            <label>Details</label>
            <textarea rows="8" class="form-control" name="txtDetails" id="txtDetails"><?php echo $productdetail->product_detail;?></textarea>
          </div>
          <div class="form-group">
            <label>Category</label>
            <select class="form-control validate[required]" name="selCategory" id="selCategory">
              <option value="">Category</option>
              <?php 
               if($cateDetail && $cateDetail != "")
               {
                 
                echo $cateDetail; 
               
               }
               ?>
              </select>
          </div>
          <?php /*
          <div class="form-group">
            <label>Sub Category</label>
            <select class="form-control validate[required]" name="txtSubCategory" id="txtSubCategory">
              <option value="">Sub Category</option>
            </select>
          </div> */?>
          <div class="form-group">
            <div> <span>
              <input type="checkbox" value="0" id="checkStockShipping" class="" name="checkStockShipping" <?php if($productdetail->stock_shipping == 0){?> checked="checked" <?php } ?> />
              </span>
              <label>In stock for shipping</label>
            </div>
          </div>
          <div class="form-group">
            <div> <span>
              <input type="checkbox" value="0" id="checkEligibleStock" class="" name="checkEligibleStock" <?php if($productdetail->stock_pickup == 0){?> checked="checked" <?php } ?> />
              </span>
              <label>Eligible for Stock Pickup</label>
            </div>
          </div>
        </div>
        <div class="col-lg-12">
          <button type="submit" class="btn btn-default" value="Submit" name="submit">Submit</button>
          <button type="reset" class="btn btn-default">Reset</button>
        </div>
      </form>
    </div>
  </div>
</section>
