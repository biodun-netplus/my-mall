<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
.onoffswitch-inner:before {
    content: "Approved" !important;
}
.onoffswitch-inner:after {
    content: "Unapproved" !important;
}
</style>
<!-- Page Heading -->
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">User List</h1>
  </div>
</div>
<!-- /.row -->
<div class="col-lg-10 message"></div>
<div class="row">
  <div class="col-lg-10">
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-striped">
        <thead>
          <tr>
            <th>DisplayName</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Address</th>
            <th>Logo</th>
            <th>Approved/Unapproved</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php 
		if(isset($records) && $records)
		{ 
			foreach($records as $rows)
			{
		?>
          <tr>
            <td><?php echo $rows->txtDisplayName; ?></td>
            <td><?php echo $rows->txtEmail; ?></td>
            <td><?php echo $rows->txtPhone; ?></td>
            <td><?php echo $rows->txtAddress; ?></td>
            <td>
            <?php if($rows->txtImage != '')
			{ ?>
             <img src="<?php echo base_url().'/assets/upload/logo/'.$rows->txtImage; ?>" width="50" />
             <?php 
			}
			?>
             </td>
            <td><div class="onoffswitch">
                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch_<?php echo $rows->id ;?>" <?php if($rows->userIsactive == 1){?>  checked <?php } ?>>
                <label class="onoffswitch-label" for="myonoffswitch_<?php echo $rows->userId;?>">
                <div class="onoffswitch-inner"></div>
                <div class="onoffswitch-switch"></div>
                </label>
              </div></td>
            <td>
            <a href="<?php echo base_url();?>admin/registration/edit/<?php echo base64_encode($rows->userId);?>" id=""><img src="<?php echo base_url().'/assets/img/edit.png'; ?>" width="24" /></a>  
            <a href="<?php echo base_url();?>admin/registration/delete/<?php echo base64_encode($rows->userId);?>" onclick="return confirm('Are you sure, you want to delete this record?');"><img src="<?php echo base_url().'/assets/img/delete.png'; ?>" width="24" /></a></td>
          </tr>
          <?php
			}
		}
		else
		{
		?>
          <tr>
            <td colspan="7"><div class="no-record">No record found!</div></td>
          </tr>
          <?php
		}
		?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<!-- /.row -->
<script type="text/javascript">

$(document).ready(function(){
	
	$('.onoffswitch-checkbox').click(function(){
		var clicked_id_string = $(this).attr('id');
		var clicked_id = clicked_id_string.split('_');
		  $.ajax({
				type: "POST",
		
				url: "<?php echo base_url(); ?>admin/registration/approved_unapproved",
		
				dataType: "html",
		
				data: "ids="+clicked_id[1],
		
				cache:false,
		
				success: 
				function(data){
					setTimeout(function(){$('.message').html(data)},10000);
				}

		  });
	});
	
});
</script>
