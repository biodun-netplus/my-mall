<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('merchant-head');
?>

<section class="background-white">
  <div class="container">
    <div class="border ">
      <!--<h3 class="title"><span class="glyphicon glyphicon-info-sign"></span> Information</h3>-->
      <div class="clearfix"></div>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="border-space-table">
        <tr>
          <td width="20%">Business Name</td>
          <td width="10%">:</td>
          <td><?php echo $merchantDetail->display_name; ?></td>
        </tr>
        <tr>
          <td>Email</td>
          <td>:</td>
          <td><?php echo $merchantDetail->address; ?></td>
        </tr>
        <tr>
          <td>Telephone</td>
          <td>:</td>
          <td><?php echo $merchantDetail->phone; ?></td>
        </tr>
        <tr>
          <td>Address:</td>
          <td>:</td>
          <td><?php echo $merchantDetail->email; ?></td>
        </tr>
        <tr>
          <td>Primary Category</td>
          <td>:</td>
          <td><?php echo $merchantDetail->category_name; ?></td>
        </tr>
      </table>
       </div>
      </div>
    </div>
  </div>
</section>
