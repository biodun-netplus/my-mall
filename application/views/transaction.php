<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('merchant-head'); 
?>

<section class="background-white">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header1">Transaction</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12"> 
        <!-- ---------- search ----------------------> 
      </div>
    </div>
    <div class="border1">
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th width="15%" align="left" valign="top">Transaction Id.</th>
              <th width="166" align="left" valign="top">User Details</th>
              <th width="227" align="left" valign="top">Product Details</th>
              <th width="8%" align="left" valign="top" style="text-align: left;">Totals</th>
              <th width="8%" align="left" valign="top" style="text-align: left;">Amount Due</th>
              <th align="left" width="15%" valign="top">Date of Transaction</th>
              <th align="left" width="7%" valign="top">Transaction Status</th>
            </tr>
          </thead>
          <tbody>
      <?php  
      
		  if($transaction && $transaction != '' )
		  {
			  foreach($transaction as $rows)
			  {

			 ?>
            <tr>
              <td><?php echo $rows->order_id ?></td>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><b>Email:</b> <?php echo $rows->email?></td>
                  </tr>
                  <tr>
                    <td><b>Phone:</b> <?php echo $rows->phone?></td>
                  </tr>
                  <tr>
                    <td><b>Address:</b> <?php echo $rows->delivery_addres?></td>
                  </tr>
                  <tr>
                    <td><b>Area:</b> <?php echo $rows->delivery_area?></td>
                  </tr>
                  <tr>
                    <td><b>Contact No:</b> <?php echo $rows->contact_no_delivery?></td>
                  </tr>
                </table></td>
              <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr>
                    <td width="50%"><b>Name</b></td>
                    <td width="10%"><b>Qty</b></td>
                    <td width="12%"><b>Price</b></td>
                    <td width="12%"><b>Total</b></td>
                  </tr>
              <?php //echo "<!---"; print_r($rows) ; echo "--->";
              $productColor  = $productSize = "";
              $productAmt    = 0;
              $cartDeatil = $this->cart_model->get_all_items_cart('p.pid = '.$_SESSION['userDetail']->id , $rows->order_id);
              $i = 0;
              foreach ($cartDeatil as $record) { 
                $i++;
                if($record->color != '' && $record->size != '' )
                  {

                    $productdetail     = $this->product_model->relate_product_size_colour(array('products.id'=>$record->product_id,'relation_color_size.colour_id'=>$record->color,'relation_color_size.size_id'=>$record->size));
                    $productColor  = $productSize = "";

                    //print_r($productdetail);
                    if($productdetail && $productdetail != '')
                    {
                    
                      foreach ($productdetail as $prodDeatil) 
                      {

                        if($prodDeatil->color && $prodDeatil->color != '')
                        {
                            $productColor   = $prodDeatil->color;

                        }
                        if($prodDeatil->size && $prodDeatil->size != '')
                        {
                            $productSize   = $prodDeatil->size;

                        }

                      }

                    }

                  }
                  else if($record->size != '' )
                  {

                    $productdetail     = $this->product_model->relate_product_size(array('relation_color_size.size_id'=>$record->size ,'products.id'=>$record->product_id )); 
                    //print_r($productdetail);
                    
                    if($productdetail && $productdetail != '')
                    {
                    
                      foreach ($productdetail as $prodDeatil) 
                      {

                       if($prodDeatil->size && $prodDeatil->size != '')
                        {
                            $productSize   = $prodDeatil->size;

                        }

                      }

                    }

                  }
                  else if($record->color  != '' )
                  {

                    $productdetail     = $this->product_model->relate_product_colour(array('relation_color_size.colour_id'=> $record->color,'products.id'=>$record->product_id )); 

                    if($productdetail && $productdetail != '')
                    {
                    
                      foreach ($productdetail as $prodDeatil) 
                      {

                        if($prodDeatil->color && $prodDeatil->color != '')
                        {
                            $productColor   = $prodDeatil->color;

                        }

                      }

                    }

                  }

              ?>
              
                  <tr>
                    <td width="60%"><b><?php echo $i; ?>.</b> <?php echo $record->product_name;?>
                    <div style="margin-left:10px;">
                    <?php 
                      if($productColor && $productColor != '')
                      {
                          echo "Color: ".$productColor;
                      }
                      if($productSize != '')
                      {
                          if($productColor != '')
                          echo "<br>";
                        
                          echo  "Size: ".$productSize;
                      }
                      $productAmt = ($record->price * $record->quantity) + $productAmt;
                      ?> 
                      </div>
                    </td>
                    <td width="10%"><?php echo $record->quantity; ?></td>
                    <td width="15%"><?php echo number_format($record->price); ?></td>
                    <td width="15%"><?php echo number_format(($record->price * $record->quantity)); ?></td>
                  </tr>
              <?php 
              }
              ?>
                <?php /*
                <tr>
                    <td width="15%" colspan="3"><b>Delivery charges:</b></td>
                    <td width="15%"><?php echo number_format($rows->delivery_amt); ?></td>
                </tr>
                */
                ?>

                </table></td>
              <td><?php //echo number_format($rows->total_ammount + $rows->delivery_amt);
              echo number_format($productAmt); ?></td>
              <td><?php 
              // $dueAmt = (($rows->total_ammount + $rows->delivery_amt)*4.5)/100;
              // echo number_format(($rows->total_ammount + $rows->delivery_amt)-$dueAmt); 
              $dueAmt = (($productAmt)*4.5)/100;
              echo number_format(($productAmt)-$dueAmt); 
              ?></td>
              <td><?php echo $rows->created; ?></td>
              <?php
              if($rows->status  == 'Pending')
              $status = '<span style="color:grey;">Pending</span>';
              if($rows->status  == 'Completed')
              $status = '<span style="color:green;">Completed</span>';
              if($rows->status  == 'Canceled')
              $status = '<span style="color:orange;">Canceled</span>';
              if($rows->status  == 'Failed')
              $status = '<span style="color:red;">Failed</span>';
              ?>
              <td><?php echo $status; ?></td>
            </tr>
            <?php
		 
			  }
		  }
		  else
		  {
		  ?>
            <tr>
              <td colspan="12"><div style="margin-bottom:10px" class="no-record text-center">No Record Found!</div></td>
            </tr>
            <?php
		  }
		  ?>
          </tbody>
        </table>
      </div>
    </div>
    <?php if($this->pagination->create_links() && $transaction) {?>
    <div><?php echo $this->pagination->create_links();?></div>
    <?php }?>
  </div>
</section>
