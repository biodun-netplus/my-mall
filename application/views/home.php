<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//var_dump($headerBanner);
?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/new/css/reset.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/new/css/style.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/new/css/responsive.css">
<script type="text/javascript" src="<?php echo base_url() ?>assets/new/js/jssor.slider.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<script>
    jssor_1_slider_init = function() {

        var jssor_1_SlideoTransitions = [
            [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
            [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
            [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
            [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
            [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
            [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
            [{b:10000,d:2000,x:-379,e:{x:7}}],
            [{b:10000,d:2000,x:-379,e:{x:7}}],
            [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
        ];

        var jssor_1_options = {
            $AutoPlay: true,
            $SlideDuration: 800,
            $SlideEasing: $Jease$.$OutQuint,
            $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
            },
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
            },
            $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        //responsive code begin
        //you can remove responsive code if you don't want the slider scales while window resizing
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 1920);
                jssor_1_slider.$ScaleWidth(refSize);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }
        ScaleSlider();
        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
        //responsive code end
    };
</script>
<!-- Header -->
<header>

<!--    <div id="banner" class="slider-wrapper theme-default">-->
        <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height: 350px; overflow: hidden; visibility: hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                <div style="position:absolute;display:block;background:url('<?php echo base_url() ?>assets/new/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
            </div>
            <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1300px; height: 350px; overflow: hidden;">
                <?php

                if($headerBanner && $headerBanner != ''){
                    foreach ($headerBanner as $row) {

                        $bannerImg    = base_url().'upload_home_slider_image/'.$row->path;
                        $bannerLink   = 'javaScript:;';
                        if($row->link != '')
                            $bannerLink   = $row->link;
                        ?>
                        <div data-p="225.00" style="display: none;">
                            <a href="<?php echo $bannerLink ;?>"> <img data-u="image" src="<?php echo $bannerImg;?>" /></a>
                        </div>


                        <?php
                    }
                }
                ?>


            </div>
            <!-- Bullet Navigator -->
            <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
                <!-- bullet navigator item prototype -->
                <div data-u="prototype" style="width:16px;height:16px;"></div>
            </div>
            <!-- Arrow Navigator -->
            <!-- <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
                       <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span> -->

        </div>
<!--       </div>-->

</header>
<div class="container">
<h2>Featured Merchant</h2>
<div class="disp1">
<div class="img">
    <?php
//    var_dump($featuredProduct);
    ?>
    <img  class="lazy" data-original="<?php echo base_url() ?>featured_merchant/<?php echo $featuredMerchant[0]->image;?>">
</div>

<div class="ptdis">
   <?php
      			if($featuredProduct && $featuredProduct  != '')
      			{
                      $count_div = 1;
                      $total_featured_products = count($featuredProduct);
//                      echo $total_featured_products;
                      $count_row =  1;
      				foreach($featuredProduct as $rows)
      				{
?>
                          <div class="product">
                              <a href="<?php echo base_url() ?>product/<?php echo base64_encode($rows->id)?>">
                              <img class="lazy" data-original="<?php echo base_url() ?>product_images/<?php echo $rows->image?>" >
                              <p><?php
                                  echo substr(ucfirst(strtolower($rows->product_name)),0,22);
                                  if(strlen($rows->product_name) > 22)
                                  {
                                      echo '...';
                                  }
                                      ?></p>
                              <div class="price">&#8358;<?php echo number_format($rows->product_price,0)?></div>
                              <div class="btn3"><p>Add To Cart</p></div></a>
                          </div><?php
                      }
                  }
                      ?>




</div>

<br class="clearfix">

<?php
$i = 0;
//    var_dump($category1);

foreach($category1 as $row)
{
    $this->load->model('home_model');
    $subcat = $this->home_model->get_prefered_categories($row->id);
//    var_dump($subcat);

        if($i%2== 0)
        {
            ?>
        <div class="wrap" style="float: left;">
            <div class="head">
                <h3 class="float_lft"><?php echo $row->category_name;?></h3>
                <p class="float_rgt"><a href="<?php echo base_url() ?>category/<?php echo base64_encode($row->id);?>">VIEW ALL</a></p>
            </div>

            <div class="info">
                <div class="imgtile" style="width: ">
                    <img class="float_lft lazy" style="padding: 10px;width:66%;height: 240px" data-original="<?php echo base_url() ?>category_images/<?php echo $row->banner?>">
                </div>
                <?php
            if($subcat){
                $x = 0;
                foreach($subcat as $subcat_row)
                {

                    $id = $subcat_row->sub_category_id;
                    $query = $this->home_model->get_category_name($id);

                    if($subcat_row->order_id==1)
                    {
//
                        ?>
                        <a href="<?php echo base_url() ?>category/<?php echo base64_encode($query[0]->id);?>">
                            <div class="tiles" style="padding: 10px; margin-top: 10px; width: 32%; float: left; text-align: center; border-bottom: none;">
                                <h3 style="margin: 0; padding: 0;"><?php echo $query[0]->category_name?></h3>
                                <p style="font-size: 11px; color: grey;">Top Brands</p>
                                <img class="lazy" data-original="<?php echo base_url() ?>prefered_category/<?php echo $subcat_row->image?>">
                            </div>
                        </a>
                <div style="padding: 10px; height: 230px;">
                <?php
                    }
                    if($subcat_row->order_id==2)
                    {
//
                        ?>
                        <a href="<?php echo base_url() ?>category/<?php echo base64_encode($query[0]->id);?>">
                            <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none;">
                                <h3 style="margin: 0; padding: 0;"><?php echo $query[0]->category_name?></h3>
                                <p style="font-size: 11px; color: grey;">Top Brands</p>
                                <img style="margin-top: 20px;" class="lazy" data-original="<?php echo base_url() ?>prefered_category/<?php echo $subcat_row->image?>">
                            </div>
                        </a>
                        <?php
                    }
                    if($subcat_row->order_id==3)
                    {
                        ?>
                        <a href="<?php echo base_url() ?>category/<?php echo base64_encode($query[0]->id);?>">
                            <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none;">
                                <h3 style="margin: 0; padding: 0;"><?php echo $query[0]->category_name?></h3>
                                <p style="font-size: 11px; color: grey;">Top Brands</p>
                                <img class="lazy" data-original="<?php echo base_url() ?>prefered_category/<?php echo $subcat_row->image?>">
                            </div>
                        </a>
                        <?php
                    }
                    if($subcat_row->order_id==4)
                    {
                        ?>
                        <a href="<?php echo base_url() ?>category/<?php echo base64_encode($query[0]->id);?>">
                            <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center;">
                                <h3 style="margin: 0; padding: 0;"><?php echo $query[0]->category_name?></h3>
                                <p style="font-size: 11px; color: grey;">Top Brands</p>
                                <img style="margin-top: 20px;" class="lazy" data-original="<?php echo base_url() ?>prefered_category/<?php echo $subcat_row->image?>">
                            </div>
                        </a>
                        <?php
                    }

                    $x++;
                }
            }?>






            </div>

            </div>

        </div>
            <?php
        }
        else{
            ?>
        <div class="wrap" style="float: right;">
            <div class="head">
                <h3 class="float_lft"><?php echo $row->category_name;?></h3>
                <p class="float_rgt"><a href="<?php echo base_url() ?>category/<?php echo base64_encode($row->id);?>">VIEW ALL</a></p>
            </div>

            <div class="info">
                <div class="imgtile">
                    <img class="float_lft lazy" style="padding: 10px;width:66%;height: 240px" data-original="<?php echo base_url() ?>category_images/<?php echo $row->banner?>">
                </div>
                <?php
            if($subcat){
                $x = 0;
                foreach($subcat as $subcat_row)
                {
                    $id = $subcat_row->sub_category_id;
//                 $id = $subcat_row->sub_category_id;
                    $query = $this->home_model->get_category_name($id);
                    if($subcat_row->order_id==1)
                    {
//                    echo $query->image;
                        ?>
                        <a href="<?php echo base_url() ?>category/<?php echo base64_encode($query[0]->id);?>">
                            <div class="tiles" style="padding: 10px; margin-top: 10px; width: 32%; float: left; text-align: center; border-bottom: none;">
                                <h3 style="margin: 0; padding: 0;"><?php echo $query[0]->category_name?></h3>
                                <p style="font-size: 11px; color: grey;">Top Brands</p>
                                <img class="lazy" data-original ="<?php echo base_url() ?>prefered_category/<?php echo $subcat_row->image?>">
                            </div>
                        </a>
                <div style="padding: 10px; height: 230px;">
                <?php
                    }
                    if($subcat_row->order_id==2)
                    {
                        ?>
                        <a href="<?php echo base_url() ?>category/<?php echo base64_encode($query[0]->id);?>">
                            <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none;">
                                <h3 style="margin: 0; padding: 0;"><?php echo $query[0]->category_name?></h3>
                                <p style="font-size: 11px; color: grey;">Top Brands</p>
                                <img style="margin-top: 20px;" class="lazy" data-original="<?php echo base_url() ?>prefered_category/<?php echo $subcat_row->image?>">
                            </div>
                        </a>
                        <?php
                    }
                    if($subcat_row->order_id==3)
                    {
                        ?>
                        <a href="<?php echo base_url() ?>category/<?php echo base64_encode($query[0]->id);?>">
                            <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none;">
                                <h3 style="margin: 0; padding: 0;"><?php echo $query[0]->category_name?></h3>
                                <p style="font-size: 11px; color: grey;">Top Brands</p>
                                <img class="lazy" data-original="<?php echo base_url() ?>prefered_category/<?php echo $subcat_row->image?>">
                            </div>
                        </a>
                        <?php
                    }
                    if($subcat_row->order_id==4)
                    {
                        ?>
                        <a href="<?php echo base_url() ?>category/<?php echo base64_encode($query[0]->id);?>">
                            <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center;">
                                <h3 style="margin: 0; padding: 0;"><?php echo $query[0]->category_name?></h3>
                                <p style="font-size: 11px; color: grey;">Top Brands</p>
                                <img style="margin-top: 20px;" class="lazy" data-original="<?php echo base_url() ?>prefered_category/<?php echo $subcat_row->image?>">
                            </div>
                        </a>
                        <?php
                    }

                    $x++;
                }
            }
                ?>
            </div>
            </div>

        </div>
            <?php
        }


//{
//    $get_no_of_sub = $this->home_model->get_no_of_sub($row->id);
//
//    if($get_no_of_sub > 3)
//        var_dump($get_no_of_sub);
    $i++;
}
?>







<br class="clearfix">

<div class="disp2">
    <div class="menu">
        <div class="head">
            <h3 class="float_lft">OTHER CATEGORY</h3>
        </div>
        <?php
//        var_dump($other_category);
//        echo $other_category[0]->id;
        $subcat_other = $this->home_model->get_sub_categories($other_category[0]->id);

        ?>
        <div class="menu_itm">
            <ul class="other_cat">
                <?php
                if($subcat_other)
                {


                foreach($subcat_other as $sub)
                {
                    ?>
                    <a href="<?php echo base_url() ?>category/<?php echo base64_encode($sub->id);?>"><li><?php echo $sub->category_name;?></li></a>
                    <?php
                }
                }
                ?>

            </ul>
        </div>

    </div>
    <?php






    $travel = $this->home_model->get_category_by_name('travel');
//    var_dump($travel);
    $service = $this->home_model->get_category_by_name('Service');
    $import = $this->home_model->get_category_by_name('Import ');
    $sport   = $this->home_model->get_category_by_name('Sports');
    $beauty = $this->home_model->get_category_by_name('Beauty ');
    ?>
    <a href="<?php echo base_url() ?>category/<?php echo base64_encode($travel[0]->id);?>"> <img style="margin-left: 3px; vertical-align: top; padding: 5px;" class="lazy" data-original="<?php echo base_url() ?>assets/new/img/o1.png"></a>
    <a href="<?php echo base_url() ?>category/<?php echo base64_encode($service[0]->id);?>"> <img style="vertical-align: top;  padding: 5px; border-right:1px solid #ddd;" class="lazy" data-original="<?php echo base_url() ?>assets/new/img/o2.png">
    <a href="<?php echo base_url() ?>category/<?php echo base64_encode($import[0]->id);?>"> <img style=" padding: 5px; " class="lazy" data-original="<?php echo base_url() ?>assets/new/img/o3.png">
    <a href="<?php echo base_url() ?>category/<?php echo base64_encode($sport[0]->id);?>"> <img style="width: 41%; margin-left: 3px; vertical-align: top;  padding: 5px;" class="lazy" data-original="<?php echo base_url() ?>assets/new/img/p1.png">
    <a href="<?php echo base_url() ?>category/<?php echo base64_encode($beauty[0]->id);?>"> <img style="width: 33%; float: right;  padding: 5px;" class="lazy" data-original="<?php echo base_url() ?>assets/new/img/p2.png">

</div>
</div>
</div>

<br class="clearfix">
<?php
//var_dump($merchantProfile);
?>
<div class="mercht">
    <div class="contn">
        <p style="color:#00587C;    font-family: daxecobank;font-size: 20px;font-weight: bold;">Meet our Merchants, hear their stories.</p>
        <p style="color:#000000;;font-family: daxecobank;font-size: 20px">Discover merchants and products with reviews from around the market place</p>
        <div class="box_wrap">
<?php
//        $merchantProfile1 = $this->home_model->all_merchant(array('isactive' => 't', 'image !=' => "") , "RANDOM", 4 );
//var_dump($merchantProfile1);
        foreach($merchantProfile as $our)
        {
//            var_dump($our);
            if(file_exists(base_url().'provider_images/'.$our->image))
                $merchantLogo = base_url().'provider_images/'.$our->image;
            else
                $merchantLogo = base_url().'assets/img/newmerchant.jpg';
         ?>
            <div class="box">
                <img class="lazy" data-original="<?php echo $merchantLogo;?>" style=" border:1px solid grey;">
                <p style="text-align: left;color: #000000; padding-top: 10px;margin-bottom: 0px;" id="dname;    "><b><?php echo $our->display_name?></b></p>
                <p style="text-align: left;"><i><?php
                    if($our->description != '')
                    {
                        if(strlen($our->description) > 50)
                        {

                            echo substr($our->description, 0,  50)."...." ;

                        }
                        else
                            echo $our->description;
                    }

                    ?></i></p>
            </div>
            <?php
        }
        ?>

        </div>

        <br class="clearfix">

        <div class="button" style="width: 35%">
            <div class="btn2" style="width: 200px;height: 50px;"><a href="<?php echo base_url().'meet-our-merchants';?>"><p style="font-size: 20px">See All Merchant</p></a></div>

            <div class="btn1" style="width: 200px;height: 50px;"><a href="<?php echo base_url().'registration';?>"><p style="font-size: 20px">Become a Merchant</p></a></div>
        </div>


    </div>

    <br class="clearfix">


</div>
<section class="tools">
    <div class="container">
        <div style="text-align: center">
            <p style="color:#00587C"><b>Banking Tools</b></p>
            <p>Connect with a like-minded business community to find knowledge, customers and opportunities to grow your
                business</p>
        </div>
        <div style="width: 90%; margin: 0px auto;">
            <div class="tool-box">
                <table width=100% style="height: 170px">
                    <tr style="vertical-align: top;">
                        <td style="padding-right: 10px;"><p style="color:#00587C"><b>Ecobank Cards</b></p>
                            <p>Ecobank Card account holders can operate their accounts through any of our globally
                                recognised cards</p></td>
                        <td><img class="lazy" data-original="<?php echo base_url()?>assets/img/cards.png"></td>
                    <tr>
                </table>
                <a href="http://www.ecobank.com/cards.aspx" target="_blank"><img v="<?php echo base_url()?>assets/img/more-button.png"></a> </div>
            <div class="tool-box">
                <table width=100% style="height: 170px">
                    <tr style="vertical-align: top;">
                        <td style="padding-right: 10px;"><p style="color:#00587C"><b>Ecobank Loans</b></p>
                            <p>Smile every mile... move ahead in life with an Ecobank Car or 'Moto'</p></td>
                        <td><img class="lazy" data-original="<?php echo base_url()?>assets/img/loans.png"></td>
                    <tr>
                </table>
                <a href="http://www.ecobank.com/loans.aspx" target="_blank"><img class="lazy" data-original="<?php echo base_url()?>assets/img/more-button.png"></a> </div>
            <div class="tool-box last">
                <table width=100% style="height: 170px">
                    <tr style="vertical-align: top;">
                        <td style="padding-right: 10px;"><p style="color:#00587C"><b>Online Account</b></p>
                            <p>Apply online for your Ecobank Acc</p></td>
                        <td><img class="lazy" data-original="<?php echo base_url()?>assets/img/online.png"></td>
                    </tr>
                </table>
                <a href="http://www.ecobank.com/currentaccounts.aspx" target="_blank"><img class="lazy" data-original="<?php echo base_url()?>assets/img/more-button.png"></a> </div>
        </div>
    </div>
</section>