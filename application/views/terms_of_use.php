<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="background-white">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header">Terms of use</h3>
        <div class="page-content">
        	<?php
        	if($pageDetail && !empty($pageDetail))
        	{
        		echo $pageDetail->detail;
        	}
        	?>
        </div>
      </div>
    </div>
  </div>
</section>
