<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('merchant-head');
?>

<section class="background-white">
  <div class="container">
   <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header1 col-lg-6">Product Size</h3>
        <div class="col-lg-6" style="text-align: right; vertical-align: bottom; margin-top: 2em;"><a href="<?php echo base_url();?>add-product-size/<?php echo base64_encode($productId);?>">Add Size</a>&nbsp;|&nbsp; <a href="<?php echo base_url();?>product-color/<?php echo base64_encode($productId);?>">Color</a> &nbsp;|&nbsp; <a href="<?php echo base_url();?>relate-product-size-colour/<?php echo base64_encode($productId);?>">Relate Colour & Size</a></div>
      </div>
    </div>

    <div class="border1 ">
   
         <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th width="" align="left" valign="top">Size</th>
              <th align="left">Action</th>
            </tr>
          </thead>
          <tbody> 

        <?php
        if($productSize && $productSize  != '')
        {
            
            foreach($productSize as $rows)
            {
                 
        ?>
            <tr>
              <td><?php echo $rows->size; ?></td>
              <td style="text-align:left;"><a href="<?php echo base_url()?>edit-product-size/<?php echo base64_encode($rows->id);?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo base_url()?>delete-product-size/<?php echo base64_encode($rows->id);?>" onclick="return confirm('Are you sure, you want to delete this record?')"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
               </td>
            </tr>
          <?php 
      }
    }
    else
      {
    ?>
      <tr>
           <td colspan="12"><div style="margin-bottom:10px" class="no-record text-center">No Record Found!</div></td>
        </tr>
    <?php
    }
    ?>

    </tbody>
        </table>
      </div>
       
    </div>
  </div>
</section>
