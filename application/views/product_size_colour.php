<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('merchant-head');
?>

<section class="background-white">
  <div class="container">
   <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header1 col-lg-10">Product Size And Colour</h3>
        <div class="col-lg-2" style="text-align: right; vertical-align: bottom; margin-top: 2em;"><a href="<?php echo base_url();?>add-product-size-colour/<?php echo base64_encode($productId);?>">Add Size And Colour</a></div>
      </div>
    </div>

    <div class="border1 ">
   
         <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th width="" align="left" valign="top">Size</th>
              <th width="" align="left" valign="top">Colour</th>
              <th width="" align="left" valign="top">Quantity</th>
              <th align="left">Action</th>
            </tr>
          </thead>
          <tbody> 

        <?php
        if($productSizeColour && $productSizeColour  != '')
        {
            
            foreach($productSizeColour as $rows)
            {
                 
        ?>
            <tr>
              <td>
               <?php $res = $this->product_model->get_single_product_size(array('product_size.id' => $rows->size_id)); 
                 if($res) echo $res->size;
               ?></td>
              <td><?php $res = $this->product_model->get_single_product_color(array('product_color.id' => $rows->colour_id));  if($res)  echo $res->color;
               ?></td>
              <td><?php echo $rows->qty; ?></td>
              <td style="text-align:left;"><a href="<?php echo base_url()?>edit-product-size-colour/<?php echo base64_encode($rows->id);?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="<?php echo base_url()?>delete-product-size-colour/<?php echo base64_encode($rows->id);?>" onclick="return confirm('Are you sure, you want to delete this record?')"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
               </td>
            </tr>
          <?php 
      }
    }
    else
      {
    ?>
      <tr>
           <td colspan="12"><div style="margin-bottom:10px" class="no-record text-center">No Record Found!</div></td>
        </tr>
    <?php
    }
    ?>

    </tbody>
        </table>
      </div>
       
    </div>
  </div>
</section>
