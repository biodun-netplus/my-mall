<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="merchant_display">
  <div class="container">
    <div class="display-div" style="height: auto;">
      <div class="merchant_box_content" style="height: auto; padding-bottom:5px;">
        <div style="color: #009AB1; font-size: 22px; margin-bottom: -12px">
            <?php if($merchantDetail) echo $merchantDetail->display_name ?>
        </div>
        <div class="item-box">
            <?php if($relatedProducts && $relatedProducts != '') echo count($relatedProducts); else echo '0';?> <br/>
          items
        </div>
      	<?php
      	$itemBoxWidth    = 60;
      	if($merchantDetail && $merchantDetail->display_name != "" && strlen($merchantDetail->display_name) > 17) {
      		
      		$newWidth 	= strlen($merchantDetail->display_name);
      		$itemBoxWidth 	= $itemBoxWidth - $newWidth;
      		if(strlen($merchantDetail->display_name) > 21 && strlen($merchantDetail->display_name) > 26)
      		$itemBoxWidth 	= $itemBoxWidth = 3;
      	} 
      	?>
        <div style="border-bottom:2px solid #009AB1; margin-bottom: 10px; width: <?php //echo $itemBoxWidth;?>13%; float: right;"></div>
        <div style="clear: both;"></div>
      </div>
      <div class="merchant_box_content" style="margin-bottom: 25px; font-size: 16px; height: auto;"> 
      <?php 
    if($merchantDetail && !empty($merchantDetail) && $merchantDetail->category_name && $merchantDetail->category_name != '')
    {
    ?>
      <a href="javaScript:;" style="padding-right: 10px"><?php if($merchantDetail) echo $merchantDetail->category_name?></a> 
      <?php
    }
    ?>
      </div>
      
      <div style="display:none;margin:0 auto;" class="html5gallery" data-skin="light" data-width="602" data-height="430" data-resizemode="fill">
      
      <?php
    if($merchantBanner && $merchantBanner != '')
    {
      $arr = explode(',',$merchantBanner->header_banner); 
      foreach($arr as $rows)
      {
        if(strpos($rows, 'youtube.com/') === false )
        {
        ?>
        <a href="<?php echo base_url().'banner_images/'.$rows ;?>"><img src="<?php echo base_url().'banner_images/'.$rows ;?>" width="100" height="100"></a>
              <?php
        }
        else
        {
          $youtube_thumb1 = explode('?v=',$rows);
          $youtube_thumbnail = explode('&',$youtube_thumb1[1]);
        ?>
              <a href="<?php echo $rows ;?>"><img src="http://img.youtube.com/vi/<?php echo $youtube_thumbnail[0];?>/2.jpg"></a>
              <?php
        }
   
      }
    }
    else
    {   
      ?>
      <a href="<?php echo base_url()?>assets/img/merchantIMG.png"><img src="<?php echo base_url()?>assets/img/merchantIMG.png" width="100" height="100"></a>
      <a href="<?php echo base_url()?>assets/img/merchantIMG.png"><img src="<?php echo base_url()?>assets/img/merchantIMG.png" width="100" height="100"></a>
      <a href="<?php echo base_url()?>assets/img/merchantIMG.png"><img src="<?php echo base_url()?>assets/img/merchantIMG.png" width="100" height="100"></a>
      <a href="<?php echo base_url()?>assets/img/merchantIMG.png"><img src="<?php echo base_url()?>assets/img/merchantIMG.png" width="100" height="100"></a>
      <a href="<?php echo base_url()?>assets/img/merchantIMG.png"><img src="<?php echo base_url()?>assets/img/merchantIMG.png" width="100" height="100"></a>
      <?php
    }
    ?>
      </div>

    </div>
    <div class="side-div">
      <div class="merchant_box_large">
     <?php
    $merchantImg = base_url().'assets/img/newmerchant.jpg'; 
    if($merchantDetail->image && $merchantDetail->image != '')
    {
      $merchantImg = base_url().'banner_images/'.$merchantDetail->image;
    }
    ?>
        <div class="merchant_box_circle_small" style="margin-left: 10px;background-image:url(<?php echo $merchantImg;?>);"></div>
        <div class="merchant_box" style="width: 210px;">
          <table width="100%" style="color: #000000; margin-left:7px; ">
            <tr>
              <td style="font-size: 20px" rowspan="2"><b><?php if($merchantDetail) echo ucwords($merchantDetail->first_name);?></b> <br/>
                Owner </td>
            </tr>
            <tr>
              <td style="height: 20px" rowspan="2"></td>
            </tr>
            <!--<tr>
              <td style="color:#8C8C8C;"> Ratings</td>
              <td><div class="star-rating"> <span class="fa fa-star-o" data-rating="1"></span> <span class="fa fa-star-o" data-rating="2"></span> <span class="fa fa-star-o" data-rating="3"></span> <span class="fa fa-star-o" data-rating="4"></span> <span class="fa fa-star-o" data-rating="5"></span>
                  <input type="hidden" name="whatever" class="rating-value" value="3">
                </div></td>
            </tr>-->
          </table>
        </div>
      </div>
      <div class="merchant_box_large" style="margin-bottom: 25px; height: auto; width: 100%;">
        <div style="color: #7DA22B; font-size: 20px; margin-bottom: -12px">Profile</div>
        <div style="border-bottom:1px solid #8C8C8C; margin-bottom: 10px; width: 85%; float: right;"></div>
        <div style="clear: both;"></div>
        <?php
        if($merchantDetail && $merchantDetail->description != '')
        {
          echo  $merchantDetail->description;
        }
        ?>
        </div>
      <div class="merchant_box_large" style="overflow-y: auto; height: auto !important;">
        <div style="font-size: 20px; margin-bottom: -15px">Contact Owner</div>
        <div style="border-bottom:1px solid #8C8C8C; margin-bottom: 10px; width: 68%; float: right;"></div>
        <form id="merchantContactus">
        <div style="clear: both; margin-bottom: 20px"></div>
        <input type="text" id="txtName" class="form-control contact-text" placeholder="Last Name, Other Names">
        <input type="text" id="txtEmail" class="form-control contact-text" placeholder="Email Address">
        <input type="text" id="numPnone" class="form-control contact-text" placeholder="Phone Number">
        <input type="text" id="txtComments" class="form-control contact-text" placeholder="Comments">
        <input type="checkbox" id="cheReceiveUpdates" value="1">
        <input type="hidden" id="numMerId" value="<?php echo base64_encode($merId);?>" >
        I want to receive updates on deals from this merchant
        <div style="height: 10px"></div>
        <button style="height: 50px" type="button" name="login" class="btn bg-contact btn-block">Submit</button>
      </form>
      </div>
    </div>
  </div>
</section>
<section class="product_display">
  <div class="container" style="padding-bottom: 10px">
    <div style="float: left; color: #009AB1; font-size: 25px;">Featured Products from this merchant</div>
  </div>
  <div class="container">
    <div class="related-product">
  <?php if($relatedProducts && $relatedProducts != '' && count($relatedProducts)>4 ){ ?>
<!--    <div class="customNavigation"> <a class="prev"><img src="--><?php //echo base_url()?><!--assets/img/greenarrow-left.png" style="cursor: pointer;padding-right: 6px;"></a> <a class="next"><img src="--><?php //echo base_url()?><!--assets/img/greenarrow-right.png" style="cursor: pointer;"></a> </div>-->
    <?php }
  elseif($relatedProducts == '')
    echo '<div class="no-record text-center">No Record Found!</div>';
  ?>

       <?php
        if($relatedProducts && $relatedProducts  != '')
        {
            $count_div = 1;
            $total_products = count($relatedProducts);
            $count_row =  1;
            foreach($relatedProducts as $rows)
            {
               $merchantLogo = '';
               if($rows->image != '')
               $productsImg = 'product_images/'.$rows->image;
               else
               $productsImg = 'assets/img/logo.png';

?>


                <div class="product-box"> <a href="<?php echo base_url().'product/'.base64_encode($rows->id);?>">
                  <div class="image"><img height="200" src="<?php echo base_url().$productsImg;?>"></div>
                  <div class="details">
                   <table width="100%">
                  <tr>
                  <td colspan="2" style="color: #ffffff;height: 42px;vertical-align: top;">
                  <?php
					if($rows->product_name != '')
					{
					  if(strlen($rows->product_name) > 36)
					  {

					    echo substr($rows->product_name, 0,  (32 - strlen($rows->product_name)))."...." ;

					  }
					  else
					  echo $rows->product_name;
					}
					?>
                  </td>
                  </tr>
                  <tr>
                  <td style="color: #A8C738; font-size: 22px; font-weight: 600;">&#x20A6;<?php echo number_format($rows->product_price); ?></td>
                  <td><input type="button" class="btn-block btn btn-cart" value="add to cart"></td>
                  </tr>
                  </table>
                  </div>
                  </a> </div>
                <?php
               // end of if...



               // end of else
              $count_div ++;
              $count_row ++;
            }
        }
   ?>

  </div>
  </div>
</section>
