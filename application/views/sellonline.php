<!DOCTYPE html>
<html lang="en"> 
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keyword" content="online mall, shopping, ecommerce, sales, website, sell, buy, marketing, delivery, logistics, Ecobank, MyMall, phones, fashion, electronics, home appliances, home decor, services, payments" >
        <meta name="description" content="MyMall Nigeria is an Ecobank Plc powered online mall for sellers and buyers.">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?php echo base_url()?>assets/img/favicon.png" type="image/x-icon" />
	<title>Your one-stop online mall for electronics, fashion, home appliances, phones, services and more | MyMall Nigeria</title>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/sell-online/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/sell-online/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/sell-online/style.css">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:600italic,400,800,700,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=BenchNine:300,400,700' rel='stylesheet' type='text/css'>
	<script src="<?php echo base_url();?>assets/js/sell-online/modernizr.js"></script>
	<!--[if lt IE 9]>
      <script src="assets/js/sell-online/html5shiv.js"></script>
      <script src="assets/js/sell-online/respond.min.js"></script>
    <![endif]-->

</head>
<body>
	
	<!-- ====================================================
	header section -->
	<header class="top-header">
		<div class="container">
		    
			<div class="row">
				<div class="col-xs-10 header-logo">
					<br>
					<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/sell-online/logo.jpg" alt="logo" class="img-responsive logo"></a>
				</div>

				<div class="col-xs-2">
			
					    <div class="navbar-header">
						  <div class="store">
					      <a href="<?php echo base_url();?>registration">Open a Store</a>
						 </div>
					    </div><!--end of navbar-header-->


			</div>
			</div>
		</div>
	</header> <!-- end of header area -->


	<!-- banner -->
	<section class="banner">
		<div class="container-fluid">
			<div class="row">
             
					<div class="banner-main clearfix">
						<div class="banner-img">
							 <a href="#"><img class="img-responsive" src="<?php echo base_url();?>assets/img/sell-online/item1.jpg" alt="Welcome to the marketplace that suits both your business and your audeience "></a>
						</div>
					</div><!--end of banner-main-->
				</div><!--end of row-->
				</div><!--end of container-fluid-->
				
				<!--middle menu-->
			
				
		<div class="container">
			<div class="row">
				<div class="what-we-offer">
					WHAT WE OFFER
				</div><!--end of what-we-offer-->
			</div><!--end of row-->
		 </div><!--end of container-->
				
			<header class="middle-header">
				<div class="container-fluid">
				  <div class="row">
				    
                  <nav class="navbar navbar-default">
					  <div class="container-fluid nav-bar-2">
					    <!-- Brand and toggle get grouped for better mobile display -->
					    <div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					    </div>

					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      
					      <ul class="nav navbar-nav">
					        <li><a class="menu active" href="#marketing" >Marketing</a></li>
					        <li><a class="menu" href="#payment">Payment</a></li>
					        <li><a class="menu" href="#logistics">Logistics</a></li>
					        <li><a class="menu" href="#webstore">Webstore</a></li>
					     </ul>
					    </div><!-- /navbar-collapse -->
					  </div><!-- / .container-fluid -->
					</nav>
				</div>
			</div>
		</div>
	</header> 				
	
	
	<section class="slider" id="marketing">
	<!--markerting section starts here-->			
			<div class="container">
			   <div class="row">	
				<div class="col-md-6">
					<div class="marketing">
						<div class="marketing-img">
							<img class="img-responsive" src="<?php echo base_url();?>assets/img/sell-online/marketing_img.jpg" alt="marketing">
						</div>
                           </div>
                               </div>


				<div class="col-md-6">
					<div class="marketing-txt">
						<h1>Marketing</h1>
						<p class="lead text">WJoin hundreds of sellers already doing business on MyMall and expose your products to millions of shoppers who can't wait to see what you have. Take advantage of our full suite of marketing tools tailored to take help promote your business</p>
						
						<div class="store-btn">
						<a href="<?php echo base_url();?>registration">Open your store</a>
						</div>
							</div><!--marketing-txt-->
							</div><!--end of col-md-6-->
					</div><!--end of row--> 
			</div>

<!--marketing section ends here-->

<!--payment section starts here-->	
<section class="service text-center" id="payment">		
		<div class="payment-container">	
			<div class="container">
			   <div class="row">	
				<div class="col-md-6">
					
					<div class="payment-txt">
						<h1 style="color:#fff;">Payment</h1>
						<p class="lead text">Selling on MyMall is affordable, transparent, and secure. Once an item sells, there is a 3.5% transaction fee with a settlement period of 7 working days. Ecobank offers best-in-class security of your payments and settlement with complementary accounting and other reconciliation tools managing your transactions is easy </p>
						
							<div class="store-btn-2">
						<a href="<?php echo base_url();?>registration">Open your store</a>
						</div>
							</div><!--marketing-txt-->
							</div><!--end of col-md-6-->
							
							
						<div class="payment-img">
							<img class="img-responsive" src="<?php echo base_url();?>assets/img/sell-online/payment.jpg" alt="marketing">
						</div>
                           </div>
                               </div>

</div>
					</div><!--end of row--> 
			</div>
	</div><!--end of payment-container-->
	
	<!--payment ssection ends here-->
	
	
	<!--Logistics section starts here-->	
	<section class="team" id="logistics">		
			<div class="container">
			   <div class="row">	
				<div class="col-md-6">
					<div class="logistics">
						<div class="logistics-img">
							<img class="img-responsive" src="<?php echo base_url();?>assets/img/sell-online/logistics.jpg" alt="marketing">
						</div>
                           </div>
                               </div>


				<div class="col-md-6">
					<div class="logistics-txt">
						<h1>Logistics</h1>
						<p class="lead text">MyMall provides in-transit insurance on pick-up and drop off services for all items purchased on the platform with 24hrs - 72hrs nationwide delivery </p>
						<div class="store-btn">
						<a href="<?php echo base_url();?>registration">Open your store</a>
						</div>
							</div><!--logistics-txt-->
							</div><!--end of col-md-6-->
					</div><!--end of row--> 
			</div>

<!--logistics section ends here-->

	
	<!--webstore section starts here-->	
	<div class="api-map" id="webstore">		
		<div class="webstore-container">	
			<div class="container">
			   <div class="row">	
				<div class="col-md-6">
					
					<div class="webstore">
						<h1>Webstore</h1>
						<p class="lead text">Manage your business anywhere on with our robust inventory management module. You spend less time managing your store. Get brand visibility when you open a store on MyMall. Our tools and services make it easy to manage, promote and grow your business – Open your store</p>
						
						<div class="store-btn">
						<a href="<?php echo base_url();?>registration">Open your store</a>
						</div>
						
							</div><!--marketing-txt-->
							</div><!--end of col-md-6-->
							
							
						<div class="webstore-img">
							<img class="img-responsive" src="<?php echo base_url();?>assets/img/sell-online/webstore.jpg" alt="marketing">
						</div>
                           </div>
                               </div>

</div>
					</div><!--end of row--> 
			</div>
	</div><!--end of webstore-container-->
	
	<!--webstore ssection ends here-->
	
	
	
<!--banner bottom section ends here-->
	
	
		<section class="banner-bottom">
		<div class="container">
			<div class="row">
             
					<div class="banner-bottom-main">
						<div class="banner-bottom-img">
							<img class="img-responsive" src="<?php echo base_url();?>assets/img/sell-online/banner-2.jpg" alt="">
						</div>
					</div><!--end of banner-bottom-main-->
				</div><!--end of row-->
				
				<h1 class="community text-center">JOIN MY MALL COMMUNITY</h1>
						
						
							<div class="row">	
							    
								<div class="col-md-4 shp">
					               <div class="shop">
						                <a href="<?php echo base_url();?>">SHOP</a>
							      </div>
								 </div>
								  
								  <div class="col-md-4 sell">
					               <div class="shop" style="background-color:#005B82;">
						                <a href="<?php echo base_url();?>registration">SELL</a>
							      </div>
								   </div>
								   
								     <div class="col-md-4 profile">
					               <div class="shop">
						                <a href="<?php echo base_url();?>meet-our-merchants">ALL MERCHANTS</a>
							      </div>
								   </div>
			
			</div>
				</div><!--end of container-->
		
	
<!-- Footer -->
<footer class="text-center">
  <div class="footer-above">
    <div class="container">
      <h1>Experience freedom to shop from several online businesses in Nigeria.</h1>
      <h2>Receive goods nationwide in as little as 24 hours. </h2>
      <br/>

        <form id="subscribe-from" action="#" name="subscribeFrom">
          <div class="subscribe-msg"></div>

          <div class="sub_form">
              <div class="sub_textfield">
                  <input type="email" id="email" name="email" class="form-control sub-text" placeholder="Subscribe to recieve updates on Mymall">
              </div>
              <div class="sub_btn">
                  <button style="height: 70px" type="button" name="subscribe" id="subscribe" class="btn bg-sub btn-block">Subscribe</button>
              </div>
          </div>
        </form>
    </div>
  </div>

  <div class="footer-below">
    <div class="container">
        <div class="social">
            <a href="https://www.facebook.com/MymallNg" target="_blank"><img src="<?php echo base_url()?>assets/img/facebook.jpg"></a>
            <a href=" https://twitter.com/MymallNg" target="_blank"><img src="<?php echo base_url()?>assets/img/twitter.jpg"></a>
            <a href=" https://plus.google.com/u/0/104426499827940803527" target="_blank"><img src="<?php echo base_url()?>assets/img/gplus.jpg"></a>
        </div>

        <div class="footer_links">
            <a href="<?php echo base_url()?>terms-of-use">Terms of use</a>&nbsp;|&nbsp;
            <a href="<?php echo base_url()?>faqs">FAQs</a>&nbsp;|&nbsp;
            <a href="<?php echo base_url()?>contact-us">Contact Us</a>&nbsp;|&nbsp;
            <a href="<?php echo base_url()?>registration">Registration</a>
            <span class="contact-no">Contact No:&nbsp;&nbsp;08024762146</span>
       </div>

        <div class="copyright">
            &copy; Mymall.com.ng <?php echo date('Y') ?>
        </div>

    </div>
  </div>

</footer> 

	<!-- script tags
	============================================================= -->
	<script src="<?php echo base_url();?>assets/js/sell-online/jquery-2.1.1.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/script.js"></script>
	<script src="https://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="<?php echo base_url();?>assets/js/sell-online/gmaps.js"></script>
	<script src="<?php echo base_url();?>assets/js/sell-online/smoothscroll.js"></script>
	<script src="<?php echo base_url();?>assets/js/sell-online/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/sell-online/custom.js"></script>
        
 
</body>
</html>