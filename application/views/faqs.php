<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="background-white">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header">FAQs ON MYMALL</h3>
        <div class="page-content margin-left">
          <b>1. What is MyMall?</b>
          <div>An online marketplace powered by Ecobank providing tools for trade to succeed online. Registered merchants get access to personalized web store, logistics support, secure payment collections and settlement, digital marketing solutions, and inventory management.</div>

          <b>2. How different is MyMall from other ecommerce platforms?</b>
          <div>MyMall offers the following unique value propositions to its merchants;
            <ol>
              <li>Dedicated merchant managers</li>
              <li>Dedicated marketing & sales focus for your web store</li>
              <li>Shorter settlement circle</li>
              <li>Lower subscription and commission fees</li>
              <li>Access to facility and other business advisory services as sponsored by Ecobank</li>
            </ol>
          </div>

          <b>3. Do I need to have an Ecobank account?</b>
          <div>For an initial registration, an Ecobank account is not required but to complete the registration and be activated on the site, an Ecobank account will be required.</div>

          <b>4. How can I register on MyMall?</b>

          <div>An initial registration can be done on www.mymall.com.ng or request for the 
          registration form by sending a mail to MYMALL@ecobank.com. Customers can also visit 
          the nearest branch to register</div>

          <b>5. Who handles my delivery? How long will it take and at what cost?</b>

          <div>FedEx handles delivery. It takes between 24hrs and 72hrs depending on the delivery location of the customer relative to your pick-up location. There is no cost to the merchant for delivery.</div>

          <b>6. How do I know when customer buys from my merchant’s store?</b>

          <div>The Merchant will receive a transaction notification on his profile</div>

          <b>7. How will I confirm if the agent that comes to pick up the item is truly from FedEx?</b>

          <div>The FEDEX official who must be in his official uniform must have a copy of the 
          transaction reference, and his ID </div>

          <b>8. How will I know when to package the goods?</b>

          <div>Once you receive an email transaction notification on your profile, with a follow up call by the Merchant managers</div>

          <b>9. What are the terms for refunds, and returns and when will I get my money?</b>

          <div><u>Refunds</u> – Approved refunds will be processed within 14 working days <br/>

          <u>Returns</u> – Approved returns will be initiated by WebMall and FedEx within 7 days from the date of pick up from the customer</div>

          <b>10. What is a Merchant Manager?</b>

          <div>MYMALL administrator </div>

          <b>11. When will I get paid for goods sold on my store?</b>

          <div>Provided there is no dispute on your products following delivery, goods will be paid for within 4 days from the date of pick up.</div>

          <b>12. A merchant cannot upload a picture, what happened?</b> 

          <div>The uploaded image does not meet the minimum criteria for display to customers on 

          the website</div>

          <b>13. Is there a limit to the number of products I can upload? </b>

          <div>There is no limit. </div>
        </div>
      </div>
    </div>
  </div>
</section>