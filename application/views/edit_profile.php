<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('merchant-head');
?>

<section class="background-white">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header1"> Update Profile</h3>
      </div>
    </div>
    <div class="row">
    <div class="col-lg-12">
      <?php
		if(isset($msg) && $msg != '')
		{
		?>
      <div class="alert alert-success">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
        <strong><?php echo $msg;?></strong> </div>
      <?php
    }
    else if(isset($error_msg) && $error_msg != '')
    {
    ?>
      <div class="alert alert-danger">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
        <strong><?php echo $error_msg;?></strong> </div>
      <?php
    }
    ?>
    </div>
    </div>
    <div class="border ">
      <form role="form" action="<?php echo base_url();?>edit-profile" id="edit-profile" method="post" enctype="multipart/form-data">
        <div class="col-lg-6">
          <div class="form-group">
            <label>Name</label>
            <input class="form-control validate[required,ajax[ajaxUserCall]]" type="text" name="first_name" id="first_name" value="<?php echo $merchantDetail->first_name;?>"  />
          </div>
          <div class="form-group">
            <label>Full Name</label>
            <input class="form-control validate[required]" type="text" name="txtFullName" id="txtFullName" value="<?php echo $merchantDetail->full_name;?>" />
          </div>
          <div class="form-group">
            <label>Email</label>
            <input class="form-control validate[required,custom[email],ajax[ajaxUserCall]]" type="text" name="email" id="email" value="<?php echo $merchantDetail->email;?>"  />
          </div>
          <?php /*
          <div class="form-group">
            <label>Password</label>
            <input class="form-control" type="password" name="txtPassword" id="txtPassword">
          </div>  */ ?>
          <div class="form-group">
            <label>Phone</label>
            <input class="form-control validate[required,funcCall[customPhone[txtPhone]]" type="text" name="txtPhone" id="txtPhone" value="<?php echo $merchantDetail->phone;?>"  />
          </div>
          <div class="form-group">
            <label>Address</label>
            <textarea rows="7" class="form-control validate[required]" name="txtAddress" id="txtAddress"><?php echo $merchantDetail->address;?></textarea>
          </div>
          <?php /*
          <div class="form-group">
            <label>Country</label>
            <select class="form-control validate[required]" name="txtCountry" id="txtCountry">
              <option value="">Country</option>
              <?php
              if($countries)
              {
                foreach($countries as $rows)
                {
                  
              ?>
                    <option value="<?php echo $rows->id; ?>" <?php if($merchantDetail->country == $rows->id) { ?> selected="selected" <?php } ?> ><?php echo $rows->name; ?></option>
                    <?php
                }
              }
              ?>
            </select>
          </div>*/?>
        </div> 
        <div class="col-lg-6">
          <div class="form-group">
            <label>State</label>
            <select class="form-control validate[required]" name="txtState" id="txtState">
              <option value="">State</option>
              <?php
      				if($states)
      				{
      					foreach($states as $row)
      					{
      						
      				?>
                    <option value="<?php echo $row->state_id; ?>" <?php if($merchantDetail->state == $row->state_id) { ?> selected="selected" <?php } ?> ><?php echo $row->name; ?></option>
                    <?php
      					}
      				}
      				?>
            </select>
          </div>
          <div class="form-group">
            <label>City</label>
            <input class="form-control validate[required]" type="text" name="txtCity" id="txtCity" value="<?php echo $merchantDetail->city;?>"  />
          </div>
          <?php /*
          <div class="form-group">
            <label>Zip</label>
            <input class="form-control validate[required]" type="text" name="txtZip" id="txtZip" value="<?php echo $merchantDetail->zip;?>"  />
          </div>
           */ ?>
          <div class="form-group">
            <label>Category</label>
            <select class="form-control validate[required]" name="txtCategory" id="txtCategory">
              <option value="">Category</option>
              <?php
        			 if($categories)
        			 {
        				foreach($categories as $row)
        				{
        					
        			 ?>
                      <option value="<?php echo $row->id; ?>" <?php if($merchantDetail->category == $row->id) { ?> selected="selected" <?php } ?> ><?php echo $row->category_name; ?></option>
                      <?php
        				}
        			 }
        			 ?>
            </select>
          </div>
          <div class="form-group" style=" margin-bottom: 33px;">
            <label>Logo</label>
            <?php
            if($merchantDetail->image != ''){
            ?>
              <img src="<?php echo base_url()?>banner_images/<?php echo $merchantDetail->image;?>" height="100" /><br /><br />
            <?php 
            }
            ?>
            <input class="validate[funcCall[validateImage[txtImage]]" type="file" name="txtImage" id="txtImage">
            <input type="hidden" name="txtOldImage" id="txtOldImage" value="<?php echo $merchantDetail->image;?>" />
          </div>
          <?php /*
          <div class="form-group" style=" margin-bottom: 33px;">
            <label>User Image</label>
            <?php
            if($merchantDetail->user_img != ''){
            ?>
                  <img src="<?php echo base_url()?>provider_images/<?php echo $merchantDetail->user_img;?>" height="100" /><br /><br />
                  <?php 
            }
            ?>
            <input class="validate[funcCall[validateImage[txtUserImg]]" type="file" name="txtUserImg" id="txtUserImg">
            <input type="hidden" name="txtOldUserImg" id="txtOldUserImg" value="<?php echo $merchantDetail->user_img;?>" />
          </div>
          */?>
          <div class="form-group">
            <label>Deliveryarea</label>
            <select class="form-control" name="txtDeliveryarea" id="txtDeliveryarea">
              <option value="">Deliveryarea</option>
              <?php
        			 if($states_merchant)
        			 {
        				foreach($states_merchant as $row)
        				{
        					
        			 ?>
                      <option value="<?php echo $row->name; ?>" <?php if($merchantDetail->deliveryarea == $row->name) { ?> selected="selected" <?php } ?> ><?php echo $row->name; ?></option>
                      <?php
        				}
        			 }
        			 ?>
            </select>
          </div>
          <div class="form-group">
            <label>Do you have an Ecobank account?</label>
            <div> <span>
              <input type="radio" value="Yes" id="Yes" class="bankaccounselect validate[required]" name="bankaccounselect" <?php if($merchantDetail->account_type != 'No') { ?>  checked="checked" <?php } ?> />
              Yes</span> <span>
              <input type="radio" value="No" id="No" class="bankaccounselect validate[required]" name="bankaccounselect" <?php if($merchantDetail->account_type == 'No') { ?>  checked="checked" <?php } ?> />
              No</span> </div>
            <p <?php if($merchantDetail->account_type == 'No') { ?>  style="display:none;" <?php } ?>  id="BankShow">
              <input class="form-control validate[required]" type="text" name="txtAccountType" id="txtAccountType" value="<?php echo $merchantDetail->account_type; ?>" />
            </p>
          </div>
        </div>
        <div class="col-lg-12">
          <button type="submit" class="btn-submit btn btn-default validate[funcCall[submitEditProfileForm[edit-profile]]" value="Submit" name="submit">Submit</button>
          <button type="reset" class="btn btn-default">Reset</button>
        </div>
      </form>
    </div>
  </div>
</section>
