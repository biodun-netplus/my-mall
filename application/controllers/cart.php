<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->load->model('product_model');
		$this->load->model('cart_model');
		$this->load->model('order_model');
		$this->load->model('user_model');
		
	}
	
	public function index(){

		
		
		$data   					= array();
		$userId						= '';
		$orderId					= '';	
		$data['UserDetail'] 		= '';
		//$data['deliveryAmount'] 	= 0;
		$totalAmt					= 0;
		$data['shippingAdd'] 		= '';
		
		if(isset($_SESSION['cart'])  && $_SESSION['cart'])
		{
			if(!isset($_SESSION['Check'])) 
			{ 
				 $_SESSION['Check'] = "55".$this->generateOrder(10);
				 $orderId       	= $_SESSION['Check'];
			}
			else
			{
				 //$_SESSION['Check'] = "55".$this->generateOrder(10);
				 $orderId 			= $_SESSION['Check'];
			}

			/*$_SESSION['Check'] 	= "55".$this->generateOrder(10);
			$orderId 			= $_SESSION['Check'];*/
			
			if(isset($_SESSION['userDetail']) && !empty($_SESSION['userDetail'])){
				$userId  = $_SESSION['userDetail']->id;
			}
			
			/*** update cart and order status ***/
			if($this->cart_model->update_cart($userId, $orderId))
			{
			
				$cartDetail			= $this->cart_model->get_all_items_cart('', $orderId);
				$data['cartDetail']	= $cartDetail;
				
				
				// manage order detail start....
				$lastInsertedOrderId 	= '';
				if(isset($_SESSION['lastInsertedOrderId']))
				{
					$lastInsertedOrderId	= $_SESSION['lastInsertedOrderId'];
				}
				
				//$checkOrder = $this->order_model->single_order_detail(array('order_id'=>$orderId));
				$checkOrder = $this->order_model->single_order_detail(array('id' => $lastInsertedOrderId));



				// get total amt of cart
				 if($cartDetail  && $cartDetail != '')
				 {
					 foreach($cartDetail as $rows)
					 {
						 $totalAmt   = ($rows->price * $rows->quantity) + $totalAmt;
					 }
				 }
				
				
				$detail['buyer_id']					= $userId;
				$detail['total_ammount']			= $totalAmt;
				$detail['created']					= date('Y-m-d h:m:s');
				$detail['status']					= 'Pending';
				$detail['order_id']					= $orderId;
				
				
				if($checkOrder)
				{
					// update order detail
					$this->order_model->update_order($detail,array('id' => $lastInsertedOrderId));
				}
				else
				{
					// insert row in order
					$_SESSION['lastInsertedOrderId'] = $this->order_model->insert_order($detail);	
					
				}
				
			}
			// end of order detail
			

			/***   get shipping address detail ****/
			
			///print_r($_COOKIE); echo "zssss".$this->input->cookie('customerShippingEmail',TRUE);


			if($this->input->cookie('customerShippingEmail') )
			{
				$data['shippingAdd'] = $this->user_model->get_shipping_address(array('email'=>$this->input->cookie('customerShippingEmail',TRUE),'display'=>0));
			}
			
			/**** search shipping address by email ****/
			if($this->input->post('txtEmail') && $this->input->post('txtEmail') != '')
			{
				
				
						
				$shippingAdd 			= $this->user_model->get_shipping_address(array('email'=>$_POST['txtEmail'],'display'=>0));
				$data['shippingAdd'] 	= $shippingAdd; 

						// var_dump($data['shippingAdd']);
				if($shippingAdd)
				{
					
					if(count($shippingAdd) == 1) 
					{
						

						setcookie('customerShippingEmail', $shippingAdd->email ,time() + 60 * 60 * 24 * 30 , '/');
						setcookie('customerShippingPhone', $shippingAdd->phone ,time() + 60 * 60 * 24 * 30 , '/');
						
					}
					else
					{
						$countRow = 0;
						foreach($shippingAdd as $row){
							
							//setcookie('customerShippingEmail', $row->email ,time() + 60 * 60 * 24 * 30 , '/');
							if($countRow == 0)
							{
								
								setcookie('customerShippingEmail', $row->email ,time() + 60 * 60 * 24 * 30 , '/');
								setcookie('customerShippingPhone', $row->phone ,time() + 60 * 60 * 24 * 30 , '/');
															
								$countRow ++;
							}
							else 
							continue;
							
							
						}

						

					}
						header("Location: cart");
						die();
					
				}
				
				
			}
			$data['ordersDetail'] 	 = $this->order_model->single_order_detail(array('order_id'=>$orderId));
			$data['states_merchant'] = $this->common_model->get_merchant_state();
			

			$data['couriers'] =  $this->user_model->get_all_courier();
			
			
			$this->layout->css(base_url().'assets/css/validationEngine.jquery.css');
			$this->layout->js(base_url().'assets/js/jquery.validationEngine-en.js');
			$this->layout->js(base_url().'assets/js/jquery.validationEngine.js');
			
			$this->layout->script("<script type='text/javascript'>  $('#customer-detail').validationEngine({ promptPosition : 'topLeft',}); </script>");

			$this->layout->view('cart',$data);	

		}
		else{
			header("Location: home");
			die();
		}
		
	}
	
	public function generateOrder($length)
	{
		$code = "";
		$code = date('Ymdhis');
		return $code;
	}

	public function get_courier_name()
	{
		$courier_name =  $this->input->post('courier');
		$_SESSION['courier'] = $courier_name;
		return $courier_name;
		
	}

	public function get_delivery_lga()
	{

		$state = $this->input->post('state');
		$lga = $this->cart_model->get_lga($state);
	}
	

	public function update_total_amount(){
		$total_amount = $this->input->post('total_amount');
		$total_amount = preg_replace("/[^0-9,.]/", "", $total_amount);
		$total_amount = (int)str_replace(',', '', $total_amount);
		$order_id = $_SESSION['Check'];
		
		$this->cart_model->new_total_amount($total_amount, $order_id);
	}
	
	public function shipping_method_price()
	{	
		
		$courier = $this->user_model->get_courier($_SESSION['courier']);
		$courier_id = $courier->courier_code;


		if(isset($_SESSION['Check']))
		{
			$order_id = $_SESSION['Check'];
			$cart = $_SESSION['cart'];
			$max = count($cart);

			$quantity = $this->cart_model->get_cart_item($order_id);
			$counter = count($quantity);
			$qty = 0;
			for($i=0; $i<$counter; $i++)
			{
				
				$qty = $qty + (int)$quantity[$i]->quantity;
				
			}
			
			$weight = 0.50;
			for($i=0; $i<$max; $i++){
				
				if(!isset($qty) && $qty == ''){
					$qty = 1;
				}

				$data = $_SESSION['cart'][$i]['productid'];
			

				$proDetail =  $this->product_model->single_product_detail(array('products.id' => $data));
				if($proDetail->weight > 0){
					$qty = (int)$cart[$i]['qty'];
					$hasweight += $proDetail->weight * $qty;
				}else{
					$qty = (int)$cart[$i]['qty'];
					$noweight += $weight * $qty;	
				}

			}
			$weight = $hasweight + $noweight;



			$this->cart_model->store_item_weight($order_id, $weight);		

			$delivery = $this->order_model->get_delivery_location($order_id);
			
			$pickup_lga = $this->order_model->get_pickup_lga($delivery[0]->pickup_lga);

			$pickup_state = $this->order_model->get_pickup_state($delivery[0]->pickup_state);
			$delivery_state = $delivery[0]->delivery_state;
			$delivery_lga = $delivery[0]->delivery_lga;

			$url = 'https://new.saddleng.com/api/v2/shipping_price';
			//old token
			//$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MTMzNTQ0MDMsImV4cCI6MTgyODg4NzIwMywianRpIjoiNHdqUGNmQXlMaTM4N2xDVnFsT0k4NCIsInN1YiI6InZpbmNlOjI1MSJ9.SHrUGoiv0Mk2O9z5AGf7Gs2uHkqQT8ZmWaPENR6evUI";
			
			//test token 
			$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MzM3MjAwMzcsImV4cCI6MTg0OTMzOTIzNywianRpIjoiZFhjTzgzZ2pqSUs3TFZkYXJsbklwIiwic3ViIjoiZGFwb0E6NDc2In0.eWLZYV3pboKBUvuue7hr68752jkJzM7DRsr8UHcMuzc";
			
			$body = json_encode(array('delivery_state' => $delivery_state, 'pickup_state' => $pickup_state, 'pickup_lga' => $pickup_lga, 'delivery_lga'=> $delivery_lga, 'weight' => $weight, 'courier_id' => $courier_id));
			$header = array('Content-Type: application/json', 
			'Authorization: Bearer '.$token);

	
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			$price = curl_exec($ch);


			$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			curl_close($ch);
			if ($httpcode == 200 && $price > 0) {
				$shippingPrice = $price;
				
				$courier_id  = $this->order_model->get_courier_id($courier_id);
				$this->order_model->update_order(array('delivery_amt' =>$shippingPrice, 'courier_id' => $courier_id), array('order_id' => $order_id));
			
			} else if($httpcode == 404) {
				$shippingPrice = 0;
				
			}else{
				return false;
			}
			echo json_encode(['shipping_price'=>$shippingPrice]);

		}
		

	}

	public function ajax_update_shipping($id){
		
		$id									= base64_decode($id);
		$_SESSION['customerShippingId']		= $id;
		
		$shippingDetail = $this->user_model->single_shipping_address(array('id' => $id), $start = 0, $limit = '');
		$delivery_state = $shippingDetail->delivery_state;
	
		
		if($shippingDetail)
		{
			if(isset($_SESSION['Check']))
			{
				$orderId = $_SESSION['Check'];
				$orderAmt = $this->order_model->single_order_detail(array('order_id' => $orderId));
				$updateDetail['total_ammount']				= $orderAmt->total_ammount;
				$updateDetail['delivery_amt']				= $deliveryAmount;
				$updateDetail['shipping_add_id']			= $id;
				
				$this->order_model->update_order($updateDetail ,array('order_id' => $orderId));
			}

			//echo number_format($deliveryAmount, 2).'::'.number_format(($orderAmt->total_ammount + $deliveryAmount), 2).'::'.'<div style="border-bottom: 1px solid rgb(140, 140, 140); margin-bottom: 10px; margin-top: 10px; width: 100%;"></div><button style="height: 30px; padding: 2px 18px; display: block;" type="button" class="btn bg-orange btn-block proceed-to-checkout">PROCEED TO CHECKOUT</button>'.'::'.number_format(($orderAmt->total_ammount + $deliveryAmount), 2).'::'.$delivery_area;
			
		}
	}
	

	
	
	public function reset_customer_shipping_detail(){
		
		if(isset($_COOKIE['customerShippingEmail']))
		{
			//unset($_SESSION['customerShippingEmail']);
			//setcookie ("customerShippingEmail", "", time() - 3600);
			delete_cookie("customerShippingEmail");
		}

		
		if(isset($_COOKIE['customerShippingPhone']))
		{
			//unset($_SESSION['customerShippingPhone']);
			//setcookie ("customerShippingPhone", "", time() - 3600);
			delete_cookie("customerShippingPhone");

		}
		
		header("Location: ".base_url()."cart");
		die();
	}
	
	/***** add shipping detail with existing email and phone no.*****/
	public function add_customer_shipping_address()
	{
		
		//print_r($_COOKIE);
		//echo $this->input->cookie('customerShippingEmail');

		if($this->input->post('submit') && $this->input->post('submit') == 'Add Address')
		{
			
			//$data['email']					= $_SESSION['customerShippingEmail'];
			//$data['phone']					= $_SESSION['customerShippingPhone'];
			$data['email']					= $_COOKIE['customerShippingEmail'];
			$data['phone']					= $_COOKIE['customerShippingPhone'];
			$data['delivery_contact_name']	= $this->input->post('txtDeliveryContactName');
			$data['delivery_address']		= $this->input->post('txtDeliveryAddress');
			$data['delivery_state']			= $this->input->post('selectDeliveryarea');
			$data['delivery_lga']			= $this->input->post('selectDeliverylga');
			$data['contact_no_delivery']	= $this->input->post('txtDeliveryContactNo');

				
			$shippingDetail = $this->user_model->single_shipping_address($data, $start = 0, $limit = 1);
			
			
			if(!$shippingDetail)    /***** get existing detail ******/
			{
				$this->user_model->insert_shipping_address($data);
			
				
			}
			else
			{
				$_SESSION['customerShippingAddError'] = 'Shipping address already exist! Please add diffrent';
			}
		}
		header("Location: ".base_url()."cart");
		die();
	}
	
	
	
	public function customer_shipping_detail()
	{
		
		if($this->input->post('submit') && $this->input->post('submit') != '')
		{
			$email 			= $this->input->post('txtEmail');
			$phone			= $this->input->post('txtPhone');
			$shippingDetail = $this->user_model->single_shipping_address(array('email' => $email ), $start = 0, $limit = 1);
			

			if($shippingDetail)/***** get existing detail ******/
			{
				
				if($shippingDetail->email)
				$email 	= $shippingDetail->email;	

				if($shippingDetail->phone)
				$phone 	= $shippingDetail->phone;	
			
				$data['email']					= $email;
				$data['phone']					= $phone;
				$data['delivery_contact_name']	= $this->input->post('txtDeliveryContactName');
				$data['delivery_address']		= $this->input->post('txtDeliveryAddress');
				$data['delivery_state']			= $this->input->post('selectDeliveryarea');
				$data['delivery_lga']			= $this->input->post('selectDeliverylga');
				$data['contact_no_delivery']	= $this->input->post('txtDeliveryContactNo');
				
				/***** check address already exist or not *****/
				
				$checkShippingDetail = $this->user_model->single_shipping_address($data, $start = 0, $limit = 1);

				if($checkShippingDetail)
				{
					//$_SESSION['customerShippingEmail'] 	  = $email;
					//$_SESSION['customerShippingPhone'] 	  = $phone;

					setcookie('customerShippingEmail', $email ,time() + 60 * 60 * 24 * 30 , '/');
					setcookie('customerShippingPhone', $phone ,time() + 60 * 60 * 24 * 30 , '/');


					$_SESSION['customerShippingAddError'] = 'Shipping address already exist! Please add diffrent';
					
				}
				else
				{
					if($this->user_model->insert_shipping_address($data))
					{
						//$_SESSION['customerShippingEmail'] = $email;
						//$_SESSION['customerShippingPhone'] = $phone;

						setcookie('customerShippingEmail', $email ,time() + 60 * 60 * 24 * 30 , '/');
						setcookie('customerShippingPhone', $phone ,time() + 60 * 60 * 24 * 30 , '/');
	


					}

				}
			}
			else
			{
				
				$data['email']					= $email;
				$data['phone']					= $phone;
				$data['delivery_contact_name']	= $this->input->post('txtDeliveryContactName');
				$data['delivery_address']		= $this->input->post('txtDeliveryAddress');
				$data['delivery_state']			= $this->input->post('selectDeliveryarea');
				$data['delivery_lga']			= $this->input->post('selectDeliverylga');
				$data['contact_no_delivery']	= $this->input->post('txtDeliveryContactNo');
				
				if($this->user_model->insert_shipping_address($data))
				{
					//$_SESSION['customerShippingEmail'] = $email;
					//$_SESSION['customerShippingPhone'] = $phone;

					setcookie('customerShippingEmail', $email ,time() + 60 * 60 * 24 * 30 , '/');
					setcookie('customerShippingPhone', $phone ,time() + 60 * 60 * 24 * 30 , '/');

				}
			}
			
		}
		
		header("Location: ".base_url()."cart");
		die();
	}
	
	
	public function update_payment_status()
	{
		
		$paymentStatus		= '';
		
		if($this->input->post('paymentStatus') && $this->input->post('paymentStatus') == 'netpluspay')
		$paymentStatus		= 'Netpluspay';
		
		if($this->input->post('paymentStatus') && $this->input->post('paymentStatus') == 'ecobankcards')
		$paymentStatus		= 'Card';
		
		if($this->input->post('paymentStatus') && $this->input->post('paymentStatus') == 'ecobank')
		$paymentStatus		= 'Internet Banking';

		if($this->input->post('paymentStatus') && $this->input->post('paymentStatus') == 'pod')
		$paymentStatus		= 'Pay on Delivery';

        if($this->input->post('paymentStatus') && $this->input->post('paymentStatus') == 'bankdeposit')
            $paymentStatus		= 'Bank Deposit';
		
		if($this->input->post('paymentStatus') && $this->input->post('paymentStatus') == 'interswitchng')
		$paymentStatus		= 'interswitchng';

		if($this->input->post('paymentStatus') && $this->input->post('paymentStatus') == 'Coupon')
		$paymentStatus		= 'Coupon';

		if($this->cart_model->update_cart_product(array('payment'=>$paymentStatus), array('order_id'=>$_SESSION['Check'])))
		{
			$this->order_model->update_order(array( 'payment_type' => $paymentStatus), array('order_id'=>$_SESSION['Check']));
			echo 'update';
		}
		
	
	}
	
	public function netpluspay()
	{
		
		
		$shippingAdd 	= $this->user_model->get_shipping_add_by_orderid(array('order.order_id' => $_SESSION['Check']));
		$cartDetail		= $this->cart_model->get_all_items_cart('', $_SESSION['Check']);
		
		$product_name	= '';
		$amount			= 0;
		foreach($cartDetail as $rows)
		{
			$amount  	  	= ($rows->price * $rows->quantity) + $amount;
			$product_name	= $rows->product_name;
			
		}
		$amount		= $amount + $shippingAdd->delivery_amt ;
		//$amount     = $amount * 100;
		
		// deduct coupon amt
		if($shippingAdd->redeemed_total != "" && $shippingAdd->redeemed_total > 0)
		$amount     = $amount - $shippingAdd->redeemed_total;

		$amount 	= number_format((float)$amount, 2, '.', '');

		$returnUrl = base_url().'order/netpluspay_success';
		$notifyUrl = base_url().'order/netpluspay_notify';
		$CancleUrl = base_url().'order/pay_cancle';

		echo '<form method="POST" id="netpluspay_form" name="netpluspay_form" action="https://netpluspay.com/payment/paysrc/" target="_top">
			<input type="hidden" name="total_amount" value="'.$amount.'">
			<input type="hidden" name="email" value="'.$_COOKIE['customerShippingEmail'].'">
			<input type="hidden"  name="full_name" value="'.$_COOKIE['customerShippingEmail'].'">
			<input type="hidden" name="merchant_id" value="MID5ae245abe4cd47.41527052">
			<input type="hidden" name="currency_code" value="NGN">
			<input type="hidden" name="narration" value="Order from MyMall">
			<input type="hidden" name="order_id" value="'.$_SESSION['Check'].'">
			<input type="hidden" name="return_url" value="'.$returnUrl.'">
			<input type="hidden" name="recurring" value="no">
			<button type="submit" class="btn bg-orange btn-block"style="height: 30px; padding:2px 18px;">CHECK OUT</button></form>';
		
		
	}
	
	public function cardpay()
	{
		$amount    		= 0;
		$shippingAdd 	= $this->user_model->get_shipping_add_by_orderid(array('order.order_id' => $_SESSION['Check']));

		$amount 		= $shippingAdd->total_ammount +  $shippingAdd->delivery_amt;

		// deduct coupon amt
		if($shippingAdd->redeemed_total != "" && $shippingAdd->redeemed_total > 0)
		$amount     = $amount - $shippingAdd->redeemed_total;

		
		echo '<form method="POST" id="upay_form" name="upay_form" action="http://57.66.111.227/customerportal/MerchantServices/MakePayment.aspx" target="_top">
            <input type="hidden" name="mercId" value="01172">
            <input type="hidden" name="currCode" value="566">
            <input type="hidden" name="amt" value="'.$amount.'">
            <input type="hidden" name="orderId" value="'.$_SESSION['Check'].'" >
            <input type="hidden" name="prod" value="products">
            <input type="hidden" name="email" value="'.$_COOKIE['customerShippingEmail'].'">
            <input type="submit" class="btn bg-orange btn-block checkOut" name="submit" value="CHECK OUT" style="height: 30px; padding:2px 18px;">
          </form>	
';
	}


	public function interswitchngpay()
	{
		
		
		$shippingAdd 	= $this->user_model->get_shipping_add_by_orderid(array('order.order_id' => $_SESSION['Check']));
		$cartDetail		= $this->cart_model->get_all_items_cart('', $_SESSION['Check']);
		
		$product_name	= '';
		$amount			= 0;
		foreach($cartDetail as $rows)
		{
			$amount  	  	= ($rows->price * $rows->quantity) + $amount;
			$product_name	= $rows->product_name;
			
		}
		
		$amount				= $amount + $shippingAdd->delivery_amt;
		
		// deduct coupon amt
		if($shippingAdd->redeemed_total != "" && $shippingAdd->redeemed_total > 0)
		$amount     = $amount - $shippingAdd->redeemed_total;

		$amount						= $amount * 100;

		$_SESSION['cartAmount'] 	= $amount;
		
		$site_redirect_url 	= base_url().'order/interswitchng_url';
		//$transaction_ref 	= substr(number_format(time() * rand(),0,'',''),0,12);
		$transaction_ref 	= $_SESSION['Check'];
		$product_id 	 	= '5576';
		$pay_item_id 		= '101';
		$mac_key = '7E628F9302BA85744B213EB6EAD352744ABE39F00A95AE9AF2614055AE02AC9904AB91A6722BBBBE066E1A61417127304F79B0ACF308818441A9FCBA79202BF3';
		//$mac_key =   'D3D1D05AFE42AD50818167EAC73C109168A0F108F32645C8B59E897FA930DA44F9230910DAC9E20641823799A107A02068F7BC0F4CC41D2952E249552255710F';

	    $stringtobehashed = $transaction_ref.$product_id.$pay_item_id.$amount.$site_redirect_url .$mac_key;

		$hash = hash('sha512', $stringtobehashed);

		echo '<form name="form1" action="https://webpay.interswitchng.com/paydirect/pay" method="post">
			<input name="product_id" type="hidden" value="'.$product_id.'"/>
			<input name="pay_item_id" type="hidden" value="'.$pay_item_id.'"/>
			<input name="amount" type="hidden" value="'.$amount.'"/>
			<input name="currency" type="hidden" value="566"/>
			<input name="site_redirect_url" type="hidden" value="'.$site_redirect_url.'"/>
			<input name="txn_ref" type="hidden" value="'.$transaction_ref.'"/>
			<input name="hash" type="hidden" value="'.$hash.'"/>
			<input name="cust_name" type="hidden" value="'.$_COOKIE['customerShippingEmail'].'"/>
			<input name="cust_id" type="hidden" value="'.$_COOKIE['customerShippingEmail'].'"/>
			<input type="submit" class="btn bg-orange btn-block" value="CHECK OUT" style="height: 30px; padding:2px 18px;" >
			</form>';
	
	}
    public function payondelivery()
    {

        $action = base_url().'order/payondelivery_response';?>
        <form name="form1" action="<?php echo $action;?>" method="post"><?php
			echo '<input type="submit" class="btn bg-orange btn-block checkout" value="CHECK OUT" style="height: 30px; padding:2px 18px;" >
			</form>';

    }
    public function confirm()
    {

        $action = base_url().'order/confirm_response';?>
        <form name="form1" action="<?php echo $action;?>" method="post"><?php
			echo '<input type="submit" class="btn bg-orange btn-block checkout" value="CHECK OUT" style="height: 30px; padding:2px 18px;" >
			</form>';

    }
    public function bankdeposit()
    {

        $action = base_url().'order/bankdeposit_response';?>
        <form name="form1" action="<?php echo $action;?>" method="post"><?php
			echo '<input type="submit" class="btn bg-orange btn-block checkout" value="CHECK OUT" style="height: 30px; padding:2px 18px;" >
			</form>';

	}
	

	public function coupon()
	{
		$orderID 				= $_SESSION['Check'];
		$couponNo 				= $this->input->get('cpCode');
		// $_SESSION['couponValue'] = $couponNo;
		$shippingID             = $_SESSION['shippingID'];
		$currentDate 			= date('Y-m-d');
		$returnAmt 				= 0;
		$couponeAmt 			= 0;

		if($couponNo == "")
		{
			echo "Please enter coupon code!";
		}
		else
		{
			
			$checkCoupon    = $this->common_model->get_single_reward_coupon(array('code' => $couponNo));
			if(!$checkCoupon && empty($checkCoupon))
			{
				echo "Coupon doesn't exist!"; 
			}
			else
			{	
				 $couponStatus = $this->common_model->get_single_reward_coupon(array('code' => $couponNo, 'status' => 'Unused'), 1);
				 // $couponStatus = $this->common_model->get_single_reward_coupon(array('code' => $couponNo, 'status' => 'Unused', 'email' => ''), 1);				
				 	if(!$couponStatus && empty($couponStatus))
				{
					echo "Coupon has expired!"; 
				}
				else
				{

									 // $couponStatus = $this->common_model->get_single_reward_coupon(array('code' => $couponNo, 'status' => 'Unused'), 1);

					// print_r($couponStatus);
					foreach ($couponStatus as $key => $value) {
						# code...
						$email = $couponStatus->email;
					}
					

					if(strcmp($email, $_COOKIE['customerShippingEmail']) != 0 && !empty($email)){
						echo "Coupon cannot be used by this Customer";
					}else{


					$shippingAdd 	= $this->user_model->get_shipping_add_by_orderid(array('order.order_id' => $orderID));
					$cartDetail		= $this->cart_model->get_all_items_cart('', $orderID);
		
					$amount			= 0;
					$flagAmt        = 0;

					foreach($cartDetail as $rows)
					{
						$amount  	= ($rows->price * $rows->quantity) + $amount;
						
					}
					$amount			= $amount + $shippingAdd->delivery_amt; 
					$couponeAmt    = $couponStatus->value;
					if($amount >= $couponeAmt)
					{
					if($couponStatus->type == "V")
			        {
			        	
			        	//$couponeAmt    = $couponStatus->value;
			          	if($amount >= $couponeAmt)
			          	$returnAmt     = $amount - $couponeAmt;
			          	
			          	else
			          	$flagAmt 	   = 1;

			        }
			        elseif($couponStatus->type=='P')
			        {         
			        	 
			        	$couponeAmt    = (($amount*$couponStatus->value)/100);
			          	if($amount >= $couponeAmt)
			          	$returnAmt     = $amount - $couponeAmt;
			           
			          	else
			          	$flagAmt 	   = 1;

			        }
			        // if($flagAmt != 0)
			        // {
			        // 	echo "Limit of amount should not less than coupon amount!";
			        // }
			        // else {
					

					

						if($returnAmt >= 0){

							$emailId 		= "";

							if(isset($_COOKIE['customerShippingEmail']) && !empty($_COOKIE['customerShippingEmail']))
							{

								$emailId = $_COOKIE['customerShippingEmail'];

							}
							 /***** insert detail in coupon ****/
							$data['coupon_code']     	=  $couponNo;
							$data['email']     			=  $emailId;
							$data['value']     			=  $couponeAmt;
							$data['orderId']     		=  $orderID;

							$this->common_model->add_coupon($data);

							if($couponStatus->session == 'S')
				        	{
								$_SESSION['couponId'] = $couponStatus->id;  
							}
							else
							{
								$_SESSION['couponId'] = "";  
							}

							$this->order_model->update_order(array('redeemed_total' => $couponeAmt), array('order_id' => $orderID)); 

							$orderDetail = $this->order_model->single_order_detail(array('order_id'=>$orderID));
							$orderTotal = 0;

							if($orderDetail)
							{
								$orderTotal = $orderDetail->total_ammount+$orderDetail->delivery_amt;
							}
							if(($orderTotal-$couponeAmt) > 0)
							{
								$orderTotal = $orderTotal-$couponeAmt;
							}
							else {
								$orderTotal = 0.00;
							}
							
							echo "Coupon successfully applied!::".number_format($couponeAmt)."::".number_format($orderTotal)."::".number_format($amount);

						}
					}else{
						echo "Total cost of purchase cannot be less than coupon value";
					}
					// }
					}

				}

			}

			
		}

	} // end of coupon function

	public function coupon_update()
	{
		if(isset($_SESSION['couponId']) && $_SESSION['couponId'] != "")
		{
			$used_date = date('Y-m-d H:i:s');
			$this->common_model->update_reward_coupon(array('status' => 'Used'), array('id' => $_SESSION['couponId']), array('used_date' => $used_date));
						
		}

	} // end of coupon_update function





}
