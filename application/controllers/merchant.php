<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Merchant extends CI_Controller {

    

	function __construct()

	{

		parent::__construct();



		$this->load->model('product_model');

		$this->load->model('user_model');

		$this->load->model('cart_model');

		$this->load->model('home_model');

		

	}


	public function update_slug()

	{

		
		$userDetail 	= $this->home_model->all_merchant();	

		foreach ($userDetail as $value) {

			$data =  array(	 'slug' => preg_replace('/[^A-Za-z0-9-]+/', '-', $value->first_name));

			$this->user_model->update_user($data,  $value->id );

			
		}



	}




	public function detail($mer)

	{

		
		//$merchantDetail  		 = $this->user_model->single_merchant_detail(array('category.isactive'=>'t','users.slug'=>$mer, 'users.isactive'=>'t'));
               $merchantDetail  		 = $this->user_model->single_merchant_detail(array('users.slug'=>$mer, 'users.isactive'=>'t'));

		//print_r($merchantDetail);
		
		if($merchantDetail)
		{
		

			// update viewed count start...
			$this->user_model->update_user(array('viewed' => ($merchantDetail->viewed + 1)), $merchantDetail->id); 
			// viewed count end...

			$merId 					 = $merchantDetail->id;  
	
			$data['merchantDetail']	 = $merchantDetail; 
	
			$data['merId'] 			 = $merId;
	
			$data['relatedProducts'] = $this->product_model->related_product_by_id(array('pid' => $merId, 'isactive' => 't'));
	
			$data['merchantBanner']  = $this->product_model->merchant_banner($merId);
	
			$this->layout->js(base_url().'assets/js/stars.js');
	
			$this->layout->css(base_url().'assets/css/owl.carousel.css');
	
			$this->layout->css(base_url().'assets/css/owl.theme.css');
	
			$this->layout->js(base_url().'assets/js/owl.carousel.min.js');
	
			$this->layout->js(base_url().'assets/js/custom_js.js');
	
			$this->layout->js(base_url().'assets/js/html5gallery.js');
	
			$this->layout->view('merchant',$data);
		}
		else
		{
			$this->layout->css(base_url().'assets/css/main.css');
			$data['productDetail']  = $this->home_model->all_product(array('products.isactive' => 't'), 8);
			$this->layout->view("my404_view",$data);
		}

	}





	public function contact_us()

	{

		

		$merId 		= base64_decode($this->input->post('numMerId'));

		$detail 	= $this->user_model->single_user_detail(array('id' => $merId ));

		if($detail)

		{

			

			$data['name'] 			= $this->input->post('txtName');

			$data['user_email'] 	= $this->input->post('txtEmail');

			$data['phone_no'] 		= $this->input->post('numPnone');

			$data['comment'] 		= $this->input->post('txtComments');

			$data['receive_update'] = $this->input->post('cheReceiveUpdates');

			$data['merchant_email'] = $detail->email;

			

			$this->common_model->insert_merchant_contactus($data);

			

			$cheReceiveUpdates  = 'No';

			

			if($this->input->post('cheReceiveUpdates') == 1)

			{

				$cheReceiveUpdates = 'Yes';

			}

			

			$message = '

					<!DOCTYPE html>
					<html>
					<head lang="en">
					    <meta charset="UTF-8">
					    <title></title>
					</head>
					<body style="background-color: #f3f3f3; padding: 0; margin: 0;font-family: \'Calibri\', Arial, sans-serif;">
					<div style="margin: 0 auto; width:600px;background-color: #fff;">
					    <table width="100%">
					        <tr>
					            <td colspan="6">
					                <div style="background-color: #00587C;
					                        width:100%; height:100px; border-bottom: solid 3px #9AC434; padding:20px 0">
					                    <table>
					                        <tr>
					                            <td width="5%"></td>
					                            <td width="60%">
					                                <h1 style="color:#fff; font-size:36px; font-weight: normal"></h1>
					                            </td>
					                            <td width="30%">
					                                <img src="'.base_url().'assets/img/logo.png" />
					                            </td>
					                            <td width="5%"></td>
					                        </tr>
					                    </table>
					                </div>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <p>
					                    Dear <strong>'.$detail->first_name.'</strong>,<br /><br />
					                    New request received, details are listed below.
					                </p>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <div style="border: solid 1px #ccc; background-color: #f3f3f3; padding: 15px;">
					                    
					                    <table width="100%">
					                        <tr>
					                            <td width="20%" height="30">Name</td>
					                            <td width="60%">
					                                <div style="color: #000;line-height:22px;">
					                                    '.$this->input->post('txtName').'
					                                </div>
					                            </td>
					                            <td width="20%"></td>
					                        </tr>
					                        <tr>
					                            <td width="20%" height="30">Email</td>
					                            <td width="60%">
					                                <div style="color: #000;line-height:22px;">
					                                    '.$this->input->post('txtEmail').'
					                                </div>
					                            </td>
					                            <td width="20%"></td>
					                        </tr>
					                         <tr>
					                            <td width="20%" height="30">Phone</td>
					                            <td width="60%">
					                                <div style="color: #000;line-height:22px;">
					                                    '.$this->input->post('numPnone').'
					                                </div>
					                            </td>
					                            <td width="20%"></td>
					                        </tr>
					                         <tr>
					                            <td width="20%" height="30">Comments</td>
					                            <td width="60%">
					                                <div style="color: #000;line-height:22px;">
					                                    '.$this->input->post('txtComments').'
					                                </div>
					                            </td>
					                            <td width="20%"></td>
					                        </tr>
					                         <tr>
					                            <td width="20%" height="30">I want to receive updates on deals from this merchant</td>
					                            <td width="60%">
					                                <div style="color: #000;line-height:22px;">
					                                    '.$cheReceiveUpdates.'
					                                </div>
					                            </td>
					                            <td width="20%"></td>
					                        </tr>
					                    </table>
					                </div>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <p style="text-align: center">
					                    For more information, call 0818 778 2542, 0818 778 2542 or send an email to <a href="mailto:operator@mymall.com.ng" target="_top">operator@mymall.com.ng</a>
					                </p>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <table width="100%">
					                    <tr>
					                        <td width="20%"><img src="'.base_url().'assets/img/ecobank.png" /></td>
					                        <td width="20%"></td>
					                        <td width="20%"></td>
					                        <td width="1%"><img src="'.base_url().'assets/img/fedex.png" height="25px" width="auto" /></td>
					                      
					                    </tr>
					                </table>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px; border-top: solid 1px #ccc;">
					                <table width="100%">
					                    <tr>
					                        <td width="60%">
					                            <p style="font-size:13px; color: #333;line-height: 30px">
					                                &copy; 2015, All rights reserved. Mymall
					                            </p>
					                        </td>

					                        <td width="40%">
					                            <img src="'.base_url().'assets/img/youtube.jpg" style="float:right; margin-left:5px;" />
					                            <img src="'.base_url().'assets/img/linkin.jpg" style="float:right; margin-left: 5px;" />
					                            <a target="_blank" href="https://twitter.com/MymallNg"><img src="'.base_url().'assets/img/twitter.jpg" style="float:right; margin-left: 5px;" /></a>
					                            <a target="_blank" href="https://www.facebook.com/MymallNg"><img src="'.base_url().'assets/img/facebook.jpg" style="float:right; margin-left: 5px;" /></a>
					                        </td>
					                    </tr>
					                </table>
					            </td>
					        </tr>
					    </table>
					</div>
					</body>
					</html>

				   ';


			$msg = ($message);


			$this->load->library('email');

			

			$config['mailtype'] = "html";

			

			$this->email->initialize($config);

			
			//$this->email->from("amit@geeksperhour.com", "Mymall");	
			//$this->email->from(AdminEmail, AdminEmailName);

			

			//$this->email->to($detail->email, $detail->display_name);

			//$this->email->to('parmeet@geeksperhour.com', $detail->display_name);

			//$this->email->cc(AdminEmail, AdminEmailName); 

			
			$this->email->subject('Contact Us Form Mymall - Online Store Solution');

			

			$this->email->message($msg);

			

			if($this->email->send())

			{

				echo '<div class="alert alert-success message" role="alert">Mail has been sent successfully.<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">x</span></button></div>';

			}

			else

			{

				echo '<div class="alert alert-danger message" role="alert">Mail has not sent. Please try again!<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">x</span></button></div>';

			}

		}

		else

		{

			echo '<div class="alert alert-danger message" role="alert">Mail has not sent. Please try again!<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">x</span></button></div>';

		}

	} //end of function



	public function profile()

	{

		

		$merchantDetail			 	= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail']->id ));  
		

		$data['merchantDetail']	 	= $merchantDetail; 

		///$data['merchantProducts'] 	= $this->product_model->related_product_by_id(array('pid' => $_SESSION['userDetail']->id) );
	

		$this->layout->css(base_url().'assets/css/cycle2.css');

		$this->layout->js(base_url().'assets/js/jquery.cycle2.js');

		$this->layout->js(base_url().'assets/js/jquery.cycle2.carousel.js');

		$this->layout->script("<script>$.fn.cycle.defaults.autoSelector = '.slideshow';</script>"); 

		

		$this->layout->view('profile',$data);

	}



	public function edit_profile()

	{

		

		$data 		= '';

		$image 	= $userImg 	= '';

		
		if($this->input->post('submit') && $this->input->post('submit') != '')

		{
			$checkuser			= $this->user_model->single_user_detail_not_equle_id($_SESSION['userDetail']->id, $this->input->post('first_name'), $this->input->post('email'));
			
			//$regexEmail 		= preg_match('/^[A-z0-9_\-]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z.]{2,4}$/', $this->input->post('email'));
			if(!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL))
			{
			
				$data['error_msg'] = $this->input->post('email').' is an invalid email. please enter correct!';
			}
			else if(!$checkuser)
			{

			
				// logo upload 

				if(isset($_FILES['txtImage']['name']) && $_FILES['txtImage']['name'] != '')

				{

					$time 	= strtotime(date('Y-m-d H:i:s'));

					$ext 	= pathinfo($_FILES['txtImage']['name'], PATHINFO_EXTENSION);

					$image  = $time.'.'.$ext; 

					move_uploaded_file($_FILES['txtImage']['tmp_name'],'./banner_images/'.$image);

					$this->load->library('image_lib');
					
					$config['source_image'] = './banner_images/'.$image;
				
					$config['width'] = 400;
					
					$config['height'] = 400;
					
					$config['maintain_ratio'] = TRUE;
					
					$config['new_image'] = './banner_images/'.$image;
					
					$flag_for_image = 0;
					
					$this->image_lib->initialize($config);
					
					if(!$this->image_lib->resize()){
					
						$flag_for_image = 1;
					
					} 	
					
					$this->image_lib->clear();


					if($this->input->post('txtOldImage') && $this->input->post('txtOldImage') !='' && file_exists('../../banner_images/'.$this->input->post('txtOldImage')))

					unlink('../../banner_images/'.$this->input->post('txtOldImage'));

				}

				else

				{

					$image 	= $this->input->post('txtOldImage');

				}

				// user image upload 

				/*if(isset($_FILES['txtUserImg']['name']) && $_FILES['txtUserImg']['name'] != '')

				{

					$time 		= strtotime(date('Y-m-d H:i:s'));

					$ext 		= pathinfo($_FILES['txtUserImg']['name'], PATHINFO_EXTENSION);

					$userImg 	= $time.'.'.$ext; 

					move_uploaded_file($_FILES['txtUserImg']['tmp_name'],'./provider_images/'.$userImg);


					$this->load->library('image_lib');
					
					$config['source_image'] = './provider_images/'.$userImg;
				
					$config['width'] = 400;
					
					$config['height'] = 400;
					
					$config['maintain_ratio'] = TRUE;
					
					$config['new_image'] = './product_images/'.$userImg;
					
					$flag_for_image = 0;
					
					$this->image_lib->initialize($config);
					
					if(!$this->image_lib->resize()){
					
						$flag_for_image = 1;
					
					} 	
					
					$this->image_lib->clear();


					if($this->input->post('txtOldUserImg') && $this->input->post('txtOldUserImg') !='' && file_exists('../../provider_images/'.$this->input->post('txtOldUserImg')))

					unlink('../../provider_images/'.$this->input->post('txtOldUserImg'));

				}

				else

				{

					$userImg 	= $this->input->post('txtOldUserImg');

				}*/
				//   'zip' 				=> $this->input->post('txtZip'),
				$data =  array('first_name' 		=> $this->input->post('first_name'),

							  'full_name' 			=> $this->input->post('txtFullName'),

							  'slug' 				=> preg_replace('/[^A-Za-z0-9-]+/', '-', $this->input->post('first_name')),

							  'email' 				=> $this->input->post('email'),

							  'category' 			=> $this->input->post('txtCategory'),

							  'phone' 				=> $this->input->post('txtPhone'),

							  'address' 			=> $this->input->post('txtAddress'),

							  'country' 			=> "NG",

							  'state' 				=> $this->input->post('txtState'),

							  'city' 				=> $this->input->post('txtCity'),

							  'image' 				=> $image,

							  //'user_img' 			=> $userImg,

							  'deliveryarea' 		=> $this->input->post('txtDeliveryarea'),

							);

				if($this->input->post('txtPassword') && $this->input->post('txtPassword') != '')

				{

					$data['password'] = md5($this->input->post('txtPassword'));

				}

				if($this->input->post('bankaccounselect') && $this->input->post('bankaccounselect') == 'No')

				{

					$data['account_type'] = 'No';

				}

				else

				{

					$data['account_type'] = $this->input->post('txtAccountType');

				}


				if($this->user_model->update_user($data,  $_SESSION['userDetail']->id ))

				$data['msg'] = 'Profile has been successfully updated.';

				else

				$data['error_msg'] = 'Profile has not updated. Please try again!';

			}
			else
			{
				//$data['error_msg'] = 'Merchant has not register.please try again!';

				$existFields	= '';
				if($checkuser->first_name == $this->input->post('first_name') )
				{
					$existFields	.= 'Business name';
				}
				
				if($existFields != '')
				{
					$existFields	.= ' and ';
				}
				
				if($checkuser->email == $this->input->post('email') )
				{
					$existFields	.= 'Email';
				}

				$data['error_msg'] = $existFields.' already exist. please enter another!';

			}

		

		}

		
		$merchantDetail				= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));

		$data['merchantDetail']		= $merchantDetail;

		

		$data['countries'] 			= $this->common_model->get_countries();

		$data['states_merchant'] 	= $this->common_model->get_states_merchant();

		$data['states'] 			= $this->common_model->get_states($merchantDetail->country);

		$data['categories'] 		= $this->category_model->all_category_list(array('isactive' => 1 ));

		

		$this->layout->css(base_url().'assets/css/cycle2.css');

		$this->layout->js(base_url().'assets/js/jquery.cycle2.js');

		$this->layout->js(base_url().'assets/js/jquery.cycle2.carousel.js');

		$this->layout->css(base_url().'assets/css/validationEngine.jquery.css');

		$this->layout->js(base_url().'assets/js/jquery.validationEngine-en.js');

		$this->layout->js(base_url().'assets/js/jquery.validationEngine.js');

		$this->layout->script("<script type='text/javascript'> $.fn.cycle.defaults.autoSelector = '.slideshow'; $('#edit-profile').validationEngine({ promptPosition : 'topLeft',ajaxFormValidationURL : '".base_url()."ajax_validation/checkuniquenessForexistingUser',}); </script>");
		

		$this->layout->view('edit_profile',$data);

	}

	public function change_password()

	{

		$data 		= '';

		
		if($this->input->post('submit') && $this->input->post('submit') != '')

		{
			$checkuserpass	= $this->user_model->single_user_detail(array('id' => $_SESSION['userDetail']->id, 'password' =>md5($this->input->post('txtOldPassword'))));
			
			
			if($this->input->post('txtNewPassword') && $this->input->post('txtNewPassword') == '')
			{
			
				$data['error_msg'] = 'Invalid password. Please enter correct!';
			}
			else if(!$checkuserpass)
			{
				$data['error_msg'] = 'Invalid password. Please enter correct!';
			}
			else if($checkuserpass)
			{

				$data['password'] = md5($this->input->post('txtNewPassword'));


				if($this->user_model->update_user($data,  $_SESSION['userDetail']->id ))

				$data['msg'] = 'Password has been successfully updated.';

				else

				$data['error_msg'] = 'Password has not updated. Please try again!';

			}
			
		}

		
		$merchantDetail				= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));

		$data['merchantDetail']		= $merchantDetail;

		
		$this->layout->css(base_url().'assets/css/validationEngine.jquery.css');

		$this->layout->js(base_url().'assets/js/jquery.validationEngine-en.js');

		$this->layout->js(base_url().'assets/js/jquery.validationEngine.js');

		$this->layout->script("<script type='text/javascript'> $('#change-password').validationEngine({ promptPosition : 'topLeft'}); </script>");
		

		$this->layout->view('change_password',$data);

	}

	public function contact()

	{

		

		if($this->input->get('txtEmail') && $this->input->get('txtEmail') != '')

		{

			

			$merchantDetail = $this->user_model->single_user_detail(array('id' => base64_decode($this->input->get('txtMerchant'))));

			

			if($merchantDetail->email != '')

			{


			

				$data['name'] 			= $this->input->get('txtName');

				$data['user_email'] 	= $this->input->get('txtEmail');

				$data['phone_no'] 		= $this->input->get('txtPhone');

				$data['comment'] 		= $this->input->get('txtMessage');

				//$data['receive_update'] = $this->input->get('cheReceiveUpdates');

				$data['merchant_email'] = $merchantDetail->email;


				$this->common_model->insert_merchant_contactus($data);


				
				$message = '

					<!DOCTYPE html>
					<html>
					<head lang="en">
					    <meta charset="UTF-8">
					    <title></title>
					</head>
					<body style="background-color: #f3f3f3; padding: 0; margin: 0;font-family: \'Calibri\', Arial, sans-serif;">
					<div style="margin: 0 auto; width:600px;background-color: #fff;">
					    <table width="100%">
					        <tr>
					            <td colspan="6">
					                <div style="background-color: #00587C;
					                        width:100%; height:100px; border-bottom: solid 3px #9AC434; padding:20px 0">
					                    <table>
					                        <tr>
					                            <td width="5%"></td>
					                            <td width="60%">
					                                <h1 style="color:#fff; font-size:36px; font-weight: normal"></h1>
					                            </td>
					                            <td width="30%">
					                                <img src="'.base_url().'assets/img/logo.png" />
					                            </td>
					                            <td width="5%"></td>
					                        </tr>
					                    </table>
					                </div>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <p>
					                    Dear <strong>'.$merchantDetail->display_name.'</strong>,<br /><br />
					                    New request received, details are listed below.
					                </p>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <div style="border: solid 1px #ccc; background-color: #f3f3f3; padding: 15px;">
					                    
					                    <table width="100%">
					                        <tr>
					                            <td width="20%" height="30">Name</td>
					                            <td width="60%">
					                                <div style="color: #000;line-height:22px;">
					                                    '.$this->input->get('txtName').'
					                                </div>
					                            </td>
					                            <td width="20%"></td>
					                        </tr>
					                        <tr>
					                            <td width="20%" height="30">Email</td>
					                            <td width="60%">
					                                <div style="color: #000;line-height:22px;">
					                                    '.$this->input->get('txtEmail').'
					                                </div>
					                            </td>
					                            <td width="20%"></td>
					                        </tr>
					                         <tr>
					                            <td width="20%" height="30">Phone</td>
					                            <td width="60%">
					                                <div style="color: #000;line-height:22px;">
					                                    '.$this->input->get('txtPhone').'
					                                </div>
					                            </td>
					                            <td width="20%"></td>
					                        </tr>
					                         <tr>
					                            <td width="20%" height="30">Comments</td>
					                            <td width="60%">
					                                <div style="color: #000;line-height:22px;">
					                                    '.$this->input->get('txtMessage').'
					                                </div>
					                            </td>
					                            <td width="20%"></td>
					                        </tr>
					                    </table>
					                </div>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <p style="text-align: center">
					                    For more information, call 0818 778 2542, 0818 778 2542 or send an email to <a href="mailto:operator@mymall.com.ng" target="_top">operator@mymall.com.ng</a>
					                </p>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <table width="100%">
					                    <tr>
					                        <td width="20%"><img src="'.base_url().'assets/img/ecobank.png" /></td>
					                        <td width="20%"></td>
					                        <td width="20%"></td>
					                        <td width="1%"><img src="'.base_url().'assets/img/fedex.png" height="25px" width="auto" /></td>
					                        
					                    </tr>
					                </table>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px; border-top: solid 1px #ccc;">
					                <table width="100%">
					                    <tr>
					                        <td width="60%">
					                            <p style="font-size:13px; color: #333;line-height: 30px">
					                                &copy; 2015, All rights reserved. Mymall
					                            </p>
					                        </td>

					                        <td width="40%">
					                            <img src="'.base_url().'assets/img/youtube.jpg" style="float:right; margin-left:5px;" />
					                            <img src="'.base_url().'assets/img/linkin.jpg" style="float:right; margin-left: 5px;" />
					                            <a target="_blank" href="https://twitter.com/MymallNg"><img src="'.base_url().'assets/img/twitter.jpg" style="float:right; margin-left: 5px;" /></a>
					                            <a target="_blank" href="https://www.facebook.com/MymallNg"><img src="'.base_url().'assets/img/facebook.jpg" style="float:right; margin-left: 5px;" /></a>
					                        </td>
					                    </tr>
					                </table>
					            </td>
					        </tr>
					    </table>
					</div>
					</body>
					</html>

				   ';


				$msg = ($message);


				$this->load->library('email');

				

				$config['mailtype'] = "html";

				

				$this->email->initialize($config);


				//$this->email->to('toparmeet@gmail.com', $merchantDetail->display_name);


				//$this->email->to($merchantDetail->email, $merchantDetail->display_name);

				//$this->email->cc(AdminEmail, AdminEmailName); 

				
				$this->email->from($this->input->get('txtEmail'), $this->input->get('txtName'));

				//$this->email->from('toparmeet@gmail.com');

				

				$this->email->subject('Contact Us Form Mymall - Online Store Solution ');

				$this->email->message($msg);

				

				if($this->email->send()){



					echo '<div class="alert alert-success successMessage" role="alert">Mail has been sent successfully.</div>';

				}

			}

			else

			{

				echo '<div class="alert alert-danger successMessage" role="alert">Mail has not sent. Please try again!</div>';

			}

		}

		else

		{

			echo '<div class="alert alert-danger successMessage" role="alert">Mail has not sent. Please try again!</div>';

		}

			

	} // end of contact function..


	public function transaction()
	{
		$this->load->library('pagination');
		$data				= array();
		
		$data['merchantDetail']		= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));
		
		
		$total_rows  				= $this->user_model->transaction_by_id(array('products.pid' =>$_SESSION['userDetail'] ->id, 'order.status' => 'Completed'));
		if(count($total_rows) > 0 )
		{
			
			$config['base_url'] 			= base_url().'transaction/';
			$config['total_rows'] 			= count($total_rows);//$query_num_of_row->num_rows();
			$config['per_page'] 			= 10; //this should be change if we change numberOfrecord
			$config['uri_segment'] 			= 4;
			$config['last_link'] 			= 'Last';
			$config['first_link'] 			= 'First';
			$config['page_query_string'] 	= TRUE;
			$config['num_links'] 			= 4;

			$this->pagination->initialize($config);
			
			if(isset($_GET['p']) )
			{
				$offset = $this->pagination->per_page * $_GET['p'] - $this->pagination->per_page ;
				
			}else{
				$offset = '';
			}
		}
		
		$data['transaction'] 		= $this->user_model->transaction_by_id(array('products.pid' =>$_SESSION['userDetail'] ->id, 'order.status' => 'Completed'),$this->pagination->per_page,$offset);
		

		$this->layout->css(base_url().'assets/css/pagination.css');
		$this->layout->css(base_url().'assets/css/cycle2.css');
		$this->layout->js(base_url().'assets/js/jquery.cycle2.js');
		$this->layout->js(base_url().'assets/js/jquery.cycle2.carousel.js');
		
		$this->layout->script("<script type='text/javascript'> $.fn.cycle.defaults.autoSelector = '.slideshow';</script>");

		$this->layout->view('transaction',$data);

	}



	public function merchant_description()
	{
		if($this->input->post('submit') && $this->input->post('submit') != '')
		{
			
			// user image upload 
			$userImg    		= "";
			if(isset($_FILES['txtUserImg']['name']) && $_FILES['txtUserImg']['name'] != '')

			{

				$time 		= strtotime(date('Y-m-d H:i:s'));

				$ext 		= pathinfo($_FILES['txtUserImg']['name'], PATHINFO_EXTENSION);

				$userImg 	= $time.'.'.$ext; 

				move_uploaded_file($_FILES['txtUserImg']['tmp_name'],'./provider_images/'.$userImg);


				$this->load->library('image_lib');
				
				$config['source_image'] = './provider_images/'.$userImg;
			
				$config['width'] = 400;
				
				$config['height'] = 400;
				
				$config['maintain_ratio'] = TRUE;
				
				$config['new_image'] = './product_images/'.$userImg;
				
				$flag_for_image = 0;
				
				$this->image_lib->initialize($config);
				
				if(!$this->image_lib->resize()){
				
					$flag_for_image = 1;
				
				} 	
				
				$this->image_lib->clear();


				if($this->input->post('txtOldUserImg') && $this->input->post('txtOldUserImg') !='' && file_exists('../../provider_images/'.$this->input->post('txtOldUserImg')))

				unlink('../../provider_images/'.$this->input->post('txtOldUserImg'));

			}

			else

			{

				$userImg 	= $this->input->post('txtOldUserImg');

			}




			$data =  array('description' 		=> $this->input->post('txtDescription'),
						   'user_img' 			=> $userImg,

						  );

			if($this->user_model->update_user($data,  $_SESSION['userDetail']->id ))

			$data['msg'] = 'Detail has been successfully updated.';

			else

			$data['error_msg'] = 'Detail has not updated. Please try again!';

		}

		
		$merchantDetail				= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));

		$data['merchantDetail']		= $merchantDetail;

		$this->layout->view('merchant_description',$data);

	}



	public function meet_our_merchants()
	{
		
		$this->layout->css(base_url().'assets/css/main.css');
		$this->layout->css(base_url().'assets/css/tabcontent.css');
		$this->layout->js(base_url().'assets/js/tabcontent.js');

		$this->layout->view('meet_our_merchants');

	}


} // END of class

