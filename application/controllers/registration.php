<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->model('user_model');
		$this->load->model('category_model');
		$this->load->model('common_model');
	}


	public function index()
	{
		$data 						= '';
		$image						= '';
		$data['userArray']			= '';


		if($this->input->post('submit') && $this->input->post('submit') != '')
		{
			
					
			$data['userArray']	= $_POST;
			$checkuser			= $this->user_model->single_user_detail(array('first_name' => $this->input->post('first_name')), array('email' => $this->input->post('email')));
			
			//$regexEmail 		= preg_match('/^[A-z0-9_\-]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z.]{2,4}$/', $this->input->post('email'));
			
			if(!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL))
			{
				$data['error_msg'] = $this->input->post('email').' is an invalid email. please enter correct!';
			}
			else if(!$checkuser)
			{
				// logo upload 
				if(isset($_FILES['txtImage']['name']) && $_FILES['txtImage']['name'] != '')
				{
					$time 	= strtotime(date('Y-m-d H:i:s'));
					$ext 	= pathinfo($_FILES['txtImage']['name'], PATHINFO_EXTENSION);
					$image  = $time.'.'.$ext; 
					move_uploaded_file($_FILES['txtImage']['tmp_name'],'./banner_images/'.$image);

					$this->load->library('image_lib');
					
					$config['source_image'] = './banner_images/'.$image;
				
					$config['width'] = 400;
					
					$config['height'] = 400;
					
					$config['maintain_ratio'] = TRUE;
					
					$config['new_image'] = './banner_images/'.$image;
					
					$flag_for_image = 0;
					
					$this->image_lib->initialize($config);
					
					if(!$this->image_lib->resize()){
					
						$flag_for_image = 1;
					
					} 	
					
					$this->image_lib->clear();

				}

				$displayName = $slug = "";
				if($this->input->post('first_name') && $this->input->post('first_name') != '')
				{
					$displayName = ucwords(preg_replace('/[^A-Za-z0-9-\ ]+/', '', $this->input->post('first_name')));
					$slug 		 = preg_replace('/[^A-Za-z0-9-]+/', '-', $this->input->post('first_name'));
					
				}

				// 'zip' 				=> $this->input->post('txtZip'),

				$state_id = $this->common_model->get_state_by_name(array('name' => $this->input->post('selectDeliveryarea')));
				$lga_id = $this->common_model->get_lga_by_name(array('name' => $this->input->post('selectDeliverylga')));
				
				$data =  array('first_name' 		=> $this->input->post('first_name'),
							  'display_name' 		=> $displayName,
							  'full_name' 			=> $this->input->post('txtFullName'),
							  'slug' 				=> $slug,
							  'email' 				=> $this->input->post('email'),
							  'password' 			=> md5($this->input->post('txtPassword')),
							  'category' 			=> $this->input->post('seleCategory'),
							  'phone' 				=> $this->input->post('txtPhone'),
							  'address' 			=> $this->input->post('txtAddress'),
							  'country' 			=> "NG",
							  'state' 				=> $this->input->post('txtState'),
							  'city' 				=> $this->input->post('txtCity'),
							  'image' 				=> $image,
							  'pickup_state' 		=> $state_id->id,
							  'pickup_lga' 			=> $lga_id->id,
							  'account_type' 		=> $this->input->post('txtAccountType'),
							  'user_type' 			=> 'p',
							  'isactive' 			=> 'p',
							  'created'				=> date("Y:m:d H-i-s")
							);
				$state = $this->common_model->get_state_by_id(array('state_id' => $this->input->post('txtState')));
				$category = $this->category_model->get_single_category(array('id' => $this->input->post('seleCategory')));
				if($this->user_model->insert_user($data))
				{
					
					$message = '
								<body style="background-color: #f3f3f3; padding: 0; margin: 0;font-family: \'Calibri\', Arial, sans-serif;">
								<div style="margin: 0 auto; width:600px;background-color: #fff;">
								    <table width="100%">
								        <tr>
								            <td colspan="6">
								                <div style="background-color: #00587C;
								                        width:100%; height:100px; border-bottom: solid 3px #9AC434; padding:20px 0">
								                    <table>
								                        <tr>
								                            <td width="5%"></td>
								                            <td width="60%">
								                                <h1 style="color:#fff; font-size:36px; font-weight: normal">Congratulations!</h1>
								                            </td>
								                            <td width="30%">
								                                <img src="'.base_url().'assets/img/logo.png" />
								                            </td>
								                            <td width="5%"></td>
								                        </tr>
								                    </table>
								                </div>
								            </td>
								        </tr>

								        <tr>
								            <td colspan="6" style="padding:10px 20px;">
								                <p>
								                    Dear <strong>Admin</strong>,<br /><br />
								                    New merchant has been registered, details are listed below.
								                </p>
								            </td>
								        </tr>

								        <tr>
								            <td colspan="6" style="padding:10px 20px;">
								                <table width="100%">
								                    <tr>
								                        <td width="40%" height="30">Username</td>
								                        <td width="60%">
								                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px; padding: 0 10px;">
								                                <a href="javaScript:;" style="color: #fff;">'.$this->input->post('email').'</a>
								                            </div>
								                        </td>
								                        <td width="20%"></td>
								                    </tr>
								                    <tr>
								                        <td width="40%" height="30">Full Name</td>
								                        <td width="60%">
								                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
								                            padding: 0 10px;">
								                                '.$this->input->post('txtFullName').'
								                            </div>
								                        </td>
								                        <td width="20%"></td>
								                    </tr>
								                    <tr>
								                        <td width="40%" height="30">Phone</td>
								                        <td width="60%">
								                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
								                            padding: 0 10px;">
								                                '.$this->input->post('txtPhone').'
								                            </div>
								                        </td>
								                        <td width="20%"></td>
								                    </tr>
								                    <tr>
								                        <td width="40%" height="30">Address</td>
								                        <td width="60%">
								                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
								                            padding: 0 10px;">
								                                '.$this->input->post('txtAddress').'
								                            </div>
								                        </td>
								                        <td width="20%"></td>
								                    </tr>
								                    <tr>
								                        <td width="40%" height="30">State</td>
								                        <td width="60%">
								                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
								                            padding: 0 10px;">
								                                '.$state->name.'
								                            </div>
								                        </td>
								                        <td width="20%"></td>
								                    </tr>
								                    <tr>
								                        <td width="40%" height="30">City</td>
								                        <td width="60%">
								                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
								                            padding: 0 10px;">
								                                '.$this->input->post('txtCity').'
								                            </div>
								                        </td>
								                        <td width="20%"></td>
								                    </tr>
								                    <tr>
								                        <td width="40%" height="30">Category</td>
								                        <td width="60%">
								                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
								                            padding: 0 10px;">
								                                '.$category->category_name.'
								                            </div>
								                        </td>
								                        <td width="20%"></td>
								                    </tr>
								                    <tr>
								                        <td width="40%" height="30">Merchant Pickup State:</td>
								                        <td width="60%">
								                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
								                            padding: 0 10px;">
								                                '.$this->input->post('txtDeliveryState').'
								                            </div>
								                        </td>
								                        <td width="20%"></td>
													</tr>
													<tr>
								                        <td width="40%" height="30">Merchant Pickup LGA:</td>
								                        <td width="60%">
								                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
								                            padding: 0 10px;">
								                                '.$this->input->post('txtDeliveryLga').'
								                            </div>
								                        </td>
								                        <td width="20%"></td>
								                    </tr>

								                    <tr>
								                        <td width="40%" height="30">Do you have an Ecobank account?</td>
								                        <td width="60%">
								                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
								                            padding: 0 10px;">
								                                '.$this->input->post('txtAccountType').'
								                            </div>
								                        </td>
								                        <td width="20%"></td>
								                    </tr>

								                </table>
								            </td>
								        </tr>


								        <tr>
								            <td colspan="6" style="padding:10px 20px;">
								                <p style="text-align: center">
								                    For more information, call 0818 778 2542, 0818 778 2542 or send an email to <a href="mailto:operator@mymall.com.ng" target="_top">operator@mymall.com.ng</a>
								                </p>
								            </td>
								        </tr>

								        <tr>
								            <td colspan="6" style="padding:10px 20px;">
								                <table width="100%">
								                    <tr>
								                        <td width="20%"><img src="'.base_url().'assets/img/ecobank.png" /></td>
								                        <td width="20%"></td>
								                        <td width="20%"></td>
								                        <td width="1%"><img src="'.base_url().'assets/img/fedex.png" height="25px" width="auto" /></td>
								                    </tr>
								                </table>
								            </td>
								        </tr>

								        <tr>
								            <td colspan="6" style="padding:10px 20px; border-top: solid 1px #ccc;">
								                <table width="100%">
								                    <tr>
								                        <td width="60%">
								                            <p style="font-size:13px; color: #333;line-height: 30px">
								                                &copy; 2015, All rights reserved. Mymall
								                            </p>
								                        </td>

								                        <td width="40%">
								                            <img src="'.base_url().'assets/img/youtube.jpg" style="float:right; margin-left:5px;" />
								                            <img src="'.base_url().'assets/img/linkin.jpg" style="float:right; margin-left: 5px;" />
								                            <a target="_blank" href="https://twitter.com/MymallNg"><img src="'.base_url().'assets/img/twitter.jpg" style="float:right; margin-left: 5px;" /></a>
								                            <a target="_blank" href="https://www.facebook.com/MymallNg"><img src="'.base_url().'assets/img/facebook.jpg" style="float:right; margin-left: 5px;" /></a>
								                        </td>
								                    </tr>
								                </table>
								            </td>
								        </tr>
								    </table>
								</div>
							   ';


					$emailList = array('oogunleye@ecobank.com', 'smeclub@ecobank.com');

					foreach ($emailList as $address)
					{
					
						
						$msg  =( "<html>\n");
						
						$msg .=( "<head>\n");
											
						$msg .=( "</head>\n");
						
						$msg .=( "<body>\n");
						
						$msg .= ($message);
						
						$msg .=( "</body>\r\n");
						
						$msg .=( "</html>\r\n");
						
						$this->load->library('email');
						
						$config['mailtype'] = "html";
						
						$this->email->initialize($config);
						
						 $this->email->clear();

						$this->email->from($this->input->post('email'), $this->input->post('first_name'));

						//$this->email->to("oogunleye@ecobank.com", "Mymall");
										
						$this->email->to($address);
			
						$this->email->subject('New merchant registration');
						
						$this->email->message($msg);
						
					//	$this->email->send();
				    }
					

					
					//  send mail to user.....

					$message = '
								<body style="background-color: #f3f3f3; padding: 0; margin: 0;font-family: \'Calibri\', Arial, sans-serif;">
								<div style="margin: 0 auto; width:600px;background-color: #fff;">
								    <table width="100%">
								        <tr>
								            <td colspan="6">
								                <div style="background-color: #00587C;
								                        width:100%; height:100px; border-bottom: solid 3px #9AC434; padding:20px 0">
								                    <table>
								                        <tr>
								                            <td width="5%"></td>
								                            <td width="60%">
								                                <h1 style="color:#fff; font-size:36px; font-weight: normal">Congratulations!</h1>
								                            </td>
								                            <td width="30%">
								                                <img src="'.base_url().'assets/img/logo.png" />
								                            </td>
								                            <td width="5%"></td>
								                        </tr>
								                    </table>
								                </div>
								            </td>
								        </tr>

								        <tr>
								            <td colspan="6" style="padding:10px 20px;">
								                <p>
								                    Dear <strong>'.$this->input->post('first_name').'</strong>,<br /><br />
								                    Thank you for registering your business with Maymall, your store details are listed below.
								                </p>
								            </td>
								        </tr>

								        <tr>
								            <td colspan="6" style="padding:10px 20px;">
								                <table width="100%">
								                    <tr>
								                        <td width="20%" height="30">Username</td>
								                        <td width="60%">
								                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px; padding: 0 10px;">
								                                <a href="javaScript:;" style="color: #fff;">'.$this->input->post('email').'</a>
								                            </div>
								                        </td>
								                        <td width="20%"></td>
								                    </tr>
								                </table>
								            </td>
								        </tr>

								        <tr>
								            <td colspan="6" style="padding:10px 20px;">
								                <div style="border: solid 1px #ccc; background-color: #f3f3f3; padding: 15px;">
								                    <p>What happens next...</p>
								                    <p>You will be contacted by a customer service agent to complete your registration.</p>
								                </div>
								            </td>
								        </tr>

								        <tr>
								            <td colspan="6" style="padding:10px 20px;">
								                <div style="background-color:#9AC434; height:40px; width:45%; text-align:center; color:#fff;
								                line-height: 40px; float: left"><a href="'.base_url().'" style="color: #fff;text-decoration: none;">LOGIN TO YOUR ACCOUNT</a></div>

								                <div style="background-color:#eec400; height:40px; width:45%; text-align:center; color:#fff;
								                line-height: 40px; float: right"><a href="'.base_url().'" style="color: #fff;text-decoration: none;">HOW IT WORKS</a></div>
								            </td>
								        </tr>

								        <tr>
								            <td colspan="6" style="padding:10px 20px;">
								                <p style="text-align: center">
								                    For more information, call 0818 778 2542, 0818 778 2542 or send an email to <a href="mailto:operator@mymall.com.ng" target="_top">operator@mymall.com.ng</a>
								                </p>
								            </td>
								        </tr>

								        <tr>
								            <td colspan="6" style="padding:10px 20px;">
								                <table width="100%">
								                    <tr>
								                        <td width="20%"><img src="'.base_url().'assets/img/ecobank.png" /></td>
								                        <td width="20%"></td>
								                        <td width="20%"></td>
								                        <td width="1%"><img src="'.base_url().'assets/img/fedex.png" height="25px" width="auto" /></td>
								                    </tr>
								                </table>
								            </td>
								        </tr>

								        <tr>
								            <td colspan="6" style="padding:10px 20px; border-top: solid 1px #ccc;">
								                <table width="100%">
								                    <tr>
								                        <td width="60%">
								                            <p style="font-size:13px; color: #333;line-height: 30px">
								                                &copy; 2015, All rights reserved. Mymall
								                            </p>
								                        </td>

								                        <td width="40%">
								                            <img src="'.base_url().'assets/img/youtube.jpg" style="float:right; margin-left:5px;" />
								                            <img src="'.base_url().'assets/img/linkin.jpg" style="float:right; margin-left: 5px;" />
								                            <a target="_blank" href="https://twitter.com/MymallNg"><img src="'.base_url().'assets/img/twitter.jpg" style="float:right; margin-left: 5px;" /></a>
								                            <a target="_blank" href="https://www.facebook.com/MymallNg"><img src="'.base_url().'assets/img/facebook.jpg" style="float:right; margin-left: 5px;" /></a>
								                        </td>
								                    </tr>
								                </table>
								            </td>
								        </tr>
								    </table>
								</div>
							   ';
					
					$msg  =( "<html>\n");
					
					$msg .=( "<head>\n");
										
					$msg .=( "</head>\n");
					
					$msg .=( "<body>\n");
					
					$msg .= ($message);
					
					$msg .=( "</body>\r\n");
					
					$msg .=( "</html>\r\n");
					
					$this->load->library('email');
					
					$config['mailtype'] = "html";
					
					$this->email->initialize($config);
					
					$this->email->from(AdminEmail, AdminEmailName);

					//$this->email->to("parmeet@geeksperhour.com", "Mymall");
					$this->email->to($this->input->post('email'), $this->input->post('first_name'));
									
					$this->email->subject('Registration Successful on Mymall - Online Store Solution');
					
					$this->email->message($msg);
					
					$this->email->send();

					$data['userArray']	= "";
					

					$data['msg'] = 'Merchant Registration Successful, Please check your mail to find out the next steps in your registration.';
				}
				else
				$data['error_msg'] = 'Merchant has not register.please try again!';
			}
			else
			{
				//$data['error_msg'] = 'Merchant has not register.please try again!';

				$existFields	= '';
				if($checkuser->first_name == $this->input->post('first_name') )
				{
					$existFields	.= 'Business name';
				}
				
				if($existFields != '')
				{
					$existFields	.= ' and ';
				}
				
				if($checkuser->email == $this->input->post('email') )
				{
					$existFields	.= 'Email';
				}

				$data['error_msg'] = $existFields.' already exist. please enter another!';

			}
		
		}
		

		
		//$this->layout->title('Ecobank WebMall - Online Store Solution | Registration');  // for dynamic title.. 
		
		$data['countries'] 			= $this->common_model->get_countries();
		$data['states_merchant'] 	= $this->common_model->get_states_merchant();
		$data['categories'] 		= $this->category_model->all_category_list(array('isactive' => 1 ));
		$data['states']				= $this->common_model->get_states("1", "name asc");
		$this->layout->css(base_url().'assets/css/validationEngine.jquery.css');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine-en.js');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine.js');
		$this->layout->view('user-registration',$data);
	}



	// ajax get state function
	public function get_state($countryId){
		
		$option  = '';
		$records = $this->common_model->get_states($countryId);
		if($records)
		{
			foreach($records as $row)
			{
				$option .='<option value="'.$row->state_id.'">'.$row->name.'</option>';
			}
			
		}
		echo $option ;
		
		
	}// End of function...
		
}
