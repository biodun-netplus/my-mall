<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Miscellaneous extends CI_Controller {

    

	function __construct()

	{

		parent::__construct();


	}


	public function terms_of_use()

	{

		$data					= array();
		$data['pageDetail'] = $this->common_model->miscellaneous(1);
		$this->layout->view('terms_of_use',$data);


	}



	public function faqs()

	{

		$data		= array();
		$this->layout->view('faqs',$data);


	}
	
	public function contact_us()

	{

		$data		= array();
		$this->layout->view('contact_us',$data);


	}
	public function subscribe()

	{
		if($this->input->post('email') != '' )
		{
			
			$data = array('email'=>$this->input->post('email') );
			$checkRow = $this->common_model->check_subscribe($data);
			if(!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL))
			{

				echo '<div class="alert alert-danger" role="alert">'.$this->input->post('email').' is an invalid email. Please enter correct!</div>';
				 
			}
			else if($checkRow)
			{

				echo '<div class="alert alert-danger" role="alert">'.$this->input->post('email'). 'is already subscribed!</div>';
				 
			}
			else
			{
				if($this->common_model->add_subscribe($data))
				echo '<div class="alert alert-success" role="alert">'.$this->input->post('email'). ' has subscribed.</div>';
			}
			
		}


	}

	


} // END of class

