<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		

	}

	public function index()
	{
		
		$keyword 	= '';
		$category 	= '';
		$merchant 	= '';
		
		$keyword 	= $this->input->get('keyword');
		if($this->input->get('category') && $this->input->get('category') != '')
		$category 	= $this->input->get('category');
		if($this->input->get('merchant') && $this->input->get('merchant') != '')
		$merchant 	= $this->input->get('merchant');
		
		$data['search']			=$this->common_model->search('','product_name',$keyword);

//        $a			=  /*(array)*/$this->common_model->search('','product_name',$keyword);
//        var_dump($a);
        /*$i = 0;
//        var_dump($a);
        $full_text_name_array = array();
        while($i <= count($a))
        {
            $full_text_name_array[$a[$i]->id]=$a[$i]->id;
            $i++;
        }

        $b			= $this->common_model->search('','product_detail',$keyword);
        $i = 0;
//        var_dump($a);
        $full_text_desc_array = array();
        while($i <= count($a))
        {
            $full_text_desc_array[$a[$i]->id]=$a[$i]->id;
            $i++;
        }

        var_dump($full_text_name_array);
        $final_name_desc_array = array_values(array_unique(array_merge($full_text_name_array , $full_text_desc_array)));
        $search_data = array();
//        var_dump($final_name_desc_array);
        $i = 0;
        foreach($final_name_desc_array as $key=>$val)
        {
            if($i < count($final_name_desc_array)-1)
            {
                $id = $val;
//            echo $id.'<br>';
                $search_data[]  = $this->common_model->search_id($id)[0];
            }

            $i++;
        }

        var_dump($search_data);*/
		$data['categoryDetail']	= $this->category_model->all_category_list(array('isactive' => 't'), '', 0);

		$this->layout->js(base_url().'assets/js/custom_js.js');
		$this->layout->css(base_url().'assets/css/main.css');


		$this->layout->view('search',$data);
	}


	public function ajax_sidebar($catId)
	{
		
		$subCatHtml		= 	$brandHtml		= 	$colourHtml		= '';
		$catId 			= 	base64_decode($catId);

		/******* get sub category by cat id *********/

		$subCategoryDetail = $this->category_model->all_subcategory_list(array('sub_category.parent_id'=>$catId), '', 0);

		if($subCategoryDetail && $subCategoryDetail != "")
        {   
            
            //$subCatHtml	.='<form action="#">';
            $countRow   = -1;
            foreach ($subCategoryDetail as $row) {

                $countRow ++;
                if($countRow <= 10)
                {

	                $subCatHtml	.='<input class="filter-type" type="checkbox" name="subCat[]" value="'.base64_encode($row->id).'"> '.$row->name.'<br>';
                }
                else
                {
                    if($countRow == 11)
                    {
                        $subCatHtml	.='<div class="show-subCate" style="display:none;">';
                    }
                    
                         
                    $subCatHtml	.='<input class="filter-type" type="checkbox" name="subCat[]" value="'.base64_encode($row->id).'"> '.$row->name.'<br>';
                    
                    if($countRow == count($subCategoryDetail)-1)
                    {
                        $subCatHtml	.='</div>';
                    }
                }
            }
            //$subCatHtml	.='</form>';

                        
        }
        if($subCategoryDetail && count($subCategoryDetail) > 10)
        {
            $subCatHtml	.='<div class="sidebar_more" myval="show-subCate">More</div>';
        
        }

        $data['subCat']		= $subCatHtml;

        /******* get brand by cat id *********/

        $brandDetail = $this->category_model->product_by_category(array('products.category'=> $catId), '', 0, 'brand');

        if($brandDetail && $brandDetail != "")
        {   
            
        	//$brandHtml		.='<form action="#">';
            $countRow   	= -1;

            foreach ($brandDetail as $row) {
                
                $countRow ++;
                if($countRow <= 10)
                {

                        
                    $brandHtml	.='<input class="filter-type" type="checkbox" name="brand[]" value="'.$row->brand.'"> '.$row->brand.'<br>';
                }
	            else
	            {
	            	if($countRow == 11) 
	              	{
	                	$brandHtml .='<div class="show-brand" style="display:none;">';
	              	}
	            
	             
                    $brandHtml	.='<input class="filter-type" type="checkbox" name="brand[]" value="'.$row->brand.'"> '.$row->brand.'<br>';
                    
                    if($countRow == count($brandDetail)-1)
                    {
                        $brandHtml	.= '</div>';
                    }
                }
            }

			//$brandHtml	.='</form>';                        
        }
        
        /******* get colour by cat id *********/          
                  
        if($brandDetail && count($brandDetail) > 10)
        {

            $brandHtml	.='<div class="sidebar_more" myval="show-brand">More</div>';
        
        }

        $data['brand']		= $brandHtml;


        $colourDetail = $this->product_model->get_product_colour_by_cat(array('category.id'=> $catId), '', 0, 'product_color.color');

        if($colourDetail && $colourDetail != "")
        {   
            
            //$colourHtml		.='<form action="#">';
            $countRow   	= -1;
            
            foreach ($colourDetail as $row) {
                
                $countRow ++;
                if($countRow <= 10)
                {

                    $colourHtml	.=  '<input class="filter-type" type="checkbox" name="colour[]" value="'.base64_encode($row->id).'"> '.$row->color.'<br>';
                }
                else
                {
	                if($countRow == 11)
	                {
	                    $colourHtml	.='<div class="show-colour" style="display:none;">';
	                }
	                      
                         
                    $colourHtml	.=  '<input class="filter-type" type="checkbox" name="colour[]" value="'.base64_encode($row->id).'"> '.$row->color.'<br>';

                    if($countRow == count($colourDetail)-1)
                    {
                        $colourHtml	.= '</div>';
                    }
                }
            }
            //$colourHtml	.='</form>';   
                        
        }
        
        if($colourDetail && count($colourDetail) > 10)
      	{
      		$colourHtml	.='<div class="sidebar_more" myval="show-colour">More</div>';
      
      	}

      	$data['colour']		= $colourHtml;
     
        echo json_encode($data) ;
 
		
	} // END of ajax_sidebar...

	public function ajax_search()
	{

	    $searchBycat = $searchByBrand = $searchByColour = $searchBySubcat =  $whereCon = $searchHtml = '';


	    if ($this->input->post('brand') && $this->input->post('brand') != '')
	    {
	       

	        $searchByBrand 	= implode("','", $this->input->post('brand'));
	        $searchByBrand 	= "'" . $searchByBrand . "'";
	        $searchByBrand 	= 'brand IN (' . $searchByBrand . ')';
	        $whereCon 		.= $searchByBrand;
    	}

	    if ($this->input->post('subCat') && $this->input->post('subCat') != '')
	    {
	        $arr 	= '';
	        foreach ($this->input->post('subCat') as $key => $value) {
	        	$arr[]	= base64_decode($value);

	        }
	        $searchBySubcat 	= implode("','", $arr);
	        $searchBySubcat 	= "'" . $searchBySubcat . "'";
	        $searchBySubcat 	= 'sub_category IN (' . $searchBySubcat . ')';

	        if ($whereCon && $whereCon != '') {
            	$whereCon .= ' AND ';
        	}

	        $whereCon 			.= $searchBySubcat;
    	}

 	    if ($this->input->post('colour') && $this->input->post('colour') != '')
	    {
	        $arr 	= '';
	        foreach ($this->input->post('colour') as $key => $value) {
	        	$arr[]	= base64_decode($value);

	        }
	        $searchByColour 	= implode("','", $arr);
	        $searchByColour 	= "'" . $searchByColour . "'";
	        $searchByColour 	= ' id IN (SELECT pid FROM product_color WHERE id IN (' . $searchByColour . ' ))';

	        if ($whereCon && $whereCon != '') {
            	$whereCon .= ' AND ';
        	}

	        $whereCon 			.= $searchByColour;
    	}

	    if ($this->input->post('type') && $this->input->post('type') != '')
	    {
	       
	    	if ($whereCon && $whereCon != '') {
            	$whereCon .= ' AND ';
        	}
	        
	        $searchBycat 	= 'category = '. base64_decode($this->input->post('type')) ;
	        $whereCon 		.= $searchBycat;
    	}

    	if ($whereCon && $whereCon != '') {
            	$whereCon .= ' AND ';
        }

        $whereCon .= " products.isactive = 't'";

    	$result	= $this->db->query(" SELECT * FROM `products` WHERE " . $whereCon);

    	if($result->num_rows() > 0)
    	{

    		$searchImg = base_url().'assets/img/logo.png';

    		foreach ($result->result() as $rows) {

    			$searchImg = base_url().'product_images/'.$rows->image;
                $productTitle   = "";
                if($rows->product_name != '')
                {
                    if(strlen($rows->product_name) > 36)
                    {
                                       
                        $productTitle = substr($rows->product_name, 0,  (32 - strlen($rows->product_name)))."...." ; 
                    }
                    else
                    $productTitle = $rows->product_name;
                }
    			$searchHtml	.='
    			<div class="product_wrap">
                  <a href="'.base_url().'product/'.base64_encode($rows->id).'">
                    <div class="image"><img src="'.$searchImg.'"></div>
                    	<div class="details">
	                        <table width="100%">
	                            <tr>
	                                <td colspan="2" style="color: #ffffff;height:42px;vertical-align: top;">'.$productTitle.'</td>
	                            </tr>
	                            <tr>
	                                <td style="color: #A8C738; font-size: 22px; font-weight: 600;">&#x20A6; '.number_format($rows->product_price).'</td>
	                                <td><input type="button" class="btn-block btn btn-cart" value="add to cart"></td>
	                            </tr>
	                        </table>
                    	</div>
                    </a>
                </div>';
 			
   			}

    	}
    	else
    	{
    		$searchHtml	= '<div class="no-record text-center">No Record Found!</div>';

    	}

  		$data['searchResult']		= $searchHtml;
  		$data['searchCount']		= $result->num_rows();

   		echo json_encode($data);


	} // END of ajax_search function...


}
