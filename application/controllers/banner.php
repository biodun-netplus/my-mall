<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->load->model('product_model');
		$this->load->model('user_model');
		$this->load->library('pagination');
	}



	public function index()
	{

		$data				= array();
		
		$data['merchantDetail']		= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));
		
		
		$data['merchantBanner'] = $this->product_model->merchant_banner($_SESSION['userDetail']->id);
			
		$this->layout->css(base_url().'assets/css/cycle2.css');
		$this->layout->js(base_url().'assets/js/jquery.cycle2.js');
		$this->layout->js(base_url().'assets/js/jquery.cycle2.carousel.js');
		
		$this->layout->script("<script type='text/javascript'> $.fn.cycle.defaults.autoSelector = '.slideshow';</script>");
		
		$this->layout->view('banner',$data);
	}


	public function add()
	{

		$image				= '';
		$error				= '';
		$data				= array();
		  
		if($this->input->post('submit') && $this->input->post('submit') != '')
		{
			
			/** check extension **/
			if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != ''){
				$extension = substr(strrchr($_FILES['image']['name'], '.'), 1);
				if (($extension!= "jpg") && ($extension!= "JPG") && ($extension != "jpeg")  && ($extension != "JPEG") && ($extension != "gif")  && ($extension != "GIF") && ($extension != "png")  && ($extension != "PNG") && ($extension != "bmp") && ($extension != "BMP") && ($extension != "epub") && ($extension != "pdf") && ($extension != "EPUB") && ($extension != "PDF")){
					$error = 1;
				}
			}
			
			if($error == '')
			{
				
				// upload 
				if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '')
				{
					$time 	= strtotime(date('Y-m-d H:i:s'));
					$ext 	= pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
					$image  = $time.'.'.$ext; 
					move_uploaded_file($_FILES['image']['tmp_name'],'./banner_images/'.$image);
					
					$config['image_library'] 	= 'gd2';
					$config['source_image'] 	= './banner_images/'.$image;
					$config['create_thumb'] 	= FALSE;
					$config['maintain_ratio'] 	= TRUE;
					$config['width']         	= 602;
					$config['height']       	= 430;
					
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
				}
				
				$checkBannerExistence = $this->product_model->merchant_banner($_SESSION['userDetail']->id );
				if($checkBannerExistence)
				{
					$bannerImage	= '';
					if($checkBannerExistence->header_banner != '')
					$bannerImage 	= $checkBannerExistence->header_banner;
					
					if($image != ''){
						
						if($bannerImage !='')
						{
							$bannerImage	.= ',';
						}
						$bannerImage	.= $image;
						
					}

					
					if($this->input->post('txtUrl') && $this->input->post('txtUrl') !='')
					{
						if($bannerImage !='')
						{
							$bannerImage	.= ',';
						}
						$bannerImage	.= $this->input->post('txtUrl');
					}
					
					
					$data =  array('header_banner' 		=> $bannerImage ,
						   	   	   'provider_id'		=> $_SESSION['userDetail']->id,
						  		   );

					
					$this->product_model->update_banner($data, $checkBannerExistence->id);
				}
				else
				{
					$bannerImage	= '';
					
					if($image != '')
					{
						if($bannerImage !='')
						{
							$bannerImage	.= ',';
						}
						$bannerImage	.= $image;
					}
					
					if($this->input->post('txtUrl') && $this->input->post('txtUrl') !='')
					{
						if($bannerImage !='')
						{
							$bannerImage	.= ',';
						}
						$bannerImage	.= $this->input->post('txtUrl');
					}
					
					$data =  array('header_banner' 		=> $bannerImage ,
						   	   	   'provider_id'		=> $_SESSION['userDetail']->id,
						  		   );
					
					$this->product_model->add_banner($data);
				}
				$data['msg'] = 'Banner has been successfully added.';
				
			}
			else
			$data['error_msg'] = 'Select banner image only jpg,gif,png,jpeg or bmp file.';
		
		}
		
		/* merchant header start */
		$data['merchantDetail']		= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));
		
		/* merchant header end */
		
		$this->layout->css(base_url().'assets/css/cycle2.css');
		$this->layout->js(base_url().'assets/js/jquery.cycle2.js');
		$this->layout->js(base_url().'assets/js/jquery.cycle2.carousel.js');
		$this->layout->css(base_url().'assets/css/validationEngine.jquery.css');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine-en.js');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine.js');
		
		$this->layout->script("<script type='text/javascript'> $.fn.cycle.defaults.autoSelector = '.slideshow'; $('#add-banner').validationEngine({ promptPosition : 'topLeft',});  $('#add-banner').click(function() { if($('#add-banner #txtUrl').val() != ''){ $('#add-banner #image').removeClass( 'validate[required]' ); $('.imageformError').hide(); } else {  $('#add-banner #image').addClass( 'validate[required]' ); $('.imageformError').show(); }  });</script>");
		
		$this->layout->view('add_banner',$data);
	}


	public function delete($source)
	{
		
		$source		= base64_decode($source); 
		
				
		if($source != '') 
		{
			
			if(strpos($source, 'url_') !== false )
			{
				$source 	= 'https://www.youtube.com/watch?v='.substr($source,4);
			
			}
			
			$merchantBanner = $this->product_model->merchant_banner($_SESSION['userDetail']->id);
			
			if($merchantBanner && $merchantBanner != '')
		  	{
				$arr 			= explode(',',$merchantBanner->header_banner); 
				$deleteSource	= '';
				
				foreach(array_keys($arr, $source) as $key)
				{
				   $deleteSource	= $arr[$key];
				   unset($arr[$key]);
				   
				}

				if($deleteSource != '')
				{
					
					$bannerImage1 = implode(',',$arr); 
					$this->product_model->update_banner(array('header_banner' => $bannerImage1),$merchantBanner->id);
					
					$_SESSION['message']	= 'Banner has been deleted.';
					
					if(strpos($deleteSource, 'youtube.com/') === false )
					{
						$unlinkImg	= './banner_images/'.$deleteSource;
						if(file_exists($unlinkImg)){
							unlink($unlinkImg);
						}
						
					}
					
				}
				else
				{
					$_SESSION['errorMessage']	= 'Banner has not deleted. Please try again!';
				}
				
			}
			
		}
		else
		{
			$_SESSION['errorMessage']	= 'Banner has not deleted. Please try again!';
		}
		
		header('location:'.base_url().'banner');
		die();
		
	}
	// end of delete function...


}

	
	