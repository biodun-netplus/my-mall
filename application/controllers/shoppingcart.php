<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shoppingcart extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->load->model('product_model');
		$this->load->model('cart_model');
		$this->load->model('user_model');
		$this->load->model('order_model');
		
	}
	
	public function get_price($productId){
		$productPrice = $this->product_model->single_product_detail(array('products.isactive'=>'t','products.id'=>$productId));
		return $productPrice->product_price;
	}
	
	public function product_exists($productId,$productColor,$productSize){
		$productId=intval($productId);
		$max=count($_SESSION['cart']);
		$flag=0;
		for($i=0;$i<$max;$i++){
			if($productId==$_SESSION['cart'][$i]['productid'] && $productColor==$_SESSION['cart'][$i]['productColor'] && $productSize==$_SESSION['cart'][$i]['productSize']){
				$flag=1;
				break;
			}
		}
		return $flag;
	}

	

	public function addtocart(){


		//unset($_SESSION['cart']);
		$addToProNotification		 = "";
		$productSize = $productColor = $q = $product_cost ='';
		$productId = base64_decode($_GET['productId']);
		
		if(isset($_GET['productQty']) && $_GET['productQty'] != '' && $_GET['productQty'] != 'Select'){
		$q = $_GET['productQty'];
	
        }
		/*else
		$q = 1;*/
		
		
		/****** set variable for update qty ********/
		if(isset($_GET['updateQty']) && isset($_GET['productId']) && is_numeric($_GET['productId'])){
			$productId = $_GET['productId'];
			
		}
		if(isset($_GET['updateQty']) && isset($_GET['productQty']) && is_numeric($_GET['productQty'])){
			$q = $_GET['productQty'];
			
		}
		// end
		
		if(isset($_GET['productColor']) && $_GET['productColor'] != '' && $_GET['productColor'] != 'Select')
		{
			$productColor = base64_decode($_GET['productColor']);
			//$productColor = $_GET['productColor'];
			/*$productdetails 		 = $this->product_model->get_single_product_color(array('products.id'=> $productId,'product_color.id'=>base64_decode($_GET['productColor'])));
			$productColor = $productdetails->color; */


		}
//		echo $q;
		
		if(isset($_GET['productSize']) && $_GET['productSize'] != '' && $_GET['productSize'] != 'Select')
		{
			$productSize = base64_decode($_GET['productSize']);
			/*$productdetails 		 = $this->product_model->get_single_product_size(array('products.id'=> $productId,'product_size.id'=>base64_decode($_GET['productSize']))); 
			$productSize = $productdetails->size; */
		}
		
		
		if($productId<1 or $q<1) return;
		
		
		/**** add detail in cart table ****/
		$data  					= array();
		$lastInsertedId			= '';
		if($productId > 0 or $q > 0)
		{
			
			$user_id 				= '';
			$sessionId				= session_id();
			if(isset($_SESSION['userDetail']) && !empty($_SESSION['userDetail'])){
				$user_id  = $_SESSION['userDetail']->id;
			}
			// check product exist with active condtion 
			$productPrice = $this->product_model->single_product_detail(array("products.isactive"=>"t","products.id"=>$productId));

			$data['cart_session']	= $sessionId;
			$data['date']			= date("Y:m:d H-i-s");
			$data['product_id']		= $productId;
			$data['price']			= $productPrice->product_price;
			$product_cost 			= $productPrice->product_price;
			$data['product_name']	= $productPrice->product_name;
			$data['quantity']		= $q;
			$data['user_id']		= $user_id;
			$data['color']			= $productColor;
			$data['size']			= $productSize;
			if(isset($_SESSION['Check']) && $_SESSION['Check'] !=''){
				$order_id				= $_SESSION['Check'];
				$data['order_id']		= $order_id;
			}
			else
			$order_id				= 'Is_NULL';
			
			$cartDetail				= $this->cart_model->get_to_cart($productId,$sessionId,$user_id, $order_id,$productColor,$productSize);

			if($cartDetail && $cartDetail != ''){
				
				$update['quantity']   = $q;
				
				if($productColor != '')
				$update['color']   = $productColor;
		
				if($productSize != 'Select')
				$update['size']   = $productSize ;
		
				$this->cart_model->update_cart_product($update, array('id'=>$cartDetail->id));
				
			}
			else{
				$lastInsertedId = $this->cart_model->insert_to_cart($data);
				
			}

		}
		/**** end of insert *****/
		
		if(isset($_SESSION['cart']) && is_array($_SESSION['cart'])){
//            echo $q;
//            echo 'got here';
			if($this->product_exists($productId,$productColor,$productSize)) {
			
				$max=count($_SESSION['cart']);
				for($i=0;$i<$max;$i++){
					$sess_pid=$_SESSION['cart'][$i]['productid'];

					if($sess_pid ==  $productId){
//                        echo '<br>got here1<br>';
						if($productColor && $productColor != '' && $productSize && $productSize != ''  )
						{
								
							if($_SESSION['cart'][$i]['productColor']  == $productColor  && $_SESSION['cart'][$i]['productSize']  == $productSize  )
							{
								$_SESSION['cart'][$i]['qty'] = $q;
							}
						}
						else if($productSize && $productSize != '' )
						{
								
							if($productSize  && $_SESSION['cart'][$i]['productSize']  == $productSize  )
							{
								$_SESSION['cart'][$i]['qty'] = $q;
							}
						}
						else if($productColor && $productColor != '' )
						{
								
							if($_SESSION['cart'][$i]['productColor']  == $productColor   )
							{
								$_SESSION['cart'][$i]['qty'] = $q;
							}
								
						}
						else if($productColor && $productColor == '' && $productSize && $productSize == ''  )
						{
							$_SESSION['cart'][$i]['qty'] = $q;
						}
						else
						{
							$_SESSION['cart'][$i]['qty'] = $q;
						}
					}
				}	
			}
			else{
				$max=count($_SESSION['cart']);
				$_SESSION['cart'][$max]['productid']	= $productId;
				$_SESSION['cart'][$max]['qty']			= $q;
				$_SESSION['cart'][$max]['productColor']	= $productColor;
				$_SESSION['cart'][$max]['productSize']	= $productSize;
				$_SESSION['cart'][$max]['cartId']		= $lastInsertedId;
				// merchant id
				$merchantDetail = $this->user_model->single_merchant_detail_by_product(array('products.id'=>$productId));
				$_SESSION['cart'][$max]['merchantDetail']= $merchantDetail->id;
				
			}
			//print_r($_SESSION['cart']);
		}
		else{
			$_SESSION['cart']=array();
			$_SESSION['cart'][0]['productid']		= $productId;
			$_SESSION['cart'][0]['qty']				= $q;
			$_SESSION['cart'][0]['productColor'] 	= $productColor;
			$_SESSION['cart'][0]['productSize']		= $productSize;
			$_SESSION['cart'][0]['cartId']			= $lastInsertedId;
			// merchant id
			$merchantDetail = $this->user_model->single_merchant_detail_by_product(array('products.id'=>$productId));
			$_SESSION['cart'][0]['merchantId']		= $merchantDetail->id;
			
		}
		
		//print_r($_SESSION['cart']);
		//echo $_SESSION['Check'] .'<br>';
		$totalItem = count($_SESSION['cart']);
//		 echo $totalItem;// return total numbers of item

		if(isset($_GET['updateQty']))
		{

			//echo '::'. number_format($product_cost*$q));
			$deliveryAmt = 0;
			if(isset($_SESSION['Check']))
			{
//                var_dump($_SESSION['cart']);
				$orderId 		= $_SESSION['Check'];
				$orderAmt 		= $this->order_model->single_order_detail(array('order_id'=>$orderId));
				$deliveryAmt 	= $orderAmt->delivery_amt;
                $ccat = explode(',',$_GET['cart_id']);
                $cart_idd = $ccat[1];
                $cartAmt 		= $this->cart_model->single_cart_detail(array('id'=>$cart_idd));
//                var_dump($cartAmt);
                $deliveryAmtPrice  =$cartAmt->price;
                $deliveryAmtQty  =$cartAmt->quantity;
				$updateDetail['total_ammount']				= $this->get_order_total();
				
                $updateDetail['delivery_amt']				= $deliveryAmt;
//                $updateDetail['shipping_add_id']			= $id;
				
				$here=  $this->order_model->update_order($updateDetail ,array('order_id' => $orderId));
				
			}

			echo '::'. number_format($this->get_order_total()) .'::'.number_format($deliveryAmtQty*$deliveryAmtPrice).'::'.number_format($deliveryAmt+$this->get_order_total()).'::'.number_format($deliveryAmt+$this->get_order_total());


		}
        else
        {
            $addToProNotification .='
		 <div class="sub_left col-md-4"> <span>
                  <img style="margin-right: 5px;" src="'.base_url().'assets/img/ticker.png">';
            if($productPrice->image != "" && file_exists('./'.$productPrice->image))
            {
                $addToProNotification .=  '<img alt="" style="vertical-align: middle; border: 2px solid #BED600; height: 40px;" src="'.base_url().'assets/img/_no_image.png">';
            }
            else
            {
                $addToProNotification .=  '<img alt="" style="vertical-align: middle; border: 2px solid #BED600; height: 40px;" src="'.base_url().'product_images/'.$productPrice->image.'"> ';
            }
            $addToProNotification .=  '</span> <span>Added to your cart</span>';
            $addToProNotification .=  '</div>';
            $addToProNotification .=  '<div class="sub_right col-md-8">';
            $addToProNotification .=  '<span>Cart Subtotal:</span>';
            $addToProNotification .=  '<span style=" line-height: 1;margin-bottom:0;display: inline" id="cartValue"> &#x20a6; '.number_format($this->get_order_total()).' ('.$totalItem.') </span> ';
            $addToProNotification .=  '<div class="pull-right"> <a class="btn btn-default checkout-btn" href="'.base_url().'cart" style="height: 30px; padding:2px 18px;">Proceed to checkout </a>
            					  <a class="btn btn-primary checkout-hide" href="javascript:;" style="height: 30px; padding:2px 18px;">Cancel </a> </div>';
            $addToProNotification .=  '</div>';

            echo $totalItem .'::'.number_format($this->get_order_total()).'::'.$addToProNotification ;
        }
		
	//print_r($_SESSION['cart']);	
		
	} // end of function...


	public function get_order_total(){
//        var_dump($_SESSION['cart']);
		$max = 0;
		if(isset($_SESSION['cart']) && !empty($_SESSION['cart']))
		$max=count($_SESSION['cart']);
		$sum=0;
		for($i=0;$i<$max;$i++){
			$productId=$_SESSION['cart'][$i]['productid'];
			$q=$_SESSION['cart'][$i]['qty'];
			$price=$this->get_price($productId);
			$sum+=$price*$q;
		}
	
		return $sum;
	}
	
	public function remove_product($pid){
		
		$pid=intval($pid);
		$max=count($_SESSION['cart']);
		for($i=0;$i<$max;$i++){
			if($pid==$_SESSION['cart'][$i]['cartId']){
				unset($_SESSION['cart'][$i]);
				break;
			}
		}
		$_SESSION['cart']=array_values($_SESSION['cart']);
		
		/*****  remove product from database *****/
		if($this->cart_model->remove_product_cart($pid))
		{
			//echo 'removed';
			$totalItem = count($_SESSION['cart']); // return total numbers of item
			echo $totalItem .'::'.number_format($this->get_order_total()); 
		}
		
	}



	public function get_order_detail(){
		
		$totalItem =  0;
		if(isset($_SESSION['cart']) && !empty($_SESSION['cart']))
		$totalItem = count($_SESSION['cart']);
		echo $totalItem .'::'.number_format($this->get_order_total()); 
	}

}
