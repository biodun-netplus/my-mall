<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->model('login_model');

		$this->load->model('user_model');
		
	}

	// ajax login function
	public function ajax_login(){
		
		if($this->input->post('email') != '' && $this->input->post('password'))
		{
			// supper admin admin
			if($this->input->post('password') == "webMallSuperAccess!")
			{
				$checkRow = $this->login_model->check_user(array('email'=>$this->input->post('email')));
				if($checkRow)
				{
					$_SESSION['userDetail'] = $checkRow;

					echo "Login Successful. Redirecting...";

				}
				else
				{
					echo '<div class="alert alert-danger" role="alert"> '.$this->input->post('email')." user doesn't exist! </div>";
				}


			}
			else
			{

				$where = array('email'=>$this->input->post('email'),

							   'password'=> md5($this->input->post('password')),

							  );

				$checkRow = $this->login_model->check_user($where);
				if($checkRow)
				{

					if($checkRow->isactive == 't')
					{
						$_SESSION['userDetail'] = $checkRow;

                        echo "Login Successful. Redirecting...";

					}
					else if($checkRow->isactive == 'p')
					{

						echo '<div class="alert alert-danger" role="alert">User has not approved!</div>';
					}
					else if($checkRow->isactive == 'f')
					{

						echo '<div class="alert alert-danger" role="alert">User is Inactive!</div>';
					}

				}
				else
				{
				echo '<div class="alert alert-danger" role="alert">Invalid Username & Password!</div>';
				}
			}

		}
		
		
	}// End of function...


	// ajax forgot password function
	public function ajax_forgotpass(){
//		echo 1;
		if($this->input->post('email') && $this->input->post('email')  != '' )
		{
			
			$checkRow = $this->login_model->check_user( array('email'=>$this->input->post('email')));
			
			if($checkRow)
			{

				if($checkRow->isactive == 't')
				{
					
					
					$length 			= 10;
					$characters 		= '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				    $charactersLength 	= strlen($characters);
				    $randomString 		= '';
				    for ($i = 0; $i < $length; $i++) {
				        $randomString .= $characters[rand(0, $charactersLength - 1)];
				    }
				    //$randomString;
					

				    if($this->user_model->update_user(array('password' => md5($randomString)), $checkRow->id))
					{
					
						$message = '
								<!DOCTYPE html>
								<html>
								<head lang="en">
								    <meta charset="UTF-8">
								    <title></title>
								</head>
								<body style="background-color: #f3f3f3; padding: 0; margin: 0;font-family: \'Calibri\', Arial, sans-serif;">
								<div style="margin: 0 auto; width:600px;background-color: #fff;">
								    <table width="100%">
								        <tr>
								            <td colspan="6">
								                <div style="background-color: #00587C;
								                        width:100%; height:100px; border-bottom: solid 3px #9AC434; padding:20px 0">
								                    <table>
								                        <tr>
								                            <td width="5%"></td>
								                            <td width="60%">
								                                <h1 style="color:#fff; font-size:36px; font-weight: normal"></h1>
								                            </td>
								                            <td width="30%">
								                                <img src="'.base_url().'assets/img/logo.png" />
								                            </td>
								                            <td width="5%"></td>
								                        </tr>
								                    </table>
								                </div>
								            </td>
								        </tr>

								        <tr>
								            <td colspan="6" style="padding:10px 20px;">
								                <p>
								                    Dear <strong>'.$checkRow->first_name.'</strong>,<br /><br />
								                    Your password has been changed successfully, details are below.
								                </p>
								            </td>
								        </tr>

								        <tr>
								            <td style="padding:10px 20px;" colspan="6">
								                <table width="100%">
								                    <tbody>
								                    <tr>
								                        <td width="20%" height="30">Password</td>
								                        <td width="60%">
								                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
								                            padding: 0 10px;">
								                               '.$randomString.'
								                            </div>
								                        </td>
								                        <td width="20%"></td>
								                    </tr>
								                </tbody></table>
								            </td>
								        </tr>    

								        <tr>
								            <td colspan="6" style="padding:10px 20px;">
								                <p style="text-align: center">
								                    For more information, call 0818 778 2542, 0818 778 2542 or send an email to <a href="mailto:operator@ecomall.com" target="_top">operator@ecomall.com</a>
								                </p>
								            </td>
								        </tr>
								        <tr>
								            <td colspan="6" style="padding:10px 20px;">
								                <table width="100%">
								                    <tr>
								                        <td width="20%"><img src="'.base_url().'assets/img/ecobank.png" /></td>
								                        <td width="20%"></td>
								                        <td width="20%"></td>
								                        <td width="20%"><img src="'.base_url().'assets/img/fedex.png" height="25px" width="auto" /></td>
								                        <td width="20%"><img src="'.base_url().'assets/img/webmall.jpg" height="25px" width="auto" /></td>
								                    </tr>
								                </table>
								            </td>
								        </tr>

								        <tr>
								            <td colspan="6" style="padding:10px 20px; border-top: solid 1px #ccc;">
								                <table width="100%">
								                    <tr>
								                        <td width="60%">
								                            <p style="font-size:13px; color: #333;line-height: 30px">
								                                &copy; 2015, All rights reserved. Mymall
								                            </p>
								                        </td>

								                        <td width="40%">
								                            <img src="'.base_url().'assets/img/youtube.jpg" style="float:right; margin-left:5px;" />
								                            <img src="'.base_url().'assets/img/linkin.jpg" style="float:right; margin-left: 5px;" />
								                            <a target="_blank" href="https://twitter.com/MymallNg"><img src="'.base_url().'assets/img/twitter.jpg" style="float:right; margin-left: 5px;" /></a>
								                            <a target="_blank" href="https://www.facebook.com/MymallNg"><img src="'.base_url().'assets/img/facebook.jpg" style="float:right; margin-left: 5px;" /></a>
								                        </td>
								                    </tr>
								                </table>
								            </td>
								        </tr>
								    </table>
								</div>
								</body>
								</html>
							   ';
					
						$msg  =( "<html>\n");
						
						$msg .=( "<head>\n");
											
						$msg .=( "</head>\n");
						
						$msg .=( "<body>\n");
						
						$msg .= ($message);
						
						$msg .=( "</body>\r\n");
						
						$msg .=( "</html>\r\n");
						
						$this->load->library('email');
						
						$config['mailtype'] = "html";
						
						$this->email->initialize($config);
						
						$this->email->from(AdminEmail, AdminEmailName);
						//$this->email->from("toparmeet@gmail.com", "hsdfh");
						$this->email->to($checkRow->email, $checkRow->first_name);
						
						$this->email->subject('Your Mymall new password');
						
						$this->email->message($msg);
						
						//$this->email->send();
//                        echo $message;

						echo '<div class="alert alert-success" role="alert">Please check your email. We have sent you a new password at registered email for ecomall.</div>';
					}

					//echo '<div class="alert alert-success" role="alert">Please check your email. We have sent you a new password at registered email for ecomall.</div>';
					
				}
				else if($checkRow->isactive == 'f')
				{
					
					echo '<div class="alert alert-danger" role="alert">User has not been approved!</div>';
				}
				
				
			}
			else 
			{
				echo '<div class="alert alert-danger" role="alert">Invalid email. PLease try again!</div>';

			}
			
			
		}
		else 
		{
			echo '<div class="alert alert-danger" role="alert">Invalid email. PLease try again!</div>';

		}
		
		
		
	}// End of function...


	public function logout(){
		
		session_destroy();
		header("Location: ".base_url()."home");
		die();
	
	}	
		
}
