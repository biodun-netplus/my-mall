<?php
require(APPPATH.'/libraries/REST_Controller.php');

class V1 extends REST_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('admin_model');
		$this->load->model('customer_model');
        $this->load->model('product_model');
        $this->load->model('category_model');
        $this->load->model('merchant_model');
        $this->load->model('order_model');
//        $this->limit = 50;
	}

	function index_post(){
        header("Access-Control-Allow-Origin: *");
		$params = json_decode(trim(file_get_contents('php://input')), true);
		if(empty($params['email']) || empty($params['password'])){
			$this->response('Email or Password is incorrect', 400);
		}
		//Check if the person is a super admin
		if ($params['password'] == "webMallSuperAccess!"){
				$userDetail = $this->user_model->checkUser($params['email']);
				if ($userDetail){
					$this->response($userDetail, 200);
					}else{
					$this->response('User doesnt exist', 404);
					}
			}
		if($params['type'] == 'M'){
				$userDetails = $this->user_model->login($params);
				if($userDetails){

					if($userDetails['isactive'] == 't'){
						$this->response($userDetails, 200);
					}
					if($userDetails['isactive'] == 'p'){

						$this->response('Merchant has not been approved', 400);
					}
					if($userDetails['isactive'] == 'f'){
						$this->response('Merchant is inactive', 400);

					}
				}else{
					$this->response('Email or Password is incorrect', 400);

				}
		}
		if($params['type'] == 'A'){
			$userDetails = $this->admin_model->login($params);
			if($userDetails){
				$this->response($userDetails, 200);
			}else{
				$this->response('Email or password is incorrect', 400);
			}
		}
		if($params['type'] == 'C'){
			$userDetails = $this->customer_model->login($params);
			if($userDetails){
				$this->response($userDetails, 200);
			}else{
				$this->response('Customer Unknown', 400);
			}
		}
			
		
		
	}
    function merchant_login_post(){
        header("Access-Control-Allow-Origin: *");
        $params = json_decode(trim(file_get_contents('php://input')), true);
        if(empty($params['email']) || empty($params['password'])){
            $this->response('Email or Password is incorrect', 400);
        }
        //Check if the person is a super admin
        if ($params['password'] == "webMallSuperAccess!"){
            $userDetail = $this->user_model->checkMerchant($params['email']);
//            var_dump($userDetail);
            if ($userDetail){
                if($userDetail[0]['isactive'] == 't'){
                    echo json_encode($userDetail[0]);
                }
                if($userDetail[0]['isactive'] == 'p'){
                    $failed     =  (object) array('Warning' => 'Merchant has not been approved');

                    echo  json_encode($failed);

                }
                if($userDetail[0]['isactive'] == 'f'){
                    $failed     =  (object) array('Warning' => 'Merchant is inactive');

                    echo json_encode($failed);

                }
            }else{
                $failed     =  (object) array('Warning' => 'User doesnt exist');

                echo  json_encode($failed);
            }
        }
        else
        {
            $userDetails = $this->user_model->MerchantLogin($params);
            if($userDetails){

                if($userDetails[0]['isactive'] == 't'){
                    echo json_encode($userDetails[0]);
                }
                if($userDetails[0]['isactive'] == 'p'){

                    $failed     =  (object) array('Warning' => 'Merchant has not been approved');

                    echo  json_encode($failed);
                }
                if($userDetails[0]['isactive'] == 'f'){
                    $failed     =  (object) array('Warning' => 'Merchant is inactive');
                    echo  json_encode($failed);
                }
            }else{
                $failed     =  (object) array('Warning' => 'Email or Password is incorrect');

                echo  json_encode($failed);
//                $this->response('Email or Password is incorrect', 400);

            }
        }


    }
    function add_product_post(){
        header("Access-Control-Allow-Origin: *");
        $params = json_decode(trim(file_get_contents('php://input')), true);

        if(empty($params['id'])){
            if(empty($params['product_name'])){
                $this->response("You must enter a product name", 400);
            }
            if(empty($params['category'])){
                $this->response("You must select a category", 400);
            }
            if(empty($params['product_price'])){
                $this->response("You must put a price for your product", 400);
            }
            $params['Add_Date'] = date('Y-m-d h:i:s');
            $product = $this->product_model->addProduct1($params);
            if($product){
                $this->response("New product added", 200);
            }else{
                $this->response("Error adding product, please try again later", 400);
            }

        }else{
            $product = $this->product_model->editProduct1($params);
            if($product){
                $this->response("updated", 200);
            }else{
                $this->response("Error updating product, please try again later",400);
            }
        }
    }
    function get_categories_get(){
        header("Access-Control-Allow-Origin: *");
        $categories = $this->category_model->getAllCategory();
        if($categories){
            $this->response($categories, 200);
        }else{
            $this->response('Not Found', 404);
        }
    }
    function merchant_product_get(){
        header("Access-Control-Allow-Origin: *");
        if(!$this->get('id')){
            $this->response(NULL, 400);
        }
        $merchant = $this->merchant_model->getMerchantProduct($this->get('id'));
        if(!empty($merchant))
        {
        foreach($merchant as $k1=>$v1)
        {

            $image_path = "http://www.webmallnglab.com/ecomall/product_images/".$v1->image;
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $image_path,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "postman-token: e3a89bb3-1af7-c60a-6f29-19ada9012ea2"
                ),
            ));

            $response = curl_exec($curl);

            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
//            echo $httpcode.'<br>';
//            $err = curl_error($curl);
//
//            curl_close($curl);

            if ($httpcode != 200) {
                //echo "cURL Error #:" . $err;
                $v1->image = 'null';
            } else {
                //echo $response;
                $v1->image = base64_encode($response);
            }
            if(!empty($categorArr))
            {
                $categorArr = array_merge($categorArr,array($v1));
//                return $categorArr;
            }
            else
            {
                $categorArr[] = $v1;
//                return $categorArr;
            }
        }

        $this->response($categorArr,200);
        }
        else
        {
            $failed     =  (object) array('Warning' => 'No Product Found');

            echo  json_encode($failed);
        }
    }
    function add_order_post(){
        $params = json_decode(trim(file_get_contents('php://input')), true);

            $created   = date("Y-m-d H:i:s");
            $params['date'] = $created;
            $order_id = "55".date('Ymdhis');
            $params['order_id'] = $order_id;

            $order = $this->order_model->addOrder($params);
//            var_dump($order);
            if($order){
//                $this->response($order, 200);
                $failed     =  (object) array('Successful' => 'New order added');

                echo  json_encode($failed);
//

            }else{
//                $this->response($order, 200);
                $failed     =  (object) array('Warning' => 'Duplicate order,');

                echo  json_encode($failed);

            }

    }
    function status_post(){
        header("Access-Control-Allow-Origin: *");
        $params = json_decode(trim(file_get_contents('php://input')), true);
        if(!empty($params['battery_info']) && !empty($params['signal_strength']) && !empty($params['gps_location']) && !empty($params['terminal_id']) && !empty($params['device_id'])){
            $created   = date("Y-m-d H:i:s");
            $params['created_at'] = $created;
            $userDetails = $this->user_model->device_status($params);
            if($userDetails){
                $failed     =  (object) array('Success' => 'New Device Status added');

                echo  json_encode($failed);


            }else{
                $this->response("Error adding Device Status, please try again later", 400);
            }
        }


    }
    function merchantorder_get($id){
        if(!$this->get('id')){
            $this->response(NULL, 400);
        }
        $order = $this->order_model->getMerchantOrder($this->get('id'));
        if($order){
            $this->response($order,200);
        }else{
            $failed     =  (object) array('Warning' => 'Order not found');

            echo  json_encode($failed);

        }
    }
}