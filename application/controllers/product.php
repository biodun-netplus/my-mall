<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();

		ini_set('memory_limit', '-1');
		$this->load->model('product_model');
		$this->load->model('user_model');
		$this->load->model('home_model');
	}
	public function validate()
	{
	    $this->load->view('godaddy.html');
	}

	public function detail($pro)
	{
	
		$productDetail  		 = $this->product_model->single_product_detail(array('products.isactive'=>'t','products.id'=>base64_decode($pro)));
		if($productDetail)
		{
			
			// update viewed count start...
			$this->product_model->update_product(array('viewed' => ($productDetail->viewed + 1)), $productDetail->id);
			// viewed count end...

			$data['productDetail']	 = $productDetail;  
			$merId					 = $productDetail->pid; 

			//$data['productColor']	 = $this->product_model->product_color(array('product_color.pid'=>base64_decode($pro)));
			//$data['productSize']	 = $this->product_model->product_size(array('product_size.pid'=>base64_decode($pro)));

			// getting attribute of product 
			$productSizeColour 	= $this->product_model->get_product_size_colour_by_product(array('relation_color_size.product_id' => base64_decode($pro)));

			$pColor 			= array();
			$pSize 				= array();
			$pQty 				= 0;
			$flagQty 			= 0; 
			//echo "<!--- "; print_r($productSizeColour); echo "-->";

			if($productSizeColour && $productSizeColour  != '')
        	{
            
            	foreach($productSizeColour as $rows)
            	{
            		if($rows->size_id != "")
            		{
            			$res = $this->product_model->get_single_product_size(array('product_size.id' => $rows->size_id));
            			//echo "<!--- "; print_r($res); echo "-->"; 
            			if($res && !empty($res))
            			$pSize[] = $res;

            		}
            		if($rows->colour_id != "")
            		{
            			$res = $this->product_model->get_single_product_color(array('product_color.id' => $rows->colour_id)); 
            			if($res && !empty($res))
                            if(!in_array($res,$pColor))
                            {
                                $pColor[] = $res;
                            }


            		}
            	}
            	if($flagQty == 0 )
            	{
            		$pQty = $rows->qty;
            		$flagQty ++;

            	}
            }
            $data['productColor']  = $pColor;
            $data['productSize']   = $pSize; 
            $data['productQty']    = $pQty;

			//$data['merchantDetail']	 = $this->user_model->single_merchant_detail(array('category.isactive'=>'t','users.id'=>$merId));
		        $data['merchantDetail']	 = $this->user_model->single_merchant_detail(array('users.isactive'=>'t','users.id'=>$merId));
			
			$data['relatedProducts'] = $this->product_model->related_product_by_id(array('pid' => $merId , 'isactive' => 't'));
			
			//$this->layout->js(base_url().'assets/js/stars.js');
			$this->layout->css(base_url().'assets/css/owl.carousel.css');
			$this->layout->css(base_url().'assets/css/owl.theme.css');
			$this->layout->js(base_url().'assets/js/owl.carousel.min.js');
			$this->layout->js(base_url().'assets/js/custom_js.js');
			$this->layout->view('product',$data);
		}
		else
		{
			$this->layout->css(base_url().'assets/css/main.css');
			$data['productDetail']  = $this->home_model->all_product(array('isactive' => 't'), 8);
			$this->layout->view("my404_view",$data);
		}
	}

	public function index()

	{

		$merchantDetail			 	= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail']->id ));  
		
		$data['merchantDetail']	 	= $merchantDetail; 

		$data['productsDetail'] 	= $this->product_model->related_product_by_id(array('pid' => $_SESSION['userDetail']->id) );
	

		$this->layout->view('productList',$data);

	}

	public function add()
	{

		$image1	=	$image2	=	$image3	= '';
		  
		if($this->input->post('submit') && $this->input->post('submit') != '')
		{
			
			
			/** check extension **/
			
			$image1Flage = 0;
			$image2Flage = 0;
			$image3Flage = 0;
			if(isset($_FILES['txtImage1']['name']) && $_FILES['txtImage1']['name'] != ''){
				$extension = substr(strrchr($_FILES['txtImage1']['name'], '.'), 1);
				if (($extension!= "jpg") && ($extension!= "JPG") && ($extension != "jpeg")  && ($extension != "JPEG") && ($extension != "gif")  && ($extension != "GIF") && ($extension != "png")  && ($extension != "PNG") && ($extension != "bmp") && ($extension != "BMP")){
					$image1Flage = 1;
				}
			}
			
			
			if(isset($_FILES['txtImage2']['name']) && $_FILES['txtImage2']['name'] != ''){
				$extension = substr(strrchr($_FILES['txtImage2']['name'], '.'), 1);
				if (($extension!= "jpg") && ($extension!= "JPG") && ($extension != "jpeg")  && ($extension != "JPEG") && ($extension != "gif")  && ($extension != "GIF") && ($extension != "png")  && ($extension != "PNG") && ($extension != "bmp") && ($extension != "BMP")){
					$image2Flage = 1;
				}
			}
			
			
			if(isset($_FILES['txtImage3']['name']) && $_FILES['txtImage3']['name'] != ''){
				$extension = substr(strrchr($_FILES['txtImage3']['name'], '.'), 1);
				if (($extension!= "jpg") && ($extension!= "JPG") && ($extension != "jpeg")  && ($extension != "JPEG") && ($extension != "gif")  && ($extension != "GIF") && ($extension != "png")  && ($extension != "PNG") && ($extension != "bmp") && ($extension != "BMP")){
					$image3Flage = 1;
				}
			}
			
			
			
			if($image1Flage == 1){
				$data['error_msg']		= 'Please select image type png,jpg,jpeg,gif,bmp for image 1.';
			}
			else if($image1Flage == 1){
				$data['error_msg']		= 'Please select image type png,jpg,jpeg,gif,bmp for image 2.';
			}
			else if($image2Flage == 1){
				$data['error_msg']		= 'Please select image type png,jpg,jpeg,gif,bmp for image 3.';
			}
			else
			{

				$image1	= $image2 = $image3	= '';


				if(isset($_FILES['txtImage1']['name']) && $_FILES['txtImage1']['name'] != '')
				{
					$time 	= strtotime(date('Y-m-d H:i:s'));
					$ext 	= pathinfo($_FILES['txtImage1']['name'], PATHINFO_EXTENSION);
					$image1  = $time.'.'.$ext; 
					move_uploaded_file($_FILES['txtImage1']['tmp_name'],'./product_images/'.$image1);

					$this->load->library('image_lib');
					
					$config['source_image'] = './product_images/'.$image1;
				
					$config['width'] = 400;
					
					$config['height'] = 400;
					
					$config['maintain_ratio'] = FALSE;
					
					$config['new_image'] = './product_images/'.$image1;
					
					$flag_for_image = 0;
					
					$this->image_lib->initialize($config);
					
					if(!$this->image_lib->resize()){
					
						$flag_for_image = 1;
					
					} 	
					
					$this->image_lib->clear();

				}
				if(isset($_FILES['txtImage2']['name']) && $_FILES['txtImage2']['name'] != '')
				{
					$time 	= strtotime(date('Y-m-d H:i:s'));
					$ext 	= pathinfo($_FILES['txtImage2']['name'], PATHINFO_EXTENSION);
					$image2  = $time.'2.'.$ext; 
					move_uploaded_file($_FILES['txtImage2']['tmp_name'],'./product_images/'.$image2);

					$this->load->library('image_lib');
					
					$config['source_image'] = './product_images/'.$image2;
				
					$config['width'] = 400;
					
					$config['height'] = 400;
					
					$config['maintain_ratio'] = FALSE;
					
					$config['new_image'] = './product_images/'.$image2;
					
					$flag_for_image = 0;
					
					$this->image_lib->initialize($config);
					
					if(!$this->image_lib->resize()){
					
						$flag_for_image = 1;
					
					} 	
					
					$this->image_lib->clear();


				}
				if(isset($_FILES['txtImage3']['name']) && $_FILES['txtImage3']['name'] != '')
				{
					$time 	= strtotime(date('Y-m-d H:i:s'));
					$ext 	= pathinfo($_FILES['txtImage3']['name'], PATHINFO_EXTENSION);
					$image3  = $time.'3.'.$ext; 
					move_uploaded_file($_FILES['txtImage3']['tmp_name'],'./product_images/'.$image3);

					$this->load->library('image_lib');
					
					$config['source_image'] = './product_images/'.$image3;
				
					$config['width'] = 400;
					
					$config['height'] = 400;
					
					$config['maintain_ratio'] = FALSE;
					
					$config['new_image'] = './product_images/'.$image3;
					
					$flag_for_image = 0;
					
					$this->image_lib->initialize($config);
					
					if(!$this->image_lib->resize()){
					
						$flag_for_image = 1;
					
					} 	
					
					$this->image_lib->clear();

				}

				$checkStockShipping	=	$checkEligibleStock 	= '1';

				if($this->input->post('checkStockShipping') != '')
				{
						$checkStockShipping	= $this->input->post('checkStockShipping');

				}
				if($this->input->post('checkEligibleStock') != '')
				{
						$checkEligibleStock	= $this->input->post('checkEligibleStock');

				}

				//print_r($_POST); die();

				//'currency' 			=> $this->input->post('txtCurrency'),
				//'product_quantity' 	=> $this->input->post('txtQuantity'),
				$data =  array('product_name' 		=> $this->input->post('txtName'),
							  'list_price' 			=> $this->input->post('txtListPrice'),
							  'product_price' 		=> $this->input->post('txtOurPrice'),
							  'currency' 			=> 'Naira',
							  'product_quantity' 	=> 0,
							  'brand' 				=> $this->input->post('txtBrand'),
							  'product_detail' 		=> $this->input->post('txtDetails'),
							  'category' 			=> $this->input->post('selCategory'),
							  //'sub_category' 		=> $this->input->post('txtSubCategory'),
							  'stock_shipping' 		=> $checkStockShipping,
							  'stock_pickup' 		=> $checkEligibleStock,
							  'image' 				=> $image1,
							  'image2' 				=> $image2,
							  'image3' 				=> $image3,
							  'pid' 				=> $_SESSION['userDetail']->id ,
							  'isactive' 			=> 'p',
							  'Add_Date'			=> date("Y:m:d H-i-s")
							);
				$productLastInsertedId = $this->product_model->add_product($data);
				if($productLastInsertedId)
				{
					
					//$data['msg'] = 'Product has been inserted successfully.';
					//header("Location: ".base_url()."profile");
					// send mail


					$message = '
					<body style="background-color: #f3f3f3; padding: 0; margin: 0;font-family: \'Calibri\', Arial, sans-serif;">
					<div style="margin: 0 auto; width:600px;background-color: #fff;">
					    <table width="100%">
					        <tr>
					            <td colspan="6">
					                <div style="background-color: #00587C;
					                        width:100%; height:100px; border-bottom: solid 3px #9AC434; padding:20px 0">
					                    <table>
					                        <tr>
					                            <td width="5%"></td>
					                            <td width="60%">
					                                <h1 style="color:#fff; font-size:36px; font-weight: normal"></h1>
					                            </td>
					                            <td width="30%">
					                                <img src="'.base_url().'assets/img/logo.png" />
					                            </td>
					                            <td width="5%"></td>
					                        </tr>
					                    </table>
					                </div>
					            </td>
					        </tr>
					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <p>
					                    Dear <strong>Admin</strong>,<br /><br />
					                    New product has been added.
					                    Here is link : <a href="'.base_url().'admin/edit_product.php?edit_id='.$productLastInsertedId.'">'.base_url().'admin/edit_product.php?edit_id='.$productLastInsertedId.'</a> 
					                </p>
					            </td>
					        </tr>
					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <table width="100%">
					                    <tr>
					                        <td width="20%"><img src="'.base_url().'assets/img/ecobank.png" /></td>
					                        <td width="20%"></td>
					                        <td width="20%"></td>
					                        <td width="1%"><img src="'.base_url().'assets/img/fedex.png" height="25px" width="auto" /></td>
					                    </tr>
					                </table>
					            </td>
					        </tr>
					        <tr>
					            <td colspan="6" style="padding:10px 20px; border-top: solid 1px #ccc;">
					                <table width="100%">
					                    <tr>
					                        <td width="60%">
					                            <p style="font-size:13px; color: #333;line-height: 30px">
					                                &copy; 2015, All rights reserved. Mymall
					                            </p>
					                        </td>
					                        <td width="40%">
					                            <img src="'.base_url().'assets/img/youtube.jpg" style="float:right; margin-left:5px;" />
					                            <img src="'.base_url().'assets/img/linkin.jpg" style="float:right; margin-left: 5px;" />
					                            <a target="_blank" href="https://twitter.com/MymallNg"><img src="'.base_url().'assets/img/twitter.jpg" style="float:right; margin-left: 5px;" /></a>
					                            <a target="_blank" href="https://www.facebook.com/MymallNg"><img src="'.base_url().'assets/img/facebook.jpg" style="float:right; margin-left: 5px;" /></a>
					                        </td>
					                    </tr>
					                </table>
					            </td>
					        </tr>
					    </table>
					</div>
				   ';


					$emailList = array('ayomide@webmallng.com');

					foreach ($emailList as $address)
					{
					
						
						$msg  =( "<html>\n");
						
						$msg .=( "<head>\n");
											
						$msg .=( "</head>\n");
						
						$msg .=( "<body>\n");
						
						$msg .= ($message);
						
						$msg .=( "</body>\r\n");
						
						$msg .=( "</html>\r\n");
						
						$this->load->library('email');
						
						$config['mailtype'] = "html";
						
						$this->email->initialize($config);
						
						$this->email->clear();

						$this->email->from(AdminEmail, AdminEmailName);
										
						$this->email->to($address);

						// $this->email->cc("mayowa@netplusadvisory.com");
			
						$this->email->subject('New product added');
						
						$this->email->message($msg);
						
					//	$this->email->send(); 
				    }
					header("Location: ".base_url()."product-colour/".base64_encode($productLastInsertedId));
					die();
				}
				else
				$data['error_msg'] = 'Product has not inserted. Please try again!';

			}
	
		
		}
		
		$data['cateDetail']			= $this->category_model->get_categories();
		/* merchant header start */
		$merchantDetail				= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));
		$data['merchantDetail']		= $merchantDetail;
		
		$data['merchantBanner']  	= $this->product_model->merchant_banner($_SESSION['userDetail']->id);	
		/* merchant header end */
		
		$this->layout->css(base_url().'assets/css/validationEngine.jquery.css');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine-en.js');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine.js');
		// editor
		$this->layout->css(base_url().'assets/css/editor.css');
		$this->layout->js(base_url().'assets/js/editor.js');

		$this->layout->script("<script type='text/javascript'> $('#add-product').validationEngine({ promptPosition : 'topLeft',});$('#txtDetails').Editor(); $('form#add-product').submit(function(){ $('#txtDetails').val($('.Editor-editor').html()); }); </script>");
		
		$this->layout->view('add_product',$data);
	}

	public function edit($id)
	{

		$id 	= base64_decode($id);
		$image1	=	$image2	=	$image3	= '';
		  
		if($this->input->post('submit') && $this->input->post('submit') != '')
		{
			
			
			/** check extension **/
			
			$image1Flage = 0;
			$image2Flage = 0;
			$image3Flage = 0;
			if(isset($_FILES['txtImage1']['name']) && $_FILES['txtImage1']['name'] != ''){
				$extension = substr(strrchr($_FILES['txtImage1']['name'], '.'), 1);
				if (($extension!= "jpg") && ($extension!= "JPG") && ($extension != "jpeg")  && ($extension != "JPEG") && ($extension != "gif")  && ($extension != "GIF") && ($extension != "png")  && ($extension != "PNG") && ($extension != "bmp") && ($extension != "BMP")){
					$image1Flage = 1;
				}
			}
			
			
			if(isset($_FILES['txtImage2']['name']) && $_FILES['txtImage2']['name'] != ''){
				$extension = substr(strrchr($_FILES['txtImage2']['name'], '.'), 1);
				if (($extension!= "jpg") && ($extension!= "JPG") && ($extension != "jpeg")  && ($extension != "JPEG") && ($extension != "gif")  && ($extension != "GIF") && ($extension != "png")  && ($extension != "PNG") && ($extension != "bmp") && ($extension != "BMP")){
					$image2Flage = 1;
				}
			}
			
			
			if(isset($_FILES['txtImage3']['name']) && $_FILES['txtImage3']['name'] != ''){
				$extension = substr(strrchr($_FILES['txtImage3']['name'], '.'), 1);
				if (($extension!= "jpg") && ($extension!= "JPG") && ($extension != "jpeg")  && ($extension != "JPEG") && ($extension != "gif")  && ($extension != "GIF") && ($extension != "png")  && ($extension != "PNG") && ($extension != "bmp") && ($extension != "BMP")){
					$image3Flage = 1;
				}
			}
			
			
			
			if($image1Flage == 1){
				$data['error_msg']		= 'Please select image type png,jpg,jpeg,gif,bmp for image 1.';
			}
			else if($image1Flage == 1){
				$data['error_msg']		= 'Please select image type png,jpg,jpeg,gif,bmp for image 2.';
			}
			else if($image2Flage == 1){
				$data['error_msg']		= 'Please select image type png,jpg,jpeg,gif,bmp for image 3.';
			}
			else
			{

				$image1	= $image2 = $image3	= '';


				if(isset($_FILES['txtImage1']['name']) && $_FILES['txtImage1']['name'] != '')
				{
					$time 	= strtotime(date('Y-m-d H:i:s'));
					$ext 	= pathinfo($_FILES['txtImage1']['name'], PATHINFO_EXTENSION);
					$image1  = $time.'.'.$ext; 
					move_uploaded_file($_FILES['txtImage1']['tmp_name'],'./product_images/'.$image1);

					$this->load->library('image_lib');
					
					$config['source_image'] = './product_images/'.$image1;
				
					$config['width'] = 400;
					
					$config['height'] = 400;
					
					$config['maintain_ratio'] = FALSE;
					
					$config['new_image'] = './product_images/'.$image1;
					
					$flag_for_image = 0;
					
					$this->image_lib->initialize($config);
					
					if(!$this->image_lib->resize()){
					
						$flag_for_image = 1;
					
					} 	
					
					$this->image_lib->clear();

					if(file_exists('./product_images/'.$this->input->post('oldImage1'))){
						unlink('./product_images/'.$this->input->post('oldImage1'));

					}

				}
				else
				{
					
					$image1		= $this->input->post('oldImage1');

				} 
				if(isset($_FILES['txtImage2']['name']) && $_FILES['txtImage2']['name'] != '')
				{
					$time 	= strtotime(date('Y-m-d H:i:s'));
					$ext 	= pathinfo($_FILES['txtImage2']['name'], PATHINFO_EXTENSION);
					$image2  = $time.'2.'.$ext; 
					move_uploaded_file($_FILES['txtImage2']['tmp_name'],'./product_images/'.$image2);

					$this->load->library('image_lib');
					
					$config['source_image'] = './product_images/'.$image2;
				
					$config['width'] = 400;
					
					$config['height'] = 400;
					
					$config['maintain_ratio'] = FALSE;
					
					$config['new_image'] = './product_images/'.$image2;
					
					$flag_for_image = 0;
					
					$this->image_lib->initialize($config);
					
					if(!$this->image_lib->resize()){
					
						$flag_for_image = 1;
					
					} 	
					
					$this->image_lib->clear();

					if($this->input->post('oldImage2') != "" && file_exists('./product_images/'.$this->input->post('oldImage2'))){
						unlink('./product_images/'.$this->input->post('oldImage2'));

					}
				}
				else
				{
					
					$image2		= $this->input->post('oldImage2');

				}
				if(isset($_FILES['txtImage3']['name']) && $_FILES['txtImage3']['name'] != '')
				{
					$time 	= strtotime(date('Y-m-d H:i:s'));
					$ext 	= pathinfo($_FILES['txtImage3']['name'], PATHINFO_EXTENSION);
					$image3  = $time.'3.'.$ext; 
					move_uploaded_file($_FILES['txtImage3']['tmp_name'],'./product_images/'.$image3);

					$this->load->library('image_lib');
					
					$config['source_image'] = './product_images/'.$image3;
				
					$config['width'] = 400;
					
					$config['height'] = 400;
					
					$config['maintain_ratio'] = FALSE;
					
					$config['new_image'] = './product_images/'.$image3;
					
					$flag_for_image = 0;
					
					$this->image_lib->initialize($config);
					
					if(!$this->image_lib->resize()){
					
						$flag_for_image = 1;
					
					} 	
					
					$this->image_lib->clear();
					if($this->input->post('oldImage3') != "" && file_exists('./product_images/'.$this->input->post('oldImage3'))){
						unlink('./product_images/'.$this->input->post('oldImage3'));

					}

				}
				else
				{
					$image3		= $this->input->post('oldImage3');


				}

				//print_r($_POST);

				$checkStockShipping	=	$checkEligibleStock 	= '1';

				if($this->input->post('checkStockShipping') != '')
				{
						$checkStockShipping	= $this->input->post('checkStockShipping');

				}
				if($this->input->post('checkEligibleStock') != '')
				{
						$checkEligibleStock	= $this->input->post('checkEligibleStock');

				}


				$data =  array('product_name' 		=> $this->input->post('txtName'),
							  'list_price' 			=> $this->input->post('txtListPrice'),
							  'product_price' 		=> $this->input->post('txtOurPrice'),
							  'currency' 			=> 'Naira',
							  'product_quantity' 	=> 0,
							  'brand' 				=> $this->input->post('txtBrand'),
							  'product_detail' 		=> $this->input->post('txtDetails'),
							  'category' 			=> $this->input->post('selCategory'),
							  //'sub_category' 		=> $this->input->post('txtSubCategory'),
							  'stock_shipping' 		=> $checkStockShipping,
							  'stock_pickup' 		=> $checkEligibleStock,
							  'image' 				=> $image1,
							  'image2' 				=> $image2,
							  'image3' 				=> $image3,
							);
							
				$productdata  			= $this->product_model->single_product_detail(array('products.id'=> $id));
				if($productdata)
				{
					if($productdata->isactive == 'd')
						$data['isactive'] =  'p';
				}

				if($this->product_model->update_product($data, $id))
				{
					
					$data['msg'] = 'Product has been updated successfully.';
				}
				else
				$data['error_msg'] = 'Product has not updated. Please try again!';

			}
	
		
		}
		
		/* merchant header start */
		$merchantDetail				= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));
		$data['merchantDetail']		= $merchantDetail;
		
		$data['merchantBanner']  	= $this->product_model->merchant_banner($_SESSION['userDetail']->id);	
		/* merchant header end */
		$productdetail  			= $this->product_model->single_product_detail(array('products.id'=> $id));

		$data['productdetail']  	= $productdetail ;

		//$data['subcatdetail']  		= $this->category_model->all_subcategory_list(array('parent_id' => $productdetail->category ), '', $start=0, $array = FALSE);
		
		$data['cateDetail']			= $this->category_model->get_categories(0, $productdetail->category);

		if(!$productdetail)
		{
			header("Location: ".base_url()."profile");

		}

		
		$this->layout->css(base_url().'assets/css/validationEngine.jquery.css');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine-en.js');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine.js');
		// editor
		$this->layout->css(base_url().'assets/css/editor.css');
		$this->layout->js(base_url().'assets/js/editor.js');

		$this->layout->script("<script type='text/javascript'> $('#edit-product').validationEngine({ promptPosition : 'topLeft',}); $('#edit-product').click(function() { if($('#oldImage1').val() != ''){ $('#txtImage1').removeClass( 'validate[required]' ); $('.imageformError').hide(); } else {  $('#txtImage1').addClass( 'validate[required]' ); $('.imageformError').show(); }  });$('#txtDetails').Editor(); $('form#edit-product').submit(function(){ $('#txtDetails').val($('.Editor-editor').html()); }); $('.Editor-editor').html($('#txtDetails').val()); </script>");

		
		$this->layout->view('edit_product',$data);
	}

	public function delete($id)
	{

		$id 					= base64_decode($id);
		$productdetail  		= $this->product_model->single_product_detail(array('products.id'=> $id));
		if($productdetail)
		{
			if($productdetail->image != '')
			{
				if(file_exists('./product_images/'.$productdetail->image)){
					unlink('./product_images/'.$productdetail->image);

				}
			}
			if($productdetail->image2 != '')
			{
				if(file_exists('./product_images/'.$productdetail->image2)){
					unlink('./product_images/'.$productdetail->image2);

				}
			}
			if($productdetail->image3 != '')
			{
				if(file_exists('./product_images/'.$productdetail->image3)){
					unlink('./product_images/'.$productdetail->image3);

				}
			}
			
			$this->product_model->delete_product($id);

			$this->product_model->delete_product_colour_by_pid(array('pid' => $id ));
			
			$this->product_model->delete_product_size_by_pid(array('pid' => $id ));

			$this->product_model->delete_product_size_colour(array('product_id' => $id ));

		}
		header("Location: ".base_url()."profile");
			
	}

	public function product_colour($pid)
	{


		$id 					= base64_decode($pid);
		$data['productId']		= $id;
		$data['productColour'] 	= $this->product_model->product_color(array('product_color.pid' => $id, 'products.pid' => $_SESSION['userDetail']->id ));

		/* merchant header start */
		$merchantDetail				= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));
		$data['merchantDetail']		= $merchantDetail;
		
		$data['merchantBanner']  	= $this->product_model->merchant_banner($_SESSION['userDetail']->id);	
		/* merchant header end */

		$this->layout->view('product_colour',$data);


	}


	public function add_colour($pid)
	{

		$id 					= base64_decode($pid);
		
		if($this->input->post('submit') && $this->input->post('submit') != '')
		{
			
			$data =  array('color' 					=> $this->input->post('txtColour'),
						   'pid' 					=> $id,
						   );

				if($this->product_model->add_product_colour($data))
				{
					
					//$data['msg'] = 'Product colour has been inserted successfully.';
					header("Location: ".base_url()."product-colour/".base64_encode($id));
					die();
				}
				else
				$data['error_msg'] = 'Product colour has not inserted. Please try again!';


		}

		$data['productId']		= $id;

		/* merchant header start */
		$merchantDetail				= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));
		$data['merchantDetail']		= $merchantDetail;
		
		$data['merchantBanner']  	= $this->product_model->merchant_banner($_SESSION['userDetail']->id);	
		/* merchant header end */

		$this->layout->css(base_url().'assets/css/validationEngine.jquery.css');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine-en.js');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine.js');

		$this->layout->script("<script type='text/javascript'> $('#add-product-colour').validationEngine({ promptPosition : 'topLeft',}); </script>");
		
		$this->layout->view('add_product_colour',$data);
		
			
	} 


	public function edit_colour($id)
	{

		$id 					= base64_decode($id);
		
		if($this->input->post('submit') && $this->input->post('submit') != '')
		{
			
			$data =  array('color' 					=> $this->input->post('txtColour'),
						   'id' 					=> $id,
						   );

				if($this->product_model->update_product_colour($data, $id))
				{
					
					$data['msg'] = 'Product colour has been updated successfully.';
				}
				else
				$data['error_msg'] = 'Product colour has not updated. Please try again!';


		}

		$data['productColourId']		= $id;

		/* merchant header start */
		$merchantDetail				= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));
		$data['merchantDetail']		= $merchantDetail;
		
		$data['merchantBanner']  	= $this->product_model->merchant_banner($_SESSION['userDetail']->id);	
		/* merchant header end */


		$data['productColourDetail'] 	= $this->product_model->get_single_product_color(array('product_color.id' => $id, 'products.pid' => $_SESSION['userDetail']->id ));


		$this->layout->css(base_url().'assets/css/validationEngine.jquery.css');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine-en.js');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine.js');

		$this->layout->script("<script type='text/javascript'> $('#edit-product-colour').validationEngine({ promptPosition : 'topLeft',});  </script>");
		
		$this->layout->view('edit_product_colour',$data);
		
			
	} 

	public function delete_colour($id)
	{

		$id 					= base64_decode($id);

		$productColourDetail	= $this->product_model->get_single_product_color(array('product_color.id' => $id, 'products.pid' => $_SESSION['userDetail']->id ));

		if($productColourDetail)
		{

			$this->product_model->delete_product_colour(array('product_color.id' => $id));
			header("Location: ".base_url()."product-colour/".base64_encode($productColourDetail->pid));
		}
		else
		{
			header("Location: ".base_url()."profile");

		}

			
	}



	public function product_size($pid)
	{


		$id 					= base64_decode($pid);
		$data['productId']		= $id;
		$data['productSize'] 	= $this->product_model->product_size(array('product_size.pid' => $id, 'products.pid' => $_SESSION['userDetail']->id ));

		/* merchant header start */
		$merchantDetail				= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));
		$data['merchantDetail']		= $merchantDetail;
		
		$data['merchantBanner']  	= $this->product_model->merchant_banner($_SESSION['userDetail']->id);	
		/* merchant header end */

		$this->layout->view('product_size',$data);


	}


	public function add_size($pid)
	{

		$id 					= base64_decode($pid);
		
		if($this->input->post('submit') && $this->input->post('submit') != '')
		{
			
			$data =  array('size' 					=> $this->input->post('txtSize'),
						   'pid' 					=> $id,
						   );

				if($this->product_model->add_product_size($data))
				{
					
					//$data['msg'] = 'Product size has been inserted successfully.';
					header("Location: ".base_url()."product-size/".base64_encode($id));
					die();
				}
				else
				$data['error_msg'] = 'Product size has not inserted. Please try again!';


		}

		$data['productId']		= $id;

		/* merchant header start */
		$merchantDetail				= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));
		$data['merchantDetail']		= $merchantDetail;
		
		$data['merchantBanner']  	= $this->product_model->merchant_banner($_SESSION['userDetail']->id);	
		/* merchant header end */

		$this->layout->css(base_url().'assets/css/validationEngine.jquery.css');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine-en.js');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine.js');

		$this->layout->script("<script type='text/javascript'> $('#add-product-size').validationEngine({ promptPosition : 'topLeft',}); </script>");
		
		$this->layout->view('add_product_size',$data);
		
			
	} 


	public function edit_size($id)
	{

		$id 					= base64_decode($id);
		
		if($this->input->post('submit') && $this->input->post('submit') != '')
		{
			
			$data =  array('size' 					=> $this->input->post('txtSize'),
						   'id' 					=> $id,
						   );

				if($this->product_model->update_product_size($data, $id))
				{
					
					$data['msg'] = 'Product size has been updated successfully.';
				}
				else
				$data['error_msg'] = 'Product size has not updated. Please try again!';


		}

		$data['productSizeId']		= $id;

		/* merchant header start */
		$merchantDetail				= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));
		$data['merchantDetail']		= $merchantDetail;
		
		$data['merchantBanner']  	= $this->product_model->merchant_banner($_SESSION['userDetail']->id);	
		/* merchant header end */


		$data['productSizeDetail'] 	= $this->product_model->get_single_product_size(array('product_size.id' => $id, 'products.pid' => $_SESSION['userDetail']->id ));


		$this->layout->css(base_url().'assets/css/validationEngine.jquery.css');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine-en.js');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine.js');

		$this->layout->css(base_url().'assets/css/wheelcolorpicker.css');
		$this->layout->js(base_url().'assets/js/jquery.wheelcolorpicker.js');
				
		$this->layout->script("<script type='text/javascript'> $('#edit-product-size').validationEngine({ promptPosition : 'topLeft',}); </script>");
		
		$this->layout->view('edit_product_size',$data);
		
			
	} 

	public function delete_size($id)
	{

		$id 					= base64_decode($id);

		$productSizeDetail	= $this->product_model->get_single_product_size(array('product_size.id' => $id, 'products.pid' => $_SESSION['userDetail']->id ));

		if($productSizeDetail)
		{

			$this->product_model->delete_product_size(array('product_size.id' => $id));
			header("Location: ".base_url()."product-size/".base64_encode($productSizeDetail->pid));
		}
		else
		{
			header("Location: ".base_url()."profile");

		}

			
	}

	public function relate_product_size_colour($pid)
	{


		$id 					= base64_decode($pid);
		$data['productId']		= $id;
		//$data['productSizeColour'] 	= $this->product_model->relate_product_size_colour(array('relation_color_size.product_id' => $id, 'products.pid' => $_SESSION['userDetail']->id ));
		$data['productSizeColour'] 	= $this->product_model->get_product_size_colour_by_product(array('relation_color_size.product_id' => $id, 'products.pid' => $_SESSION['userDetail']->id ));

		/* merchant header start */
		$merchantDetail				= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));
		$data['merchantDetail']		= $merchantDetail;
		
		$data['merchantBanner']  	= $this->product_model->merchant_banner($_SESSION['userDetail']->id);	
		/* merchant header end */

		$this->layout->view('product_size_colour',$data);


	}

	public function add_product_size_colour($pid)
	{

		$id 					= base64_decode($pid);
		
		if($this->input->post('submit') && $this->input->post('submit') != '')
		{
			
			$data =  array('product_id' 				=> $id,
						   'size_id' 					=> $this->input->post('txtSize'),
						   'colour_id' 					=> $this->input->post('txtColour'),
						   'qty'						=> $this->input->post('txtqty'),
						   );

				if($this->product_model->add_product_size_colour($data))
				{
					
					//$data['msg'] = 'Product colour and size has been inserted successfully.';
					header("Location: ".base_url()."relate-product-size-colour/".base64_encode($id));
					die();
				}
				else
				$data['error_msg'] = 'Product colour and size has not inserted. Please try again!';


		}

		$data['productId']		= $id;

		/* merchant header start */
		$merchantDetail				= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));
		$data['merchantDetail']		= $merchantDetail;
		
		$data['merchantBanner']  	= $this->product_model->merchant_banner($_SESSION['userDetail']->id);	
		/* merchant header end */
		
		$data['productColour']  	= $this->product_model->product_color(array('products.id' => $id, 'products.pid' => $_SESSION['userDetail']->id));	
		$data['productSize']  		= $this->product_model->product_size(array('products.id' => $id, 'products.pid' => $_SESSION['userDetail']->id));	

		$this->layout->css(base_url().'assets/css/validationEngine.jquery.css');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine-en.js');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine.js');

		$this->layout->script("<script type='text/javascript'> $('#add-product-colour-size').validationEngine({ promptPosition : 'topLeft',}); </script>");
		
		$this->layout->view('add_product_colour_size',$data);
		
			
	} 

	public function edit_product_size_colour($id)
	{

		$id 					= base64_decode($id);
		
		if($this->input->post('submit') && $this->input->post('submit') != '')
		{
			
			$data =  array('id' 						=> $id,
						   'size_id' 					=> $this->input->post('txtSize'),
						   'colour_id' 					=> $this->input->post('txtColour'),
						   'qty'						=> $this->input->post('txtqty'),
						   );


			$where 	= array('id' =>  $id);


			if($this->product_model->update_product_size_colour($where, $data))
			{
					
				$data['msg'] = 'Product size and colour has been updated successfully.';
			}
			else
			$data['error_msg'] = 'Product size and colour has not updated. Please try again!';


		}


		/* merchant header start */
		$merchantDetail				= $this->user_model->single_merchant_detail(array('users.id' => $_SESSION['userDetail'] ->id ));
		$data['merchantDetail']		= $merchantDetail;
		
		$data['merchantBanner']  	= $this->product_model->merchant_banner($_SESSION['userDetail']->id);	
		/* merchant header end */

		$data['productDetail']		= $this->product_model->get_single_product_size_colour(array('relation_color_size.id' => $id));
		
		$data['productColour']  	= $this->product_model->product_color(array('products.id' => $data['productDetail']->product_id, 'products.pid' => $_SESSION['userDetail']->id));	
		$data['productSize']  		= $this->product_model->product_size(array('products.id' => $data['productDetail']->product_id, 'products.pid' => $_SESSION['userDetail']->id));	
		


		$this->layout->css(base_url().'assets/css/validationEngine.jquery.css');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine-en.js');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine.js');

		$this->layout->script("<script type='text/javascript'> $('#edit-product-size-colour').validationEngine({ promptPosition : 'topLeft',}); </script>");
		
		$this->layout->view('edit_product_colour_size',$data);
		
			
	} 

	public function delete_product_size_colour($id)
	{

		$id 					= base64_decode($id);

		$productDetail	= $this->product_model->get_single_product_size_colour(array('relation_color_size.id' => $id));

		if($productDetail)
		{

			$this->product_model->delete_product_size_colour(array('relation_color_size.id' => $id));
			header("Location: ".base_url()."relate-product-size-colour/".base64_encode($productDetail->product_id));
		}
		else
		{
			header("Location: ".base_url()."profile");

		}

			
	}



	public function ajax_subcategory($catId)
	{
	
		$subCategory  		 = $this->category_model->all_subcategory_list(array('status'=>'t','parent_id'=>$catId), '', 0, $array = FALSE);

		$html 				 = '';
		if($subCategory)
		{

			
			foreach ($subCategory as $row) {
				$html 	.= '<option value="'.$row->id.'">'.$row->name.'</option>';
			
			}

		}
		echo $html;


	}

	public function ajax_size($proId, $sizeId)
	{
	
		$productdetail 		 = $this->product_model->relate_product_size_colour(array('products.id'=>base64_decode($proId),'relation_color_size.size_id'=>base64_decode($sizeId)));

		$htmlColour 				 = '';
		if($productdetail)
		{

			
			foreach ($productdetail as $row) {
				$htmlColour 	.= '<option value="'.base64_encode($row->color_id).'">'.$row->color.'</option>';
			
			}

		}
		echo $htmlColour;


	}
	public function ajax_colour($proId, $colourId)
	{
	
		$productdetail 		 = $this->product_model->relate_product_size_colour(array('products.id'=>base64_decode($proId),'relation_color_size.colour_id'=>base64_decode($colourId)));

		$htmlSize 				 = '';
		$htmlqty 				 = '';
		if($productdetail)
		{

			
			$index				= 0;    
			foreach ($productdetail as $row) {
				$htmlSize 	.= '<option value="'.base64_encode($row->size_id).'">'.$row->size.'</option>';

				if($index == 0)
				{
					for ($i=1; $i <= $row->qty; $i++) { 
						$htmlqty 	.= '<option value="'.$i.'">'.$i.'</option>';
					}

				}
				$index ++;
			
			}

		}
		$data['size']		= $htmlSize;
		$data['qty']		= $htmlqty;
		echo json_encode($data);


	}
	public function ajax_qty($proId, $colourId = '', $sizeId = '')
	{
		
		if($colourId != '' && $sizeId != ''  && $colourId != 'undefined' && $sizeId != 'undefined')
		{

			$productdetail 		 = $this->product_model->relate_product_size_colour(array('products.id'=>base64_decode($proId),'relation_color_size.colour_id'=>base64_decode($colourId),'relation_color_size.size_id'=>base64_decode($sizeId)));
		}
		else if($sizeId != '' && $sizeId != 'undefined')
		{

			$productdetail 		 = $this->product_model->relate_product_size(array('relation_color_size.size_id'=>base64_decode($sizeId) ,'products.id'=>base64_decode($proId) )); 
		}
		else if($colourId != '' && $colourId != 'undefined' )
		{

			$productdetail 		 = $this->product_model->relate_product_colour(array('relation_color_size.colour_id'=>base64_decode($colourId),'products.id'=>base64_decode($proId) )); 
		}


		$html 				 = '';
		if($productdetail)
		{

			foreach ($productdetail as $row) {

				for ($i=1; $i <= $row->qty ; $i++) { 
					$html 	.= '<option value="'.$i.'">'.$i.'</option>'; 
				}

				
			
			}

		}
		echo $html;


	}

	/** script to update the product qyt **/
	public function update_product_qty()
	{
		$productdetail 	 = $this->product_model->related_product_by_id();
		foreach ($productdetail as $rows) {

			$proQty = $this->product_model->get_single_product_size_colour(array('relation_color_size.product_id' => $rows->id));

			if(empty($proQty))
			{
				$this->product_model->add_product_size_colour(array('product_id' => $rows->id, 'qty' => 10));

			}
			
		}

	}// end of function 


}
