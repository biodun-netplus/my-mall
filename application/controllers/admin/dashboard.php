<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public $layout_view = 'admin/layout/default';
	
	public function index()
	{
		$this->layout->title('Ecobank WebMall - Online Store Solution | admin');  // for dynamic title.. 
		$this->layout->view('admin/dashboard');
	}
}
