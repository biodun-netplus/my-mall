<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

	public $layout_view = 'admin/layout/default';
	function __construct()
	{
		parent::__construct();

		$this->load->model('registration_model');
		$this->load->model('category_model');
		$this->load->model('common_model');
	}
	
	// view list of users
	public function index()
	{
		
		$this->layout->title('Ecobank WebMall - Online Store Solution | Registration');  // for dynamic title.. 
		$data['records'] = $this->registration_model->all_users_list();
		$this->layout->css(base_url().'assets/css/admin/styles.css');
		$this->layout->view('admin/list-registration',$data);
	}


	public function add()
	{
		$data 		= '';
		$image 		= '';
		
		if($this->input->post('submit') && $this->input->post('submit') != '')
		{
			// logo upload 
			if(isset($_FILES['txtImage']['name']) && $_FILES['txtImage']['name'] != '')
			{
				$time 	= strtotime(date('Y-m-d H:i:s'));
				$ext 	= pathinfo($_FILES['txtImage']['name'], PATHINFO_EXTENSION);
				$image  = $time.'.'.$ext; 
				move_uploaded_file($_FILES['txtImage']['tmp_name'],'./assets/upload/logo/'.$image);
			}
			
			$data1 = array('txtAddress' 		=> $this->input->post('txtAddress'),
						  'txtCountry' 			=> $this->input->post('txtCountry'),
						  'txtState' 			=> $this->input->post('txtState'),
						  'txtCity' 			=> $this->input->post('txtCity'),
						  'txtZip' 				=> $this->input->post('txtZip'),
						  'txtCategory' 		=> $this->input->post('txtCategory'),
						 /* 'txtSubCategory'		=> $this->input->post('txtSubCategory'),*/
						  'txtImage' 			=> $image,
						  'txtDeliveryarea' 	=> $this->input->post('txtDeliveryarea'),
						  'txtAccountType' 		=> $this->input->post('txtAccountType'),
						  'numIsactive' 		=> 1,
						);
			$lastInsertId = $this->registration_model->insert_merchant($data1);
			
			$data2 = array('txtName' 			=> $this->input->post('txtName'),
						  'numMerchantId' 		=> $lastInsertId,
						  'txtDisplayName' 		=> $this->input->post('txtDisplayName'),
						  'txtEmail' 			=> $this->input->post('txtEmail'),
						  'txtPassword' 		=> md5($this->input->post('txtPassword')),
						  'txtPhone' 			=> $this->input->post('txtPhone'),
						  'numIsactive' 		=> 1,
						);
			
			if($this->registration_model->insert_user($data2))
			{
				$data['msg'] = 'User has been successfully register.';
			}
			else
			$data['error_msg'] = 'User has not register.';
		
		}
		
		$this->layout->title('Ecobank WebMall - Online Store Solution | Registration');  // for dynamic title.. 
		$data['countries'] = $this->common_model->get_countries();
		$data['states_merchant'] = $this->common_model->get_states_merchant();
		$data['categories'] = $this->category_model->all_category_list(array('numIsactive' => 1 ));
		$this->layout->css(base_url().'assets/css/validationEngine.jquery.css');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine-en.js');
		$this->layout->js(base_url().'assets/js/jquery.validationEngine.js');
		$this->layout->view('admin/add-registration',$data);
	}



	public function edit($id)
	{
		$id         =  base64_decode($id);  // user id
		$data 		= '';
		$image 		= '';
		
		$getMerchantId = $this->registration_model->single_user_detail(array('users.id'=>$id));
		if($this->input->post('submit') && $this->input->post('submit') != '')
		{
			// logo upload 
			if(isset($_FILES['txtImage']['name']) && $_FILES['txtImage']['name'] != '')
			{
				$time 	= strtotime(date('Y-m-d H:i:s'));
				$ext 	= pathinfo($_FILES['txtImage']['name'], PATHINFO_EXTENSION);
				$image  = $time.'.'.$ext; 
				move_uploaded_file($_FILES['txtImage']['tmp_name'],'./assets/upload/logo/'.$image);
				if(file_exists('./assets/upload/logo/'.$this->input->post('txtImageOld')))
				{
					unlink('./assets/upload/logo/'.$this->input->post('txtImageOld'));
				}
				
			}
			else
			{
				$image = $this->input->post('txtImageOld');
				
			}
			// update merchant detail
			if($getMerchantId->numMerchantId > 0)
			{
			
				$data1 = array('txtAddress' 	    => $this->input->post('txtAddress'),
							  'txtCountry' 			=> $this->input->post('txtCountry'),
							  'txtState' 			=> $this->input->post('txtState'),
							  'txtCity' 			=> $this->input->post('txtCity'),
							  'txtZip' 				=> $this->input->post('txtZip'),
							  'txtCategory' 		=> $this->input->post('txtCategory'),
							  /*'txtSubCategory'		=> $this->input->post('txtSubCategory'),*/
							  'txtImage' 			=> $image,
							  'txtDeliveryarea' 	=> $this->input->post('txtDeliveryarea'),
							  'txtAccountType' 		=> $this->input->post('txtAccountType'),
							);
				
				$this->registration_model->update_merchant($getMerchantId->numMerchantId,$data1);
			}
			// update uaser detail
			$data2 = array('txtName' 			=> $this->input->post('txtName'),
						  'txtDisplayName' 		=> $this->input->post('txtDisplayName'),
						  'txtEmail' 			=> $this->input->post('txtEmail'),
						  'txtPhone' 			=> $this->input->post('txtPhone'),
						);
						
			if($this->input->post('txtPassword') && $this->input->post('txtPassword') != '')
			{
				$data2 = array('txtPassword' => md5($this->input->post('txtPassword')));
			}
						
			if($this->registration_model->update_user('',$id,$data2))
			{
				$data['msg'] = 'User has been successfully updated.';
			}
			else
			$data['error_msg'] = 'User has not updated.Please try again!';
		
		}
		
		$this->layout->title('Ecobank WebMall - Online Store Solution | Registration');  // for dynamic title.. 
		
		$userRecord = $this->registration_model->single_user_detail(array('id'=>$id));
		$merchantRecord = $this->registration_model->single_merchant_detail(array('id'=>$getMerchantId->numMerchantId));
		
		if($userRecord)
		{
			
			$data['userRecords'] = $userRecord;
			$data['merchantRecords'] = $merchantRecord;
			$data['countries']  = $this->common_model->get_countries();
			$data['states'] = $this->common_model->get_states($merchantRecord->txtCountry);
			$data['states_merchant'] = $this->common_model->get_states_merchant();
			$data['categories'] = $this->category_model->all_category_list(array('numIsactive' => 1 ));
			$this->layout->css(base_url().'assets/css/validationEngine.jquery.css');
			$this->layout->js(base_url().'assets/js/jquery.validationEngine-en.js');
			$this->layout->js(base_url().'assets/js/jquery.validationEngine.js');
			$this->layout->view('admin/edit-registration',$data);
		}
		else
		{
			redirect('admin/registration/');

		}
		
	}
	

	// ajax approved unapproved function
	public function approved_unapproved(){
		
		if($this->input->post('ids') != '' && is_numeric($this->input->post('ids'))){
			$checkRow = $this->registration_model->single_user_detail(array('id'=>$this->input->post('ids')));
			if($checkRow)
			{

				if($checkRow->numIsactive == 0)
				{
					
					$this->registration_model->update($this->input->post('ids'),array('numIsactive'=>1));
					echo '<div class="alert alert-success" role="alert">User has been approved successfully.</div>';
					
				}
				else if($checkRow->numIsactive == 1)
				{
					$this->registration_model->update($this->input->post('ids'),array('numIsactive'=>0));
					echo 'div class="alert alert-success" role="alert">User has unapproved successfully.</div>';
				}
				 
			}
			
		}
		else
		{
			echo '<div class="alert alert-danger" role="alert">User has not approved. Please try again!</div>';
		}
		
	}// End of function...
		
	// ajax get state function
	public function get_state($countryId){
		
		$option  = '';
		$records = $this->common_model->get_states($countryId);
		if($records)
		{
			foreach($records as $row)
			{
				$option .='<option value="'.$row->stateId.'">'.$row->name.'</option>';
			}
			
		}
		echo $option ;
		
		
	}// End of function...

	public function delete($id){
		
		$id = base64_decode($id);
		
		if($id != '' && is_numeric($id)){
			
			$checkuser = $this->registration_model->single_user_detail(array('id'=>$id));

			if($checkuser)
			{

				if($checkuser->numMerchantId > 0)
				{
					
					$merchantRecord = $this->registration_model->single_merchant_detail(array('id'=>$checkuser->numMerchantId));
					
					$image = $merchantRecord->txtImage;
					
					if($image && $image != '')
					{
						if(file_exists('./assets/upload/logo/'.$image))
						unlink('./assets/upload/logo/'.$image);
					}
					
					$this->registration_model->delete_merchant(array('id'=>$checkuser->numMerchantId));
					
				}
				$this->registration_model->delete_user(array('id'=>$id));
				 
			}
			redirect('admin/registration/');
		}
		else
		{
			redirect('admin/registration/');
		}
		
	}// End of function...
		
}
