<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Category extends CI_Controller {

    

	function __construct()

	{

		parent::__construct();



		$this->load->model('product_model');

		$this->load->model('user_model');

	}



	public function index()

	{

		$data['categoryDetail'] 	= $this->category_model->product_by_category_array(); 
		//$data['subCategoryDetail'] 	= $this->category_model->all_subcategory_list(array('status'=>'t')); 

		$this->layout->css(base_url().'assets/css/default.css');
		$this->layout->css(base_url().'assets/css/nivo-slider.css');
		$this->layout->js(base_url().'assets/js/jquery.nivo.slider.js');
		
		$this->layout->css(base_url().'assets/css/owl.carousel.css');
		$this->layout->css(base_url().'assets/css/owl.theme.css');
		$this->layout->js(base_url().'assets/js/owl.carousel.min.js');
		
		$this->layout->css(base_url().'assets/css/cycle2.css');
		$this->layout->js(base_url().'assets/js/jquery.cycle2.js');
		$this->layout->js(base_url().'assets/js/jquery.cycle2.carousel.js');		
		
		$this->layout->css(base_url().'assets/css/main.css');
		$this->layout->js(base_url().'assets/js/custom_js.js');

		$this->layout->script("<script type='text/javascript'>  $.fn.cycle.defaults.autoSelector = '.slideshow'; $(window).load(function() {  $('#slider').nivoSlider(); }); $(document).ready(function () { $('#left').click(function () { $('#cont').animate({eft: '-=50' }, 'slow'); }); $('#right').click(function () { $('#cont').animate({ left: '+=50' }, 'slow');  }); }); </script>");
		$this->layout->view('category',$data);

	}

	public function product_by_category($catId)
	{

		$cat_id	= base64_decode($catId);


            $data['categoryDetail'] 	= $this->category_model->get_single_category(array('id' => $cat_id));
              //var_dump($data['categoryDetail']);
              if($data['categoryDetail']->cat_parent_id != ''){

                if($data['categoryDetail']->cat_sub_parent_id != ''){
                      $main_cat_id = $data['categoryDetail']->cat_sub_parent_id;
                  }
                  else
                  {
                      $main_cat_id = $data['categoryDetail']->cat_parent_id;
                  }
              }
              else
              {
                  $main_cat_id = $data['categoryDetail']->id;
              }
//        echo $main_cat_id;


		$data['subCategoryDetail'] 	= $this->category_model->get_subcategory_list(array('isactive'=>'t', 'parent_id !='=> 0));

		if($data['categoryDetail'] )
		{
			
			$productDet 	= ''; 
			$obj_merged 	= '';
			$sub_obj_merged 	= '';
			$sub_sub_cat 	= array();
            $main_prod = array();
           if($data['categoryDetail']->parent_id == 0)
           {
//               echo 'here';

               $productDeta 	= $this->category_model->product_by_category(array('products.category' => $cat_id,'products.isactive'=>'t','users.isactive'=>'t'));
//               echo $this->db->last_query();
               $subCategoriesDetail 	= $this->category_model->get_subcategories_by_parent_id($cat_id);

               if($subCategoriesDetail && !empty($subCategoriesDetail))
               {

                   for ($i=0; $i < count($subCategoriesDetail); $i++) {

                       $subsubCategoriesDetail 	= $this->category_model->get_subcategories_by_parent_id($subCategoriesDetail[$i]);
                       foreach($subsubCategoriesDetail as $sub)
                       {
                           $sub_sub_cat[] = $sub;
                       }


                       $sub_productDeta 	= $this->category_model->product_by_sub_category(array('products.sub_category' => $subCategoriesDetail[$i],'products.isactive'=>'t','users.isactive'=>'t'));

                       if($sub_productDeta && !empty($productDeta))
                       {
                           foreach ($sub_productDeta as $record) {
                               $obj_merged[] = $record;

                           }
                       }

                   }

               }
               else{
                   $obj_merged 		= $this->category_model->product_by_category(array('products.category' => $cat_id,'products.isactive'=>'t','users.isactive'=>'t'));
//                   echo $this->db->last_query();
               }

               if($sub_sub_cat && !empty($sub_sub_cat))
               {

                   for ($i=0; $i < count($sub_sub_cat); $i++) {
//                    echo $subCategoriesDetail[$i];


//                    var_dump($subsubCategoriesDetail);
                       $sub_sub_productDeta 	= $this->category_model->product_by_sub_sub_category(array('products.sub_sub_category' => $sub_sub_cat[$i],'products.isactive'=>'t','users.isactive'=>'t'));

                       if($sub_sub_productDeta && !empty($sub_sub_productDeta))
                       {
                           foreach ($sub_sub_productDeta as $record) {
                               $sub_obj_merged[] = $record;

                           }
//var_dump($obj_merged);
                       }

                   }

               }


               if($obj_merged && !empty($obj_merged))
               {
                   $a = array_unique(array_merge($productDeta,$obj_merged));
                   if($sub_obj_merged && !empty($sub_obj_merged))
                   {
                       $b = array_unique(array_merge($a,$sub_obj_merged));

                       foreach($b as  $val)
                       {
//                           echo $val.'<br>';
                           $query = $this->category_model->product_by_id(array('products.id' => $val,'products.isactive'=>'t','users.isactive'=>'t'));
                           //var_dump($query);
                        
                            $main_prod[] = $query;

                       }
                   }
                   else
                   {
                       foreach($a as  $val)
                       {

                           $query = $this->category_model->product_by_id(array('products.id' => $val,'products.isactive'=>'t','users.isactive'=>'t'));
                            
                           $main_prod[] = $query;

                       }
                   }
               }
               else
               {
                   foreach($productDeta as  $val)
                   {
//                       echo $val.'<br>';
                       $query = $this->category_model->product_by_id(array('products.id' => $val,'products.isactive'=>'t','users.isactive'=>'t'));
                        
                       $main_prod[] = $query;

                   }
               }
           }
           elseif($data['categoryDetail']->parent_id != 0 &&  !$this->category_model->has_grand_parent($cat_id) )
           {

                $sub_sub_cat = array() ;
                $subsubCategoriesDetail 	= $this->category_model->get_subcategories_by_parent_id($cat_id);

                $sub_productDeta 	= $this->category_model->product_by_sub_category(array('products.sub_category' => $cat_id,'products.isactive'=>'t','users.isactive'=>'t'));
//                echo $this->db->last_query();
//               var_dump($sub_productDeta);
//                if($subsubCategoriesDetail && !empty($subsubCategoriesDetail))
//                {
//                    echo 'enter here';
//                    for ($i=0; $i < count($subsubCategoriesDetail); $i++) {
//
//                        $sub_sub_productDeta 	= $this->category_model->product_by_sub_sub_category(array('products.sub_sub_category' => $subsubCategoriesDetail[$i],'products.isactive'=>'t','users.isactive'=>'t'));
//
//                        if($sub_sub_productDeta && !empty($sub_sub_productDeta))
//                        {
//                            foreach ($sub_sub_productDeta as $record) {
//                                $sub_obj_merged[] = $record;
//
//                            }
//
//                        }
//
//                    }
//                    if(!empty($sub_productDeta) && !empty($sub_obj_merged))
//                    {
//                        $a = array_unique(array_merge($sub_productDeta,$sub_obj_merged));
//                        foreach($a as  $val)
//                        {
//
//                            $query = $this->category_model->product_by_id(array('products.id' => $val,'products.isactive'=>'t','users.isactive'=>'t'));
//
//                            $main_prod[] = $query;
//
//                        }
//                    }
//
////                    var_dump($main_prod);
//                }
//                else
//                {
//                    echo 'enter here2';
                    if($sub_productDeta && !empty($sub_productDeta))
                    {
                        foreach($sub_productDeta as  $val)
                        {

                            $query = $this->category_model->product_by_id(array('products.id' => $val,'products.isactive'=>'t','users.isactive'=>'t'));

                            $main_prod[] = $query;

                        }
                    }


//                }
            }





            //var_dump($main_prod);exit;

			$data['catId'] = $main_cat_id;
			$data['productDetail'] 	= $main_prod;//array_slice($main_prod,0,30);


			$this->layout->css(base_url().'assets/css/cycle2.css');
			$this->layout->js(base_url().'assets/js/jquery.cycle2.js');
			$this->layout->js(base_url().'assets/js/jquery.cycle2.carousel.js');


			$this->layout->css(base_url().'assets/css/main.css');
			$this->layout->js(base_url().'assets/js/custom_js.js');

			$this->layout->script("<script type='text/javascript'>  $.fn.cycle.defaults.autoSelector = '.slideshow';  $('.hoverme').show(); setTimeout(function(){ $('.nav-bottom').show(); },1000); </script>");
			$this->layout->view('poduct_by_category',$data);
		}

		else
		{
			$this->layout->css(base_url().'assets/css/main.css');
			$data['productDetail']  = $this->home_model->all_product(array('products.isactive' => 't'), 8);
			$this->layout->view("my404_view",$data);
		}

	}


	public function ajax_product_by_subcate()
	{

		$html 		    	= "";
		$subCatId 			= base64_decode($this->input->post('subCatId'));

		$sub_categories 	= $this->category_model->get_subcategories_by_parent_id($subCatId);
        // put catehory id
        $sub_categories 	= array_merge($sub_categories, array($subCatId));

      	if($sub_categories && !empty($sub_categories))
        {

          	for ($i=0; $i < count($sub_categories); $i++) {

      			$productDetail	= $this->product_model->related_product_by_id(array('category' => $sub_categories[$i],'products.isactive'=>'t'));

      			if($productDetail && !empty($productDetail))
        		{

			        foreach($productDetail as $productRow)
			        {
			          	$productImg   = base_url().'assets/img/logo.png';
			          	if($productRow->image != '' )
			          	$productImg  = base_url().'product_images/'.$productRow->image;
						$html .='
					    <div class="product_wrap product_wrap1"> <a href="'.base_url().'product/'.base64_encode($productRow->id).'">
					      <div class="image"><img src="'.$productImg.'"></div>
					      <div class="details">
					        <table>
					          <tr>
					            <td colspan="2" style="color: #fff; height: 42px; vertical-align: top; min-width: 210px; width: 100%;">'.$productRow->product_name.'</td>
					          </tr>
					          <tr>
					            <td style="color: #A8C738; font-size: 22px; font-weight: 600;">&#x20A6;'.number_format($productRow->product_price).'</td>
					            <td><input type="button" class="btn-block btn btn-cart" value="add to cart"></td>
					          </tr>
					        </table>
					      </div>
					      </a> </div>';

		        	}

		      	}

      		}
      	}

      	else
     	//if($html == "")
      	{
        	$html ='<div class="no-record text-center">No Record Found!</div>';
      	}

      	echo $html;

	}



}
