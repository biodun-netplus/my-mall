<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->load->model('home_model');
//        load in layout library
	}

	public function index()
	{
		
		
		//$data['merchant'] = $this->home_model->all_merchant(array('isactive'=>'t'));

		$data['products'] 	 	= $this->home_model->all_product(array('users.isactive'=>'t','products.isactive'=>'t'));
		$data['ourMerchant'] 		= $this->home_model->meet_our_merchants();
		$data['shopMerchants'] 		= $this->home_model->shop_by_merchants();
		$data['headerBanner'] 		= $this->home_model->get_banner();
//        var_dump($data['headerBanner']);
		$data['featuredMerchant'] 		= $this->home_model->featured_merchant();
		$data['featuredProduct'] 		= $this->home_model->featured_product();
		$data['category1'] 		= $this->home_model->get_main_category();
		$data['other_category'] 		= $this->home_model->get_main_other_category();
//		$data['other_category'] 		= $this->home_model->get_main_other_category();
//        var_dump($data['category']);
		$data['merchantProfile'] 	= $this->home_model->all_merchant(array('isactive' => 't', 'image !=' => "") , "RANDOM", 5 );


		
		$this->layout->css(base_url().'assets/css/owl.carousel.css');
		$this->layout->css(base_url().'assets/css/owl.theme.css');
		$this->layout->js(base_url().'assets/js/owl.carousel.min.js');
		
		$this->layout->js(base_url().'assets/js/custom_js.js');
		$this->layout->view('home',$data);
	}
}
