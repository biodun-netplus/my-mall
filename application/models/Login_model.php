<?php

class Login_model extends CI_Model {

    function __construct()

    {
        parent::__construct();
    }

	public function check_user($where)
	{
		$this->db->select('*')->from('users')->where($where);

		$data = $this->db->get();

		if( $data->num_rows() > 0 ){

			return $data->row();

		} else {

			return FALSE;

		}

	} //end of check_user function

}// End of class...