<?php

class Cart_model extends CI_Model {

    function __construct()

    {

        parent::__construct();

    }

    public function insert_to_cart($data)
    {

		$this->db->insert('cart',$data);	
		return $this->db->insert_id();


    }//End of insert_to_cart function

    public function get_to_cart($productId,$sessionId,$userId, $orderId,$productColor = '',$productSize = '')
    {
		$where = '';
		if($userId != '')
		{
			$where .= " AND user_id='".$userId."'";
		}
		
		if($orderId == 'Is_NULL')
		{
			$where .= "  AND order_id IS NULL";
		}
		else if(isset($_SESSION['Check']) && $_SESSION['Check'] !='')
		{
			$where .= "  AND order_id = '".$_SESSION['Check']."'"; 
		}
		if($productColor != '')
		{
			$where .= " AND color='".$productColor."'";
		}
		if($productSize != '')
		{
			$where .= " AND size='".$productSize."'";
		}
		$cart = $this->db->query("select * from cart where product_id='".$productId."' AND cart_session='".$sessionId."'".$where."");

    	if( $cart->num_rows() > 0 )
    	{
    		return $cart->row();

    	} 
		else 
		{

    		return FALSE;

    	}


    }//End of get_to_cart function

    public function update_cart_product($data, $where)
    {

		$this->db->where($where);

    	if( $this->db->update('cart', $data) )
    	{
    		return TRUE;

    	} 
		else 
		{

    		return FALSE;

    	}

    }//End of update_cart function


    public function update_cart($userId, $orderId)
    {
		$update = 0;
		if(isset($_SESSION['cart']))
		{
			for($i = 0; $i < count($_SESSION['cart']); $i ++)
			{
				$cartId = $_SESSION['cart'][$i]['cartId'];
				
				$query  = "update cart set order_id = '".$orderId."',user_id = '".$userId."' where id = '".$cartId."' ";
				
				$this->db->query($query);
				$update =  1;
			
			}
		}
		
    	if( $update == 1 )
    	{
    		return TRUE;

    	} 
		else 
		{

    		return FALSE;

    	}
		
	}//End of update_cart function
	
	public function get_cart_item($order_id)
	{
		$this->db->select('quantity')->from('cart');
		$this->db->where(['order_id' => $order_id]);
		$qty = $this->db->get();
		if($qty->num_rows() > 0)
		{
			return $qty->result();
		}else{
			return false;
		}
	}

    public function get_all_items_cart($user_id = '', $orderId, $groupBy = "")
    {
		$group_by = $whereAnd =  "";
		if($groupBy != "")
		{
			$group_by = " GROUP BY ".$groupBy;
		}

		if($user_id != ""){
			$whereAnd .= $user_id. " and ";
		}
		$cart = $this->db->query("SELECT cart.*,u.id AS merchant_id, u.country, u.first_name, u.slug, u.display_name, u.image As merchant_img, u.address As merchant_add, u.pickup_state As merchant_deliverystate, u.pickup_lga As merchant_deliverylga, u.phone As merchant_phone, u.email AS merchant_email, p.image as product_img ,p.product_detail, p.product_price, p.product_quantity as product_quantity  from cart LEFT JOIN products AS p ON p.id = cart.product_id LEFT JOIN users AS u ON u.id = p.pid  WHERE ".$whereAnd."order_id = '".$orderId."' " .$group_by." ORDER BY u.id ASC");

    	if( $cart->num_rows() > 0 )
    	{
    		return $cart->result();

    	} 
		else 
		{

    		return FALSE;

    	}


    }//End of get_all_items_cart function

	public function get_lga($state)
	{
		$this->db->select('lga.name');
        $this->db->from('f_states');
		$this->db->join('lga', 'lga.state_id = f_states.id ','left');
		$this->db->where('f_states.name', $state);
		$data = $this->db->get();

		$html		= '';
		$html	.= '<option value="">Delivery LGA</option>	';
		
		foreach ($data->result() as $row) {
			var_dump($row);
			$html .= '<option value="'.$row->name.'">'.$row->name.'</option>	';
		}
		
		echo  $html;

	}

    public function remove_product_cart($id)
    {
		
		$this->db->where('id', $id);


    	if( $this->db->delete('cart') )
    	{
    		return TRUE;

    	} 
		else 
		{

    		return FALSE;

    	}


    }//End of remove_product_cart function
    public function remove_address($id)
    {

        $this->db->where('id', $id);


        if( $this->db->delete('cart') )
        {
            return TRUE;

        }
        else
        {

            return FALSE;

        }


    }
    public function single_cart_detail($where)
    {
        $this->db->select('*')->from('cart')->where($where);

        $data = $this->db->get();

        if( $data->num_rows() > 0 ){

            return $data->row();

        } else {

            return FALSE;

        }

	}

	public function get_total_amount($id)
	{
		$this->db->select('total_ammount')->from('order')->where('order_id', $id);
		$data = $this->db->get();
		

		if($data->num_rows() > 0){
			return $data->row();
		}else{
			return FALSE;
		}
	}

	public function new_total_amount($amount, $id)
	{
		$this->db->set('total_ammount', $amount);
		$this->db->where('order_id', $id);
		$this->db->update('order');
	}

	public function get_product_price($id)
	{
		$this->db->select('product_price')->from('products')->where('pid', $id);
		$data = $this->db->get();
		if($data->num_rows() > 0)
		{
			var_dump($data->result());
			return $data->result();
		}else{
			return FALSE;
		}
	}

	public function store_item_weight($id, $weight)
	{
		$this->db->set('item_weight', $weight);
		$this->db->where('order_id', $id);
		$this->db->update('order');
	}
	// public function remove_total_amount($order_id)
	// {
	// 	$this->db->set('total_ammount', 0);
	// 	$this->db->where('order_id', $order_id);

	// 	$this->db->update('order');
		
		
	// }
	

	
    

    
}// end of class...