<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Merchant_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	public function getAllMerchants(){
        $this->db->reconnect();
		$query = $this->db->get('users');
		return $query->result_array();
	}
	public function getMerchant($id){
        $this->db->reconnect();
		$query = $this->db->get_where('users', array('id' => $id));
		return $query->result_array();
	}
    public function getMerchantProduct($id){
        $this->db->reconnect();
        $transaction_array = array();
        $query = $this->db->get_where('products', array('pid' => $id,'isactive'=>'t'));
//        echo $query->num_rows();
        foreach($query->result() as $key)
        {

            $product_id = $key->id;
//            echo $product_id.'<br>';
            $this->db->reconnect();
            $this->db->select('relation_color_size.qty,product_color.color,product_size.size');
            $this->db->from('relation_color_size');
            $this->db->join("product_color","relation_color_size.colour_id=product_color.id","inner");
            $this->db->join("product_size","relation_color_size.size_id=product_size.id",'inner');
            $this->db->where('relation_color_size.product_id',$product_id);

            $query = $this->db->get();
            if($query->num_rows() > 0 )
            {
//                $pid =
                $key->relations = $query->result();
                if(!empty($transaction_array))
                {
                    $transaction_array = array_merge($transaction_array,array($key));
                }
                else
                {
                    $transaction_array[]=$key;
                }
            }
            else
            {
                if(!empty($transaction_array))
                {
                    $transaction_array = array_merge($transaction_array,array($key));
                }
                else
                {
                    $transaction_array[]=$key;
                }
            }
        }
        return $transaction_array;
    }
	public function addMerchant($data){
        $this->db->reconnect();
		$query = $this->db->insert('users',$data);
		return $query;
	}
	public function getStates(){
        $this->db->reconnect();
		$query = $this->db->get('states');
		return $query->result_array();
	}
	public function editMerchant($data){
        $this->db->reconnect();
		$this->db->where('id', $data['id']);
		$query = $this->db->update('users', $data);
		return $query;
	}
	public function getMerchantAddress($id){
        $this->db->reconnect();
		$this->db->select('delivery_state');
		$this->db->from('users');
		$this->db->join('products', 'products.pid = users.id', 'inner');
		$this->db->where('products.id',$id);
        $query = $this->db->get(); 
		return $query->result_array();
	}
}