<?php

class Category_model extends CI_Model {

    function __construct()

    {
        parent::__construct();
    }

	
	public function all_category_list($where = '', $limit='', $start=0, $array = FALSE)
	{
		
		if($limit != '')
		{
			
			$this->db->limit($limit, $start);
		}
		
		$this->db->select('*');

		$this->db->from('category');
		
		if($where && $where != '')
		{
			$this->db->where($where);
			$this->db->where(array('parent_id' => 0 ));

			
		}
				 
		//$this->db->order_by("DateCreated", "asc");

		$data = $this->db->get();

		if( $data->num_rows() > 0 ){
			if($array){
				return $data->result_array();
			}else {
				return $data->result();
			}

		} else {

			return FALSE;

		}

	} //end of all_category_list function

	public function get_single_category($where = '')
	{
		
		$this->db->select('*');

		$this->db->from('category');
		
		if($where && $where != '')
		{
			$this->db->where($where);
			
		}
				 
		//$this->db->order_by("DateCreated", "asc");

		$data = $this->db->get();
//        echo $this->db->last_query();
		if( $data->num_rows() > 0 ){
            $data->row()->parent_name = '';
                if($data->row()->parent_id != '0')
                {
                    $pid = $data->row()->parent_id;
                    $proposal_query = $this->db->query("SELECT * FROM category WHERE id =$pid ");
//                    var_dump($proposal_query->result());
                    if($proposal_query->result()[0]->parent_id == '0')
                    {


                        $data->row()->cat_parent_name = $proposal_query->result()[0]->category_name;
                        $data->row()->cat_parent_id = $proposal_query->result()[0]->id;
                        $data->row()->cat_sub_parent_id = '';
                        $data->row()->cat_sub_parent_name = '';
                    }
                    else
                    {
                        $data->row()->cat_parent_name = $proposal_query->result()[0]->category_name;
                        $data->row()->cat_parent_id = $proposal_query->result()[0]->id;
                        $real_pid = $proposal_query->result()[0]->parent_id;
                        $main_proposal_query = $this->db->query("SELECT * FROM category WHERE id =$real_pid ");
                        $data->row()->cat_sub_parent_id = $main_proposal_query->result()[0]->id;
                        $data->row()->cat_sub_parent_name = $main_proposal_query->result()[0]->category_name;

                    }
                }
            else
            {
                $data->row()->cat_parent_name = "";
                $data->row()->cat_parent_id = '';
                $data->row()->cat_sub_parent_id = '';
                $data->row()->cat_sub_parent_name = '';
            }




				return $data->row();

		} else {

			return FALSE;

		}

	} //end of get_single_category function

    public function has_grand_parent($id)
    {
//        echo $id;
        $this->db->select('*');

        $this->db->from('category');
        $this->db->where('id',$id);


        $data = $this->db->get();

//        echo $this->db->last_query();
        if( $data->num_rows() > 0 ){

            $this->db->select('*');

            $this->db->from('category');
            $this->db->where('id',$data->result()[0]->parent_id);
            $data1 = $this->db->get();
//            echo $this->db->last_query();
            if( $data1->num_rows() > 0 ){
//                var_dump($data1->result());
                $this->db->select('*');

                $this->db->from('category');
                $this->db->where('id',$data1->result()[0]->parent_id);
                $data2 = $this->db->get();
//                echo $this->db->last_query();
                if( $data2->num_rows() > 0 ){
//                    var_dump($data->result());

                    return true;

                } else {
//                    var_dump($data2->result());
                    return FALSE;

                }
            }
        }
    }
	public function product_by_category_array($where = '', $limit='', $start=0)

	{
		$detail				= array();
		
		$categoryDetail		= $this->all_category_list(array('isactive'=>'t'), 3, 0, $array = true);

		$index				= 0;
		
		if(	$categoryDetail  )
		{
			foreach($categoryDetail  as $row)
			{
					
					$detail [$index]		= $row;

					// getting sub categories 
					$subCategoriesDetail 	= $this->category_model->get_subcategories_by_parent_id($row['id']);

          			// put catehory id
          			$subCategoriesDetail 	= array_merge($subCategoriesDetail, array($row['id']));

          			$countProduct 		= 1;
          			$checkProduct 		= TRUE;
          			$countI				= 0;
          			if($subCategoriesDetail && !empty($subCategoriesDetail))
            		{
            			
            			for ($i = 0; $i < count($subCategoriesDetail); $i++) {


            				///$this->db->limit(6, 0);
					
							$this->db->select('products.*');
					
							$this->db->from('products');
							
							$this->db->where(array('isactive'=>'t', 'category' => $subCategoriesDetail[$i]));
                            $this->db->order_by('id', 'RANDOM');
                            $this->db->limit(20);
							$data = $this->db->get(); 
					
							if( $data->num_rows() > 0 ){
									
									
									foreach($data->result_array() as $rows)
									{
										$countProduct ++;
										$detail [$index]['productDetail'] [$countI]= $rows;
										$countI	 ++;
										$checkProduct = FALSE;
									}
							}
							else{
								//$detail [$index]['productDetail']  = "";
							}
							//$index	 ++;
							$countProduct ++;

            			}// end of subCategoriesDetail foreach


            		}
            		if($checkProduct){ 
						$detail [$index]['productDetail']  = "";
					}
					else if(count($detail [$index]['productDetail']) > 6) {
						
						$output = array_slice($detail [$index]['productDetail'], 0, 6);
						$detail [$index]['productDetail'] = "";
						$detail [$index]['productDetail'] = $output;

					}
					$index	 ++;
					

			} // end of foreach
			
			//echo "<!---"; print_r($detail); echo "!-->";
			return $detail;
		}
		else
		{
			return FALSE;
		
		}

	} //end of product_by_category_array function


    public function product_by_id($where = '', $limit='', $start=0, $groupBy = '')

    {
        $categorArr  	= array();
        if($limit != '')
        {

            $this->db->limit($limit, $start);
        }

        $this->db->select('products.*');

        $this->db->from('products');

//        $this->db->join('category', 'products.category = category.id','inner');
        $this->db->join('users', '(products.pid = users.id ','left');
//		$this->db->join('category', 'products.category = category.id');

        if($where && $where != '')
        {
            $this->db->where($where);

        }
        $this->db->order_by('Add_Date', 'DESC');
//        $this->db->limit(20);

        if($groupBy != '')
        {
            $this->db->group_by($groupBy);
        }

        $data = $this->db->get();
//        echo $this->db->last_query();
//        var_dump( $data->result_array())
        if( $data->num_rows() > 0 ){

            return $data->result_array() ;
        } else {

            return FALSE;

        }

    }
	public function product_by_category($where = '', $limit='', $start=0, $groupBy = '')

	{
        $categorArr  	= array();
		if($limit != '')
		{
			
			$this->db->limit($limit, $start);
		}
		
		$this->db->select('products.*, category.category_name');

		$this->db->from('products');
		
		$this->db->join('category', 'products.category = category.id','inner');
		$this->db->join('users', '(products.pid = users.id ','left');
//		$this->db->join('category', 'products.category = category.id');

		if($where && $where != '')
		{
			$this->db->where($where);
			
		}
        $this->db->order_by('Add_Date', 'DESC');
//        $this->db->limit(20);

		if($groupBy != '')
		{
			$this->db->group_by($groupBy);
		}
				 
		$data = $this->db->get();
//        echo $this->db->last_query();
//        var_dump( $data->result_array());
		if( $data->num_rows() > 0 ){

            foreach($data->result() as $row)
            {

                $current_id = $row->id; //echo "<!---";  echo $current_id. '<br>'; echo "!-->";

                if(!empty($categorArr))
                {
                    $categorArr = array_merge($categorArr,array($row->id));
                }
                else
                {
                    $categorArr[] = $row->id;
                }


            }
            //var_dump($categorArr);
            return $categorArr;
		} else {

			return FALSE;

		}

	} //end of product_by_category function
    public function product_by_sub_category($where = '', $limit='', $start=0, $groupBy = '')

    {
        $categorArr  	= array();
        if($limit != '')
        {

            $this->db->limit($limit, $start);
        }

        $this->db->select('products.*, category.category_name');

        $this->db->from('products');

        $this->db->join('category', 'products.sub_category = category.id','inner');
        $this->db->join('users', '(products.pid = users.id ','left');
//		$this->db->join('category', 'products.category = category.id');

        if($where && $where != '')
        {
            $this->db->where($where);

        }
        $this->db->order_by('Add_Date', 'DESC');
//        $this->db->limit(20);

        if($groupBy != '')
        {
            $this->db->group_by($groupBy);
        }

        $data = $this->db->get();
//        echo $this->db->last_query();

        if( $data->num_rows() > 0 ){

            foreach($data->result() as $row)
            {

                $current_id = $row->id; //echo "<!---";  echo $current_id. '<br>'; echo "!-->";

                if(!empty($categorArr))
                {
                    $categorArr = array_merge($categorArr,array($row->id));
                }
                else
                {
                    $categorArr[] = $row->id;
                }


            }
            return $categorArr;
        } else {

            return FALSE;

        }

    }
    public function product_by_sub_sub_category($where = '', $limit='', $start=0, $groupBy = '')

    {
        $categorArr  	= array();
        if($limit != '')
        {

            $this->db->limit($limit, $start);
        }

        $this->db->select('products.*, category.category_name');

        $this->db->from('products');

        $this->db->join('category', 'products.sub_sub_category = category.id','inner');
        $this->db->join('users', '(products.pid = users.id ','left');
//		$this->db->join('category', 'products.category = category.id');

        if($where && $where != '')
        {
            $this->db->where($where);

        }
        $this->db->order_by('id', 'RANDOM');
        $this->db->limit(20);


        if($groupBy != '')
        {
            $this->db->group_by($groupBy);
        }

        $data = $this->db->get();
//        echo $this->db->last_query();

        if( $data->num_rows() > 0 ){

            foreach($data->result() as $row)
            {

                $current_id = $row->id; //echo "<!---";  echo $current_id. '<br>'; echo "!-->";

                if(!empty($categorArr))
                {
                    $categorArr = array_merge($categorArr,array($row->id));
                }
                else
                {
                    $categorArr[] = $row->id;
                }


            }
            return $categorArr;
        } else {

            return FALSE;

        }

    }


	public function all_subcategory_list($where = '', $limit='', $start=0, $array = FALSE)

	{

		if($limit != '')
		{
			
			$this->db->limit($limit, $start);
		}
		
		$this->db->select('*');

		$this->db->from('sub_category');
		
		if($where && $where != '')
		{
			$this->db->where($where);
			
		}

		$this->db->order_by('parent_id asc'); 
		
		$data = $this->db->get();
		
		if( $data->num_rows() > 0 ){
			
			if($array){
				return $data->result_array();
			}
			else {
				return $data->result();
			}

		} 
		else {

			return FALSE;

		}


	} //end of all_subcategory_list function

	public function get_subcategory_list($where = '', $limit='', $start=0, $array = FALSE)
	{

		if($limit != '')
		{
			
			$this->db->limit($limit, $start);
		}
		
		$this->db->select('*');

		$this->db->from('category');
		
		if($where && $where != '')
		{
			$this->db->where($where);
			
		}

		$this->db->order_by('parent_id asc'); 
		
		$data = $this->db->get();
		
		if( $data->num_rows() > 0 ){
			
			if($array){
				return $data->result_array();
			}
			else {
				return $data->result();
			}

		} 
		else {

			return FALSE;

		}


	} //end of get_subcategory_list function


	//================  Get categories with sub level categories ===================//
	public function get_categories($parent = 0, $selected = "")
	{
	    $html  = "";
	    $getParent = "";
	    $query = $this->db->query("SELECT * FROM `category` WHERE `parent_id` = '$parent' ");

	    if( $query->num_rows() > 0 ){
	    
		    foreach($query->result() as $row)
		    {
		       
		        $current_id = $row->id;

		        if($row->parent_id > 0 )
		        {
		          
		          $getParent = (array_reverse(explode(">", $this->get_sub_level($row->parent_id))));
		          $getParent = implode(" > ", $getParent);
		          $getParent = substr($getParent, 2)." > ";
		        }

		        $html .= '<option value="'.$row->id.'"';

		        if($selected ==  $row->id)
		        {
		        	$html .= ' selected="selected" ';
		    	}

		        $html .= ' >' . $getParent. $row->category_name;

		        $has_sub = NULL;

		        $hasSub = $this->db->query("SELECT COUNT(`parent_id`) FROM `category` WHERE `parent_id` = '$current_id'");
		        $has_sub = $query->num_rows($hasSub);
		        if($has_sub)
		        {
		            
		            $html .= $this->get_categories($current_id, $selected); 

		        }
		        $html .= '</option>';
		         
		    }
		}

	    return $html;
	} 

	public function get_sub_level($parent = 0)
	{
	    $html  = "";
	    $getParent = "";

	    $query = $this->db->query("SELECT * FROM `category` WHERE `id` = '$parent' ");
	    if( $query->num_rows() > 0 ){
	    
		    foreach($query->result() as $row)
		    {
	    
	       
		        $current_id = $row->parent_id;
		        $html      .= $row->category_name;

		        $has_sub = NULL;
		        $hasSub = $this->db->query("SELECT COUNT(`id`) FROM `category` WHERE `id` = '$current_id' AND parent_id > 0 ");
		        $has_sub = $query->num_rows($hasSub);
		       
		        //die();

		        if($has_sub)
		        {
		            
		            $html .= ">". $this->get_sub_level($current_id); 

		        }
		    }
	    }

	    return $html;
	}
    public function get_parent($id)
    {
        $this->db->select('category.* ');

        $this->db->from('category');
        $this->db->where('category.id', $id);
//        $this->db->order_by('id', 'RANDOM');
//        $this->db->limit(4);
        $data = $this->db->get();
        if( $data->num_rows() > 0 ){

            return $data->result();

        } else {

            return FALSE;

        }
    }

	public function get_subcategories_by_parent_id($parent = 0)
	{
	    $categorArr  	= array();
	    $getParent 		= "";


	    $query = $this->db->query("SELECT * FROM `category` WHERE `parent_id` = '$parent' and isactive='t'");

	    if( $query->num_rows() > 0 ){
	    
		    foreach($query->result() as $row)
		    {
		       
		        $current_id = $row->id; //echo "<!---";  echo $current_id. '<br>'; echo "!-->";

		        if(!empty($categorArr))
		        {
		        	$categorArr = array_merge($categorArr,array($row->id));
		        }
		        else
		        {
		        	$categorArr[] = $row->id;
		        }

//		        $has_sub = NULL;
//
//		        $hasSub = $this->db->query("SELECT COUNT(`parent_id`) FROM `category` WHERE `parent_id` = '$current_id' and isactive='t'");
//		        $has_sub = $query->num_rows($hasSub);
//		        if($has_sub)
//		        {
//		        	$tempArr = array();
//
//		            $tempArr = $this->get_subcategories_by_parent_id($current_id);
//
//		            $categorArr = array_merge($categorArr,$tempArr);
//
//		        }
		         
		    }
		}
//var_dump($categorArr);

	    

	    return $categorArr;
	}

    public function getAllCategory(){
        $this->db->reconnect();
        $query = $this->db->get('category');
        return $query->result_array();

    }
}// End of class...
