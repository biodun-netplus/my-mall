<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	public function login($data){
		$query = $this->db->get_where('admin', array('username' => $data['email'], 'password' => md5( $data['password'])));
		return $query->result_array();
	}
}