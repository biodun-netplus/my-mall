<?php

class Home_model extends CI_Model {

    function __construct()

    {
        parent::__construct();
    }

	public function slider()
	{
		$this->db->select('*');

		$this->db->from('home-page-slider');
	
		
		$data = $this->db->get();

		if( $data->num_rows() > 0 ){

			return $data->result();

		} else {

			return FALSE;

		}

	} //end of slider function



	public function get_banner()
	{
		$this->db->select('*');

		$this->db->from('home-page-slider');

		$this->db->where('isactive', 't');

		$data = $this->db->get();

		if( $data->num_rows() > 0 ){

			return $data->result();

		} else {

			return FALSE;

		}

	} //end of slider function


	
	public function all_merchant($where = '' , $orderBy = '', $limit = '', $start = 0 )
	{
		if($limit != '')
		{
			
			$this->db->limit($limit, $start);
		}
		
		$this->db->select('*');

		$this->db->from('users');

		if($where != '')
		{
		
			$this->db->where($where);
		}
		
		if($orderBy != '')
		{
	
			$this->db->order_by('id', $orderBy);
		}
		else
		{
			$this->db->order_by('id', 'RANDOM');
		}
	    
		//$this->db->limit(16);
		
		$data = $this->db->get();
//        echo $this->db->last_query();
		if( $data->num_rows() > 0 ){

			return $data->result();

		} else {

			return FALSE;

		}

	} //end of all merchant function

	public function all_product($where, $limit = "")
	{
		
		$this->db->select('products.*,users.isactive,users.id as UID');

		$this->db->from('products');
        $this->db->join('users', 'products.pid = users.id','INNER');
//		$this->db->where('product_price<10000');
		$this->db->where($where);


		$this->db->order_by('id', 'RANDOM');
	    
		if($limit != "")
		{
			$this->db->limit($limit);
		}
		else
		{
			$this->db->limit(16);
		}
		
		$data = $this->db->get();

		if( $data->num_rows() > 0 ){

			return $data->result();

		} else {

			return FALSE;

		}

	} //end of all product function
    public function featured_merchant()
    {
        $this->db->select('*');

        $this->db->from('featured_merchant');

        $this->db->where('id', '1');

        $data = $this->db->get();

        if( $data->num_rows() > 0 ){

            return $data->result();

        } else {

            return FALSE;

        }

    }
    public function get_main_category()
    {
        $this->db->select('*');

        $this->db->from('category');

        $this->db->where('parent_id', '0');
        $this->db->where('category_name !=','Other categories');
        $this->db->where('isactive ','t');

        $this->db->limit(6);
//        echo $this->db->last_query();
        $data = $this->db->get();

        if( $data->num_rows() > 0 ){

            return $data->result();

        } else {

            return FALSE;

        }

    }
    public function get_main_other_category()
    {
        $this->db->select('*');

        $this->db->from('category');

        $this->db->where('category_name =','Other categories');

//        echo $this->db->last_query();
        $data = $this->db->get();

        if( $data->num_rows() > 0 ){

            return $data->result();

        } else {

            return FALSE;

        }

    }
    public function get_main_other_category_by_name($cat_name)
    {
        $this->db->select('*');

        $this->db->from('category');

        $this->db->where('category_name =',$cat_name);

//        echo $this->db->last_query();
        $data = $this->db->get();

        if( $data->num_rows() > 0 ){

            return $data->result();

        } else {

            return FALSE;

        }

    }
    public function get_no_of_sub($pid)
    {
        $this->db->select('category.* ');

        $this->db->from('category');
        $this->db->where('category.parent_id', $pid);
        $data = $this->db->get();
        return $data->num_rows();
    }
    public function get_sub_categories($pid)
    {

        $this->db->select('category.* ');

        $this->db->from('category');
        $this->db->where('category.parent_id', $pid);
        $this->db->order_by('id', 'RANDOM');
        $this->db->limit(6);
        $data = $this->db->get();
        if( $data->num_rows() > 0 ){

            return $data->result();

        } else {

            return FALSE;

        }
    }
    public function get_prefered_categories($pid)
    {
        $this->db->select('prefered_category.* ');

        $this->db->from('prefered_category');
        $this->db->where('prefered_category.category_id', $pid);
        $this->db->order_by('order_id', 'ASC');
        $this->db->limit(4);
        $data = $this->db->get();
        if( $data->num_rows() > 0 ){

            return $data->result();

        } else {

            return FALSE;

        }
    }
    public function get_category_name($pid)
    {
        $this->db->select('* ');

        $this->db->from('category');
        $this->db->where('id', $pid);
//        $this->db->order_by('order_id', 'ASC');
//        $this->db->limit(4);
        $data = $this->db->get();

            return $data->result();


    }
    public function get_category_by_name($name)
    {
        $this->db->select('* ');

        $this->db->from('category');
        $this->db->like('category_name', $name);
//        $this->db->order_by('order_id', 'ASC');
//        $this->db->limit(4);
        $data = $this->db->get();

        return $data->result();


    }
    public function featured_product()
    {

        $this->db->select('featured_merchant.id as featured_id,featured_merchant.merchant_id as featured_merchant_id,
        featured_merchant.image as featured_image,
        users.display_name,users.id,products.*');

        $this->db->from('featured_merchant');
        $this->db->join('users', 'featured_merchant.merchant_id = users.id','INNER');
        $this->db->join('products', 'users.id = products.pid','INNER');
        $this->db->where('featured_merchant.id', '1');



        $this->db->order_by('products.id', 'RANDOM');


            $this->db->limit(4);


        $data = $this->db->get();

        if( $data->num_rows() > 0 ){

            return $data->result();

        } else {

            return FALSE;

        }

    } //end of all product function


    public function meet_our_merchants()
	{
		$ourMerchant = array();
		/*$this->db->select('users.*, category.category_name')->from('users')->where(array('users.isactive' => 't'));
		$this->db->join('category', 'users.category = category.id');	
		$this->db->join('products', 'users.id = products.pid');	
		$this->db->order_by('id', 'RANDOM');
		$this->db->limit(1);
		$data = $this->db->get();*/
		
		//$data = $this->db->query("SELECT users.display_name,users.id,users.first_name,users.slug, category.category_name from users LEFT JOIN category on users.category = category.id LEFT JOIN products on users.id = products.pid WHERE pid in (SELECT p.pid from products as p WHERE p.isactive = 't' GROUP BY  p.pid HAVING COUNT( p.pid ) > 2 ) ORDER BY RAND() LIMIT 1 ");

        $data = $this->db->query("SELECT users.display_name,users.id,users.first_name,users.slug,users.image, category.category_name from users JOIN category on users.category = category.id JOIN products on users.id = products.pid WHERE users.isactive = 't' ORDER BY RAND() LIMIT 1");



		if( $data->num_rows() > 0 ){

			$detail = $data->row();

			$ourMerchant['displayName'] 	=  $detail->display_name;
			$ourMerchant['id'] 			 	=  $detail->id;
			$ourMerchant['categoryName'] 	=  $detail->category_name;
			$ourMerchant['firstName'] 		=  $detail->first_name;
			$ourMerchant['slug'] 			=  $detail->slug;
			
			if($detail->image != '') {
                $ourMerchant['merchantImg'] = base_url() . 'banner_images/' . $detail->image;
            }else {
                $ourMerchant['merchantImg'] = base_url() . 'assets/img/logo.png';
            }


			// get product
			$this->db->select('products.*')->from('products')->where(array('isactive' => 't','pid'=>$detail->id));
			$this->db->order_by('id', 'RANDOM');
			$this->db->limit(3);
			$data1 = $this->db->get();
			
			if( $data1->num_rows() > 0 )
			{
				$ourMerchant['product'] = $data1->result();
			}

			// count total product of merchant
			$this->db->select('products.*')->from('products')->where(array('isactive' => 't','pid'=>$detail->id));
			$data2 = $this->db->get();
			if( $data2->num_rows() > 0 )
			{
				$ourMerchant['totalProduct'] = $data2->num_rows();
			}
			else
			{
				$ourMerchant['totalProduct'] = 0;
			}
			return $ourMerchant;
		} else {
			return FALSE;
		}

	} //end of Meet Our Merchants function

	public function shop_by_merchants()
	{
		
		$ourMerchant = array();
		
		/*$this->db->select('users.*, category.category_name')->from('users')->where(array('users.isactive' => 't'));
		$this->db->join('category', 'users.category = category.id');	
		$this->db->join('products', 'users.id = products.pid');	
		$this->db->order_by('id', 'RANDOM');
		$this->db->limit(1);
		$data = $this->db->get();*/
		
		//$data = $this->db->query("SELECT users.*, category.category_name from users LEFT JOIN category on users.category = category.id LEFT JOIN products on users.id = products.pid WHERE pid in (SELECT p.pid from products as p WHERE p.isactive = 't' GROUP BY  p.pid HAVING COUNT( p.pid ) > 0 ) GROUP BY products.pid ");

        $data = $this->db->query("SELECT users.display_name,users.id,users.first_name,users.slug,users.image, category.category_name from users JOIN category on users.category = category.id JOIN products on users.id = products.pid GROUP BY users.id ORDER BY RAND() LIMIT 16");


        if( $data->num_rows() > 0 ){

			return $data->result();
			
		} 
		else 
		{

			return FALSE;

		}

	} //end of Shop By Merchants function


	public function get_merchants_by_regexp($regexp )
	{
		
		$data = $this->db->query("SELECT * FROM users WHERE first_name REGEXP  '".$regexp."' AND isactive =  't' group by users.id order by first_name asc ");

		if( $data->num_rows() > 0 ){

			return $data->result();

		} else {

			return FALSE;

		}

	} //end of get_merchants_by_regexp function

    public function get_merchants_with_active_products()
    {

        $data = $this->db->query("SELECT
                            DISTINCT users.id,
                            users.slug,
                            users.image,
                                users.first_name
                                , users.display_name
                            ,  users.isactive
                            FROM
                                users
                                INNER JOIN products
                                    ON (users.id = products.pid)
                            WHERE users.isactive = 't' ORDER BY users.first_name; ");

        if( $data->num_rows() > 0 ){

            return $data->result();

        } else {

            return FALSE;

        }

    } 

    function startsWith($haystack, $needle)
    {
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
    }


}// End of class...
