<?php



class Product_model extends CI_Model {



    function __construct()



    {

        parent::__construct();

    }



    public function merchant_banner($id, $limit='', $start=0)

	{
		
		if($limit != '')
		{
			
			$this->db->limit($limit, $start);
		}


		$this->db->select('*');

		$this->db->from('provider_banner');

		$this->db->where(array('provider_id' => $id));

		$data = $this->db->get();


		if( $data->num_rows() > 0 ){



			return $data->row();



		} else {



			return FALSE;



		}



	} //end of related_product_by_id function



	public function add_banner($data)

	{



		$this->db->insert('provider_banner',$data);	

		return $this->db->insert_id();



	} //end of add_banner function





	public function update_banner($data, $id)

	{



		$this->db->where('id', $id);

		$this->db->update('provider_banner', $data); 

		return true; 



	} //end of update_banner function




	public function add_product($data)

	{



		$this->db->insert('products',$data);	

		return $this->db->insert_id();



	} //end of add_banner function





	public function update_product($data, $id)

	{



		$this->db->where('id', $id);

		$this->db->update('products', $data); 

		return true; 



	} //end of update_banner function


	public function delete_product($id)

	{



		$this->db->where('id', $id);

		$this->db->delete('products'); 

		return true; 



	} //end of delete_product function



	public function single_product_detail($where = '')
	{

		$this->db->select('products.*, category.category_name, category.weight, category.unit');

		$this->db->from('products');

		$this->db->join('category', 'products.sub_category = category.id', 'LEFT');
		

		if(!empty($where))

		{

			$this->db->where($where);

		}



		$data = $this->db->get();

//echo "<!---".$this->db->last_query()."!-->";

		if( $data->num_rows() > 0 ){



			return $data->row();



		} else {



			return FALSE;



		}



	} //end of single_product_detail function





	public function related_product_by_id($where = "")

	{



		$this->db->select('*');

		$this->db->from('products');
                if(!empty($where))

		{
		     $this->db->where($where);
                }

		$data = $this->db->get();



		if( $data->num_rows() > 0 ){



			return $data->result();



		} else {



			return FALSE;



		}



	} //end of related_product_by_id function


	public function add_product_colour($data)

	{


		$this->db->insert('product_color',$data);	

		return $this->db->insert_id();



	} //end of add_product_colour function



	public function update_product_colour($data, $id)

	{

		$this->db->where('id', $id);

		$this->db->update('product_color', $data); 

		return true; 


	} //end of update_product_colour function





	public function delete_product_colour($where)

	{

		$this->db->where($where);

		$this->db->delete('product_color'); 

		return true; 


	} //end of delete_product_colour function

	public function delete_product_colour_by_pid($where)

	{

		$this->db->where($where);

		$this->db->delete('product_color'); 

		return true; 


	} //end of delete_product_colour function

	public function get_single_product_color($where)
	{

		$this->db->select('product_color.*');

		$this->db->from('product_color');
		
		$this->db->join('products', 'products.id = product_color.pid','LEFT');
			 
		$this->db->where($where);

        $this->db->group_by('product_color.color');

		$data = $this->db->get();



		if( $data->num_rows() > 0 ){



			return $data->row();



		} else {



			return FALSE;



		}


	} //end of product_color function

	public function product_color($where)

	{


		$this->db->select('product_color.*');

		$this->db->from('product_color');
		
		$this->db->join('products', 'products.id = product_color.pid','LEFT');
			 
		$this->db->where($where);

		$data = $this->db->get();



		if( $data->num_rows() > 0 ){



			return $data->result();



		} else {



			return FALSE;



		}



	} //end of product_color function




	public function add_product_size($data)

	{


		$this->db->insert('product_size',$data);	

		return $this->db->insert_id();



	} //end of add_product_size function



	public function update_product_size($data, $id)

	{

		$this->db->where('id', $id);

		$this->db->update('product_size', $data); 

		return true; 


	} //end of update_product_size function





	public function delete_product_size($where)

	{

		$this->db->where($where);

		$this->db->delete('product_size'); 

		return true; 


	} //end of delete_product_size function

	public function delete_product_size_by_pid($where)

	{

		$this->db->where($where);

		$this->db->delete('product_size'); 

		return true; 


	} //end of delete_product_size function

	public function get_single_product_size($where)

	{

		$this->db->select('product_size.*');

		$this->db->from('product_size');
		
		$this->db->join('products', 'products.id = product_size.pid','LEFT');
			 
		$this->db->where($where);

		$data = $this->db->get();

		if( $data->num_rows() > 0 ){



			return $data->row();



		} else {



			return FALSE;



		}


	} //end of get_single_product_size function

	public function product_size($where)

	{



		$this->db->select('product_size.*');

		$this->db->from('product_size');

		$this->db->join('products', 'products.id = product_size.pid','LEFT');
		
		$this->db->where($where);

		$data = $this->db->get();



		if( $data->num_rows() > 0 ){



			return $data->result();



		} else {



			return FALSE;



		}



	} //end of product_size function


	public function relate_product_size_colour($where)

	{

		$this->db->distinct();
		$this->db->select('product_size.id AS size_id, product_color.id AS color_id, relation_color_size.id, relation_color_size.qty, product_size.size,  product_color.color');

		$this->db->from('relation_color_size');

		$this->db->join('product_size', 'relation_color_size.size_id = product_size.id', 'left');

		$this->db->join('product_color', 'relation_color_size.colour_id = product_color.id','left');
		
		$this->db->join('products', 'products.id = product_size.pid AND products.id = product_color.pid AND products.id =  relation_color_size.product_id','LEFT');
		
		$this->db->where($where);

		$this->db->group_by('product_size.id ,  product_color.id');

		$data = $this->db->get();


		if( $data->num_rows() > 0 ){



			return $data->result();



		} else {



			return FALSE;



		}


	} //end of relate_product_size_colour function

	public function relate_product_colour($where)

	{

		$this->db->select('relation_color_size.id, relation_color_size.qty, product_color.color, product_color.id AS color_id ');

		$this->db->from('relation_color_size');

		$this->db->join('product_color', 'relation_color_size.colour_id = product_color.id','left');
		
		$this->db->join('products', 'products.id = product_color.pid AND products.id =  relation_color_size.product_id','LEFT');
		
		$this->db->where($where);

		$data = $this->db->get();


		if( $data->num_rows() > 0 ){



			return $data->result();



		} else {



			return FALSE;



		}


	} //end of relate_product_size_colour function

	public function relate_product_size($where)

	{

		$this->db->select('relation_color_size.id, relation_color_size.qty, product_size.size, product_size.id AS size_id ');

		$this->db->from('relation_color_size');

		$this->db->join('product_size', 'relation_color_size.size_id = product_size.id', 'left');

		$this->db->join('products', 'products.id = product_size.pid AND products.id = relation_color_size.product_id','LEFT');
		
		$this->db->where($where);

		$data = $this->db->get();

		if( $data->num_rows() > 0 ){

			return $data->result();

		}
		else
		{


			return FALSE;


		}


	} //end of relate_product_size function
	
	

	public function get_single_product_size_colour($where)

	{

		$this->db->select('*');

		$this->db->from('relation_color_size');

		$this->db->where($where);

		$data = $this->db->get();

		if( $data->num_rows() > 0 ){

			return $data->row();

		} else {

			return FALSE;

		}


	} //end of get_single_product_size_colour function


	public function get_product_size_colour_by_product($where)

	{

		$this->db->select('relation_color_size.*');

		$this->db->from('relation_color_size');
		
		$this->db->join('products', 'products.id =  relation_color_size.product_id','LEFT');

		$this->db->where($where);

		$data = $this->db->get();

		if( $data->num_rows() > 0 ){

			return $data->result();

		} else {

			return FALSE;

		}


	} //end of get_single_product_size_colour function


	public function add_product_size_colour($data)

	{

		$this->db->insert('relation_color_size',$data);	

		return $this->db->insert_id();



	} //end of add_product_size_colour function


	public function update_product_size_colour($where, $data)

	{

		$this->db->where($where);

		$this->db->update('relation_color_size', $data); 
		
		return true; 



	} //end of update_product_size_colour function

	public function delete_product_size_colour($where)

	{

		$this->db->where($where);

		$this->db->delete('relation_color_size'); 

		return true; 


	} //end of delete_product_size_colour function



	public function get_product_colour_by_cat($where = '', $limit='', $start=0, $groupBy = '')

	{

		if($limit != '')
		{
			
			$this->db->limit($limit, $start);
		}


		$this->db->select('product_color.*');

		$this->db->from('product_color');
		
		$this->db->join('products', 'products.id = product_color.pid','LEFT');

		$this->db->join('category', 'products.category = category.id','LEFT');

		if($where && $where != '')
		{
			$this->db->where($where);
			
		}

		if($groupBy != '')
		{
			$this->db->group_by($groupBy);
		}

		$data = $this->db->get();



		if( $data->num_rows() > 0 ){



			return $data->result();



		} 
		else 
		{

			return FALSE;

		}


	} //end of get_product_colour_cat function

    public function addProduct1($data){
        $this->db->reconnect();
        $query = $this->db->insert('products', $data);

        return $query;
    }
    public function editProduct1($data){
        $this->db->reconnect();
        $this->db->where('id', $data['id']);
        $query = $this->db->update('products', $data);
        return $query;
    }

}// End of class...