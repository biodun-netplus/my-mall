<?php

class Order_model extends CI_Model {

    function __construct()

    {
        parent::__construct();
    }


    public function insert_order($data)
    {
    	
		$this->db->insert('order',$data);	
		return $this->db->insert_id();

    }//End of insert function

    public function get_delivery_location($data)
    {

        $this->db->select('u.address, u.pickup_state, u.pickup_lga, u.display_name, u.phone, u.email, s.delivery_address, 
                            s.delivery_state, s.delivery_lga, s.delivery_contact_name, s.contact_no_delivery, s.email, 
                            c.product_name, c.size, c.color, c.price, c.quantity, p.image, p.id, o.delivery_amt');
        $this->db->from('cart c'); 
        $this->db->join('order o', 'c.order_id= o.order_id', 'left');
        $this->db->join('products p', 'c.product_id=p.id', 'left');
        $this->db->join('shipping_addres_list s', 'o.shipping_add_id= s.id', 'left');
        $this->db->join('users u', 'p.pid=u.id', 'left');
        $this->db->where('o.order_id',$data);
              
        $query = $this->db->get(); 
        

        $query = $query->result();
     

        return $query;
     

        //SELECT * FROM `cart` `c` LEFT JOIN `order` `o` ON `c`.`order_id`= `o`.`order_id` 
        //LEFT JOIN `products` `p` ON `c`.`product_id`=`p`.`id` 
        //LEFT JOIN `shipping_addres_list` `s` ON `o`.`shipping_add_id`=`s`.`id` 
        //LEFT JOIN `users` `u` ON `p`.`pid`=`u`.`id` 
        //WHERE `o`.`order_id` = '5520180420025611'

        // $sql = "SELECT users.address as merchant_address,users.deliveryarea as mechant_delivery_area,users.display_name as merchant_display_name,
        //users.phone as merchant_phone,users.email as merchant_email, shipping_addres_list.delivery_addres as customer_address,
        //shipping_addres_list.delivery_area as customer_delivery_area,shipping_addres_list.delivery_contact_name as customer_display_name,
        //shipping_addres_list.contact_no_delivery as customer_phone,shipping_addres_list.email as customer_email, 
        //cart.product_name,cart.size,cart.color,cart.price,cart.quantity, products.image,products.id as product_id, order.delivery_amt 
        //FROM cart LEFT JOIN `order` ON (cart.order_id = order.order_id) LEFT JOIN products ON (cart.product_id = products.id) 
        //LEFT JOIN shipping_addres_list ON (order.shipping_add_id = shipping_addres_list.id) LEFT JOIN users ON (products.pid = users.id)  
        //WHERE  order.order_id = '$order_id' ";
        // $records = mysql_query($sql) or die(mysql_error());
        // $row = mysql_fetch_object($records);
    }

    public function get_pickup_state($id)
    {
        $this->db->select('name')->from('states')->where('id', $id);
        $data = $this->db->get();
        $result = $data->row();
        return $result->name;
    }

    public function get_pickup_lga($id)
    {
        $this->db->select('name')->from('lga')->where('id', $id);
        $data = $this->db->get();

        $result = $data->row();
        return $result->name;
    }




    public function update_order($data, $where = array())
    {
       // var_dump($data);

        $this->db->where($where);
        
        
        $this->db->update('order', $data); 
      //  die;
		return true; 
		
    }//End of update order function



   public function get_courier_id($courier_code)
   {

        $this->db->select('id')->from('shipping_method')->where('courier_code', $courier_code);
        $data = $this->db->get();
        $result = $data->row();
        return $result->id;
   }



	public function all_orders_list()
	{
		$this->db->select('order.*');

		$this->db->from('order');
		
		$this->db->order_by("order.created", "asc");

		$data = $this->db->get();

		if( $data->num_rows() > 0 ){

			return $data->result();

		} else {

			return FALSE;

		}

	} //end of all_order_list function

	public function single_order_detail($where)
	{
		$this->db->select('*')->from('order')->where($where);

		$data = $this->db->get();

		if( $data->num_rows() > 0 ){
			return $data->row();

		} else {

			return FALSE;

		}

	} //end of all_order_list function
    public function addOrder($data){
        $this->db->reconnect();

        if( $this->db->insert('cart',array(
            'product_id' => $data['product_id'] ,
            'price' =>$data['price'] ,
            'product_name' => $data['product_name'],
            'quantity' => $data['quantity'],
            'date' => $data['date'],
            'order_id' =>$data['order_id'],
            'size' => $data['size'],
            'color' => $data['color']
        ))){




            $this->db->reconnect();

            $this->db->select('shipping_address.*');
            $this->db->from('shipping_address');
            $this->db->where('shipping_address.email',$data['email']);
            $query = $this->db->get();
            if($query->num_rows() == 0 )
            {

                if($query = $this->db->insert('shipping_address',array(
                    'first_name' => $data['name'],
                    'email' => $data['email'],
                    'phone' => $data['phone']
                ))){

                    $last_id = $this->db->insert_id();
                    $this->db->reconnect();

                    $this->db->select('order.*');
                    $this->db->from('order');
                    $this->db->where('order.order_id',$data['order_id']);
                    $query = $this->db->get();
                    if($query->num_rows() == 0 )
                    {
                        if($query = $this->db->insert('order',array(
                            'total_ammount' => $data['total_amount'] ,
                            'delivery_amt' => $data['delivery_amt'] ,
                            'created' =>    $data['date'],
                            'status' => $data['status'],
                            'payment_type' => $data['payment_type'],
                            'order_id' => $data['order_id'],
                            'shipping_add_id' => $last_id,

                            'transaction_ref' => $data['transaction_ref']
                        ))){
                            $this->db->reconnect();
                            $this->db->select('relation_color_size.qty');
                            $this->db->from('relation_color_size');

//                $this->db->where('relation_color_size.product_id',$data['product_id']);
                            if($data['size'] != '' && $data['size'] != null)
                            {
                                $this->db->where('relation_color_size.size_id',$data['size']);
                            }
                            if($data['color'] != '' && $data['color'] != null)
                            {
                                $this->db->where('relation_color_size.colour_id',$data['color']);
                            }

                            $this->db->where('relation_color_size.product_id',$data['product_id']);

                            $query = $this->db->get();
                            if($query->num_rows() > 0 )
                            {
//                echo $this->db->last_query();
                                $old_quantity = (int)$query->result()[0]->qty;
                                $new_quantity = $old_quantity - $data['quantity'];
//                echo '<br>'.$new_quantity.'<br>';
                                $this->db->reconnect();
                                $data1 = array(
                                    'qty' => $new_quantity
                                );
                                if($data['size'] != '' && $data['size'] != null)
                                {
                                    $this->db->where('size_id',$data['size']);
                                }
                                if($data['color'] != '' && $data['color'] != null)
                                {
                                    $this->db->where('colour_id',$data['color']);
                                }
                                $this->db->where('product_id',$data['product_id']);

                                $query = $this->db->update('relation_color_size', $data1);

                                return $query;
                            }
                            else
                            {
                                return $query;
                            }
                        }}
                }
            }
            else
            {

                $last_id = $query->result_array()[0]['id'];
                $this->db->reconnect();
                echo $last_id;
                $this->db->select('order.*');
                $this->db->from('order');
                $this->db->where('order.order_id',$data['order_id']);
                $query = $this->db->get();
                if($query->num_rows() == 0 )
                {
                    if($query = $this->db->insert('order',array(
                        'total_ammount' => $data['total_amount'] ,
                        'delivery_amt' => $data['delivery_amt'] ,
                        'created' =>    $data['date'],
                        'status' => $data['status'],
                        'payment_type' => $data['payment_type'],
                        'order_id' => $data['order_id'],
                        'shipping_add_id' => $last_id,

                        'transaction_ref' => $data['transaction_ref']
                    ))){
                        $this->db->reconnect();
                        $this->db->select('relation_color_size.qty');
                        $this->db->from('relation_color_size');

//                $this->db->where('relation_color_size.product_id',$data['product_id']);
                        if($data['size'] != '' && $data['size'] != null)
                        {
                            $this->db->where('relation_color_size.size_id',$data['size']);
                        }
                        if($data['color'] != '' && $data['color'] != null)
                        {
                            $this->db->where('relation_color_size.colour_id',$data['color']);
                        }

                        $this->db->where('relation_color_size.product_id',$data['product_id']);

                        $query = $this->db->get();
                        if($query->num_rows() > 0 )
                        {
//                echo $this->db->last_query();
                            $old_quantity = (int)$query->result()[0]->qty;
                            $new_quantity = $old_quantity - $data['quantity'];
//                echo '<br>'.$new_quantity.'<br>';
                            $this->db->reconnect();
                            $data1 = array(
                                'qty' => $new_quantity
                            );
                            if($data['size'] != '' && $data['size'] != null)
                            {
                                $this->db->where('size_id',$data['size']);
                            }
                            if($data['color'] != '' && $data['color'] != null)
                            {
                                $this->db->where('colour_id',$data['color']);
                            }
                            $this->db->where('product_id',$data['product_id']);

                            $query = $this->db->update('relation_color_size', $data1);

                            return $query;
                        }
                        else
                        {
                            return $query;
                        }
                    }}
            }



        }

    }
    public function getMerchantOrder($id){
        $this->db->select('cart.order_id,product_color.color,product_size.size,cart.product_id,cart.product_name,cart.quantity,order.total_ammount,order.transaction_ref, order.payment_type,order.delivery_amt as delivery,order.status as delivery_status,order.shipping_add_id, users.display_name, shipping_address.first_name, shipping_address.last_name, shipping_address.email');
        $this->db->from('cart');
        $this->db->join('products', 'cart.product_id = products.id', 'left');
        $this->db->join('users', 'products.pid = users.id', 'left');
        $this->db->join('product_color', 'cart.color = product_color.id', 'left');
        $this->db->join('product_size', 'cart.size = product_size.id', 'left');
        $this->db->join('order', 'cart.order_id = order.order_id', 'left');
        $this->db->join('shipping_address', 'order.shipping_add_id = shipping_address.id', 'left');
        $this->db->where('users.id',$id);
        $query = $this->db->get();
        return $query->result_array();
    }

      public function insert_coupon_log($data)
    {
        
        $query = $this->db->insert('coupon_log',$data);   
         return $query;

    }//End of insert function

    public function get_item_weight($id)
    {
        $this->db->select('item_weight');
        $this->db->from('order');
        $this->db->where('order_id', $id);
        $data = $this->db->get();
        if( $data->num_rows() > 0 ){
			return $data->row();

		} else {

			return FALSE;

		}
    }

    public function get_payment_type($id)
    {
        $this->db->select('payment_type');
        $this->db->from('order');
        $this->db->where('order_id', $id);
        $data = $this->db->get();
        if( $data->num_rows() > 0 ){
			return $data->row();

		} else {

			return FALSE;

		}
    }


}// End of class...