<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	public function getCustomers(){
		$query = $this->db->get('shipping_addres_list');
		return $query->result_array();
	}
	public function getCustomer($email){
		$query = $this->db->get_where('shipping_addres_list', array('email' => $email));
		return $query->result_array();
	}
	public function addCustomer($data){
		$query = $this->db->insert('shipping_addres_list',$data);
		return $query;
	}
	public function editCustomer($data){
		$this->db->where('id', $data['id']);
		$query = $this->db->update('shipping_addres_list', $data);
		return $query;
	}
	public function getCustomerById($id){
		$query = $this->db->get_where('shipping_addres_list', array('id' => $id));
		return $query->result_array();
	}
}