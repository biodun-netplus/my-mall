<?php

class Common_model extends CI_Model {

    function __construct()

    {

        parent::__construct();

    }


    public function get_countries()
    {

    	$countries = $this->db->get('countries');

    	if( $countries->num_rows() > 0 )
    	{
    		return $countries->result();

    	} 
		else 
		{

    		return FALSE;

    	}


    }//End of get_countries function

    public function get_states($country_id = "", $order_by = "")
    {

        $this->db->select('*');
        $this->db->from('states');
        $this->db->where('country_id', $country_id);
        
        if($order_by && $order_by != "")
        {
            $this->db->order_by($order_by);
        }

        $states = $this->db->get();

    	if( $states->num_rows() > 0 )
    	{
    		return $states->result();

    	} 
		else 
		{

    		return FALSE;

    	}


    }//End of get_states function

    public function get_state_by_name($where = "")
    {
        $this->db->select('*');
        $this->db->from('states');
        $this->db->where($where);
        $states = $this->db->get();

    	if( $states->num_rows() > 0 )
    	{
    		return $states->row();
    	} 
		else 
		{
    		return FALSE;
    	}
    }

    public function get_lga_by_name($where = "")
    {
        $this->db->select('*');
        $this->db->from('lga');
        $this->db->where($where);
        $states = $this->db->get();

    	if( $states->num_rows() > 0 )
    	{
    		return $states->row();
    	} 
		else 
		{
    		return FALSE;
    	}
    }

   	public function get_state_by_id($where = "")
    {

        $this->db->select('*');
        $this->db->from('states');
        $this->db->where($where);
        $states = $this->db->get();

    	if( $states->num_rows() > 0 )
    	{
    		return $states->row();

    	} 
		else 
		{

    		return FALSE;

    	}


    }//End of get_states function

    public function get_merchant_state(){
        $this->db->select('name');
        $this->db->from('f_states');
        $states = $this->db->get();
        if($states->num_rows() > 0)
        {
            return $states->result();
        }else{
            return false;
        }
    }


    public function get_states_merchant($country_id = '')
    {

		$this->db->select('*');
		$this->db->from('states_merchant');
		
		if($country_id == '')
		$this->db->where('country_id', 'NG');
		else
		$this->db->where('country_id', $country_id);
				 
		$this->db->order_by("name", "asc");
		$states = $this->db->get();

    	if( $states->num_rows() > 0 )
    	{
    		return $states->result();

    	} 
		else 
		{

    		return FALSE;

    	}


    }//End of get_states function

    public function get_sh_cities($where)
    {

		$this->db->select('*');
		$this->db->from('sh_cities');
		$this->db->where($where);		 
		$states = $this->db->get();

    	if( $states->num_rows() > 0 )
    	{
    		return $states->row();

    	} 
		else 
		{

    		return FALSE;

    	}


    }//End of get_states function





    public function get_merchant_fedex_location($where)
    {

		$this->db->select('*');
		$this->db->from('merchant_fedex_location');
		$this->db->where($where);		 
		$states = $this->db->get();

    	if( $states->num_rows() > 0 )
    	{
    		return $states->row();

    	} 
		else 
		{

    		return FALSE;

    	}


    }//End of get_merchant_fedex_location function
	
	
    public function get_fedex($where)
    {

		$this->db->select('*');
		$this->db->from('fedex');
		$this->db->where($where);		 
		$states = $this->db->get();

    	if( $states->num_rows() > 0 )
    	{
    		return $states->row();

    	} 
		else 
		{

    		return FALSE;

    	}


    }//End of get_fedex function


    /**

     * checkuniqueness function

     * @param $table to be checked in

     * @param $field to be checked against uniqueness

     * @param $value to be checked against uniqueness

     * return true false

     */

    public function checkuniqueness($table ='', $field = '', $value = '')

    {

        if( $table != '' && $field != '' && $value  != '')

        { 

            $uniqecheck = $this->db->get_where($table, array($field => $value), 1);

           

            if( $uniqecheck->num_rows() > 0 )

            {

            	return TRUE;

            } else {

            	return FALSE;

            }



        } else {

            return TRUE;

        }



    }//End of checkuniqueness function

    public function insert_merchant_contactus($data)
    {
    	
		$this->db->insert('contact_us',$data);	
		return $this->db->insert_id();

    }//End of insert function

	

	public function search($limit='', $p_name = 'product_name', $keyword ='')
	{

        $query= $this->db->query("SELECT p.* FROM products p JOIN users m on $p_name LIKE '%$keyword%' AND m.isactive='t' AND p.isactive='t' and m.id=p.pid and p.product_name!= '' and p.product_name!= '-' AND m.first_name !='' AND m.display_name!='' AND m.display_name!='-' order by Add_Date DESC");

//        echo $this->db->last_query();
		
		if( $query->num_rows() > 0 ){
			
			return $query->result();

		}else{

			return FALSE;
		}
		
	}
    public function search_id($id)
    {

        $query= $this->db->query("SELECT p.* FROM products p JOIN users m on p.id =$id AND m.isactive='t' AND p.isactive='t' and m.id=p.pid and p.product_name!= '' and p.product_name!= '-' AND m.first_name !='' AND m.display_name!='' AND m.display_name!='-'  order by Add_Date DESC");

//            echo $this->db->last_query();

        if( $query->num_rows() > 0 ){

            return $query->result();

        }else{

            return FALSE;
        }

    }
	
	public function add_subscribe($data)
	{

		$this->db->insert('subscribe',$data);	

		return $this->db->insert_id();



	} //end of add_subscribe function
	
	public function check_subscribe($where)
	{

		$this->db->select('*')->from('subscribe')->where($where);

		$data = $this->db->get();

		if( $data->num_rows() > 0 ){

			return $data->row();

		} else {

			return FALSE;

		}

	} //end of check_subscribe function



    /**********  coupon   ************/

    public function get_single_coupon($where = "")
    {
        $this->db->select('*');

        $this->db->from('coupon');

        if($where != "")
        $this->db->where($where);

        $this->db->limit(1);

        $data = $this->db->get();

        if( $data->num_rows() > 0 ){

            return $data->row();

        } else {

            return FALSE;

        }

    } //end of get_single_coupon function

    public function get_single_reward_coupon($where = "", $between = "")
    {
        $this->db->select('*');

        $this->db->from('reward_coupon');

        if($where != "")
        $this->db->where($where);

        if($between != "")
        $this->db->where(" now() BETWEEN `start_date` AND `expire_date` ");

        $this->db->limit(1);

        $data = $this->db->get();

        if( $data->num_rows() > 0 ){

            return $data->row();

        } else {

            return FALSE;

        }
    }//end of get_single_reward_coupon function

    public function add_coupon($data)
    {

        $this->db->insert('coupon',$data);   

        return $this->db->insert_id();


    } //end of add_coupon function
    
    public function update_reward_coupon($data, $where = "")
    {

        $this->db->where($where);

        $this->db->update('reward_coupon', $data); 

        return true; 

    } //end of update_reward_coupon function

    public function miscellaneous($pageId)
    {
        $this->db->select('*');

        $this->db->from('miscellaneous');

        $this->db->where(array('id' => $pageId ));

        $this->db->limit(1);

        $data = $this->db->get();

        if( $data->num_rows() > 0 ){

            return $data->row();

        } else {

            return FALSE;

        }
    }//end of miscellaneous function


}// end of class...