<?php

class User_model extends CI_Model {

    function __construct()

    {
        parent::__construct();
    }

    public function checkUser($username){
        $this->db->reconnect();
        $query = $this->db->get_where('admin', array('username' => $username));
        return $query->result_array();
    }
    public function login($data){
        $this->db->reconnect();
        $query = $this->db->get_where('admin', array('email' => $data['email'], 'password' => md5($data['password'])));

        return $query->result_array();
    }
    public function checkMerchant($username){
//        $this->db->reconnect();
        $query = $this->db->get_where('users', array('email' => $username), 1);
        return $query->result_array();
    }
    public function MerchantLogin($data){
//        $this->db->reconnect();
        $query = $this->db->get_where('users', array('email' => $data['email'], 'password' => md5($data['password'])),1);
        return $query->result_array();
    }

    public function device_status($data){
        $this->db->reconnect();
        $query = $this->db->insert('device_status',$data);
        return $query;
    }
    public function insert_user($data)
    {
    	
		$this->db->insert('users',$data);	
		return $this->db->insert_id();

    }//End of insert function

    public function update_user($data, $id)
    {

		$this->db->where('id', $id);
		$this->db->update('users', $data); 
		return true; 
		
    }//End of update user function


	public function all_users_list()
	{
		$this->db->select('merchant.*, users.id as userId, users.numMerchantId, users.txtName, users.txtDisplayName, users.txtEmail ,users.txtPhone,users.numIsactive as userIsactive ');

		$this->db->from('merchant');
		
		$this->db->join('users', 'merchant.id = users.numMerchantId','right');		 
		
		$this->db->order_by("users.dateCreated", "asc");

		$data = $this->db->get();

		if( $data->num_rows() > 0 ){

			return $data->result();

		} else {

			return FALSE;

		}

	} //end of all_users_list function

	public function single_user_detail($where, $orWhere = '')
	{
		$this->db->select('*');
		
		$this->db->from('users');

		$this->db->where($where);

		if($orWhere != '')
		{
			$this->db->or_where($orWhere); 

		}
		$this->db->limit(1);

		$data = $this->db->get();

		if( $data->num_rows() > 0 ){

			return $data->row();

		} else {

			return FALSE;

		}

	} //end of all_users_list function



	public function single_user_detail_not_equle_id($id, $firstName, $email)
	{
		$data = $this->db->query("SELECT * FROM `users` WHERE `id` != '".$id."' AND (`email` = '".$id."' OR `first_name` = '".$email."' ) LIMIT 1");
		
		
		if( $data->num_rows() > 0 ){

			return $data->row();

		} else {

			return FALSE;

		}

	} //end of all_users_list function



	
	public function single_merchant_detail($where)
	{
		$this->db->select('users.*, category.category_name')->from('users')->where($where);

		$this->db->join('category', 'users.category = category.id', 'LEFT');		 

		$data = $this->db->get(); 
		//echo "<!---".$this->db->last_query()."!-->";
		
		if( $data->num_rows() > 0 ){

			return $data->row();

		}
		else {

			return FALSE;

		}

	} //end of single_merchant_detail function


	public function single_merchant_detail_by_product($where)
	{
		$this->db->select('users.*')->from('users')->where($where);

		$this->db->join('products', 'users.id = products.pid');		 

		$data = $this->db->get();

		if( $data->num_rows() > 0 ){

			return $data->row();

		}
		else {

			return FALSE;

		}

	} //end of single_merchant_detail function



	public function transaction_by_id($where, $limit='', $start=0)
	{
		
		if($limit != '')
		{
			
			$this->db->limit($limit, $start);
		}
		

		$this->db->select('order.*,shipping_addres_list.email, shipping_addres_list.phone, shipping_addres_list.delivery_addres, shipping_addres_list.delivery_area, shipping_addres_list.contact_no_delivery, users.id AS user_id, products.pid As product_user_id ');
		
		$this->db->from('order');

		$this->db->join('shipping_addres_list', 'shipping_addres_list.id = order.shipping_add_id', 'left');

		$this->db->join('cart', 'cart.order_id = order.order_id', 'inner');
		
		$this->db->join('products', 'products.id = cart.product_id', 'left');
		
		$this->db->join('users', 'users.id = products.pid');
		
		$this->db->where($where); 

		$this->db->order_by("order.id", "desc"); 
		
		$this->db->group_by("transaction_ref");

		$data = $this->db->get(); 
        echo$this->db->last_query();
		//print_r($data->result()); die();
		 
		if( $data->num_rows() > 0 ){

			return $data->result();

		}
		else {

			return FALSE;

		}

	} //end of transaction_by_id function



	public function single_shipping_detail($where)
	{
		$this->db->select('*')->from('shipping_address')->where($where);

		$data = $this->db->get();

	

		if( $data->num_rows() > 0 ){

			return $data->row();

		} else {

			return FALSE;

		}

	} //end of single_shipping_detail function


	public function insert_shipping_detail($data)
    {

		$this->db->insert('shipping_address',$data);	
		return $this->db->insert_id();


    }//End of insert_shipping_detail function

	public function update_shipping_detail($data, $where)
    {

		$this->db->where($where);
		$this->db->update('shipping_address', $data); 
		return true; 

    }//End of update_shipping_detail function

	public function get_single_shipping_address($where)
	{

		$this->db->select('*');
		$this->db->from('shipping_address');
		$this->db->where($where);

		$data = $this->db->get();
		

		if( $data->num_rows() > 0 ){

			return $data->row();

		} else {

			return FALSE;

		}

	} //end of get_single_shipping_address function

	public function get_shipping_address($where)
	{
		$this->db->select('*')->from('shipping_addres_list')->where($where);

		$data = $this->db->get();

		if( $data->num_rows() > 0 ){

			if( $data->num_rows() > 1 )
			return $data->result();
			
			if( $data->num_rows() == 1 )
			return $data->row();

		} else {

			return FALSE;

		}

	} //end of get all shipping address function

    public function remove_address($id)
    {

        $this->db->where('id', $id);
        $this->db->update('shipping_addres_list', array('display'=>1));
//        echo $this->db->last_query();
        return true;

    }
	
	public function insert_shipping_address($data)
    {

		$this->db->insert('shipping_addres_list',$data);
		//var_dump($data);	
		return $this->db->insert_id();


    }//End of insert_shipping_address function

	public function single_shipping_address($where, $start = 0, $limit = '')
	{
		if($limit != '')
		{
			$this->db->limit( $start, $limit);
		}
		$this->db->select('*');
		$this->db->from('shipping_addres_list');
		$this->db->where($where);

		$data = $this->db->get();

		//$this->db->last_query();

		if( $data->num_rows() > 0 ){
			return $data->row();

		} else {

			return FALSE;

		}

	} //end of single_shipping_detail function
	
	public function get_shipping_add_by_orderid($where)
	{
		
		$this->db->select('shipping_addres_list.*, order.delivery_amt,order.total_ammount,order.redeemed_total');
		$this->db->from('shipping_addres_list');
		$this->db->join('order', 'order.shipping_add_id = shipping_addres_list.id');
		$this->db->where($where);
		$data = $this->db->get();
		//$this->db->last_query();

		
		if( $data->num_rows() > 0 ){

			return $data->row();

		} else {

			return FALSE;

		}

	} //end of single_shipping_detail function
	
	//get all courier
	public function get_all_courier()
	{
		$this->db->select('*')->from('shipping_method');
		$courier = $this->db->get();
		
		
		if($courier->num_rows() > 0)
		{
			
			return $courier->result();
		}else{
			return FALSE;
		}

		
	}
	public function get_courier($where)
		{
			$this->db->select('courier_code')->from('shipping_method');
			$this->db->where(['courier_name' => $where]);
			$courier = $this->db->get();
			if($courier->num_rows() > 0)
			{
				return $courier->row();
			}else{
				return false;
			}
		}
	
	

}// End of class...