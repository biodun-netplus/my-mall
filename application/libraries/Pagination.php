<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/**

 * CodeIgniter

 *

 * An open source application development framework for PHP 4.3.2 or newer		CodeIgniter

 * @author		ExpressionEngine Dev Team

 * @copyright	Copyright (c) 2006, EllisLab, Inc.

 * @license		http://codeigniter.com/user_guide/license.html

 * @link		http://codeigniter.com

 * @since		Version 1.0

 * @filesource

 */



// ------------------------------------------------------------------------



/**

 * Pagination Class		CodeIgniter

 * @subpackage	Libraries

 * @category	Pagination

 * @author		ExpressionEngine Dev Team

 * @link		http://codeigniter.com/user_guide/libraries/pagination.html

 */

class CI_Pagination {



	var $base_url			= ''; // The page we are linking to. If blank, uses the CI current_url() function

	var $nonajax_url		= ''; // Non-ajax url. If blank, uses the base url

	var $total_rows  		= ''; // Total number of items (database results)

	var $per_page	 		= 25; // Max number of items you want shown per page

	var $this_page			= ''; // Number of items on the current page (deprecated?)

	var $num_links			=  2; // Number of "digit" links to show before/after the currently viewed page

	var $cur_page	 		=  0; // The current page being viewed (a holdover from the old class method)

	var $uri_page_number	=  1; // page number as displayed in the url

	var $cur_offset			=  0;

	var $first_link   		= '&lt;&lt;';

	var $next_link			= '&gt;';

	var $prev_link			= '&lt;';

	var $last_link			= '&gt;&gt;';

	var $uri_segment		= 3;

	var $use_query_string	= TRUE;

	var $querystring_items	= array();

	var $query_page_seg		= 'p';

	var $query_per_seg		= 'per';



	/**

	 * Wrapper

	 */

	var $full_tag_open		= '<div id="paginate_wrapper" class="">';

	var $full_tag_close		= '</div>';



	/**

	 * Showing X to Y of Z

	 */

	var $showing_tag_open	= '<div id="paginate_info" class="">';

	var $showing_tag_close	= '</div>';



	/**

	 * Page links wrapper

	 */

	var $paging_tag_open	= '<div class="pagination"><ul>';

	var $paging_tag_close1	= '</ul>';
	var $paging_tag_close2	= '</div>';



	/**

	 * First page

	 */

	var $first_tag_open		= '<li id="paginate_first" class="">';

	var $first_tag_close	= '</li>';

	var $first_tag_open_disabled		= '<li id="paginate_first" class="disabled"><a href="javascript:;">';

	var $first_tag_close_disabled		= '</a></li>';



	/**

	 * Last page

	 */

	var $last_tag_open		= '<li id="paginate_last" class="">';

	var $last_tag_close		= '</li>';

	var $last_tag_open_disabled			= '<li id="paginate_last" class="disabled"><a href="javascript:;">';

	var $last_tag_close_disabled		= '</a></li>';



	/**

	 * Current page

	 */

	var $cur_tag_open		= '<li id="paginate_current" class=""><span>';

	var $cur_tag_close		= '</span></li>';



	/**

	 * Next page

	 */

	var $next_tag_open		= '<li id="paginate_next" class="">';

	var $next_tag_close		= '</li>';

	var $next_tag_open_disabled		= '<li id="paginate_next" class="disabled"><a href="javascript:;">';

	var $next_tag_close_disabled	= '</a></li>';



	/**

	 * Previous page

	 */

	var $prev_tag_open		= '<li id="paginate_previous" class="">';

	var $prev_tag_close		= '</li>';

	var $prev_tag_open_disabled		= '<li id="paginate_previous" class="disabled"><a href="javascript:;">';

	var $prev_tag_close_disabled	= '</a></li>';



	/**

	 * Individual numbers

	 */

	var $num_tag_open		= '<li class="">';

	var $num_tag_close		= '</li>';



	// Added By Tohin

	var $js_rebind 			= '';

	var $div                = ''; // selector of the element the AJAX content will be placed in. Leave blank to not use AJAX-prepared links

	var $postVar            = '';

    var $additional_param	= '';



	/**

	 * Constructor

	 *

	 * @access	public

	 * @param	array	initialization parameters

	 */

	function __construct($params = array()) {



		// Determine the current page number and per page.

		$CI =& get_instance();

		if ($CI->input->get($this->query_page_seg)) {

			$this->uri_page_number = (int)$CI->input->get($this->query_page_seg);

			$this->cur_page = $this->uri_page_number - 1;

		}

		else {

			$this->uri_page_number = 1;

			$this->cur_page = 0;

		}

		if ($CI->input->get($this->query_per_seg)) {

			$this->per_page = (int)$CI->input->get($this->query_per_seg);

		}



		// Load the configuration parameters

		if (count($params) > 0)

		{

			$this->initialize($params);

		}



		if (empty($this->base_url)) {

			$CI->load->helper('url');

			$this->base_url = current_url();

		}

		if (empty($this->nonajax_url)) {

			$this->nonajax_url = $this->base_url;

		}



		// calculate the offset, for use in the DB query

		$this->cur_offset = ($this->uri_page_number > 1) ? ($this->cur_page * $this->per_page) : 0;



		if ($CI->input->get()) {

			$get = $CI->input->get();

			foreach ($get as $key => $val) {

				$this->querystring_items[$key] = $val;

			}

		}



		log_message('debug', "Pagination Class Initialized");

	}



	// --------------------------------------------------------------------



	/**

	 * Initialize Preferences

	 *

	 * @access	public

	 * @param	array	initialization parameters

	 * @return	void

	 */

	function initialize($params = array()) {

		if (count($params) > 0)

		{

			foreach ($params as $key => $val)

			{

				if (isset($this->$key))

				{

					$this->$key = $val;

				}

			}

			

		// calculate the offset, for use in the DB query, added by Ocean to intialize value

		$this->cur_offset = ($this->uri_page_number > 1) ? ($this->cur_page * $this->per_page) : 0;

		}

	}



	function num_pages() {

		return ceil($this->total_rows / $this->per_page);

	}



	// --------------------------------------------------------------------



	/**

	 * Adds a single key/value pair or an array of them to the query string

	 * @param $key string|array

	 * @param $value string

	 */

	function add_querystring($key, $value) {



		if ( is_array($key) ) {

			foreach ($key as $k => $v) {

				$this->querystring_items[$k] = $v;

			}

		}



		else {

			$this->querystring_items[$key] = $value;

		}



	}



	// --------------------------------------------------------------------



	/**

	 * Assemble the query string from an array of items

	 * @return string

	 */

	function query_string() {

		if (empty($this->querystring_items)) {

			return NULL;

		}

		$temparray = array();

		foreach ($this->querystring_items as $key => $value) {

			$temparray[] = $key.'='.urlencode($value);

		}

		return '?' . implode('&amp;', $temparray);

	}



	// --------------------------------------------------------------------



	/**

	 * Generate the pagination links

	 *

	 * @access	public

	 * @return	string

	 */

	function create_links()

	{		// If our item count or per-page total is zero there is no need to continue.

		 if ($this->total_rows == 0 OR $this->per_page == 0)

		{

		   return '';

		}



		$CI =& get_instance();

		if ($CI->input->get($this->query_per_seg)) {

			$this->querystring_items[$this->query_per_seg] = $CI->input->get($this->query_per_seg);

		}



		// Calculate the total number of pages

		$num_pages = ceil($this->total_rows / $this->per_page);

		$this->num_pages = $num_pages;



		// Is there only one page? Hm... nothing more to do here then.

		if ($num_pages == 1)

		{

            $info = 'Showing : ' . $this->total_rows . ' of ' . $this->total_rows;

			//return $info;

		}



//		if ($CI->uri->segment($this->uri_segment) != 0)

//		{

//			$this->cur_page = $CI->uri->segment($this->uri_segment);

//

//			// Prep the current page - no funny business!

//			$this->cur_page = (int) $this->cur_page;

//		}

		$this->cur_page = (int)$this->cur_page;



		$this->num_links = (int)$this->num_links;



		if ($this->num_links < 1)

		{

			show_error('Your number of links must be a positive number.');

		}



		if ( ! is_numeric($this->cur_page))

		{

			$this->cur_page = 0;

		}



		// Is the page number beyond the result range?

		// If so we show the last page

		if ($this->cur_page > $this->total_rows)

		{

			$this->cur_page = ($num_pages - 1) * $this->per_page;

		}



		$uri_page_number = $this->uri_page_number;

		$this->cur_page = floor(($this->cur_page/$this->per_page) + 1);



		// Calculate the start and end numbers. These determine

		// which number to start and end the digit links with

		$start = (($this->uri_page_number - ($this->num_links - 1)) > 1) ? $this->uri_page_number - ($this->num_links - 1) : 1;

		$end   = (($this->uri_page_number + $this->num_links) < $num_pages) ? $this->uri_page_number + $this->num_links : $num_pages;



		// Add a trailing slash to the base URL if needed

		//$this->base_url = rtrim($this->base_url, '/') .'/';

		//$this->nonajax_url = rtrim($this->nonajax_url, '/') . '/';



  		// And here we go...

		$output = '';



        // SHOWING LINKS



        $curr_offset = $this->cur_offset;

        $info = 'Showing ';

		$info .= $curr_offset + 1;

		$info .= ' to ' ;



        if( ( ($curr_offset) + $this->per_page ) < ( $this->total_rows ) )

            $info .= (($curr_offset) + $this->per_page);

        else

            $info .= $this->total_rows;



        $info .= ' of ' . $this->total_rows;



        //$output .= $this->showing_tag_open . $info . $this->showing_tag_close;



		if ($num_pages == 1) {

			//return $this->full_tag_open.$output.$this->full_tag_close;

		}



		$output .= $this->paging_tag_open;

		// Render the "First" link

		if  ($this->uri_page_number > 1 AND $num_pages > 1)

		{

			$output .= $this->first_tag_open

					. $this->getAJAXlink( '1' , $this->first_link)

					. $this->first_tag_close;

		}

		else

		{

			$output .= $this->first_tag_open_disabled

					. $this->first_link

					. $this->first_tag_close_disabled;

		}



		// Render the "previous" link

		if  ($this->uri_page_number != 1)

		{

			$i = $uri_page_number - 1;

			if ($i == 0) $i = '';

			$output .= $this->prev_tag_open

					. $this->getAJAXlink( $i, $this->prev_link )

					. $this->prev_tag_close;

		}

		else {

			$i = $uri_page_number - 1;

			if ($i == 0) $i = '';

			$output .= $this->prev_tag_open_disabled

					. $this->prev_link

					. $this->prev_tag_close_disabled;

		}



		// Write the digit links

		for ($loop = $start -1; $loop <= $end; $loop++)

		{

			$i = ($loop * $this->per_page) - $this->per_page;



			if ($i >= 0)

			{

				if ($this->uri_page_number == $loop)

				{

					if ($num_pages > 1) {

						$output .= $this->cur_tag_open.$loop.$this->cur_tag_close; // Current page

					}

				}

				else

				{

					$n = ($i == 0) ? '' : $i;

					$output .= $this->num_tag_open

						. $this->getAJAXlink( $loop, $loop )

						. $this->num_tag_close;

				}

			}

		}



		// Render the "next" link

		if ($this->uri_page_number < $num_pages)

		{

			$output .= $this->next_tag_open

				. $this->getAJAXlink( $this->uri_page_number + 1 , $this->next_link )

				. $this->next_tag_close;

		}

		else {

			$output .= $this->next_tag_open_disabled

				. $this->next_link

				. $this->next_tag_close_disabled;

		}



		// Render the "Last" link

		if ($end <= $num_pages AND $this->uri_page_number != $num_pages)

		{

			//$i = (($num_pages * $this->per_page) - $this->per_page);

			$i = $num_pages;

			$output .= $this->last_tag_open . $this->getAJAXlink( $i, $this->last_link ) . $this->last_tag_close;

		}

		else {

			$i = $num_pages;

			$output .= $this->last_tag_open_disabled . $this->last_link . $this->last_tag_close_disabled;

		}

		$output .= $this->paging_tag_close1;
		$output .= $this->showing_tag_open . $info . $this->showing_tag_close;
		$output .= $this->paging_tag_close2;



		// Kill double slashes.  Note: Sometimes we can end up with a double slash

		// in the penultimate link so we'll kill all double slashes.

		$output = preg_replace("#([^:])//+#", "\\1/", $output);



		// Add the wrapper HTML if exists

		$output = $this->full_tag_open.$output.$this->full_tag_close;



		return $output;

	}



	function getAJAXlink( $count, $text) {

		$this->querystring_items[$this->query_page_seg] = $count;

		$url = $this->base_url . $this->query_string();

		$nonajax_url = $this->nonajax_url . $this->query_string();



        if( $this->div == '')

            return '<a href="'. $url . '">'. $text .'</a>';



        if( $this->additional_param == '' )

        	$this->additional_param = "{'t' : 't'}";





		return "<a href=\"".$nonajax_url."\" class=\"ajaxlink\" rel=\"".$url."\">"

				. $text .'</a>';

	}



	function getSortLink($param, $dir = 'asc') {

		$this->querystring_items[$this->query_page_seg] = 1;

		$this->querystring_items['sortby'] = $param;

		$this->querystring_items['dir'] = $dir;

		return $this->base_url . $this->query_string();

	}



	function getLastPage() {

		$this->querystring_items[$this->query_page_seg] = $this->num_pages();

		return $this->base_url . html_entity_decode($this->query_string());

	}



	public function getAJAXcode() {

		$output  = "<script>\n";

		$output .= "$(document).ready(function() {\n";

		$output .= "	$('#$this->div .ajaxlink').live('click', function() {\n";

		$output .= "		var url = $(this).attr('rel');\n";

		$output .= "		var parentdiv = $('#$this->div');\n";

		$output .= "		$(parentdiv).showLoading();\n";

		$output .= "		setTimeout(function(){\n";

		$output .= "			$(parentdiv).load(url, function() {\n";

		$output .= "				$('.button.icon', parentdiv).each(function(){\n";

		$output .= "					iconifyit($(this));\n";

		$output .= "				});\n";

		$output .= "				$(parentdiv).hideLoading();\n";

		$output .= "			});\n";

		$output .= "		}, 1000);\n";

		$output .= "		return false;\n";

		$output .= "	});\n";

		$output .= "});\n";

		$output .= "</script>\n";



		return $output;

	}



	function getLimit() {



	}



}

// END Pagination Class