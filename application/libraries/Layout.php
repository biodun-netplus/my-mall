<?php

/**

 * CodeIgnighter layout support library

 *  with Twig like inheritance blocks

 *

 *

 */


if (!defined('BASEPATH')) exit('No direct script access allowed');

class Layout {
    
	private $obj;

    private $layout_view;

    private $title = '',  $scripts = '';

    private $css_list = array(), $js_list = array();

    private $block_list, $block_new, $block_replace = false;
	
    function Layout() {

		$this->obj =& get_instance();

        $this->layout_view = "layout/default.php";
		
		$this->obj->load->model('category_model');
		$this->obj->load->model('home_model');
		$this->obj->load->model('common_model');
		$this->obj->load->model('product_model');

        // Grab layout from called controller

        if (isset($this->obj->layout_view)) $this->layout_view = $this->obj->layout_view;

    }

    function view($view, $data = null, $return = false) {

 
        $data['category'] = $this->obj->category_model->all_category_list(array('isactive' => 't'));
		$data['merchant'] = $this->obj->home_model->all_merchant(array('isactive'=>'t'), 'ASC');
		//var_dump($data);
		// Render template
		$data['content_for_layout'] = $this->obj->load->view($view, $data, true);
		
		$data['script_for_layout'] = '';
		
        if($this->scripts && $this->scripts != '')
		{
			$data['script_for_layout'] = $this->scripts;
		}

        // Render resources

        $data['js_for_layout'] = '';

        foreach ($this->js_list as $v)

            $data['js_for_layout'] .= sprintf('<script type="text/javascript" src="%s"></script>', $v);
			
			
		if($this->title && $this->title != '')
		{
			$data['title_for_layout'] = $this->title;
		}
		else
		{
			$data['title_for_layout'] = 'Your one-stop online mall for electronics, fashion, home appliances, phones, services and more | MyMall Nigeria';
		}	



        $data['css_for_layout'] = '';

        foreach ($this->css_list as $v)

            $data['css_for_layout'] .= sprintf('<link rel="stylesheet" type="text/css"  href="%s" />', $v);


        // Render template

        $this->block_replace = true;

        $output = $this->obj->load->view($this->layout_view, $data, $return);


        return $output;

    }


    /**

     * Set page title

     *

     * @param $title

     */

    function title($title) {

        $this->title = $title;

    }


    /**

     * Adds Javascript resource to current page

     * @param $item

     */

    function js($item) {

        $this->js_list[] = $item;

    }





    /**

     * Adds custom Javascript resource to current page

     * @param $script

     */

    function script($script) {

        $this->scripts = $script;

    }



    /**

     * Adds CSS resource to current page

     * @param $item

     */

    function css($item) {

        $this->css_list[] = $item;

    }



    /**

     * Twig like template inheritance

     *

     * @param string $name

     */

    function block($name = '') {

        if ($name != '') {

            $this->block_new = $name;

            ob_start();

        } else {

            if ($this->block_replace) {

                // If block was overriden in template, replace it in layout

                if (!empty($this->block_list[$this->block_new])) {

                    ob_end_clean();

                    echo $this->block_list[$this->block_new];

                }

            } else {

                $this->block_list[$this->block_new] = ob_get_clean();

            }

        }

    }

} // end of class
