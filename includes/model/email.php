<?php
include '../classes/env.php';
require 'PHPMailerAutoload.php';

class mails
{
    public function registration($array, $password)
    {
        $to = $array['email'];
        $subject = "Registration Successful on WebmallNG";
        $headers = 'From:' . FROMEMAIL . "\r\n" .
            'Reply-To:' . FROMEMAIL . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
</head>
<body style="margin:0; padding:0; background-color:#25363F;">

	<div style="margin: 0 auto; width:600px;background-color: #fff;">
	<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:12px;  background-color:#FFFFFF;">
	<tr>
            <td colspan="6">
                <div style="background-color: #496E7B;
                        width:100%; height:100px; border-bottom: solid 3px #ff9900; padding:20px 0">
                    <table>
                        <tr>
                            <td width="5%"></td>
                            <td width="60%">
                                <h1 style="color:#fff; font-size:36px; font-weight: normal">Congratulations!</h1>
                            </td>
                            <td width="30%">
                                <img src="../../images/logo.png" />
                            </td>
                            <td width="5%"></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>

        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <p>
                    Dear <strong>' . $array['first_name'] . '</strong>,<br /><br />
                    Thank you for registering your business with WebMall, your store details are listed below.
                </p>
            </td>
        </tr>

        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <table width="100%">
                    <tr>
                        <td width="40%" height="30">Username</td>
                        <td width="60%">
                        <div style="background-color:#496E7B; color: #fff; height:30px; line-height:30px;
                            margin-bottom: 5px;padding-left: 5px">
                                ' . $array['email'] . '
                            </div>
                        </td>

                    </tr>
                    <tr>
                        <td width="40%" height="30">Password</td>
                        <td width="60%">
                        <div style="background-color:#496E7B; color: #fff; height:30px; line-height:30px;
                            margin-bottom: 5px;padding-left: 5px">
                               ' . $_REQUEST['password'] . '
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <div style="border: solid 1px #ccc; background-color: #f3f3f3; padding: 15px;">
                    <p>What happens next...</p>
                    <p>Your store is now open and is visible to all customers at
                        <strong><a href="www.webmallng.com/' . $array['display_name'] . '">www.webmallng.com/' . $array['display_name'] . '</a></strong></p>
                    <ol>
                        <li>Customize your store</li>
                        <li>Upload more products</li>
                        <li>Start Selling</li>
                    </ol>
                </div>
            </td>
        </tr>

        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <div style="background-color:#9AC434; height:40px; width:45%; text-align:center; color:#fff;
                line-height: 40px; float: left"><a href="" style="text-decoration:none;"> LOGIN TO YOUR ACCOUNT</a></div>

                <div style="background-color:#eec400; height:40px; width:45%; text-align:center; color:#fff;
                line-height: 40px; float: right"><a href="" style="text-decoration:none;">HOW IT WORKS</a></div>
            </td>
        </tr>
</table>

<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:15px; color:#7f8c8d; background-color:#FFFFFF;">	<tr>
		<td width="600" style="text-align:center;">



<p style="border-top:1px solid #cccccc; padding-top:10px;background-color:#f3f3f3;font-size:12px;padding-bottom:10px;">
	<a href="http://www.facebook.com/webmallng"><img src="' . SITE_URL . 'images/email/fb.gif" alt="Facebook" border="none" /></a>
	<a href="http://www.twitter.com/webmallng"><img src="' . SITE_URL . 'images/email/tw.gif" alt="Twitter" border="none" /></a>
	<a href="http://www.instagram.com/webmallng"><img src="' . SITE_URL . 'images/email/ig.gif" alt="Instagram" border="none" /></a>
	<a href="http://www.youtube.com/webmallng"><img src="' . SITE_URL . 'images/email/yt.gif" alt="YouTube" border="none" /></a>
	<a href="https://plus.google.com/u/0/106291413256291053539/posts"><img src="' . SITE_URL . 'images/email/gp.gif" alt="GooglePlus" border="none" /></a>
<br>
<br>
	For more information, please call us on <strong>0818 778 2542</strong> or send an email to <strong><a href="mailto:info@webmallng.com" style="color:#000000; text-decoration:none;">info@webmallng.com</a></strong>
</p>
</td>

</table>
</div>
</body>
</html>';

        try{
            mail($to,$subject,$message, $headers);
        } catch (Exception $e) {
            echo $e;
        }

    }

    public function firstRegistration($array)
    {
        $to = $array['email'];
        $subject = "Welcome to WebmallNG";
        $headers = 'From:' . FROMEMAIL . "\r\n" .
            'Reply-To:' . FROMEMAIL . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Congratulations and Welcome to WebMall</title>
</head>

<body style="margin:0;padding:0;background:#25363F;">
	<table align="center" width="600" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif;font-size:12px;border:1px solid #ccc;">
	<tr>
            <td colspan="6">
                <div style="background-color: #496E7B;
                        width:100%; height:100px; border-bottom: solid 3px #ff9900; padding:20px 0">
                    <table>
                        <tr>
                            <td width="5%"></td>
                            <td width="60%">
                                <h1 style="color:#fff; font-size:36px; font-weight: normal">Congratulations!</h1>
                            </td>
                            <td width="30%">
                                <img src="../../images/logo.png" />
                            </td>
                            <td width="5%"></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>

	<tr>
		<td width="600" style="text-align:center;">
			<img src="' . SITE_URL . 'images/email/webmall-welcome-banner.jpg" border="none" alt="I would like to personally welcome you to our merchant community" />
		</td>
	</tr>

	<tr >
		<td width="600" style="text-align:center;font-family: \'Calibri\', Arial, sans-serif">
			<h2 style="color:#496E7B;">CONGRATULATIONS AND WELCOME TO WEBMALL!</h2>

			<p style="margin:0px auto;width:525px; text-align:center; font-size:15px; line-height:normal; font-family:\'Calibri\', Arial; color:#000000;">
			As CEO, I would like to personally welcome you to your new <strong><a href="' . SITE_URL . 'sell-online" style="color:#000000;">Merchant Community.</a> &trade;</strong><br/><br/>

Exciting times are ahead and we look forward to sharing a new season on our platform. We are busy making modifications, updates and promoting the platform and your individual stores. Our common goal is to create maximum exposure for your business leading to more brand awareness and new customers.<br/><br/>

I encourage you to contact us with questions about the platform, operation of your store, pricing or features and services. We will also regularly update you with tips and news about technological innovations and major events in the e-commerce space in Nigeria and the rest of the World.<br/><br/>

You are now part of the <strong><a href="' . SITE_URL . '" style="color:#000000;">WebMall</a></strong> family and we pledge to help take your business to the next step and make a statement on the web.<br/><br/>

Sincerely, <br/>
<strong>Wole Faroun, CEO</strong></p>
</td>
</tr>
</table>

</div>

</table>

<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:15px; color:#7f8c8d; background-color:#FFFFFF;">	<tr>
		<td width="600" style="text-align:center;">



<p style="border-top:1px solid #cccccc; padding-top:10px;background-color:#f3f3f3;font-size:12px;padding-bottom:10px;">
	<a href="http://www.facebook.com/webmallng"><img src="' . SITE_URL . 'images/email/fb.gif" alt="Facebook" border="none" /></a>
	<a href="http://www.twitter.com/webmallng"><img src="' . SITE_URL . 'images/email/tw.gif" alt="Twitter" border="none" /></a>
	<a href="http://www.instagram.com/webmallng"><img src="' . SITE_URL . 'images/email/ig.gif" alt="Instagram" border="none" /></a>
	<a href="http://www.youtube.com/webmallng"><img src="' . SITE_URL . 'images/email/yt.gif" alt="YouTube" border="none" /></a>
	<a href="https://plus.google.com/u/0/106291413256291053539/posts"><img src="' . SITE_URL . 'images/email/gp.gif" alt="GooglePlus" border="none" /></a>
<br>
<br>
	For more information, please call us on <strong>0818 778 2542</strong> or send an email to <strong><a href="mailto:info@webmallng.com" style="color:#000000; text-decoration:none;">info@webmallng.com</a></strong>
</p>
</td>

</table>
</div>
</body>
</html>
';

        try{
            mail($to,$subject,$message, $headers);
        } catch (Exception $e) {
            echo $e;
        }


        print_r($message);

    }

    public function customer_order_payondelivery($array, $display)
    {
        $to = $array['email'];
        $subject = "Your WebmallNG Order";
        $headers = 'From:' . FROMEMAIL . "\r\n" .
            "Cc: kemi@netplusadvisory.com \r\n" .
            'Reply-To:' . FROMEMAIL . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Order Request</title>
</head>

<body style="margin:0; padding:0; background-color:#25363F;">

	<div style="margin: 0 auto; width:600px;background-color: #fff;">
	<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:12px;  background-color:#FFFFFF;">
	<tr>
            <td colspan="6">
                <div style="background-color: #496E7B;
                        width:100%; height:100px; border-bottom: solid 3px #ff9900; padding:20px 0">
                    <table>
                        <tr>
                            <td width="5%"></td>
                            <td width="60%">
                                <h1 style="color:#fff; font-size:36px; font-weight: normal">Congratulations!</h1>
                            </td>
                            <td width="30%">
                                 <img src="../../images/logo.png" />
                            </td>
                            <td width="5%"></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
	<tr>
            <td colspan="6" style="padding:10px 20px;">
                <p>
                    Dear <strong>' . $array["name"] . ' </strong>,<br />
                   Your order is now being processed and will be delivered within two to five days
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <table width="100%">
                    <tr>
                        <td width="20%" height="30">Order Id</td>
                        <td width="60%">
                            <div style=" height:30px; line-height:30px;
                            padding: 0 10px;">
                                                 ' .
            $array["order_id"]
            . '
                            </div>
                        </td>
                        <td width="20%"></td>
                    </tr>

                </table>
            </td>
        </tr>

        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <div style="border: solid 1px #ccc; background-color: #f3f3f3; ">
                    <p style="margin-left: 10px;margin-top: 20px"><b>Product(s) Information</b></p>
<table width="97%" style="margin: 0px auto;font-size: 12px">
                    <tr style=" background-color:#d3d3d3;color: white ">
                        <th width="12%" height="30"></th>
                        <th width="40%" height="30"></th>
                        <th width="12%" height="30">Item Price</th>
                        <th width="12%" height="30">Quantity</th>
                        <th width="12%" height="30">Size</th>
                        <th width="12%" height="30">Total</th>
                        </tr>
                        <tr>
            ' .
            $display
            . '

</tr>
                </table>
                </div>

            </td>
        </tr>


</table>

<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:15px; color:#7f8c8d; background-color:#FFFFFF;">	<tr>
		<td width="600" style="text-align:center;">



<p style="border-top:1px solid #cccccc; padding-top:10px;background-color:#f3f3f3;font-size:12px;padding-bottom:10px;">
	<a href="http://www.facebook.com/webmallng"><img src="' . SITE_URL . 'images/email/fb.gif" alt="Facebook" border="none" /></a>
	<a href="http://www.twitter.com/webmallng"><img src="' . SITE_URL . 'images/email/tw.gif" alt="Twitter" border="none" /></a>
	<a href="http://www.instagram.com/webmallng"><img src="' . SITE_URL . 'images/email/ig.gif" alt="Instagram" border="none" /></a>
	<a href="http://www.youtube.com/webmallng"><img src="' . SITE_URL . 'images/email/yt.gif" alt="YouTube" border="none" /></a>
	<a href="https://plus.google.com/u/0/106291413256291053539/posts"><img src="' . SITE_URL . 'images/email/gp.gif" alt="GooglePlus" border="none" /></a>
<br>
<br>
	For more information, please call us on <strong>0818 778 2542</strong> or send an email to <strong><a href="mailto:info@webmallng.com" style="color:#000000; text-decoration:none;">info@webmallng.com</a></strong>
</p>
</td>

</table>
</div>
</body>
</html>
';
/*print_r($message);
        print_r(SITE_URL);*/
        try{
            mail($to,$subject,$message, $headers);
        } catch (Exception $e) {
            echo $e;
        }

    }

    public function customer_order_onlinePayment($array)
    {
        $to = $array['email'];
        $subject = $array['header'] . " #" . $array['oid'] . "";
        $headers = 'From:' . FROMEMAIL . "\r\n" .
            "Cc: kemi@netplusadvisory.com \r\n" .
            'Reply-To:' . FROMEMAIL . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Order Successfully Placed</title>
	</head>

	<body style="margin:0;padding:0;background:#efefef;">
	<table align="center" width="600" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif;font-size:12px;border:1px solid #ccc;">
	  <!--DWLayoutTable-->
	  <tr>
		<td colspan="3" valign="top" style="border-bottom:5px solid #bb9765;">
		  <table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr style="background-color: #4C6E7C;">
			  <td width="17" height="96"></td>
			  <td width="536" valign="middle"><a href="' . SITE_URL . '"><img src="' . SITE_URL . 'images/email/webmall-logo.gif" border="none" alt="WebMall Logo" /></a></td>
			  <td width="17"></td>
			</tr>
		  </table>
		</td>
	  </tr>
	  <tr>
		<td height="20" colspan="3" valign="top" bgcolor="#fff" style="border-bottom:5px solid #bb9765;">
		  <table width="100%" background="' . SITE_URL . 'images/shadow.png" style="background-repeat:no-repeat;" border="0" cellpadding="0" cellspacing="0">
			<tr>
			  <td style="padding:10px 0;" valign="top">
				<table width="98%" border="0" cellspacing="0" cellpadding="10" style="color:#333333; font-size:14px">
				  <tr>
					<td valign="top"> ' . $array['header'] . ' </td>
				  </tr>
				  <tr>
				    <td>
				        Order id: <b>#' . $array['oid'] . '</b><br/>
				        Transaction ref: <b>#' . $array['ref'] . '</b><br/>
				        Transaction Status: <b>#' . $array['status'] . '</b><br/>
				    </td>
				  </tr>
				  <tr>
					<td>For Inquiries Please call us on 0703.948.1991 or send an email to info@webmallng.com </td>
				  </tr>
				</table>
			  </td>
			</tr>
		  </table>
		</td>
	  </tr>
	  <tr style="background-color:#cccccc;">
		<td></td>
		<td valign="top" style="padding:0px 0">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" >

			<tr>
			  <td align="left"></td>
			  <td align="right">
			        <a href="http://www.facebook.com/webmallng"><img src="' . SITE_URL . 'images/email/fb.gif" alt="Facebook" border="none" /></a>
	                <a href="http://www.twitter.com/webmallng"><img src="' . SITE_URL . 'images/email/tw.gif" alt="Twitter" border="none" /></a>
	                <a href="http://www.instagram.com/webmallng"><img src="' . SITE_URL . 'images/email/ig.gif" alt="Instagram" border="none" /></a>
	                <a href="http://www.youtube.com/webmallng"><img src="' . SITE_URL . 'images/email/yt.gif" alt="YouTube" border="none" /></a>
	                <a href="https://plus.google.com/u/0/106291413256291053539/posts"><img src="' . SITE_URL . 'images/email/gp.gif" alt="GooglePlus" border="none" /></a>
			  </td>
			</tr>
		  </table>
		</td>
		<td></td>
	  </tr>
	</table>
	</body>
	</html>';

        try{
            mail($to,$subject,$message, $headers);
        } catch (Exception $e) {
            echo $e;
        }


    }

    public function customer_order_gtbank($array, $display)
    {
        $to = $array['email'];
        $subject = "Your WebmallNG Order";
        $headers = 'From:' . FROMEMAIL . "\r\n" .
            "Cc: kemi@netplusadvisory.com \r\n" .
            'Reply-To:' . FROMEMAIL . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Order Request</title>
</head>

<body style="margin:0; padding:0; background-color:#25363F;">

	<div style="margin: 0 auto; width:600px;background-color: #fff;">
	<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:12px;  background-color:#FFFFFF;">
	<tr>
            <td colspan="6">
                <div style="background-color: #496E7B;
                        width:100%; height:100px; border-bottom: solid 3px #ff9900; padding:20px 0">
                    <table>
                        <tr>
                            <td width="5%"></td>
                            <td width="60%">
                                <h1 style="color:#fff; font-size:36px; font-weight: normal">Congratulations!</h1>
                            </td>
                            <td width="30%"

                                 <img src="../../images/logo.png" />

                            </td>
                            <td width="5%"></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
	<tr>
            <td colspan="6" style="padding:10px 20px;">
                <p>
                    Dear <strong>' . $array["name"] . ' </strong>,<br />
                   Your order is now being processed and will be delivered within two to five days
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <table width="100%">
                    <tr>
                        <td width="20%" height="30">Order Id</td>
                        <td width="60%">
                            <div style=" height:30px; line-height:30px;
                            padding: 0 10px;">
                                                 ' .
            $array["order_id"]
            . '
                            </div>
                        </td>
                        <td width="20%"></td>
                    </tr>

                </table>
            </td>
        </tr>

        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <div style="border: solid 1px #ccc; background-color: #f3f3f3; ">
                    <p style="margin-left: 10px;margin-top: 20px"><b>Product(s) Information</b></p>
<table width="97%" style="margin: 0px auto;font-size: 12px">
                    <tr style=" background-color:#d3d3d3;color: white ">
                        <th width="12%" height="30"></th>
                        <th width="40%" height="30"></th>
                        <th width="12%" height="30">Item Price</th>
                        <th width="12%" height="30">Quantity</th>
                        <th width="12%" height="30">Size</th>
                        <th width="12%" height="30">Total</th>
                        </tr>
                        <tr>
            ' .
            $display
            . '

</tr>
                </table>
                </div>

            </td>
        </tr>
<tr>
					<td colspan="6" style="padding:10px 20px;">
        Credit the GTBANK account below with <b>&#x20A6; ' . number_format($array['grand_total'], 2) . '</b><br/>
        Name: <b>' . GTBANK_NAME . '</b><br/>
        Account: <b>' . GTBANK_ACCOUNT . '</b><br/>
					</td>
				  </tr>

</table>

<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:15px; color:#7f8c8d; background-color:#FFFFFF;">	<tr>
		<td width="600" style="text-align:center;">



<p style="border-top:1px solid #cccccc; padding-top:10px;background-color:#f3f3f3;font-size:12px;padding-bottom:10px;">
	<a href="http://www.facebook.com/webmallng"><img src="' . SITE_URL . 'images/email/fb.gif" alt="Facebook" border="none" /></a>
	<a href="http://www.twitter.com/webmallng"><img src="' . SITE_URL . 'images/email/tw.gif" alt="Twitter" border="none" /></a>
	<a href="http://www.instagram.com/webmallng"><img src="' . SITE_URL . 'images/email/ig.gif" alt="Instagram" border="none" /></a>
	<a href="http://www.youtube.com/webmallng"><img src="' . SITE_URL . 'images/email/yt.gif" alt="YouTube" border="none" /></a>
	<a href="https://plus.google.com/u/0/106291413256291053539/posts"><img src="' . SITE_URL . 'images/email/gp.gif" alt="GooglePlus" border="none" /></a>
<br>
<br>
	For more information, please call us on <strong>0818 778 2542</strong> or send an email to <strong><a href="mailto:info@webmallng.com" style="color:#000000; text-decoration:none;">info@webmallng.com</a></strong>
</p>
</td>

</table>
</div>
</body>
</html>
';

        try{
            mail($to,$subject,$message, $headers);
        } catch (Exception $e) {
            echo $e;
        }


        //print_r($message);
    }

    public function confirm_order_customer($array, $display)
    {
        $to = $array['email'];
        $subject = "Your WebmallNG Order";
        $headers = 'From:' . FROMEMAIL . "\r\n" .
            "Cc: kemi@netplusadvisory.com \r\n" .
            'Reply-To:' . FROMEMAIL . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Order Request</title>
</head>

<body style="margin:0; padding:0; background-color:#25363F;">

	<div style="margin: 0 auto; width:600px;background-color: #fff;">
	<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:12px;  background-color:#FFFFFF;">
	<tr>
            <td colspan="6">
                <div style="background-color: #496E7B;
                        width:100%; height:100px; border-bottom: solid 3px #ff9900; padding:20px 0">
                    <table>
                        <tr>
                            <td width="5%"></td>
                            <td width="60%">
                                <h1 style="color:#fff; font-size:36px; font-weight: normal">Congratulations!</h1>
                            </td>
                            <td width="30%">
                                 <img src="../../images/logo.png" />
                            </td>
                            <td width="5%"></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
	<tr>
            <td colspan="6" style="padding:10px 20px;">
                <p>
                    Dear <strong>' . $array["name"] . ' </strong>,<br />
                   Your order is now being processed and will be delivered within two to five days
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <table width="100%">
                    <tr>
                        <td width="20%" height="30">Order Id</td>
                        <td width="60%">
                            <div style=" height:30px; line-height:30px;
                            padding: 0 10px;">
                                                 ' .
            $array["order_id"]
            . '
                            </div>
                        </td>
                        <td width="20%"></td>
                    </tr>

                </table>
            </td>
        </tr>

        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <div style="border: solid 1px #ccc; background-color: #f3f3f3; ">
                    <p style="margin-left: 10px;margin-top: 20px"><b>Product(s) Information</b></p>
<table width="97%" style="margin: 0px auto;font-size: 12px">
                    <tr style=" background-color:#d3d3d3;color: white ">
                        <th width="12%" height="30"></th>
                        <th width="40%" height="30"></th>
                        <th width="12%" height="30">Item Price</th>
                        <th width="12%" height="30">Quantity</th>
                        <th width="12%" height="30">Size</th>
                        <th width="12%" height="30">Total</th>
                        </tr>
                        <tr>' .
            $display
            . '
</table>

<table width="600"  border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:15px; color:#7f8c8d; background-color:#FFFFFF;">	<tr>
		<td width="600" style="text-align:center;">



<p style="border-top:1px solid #cccccc; padding-top:10px;">
	<a href="http://www.facebook.com/webmallng"><img src="' . SITE_URL . 'images/email/fb.gif" alt="Facebook" border="none" /></a>
	<a href="http://www.twitter.com/webmallng"><img src="' . SITE_URL . 'images/email/tw.gif" alt="Twitter" border="none" /></a>
	<a href="http://www.instagram.com/webmallng"><img src="' . SITE_URL . 'images/email/ig.gif" alt="Instagram" border="none" /></a>
	<a href="http://www.youtube.com/webmallng"><img src="' . SITE_URL . 'images/email/yt.gif" alt="YouTube" border="none" /></a>
	<a href="https://plus.google.com/u/0/106291413256291053539/posts"><img src="' . SITE_URL . 'images/email/gp.gif" alt="GooglePlus" border="none" /></a>
</p>

<p style="color:#000000; font-size:12px;">
	For more information, please call us on <strong>0818 778 2542</strong> or send an email to <strong><a href="mailto:info@webmallng.com" style="color:#000000; text-decoration:none;">info@webmallng.com</a></strong>
</p><br/>
</td>
<tr bgcolor="#000000">
	<td colspan="6">
		<p style=" text-align:center;">
			<strong><a href="https://www.webmallng.com" style="color:#FF9900; text-decoration:none;">www.webmallng.com</a></strong>
		</p>
	</td>
</tr>
</table>
</body>
</html>
';

        //print_r($message);

        try{
            mail($to,$subject,$message, $headers);
        } catch (Exception $e) {
            echo $e;
        }


    }

    public function confirm_order_merchant($array, $display)
    {
        $to = $array['email'];
        $subject = "New Order on WebmallNG";
        $headers = 'From:' . FROMEMAIL . "\r\n" .
            "Cc: kemi@netplusadvisory.com \r\n" .
            'Reply-To:' . FROMEMAIL . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Order Request</title>
</head>

<body style="margin:0; padding:0; background-color:#25363F;">
<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:15px; color:#7f8c8d; background-color:#FFFFFF;">

	<tr>
		<td width="600" style="background-color:#496E7B; height:65px; text-align:center; color:#FFFFFF; border-bottom:3px solid #FF9900; padding:10px;">
			<strong><a href="' . SITE_URL . '"><img src="' . SITE_URL . 'images/email/webmall-logo.gif" border="none" alt="WebMall Logo" /></a>
		</td>

	</tr>
	</table>
	<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:15px; color:#7f8c8d; background-color:#FFFFFF;">


	<th align = "center" colspan="8" style="padding-bottom: 10px;padding-top: 10px">
	Dear Merchant Request Has Been Made For The Following Product(s)
	</th>
<tr style="background-color: #d3d3d3;border: 1px outset #808080;">
<td align="center"><b>  Product Name</b></td>
<td align="center"><b> Price</b></td>
<td colspan = "2" align="center"><b> Quantity</b></td>
<td colspan = "2" align="center"><b> Size</b></td>
<td colspan = "2" align="center"><b> Cumulative</b></td>

</tr>
<tr>
            ' .
            $display
            . '

</tr>
</table>
<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:15px; color:#7f8c8d; background-color:#FFFFFF;">	<tr>
		<td width="600" style="text-align:center;">



<p style="border-top:1px solid #cccccc; padding-top:10px;">
	<a href="http://www.facebook.com/webmallng"><img src="' . SITE_URL . 'images/email/fb.gif" alt="Facebook" border="none" /></a>
	<a href="http://www.twitter.com/webmallng"><img src="' . SITE_URL . 'images/email/tw.gif" alt="Twitter" border="none" /></a>
	<a href="http://www.instagram.com/webmallng"><img src="' . SITE_URL . 'images/email/ig.gif" alt="Instagram" border="none" /></a>
	<a href="http://www.youtube.com/webmallng"><img src="' . SITE_URL . 'images/email/yt.gif" alt="YouTube" border="none" /></a>
	<a href="https://plus.google.com/u/0/106291413256291053539/posts"><img src="' . SITE_URL . 'images/email/gp.gif" alt="GooglePlus" border="none" /></a>
</p>

<p style="color:#000000; font-size:12px;">
	For more information, please call us on <strong>0818 778 2542</strong> or send an email to <strong><a href="mailto:info@webmallng.com" style="color:#000000; text-decoration:none;">info@webmallng.com</a></strong>
</p><br/>
</td>
<tr bgcolor="#000000">
	<td colspan="6">
		<p style=" text-align:center;">
			<strong><a href="https://www.webmallng.com" style="color:#FF9900; text-decoration:none;">www.webmallng.com</a></strong>
		</p>
	</td>
</tr>
</table>
</body>
</html>
';

        try{
            mail($to,$subject,$message, $headers);
        } catch (Exception $e) {
            echo $e;
        }


        //print_r($message);
    }

    public function forgot_password($array)
    {
        $hash = $array['hash'];
        $to = $array['email'];
        $subject = "Password Reset on Webmallng";
        $headers = "From: info@webmallng.com \r\n" .
            "Reply-To: info@webmallng.com \r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Password Reset on Webmall</title>
</head>

<body style="margin:0; padding:0; background-color:#25363F;">

	<div style="margin: 0 auto; width:600px;background-color: #fff;">
	<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:12px;  background-color:#FFFFFF;">
	<tr>
            <td colspan="6">
                <div style="background-color: #496E7B;
                        width:100%; height:100px; border-bottom: solid 3px #ff9900; padding:20px 0">
                    <table>
                        <tr>
                            <td width="5%"></td>
                            <td width="60%">
                                <h1 style="color:#fff; font-size:36px; font-weight: normal">Password Reset!</h1>
                            </td>
                            <td width="30%">

                                 <img src="../../images/logo.png" />

                            </td>
                            <td width="5%"></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
	<tr>
            <td colspan="6" style="padding:10px 20px;">
                <p>
                    Dear <strong>Merchant </strong>,<br />
                   You requested for a password reset. Please, click on the link below to reset your password.
                   <br />
                       <a href="' . SITE_URL . 'reset_password.php?pr=' . $hash . '">
                       ' . SITE_URL . 'reset_password.php?pr=' . $hash . '</a>

                </p>
            </td>
        </tr>


</table>

<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:15px; color:#7f8c8d; background-color:#FFFFFF;">	<tr>
		<td width="600" style="text-align:center;">



<p style="border-top:1px solid #cccccc; padding-top:10px;background-color:#f3f3f3;font-size:12px;padding-bottom:10px;">
	<a href="http://www.facebook.com/webmallng"><img src="' . SITE_URL . 'images/email/fb.gif" alt="Facebook" border="none" /></a>
	<a href="http://www.twitter.com/webmallng"><img src="' . SITE_URL . 'images/email/tw.gif" alt="Twitter" border="none" /></a>
	<a href="http://www.instagram.com/webmallng"><img src="' . SITE_URL . 'images/email/ig.gif" alt="Instagram" border="none" /></a>
	<a href="http://www.youtube.com/webmallng"><img src="' . SITE_URL . 'images/email/yt.gif" alt="YouTube" border="none" /></a>
	<a href="https://plus.google.com/u/0/106291413256291053539/posts"><img src="' . SITE_URL . 'images/email/gp.gif" alt="GooglePlus" border="none" /></a>
<br>
<br>
	For more information, please call us on <strong>0818 778 2542</strong> or send an email to <strong><a href="mailto:info@webmallng.com" style="color:#000000; text-decoration:none;">info@webmallng.com</a></strong>
</p>
</td>

</table>
</div>
</body>
</html>';

        try{
            mail($to,$subject,$message, $headers);
        } catch (Exception $e) {
            echo $e;
        }


    }

    public function reset_password($array)
    {

        $to = $array['email'];
        $subject = "Password Reset on Webmallng";
        $headers = "From: info@webmallng.com \r\n" .
            "Reply-To: info@webmallng.com \r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Password Reset on Webmall</title>
</head>

<body style="margin:0; padding:0; background-color:#25363F;">

	<div style="margin: 0 auto; width:600px;background-color: #fff;">
	<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:12px;  background-color:#FFFFFF;">
	<tr>
            <td colspan="6">
                <div style="background-color: #496E7B;
                        width:100%; height:100px; border-bottom: solid 3px #ff9900; padding:20px 0">
                    <table>
                        <tr>
                            <td width="5%"></td>
                            <td width="60%">
                                <h1 style="color:#fff; font-size:36px; font-weight: normal">Password Reset!</h1>
                            </td>
                            <td width="30%">

                                 <img src="../../images/logo.png" />

                            </td>
                            <td width="5%"></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
	<tr>
            <td colspan="6" style="padding:10px 20px;">
                <p>
                    Dear <strong>Merchant </strong>,<br />
                   You have successfully reset your password.
                </p>
            </td>
        </tr>


</table>

<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:15px; color:#7f8c8d; background-color:#FFFFFF;">	<tr>
		<td width="600" style="text-align:center;">



<p style="border-top:1px solid #cccccc; padding-top:10px;background-color:#f3f3f3;font-size:12px;padding-bottom:10px;">
	<a href="http://www.facebook.com/webmallng"><img src="' . SITE_URL . 'images/email/fb.gif" alt="Facebook" border="none" /></a>
	<a href="http://www.twitter.com/webmallng"><img src="' . SITE_URL . 'images/email/tw.gif" alt="Twitter" border="none" /></a>
	<a href="http://www.instagram.com/webmallng"><img src="' . SITE_URL . 'images/email/ig.gif" alt="Instagram" border="none" /></a>
	<a href="http://www.youtube.com/webmallng"><img src="' . SITE_URL . 'images/email/yt.gif" alt="YouTube" border="none" /></a>
	<a href="https://plus.google.com/u/0/106291413256291053539/posts"><img src="' . SITE_URL . 'images/email/gp.gif" alt="GooglePlus" border="none" /></a>
<br>
<br>
	For more information, please call us on <strong>0818 778 2542</strong> or send an email to <strong><a href="mailto:info@webmallng.com" style="color:#000000; text-decoration:none;">info@webmallng.com</a></strong>
</p>
</td>

</table>
</div>
</body>
</html>';

//        $mail = new PHPMailer;
//
//        $mail->isSMTP();
//        $mail->Host = SMTPHOST;
//        $mail->SMTPAuth = true;
//        $mail->Username = SMTPUSER;
//        $mail->Password = SMTPPASS;
//        $mail->Port = 25;
//        $mail->SMTPDebug;
//
//        $mail->From = FROMEMAIL;
//        $mail->FromName = FROM;
//        $mail->addAddress($to);
//        $mail->isHTML(true);
//
//        $mail->Subject = $subject;
//        $mail->Body    = $message;
//
//        $mail->send();
        try{
            mail($to,$subject,$message, $headers);
        } catch (Exception $e) {
            echo $e;
        }

    }

    public function bill_payment_response($array)
    {
        $to = $array['email'];
        $subject = 'Your ' .$array['provider']. " Payment on WebmallNG";
        $headers = 'From:' . FROMEMAIL . "\r\n" .
            'Reply-To:' . FROMEMAIL . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title> Your ' .$array['provider']. ' Payment on WebmallNG</title>
</head>

<body style="margin:0;padding:0">
	<table align="center" width="600" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif;font-size:12px;border:1px solid #ccc;">
	<tr>
            <td colspan="6">
                <div style="background-color: #496E7B;
                        width:100%; height:100px; border-bottom: solid 3px #ff9900; padding:20px 0">
                    <table>
                        <tr>
                            <td width="5%"></td>
                            <td width="60%">
                                <h1 style="color:#fff; font-size:36px; font-weight: normal">Transaction Status</h1>
                            </td>
                            <td width="30%">
                                <img src="' . SITE_URL . 'images/logo.png" />
                            </td>
                            <td width="5%"></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>


	<tr >
		<td width="600" style="text-align:center;font-family: \'Calibri\', Arial, sans-serif">
			<h2 style="color:#496E7B;"></h2>

			<p style="margin:0px auto;width:525px; text-align:center; font-size:25px; line-height:normal; font-family:\'Calibri\', Arial; color:'. $array['color'].';">'. $array['message'] .'</p>
			<font>Transaction Id: '. $array['trans_id'].' </font>
</td>
</tr>
</table>


<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:15px; color:#7f8c8d; background-color:#FFFFFF;">	<tr>
		<td width="600" style="text-align:center;">



<p style="border-top:1px solid #cccccc; padding-top:10px;background-color:#f3f3f3;font-size:12px;padding-bottom:10px;">
	<a href="http://www.facebook.com/webmallng"><img src="' . SITE_URL . 'images/email/fb.gif" alt="Facebook" border="none" /></a>
	<a href="http://www.twitter.com/webmallng"><img src="' . SITE_URL . 'images/email/tw.gif" alt="Twitter" border="none" /></a>
	<a href="http://www.instagram.com/webmallng"><img src="' . SITE_URL . 'images/email/ig.gif" alt="Instagram" border="none" /></a>
	<a href="http://www.youtube.com/webmallng"><img src="' . SITE_URL . 'images/email/yt.gif" alt="YouTube" border="none" /></a>
	<a href="https://plus.google.com/u/0/106291413256291053539/posts"><img src="' . SITE_URL . 'images/email/gp.gif" alt="GooglePlus" border="none" /></a>
<br>
<br>
	For more information, please call us on <strong>0818 778 2542</strong> or send an email to <strong><a href="mailto:info@webmallng.com" style="color:#000000; text-decoration:none;">info@webmallng.com</a></strong>
</p>
</td>

</table>
</div>
</body>
</html>
';

        try{
            mail($to,$subject,$message, $headers);
        } catch (Exception $e) {
            echo $e;
        }

        print_r($message);

    }

    public function test()
    {
        $mail = new PHPMailer;

        $mail->isSMTP();
        $mail->Host = SMTPHOST;
        $mail->SMTPAuth = true;
        $mail->Username = SMTPUSER;
        $mail->Password = SMTPPASS;
        $mail->Port = 25;
        $mail->SMTPDebug;

        $mail->From = FROMEMAIL;
        $mail->FromName = FROM;
        $mail->addAddress('mayowa.anibaba@gmail.com', 'May');
        $mail->isHTML(true);

        $mail->Subject = 'Here is the subject';
        $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            echo 'Message has been sent';
        }

    }

    public function sales_invite($array)
    {
        $to = $array['email'];
        $subject = "Group Buy Invite";
        /*$from='igbokweudoka@gmail.com';
        $from_name=$array["friend"];*/
        $headers = 'From:' . FROMEMAIL . "\r\n" .
            'Reply-To:' . FROMEMAIL . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Invite</title>
</head>

<body style="margin:0; padding:0; background-color:#25363F;">

	<div style="margin: 0 auto; width:600px;background-color: #fff;">
	<table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:12px;  background-color:#FFFFFF;">
	<tr>
            <td colspan="6">
                <div style="background-color: #496E7B;
                        width:100%; height:60px; border-bottom: solid 3px #ff9900; padding:20px 0">
                    <table>
                        <tr align="center">
                            <td width="3%"></td>

                            <td width="30%">
                                <img src="../../images/logo.png" />

                            </td>
                            <td width="5%"></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
	<tr>
            <td colspan="6" style="padding:10px 20px;">
                <p>
                    <strong style="font-size:18px">Dear Customer  </strong>,<br /><br />
                   Your friend <b>'.$array["friend"].'</b> has invited you to purchase this product on WebMall</p>


                    <p>click on this <a href="' . SITE_URL . $array["link"].'" target="_blank"><b>link</b> </a> to  make a purchase</p>

                </p>
            </td>
        </tr>




</table>

<table width="600" align="center" border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse; font-family:Arial, sans-serif; font-size:15px; color:#7f8c8d; background-color:#FFFFFF;">	<tr>
		<td width="600" style="text-align:center;">



<p style="border-top:1px solid #cccccc; padding-top:10px;background-color:#f3f3f3;font-size:12px;padding-bottom:10px;">
	<a href="http://www.facebook.com/webmallng"><img src="' . SITE_URL . 'images/email/fb.gif" alt="Facebook" border="none" /></a>
	<a href="http://www.twitter.com/webmallng"><img src="' . SITE_URL . 'images/email/tw.gif" alt="Twitter" border="none" /></a>
	<a href="http://www.instagram.com/webmallng"><img src="' . SITE_URL . 'images/email/ig.gif" alt="Instagram" border="none" /></a>
	<a href="http://www.youtube.com/webmallng"><img src="' . SITE_URL . 'images/email/yt.gif" alt="YouTube" border="none" /></a>
	<a href="https://plus.google.com/u/0/106291413256291053539/posts"><img src="' . SITE_URL . 'images/email/gp.gif" alt="GooglePlus" border="none" /></a>
<br>
<br>
	For more information, please call us on <strong>0818 778 2542</strong> or send an email to <strong><a href="mailto:info@webmallng.com" style="color:#000000; text-decoration:none;">info@webmallng.com</a></strong>
</p>
</td>

</table>
</div>
</body>
</html>
';

        try{
            mail($to,$subject,$message, $headers);
        } catch (Exception $e) {
            echo $e;
        }

        print_r($message);
    }
}



?>