<?php
/**
 * Created by PhpStorm.
 * User: May
 * Date: 23/07/2015
 * Time: 09:52
 */
function CallAPI($method, $url, $data = false)
{
    $curl = curl_init();

    switch ($method)
    {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);
            $fields_string = '';
            if ($data){
                //echo $fields_string;
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }


            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    // Optional Authentication:
    //curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    //curl_setopt($curl, CURLOPT_USERPWD, "username:password");

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);

    if(curl_errno($curl)){
        return curl_errno($curl);
    } else {
        return $result;
    }

    curl_close($curl);


}