<?php
public function concom_2015_confirm_invite($array)
{
    $to = $array['email'];
    $style_a = "'work sans',Arial, Helvetica, sans-serif; text-align:center;";
    $style_b="font-family:'cocogoose',Arial, Helvetica, sans-serif; font-size:30px; color:#407684; text-align:center; ";
    $style_c="line-height:0.6cm; font-size:17px; font-family:'cocogoose',Arial, Helvetica, sans-serif; padding:20px;";
    $style_d="font-family:'work sans',Arial, Helvetica, sans-serif; border:1px solid #ccc; font-size:18px; color:#000; padding:10px; text-align:center;";
    $style_e="font-family:'work sans',Arial, Helvetica, sans-serif; font-size:14px; color:#000; padding:10px; text-align:center;";
    $style_f="font-family:'cocogoose',Arial, Helvetica, sans-serif; font-size:30px; color:#000; text-align:center;";
    $style_g="font-family:'work sans',Arial, Helvetica, sans-serif; font-size:14px; color:#fff; padding:10px; text-align:center;";
    $style_h="font-family:'work sans',Arial, Helvetica, sans-serif; border:1px solid #f0f0f0; font-size:14px; color:#000; padding:10px; text-align:center;";

    $subject = "Welcome to WebmallNG";
    $headers = 'From:' . FROMEMAIL . "\r\n" .
        'Reply-To:' . FROMEMAIL . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    $message = '<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
</head>

<body>
<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border:1px solid #ccc;">
  <!--DWLayoutTable-->
  <tr>
    <td height="500" colspan="12" valign="top"><img src="' . SITE_URL . 'concom2015/notification/images/banner02.jpg" width="600"  border="none" /></td>
  </tr>
  <tr>
    <td width="17" height="20">&nbsp;</td>
    <td width="26">&nbsp;</td>
    <td width="24">&nbsp;</td>
    <td width="14">&nbsp;</td>
    <td width="15">&nbsp;</td>
    <td width="21">&nbsp;</td>
    <td width="338">&nbsp;</td>
    <td width="41">&nbsp;</td>
    <td width="13">&nbsp;</td>
    <td width="56">&nbsp;</td>
    <td width="18">&nbsp;</td>
    <td width="17">&nbsp;</td>
  </tr>
  <tr>
    <td height="37">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td valign="middle" style="font-size:20px; font-family:'.$style_a.'">You are invited to </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="44" colspan="10" valign="top" style="'.$style_b.'">CONNECT COMMERCE 2015</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="32"></td>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="4" valign="top"><img src="' . SITE_URL . 'concom2015/notification/images/img01.jpg" width="413" height="32" border="none" /></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="28"></td>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>


  <tr>
    <td height="107">&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="10" valign="top" style="'.$style_c.'">
	<b style="color:#FF9D00;">VENUE:</b> LAGOS ORIENTAL HOTEL, VICTORIA ISLAND<br/>
        <b style="color:#FF9D00; margin-right:13px;">DATE:</b> SEPTEMBER 29, 2015<br/>
      <b style="color:#FF9D00; margin-right:17px;">TIME:</b> 8AM - 4PM	 </td>
  </tr>
  <tr>
    <td height="16"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="36"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="4" valign="top"><img src="' . SITE_URL . 'concom2015/notification/images/img01.jpg" width="413" height="32" border="none" /></td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
  </tr>









  <tr>
    <td height="30">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="53">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="7" valign="middle" style="'.$style_d.'"><b>'.$array['name'].'</b></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="17"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="55"></td>
    <td></td>
    <td></td>
    <td colspan="7" valign="middle" style="'.$style_d.'"><b>'.$array['email'].'</b></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="17"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="56"></td>
    <td></td>
    <td></td>
    <td colspan="7"  valign="middle" style="'.$style_d.'"><b>'.$array['reg_no'].'</b></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="10"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="41"></td>
    <td colspan="10" valign="top" style="'.$style_e.'">*You are to present this invitation at the venue for proper verification. </td>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td height="180" colspan="12" valign="top" bgcolor="#F3F4F8"><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <!--DWLayoutTable-->
      <tr>
        <td width="138" height="13"></td>
          <td width="76"></td>
          <td width="145"></td>
          <td width="57"></td>
          <td width="184"></td>
      </tr>
      <tr>

        <td colspan="5" valign="top" style="'.$style_f.'">RSVP:</td>
          <td>&nbsp;</td>
      </tr>
      <tr>
        <td height="13"></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>













      <tr>
        <td height="54" colspan="6" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#32515B">
          <!--DWLayoutTable-->
          <tr>
            <td width="289" height="54" valign="middle" style="'.$style_g.'">Miss Moni Oloke  (0809 8243 204)</td>
                <td width="311" valign="middle" style="'.$style_g.'">Miss Shola Ogunyemi  (0813 8305 303)</td>
            </tr>
          </table></td>
        </tr>
      <tr>
        <td height="13"></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
      </tr>
      <tr>
        <td height="31"></td>
        <td></td>
        <td valign="top"><img src="' . SITE_URL . 'concom2015/notification/images/img02.jpg" width="145" height="31" border="none" /></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td height="10" colspan="5" valign="middle" style="'.$style_h.'"><i>HELPING BUSINESSES SUCCEED ONLINE - ONE MERCHANT AT A TIME </i></td>
        </tr>
      <tr>
        <td height="10">&nbsp;</td>

      </tr>

    </table></td>
  </tr>
</table>
</body>
</html>

';
    $mail = new PHPMailer;

    $mail->isSMTP();
    $mail->Host = SMTPHOST;
    $mail->SMTPAuth = true;
    $mail->Username = SMTPUSER;
    $mail->Password = SMTPPASS;
    $mail->Port = 25;
    $mail->SMTPDebug;

    $mail->From = FROMEMAIL;
    $mail->FromName = FROM;
    $mail->addAddress($to);
    $mail->isHTML(true);

    $mail->Subject = $subject;
    $mail->Body    = $message;

    $mail->send();

    //print_r($message);
}

public function concom_2015_confirm_pending($array)
{
    $to = $array['email'];
    $style_a = "font-family:'cocogoose',Arial, Helvetica, sans-serif; font-size:30px; color:#407684; text-align:center; ";
    $style_b="line-height:0.6cm; font-size:16px; font-family:'work sans',Arial, Helvetica, sans-serif; padding:10px;";
    $style_c="background-color:#f0f0f0; border-top:1px solid #ccc; line-height:0.7cm; font-size:16px; font-family:'work sans',Arial, Helvetica, sans-serif; padding:20px;";
    $style_d="font-family:'work sans',Arial, Helvetica, sans-serif; color:#fff; font-size:20px; line-height:0.7cm; padding:10px; border-right:1px solid #fff;";
    $style_e="font-family:'work sans',Arial, Helvetica, sans-serif; color:#fff; font-size:20px; line-height:0.7cm; padding:10px;";
    $style_f="background-color:#000; text-align:center; font-family:'work sans',Arial, Helvetica, sans-serif; font-size:13px; color:#FF9D00;";

    $subject = "ConCom registration WebmallNG";
    $headers = 'From:' . FROMEMAIL . "\r\n" .
        'Reply-To:' . FROMEMAIL . "\r\n" .
        'X-Mailer: PHP/' . phpversion();
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
    $message = '<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
</head>

<body>
<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="border:1px solid #ccc;">
  <!--DWLayoutTable-->
  <tr>
    <td height="380" colspan="4" valign="top"><img src="' . SITE_URL . 'concom2015/notification/images/banner01.jpg" width="600" height="380" border="none" /></td>
  </tr>
  <tr>
    <td width="32" height="20">&nbsp;</td>
    <td width="12">&nbsp;</td>
    <td width="521">&nbsp;</td>
    <td width="35">&nbsp;</td>
  </tr>
  <tr>
    <td height="44">&nbsp;</td>
    <td colspan="2" valign="bottom" style="'.$style_a.'">REGISTRATION PENDING</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="1"></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="108"></td>
    <td></td>
    <td valign="top" style="'.$style_b.'">Thank you for your interest in attending Connect Commerce 2015. Your registration is pending and a complete registration is subject to full payment of the required amount into any of the bank accounts listed below: </td>
  <td></td>
  </tr>
  <tr>
    <td height="17"></td>
    <td></td>
    <td>&nbsp;</td>
    <td></td>
  </tr>


  <tr>
    <td height="223" colspan="4" valign="top" style="'.$style_c.'">
    <b>Account name: Netplus Strategy And Sales Advisory</b><br/>
    <b>*GT Bank: 0113727879</b><br/>
<b>*FCMB: 2684362013</b><br/>
<b>*UBA: 1017562341</b><br/>
<b>*Access Bank: 0690621646</b><br/>
<b>*Fidelity Bank: 4010962858</b> <br/><br/>  <b style="float:right; color:#FF9D00;">Connect Commerce 2015 Team</b></td>
  </tr>
  <tr>
    <td height="217" colspan="4" valign="top" bgcolor="#25363F"><table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-top:3px solid #FF9D00; ">
      <!--DWLayoutTable-->
      <tr>
        <td width="17" height="33">&nbsp;</td>
          <td width="302">&nbsp;</td>
          <td width="13">&nbsp;</td>
          <td width="9">&nbsp;</td>
          <td width="31">&nbsp;</td>
          <td width="13">&nbsp;</td>
          <td width="31">&nbsp;</td>
          <td width="12">&nbsp;</td>
          <td width="31">&nbsp;</td>
          <td width="9">&nbsp;</td>
          <td width="31">&nbsp;</td>
          <td width="68">&nbsp;</td>
        </tr>
      <tr>
        <td height="46">&nbsp;</td>
          <td rowspan="3" valign="top" style="'.$style_d.'">Questions?<br/>
            We\'ll be happy to help:
            info@webmallng.com<br/>
          0809 999 0660 </td>
          <td>&nbsp;</td>
          <td colspan="9" valign="top" style="'.$style_e.'">Connect with us: </td>
        </tr>
      <tr>
        <td height="31">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td valign="top"><a href="https://www.facebook.com/webmallng"><img src="' . SITE_URL . 'concom2015/notification/images/facebook_icon.png" width="31" height="30" border="none" /></a></td>
          <td>&nbsp;</td>
          <td valign="top"><a href="https://www.twitter.com/webmallng"><img src="' . SITE_URL . 'concom2015/notification/images/twitter_icon.png" width="31" height="30" border="none" /></a></td>
          <td>&nbsp;</td>
          <td valign="top"><a href="https://plus.google.com/u/0/106291413256291053539/posts"><img src="' . SITE_URL . 'concom2015/notification/images/google_icon.png" width="31" height="30" border="none" /></a></td>
          <td>&nbsp;</td>
          <td valign="top"><a href="https://www.youtube.com/webmallng"><img src="' . SITE_URL . 'concom2015/notification/images/youtube_icon.png" width="31" height="30" border="none" /></a></td>
          <td>&nbsp;</td>
        </tr>
      <tr>
        <td height="63">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>

      <tr>
        <td height="41">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>





    </table></td>
  </tr>
  <tr>
    <td height="50" colspan="4" valign="middle" style="'.$style_f.'"><a href="https:www.webmallng.com/connectcommerce" style="text-decoration:none; color:#ff9d00;">www.webmallng.com/connectcommerce</a></td>
  </tr>
</table>
</body>
</html>


    ';
    $mail = new PHPMailer;

    $mail->isSMTP();
    $mail->Host = SMTPHOST;
    $mail->SMTPAuth = true;
    $mail->Username = SMTPUSER;
    $mail->Password = SMTPPASS;
    $mail->Port = 25;
    $mail->SMTPDebug;

    $mail->From = FROMEMAIL;
    $mail->FromName = FROM;
    $mail->addAddress($to);
    $mail->isHTML(true);

    $mail->Subject = $subject;
    $mail->Body    = $message;

    $mail->send();

    //print_r($message);
}
?>