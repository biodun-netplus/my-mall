<?php
include 'env.php';
include_once '../classes/db_connect.php';
include '../model/create.php';
include '../model/read.php';
include '../model/email.php';

$create = new create();
$read = new read();
$mails = new mails();

if(isset($_REQUEST['businessname'])) {

    $displayname = str_replace(' ', '', preg_replace('/\s+/', '', $_REQUEST['businessname']));
    $displayname = str_replace('.', '', preg_replace('/\s+/', '', $_REQUEST['businessname']));

    $options = array('email' => $_REQUEST['email']);
    if ($read->read_single('users',$options)!='-1'){
      //  echo 1;
        header("Location:".SITE_URL."registration");
    } else {
        //echo 2;
        $array = array(
            'first_name' => $_REQUEST['businessname'],
            'display_name' => $displayname,
            'email' => $_REQUEST['email'],
            'category' => $_REQUEST['category'],
            'phone' => $_REQUEST['phone'],
            'address' => $_REQUEST['address'],
            'country' => 'NG',
            'state' => $_REQUEST['state'],
            'created' => date('Y-m-d h:i:s'),
            'deliveryarea' => $_REQUEST['deliveryarea'],
            'password_hash' => sha1($_REQUEST['password']),
            'user_type' => 'p',
            'isactive' => 'p',
            'package' => $_REQUEST['package'],
            'contact_time' => $_REQUEST['contact_time']);
       // var_dump($array);/*die;*/
        $create->add('users',$array);
        $mails->registration($array,$_REQUEST['password']);
        $mails->firstRegistration($array);
        //header("Location:".SITE_URL."includes/classes/do_login.php?password=".$_REQUEST['password']."&email=".$_REQUEST['email']."&registration=yes");
    }

} else {
    //echo 3;
   // header("Location:".SITE_URL."registration?r=2");
}

?>