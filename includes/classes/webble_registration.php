<?php
include 'env.php';
include_once '../classes/db_connect.php';
include_once '../model/create.php';

$create = new create();

$businessname = mysql_real_escape_string($_REQUEST['businessname']);
$email = mysql_real_escape_string($_REQUEST['e-mail']);
$phone = mysql_real_escape_string($_REQUEST['phone']);
$address = mysql_real_escape_string($_REQUEST['address']);
$website_type = mysql_real_escape_string($_REQUEST['group1']);
$others = mysql_real_escape_string($_REQUEST['others']);
$contact_time = mysql_real_escape_string($_REQUEST['contact_you']);

if(isset($_REQUEST['card'])){$card_payment = 'yes';}else{ $card_payment ='no';}
if(isset($_REQUEST['ib'])){$internet_banking = 'yes';}else{ $internet_banking ='no';}
if(isset($_REQUEST['adwords'])){$adwords = 'yes';}else{ $adwords ='no';}

$data = array('businessname'=>$businessname,
    'email'=>$email,
    'phone'=>$phone,
    'address'=>$address,
    'website_type'=>$website_type,
    'others'=>$others,
    'contact_time'=>$contact_time,
    'card_payment'=>$card_payment,
    'internet_banking'=>$internet_banking,
    'adwords'=>$adwords,
    'created_at'=>Date('Y-m-d h:i:s'),
);
$create->add('webble_requests',$data);
header('location:../../webble/success.html');
