<?php
/**
 * Created by PhpStorm.
 * User: Mayowa
 * Date: 26/04/2015
 * Time: 10:27
 */

include 'env.php';
include_once '../classes/db_connect.php';


$data = $_COOKIE['webmall_cart'];
$data = stripslashes($data);
$savedCartArray = json_decode($data, true);
$id = htmlspecialchars($_REQUEST['id'] , ENT_QUOTES);

$j = 0;
$jj = 0;
$newArray = array();
while($j<count($savedCartArray)){

    $arrayCart = explode('|',$savedCartArray[$j]);
    $product_id = $arrayCart[0];
    $quantity = $arrayCart[1];
    $size = $arrayCart[2];

    if($product_id != $_REQUEST['id'] || $size != $_REQUEST['size']){
        $newArray[$jj] = $product_id.'|'.$quantity.'|'.$size ;
        $jj++;
    }
    $j++;
}

$json = json_encode($newArray);

if(count($newArray)==0){
    header("location:delete_cart.php");
}else{
    setcookie("webmall_cart", $json ,time() + 60 * 60 * 24 * 30 , '/');
    header("location:".SITE_URL.'user_cart');
}
