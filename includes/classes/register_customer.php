<?php
include 'env.php';
include_once '../classes/db_connect.php';
include '../model/create.php';
include '../model/read.php';
include '../model/email.php';

$create = new create();
$read = new read();
$mails = new mails();

$options = array('email'=>$_REQUEST['email']);
$this_customer = $read->read_single('customer_details',$options);
if($this_customer == '-1') {
    $customer_id = date('Ymdhis') . rand(10, 99);
    $details = array('email' => $_REQUEST['email'], 'phone' => $_REQUEST['phone'], 'customer_id' => $customer_id, 'created_at' => date('Y-m-d h:i:s'));
    $create->add('customer_details', $details);

    $details = array('name' => $_REQUEST['name'],'address' => $_REQUEST['address'], 'delivery_area' => $_REQUEST['area'], 'contact_phone' => $_REQUEST['address_phone'], 'customer_id' => $customer_id, 'created_at' => date('Y-m-d h:i:s'));
    $create->add('customer_addresses', $details);
} else {
    $customer_id = $this_customer['customer_id'];
    $details = array('name' => $_REQUEST['name'],'address' => $_REQUEST['address'], 'delivery_area' => $_REQUEST['area'], 'contact_phone' => $_REQUEST['address_phone'], 'customer_id' => $customer_id, 'created_at' => date('Y-m-d h:i:s'));
    $create->add('customer_addresses', $details);
}

setcookie('webmall_customer', $customer_id ,time() + 60 * 60 * 24 * 30 , '/');
header("location:".SITE_URL."user_cart");

?>