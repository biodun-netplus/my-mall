<?php
/**
 * Created by PhpStorm.
 * User: Mayowa
 * Date: 26/04/2015
 * Time: 10:27
 */

include 'env.php';
include_once '../classes/db_connect.php';
include_once '../model/read.php';
$read = new read();
$data = $_COOKIE['webmall_cart'];
$data = stripslashes($data);
$savedCartArray = json_decode($data, true);
$id = htmlspecialchars($_REQUEST['id'], ENT_QUOTES);
$count = htmlspecialchars($_REQUEST['count'], ENT_QUOTES);
$cat_id = htmlspecialchars($_REQUEST['cat_id'], ENT_QUOTES);

$j = 0;
$newArray = array();

while ($j < count($savedCartArray)) {

    $arrayCart = explode('|', $savedCartArray[$j]);
    $product_id = $arrayCart[0];
    $quantity = $arrayCart[1];
    $size = $arrayCart[2];
    if($j == $cat_id )
    {
        $options = array('product_id'=>$id,'id'=>$size);
        $check_quantity = $read->read_single('product_size_quantity',$options);
        $check = (int)$check_quantity['quantity'];

        if($check_quantity['quantity'] < $count )
        {
            $_SESSION['greater_than'] = $size;
        }
        if($count <= 0)
        {
            $_SESSION['cantbeempty'] = -1;
        }

        else
        {
            if ($product_id == $id) {
                $quantity = $count;
            }
        }
    }



    $newArray[$j] = $product_id . '|' . $quantity . '|' . $size;
    $j++;
}

$json = json_encode($newArray);
setcookie("webmall_cart", $json, time() + 60 * 60 * 24 * 30, '/');
header("location:" . SITE_URL . 'user_cart');
