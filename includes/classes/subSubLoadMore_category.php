<?php
require_once('../classes/functions.php');
require_once('../classes/env.php');
include '../model/create.php';
include '../model/read.php';
include '../model/email.php';
include '../model/update.php';
$read = new read();
$update = new update();
$sub_sub_id = htmlspecialchars($_SESSION['subsubcategory_id'],ENT_QUOTES);
$category_id = htmlspecialchars($_SESSION['category_id'],ENT_QUOTES);

$sub_sub_display = 20;
$options = array();
$q = $read->read_list_join('p.id', 'products p', $options,true,""," m.isactive='t' and p.isactive='t' and m.id=p.pid and p.product_name!= '' AND p.product_price !='' AND m.first_name !='' AND m.display_name!='' AND p.sub_sub_cat_id='$sub_sub_id'");

$sub_sub_records = count($q);

$sub_sub_total_pages = ceil($sub_sub_records/$sub_sub_display);


$_SESSION['sub_sub_category'] = $_SESSION['sub_sub_category'] + 1;
$sub_sub_start = $_SESSION['sub_sub_category']*$sub_display;
$current_page = $_SESSION['sub_sub_category']+1;

$options = array();

$products = $read->read_list_join('p.id', 'products p', $options,true,"".$sub_sub_start.",".$sub_sub_display.""," m.isactive='t' and p.isactive='t' and m.id=p.pid and p.product_name!= '' AND p.product_price !='' AND m.first_name !='' AND m.display_name!='' AND p.sub_sub_cat_id='$sub_sub_id' ORDER BY Add_Date DESC");


if ($products[1] > 0)
{
    $i = 0;
    while (count($products) >= $i) {
        if($products[$i] != null) {

            $option = array('id' => $products[$i]);
            $related_product_fetch = $read->read_single('products', $option);

            $option = array('id' => $related_product_fetch['pid']);
            $seller = $read->read_single('users', $option);
            $merchant_id = $seller['id'];
            $logo = "SELECT * FROM provider_banner where provider_id = '$merchant_id'";
            $logo_query = mysql_query($logo) or die(mysql_error());
            $logo_fetch = mysql_fetch_object($logo_query);

            $list_price = $related_product_fetch['list_price'];
            /*if($related_product_fetch['product_price']>=20000)
            {
                $product_price = $related_product_fetch['product_price']+500;
            }
            else
            {*/
                $product_price = $related_product_fetch['product_price'];
//            }


            $discount = round((($list_price - $product_price) / $list_price) * 100);
            ?>
            <div class="product">
                <?php
               /* if($product_price>=20000)
                {
                    */?><!--<div class="free_delivery"></div>--><?php
/*                }*/
                if($discount > 0)
                {
                    ?>
                    <div class="discount orange_bg"><?php echo $discount;?>% Off</div>
                    <?php
                }
                ?>

                <a target="_blank" title="<?php echo $related_product_fetch['product_name'];?>"
                   href="<?php echo SITE_URL . $seller['display_name'];?>/products/<?php echo $related_product_fetch['id']?>">
                    <div class="prdt_img">
                        <img class="lazy" src="<?php echo SITE_URL;?>product_images/<?php echo $related_product_fetch['image'];?>">

                    </div>
                    <div class="prdt_name"><?php echo substr($related_product_fetch['product_name'], 0, 20);
                        if (strlen($related_product_fetch['product_name']) > 20) {
                            echo '...';
                        }
                        ?></div>
                </a>

                <div class="prdt_price">
                    <div class="price"><?php echo '₦' . number_format($product_price, 2);?></div>
                    <div class="more_btn"><a target="_blank"
                                             href="<?php echo SITE_URL . $seller['display_name'];?>/products/<?php echo $related_product_fetch['id']?>">Buy
                            Now</a></div>
                </div>
                <a href="<?php echo SITE_URL . $seller['display_name'];?>">
                    <div class="prdt_merchant">

                        <div class="mer_icon"><img class="lazy" src="<?php if (empty($logo_fetch->logo)) {
                                echo SITE_URL . 'images/logo_full_color.png';
                            } else {
                                echo SITE_URL . "banner_images/" . $logo_fetch->logo;
                            } ?>"/></div>
                        <div class="mer_name">
                            <?php echo substr( $seller['first_name'], 0, 20);
                            if (strlen( $seller['first_name']) > 20) {
                                echo '...';
                            }
                            ?></div>
                    </div>
                </a>
            </div>
            <?php
        }
        $i++;
    }
}
?>
</div>
<div class="product_content2" id="loadmore<?php echo $_SESSION['sub_sub_category']?>" style="width: 100%">
    <?php
    // Determine what page the script is on:

    // If it's not the first page, make a Previous button:
   /* echo 'current'.$current_page.'<br>';
    echo 'sub total'.$sub_sub_total_pages.'<br>';
    echo 'session'. $_SESSION['sub_sub_category'].'<br>';*/
    if ( $current_page!= $sub_sub_total_pages) {
    if($sub_sub_total_pages > $current_page) {
        echo '<div class="load-more-div"><a href="javascript:subSubLoadMore(';
        echo "'loadmore" . $_SESSION['sub_sub_category'] . "'";
        echo ',' . ($sub_sub_start) . ',' . $sub_sub_id . ')"><span>Load More</span></a></div>';
    }
    }
    ?>
</div>
<br class="clearfix" />
