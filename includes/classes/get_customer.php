<?php
include 'env.php';
include_once '../classes/db_connect.php';
include '../model/create.php';
include '../model/read.php';
include '../model/email.php';

$create = new create();
$read = new read();
$mails = new mails();

$options = array('email'=>$_REQUEST['email']);
$this_customer = $read->read_single('customer_details',$options);
if($this_customer == '-1') {
    $_SESSION['e']=$_REQUEST['email'];
    header("location:".SITE_URL."user_cart");
} else {
    $option = array('customer_id' => $this_customer['customer_id']);
    $customer = $read->read_single('customer_details', $option);
    $addresses = $read->read_list('id', 'customer_addresses', $option);
    if($addresses[1] == '')
    {
        $_SESSION['f']=$_REQUEST['email'];
        setcookie("webmall_customer", "", time() - 3600, '/');
        header("location:".SITE_URL."user_cart");
    }
    else
    {
        $customer_id = $this_customer['customer_id'];
        setcookie('webmall_customer', $customer_id ,time() + 60 * 60 * 24 * 30 , '/');
        header("location:".SITE_URL."user_cart");
    }


}

