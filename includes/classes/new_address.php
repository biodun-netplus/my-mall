<?php
include 'env.php';
include_once '../classes/db_connect.php';
include '../model/create.php';

$create = new create();

$options = array('customer_id'=>$_REQUEST['customer_id']);
$details = array('name' => $_REQUEST['name'],'address' => $_REQUEST['address'], 'delivery_area' => $_REQUEST['area'], 'contact_phone' => $_REQUEST['address_phone'], 'customer_id' => $_REQUEST['customer_id'], 'created_at' => date('Y-m-d h:i:s'));
$create->add('customer_addresses', $details);

header("location:".SITE_URL."user_cart");

?>