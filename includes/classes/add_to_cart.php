<?php
include 'env.php';
include_once '../classes/db_connect.php';
include_once '../model/read.php';
//var_dump($_SESSION['group_buy']);
$read = new read();
$newPush = htmlspecialchars($_REQUEST['product_id'] , ENT_QUOTES).'|'.htmlspecialchars($_REQUEST['qty'] , ENT_QUOTES).'|'.htmlspecialchars($_REQUEST['size'] , ENT_QUOTES);
$newArray = array();
$prod_size = htmlspecialchars($_REQUEST['size'] , ENT_QUOTES);
$prod_quantity = htmlspecialchars($_REQUEST['qty'] , ENT_QUOTES);


    if(isset($_COOKIE['webmall_cart'])){
        $data = $_COOKIE['webmall_cart'];
        $data = stripslashes($data);
        $savedCartArray = json_decode($data, true);

        $j = 0;
        $newProduct = 'true';
        while($j<count($savedCartArray)){

            $arrayCart = explode('|',$savedCartArray[$j]);
            $product_id = $arrayCart[0];
            $quantity = $arrayCart[1];
            $size = $arrayCart[2];
            $option = array('id'=>$prod_size);
            $get_quantity = $read->read_list('quantity','product_size_quantity',$option);

            if($get_quantity[1] >= $quantity) {

                if ($product_id == $_REQUEST['product_id'] && $size == $_REQUEST['size']) {
                    $quantity++;
                    $newArray[$j] = $product_id . '|' . $quantity . '|' . $size;

                    $newProduct = 'false';
                } else {
                    $newArray[$j] = $savedCartArray[$j];
                }
            }
            else
            {
                $newProduct = 'false';
            }
            $j++;
        }

        if($newProduct == 'true'){
            $newArray[count($savedCartArray)] = $newPush;
        }
        $json = json_encode($newArray);
        setcookie("webmall_cart", $json ,time() + 60 * 60 * 24 * 30 , '/');

    } else {
        $newArray[0] = $newPush;
        $json = json_encode($newArray);
        setcookie('webmall_cart', $json ,time() + 60 * 60 * 24 * 30 , '/');
    }





$i = 0;
$total = 0;
while($i < count($newArray)){
    $arrayCart = explode('|',$newArray[$i]);
    $product_id = $arrayCart[0];
    $quantity = $arrayCart[1];
    $options = array('id'=>$product_id );
    $orderedProduct = $read->read_single('products',$options);
   /* if($orderedProduct['product_price']>=20000)
    {

        $product_price = $orderedProduct['product_price']+500;
    }
    else*/if(isset($_SESSION['group_buy']))
    {

        $product_price = $orderedProduct['product_price']-((10/100)*$orderedProduct['product_price']);

    }
    else
    {

        $product_price = $orderedProduct['product_price'];

    }
    $total += ($product_price * $quantity);
    $i++;
}

if($newArray[0]<0){
    $count = 0;
} else {
    $count = count($newArray);
}

echo '₦ '. number_format($total,2).' ('.$count.')';
?>