<?php
if ($_SERVER['HTTP_HOST'] == 'localhost') {
    define('IMG_PATH', 'mymall/');
    define('SITE_URL', 'http://localhost/mymall/');
    define('IMG_PATH_MERC', 'banner_images/');
    define('PRDT_IMG', 'http://localhost/mymall/a/');
    define('IMG_URL', 'http://localhost/mymall');
    define('SITE_URL_MERC', 'http://localhost/mymall/merchant/');
    define('DEAL_IMG', 'deal_images/');
//    define('delivery_app_base_url','http://saddleng.com/v1/');
//    define('client_id','prx6ezk');
    define('courier_id','ksixga9');
} elseif ($_SERVER['HTTP_HOST'] == 'www.webmallnglab.com') {
    define('IMG_PATH', 'webmallng');
    define('SITE_URL', 'http://www.webmallnglab.com/webmall/');
    define('IMG_PATH_MERC', '../banner_images/');
    define('PRDT_IMG', '../product_images/');
    define('DEAL_IMG', '../deal_images/');
    define('IMG_URL', 'https://www.webmallnglab.com/webmall/');
    define('SITE_URL_MERC', 'http://www.webmallnglab.com/webmall/merchant/');
    define('delivery_app_base_url','http://saddleng.com/v1/');
    define('client_id','prx6ezk');
    define('courier_id','ksixga9');
} else {
    define('IMG_PATH', '../banner_images/');
    define('PRDT_IMG', '../product_images/');
    define('DEAL_IMG', '../deal_images/');
    define('IMG_URL', 'https://www.mymall.com.ng/');

    define('SITE_URL', 'https://www.mymall.com.ng/');
    define('SITE_URL_MERC', 'https://www.mymall.com.ng/merchant/');
    define('IMG_PATH_MERC', 'webmall');
    define('delivery_app_base_url','http://saddleng.com/v1/');
    define('client_id','prx6ezk');
    define('courier_id','ksixga9');
}

define('FROM','WebmallNG');
define('FROMEMAIL','info@webmallng.com');

define('GTBANK_NAME','NETPLUS STRATEGY AND SALES ADVISORY LTD');
define('GTBANK_ACCOUNT','0169422762');

?>