<?php
require_once('../classes/functions.php');
require_once('../classes/env.php');
include '../model/create.php';
include '../model/read.php';
include '../model/email.php';
include '../model/update.php';
$read = new read();
$update = new update();
$merchant_id = htmlspecialchars($_REQUEST['merchant_id'],ENT_QUOTES);
$category_id = htmlspecialchars($_REQUEST['cat_id'],ENT_QUOTES);

if($category_id==-1)
{
    $options_products = array('pid'=>$merchant_id);
    $products = $read->read_list('id','products',$options_products);
    $i = 1;
    while (count($products) >= $i) {
        if($products[$i] != null) {
            $product_id = (int)$products[$i];
            $option = array('id' => $product_id);
            $thisProduct = $read->read_single('products', $option);

            $real_merchant_id = $thisProduct['real_merchant_id'];

            $options = array('id'=>$real_merchant_id,'isactive'=>'t');
            $seller = $read->read_single('users',$options);

            $logo = "SELECT * FROM provider_banner where provider_id = '$real_merchant_id'";
            $logo_query = mysql_query($logo) or die(mysql_error());
            $logo_fetch = mysql_fetch_object($logo_query);

            $list_price = $thisProduct['list_price'];
            /* if($thisProduct['product_price']>=20000)
             {
                 $product_price = $thisProduct['product_price']+500;
             }
             else
             {*/
            $product_price = $thisProduct['product_price'];
//                }
            $discount = round((($list_price - $product_price) / $list_price) * 100);
            ?>
            <div class="merchant_product">
                <?php
                /*if($product_price>=20000)
                {
                    */?><!--<div class="free_delivery"></div>--><?php
                /*                    }*/
                if ($discount > 0) {
                    ?>
                    <div class="discount orange_bg"><?php echo $discount;?>% Off</div>
                    <?php
                }
                ?>
                <a target="_blank" title="<?php echo $thisProduct['product_name'];?>"
                   href="<?php echo SITE_URL . $seller['display_name']; ?>/products/<?php echo $thisProduct['id']; ?>">
                    <div class="prdt_img">
                        <img class="lazy"
                             src="<?php echo SITE_URL; ?>product_images/<?php echo $thisProduct['image']; ?>"/>
                    </div>
                    <div class="prdt_name"><?php echo substr($thisProduct['product_name'], 0, 20);
                        if (strlen($thisProduct['product_name']) > 20) {
                            echo '...';
                        } ?></div>
                </a>

                <div class="prdt_price">
                    <div class="price"><?php echo '₦' . number_format($product_price, 2); ?></div>
                    <div class="more_btn"><a target="_blank"
                                             href="<?php echo SITE_URL . $seller['display_name']; ?>/products/<?php echo $thisProduct['id']; ?>">Buy
                            Now</a></div>
                </div>
                <div class="prdt_merchant">
                    <a href="<?php echo SITE_URL . $seller['display_name'] ?>">
                        <div class="mer_icon"><img class="lazy" src="<?php if (empty($logo_fetch->logo)) {
                                echo SITE_URL . 'images/logo_full_color.png';
                            } else {
                                echo SITE_URL . "banner_images/" . $logo_fetch->logo;
                            } ?>"/></div>
                        <div class="mer_name">
                            <?php echo substr( $seller['first_name'], 0, 20);
                            if (strlen( $seller['first_name']) > 20) {
                                echo '...';
                            }
                            ?>

                            </div>
                    </a>
                </div>
            </div>
            <?php
        }
        $i++;
    }
}
else
{
$options_products = array('category'=>$category_id,'pid'=>$merchant_id);
$products = $read->read_list('id','products',$options_products);
$i = 1;
while (count($products) >= $i) {
    if($products[$i] != null) {
        $product_id = (int)$products[$i];
        $option = array('id' => $product_id);
        $thisProduct = $read->read_single('products', $option);

        $real_merchant_id = $thisProduct['real_merchant_id'];

        $options = array('id'=>$real_merchant_id,'isactive'=>'t');
        $seller = $read->read_single('users',$options);

        $logo = "SELECT * FROM provider_banner where provider_id = '$real_merchant_id'";
        $logo_query = mysql_query($logo) or die(mysql_error());
        $logo_fetch = mysql_fetch_object($logo_query);

        $list_price = $thisProduct['list_price'];
        /* if($thisProduct['product_price']>=20000)
         {
             $product_price = $thisProduct['product_price']+500;
         }
         else
         {*/
        $product_price = $thisProduct['product_price'];
//                }
        $discount = round((($list_price - $product_price) / $list_price) * 100);
        ?>
        <div class="merchant_product">
            <?php
            /*if($product_price>=20000)
            {
                */?><!--<div class="free_delivery"></div>--><?php
            /*                    }*/
            if ($discount > 0) {
                ?>
                <div class="discount orange_bg"><?php echo $discount;?>% Off</div>
                <?php
            }
            ?>
            <a target="_blank" title="<?php echo $thisProduct['product_name'];?>"
               href="<?php echo SITE_URL . $seller['display_name']; ?>/products/<?php echo $thisProduct['id']; ?>">
                <div class="prdt_img">
                    <img class="lazy"
                         src="<?php echo SITE_URL; ?>product_images/<?php echo $thisProduct['image']; ?>"/>
                </div>
                <div class="prdt_name"><?php echo substr($thisProduct['product_name'], 0, 20);
                    if (strlen($thisProduct['product_name']) > 20) {
                        echo '...';
                    } ?></div>
            </a>

            <div class="prdt_price">
                <div class="price"><?php echo '₦' . number_format($product_price, 2); ?></div>
                <div class="more_btn"><a target="_blank"
                                         href="<?php echo SITE_URL . $seller['display_name']; ?>/products/<?php echo $thisProduct['id']; ?>">Buy
                        Now</a></div>
            </div>
            <div class="prdt_merchant">
                <a href="<?php echo SITE_URL . $seller['display_name'] ?>">
                    <div class="mer_icon"><img class="lazy" src="<?php if (empty($logo_fetch->logo)) {
                            echo SITE_URL . 'images/logo_full_color.png';
                        } else {
                            echo SITE_URL . "banner_images/" . $logo_fetch->logo;
                        } ?>"/></div>
                    <div class="mer_name"><?php echo $seller['first_name']; ?></div>
                </a>
            </div>
        </div>
        <?php
    }
    $i++;
}
}

            ?>
<br class="clearfix" />

