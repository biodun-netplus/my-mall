<?php
require_once('../classes/functions.php');
require_once('../classes/env.php');
include '../model/create.php';
include '../model/read.php';
include '../model/email.php';
include '../model/update.php';
$read = new read();
$update = new update();
$display = 20;
$product_size_quantity_id = $_REQUEST['quantity_select'];

?>

<?php

$options = array('id' => $product_size_quantity_id);
//SELECT p.* from products p join users m on m.isactive='t' and m.id=p.pid AND p.category='$category_id' ORDER BY rand() LIMIT 8
$products = $read->read_single('product_size_quantity',$options);
$product_id = $products['product_id'];
if($products['quantity']>0)
{
    $product_id = $products['product_id'];
    $prod = mysql_fetch_object(mysql_query("SELECT * FROM products WHERE id=$product_id"))
?>
    <div class="detail_opt_itm" style="margin-top: 15px">
        <label class="label_float">QUANTITY</label>
        <select name="quantity" class="selection" id="qty">
            <?php
            for($i=1;$i<= $products['quantity'];$i++)
            {
                ?>
                <option><?php echo $i;?></option>
                <?php
            }
            ?>
        </select>
    </div>
    <div class="detail_opt_itm">
        <div class="detail_btn" onclick="javascript:addToCart(<?php echo $product_size_quantity_id;?>,<?php echo $product_id ?>,document.getElementById('qty').value)">Add to Cart</div>
        <?php
        $qty_needed = ceil(100000 / $product_price);
        $_SESSION['qty_needed'] = $qty_needed;
        /* if ($check_p_row > 0) {*/
        if ($prod->product_price <= 20000 && $prod->product_price >= 10000) {

            ?>
<!--            <div class="groupbuy_btn">Group Buy</div>-->
            <?php
        }
        ?>
    </div>
    <?php
}
else
{
    ?>
    <div class="detail_opt_itm">
        <label class="equal_spacin">QUANTITY</label>

        <label class="label_float1">Out Of Stock</label>

    </div>
<?php
}
?>
<script type="text/javascript">
    $(document).ready(function(){

        $('.group_buy').click(function(){
            $('#group_buy_popup_overlay').css('display', 'block');
            $('#group_buy_popup').css('display', 'block');
        });

        $('.group_buy_close_pop_up').click(function(){
            $('#group_buy_popup_overlay').css('display', 'none');
            $('#group_buy_popup').css('display', 'none');
        });
    })


</script>