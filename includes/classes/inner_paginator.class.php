<?php
/* PHP Pagination Class
 * @date October 18, 2011
 * @license sedatalabs - http://sedatelabs.com/licenses/
 */
class Paginator{
	var $items_per_page;
	var $items_total;
	var $current_page;
	var $num_pages;
	var $mid_range;
	var $low;
	var $limit;
	var $return;
	var $default_ipp;
	var $querystring;
	var $ipp_array;
	var $pageName;

	function Paginator()
	{
		$this->current_page = 1;
		$this->mid_range = 9;
		$this->ipp_array = array(1,2,5,10,15,20);
		$this->items_per_page = (!empty($_GET['ipp'])) ? $_GET['ipp']:$this->default_ipp;
	}

	function paginate()
	{
		if(!isset($this->default_ipp)) $this->default_ipp=8;
		if(isset($_GET['ipp']) && $_GET['ipp'] == '10')
		{
			$this->num_pages = 1;
//			$this->items_per_page = $this->default_ipp;
		}
		else
		{
			if(!is_numeric($this->items_per_page) OR $this->items_per_page <= 0) $this->items_per_page = $this->default_ipp;
			$this->num_pages = ceil($this->items_total/$this->items_per_page);
		}
		$this->current_page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1 ; // must be numeric > 0
		$prev_page = $this->current_page-1;
		$next_page = $this->current_page+1;
		if($_GET)
		{
			$args = explode("&",$_SERVER['QUERY_STRING']);
			foreach($args as $arg)
			{
				$keyval = explode("=",$arg);
				if($keyval[0] != "page" And $keyval[0] != "ipp") $this->querystring .= "&" . $arg;
			}
		}

		if($_POST)
		{
			foreach($_POST as $key=>$val)
			{
				if($key != "page" And $key != "ipp") $this->querystring .= "&$key=$val";
			}
		}
		if($this->num_pages > 0)
		{
			//$this->return = ($this->current_page > 1 And $this->items_total >= 0) ? "<a class=\"paginate prev\" href=\"$_SERVER[PHP_SELF]?page=$prev_page$this->querystring\">Prev</a> ":"<span class=\"inactive prev\" href=\"#\">Prev</span> ";

			$this->start_range = $this->current_page - floor($this->mid_range/2);
			$this->end_range = $this->current_page + floor($this->mid_range/2);

			if($this->start_range <= 0)
			{
				$this->end_range += abs($this->start_range)+1;
				$this->start_range = 1;
			}
			if($this->end_range > $this->num_pages)
			{
				$this->start_range -= $this->end_range-$this->num_pages;
				$this->end_range = $this->num_pages;
			}
			$this->range = range($this->start_range,$this->end_range);

			for($i=1;$i<=$this->num_pages;$i++)
			{
				if($this->range[0] > 2 And $i == $this->range[0]) $this->return .= "";
				// loop through all pages. if first, last, or in range, display
				if($i==1 Or $i==$this->num_pages Or in_array($i,$this->range))
				{
					$this->return .= ($i == $this->current_page And isset($_GET['page']) And $_GET['page'] != '10') ? "<li class=\"active\" ><a title=\"Go to page $i of $this->num_pages\"href=\"#\">$i</a></li>  ": ((!isset($_GET['page']) and ($i==1)) or (isset($_GET['page']) && $_GET['page']== '10') and ($i==10)) ? "<li><a class=\"active\"   title=\"Go to page $i of $this->num_pages\" href=\"$_SERVER[PHP_SELF]?page=$i$this->querystring\">$i</a></li>  " :"<li> <a class=\"paginate\"   title=\"Go to page $i of $this->num_pages\" href=\"$_SERVER[PHP_SELF]?page=$i$this->querystring\">$i</a></li> ";
				}
				if($this->range[$this->mid_range-1] < $this->num_pages-1 And $i == $this->range[$this->mid_range-1]) $this->return .= "  ";
			}
			//$this->return .= (($this->current_page < $this->num_pages And $this->items_total >= 1) And (isset($_GET['page']) && $_GET['page'] != '10') And $this->current_page > 0) ? "<a class=\"paginate next\" href=\"$_SERVER[PHP_SELF]?page=$next_page$this->querystring\">Next</a>\n": ($this->current_page < $this->num_pages And $this->items_total >= 1) ? " <a class=\"paginate next\" href=\"$_SERVER[PHP_SELF]?page=$next_page$this->querystring\">Next</a>\n" :"<span class=\"inactive next\" href=\"#\">&raquo; Next</span>\n";
			//$this->return .= ($_GET['page'] == '10') ? "<a class=\"current\" style=\"margin-left:10px\" href=\"#\">All</a> \n":"<a class=\"paginate\" style=\"margin-left:10px\" href=\"$_SERVER[PHP_SELF]?page=1&ipp=All$this->querystring\">All</a> \n";
		}
		else
		{
			for($i=1;$i<=$this->num_pages;$i++)
			{
				$this->return .= ($i == $this->current_page) ? "<li class=\"active\" ><ahref=\"#\">$i</a></li> ":"<li><a class=\"paginate\" href=\"$_SERVER[PHP_SELF]?page=$i$this->querystring\">$i</a> </li>";
			}
			//$this->return .= "<a class=\"paginate\" href=\"$_SERVER[PHP_SELF]?page=1&ipp=All$this->querystring\">All</a> \n";
		}
		$this->low = ($this->current_page <= 0) ? 0:($this->current_page-1) * $this->items_per_page;
		if($this->current_page <= 0) $this->items_per_page = 0;
		$this->limit = (isset( $_GET['ipp']) && $_GET['ipp'] == '10') ? "":" LIMIT $this->low,$this->items_per_page";
	}
	function display_pages()
	{
		return $this->return;
	}
	///// for ajax paging
	
		function ajax_paginate()
	{
		if(!isset($this->default_ipp)) $this->default_ipp=8;
		if(isset($_GET['ipp']) && $_GET['ipp'] == '10')
		{
			$this->num_pages = 1;
//			$this->items_per_page = $this->default_ipp;
		}
		else
		{
			if(!is_numeric($this->items_per_page) OR $this->items_per_page <= 0) $this->items_per_page = $this->default_ipp;
			$this->num_pages = ceil($this->items_total/$this->items_per_page);
		}
		$this->current_page = (isset($_GET['page'])) ? (int) $_GET['page'] : 1 ; // must be numeric > 0
		$prev_page = $this->current_page-1;
		$next_page = $this->current_page+1;
		if($_GET)
		{
			$args = explode("&",$_SERVER['QUERY_STRING']);
			foreach($args as $arg)
			{
				$keyval = explode("=",$arg);
				if($keyval[0] != "page" And $keyval[0] != "ipp") $this->querystring .= "&" . $arg;
			}
		}

		if($_POST)
		{
			foreach($_POST as $key=>$val)
			{
				if($key != "page" And $key != "ipp") $this->querystring .= "&$key=$val";
			}
		}
		if($this->num_pages > 0)
		{
			//$this->return = ($this->current_page > 1 And $this->items_total >= 0) ? "<a class=\"paginate prev\" href=\"$this->pageName?page=$prev_page$this->querystring\">Prev</a> ":"<span class=\"inactive prev\" href=\"#\">Prev</span> ";

			$this->start_range = $this->current_page - floor($this->mid_range/2);
			$this->end_range = $this->current_page + floor($this->mid_range/2);

			if($this->start_range <= 0)
			{
				$this->end_range += abs($this->start_range)+1;
				$this->start_range = 1;
			}
			if($this->end_range > $this->num_pages)
			{
				$this->start_range -= $this->end_range-$this->num_pages;
				$this->end_range = $this->num_pages;
			}
			$this->range = range($this->start_range,$this->end_range);

			for($i=1;$i<=$this->num_pages;$i++)
			{
				if($this->range[0] > 2 And $i == $this->range[0]) $this->return .= "";
				// loop through all pages. if first, last, or in range, display
				if($i==1 Or $i==$this->num_pages Or in_array($i,$this->range))
				{
					$this->return .= ($i == $this->current_page And isset($_GET['page']) And $_GET['page'] != '10') ? "<li class=\"active\" ><a title=\"Go to page $i of $this->num_pages\"href=\"#\">$i</a></li> ": ((!isset($_GET['page']) and ($i==1)) or (isset($_GET['page']) && $_GET['page']== '10') and ($i==10)) ? "<li class=\"active\"  ><a title=\"Go to page $i of $this->num_pages\" href=\"$this->pageName?page=$i$this->querystring\">$i</a> </li>" :" <li class=\"paginate\"  ><a title=\"Go to page $i of $this->num_pages\" href=\"$this->pageName?page=$i$this->querystring\">$i</a></li> ";
				}
				if($this->range[$this->mid_range-1] < $this->num_pages-1 And $i == $this->range[$this->mid_range-1]) $this->return .= "  ";
			}
			//$this->return .= (($this->current_page < $this->num_pages And $this->items_total >= 1) And (isset($_GET['page']) && $_GET['page'] != '10') And $this->current_page > 0) ? "<a class=\"paginate next\" href=\"$this->pageName?page=$next_page$this->querystring\">Next</a>\n": ($this->current_page < $this->num_pages And $this->items_total >= 1) ? " <a class=\"paginate next\" href=\"$this->pageName?page=$next_page$this->querystring\">Next</a>\n" :"<span class=\"inactive next\" href=\"#\">&raquo; Next</span>\n";
			//$this->return .= ($_GET['page'] == '10') ? "<a class=\"current\" style=\"margin-left:10px\" href=\"#\">All</a> \n":"<a class=\"paginate\" style=\"margin-left:10px\" href=\"$_SERVER[PHP_SELF]?page=1&ipp=All$this->querystring\">All</a> \n";
		}
		else
		{
			for($i=1;$i<=$this->num_pages;$i++)
			{
				$this->return .= ($i == $this->current_page) ? "<li class=\"active\" ><ahref=\"#\">$i</a></li> ":"<li class=\"paginate\" ><ahref=\"$this->pageName?page=$i$this->querystring\">$i</a> </li>";
			}
			//$this->return .= "<a class=\"paginate\" href=\"$_SERVER[PHP_SELF]?page=1&ipp=All$this->querystring\">All</a> \n";
		}
		$this->low = ($this->current_page <= 0) ? 0:($this->current_page-1) * $this->items_per_page;
		if($this->current_page <= 0) $this->items_per_page = 0;
		$this->limit = (isset( $_GET['ipp']) && $_GET['ipp'] == '10') ? "":" LIMIT $this->low,$this->items_per_page";
	}
	
	
}