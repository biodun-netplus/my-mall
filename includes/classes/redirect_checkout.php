<?php
include 'env.php';
include_once '../classes/db_connect.php';
include_once '../model/read.php';
include_once '../model/create.php';
include_once '../model/update.php';
unset($_SESSION['coupon_value']);
unset($_SESSION['coupon_id']);

$read = new read();
$create = new create();
$update = new update();

if(!isset($_COOKIE['webmall_cart'])){
    header('location:' . SITE_URL );
} else {
    $data = $_COOKIE['webmall_cart'];
    $userId = $_COOKIE['webmall_customer'];

    if($_REQUEST['deliveryAddress'] == 0)
    {
        $total = htmlspecialchars($_REQUEST['t'], ENT_QUOTES);
        $delivery = htmlspecialchars($_REQUEST['d'], ENT_QUOTES);
        $coupon_id = htmlspecialchars($_REQUEST['coupon_id'], ENT_QUOTES);
        $coupon_value = htmlspecialchars($_REQUEST['coupon_price'], ENT_QUOTES);
        $provider_id = htmlspecialchars($_REQUEST['provider_id'], ENT_QUOTES);

        $data = stripslashes($data);
        $savedCartArray = json_decode($data, true);
        $orderId = 'wm'.date('his').rand(10,99);

        $i = 0;

        while ($i < count($savedCartArray)) {
            $arrayCart = explode('|', $savedCartArray[$i]);
            $product_id = $arrayCart[0];
            $quantity = $arrayCart[1];
            $size = $arrayCart[2];

            $options = array('id' => $product_id);
            $orderedProduct = $read->read_single('products', $options);
            $now = date('Y-m-d h:i:s');

            if(isset($_SESSION['group_buy']))
            {
                /*echo '2<br>';*/
                $product_price = $orderedProduct['product_price']-((10/100)*$orderedProduct['product_price']);
                /*echo $product_price*/;
            }
            else
            {
                $product_price = $orderedProduct['product_price'];
            }
            $options = array('order_id' => $orderId,
                'product_id' => $product_id,
                'customer_id' => $userId,

                'address_id' => '0',
                'product_price' => $product_price,
                'product_quantity' => $quantity,
                'product_size' => $size,
                'order_placed' => $now,
                'order_shipping_price' => $delivery,
                'order_status' => 'Pending',
                'payment_status' => 'Not paid',
                'payment_ref' => '-',
                'payment_option' => '-',
                'payment_made' => '-',
                'created_at' => $now,
                'updated_at' => $now,
                'coupon_id' =>$coupon_id,
                'coupon_price' =>$coupon_value,
            );

            $create_it = $create->add('order_details', $options);
            $last_id = mysql_insert_id();
            //var_dump($create_it);
            if(empty($create_it))
            {
                //echo 'me';
                $merchant_query =mysql_query("SELECT * from order_details order by id desc limit 1");
                $merchant_fetch = mysql_fetch_array($merchant_query);

                //get merchant _ id from product id
                $merchant_parameter = array('id' => $merchant_fetch['product_id']);

                $merchant_get = $read->read_single('products',$merchant_parameter);
                //var_dump($merchant_get);

                $merchant_id = $merchant_get['pid'];
                $order_id = $merchant_fetch['id'];
                //echo 'jfjfj'.$order_id;

                //echo $merchant_id;
                $xx ="UPDATE order_details SET merchant_id = '".$merchant_id."' WHERE id = $order_id ";

                //echo $xx;
                $localhost = mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
                mysql_select_db(DB_DATABASE, $localhost);
                /*echo $xx.'<br>';
                die;*/
                $xx1 = mysql_query($xx, $localhost) or die(mysql_error());

                mysql_close($localhost);



            }

            $i++;
        }
        $_SESSION['pickup'] = -1;
    }
    else
    {
        $total = htmlspecialchars($_REQUEST['t'], ENT_QUOTES);
        $package = htmlspecialchars($_REQUEST['p'], ENT_QUOTES);
        $delivery = htmlspecialchars($_REQUEST['d'], ENT_QUOTES);
        $coupon_id = htmlspecialchars($_REQUEST['coupon_id'], ENT_QUOTES);
        $coupon_value = htmlspecialchars($_REQUEST['coupon_price'], ENT_QUOTES);
        $provider_id = htmlspecialchars($_REQUEST['provider_id'], ENT_QUOTES);

        $data = stripslashes($data);
        $savedCartArray = json_decode($data, true);
        $orderId = 'wm'.date('his').rand(10,99);

        $i = 0;

        while ($i < count($savedCartArray)) {
            $arrayCart = explode('|', $savedCartArray[$i]);
            $product_id = $arrayCart[0];
            $quantity = $arrayCart[1];
            $size = $arrayCart[2];

            $options = array('id' => $product_id);
            $orderedProduct = $read->read_single('products', $options);
            $now = date('Y-m-d h:i:s');
          /*  if($orderedProduct['product_price']>=20000)
            {
                $product_price = $orderedProduct['product_price']+500;
            }
            else*/if(isset($_SESSION['group_buy']))
            {
                /*echo '2<br>';*/
                $product_price = $orderedProduct['product_price']-((10/100)*$orderedProduct['product_price']);
                /*echo $product_price*/;
            }
            else
            {
                $product_price = $orderedProduct['product_price'];
            }

            $options = array('order_id' => $orderId,
                'product_id' => $product_id,
                'customer_id' => $userId,

                'address_id' => $_REQUEST['deliveryAddress'],
                'product_price' => $product_price,
                'product_quantity' => $quantity,
                'product_size' => $size,
                'order_placed' => $now,
                'order_shipping_price' => $delivery,
                'order_status' => 'Pending',
                'payment_status' => 'Not paid',
                'payment_ref' => '-',
                'payment_option' => '-',
                'payment_made' => '-',
                'created_at' => $now,
                'updated_at' => $now,
                'coupon_id' =>$coupon_id,
                'coupon_price' =>$coupon_value,
            );

            $create_it = $create->add('order_details', $options);
            //var_dump($create_it);
            if(empty($create_it))
            {
                //echo 'me';
                $merchant_query =mysql_query("SELECT * from order_details order by id desc limit 1");
                $merchant_fetch = mysql_fetch_array($merchant_query);

                //get merchant _ id from product id
                $merchant_parameter = array('id' => $merchant_fetch['product_id']);

                $merchant_get = $read->read_single('products',$merchant_parameter);
                //var_dump($merchant_get);

                $merchant_id = $merchant_get['pid'];
                $order_id = $merchant_fetch['id'];
                //echo 'jfjfj'.$order_id;

                //echo $merchant_id;
                $xx ="UPDATE order_details SET merchant_id = '".$merchant_id."' WHERE id = $order_id ";

                //echo $xx;
                $localhost = mysql_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
                mysql_select_db(DB_DATABASE, $localhost);
                /*echo $xx.'<br>';
                die;*/
                $xx1 = mysql_query($xx, $localhost) or die(mysql_error());

                mysql_close($localhost);



            }

            $i++;
        }
    }



    $_SESSION['webmall_order_id'] = $orderId;
    $_SESSION['webmall_state'] = $_REQUEST['s'];
    header('location:' . SITE_URL . 'checkout');
}