<?php

function msidn($number){
    if(substr($number, 0, 4) === "+234"){
        $number = substr($number, 4);
    }

    if(substr($number, 0, 3) === "234"){
        $number = substr($number, 3);
    }

    if(substr($number, 0, 1) === "0"){
        $number = substr($number, 1);
    }

    return $number;
}
?>