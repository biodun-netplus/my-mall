<?php
include 'env.php';
include_once '../classes/db_connect.php';
include_once '../model/read.php';

$read = new read();


if(isset($_COOKIE['webmall_cart'])){

    $data = $_COOKIE['webmall_cart'];
    $data = stripslashes($data);
    $savedCartArray = json_decode($data, true);

    $i = 0;
    $total = 0;

    if($savedCartArray[0]<0){
        $count = 0;
    } else {
        $count = count($savedCartArray);
    }

    while($i < count($savedCartArray)){
        $arrayCart = explode('|',$savedCartArray[$i]);
        $product_id = $arrayCart[0];
        $quantity = $arrayCart[1];
        $options = array('id'=>$product_id );
        $orderedProduct = $read->read_single('products',$options);
        /*if($orderedProduct['product_price'] >20000)
        {
            $product_price= $orderedProduct['product_price']+500;
        }
        else
        {*/
            $product_price= $orderedProduct['product_price'];
//        }
        $total += ($product_price * $quantity);
        $i++;
    }

    echo '₦ '. number_format($total,2).' ('. $count .')';
} else {
    echo '₦ 0.00 (0)';
}

?>