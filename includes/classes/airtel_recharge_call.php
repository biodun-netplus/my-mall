<?php
function vend_airtel_recharge($msisdn,$amount){
    include 'env.php';
    include_once 'db_connect.php';
    include_once '../model/update.php';
    include_once '../model/create.php';
//var_dump($_SESSION['group_buy']);
    $create = new create();
    $update = new update();
    
    $url = 'https://172.24.4.21:4443/pretups/C2SReceiver?REQUEST_GATEWAY_CODE=NETSTG&REQUEST_GATEWAY_TYPE=EXTGW&LOGIN=prentstg&PASSWORD=7fbd05ab94d03a0c8b02c39b0f48e992&SOURCE_TYPE=EXTGW&SERVICE_PORT=191';
    $xml = '<?xml version="1.0"?>
<!DOCTYPE COMMAND PUBLIC "-//Ocam//DTD XML Command 1.0//EN""xml/command.dtd">
<COMMAND>
<TYPE>EXRCTRFREQ</TYPE>
<DATE>'.date('Y-m-d').'</DATE>
<EXTNWCODE>NG</EXTNWCODE>
<MSISDN>8087392244</MSISDN>
<PIN>6677</PIN>
<LOGINID></LOGINID>
<PASSWORD></PASSWORD>
<EXTCODE></EXTCODE>
<EXTREFNUM></EXTREFNUM>
<MSISDN2>'.$msisdn.'</MSISDN2>
<AMOUNT>'.$amount.'</AMOUNT>
<LANGUAGE1>1</LANGUAGE1>
<LANGUAGE2>1</LANGUAGE2>
<SELECTOR>1</SELECTOR>
</COMMAND>';

    $data = array('trans_id' => $_REQUEST['t'], 'service' => 'airtel', 'url' => $url, 'message' => html_entity_decode($xml), 'created_at' => date('Y-m-d h:i:s'));
    $create->add('logs', $data);

    $context = stream_context_create(array(
        'http' => array(
            'method' => 'POST',
            'header' => 'Content-Type: application/xml',
            'content' => $xml
        )
    ));

    if (!$response = file_get_contents($url, false, $context)) {
        $response = error_get_last();
        $array = null;
    } else {
        $xmlObject = simplexml_load_string($response);
        $json = json_encode($xmlObject);
        $array = json_decode($json,TRUE);
    }


    $updates = array('response' => html_entity_decode($response));
    $option = array('trans_id' => $_REQUEST['t']);
    $update->edit('logs', $updates, $option);

    return $array;
}

?>