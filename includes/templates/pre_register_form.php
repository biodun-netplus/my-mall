<div class="create-a-store">
    <div class="container">

        <div class="register-txt">
            <h2 style="font-size:34px;">Time is money. Sell on WebMall in seconds.</h2>
            <h1 style="font-size:18px;">Sign up now and start selling</h1>
        </div>
        <!--end of register-txt-->

        <div class="register">
            <form action="<?php echo SITE_URL ?>registration" method="post">
                <input type="text" name="fname" placeholder="Your Full Name" required>
                <input type="text" name="email" placeholder="Your Email Address" required>
                <input type="password" name="password" placeholder=" Your Password" required>
                <input type="submit" name="presubmit" value="Create a Store"
                       style="background-color:#FF9E00; color:#ffffff;"/>
                <br class="clear" />
            </form>
        </div>
    </div>
</div>