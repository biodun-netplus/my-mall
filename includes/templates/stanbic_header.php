<!DOCTYPE html>
<html>

<head lang="en">
    <meta charset="UTF-8">
    <meta name="keywords" content= "" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" href="<?php echo SITE_URL ?>images/favicon.png" type="image/x-icon">
    <title>Season of Delight | Stanbic IBTC Bank</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo SITE_URL ?>css/reset.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL ?>css/stanbic.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL ?>css/footer.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL ?>css/nivo-slider.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL ?>themes/default/default.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL ?>css/slick.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL ?>css/slick-theme.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL ?>css/owl.carousel.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL ?>css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo SITE_URL ?>css/owl.theme.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo SITE_URL ?>css/stanbic_responsive.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<?php echo SITE_URL; ?>js/jquery-1.9.0.min.js"></script>

    <script type="text/javascript" src="<?php echo SITE_URL; ?>js/jquery.lazyload.min.js"></script>




    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-57214225-1', 'auto');
      ga('send', 'pageview');

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("img.lazy").lazyload({
                effect : "fadeIn"
            });
        });
    </script>
   
</head>



