<style>

    #cat_div:hover {
        background-color: #ccc;
    }

    .cat_ul {
        padding-top: 10px;
    }

    .cat_ul li a {
        width: 100%;
        height: auto;
        display: block;
        color: #333;
        font-size: 15px;
        padding-left: 10px;
        line-height: 30px;
        text-decoration: none;
        text-align: left;

        border-bottom: 1px solid #CDE8C3;
    }
</style>
<?php
/* getting all active merchants */
function startsWithInnerInner($haystack, $needle)
{
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}

try {
    $stmt = $dbh->prepare("SELECT
                            DISTINCT users.id,
                                users.first_name
                                , users.display_name
                            ,  users.isactive
                            FROM
                                users
                                INNER JOIN products
                                    ON (users.id = products.pid)
                            WHERE users.isactive = 't' ORDER BY users.first_name;");
    $stmt->execute();
    $merchant_data = $stmt->fetchAll();

    $m = 0;
    While ($m < count($merchant_data)) {
        if (startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'A') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'B') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'C') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'D') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'E')) {
            $AEmerchant_data[] = $merchant_data[$m];
        }
        if (startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'F') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'G') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'H') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'I') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'J')) {
            $FJmerchant_data[] = $merchant_data[$m];
        }
        if (startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'K') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'L') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'M') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'N') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'O')) {
            $KOmerchant_data[] = $merchant_data[$m];
        }
        if (startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'P') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'Q') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'R') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'S') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'T')) {
            $PTmerchant_data[] = $merchant_data[$m];
        }
        if (startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'U') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'V') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'W') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'X') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'Y') || startsWithInnerInner(strtoupper($merchant_data[$m]['first_name']), 'Z')) {
            $UZmerchant_data[] = $merchant_data[$m];
        }
        $m++;
    }


} catch (PDOException $e) {

}
?>
<div id="inner_header">
    <div class="container">
        <div class="docked_header_logo">
            <a href="<?php echo SITE_URL; ?>"><img src="<?php echo SITE_URL; ?>images/inside_logo.png"/></a>
        </div>

        <div class="inner_header_itm no-mobile">
            <div class="categories-list" style="height: auto">
                <div class="dropdown_btn">Categories <span class="caret"></span></div>
                <div class="cat_dropdown_con">
                    <div class="cat_dropdown_arrow">
                        <div class="arrow"></div>
                    </div>

                    <!-- the new megmenu for category -->
                    <div class="cat_dropdown">
                        <ul class="cd-dropdown-content">
                            <?php

                            $stmt = $dbh->prepare("SELECT
                                                        id
                                                        , category_name
                                                        , pid
                                                        , sort_order
                                                        , isactive
                                                    FROM
                                                        category
                                                        WHERE isactive = 't' ORDER BY sort_order ASC;");
                            $stmt->execute();
                            $category_data = $stmt->fetchAll();

                            $new = new users;
                            $c = 0;
                            while ($c < count($category_data)) {
                                if ($category_data[$c]['pid'] < 1) {
                                    ?>
                                    <li class="sec-dropdown">
                                        <a href="<?php echo SITE_URL . $new->FormatN($category_data[$c]['category_name']) . "/" . $category_data[$c]['id'] ?>">
                                            <?php echo ucwords(strtolower($category_data[$c]['category_name'])); ?>
                                        </a>

                                        <div class="cd-secondary-dropdown">
                                            <div class="cd-sec-dd-links">
                                                <h5><?php echo ucwords(strtolower($category_data[$c]['category_name'])); ?></h5>
                                                <ul>
                                                    <li>
                                                        <a href="<?php echo SITE_URL . $new->FormatN($category_data[$c]['category_name']) . "/" . $category_data[$c]['id'] ?>">All</a>
                                                    </li>
                                                    <?php
                                                   $category_id = $category_data[$c]['id'];

                                                    $d = 0;
                                                    while ($d < count($category_data)) {
                                                        if ($category_data[$d]['pid']==$category_id) {
                                                            ?>
                                                            <li>
                                                                <a href="<?php echo SITE_URL . $new->FormatN($category_data[$d]['category_name']) . "/" . $category_data[$d]['id'] ?>"><?php echo ucwords(strtolower($category_data[$d]['category_name'])); ?></a>
                                                            </li>
                                                        <?php
                                                        }
                                                        $d++;
                                                    }
                                                    ?>
                                                </ul>
                                            </div>
                                            <div class="cd-sec-dd-img">
                                                <img src="<?php echo SITE_URL; ?>images/menu_ext.jpg"/>
                                            </div>
                                        </div>
                                    </li>
                                <?php
                                }
                                $c++;
                            } ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="merchant-list">
                <div class="dropdown_btn">Merchants <span class="caret"></span></div>
                <div class="mer_dropdown_con">
                    <div class="mer_dropdown_arrow">
                        <div class="arrow"></div>
                    </div>
                    <div class="mer_dropdown">
                        <div class="mer_dropdown_btm">
                            <ul class="tabs" data-persist="true">
                                <li><a href="#atoe">A-E</a></li>
                                <li><a href="#ftoj">F-J</a></li>
                                <li><a href="#ktoo">K-O</a></li>
                                <li><a href="#ptot">P-T</a></li>
                                <li><a href="#utoz">U-Z</a></li>
                            </ul>

                            <div class="tabcontents">
                                <div class="tab-pane" id="atoe">
                                    <div class="tab-pane-ins">
                                        <ul>
                                            <?php
                                            $i = 0;
                                            $i2 = 0;
                                            $selectDataAEcount = round(count($AEmerchant_data) / 4);

                                            while ($i < count($AEmerchant_data)){
                                            ?>
                                            <li>
                                                <a href="<?php echo SITE_URL . $AEmerchant_data[$i]['display_name'];?>"><?php
                                                    echo substr($AEmerchant_data[$i]['first_name'], 0, 20);?></a>
                                            </li>
                                            <?php
                                            if ($i2 == $selectDataAEcount){
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="tab-pane-ins">
                                        <ul><?php $i2 = 0;
                                            }
                                            $i++;
                                            $i2++;
                                            } ?>
                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-pane" id="ftoj">
                                    <div class="tab-pane-ins">
                                        <ul>
                                            <?php
                                            $j = 0;
                                            $j2 = 0;
                                            $selectDataFJcount = round(count($FJmerchant_data) / 4);

                                            while ($j < count($FJmerchant_data)){
                                            ?>
                                            <li>
                                                <a href="<?php echo SITE_URL . $FJmerchant_data[$j]['display_name'];?>"><?php
                                                    echo substr($FJmerchant_data[$j]['first_name'], 0, 20);?></a>
                                            </li>
                                            <?php
                                            if ($j2 == $selectDataFJcount){
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="tab-pane-ins">
                                        <ul><?php $j2 = 0;
                                            }
                                            $j++;
                                            $j2++;
                                            } ?>
                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-pane" id="ktoo">
                                    <div class="tab-pane-ins">
                                        <ul>
                                            <?php
                                            $k = 0;
                                            $k2 = 0;
                                            $selectDataKOcount = round(count($KOmerchant_data) / 4);

                                            while ($k < count($KOmerchant_data)){
                                            ?>
                                            <li>
                                                <a href="<?php echo SITE_URL . $KOmerchant_data[$k]['display_name'];?>"><?php
                                                    echo substr($KOmerchant_data[$k]['first_name'], 0, 20);?></a>
                                            </li>
                                            <?php
                                            if ($k2 == $selectDataKOcount){
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="tab-pane-ins">
                                        <ul><?php $k2 = 0;
                                            }
                                            $k++;
                                            $k2++;
                                            } ?>
                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-pane" id="ptot">
                                    <div class="tab-pane-ins">
                                        <ul>
                                            <?php
                                            $l = 0;
                                            $l2 = 0;
                                            $selectDataPTcount = round(count($PTmerchant_data) / 4);

                                            while ($l < count($PTmerchant_data)){
                                            ?>
                                            <li>
                                                <a href="<?php echo SITE_URL . $PTmerchant_data[$l]['display_name'];?>"><?php
                                                    echo substr($PTmerchant_data[$l]['first_name'], 0, 20);?></a>
                                            </li>
                                            <?php
                                            if ($l2 == $selectDataPTcount){
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="tab-pane-ins">
                                        <ul><?php $l2 = 0;
                                            }
                                            $l++;
                                            $l2++;
                                            } ?>
                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-pane" id="utoz">
                                    <div class="tab-pane-ins">
                                        <ul>
                                            <?php
                                            $n = 0;
                                            $n2 = 0;
                                            $selectDataUZcount = round(count($UZmerchant_data) / 4);

                                            while ($n < count($UZmerchant_data)){
                                            ?>
                                            <li>
                                                <a href="<?php echo SITE_URL . $UZmerchant_data[$n]['display_name'];?>"><?php
                                                    echo substr($UZmerchant_data[$n]['first_name'], 0, 20);?></a>
                                            </li>
                                            <?php
                                            if ($n2 == $selectDataUZcount){
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="tab-pane-ins">
                                        <ul><?php $n2 = 0;
                                            }
                                            $n++;
                                            $n2++;
                                            } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mer_dropdown_top"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="inner_header_itm">
            <div id="docked_search_wrap">
                <div class="search">
                    <form id="search_form" action="<?php echo SITE_URL ?>search" method="get">
                        <input name="search" style="width: 80%" type="text" class="search-text" value="<?php if
                        (isset($_REQUEST['search'])) {
                            echo $_REQUEST['search'];
                        } ?>"
                               placeholder="I am searching for... "/>
                        <button type="submit" class="search_btn"
                                onclick="javascript:document.getElementById('search_form').submit()"></button>
                    </form>
                </div>
            </div>
        </div>


        <div class="inner_header_itm2">
            <div class="sellonline_btn"><a href="<?php echo SITE_URL ?>sell-online"><img
                        src="<?php echo SITE_URL; ?>images/sell-online-btn.gif"/></a></div>
            <div class="cart_wrap">
                <a class="no_style" href="<?php echo SITE_URL; ?>user_cart">
                    <span class="cart_txt add_to_cart no-mobile">&#x20A6; 0.00</span>
                    <span class="cart_icon"></span>
                </a>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        $.ajax({
            url: "<?php echo SITE_URL ?>includes/classes/get_cart.php",
            type: "POST",
            success: function (value) {
                $('.add_to_cart').html(value);
            }
        });
    });
</script>