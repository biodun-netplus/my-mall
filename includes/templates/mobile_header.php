<div id="mobile_header">
    <div class="container">
        <div class="mobile_header_wrap">
            <div class="mobile_social">
                <a href="https://www.youtube.com/webmallng">
                    <img src="<?php echo SITE_URL; ?>images/youtube.png"/>
                </a>

                <a href="https://www.twitter.com/webmallng">
                    <img src="<?php echo SITE_URL; ?>images/twitter.png"/>
                </a>

                <a href="https://www.facebook.com/webmallng">
                    <img src="<?php echo SITE_URL; ?>images/facebook.png"/>
                </a>

                <a href="https://www.instagram.com/webmallng" title="Follow us on instagram">
                    <img src="<?php echo SITE_URL; ?>images/instagram.jpg"/>
                </a>

                <a href="#" title="568A29B7">
                    <img src="<?php echo SITE_URL; ?>images/bbm.jpg"/>
                </a>

                <a href="#" title="08187782542">
                    <img src="<?php echo SITE_URL; ?>images/whatsapp.jpg"/>
                </a>

                <br class="clearfix" />
            </div>

            <div class="mobile_nav_itm">
                <a href="">Open a Store</a>
            </div>

            <div class="mobile_nav_itm" id="disp_mobile_cat">
                <a href="#">Categories</a>
            </div>
            <div class="mobile_cat">
                <ul>

                    <?php $QueryProduct = mysql_query("select * from category  where isactive = 't' AND pid=0 ");
                    $new = new users;
                    while ($Catt = mysql_fetch_object($QueryProduct)) {
                        ?>
                        <li>
                            <a href="<?php echo SITE_URL.$new->FormatN($Catt->category_name)."/".$Catt->id ?>">
                                <?php echo ucwords(strtolower($Catt->category_name)); ?>
                            </a>
                        </li>
                    <?php
                    } ?>
                </ul>
            </div>
        </div>
    </div>
</div>