<script type="text/javascript">
    setTimeout(function(){var a=document.createElement("script");
        var b=document.getElementsByTagName("script")[0];
        a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0013/2685.js?"+Math.floor(new Date().getTime()/3600000);
        a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>

<div id="mobile_menu_btn">
    <div class="container">
        <div id="hide_show_btn" title="Click to Open">MENU</div>
    </div>
</div>

<div id="fixed_header">

    <div class="container">
        <div class="socialmedia no-mobile">
            <div class="socialmedia_icon">
                <a href="https://www.youtube.com/webmallng">
                    <img src="<?php echo SITE_URL; ?>images/youtube.png"/>
                </a>
            </div>

            <div class="socialmedia_icon">
                <a href="https://www.twitter.com/webmallng">
                    <img src="<?php echo SITE_URL; ?>images/twitter.png"/>
                </a>
            </div>
            <div class="socialmedia_icon">
                <a href="https://www.facebook.com/webmallng">
                    <img src="<?php echo SITE_URL; ?>images/facebook.png"/>
                </a>
            </div>
            <div class="socialmedia_icon">
                <a href="https://www.instagram.com/webmallng" title="Follow us on instagram">
                    <img src="<?php echo SITE_URL; ?>images/instagram.jpg"/>
                </a>
            </div>
            <div class="socialmedia_icon">
                <a href="#" title="568A29B7">
                    <img src="<?php echo SITE_URL; ?>images/bbm.jpg"/>
                </a>
            </div>
            <div class="socialmedia_icon">
                <a href="#" title="08187782542">
                    <img src="<?php echo SITE_URL; ?>images/whatsapp.jpg"/>
                </a>
            </div>
        </div>


        <div class="fixed_header_lft">
            <ul>
                <li class="no-mobile" style="background-color: #FE9A00;">
                    <a href=<?php echo SITE_URL ?>sell-online title="Register on WebMall">Open a store</a>
                </li>

                <?php if(!isset($_SESSION['provider_id'])){?>
                    <li class="login-dropdown">
                        <a href="#" title="Login as Merchant">Merchant Login<span class="caret2"></span></a>

                        <div id="login-dropdown-con">
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-box">
                                <div class="login-form">
                                    <h3>Merchant Login</h3>

                                    <div class="form_alert" id="form_msg"></div>

                                    <form name="loginform" method="post" id="loginform">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="EMAIL"
                                                   name="email" id="email">
                                        </div>

                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password"
                                                   placeholder="PASSWORD" id="password">
                                        </div>

                                        <div class="form-group">
                                            <a href="<?php echo SITE_URL ?>forgotpassword.php" class="" style="color:#333;
                                        ">FORGOT PASSWORD?</a>
                                        </div>
                                        <br />

                                        <div>
                                            <button type="submit" id="login" name="login"
                                                    class="login_btn">LOGIN
                                            </button>
                                        </div>

                                        <br class="clearfix"/>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php } else { ?>
                    <li>
                        <a href="<?php echo SITE_URL?>my-profile">Profile</a>
                    </li>
                <?php } ?>
            </ul>
        </div>


        <div class="fixed_header_rgt">
            <span class="fixed_header_txt">
                <a href="#">0809 999 0660</a>
            </span>
        </div>


        <div class="fixed_header_mid no-mobile" id="hidden_div">
            <span class="fixed_header_txt">
                <a href="<?php echo SITE_URL ?>campaigns">Marketing</a>
            </span>

            <span class="dot"></span>

            <span class="fixed_header_txt">
                <a href="<?php echo SITE_URL ?>sell-online#two">Payment</a>
            </span>

            <span class="dot"></span>

            <span class="fixed_header_txt">
                <a href="<?php echo SITE_URL ?>sell-online#three">Logistics</a>
            </span>

            <span class="dot"></span>

            <span class="fixed_header_txt">
                <a href="<?php echo SITE_URL ?>sell-online#four">Webstore</a>
            </span>
        </div>

        <br class="clearfix" />
    </div>
</div>
