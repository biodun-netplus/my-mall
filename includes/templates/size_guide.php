
<div class="popup_content">
    <div class="close_pop_up"></div>

    <div class="sg_container">
        <div class="sg_header">
            SIZING INFORMATION
        </div>

        <div class="sg_content">
            <div class="section">
                <h5>SIZING DISCLAIMER:</h5>
                <p>Due to international sizing guidelines, it is possible that some items will have packaging
                    labelled according to US or EU sizing conventions. Please be sure to try on the item to confirm
                    the incorrect size before returning it to us.</p>
            </div>

            <div class="section">
                <h4>SUITS</h4>
                <div class="sizing_lft">
                    <h5>Men's Suits</h5>
                    <table width="100%">
                        <thead>
                            <tr>
                            <th><strong>Size</strong></th>
                            <th>Inches</th>
                            <th>CM</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <strong>32&quot;</strong>
                            </td>
                            <td>32</td>
                            <td>81</td>
                        </tr>
                        <tr>
                            <td><strong>34&quot;</strong></td>
                            <td>34</td>
                            <td>86</td>
                        </tr>
                        <tr>
                            <td><strong>36&quot;</strong></td>
                            <td>36</td>
                            <td>91</td>
                        </tr>
                        <tr>
                            <td><strong>38&quot;</strong></td>
                            <td>38</td>
                            <td>96</td>
                        </tr>
                        <tr>
                            <td><strong>40&quot;</strong></td>
                            <td>40</td>
                            <td>101</td>
                        </tr>
                        <tr>
                            <td><strong>42&quot;</strong></td>
                            <td>42</td>
                            <td>106</td>
                        </tr>
                        <tr>
                            <td><strong>44&quot;</strong></td>
                            <td>44</td>
                            <td>111</td>
                        </tr>
                        <tr>
                            <td><strong>46&quot;</strong></td>
                            <td>46</td>
                            <td>116</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="sizing_rgt">
                    <h5>Women's Suits</h5>
                    <table width="100%">
                        <thead>
                        <tr>
                            <th><strong>UK Size</strong></th>
                            <th colspan="2"><strong>Bust</strong></th>
                            <th colspan="2"><strong>Waist</strong></th>
                            <th colspan="2"><strong>Hips</strong></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>Inches</th>
                            <th>CM</th>
                            <th>Inches</th>
                            <th>CM</th>
                            <th>Inches</th>
                            <th>CM</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>4</td>
                            <td>31</td>
                            <td>78</td>
                            <td>24</td>
                            <td>60</td>
                            <td>33</td>
                            <td>83.5</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>32</td>
                            <td>80.5</td>
                            <td>25</td>
                            <td>62.5</td>
                            <td>34</td>
                            <td>86</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>33</td>
                            <td>83</td>
                            <td>26</td>
                            <td>65</td>
                            <td>35</td>
                            <td>88.5</td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>35</td>
                            <td>88</td>
                            <td>28</td>
                            <td>70</td>
                            <td>37</td>
                            <td>93.5</td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td>37</td>
                            <td>93</td>
                            <td>30</td>
                            <td>75</td>
                            <td>39</td>
                            <td>98.5</td>
                        </tr>
                        <tr>
                            <td>14</td>
                            <td>39</td>
                            <td>98</td>
                            <td>31</td>
                            <td>80</td>
                            <td>41</td>
                            <td>103.5</td>
                        </tr>
                        <tr>
                            <td>16</td>
                            <td>41</td>
                            <td>103</td>
                            <td>33</td>
                            <td>85</td>
                            <td>43</td>
                            <td>108.5</td>
                        </tr>
                        <tr>
                            <td>18</td>
                            <td>44</td>
                            <td>110.5</td>
                            <td>36</td>
                            <td>92.5</td>
                            <td>46</td>
                            <td>116</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <br class="clearfix" />
            </div>

            <div class="section">
                <h4>SHIRTS</h4>
                <div class="sizing_lft">
                    <h5>Men</h5>
                    <table width="100%" class="three-column">
                        <thead>
                        <tr>
                            <th>
                                <strong>Size</strong>
                            </th>
                            <th colspan="2">
                                Chest Size
                            </th>
                            <th colspan="2">
                                Neck Size
                            </th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>Inches</th>
                            <th>CM</th>
                            <th>Inches</th>
                            <th>CM</th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td><strong>XXS</strong></td>
                            <td class="style44">32-34</td>
                            <td class="style23">81-86</td>
                            <td class="style3">14.5</td>
                            <td class="style4">37.5</td>
                        </tr>
                        <tr>
                            <td><strong>XS</strong></td>
                            <td class="style44">34-36</td>
                            <td class="style23">86-91</td>
                            <td class="style3">15</td>
                            <td class="style4">38.5</td>
                        </tr>
                        <tr>
                            <td><strong>S</strong></td>
                            <td class="style44">36-38</td>
                            <td class="style23">91-96</td>
                            <td class="style3">15.5</td>
                            <td class="style4">39.5</td>
                        </tr>
                        <tr>
                            <td><strong>M</strong></td>
                            <td class="style44">38-40</td>
                            <td class="style23">96-101</td>
                            <td class="style3">16</td>
                            <td class="style4">41.5</td>
                        </tr>
                        <tr>
                            <td><strong>L</strong></td>
                            <td class="style44">40-42</td>
                            <td class="style23">101-106</td>
                            <td class="style3">17</td>
                            <td class="style4">43.5</td>
                        </tr>
                        <tr>
                            <td><strong>XL</strong></td>
                            <td class="style44">42-44</td>
                            <td class="style23">106-111</td>
                            <td class="style3">17.5</td>
                            <td class="style4">
                                45.5
                            </td>
                        </tr>
                        <tr>
                            <td><strong>XXL</strong></td>
                            <td class="style44">44-46</td>
                            <td class="style23">111-116</td>
                            <td class="style3">18.5</td>
                            <td class="style4">47.5</td>
                        </tr>
                        <tr>
                            <td>
                                <strong>XXXL</strong></td>
                            <td class="style44">46-48</td>
                            <td class="style23">116-121</td>
                            <td class="style3">19.5</td>
                            <td class="style4">49.5</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="sizing_rgt">
                    <h5>Women's Shirts & Blouses</h5>
                    <table width="100%" class="seven-column">
                        <thead>
                        <tr>
                            <th><strong>UK Size</strong></th>
                            <th colspan="2"><strong>Bust</strong></th>
                            <th colspan="2"><strong>Waist</strong></th>
                            <th colspan="2"><strong>Hips</strong></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>Inches</th>
                            <th>CM</th>
                            <th>Inches</th>
                            <th>CM</th>
                            <th>Inches</th>
                            <th>CM</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>4</td>
                            <td>31</td>
                            <td>78</td>
                            <td>24</td>
                            <td>60</td>
                            <td>33</td>
                            <td>83.5</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>32</td>
                            <td>80.5</td>
                            <td>25</td>
                            <td>62.5</td>
                            <td>34</td>
                            <td>86</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>33</td>
                            <td>83</td>
                            <td>26</td>
                            <td>65</td>
                            <td>35</td>
                            <td>88.5</td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>35</td>
                            <td>88</td>
                            <td>28</td>
                            <td>70</td>
                            <td>37</td>
                            <td>93.5</td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td>37</td>
                            <td>93</td>
                            <td>30</td>
                            <td>75</td>
                            <td>39</td>
                            <td>98.5</td>
                        </tr>
                        <tr>
                            <td>14</td>
                            <td>39</td>
                            <td>98</td>
                            <td>31</td>
                            <td>80</td>
                            <td>41</td>
                            <td>103.5</td>
                        </tr>
                        <tr>
                            <td>16</td>
                            <td>41</td>
                            <td>103</td>
                            <td>33</td>
                            <td>85</td>
                            <td>43</td>
                            <td>108.5</td>
                        </tr>
                        <tr>
                            <td>18</td>
                            <td>44</td>
                            <td>110.5</td>
                            <td>36</td>
                            <td>92.5</td>
                            <td>46</td>
                            <td>116</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <br class="clearfix" />
            </div>

            <div class="section">
                <h4>TROUSERS</h4>
                <div class="sizing_lft">
                    <h5>Men's Trousers & Jeans</h5>
                    <table width="100%" class="three-column">
                        <thead>
                        <tr>
                            <th><strong>Size</strong></th>
                            <th colspan="2"><strong>
                                    To fit Waist Size </strong></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>Inches</th>
                            <th>CM</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><strong>26&quot;</strong></td>
                            <td>26</td>
                            <td>66</td>
                        </tr>
                        <tr>
                            <td><strong>28&quot;</strong></td>
                            <td>28</td>
                            <td>71</td>
                        </tr>
                        <tr>
                            <td>
                                <strong>29&quot;</strong></td>
                            <td>29</td>
                            <td>73.5</td>
                        </tr>
                        <tr>
                            <td><strong>30&quot;</strong></td>
                            <td>30</td>
                            <td>76</td>
                        </tr>
                        <tr>
                            <td><strong>31&quot;</strong></td>
                            <td>31</td>
                            <td>78.5</td>
                        </tr>
                        <tr>
                            <td><strong>32&quot;</strong></td>
                            <td>32</td>
                            <td>81</td>
                        </tr>
                        <tr>
                            <td><strong>33&quot;</strong></td>
                            <td>33</td>
                            <td>83.5</td>
                        </tr>
                        <tr>
                            <td><strong>34&quot;</strong></td>
                            <td>34</td>
                            <td>86</td>
                        </tr>
                        <tr>
                            <td><strong>36&quot;</strong></td>
                            <td>36</td>
                            <td>91</td>
                        </tr>
                        <tr>
                            <td><strong>38&quot;</strong></td>
                            <td>38</td>
                            <td>96</td>
                        </tr>
                        <tr>
                            <td>
                                <strong>40&#39;&#39;</strong></td>
                            <td>40</td>
                            <td>101</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="sizing_rgt">
                    <h5>Women's Trousers</h5>
                    <table width="100%" class="seven-column">
                        <thead>
                        <tr>
                            <th><strong>UK Size</strong></th>
                            <th colspan="2"><strong>Bust</strong></th>
                            <th colspan="2"><strong>Waist</strong></th>
                            <th colspan="2"><strong>Hips</strong></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>Inches</th>
                            <th>CM</th>
                            <th>Inches</th>
                            <th>CM</th>
                            <th>Inches</th>
                            <th>CM</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>2</td>
                            <td>29</td>
                            <td>73.5</td>
                            <td>22</td>
                            <td>55.5</td>
                            <td>31</td>
                            <td>80</td>
                        </tr>
                        <tr>
                            <td><strong>4</strong></td>
                            <td>30</td>
                            <td>76</td>
                            <td>23</td>
                            <td>58</td>
                            <td>32</td>
                            <td>82.5</td>
                        </tr>
                        <tr>
                            <td><strong>6</strong></td>
                            <td>31</td>
                            <td>78.5</td>
                            <td>24</td>
                            <td>60.5</td>
                            <td>33</td>
                            <td>85</td>
                        </tr>
                        <tr>
                            <td><strong>8</strong></td>
                            <td>32</td>
                            <td>81</td>
                            <td>25</td>
                            <td>63</td>
                            <td>34</td>
                            <td>87.5</td>
                        </tr>
                        <tr>
                            <td><strong>10</strong></td>
                            <td>34</td>
                            <td>86</td>
                            <td>27</td>
                            <td>68</td>
                            <td>36</td>
                            <td>92.5</td>
                        </tr>
                        <tr>
                            <td><strong>12</strong></td>
                            <td>36</td>
                            <td>91</td>
                            <td>29</td>
                            <td>73</td>
                            <td>38</td>
                            <td>97.5</td>
                        </tr>
                        <tr>
                            <td><strong>14</strong></td>
                            <td>38</td>
                            <td>96</td>
                            <td>31</td>
                            <td>78</td>
                            <td>40</td>
                            <td>102.5</td>
                        </tr>
                        <tr>
                            <td><strong>16</strong></td>
                            <td>40</td>
                            <td>101</td>
                            <td>33</td>
                            <td>83</td>
                            <td>42</td>
                            <td>107.5</td>
                        </tr>
                        <tr>
                            <td><strong>18</strong></td>
                            <td>43</td>
                            <td>109</td>
                            <td>36</td>
                            <td>91</td>
                            <td>46</td>
                            <td>116.5</td>
                        </tr>
                        <tr>
                            <td><strong>20</strong></td>
                            <td>46</td>
                            <td>116</td>
                            <td>39</td>
                            <td>98</td>
                            <td>49</td>
                            <td>123.5</td>
                        </tr>
                        <tr>
                            <td><strong>22</strong></td>
                            <td>48</td>
                            <td>123</td>
                            <td>41</td>
                            <td>105</td>
                            <td>51</td>
                            <td>130.5</td>
                        </tr>
                        <tr>
                            <td><strong>24</strong></td>
                            <td>51</td>
                            <td>130</td>
                            <td>44</td>
                            <td>112</td>
                            <td>54</td>
                            <td>137.5</td>
                        </tr>
                        <tr>
                            <td><strong>26</strong></td>
                            <td>54</td>
                            <td>137</td>
                            <td>47</td>
                            <td>119</td>
                            <td>57</td>
                            <td>144.5</td>
                        </tr>
                        <tr>
                            <td><strong>28</strong></td>
                            <td>57</td>
                            <td>144</td>
                            <td>50</td>
                            <td>126</td>
                            <td>60</td>
                            <td>151.5</td>
                        </tr>
                        <tr>
                            <td><strong>30</strong></td>
                            <td>59</td>
                            <td>151</td>
                            <td>52</td>
                            <td>133</td>
                            <td>62</td>
                            <td>158.5</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <br class="clearfix" />
            </div>

            <div class="section">
                <h4>FOOTWEAR</h4>
                <div class="sizing_lft">
                    <h5>Men's Shoes</h5>
                    <table class="four-column" width="100%">
                        <thead>
                        <tr>
                            <th><strong>UK Size</strong></th>
                            <th><strong>EU Size</strong></th>
                            <th><strong>US Size</strong></th>
                            <th><strong>Foot Length (mm)</strong></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                3
                            </td>
                            <td>35.5</td>
                            <td>4</td>
                            <td>220</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>37</td>
                            <td>5</td>
                            <td>
                                229
                            </td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>38</td>
                            <td>6</td>
                            <td>237</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>
                                39
                            </td>
                            <td>7</td>
                            <td>246</td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>40.5</td>
                            <td>8</td>
                            <td>254</td>
                        </tr>
                        <tr>
                            <td>
                                7.5
                            </td>
                            <td>41</td>
                            <td>8.5</td>
                            <td>258</td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>42</td>
                            <td>9</td>
                            <td>262</td>
                        </tr>
                        <tr>
                            <td>
                                8.5
                            </td>
                            <td>42.5</td>
                            <td>9.5</td>
                            <td>266</td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>43</td>
                            <td>10</td>
                            <td>271</td>
                        </tr>
                        <tr>
                            <td>
                                9.5
                            </td>
                            <td>44</td>
                            <td>10.5</td>
                            <td>275</td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>44.5</td>
                            <td>11</td>
                            <td>279</td>
                        </tr>
                        <tr>
                            <td>
                                10.5
                            </td>
                            <td>45</td>
                            <td>11.5</td>
                            <td>283</td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td>46</td>
                            <td>12</td>
                            <td>288</td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td>47</td>
                            <td>13</td>
                            <td>296</td>
                        </tr>
                        <tr>
                            <td>13</td>
                            <td>48</td>
                            <td>14</td>
                            <td>305</td>
                        </tr>
                        <tr>
                            <td>
                                14
                            </td>
                            <td>49.5</td>
                            <td>15</td>
                            <td>314</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="sizing_rgt">
                    <h5>Women's Shoes</h5>
                    <table class="eight-column">
                        <thead>

                        <tr>
                            <th>
                                <strong>UK Size</strong>
                            </th>
                            <th>
                                <strong>EU Size</strong>
                            </th>
                            <th>
                                <strong>US Size</strong>
                            </th>
                            <th>
                                <strong>Foot Length <br />(mm)</strong>
                            </th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                2
                            </td>
                            <td>
                                35
                            </td>
                            <td>
                                5
                            </td>
                            <td>
                                212
                            </td>

                        </tr>
                        <tr>
                            <td>
                                3
                            </td>
                            <td>
                                36
                            </td>
                            <td>
                                6
                            </td>
                            <td>
                                220
                            </td>

                        </tr>
                        <tr>
                            <td>
                                4
                            </td>
                            <td>
                                37
                            </td>
                            <td>
                                7
                            </td>
                            <td>
                                229
                            </td>

                        </tr>
                        <tr>
                            <td>
                                5
                            </td>
                            <td>
                                38
                            </td>
                            <td>
                                8
                            </td>
                            <td>
                                237
                            </td>

                        </tr>
                        <tr>
                            <td>
                                6
                            </td>
                            <td>
                                39
                            </td>
                            <td>
                                9
                            </td>
                            <td>
                                246
                            </td>

                        </tr>
                        <tr>
                            <td>
                                7
                            </td>
                            <td>
                                40
                            </td>
                            <td>
                                10
                            </td>
                            <td>
                                254
                            </td>

                        </tr>
                        <tr>
                            <td>
                                8
                            </td>
                            <td>
                                41
                            </td>
                            <td>
                                11
                            </td>
                            <td>
                                262
                            </td>

                        </tr>
                        <tr>
                            <td>
                                9
                            </td>
                            <td>
                                42
                            </td>
                            <td>
                                12
                            </td>
                            <td>
                                270
                            </td>

                        </tr>
                        </tbody>
                    </table>
                </div>

                <br class="clearfix" />
            </div>
        </div>
    </div>

</div>