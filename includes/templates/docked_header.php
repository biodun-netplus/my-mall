<style>

    #cat_div:hover
    {
        background-color: #ccc;
    }
    .cat_ul
    {
        padding-top: 10px;
    }
    .cat_ul li a
    {
        width: 100%;
        height: auto;
        display: block;
        color: #333;
        font-size: 15px;
        padding-left: 10px;
        line-height: 30px;
        text-decoration: none;
        text-align: left;

        border-bottom: 1px solid #CDE8C3;
    }
</style>


<div id="docked_header">
    <div class="container">
        <div class="docked_header_logo">
            <a href="index.php"><img src="<?php echo SITE_URL; ?>images/inside_logo.png"/></a>
        </div>

        <div class="inner_header_itm no-mobile">
            <div class="categories-list">
                <div class="dropdown_btn">Categories <span class="caret"></span></div>
                <div class="cat_dropdown_con">
                    <div class="cat_dropdown_arrow">
                        <div class="arrow"></div>
                    </div>

                    <!-- the new megmenu for category -->
                    <div class="cat_dropdown">
                        <ul class="cd-dropdown-content">
                            <?php
                            $QueryProduct = mysql_query("select * from category  where isactive = 't' AND pid=0 order by sort_order ASC");
                            $new = new users;
                            while ($Catt = mysql_fetch_object($QueryProduct)) {
                                ?>
                                <li class="sec-dropdown">
                                    <a href="<?php echo SITE_URL . $new->FormatN($Catt->category_name) . "/" . $Catt->id ?>">
                                        <?php echo ucwords(strtolower($Catt->category_name)); ?>
                                    </a>
                                    <div class="cd-secondary-dropdown">
                                        <div class="cd-sec-dd-links">
                                            <h5><?php echo ucwords(strtolower($Catt->category_name)); ?></h5>
                                            <ul>
                                                <li><a href="<?php echo SITE_URL . $new->FormatN($Catt->category_name) . "/" . $Catt->id ?>">All</a></li>
                                                <?php
                                                $category_id = $Catt->id;
                                                $q = mysql_query("select * from category where pid = '$category_id' and isactive='t' ");


                                                while($sub_category_fetch = mysql_fetch_object($q))
                                                {
//                                                    var_dump($sub_category_fetch);
                                                    $merc_array = 0;
                                                    // var_dump($sub_category_fetch);
                                                    $idd= $sub_category_fetch->id;
                                                    //echo $idd.'<br>';
                                                    $sub_prod = "SELECT * FROM products WHERE  sub_cat_id='$idd' AND isactive='t'";
                                                    //echo $sub_prod."->";
                                                    $sub_prod_query = mysql_query($sub_prod) or die(mysql_error());

                                                    // echo mysql_num_rows($sub_prod_query).'<br><br>';
                                                    if(mysql_num_rows($sub_prod_query) > 0 ) {
                                                        while($sub_prod_fetch = mysql_fetch_object($sub_prod_query))
                                                        {
                                                            $merc_id = $sub_prod_fetch->pid;


                                                            $merc_prod = "SELECT * FROM users WHERE id = '$merc_id' AND isactive='t'";
                                                            $merc_prod_query = mysql_query($merc_prod) or die(mysql_error());
                                                            if(mysql_num_rows($merc_prod_query) > 0 ) {
                                                                $merc_array++;
                                                            }
                                                        }

                                                        //echo '<br>merc array :'.$merc_array.'<br>';
                                                        if($merc_array>0){
                                                            ?>
                                                            <li>
                                                                <a href="<?php echo SITE_URL . $new->FormatN($sub_category_fetch->category_name) . "/" . $sub_category_fetch->id ?>"><?php echo ucwords(strtolower($sub_category_fetch->category_name)); ?></a>
                                                            </li>
                                                            <?php
                                                        }
                                                    }
                                                }

                                                ?>
                                            </ul>
                                        </div>
                                        <div class="cd-sec-dd-img">
                                            <img src="<?php echo SITE_URL ;?>images/menu_ext.jpg" />
                                        </div>
                                    </div>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="merchant-list">
                <div class="dropdown_btn">Merchants <span class="caret"></span></div>
                <div class="mer_dropdown_con">
                    <div class="mer_dropdown_arrow">
                        <div class="arrow"></div>
                    </div>
                    <div class="mer_dropdown">
                        <div class="mer_dropdown_btm">
                            <ul class="tabs" data-persist="true">
                                <li><a href="#atoe">A-E</a></li>
                                <li><a href="#ftoj">F-J</a></li>
                                <li><a href="#ktoo">K-O</a></li>
                                <li><a href="#ptot">P-T</a></li>
                                <li><a href="#utoz">U-Z</a></li>
                            </ul>

                            <div class="tabcontents">
                                <div class="tab-pane" id="atoe">
                                    <div class="tab-pane-ins">
                                        <ul>
                                            <?php
                                            $categoryArray = array();
                                            $i = 1;
                                            $SelectQueryAE = mysql_query("SELECT * FROM users WHERE first_name REGEXP  '^[A-E].*$' AND user_type =  'p' AND isactive =  't' order by first_name asc");
                                            $selectDataAEcount = mysql_num_rows($SelectQueryAE);

                                            while ($selectDataAE = mysql_fetch_object($SelectQueryAE)) {
                                                $selectProductCount = mysql_num_rows(mysql_query("SELECT * FROM  `products` WHERE  `pid` ='" . $selectDataAE->id . "'"));
                                                if ($selectProductCount > 0) {
                                                    $categoryArray[] = $selectDataAE;
                                                }
                                                $CattegoryCount = count($categoryArray);

                                            }
                                            $selectDataAE = "";
                                            $selectDataAEcount = round($CattegoryCount / 4);
                                            foreach ($categoryArray as $key => $selectDataAE){
                                            ?>
                                            <li>
                                                <a href="<?php echo SITE_URL . $selectDataAE->display_name;?>"><?php
                                                    echo substr($selectDataAE->first_name, 0, 20) ;?></a>
                                            </li>
                                            <?php
                                            if ($i == $selectDataAEcount){
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="tab-pane-ins">
                                        <ul><?php $i = 0;
                                            }
                                            $i++;
                                            } ?>
                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-pane" id="ftoj">
                                    <div class="tab-pane-ins">
                                        <ul>
                                            <?php
                                            $categoryArray = array();
                                            $i = 1;
                                            $SelectQueryAE = mysql_query("SELECT * FROM users WHERE first_name REGEXP  '^[F-J].*$' AND user_type =  'p' AND isactive =  't' order by first_name
 asc");
                                            $selectDataAEcount = mysql_num_rows($SelectQueryAE);

                                            while ($selectDataAE = mysql_fetch_object($SelectQueryAE)) {
                                                $selectProductCount = mysql_num_rows(mysql_query("SELECT * FROM  `products` WHERE  `pid` ='" . $selectDataAE->id . "'"));
                                                if ($selectProductCount > 0) {
                                                    $categoryArray[] = $selectDataAE;
                                                }
                                                $CattegoryCount = count($categoryArray);

                                            }
                                            $selectDataAE = "";
                                            $selectDataAEcount = round($CattegoryCount / 4);
                                            foreach ($categoryArray as $key => $selectDataAE){
                                            ?>
                                            <li>
                                                <a href="<?php echo SITE_URL . $selectDataAE->display_name;?>"><?php
                                                    echo substr($selectDataAE->first_name, 0, 20) ;?></a>
                                            </li>
                                            <?php
                                            if ($i == $selectDataAEcount){
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="tab-pane-ins">
                                        <ul><?php $i = 0;
                                            }
                                            $i++;
                                            } ?>
                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-pane" id="ktoo">
                                    <div class="tab-pane-ins">
                                        <ul>
                                            <?php
                                            $categoryArray = array();
                                            $i = 1;
                                            $SelectQueryAE = mysql_query("SELECT * FROM users WHERE first_name REGEXP  '^[K-O].*$' AND user_type =  'p' AND isactive =  't' order by first_name
 asc");
                                            $selectDataAEcount = mysql_num_rows($SelectQueryAE);

                                            while ($selectDataAE = mysql_fetch_object($SelectQueryAE)) {
                                                $selectProductCount = mysql_num_rows(mysql_query("SELECT * FROM  `products` WHERE  `pid` ='" . $selectDataAE->id . "'"));
                                                if ($selectProductCount > 0) {
                                                    $categoryArray[] = $selectDataAE;
                                                }
                                                $CattegoryCount = count($categoryArray);

                                            }
                                            $selectDataAE = "";
                                            $selectDataAEcount = round($CattegoryCount / 4);
                                            foreach ($categoryArray as $key => $selectDataAE){
                                            ?>
                                            <li>
                                                <a href="<?php echo SITE_URL . $selectDataAE->display_name;?>"><?php
                                                    echo substr($selectDataAE->first_name, 0, 20) ;?></a>
                                            </li>
                                            <?php
                                            if ($i == $selectDataAEcount){
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="tab-pane-ins">
                                        <ul><?php $i = 0;
                                            }
                                            $i++;
                                            } ?>
                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-pane" id="ptot">
                                    <div class="tab-pane-ins">
                                        <ul>
                                            <?php
                                            $categoryArray = array();
                                            $i = 1;
                                            $SelectQueryAE = mysql_query("SELECT * FROM users WHERE first_name REGEXP  '^[P-T].*$' AND user_type =  'p' AND isactive =  't' order by first_name
 asc");
                                            $selectDataAEcount = mysql_num_rows($SelectQueryAE);

                                            while ($selectDataAE = mysql_fetch_object($SelectQueryAE)) {
                                                $selectProductCount = mysql_num_rows(mysql_query("SELECT * FROM  `products` WHERE  `pid` ='" . $selectDataAE->id . "'"));
                                                if ($selectProductCount > 0) {
                                                    $categoryArray[] = $selectDataAE;
                                                }
                                                $CattegoryCount = count($categoryArray);

                                            }
                                            $selectDataAE = "";
                                            $selectDataAEcount = round($CattegoryCount / 4);
                                            foreach ($categoryArray as $key => $selectDataAE){
                                            ?>
                                            <li>
                                                <a href="<?php echo SITE_URL . $selectDataAE->display_name;?>"><?php
                                                    echo substr($selectDataAE->first_name, 0, 20) ;?></a>
                                            </li>
                                            <?php
                                            if ($i == $selectDataAEcount){
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="tab-pane-ins">
                                        <ul><?php $i = 0;
                                            }
                                            $i++;
                                            } ?>
                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-pane" id="utoz">
                                    <div class="tab-pane-ins">
                                        <ul>
                                            <?php
                                            $categoryArray = array();
                                            $i = 1;
                                            $SelectQueryAE = mysql_query("SELECT * FROM users WHERE first_name REGEXP  '^[U-Z].*$' AND user_type =  'p' AND isactive =  't' order by first_name
 asc");
                                            $selectDataAEcount = mysql_num_rows($SelectQueryAE);

                                            while ($selectDataAE = mysql_fetch_object($SelectQueryAE)) {
                                                $selectProductCount = mysql_num_rows(mysql_query("SELECT * FROM  `products` WHERE  `pid` ='" . $selectDataAE->id . "'"));
                                                if ($selectProductCount > 0) {
                                                    $categoryArray[] = $selectDataAE;
                                                }
                                                $CattegoryCount = count($categoryArray);

                                            }
                                            $selectDataAE = "";
                                            $selectDataAEcount = round($CattegoryCount / 4);
                                            foreach ($categoryArray as $key => $selectDataAE){
                                            ?>
                                            <li>
                                                <a href="<?php echo SITE_URL . $selectDataAE->display_name;?>"><?php
                                                    echo substr($selectDataAE->first_name, 0, 20) ;?></a>
                                            </li>
                                            <?php
                                            if ($i == $selectDataAEcount){
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="tab-pane-ins">
                                        <ul><?php $i = 0;
                                            }
                                            $i++;
                                            } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="mer_dropdown_top"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="inner_header_itm">
            <div id="docked_search_wrap">
                <div class="search">
                    <form id="search_form" action="<?php echo SITE_URL ?>search" method="post">
                        <input name="search" style="width: 80%" type="text" class="search-text"  value="<?php if
                        (isset($_REQUEST['search'])){ echo $_REQUEST['search'];}?>" placeholder="I am searching for... " />
                        <button type="submit" class="search_btn" onclick="javascript:document.getElementById('search_form').submit()"></button>
                    </form>
                </div>
            </div>
        </div>

        <div class="inner_header_itm2">
            <div class="sellonline_btn"><a href="<?php echo SITE_URL?>sell-online"><img src="images/sell-online-btn.gif"/></a></div>

            <div class="cart_wrap">
                <a class="no_style" href="<?php echo SITE_URL; ?>user_cart">
                    <span class="cart_txt add_to_cart no-mobile">&#x20A6; 0.00</span>
                    <span class="cart_icon"></span>
                </a>
            </div>
        </div>
    </div>
</div>


<?php
include('includes/templates/mobile_header.php');
?>