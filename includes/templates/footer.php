<footer>
    <div class="top_band">
        <div class="container">
            <div class="band_lft">
                <p><a href="<?php echo SITE_URL ?>sell-online">Open A store</a></p>
            </div>

            <div class="band_mid">
                <span class="band_mid_txt">
                    <a href="<?php echo SITE_URL ?>campaigns">Marketing</a>
                </span>

                <span class="footerdot"></span>

                <span class="band_mid_txt">
                    <a href="<?php echo SITE_URL ?>sell-online#two">Payment</a>
                </span>

                <span class="footerdot"></span>

                <span class="band_mid_txt">
                    <a href="<?php echo SITE_URL ?>sell-online#three">Logistics</a>
                </span>

                <span class="footerdot"></span>

                <span class="band_mid_txt">
                    <a href="<?php echo SITE_URL ?>sell-online#four">Webstore</a>
                </span>
            </div>

            <div class="band_rgt">
                <p class="float_lft"><img src="<?php echo SITE_URL ?>images/phone.png"/>0909.918.0002, 0818.778.2542</p>

                <!--<p class="float_rgt"><a href="<?php echo SITE_URL ?>sell-online">Sell Online</a></p>-->
            </div>
        </div>
    </div>

    <div class="footer_content">
        <div class="container" style="padding: 10px 0 30px;">
            <h4>THE WEBMALL STORY</h4>
            <div class="footer_txt_content">
            <strong>WebMall</strong>, which launched in 2013, has become one of Nigeria's leading e-commerce platforms in an impressive amount of 
            time. The WebMall site is designed to create the most appealing user experience, for both the customer and the 
            merchant. We provide online and offline tools to promote the businesses of our merchants as well as affordable 
            merchant plans. Our customers have access to an extensive range of high quality products and an excellent logistics 
            service. We offer a wide range of products such as laptops, mobile phones, toys, speakers, sneakers, hair products, 
            chargers, food items, wrist watches, printers and so on. <br /><br />
            
            <h4>Online shopping on WebMall</h4>
            
            <strong>WebMall</strong> customers can shop products from a number of categories - computer & accessories; electronics; 
            home & office 
            appliances; baby, kids & toys; phones & tablets; fashion; food & drink; gifts,books & stationery; pet foods & products; 
            POS & accounting; sports & fitness; beauty, health & personal; movies, games & music and automative & industrial. 
            With over a thousand products available, customers are not only assured of quality, but variety too. 
            Get products delivered directly to your doorstep, anywhere in Nigeria. Enjoy easy and convenient payment by choosing to 
            either pay on delivery or through bank transfer. Merchants are also given the option to pay in instalments.  
            Everything at <strong>WebMall</strong> is designed around ensuring the customer experience is one of little to no 
            hiccups along the way. <br /><br />
          
            Take advantage of the amazing deals we offer regularly, but in particular, be on the lookout for our cyber week deals. 
            For a limited time only - the last week of November - WebMall is offering crazy deals on Sony Bravia LED TV, sound bar 
            with wireless subwoofer, Microsoft Office 365 personal, ruffle open back dress, women lingerie, Samsung LED curved TV, 
            men’s T-Shirts, doormats and so on. <br /><br />
        
            We stock a number of products that are sure to appeal to a wide variety of customers, including yam pounder, fit bit 
            fitness bands, baby clothes, multifunctional lunch box, infinix hot 2 X510, Crea T-shirts, QI wireless pad, pancake 
            pan, selfie sticks, hover board and more. We also boast an impressive collection of international brands such as 
            Apple, ASOS, Kenwood, HP, Samsung, Microsoft, IKEA, Blackberry and so much more. 
            </div>
            
        </div>

        <div class="container" style="padding: 10px 0 30px;">
            <h4>Popular products on WebMall?</h4>
            <div class="footer_list_content">
                DVD Player - Kenwood smoothie blender - Canon EOS 700D camera - HP Laser Jet Pro printer - 
                iHome wireless portable recharge speaker - IKEA laundry hamper - Kinelco dry iron - 
                automatic toothpick dispenser - multifunction juicer - twin cereal dispenser - 
                high waist thong shaper - rechargeable lamp - ASOS divide plimsolls - 
                Kenwood microwave oven - Sabichi easy grip pan - Sabichi spaghetti server - 
                Kenwood sandwich maker - Kenwood traditional kettle - Blackberry passport - 
                Blackberry Q5 - Blackberry classic - Kenwood blender - bow tie - brooch - 
                shoe organiser - chopping board
            </div>
        </div>

        <div class="container">
            <div class="footer_lft">
                <div class="footer_social">
                    <a href="https://www.facebook.com/webmallng">
                        <div class="footer_social_itm" id="fb_icon"></div>
                    </a>
                    <a href="https://www.twitter.com/webmallng">
                        <div class="footer_social_itm" id="tw_icon"></div>
                    </a>
                    <a href="https://instagram.com/webmallng">
                        <div class="footer_social_itm" id="inst_icon"></div>
                    </a>
                    <a href="https://www.linkedin.com/company/netplus-advisory">
                        <div class="footer_social_itm" id="link_icon"></div>
                    </a>
                    <a href="https://www.youtube.com/channel/UCTlnju21QsfbKo6_Q0jVHLQ ">
                        <div class="footer_social_itm" id="you_icon"></div>
                    </a>
                </div>

                <div class="footer_newsletter">
                    <h4 class="white light">Newsletter</h4>
                    <div class="nl_notice">
                        <p class="white">
                            Subscribe to get the latest updates on special offers, promos an deals.
                        </p>
                    </div>
                    <form action="#" method="post">
                        <div class="form_itm">
                            <input type="text" name="newsletter_email" id="newsletter_email" class="footer_textfield"
                                   placeholder="Enter Email"/>
                            <input type="button" name="submit" value="Submit" id="nl_submit" class="footer_btn"/>
                        </div>
                    </form>

                </div>
            </div>

            <div class="footer_rgt">
                <div class="footer_rgt_itm white_border">
                    <div class="footer_rgt_itm_title">Useful Links:</div>
                    <div class="footer_rgt_itm_cont">
                        <ul>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms of Use</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Merchant Manual</a></li>
                            <li><a href="/webble">Webble</a></li>
                        </ul>
                    </div>
                </div>

                <div class="footer_rgt_itm white_border">
                    <div class="footer_rgt_itm_title">Payment Partners:</div>
                    <div class="footer_rgt_itm_cont">
                        <img src="<?php echo SITE_URL ?>images/mastercard2.png"/>
                        <img src="<?php echo SITE_URL ?>images/visa2.png"/>
                        <img src="<?php echo SITE_URL ?>images/verve-black-logo.png"/>
                        <img src="<?php echo SITE_URL ?>images/stanbic2.png"/>
                        <img src="<?php echo SITE_URL ?>images/netpluspay2.png"/>
                    </div>
                </div>

                <div class="footer_rgt_itm">
                    <div class="footer_rgt_itm_title">Logistics Partners:</div>
                    <div class="footer_rgt_itm_cont">
                        <img src="<?php echo SITE_URL ?>images/fedex2.png"/>
                        <img src="<?php echo SITE_URL ?>images/timex2.png"/>
                    </div>
                </div>
            </div>

            <br class="clearfix"/>
        </div>
    </div>
</footer>

<div id="copyright">
    <div class="container">
        <table style="margin:auto">
            <tbody>
            <tr>
                <td>
                    <img style="float:right;" src="<?php echo SITE_URL ?>images/footer-logo.png" height="15"/>
                </td>
                <td style="font-size:14px; padding-left:10px; float:left; color: #fff">
                    <?php echo date('Y'); ?>, Webmall NG. All rights reserved
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="popup_overlay">
<div id="popup">
<?php include('includes/templates/size_guide.php') ?>
</div>

<div id="group_buy_popup">
    <?php include('includes/templates/new_groupbuy.php') ?>
</div>
</div>

<!--<div id="what_group_buy_popup">
    <?php /*include('includes/templates/what_is_groupbuy.php') */?>
</div>
-->
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var $_Tawk_API = {}, $_Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/55a38a1684d307454c001f9f/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();

</script>

<!--End of Tawk.to Script-->

<script type="text/javascript">
    $(document).ready(function() {
        $('#hide_show_btn').click(function(){
            $('#mobile_header').slideToggle(500);
        });

        $('#disp_mobile_cat').click(function(){
            $('.mobile_cat').slideToggle(500);
        });
    })
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.size_guide_link').click(function(){
            $('.popup_overlay').css('display', 'block');
            $('#popup').css('display', 'block');
        });

        $('.close_pop_up').click(function(){
            $('.popup_overlay').css('display', 'none');
            $('#popup').css('display', 'none');
        });

        $('.groupbuy_btn').click(function(){
            $('.popup_overlay').css('display', 'block');
            $('#group_buy_popup').css('display', 'block');
        });

        $('.group_buy_close_pop_up').click(function(){
            $('.popup_overlay').css('display', 'none');
            $('#group_buy_popup').css('display', 'none');
        });

        $('.group_buy_link').click(function(){
            $('.popup_overlay').css('display', 'block');
            $('#what_group_buy_popup').css('display', 'block');
        });

        $('.what_grp_buy_close_pop_up').click(function(){
            $('.popup_overlay').css('display', 'none');
            $('#what_group_buy_popup').css('display', 'none');
        });
    })


</script>