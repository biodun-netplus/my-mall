<footer>
    <div class="top_band">
        <div class="container">
            <div class="band_lft">
                <p><a href="<?php echo SITE_URL ?>sell-online">Open A store</a></p>
            </div>

            <div class="band_mid">
                <span class="band_mid_txt">
                    <a href="<?php echo SITE_URL ?>campaigns">Marketing</a>
                </span>

                <span class="footerdot"></span>

                <span class="band_mid_txt">
                    <a href="<?php echo SITE_URL ?>sell-online#two">Payment</a>
                </span>

                <span class="footerdot"></span>

                <span class="band_mid_txt">
                    <a href="<?php echo SITE_URL ?>sell-online#three">Logistics</a>
                </span>

                <span class="footerdot"></span>

                <span class="band_mid_txt">
                    <a href="<?php echo SITE_URL ?>sell-online#four">Webstore</a>
                </span>
            </div>

            <div class="band_rgt">
                <p class="float_lft"><img src="images/phone.png"/>0909.918.0002, 0818.778.2542</p>

                <!--<p class="float_rgt"><a href="<?php echo SITE_URL ?>sell-online">Sell Online</a></p>-->
            </div>
        </div>
    </div>

    <div class="footer_content">

        <div class="container">
            <div class="footer_lft">
                <div class="footer_social">
                    <a href="https://www.facebook.com/webmallng">
                        <div class="footer_social_itm" id="fb_icon"></div>
                    </a>
                    <a href="https://www.twitter.com/webmallng">
                        <div class="footer_social_itm" id="tw_icon"></div>
                    </a>
                    <a href="https://instagram.com/webmallng">
                        <div class="footer_social_itm" id="inst_icon"></div>
                    </a>
                    <a href="https://www.linkedin.com/company/netplus-advisory">
                        <div class="footer_social_itm" id="link_icon"></div>
                    </a>
                    <a href="https://www.youtube.com/channel/UCTlnju21QsfbKo6_Q0jVHLQ ">
                        <div class="footer_social_itm" id="you_icon"></div>
                    </a>
                </div>

                <div class="footer_newsletter">
                    <h4 class="white light">Newsletter</h4>
                    <div class="nl_notice">
                        <p class="white">
                            Subscribe to get the latest updates on special offers, promos an deals.
                        </p>
                    </div>
                    <form action="#" method="post">
                        <div class="form_itm">
                            <input type="text" name="newsletter_email" id="newsletter_email" class="footer_textfield"
                                   placeholder="Enter Email"/>
                            <input type="button" name="submit" value="Submit" id="nl_submit" class="footer_btn"/>
                        </div>
                    </form>

                </div>
            </div>

            <div class="footer_rgt">
                <div class="footer_rgt_itm white_border">
                    <div class="footer_rgt_itm_title">Useful Links:</div>
                    <div class="footer_rgt_itm_cont">
                        <ul>
                            <li><a href="#">Contact Us</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Terms of Use</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">Merchant Manual</a></li>
                            <li><a href="/webble">Webble</a></li>
                        </ul>
                    </div>
                </div>

                <div class="footer_rgt_itm white_border">
                    <div class="footer_rgt_itm_title">Payment Partners:</div>
                    <div class="footer_rgt_itm_cont">
                        <img src="<?php echo SITE_URL ?>images/mastercard2.png"/>
                        <img src="<?php echo SITE_URL ?>images/visa2.png"/>
                        <img src="<?php echo SITE_URL ?>images/verve-black-logo.png"/>
                        <img src="<?php echo SITE_URL ?>images/stanbic2.png"/>
                        <img src="<?php echo SITE_URL ?>images/netpluspay2.png"/>
                    </div>
                </div>

                <div class="footer_rgt_itm">
                    <div class="footer_rgt_itm_title">Logistics Partners:</div>
                    <div class="footer_rgt_itm_cont">
                        <img src="<?php echo SITE_URL ?>images/fedex2.png"/>
                        <img src="<?php echo SITE_URL ?>images/timex2.png"/>
                    </div>
                </div>
            </div>

            <br class="clearfix"/>
        </div>
    </div>
</footer>

<div id="copyright">
    <div class="container">
        <table style="margin:auto">
            <tbody>
            <tr>
                <td>
                    <img style="float:right;" src="images/footer-logo.png" height="15"/>
                </td>
                <td style="font-size:14px; padding-left:10px; float:left; color: #fff">
                    <?php echo date('Y'); ?>, Webmall NG. All rights reserved
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>


<script type="text/javascript" src="<?php echo SITE_URL; ?>js/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="<?php echo SITE_URL; ?>js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="<?php echo SITE_URL; ?>js/tabcontent.js"></script>
    <script type="text/javascript" src="<?php echo SITE_URL; ?>js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?php echo SITE_URL; ?>js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="<?php echo SITE_URL; ?>js/jquery.nivo.slider.js"></script>
    <script type="text/javascript" src="<?php echo SITE_URL; ?>js/jquery.form-validator.min.js"></script>
    <script type="text/javascript" src="<?php echo SITE_URL; ?>js/jquery.fancybox.js"></script>
    <script type="text/javascript" src="<?php echo SITE_URL; ?>js/slick.min.js"></script>



    <script type="text/javascript">
        $('.owl-carousel').owlCarousel({
            margin:10,
            loop:true,
            autoWidth:true,
            autoplay:true,
            items:3,
            autoplayTimeout:500,
            responsiveClass:true,
            responsive:{
                0:{
                    items:2,
                    nav:true
                },
                768:{
                    items:3,
                    nav:false
                },
                1200:{
                    items:3,
                    nav:true,
                    loop:false
                }
            }
        });

        $('.owl-carousel2').owlCarousel({
            margin:10,
            loop:true,
            autoWidth:true,
            autoplay:true,
            items:4,
            autoplayTimeout:500,
            responsiveClass:true,
            responsive:{
                0:{
                    items:2,
                    nav:true
                },
                768:{
                    items:3,
                    nav:false
                },
                1200:{
                    items:3,
                    nav:true,
                    loop:false
                }
            }
        });
    </script>


    <script type="text/javascript">
        $('.carousel').slick({
            infinite: true,
            slidesToScroll:1,
            cssEase: 'linear',
            speed: 500,
            slidesToShow: 3,
            autoplay: true,
            dots: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        arrows: false,
                        centerMode: true,
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        centerMode: false,
                        slidesToShow: 1
                    }
                }
            ]
        });
    </script>   

    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox();
        });
    </script> 

    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider({
            pauseTime: 5500,
            animSpeed: 500,
            pauseOnHover: false
        });
    });
    </script>

    <script type="text/javascript">
    $(document).ready(function() {
        $('.nav_trigger').click(function(){
            $('.nav').slideToggle(500);
        });
    })
    </script>

    <script>
      $.validate({
        modules : 'html5',
        validateOnBlur : true, // disable validation when input looses focus
        scrollToTopOnError : false // Set this property to true if you have a long form
      });
    </script>

    <script type="text/javascript" src="http://arrow.scrolltotop.com/arrow13.js"></script>


</body>
</html>