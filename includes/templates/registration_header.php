<div id="fixed_header">

    <div class="container">
        <div class="socialmedia">
            <div class="socialmedia_icon">
                <a href="https://www.youtube.com/webmallng">
                    <img src="<?php echo SITE_URL; ?>images/youtube.png"/>
                </a>
            </div>
            <div class="socialmedia_icon">
                <a href="https://plus.google.com/u/0/106291413256291053539/posts">
                    <img src="<?php echo SITE_URL; ?>images/google.png"/>
                </a>
            </div>

            <div class="socialmedia_icon">
                <a href="https://www.twitter.com/webmallng">
                    <img src="<?php echo SITE_URL; ?>images/twitter.png"/>
                </a>
            </div>
            <div class="socialmedia_icon">
                <a href="https://www.facebook.com/webmallng">
                    <img src="<?php echo SITE_URL; ?>images/facebook.png"/>
                </a>
            </div>
            <div class="socialmedia_icon">
                <a href="https://www.instagram.com/webmallng" title="Follow us on instagram">
                    <img src="<?php echo SITE_URL; ?>images/instagram.jpg"/>
                </a>
            </div>
            <div class="socialmedia_icon">
                <a href="#" title="568A29B7">
                    <img src="<?php echo SITE_URL; ?>images/bbm.jpg"/>
                </a>
            </div>
            <div class="socialmedia_icon">
                <a href="#" title="08187782542">
                    <img src="<?php echo SITE_URL; ?>images/whatsapp.jpg"/>
                </a>
            </div>
        </div>


        <div class="fixed_header_lft">
            <ul>
                <li>
                    <a href=<?php echo SITE_URL ?>sell-online title="Register on WebMall">Open a store</a>
                </li>

                <?php if(!isset($_SESSION['provider_id'])){?>
                    <li class="login-dropdown">
                        <a href="#" title="Login as Merchant">Merchant Login<span class="caret2"></span></a>

                        <div id="login-dropdown-con">
                            <div class="dropdown-arrow"></div>
                            <div class="dropdown-box">
                                <div class="login-form">
                                    <h3>Merchant Login</h3>

                                    <div class="form_alert" id="form_msg"></div>

                                    <form name="loginform" method="post" id="loginform">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="EMAIL"
                                                   name="email" id="email">
                                        </div>

                                        <div class="form-group">
                                            <input type="password" class="form-control" name="password"
                                                   placeholder="PASSWORD" id="password">
                                        </div>

                                        <div class="form-group">
                                            <a href="<?php echo SITE_URL ?>forgotpassword.php" class="" style="color:#333;
                                        ">FORGOT PASSWORD?</a>
                                        </div>
                                        <br />

                                        <div>
                                            <button type="submit" id="login" name="login"
                                                    class="login_btn">LOGIN
                                            </button>
                                        </div>

                                        <br class="clearfix"/>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php } else { ?>
                    <li>
                        <a href="<?php echo SITE_URL?>my-profile">Profile</a>
                    </li>
                <?php } ?>
            </ul>
        </div>


        <div class="fixed_header_rgt">
            <span class="fixed_header_txt">
                <a href="#">0809 999 9960</a>
            </span>
        </div>


        <div class="fixed_header_mid" id="hidden_div">
            <span class="fixed_header_txt">
                <a href="<?php echo SITE_URL ?>campaigns">Marketing</a>
            </span>

            <span class="dot"></span>

            <span class="fixed_header_txt">
                <a href="<?php echo SITE_URL ?>sell-online#two">Payment</a>
            </span>

            <span class="dot"></span>

            <span class="fixed_header_txt">
                <a href="<?php echo SITE_URL ?>sell-online#three">Logistics</a>
            </span>

            <span class="dot"></span>

            <span class="fixed_header_txt">
                <a href="<?php echo SITE_URL ?>sell-online#four">Webstore</a>
            </span>
        </div>

        <br class="clearfix" />
    </div>
</div>


<div id="registration_header">
    <div class="container">

        <div class="logo_wrap">
            <div class="logo">
                <a href="<?php echo SITE_URL; ?>"><img src="<?php echo SITE_URL; ?>images/logo.png"/></a>
            </div>
        </div>

    </div>
</div>