$(document).ready(function () {
  window.siteUrl = 'http:///43813554.ngrok.io/testmall/';
  //  window.siteUrl = "https://www.mymall.com.ng/";
	/*if (location.href.indexOf("https://") == -1)
	{
		window.siteUrl = "http://www.mymall.com.ng/"; 
	}
	else
	{
//		window.siteUrl = "https://www.mymall.com.ng/";
	}*/

	/*===Owl Carousel for Home merchants ==============================*/
	$(".owl-carousel2").each(function() {
  		var $this = $(this);
  		$this.owlCarousel({
    		itemsCustom : [
				[0, 1],
                [320, 1],
                [480, 2],
				[600, 2],
				[768, 3],
				[900, 5],
				[1000, 5],
				[1200, 5]
			],
			pagination : false
		});
  		// Custom Navigation Events
  		$this.parent().find(".next").click(function(){
    		$this.trigger('owl.next');
  		});
  		$this.parent().find(".prev").click(function(){
    		$this.trigger('owl.prev');
  		});
	});
	
	/*===Owl Carousel for Home product ===================================*/
	$(".owl-carousel1").each(function() {
  		var $this = $(this);
  		$this.owlCarousel({
    		itemsCustom : [
				[0, 1],
                [320, 1],
                [480, 2],
				[600, 2],
				[768, 3],
				[900, 4],
				[1000, 4],
				[1200, 4]
			],
			pagination : false
		});
  		// Custom Navigation Events
  		$this.parent().find(".next").click(function(){
    		$this.trigger('owl.next');
  		});
  		$this.parent().find(".prev").click(function(){
    		$this.trigger('owl.prev');
  		});
	});
	
	/*===Owl Carousel for product detail page ===========================================*/
	$(".owl-carousel-related-product").each(function() {
  		var $this = $(this);
  		$this.owlCarousel({
    		itemsCustom : [
				[0, 1],
                [320, 1],
				[480, 2],
				[600, 2],
				[768, 3],
				[900, 4],
				[1000, 4],
				[1200, 4]
			],
			pagination : false
		});
  		// Custom Navigation Events
  		$this.parent().find(".next").click(function(){
    		$this.trigger('owl.next');
  		});
  		$this.parent().find(".prev").click(function(){
    		$this.trigger('owl.prev');
  		});
	});
	
	$( "#product-tab" ).tabs();
	
	/*$("#productColor").change(function() {
		var color = $("#productColor").val();
		if(color != '')
		$("#productColor").css('background-color',color);
		if(color == '')
		$("#productColor").css('background-color','none');

	});*/
	
	$("#productColor").change(function() {
		var colour = $("#productColor").val();
		if( $('#productSize').length > 0) {
    		$.ajax({
			type: 'post',
			dataType: 'json',
			url: window.siteUrl+"product/ajax_colour/"+ $("#productId").val()+"/"+colour,
			success: function(data)
			{
				 $('#productSize').find('option').remove();
				 $('#productSize').append(data.size);
				  $('#productQty').find('option').remove();
				 $('#productQty').append(data.qty);
			}
		    });
		}
		else
		{
			var colour 	= $("#productColor").val();
			var size 	= $("#productSize").val();
			$.ajax({
			type: 'post',
			//dataType: 'json',
			url: window.siteUrl+"product/ajax_qty/"+ $("#productId").val()+"/"+colour+"/"+size,
			success: function(data)
			{
				 $('#productQty').find('option').remove();
			 	 $('#productQty').append(data);
			}
		  });
			
		}
		
	});
	
	$("#productSize").change(function() {
		var colour = $("#productColor").val();
		
		var size = $("#productSize").val();
    	$.ajax({
		type: 'post',
        //dataType: 'json',
		url: window.siteUrl+"product/ajax_qty/"+ $("#productId").val()+"/"+colour+"/"+size,
		success: function(data)
		{
			 $('#productQty').find('option').remove();
			 $('#productQty').append(data);
		}
	  });
	});
	/*$("#productSize").change(function() {
		var size = $("#productSize").val();
    	$.ajax({
		type: 'post',
        //dataType: 'json',
		url: window.siteUrl+"product/ajax_size/"+ $("#productId").val()+"/"+size,
		success: function(data)
		{
			$('#productColor').find('option').remove();
			$('#productColor').append(data);
			 $('#productQty').find('option').remove();
			 $('#productQty').append(data);
		}
		});

	});*/
	
	$(document).on("click", ".sidebar_more", function() {
		var getClass= $(this).attr("myval"); 
		$( "."+getClass ).show();
		$(this).hide();
	});


	/*=== change sub category for serach page ==================================*/
	$(".filter-type").click(function() {
		var type = $(this).val();
		 $.ajax({
            url: window.siteUrl+"search/ajax_sidebar/"+ type,
            type: 'post',
            dataType: 'json',
            success: function (data) {
				if($.trim(data.subCat) == ''){
					$('#change-subCat').parent("div").hide();
				}
				else{
					$('#change-subCat').html(data.subCat);
					$('#change-subCat').parent("div").show();
				}
				if($.trim(data.colour) == ''){
					$('#change-colour').parent("div").hide();
				}
				else{
					$('#change-colour').html(data.colour);
					$('#change-colour').parent("div").show();
				}
				if($.trim(data.brand) == ''){
					$('#change-brand').parent("div").hide();
				}
				else{
					$('#change-brand').html(data.brand);
					$('#change-brand').parent("div").show();
				}
            },
        });
	});


	/*=== change sub category for serach page ==================================*/
	$(document).on("click", ".filter-type", function(){
		 
		 $('.product_area_wrap').html('<div style="width: 100%; text-align: center; margin-top: 10%;"><img src="'+window.siteUrl+'assets/img/loader-ajax-1.GIF"></div>');
		 $.ajax({
            url: window.siteUrl+"search/ajax_search/",
            type: 'POST',
			dataType: 'json',
			data: $('#search-form').serialize(),
            success: function (data) {
				$('.product_area_wrap').html(data.searchResult);
				$('.result').html(data.searchCount +' Search Results');
				
             },
          });
	 });
	/*===Owl Carousel for Home header =======================================*/
  
$(".owl-carousel-header").each(function() {
  		var $this = $(this);
  		$this.owlCarousel({
    		navigation : false, // Show next and prev buttons
      		slideSpeed : 800,
      		//paginationSpeed : 400,
      		singleItem:true,
      		pagination : false,
      		autoPlay : 10000,
			
		});
  		// Custom Navigation Events
  		$this.parent().find(".next").click(function(){
    		$this.trigger('owl.next');
  		});
  		$this.parent().find(".prev").click(function(){
    		$this.trigger('owl.prev');
  		});

		$(".carousel-header").hover(function() {
		     $("#home-prev").show();
		     $("#home-next").show();
		 }, function() {
		     $("#home-prev").hide();
		     $("#home-next").hide();
		 });
		 $("#home-prev").hide();
		 $("#home-next").hide(); 

	});



})
