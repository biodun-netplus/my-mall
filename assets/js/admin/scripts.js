$(document).ready(function () {
	window.siteUrl = "http://www.webmallnglab.com/ecomall/admin/"; 
	
	/*=== registration page ================================================*/
	$("input:radio[name=bankaccounselect]").click(function() {
    	if($(this).val() == 'Yes'){
			$('#BankShow').show();
			$('#txtAccountType').val('');
		}
		else{
			$('#BankShow').hide();
			$('#txtAccountType').val('NO');
		}
	});	

	/*=== add states in registration page ================================================*/
	$("#txtCountry").change(function() {
    	$.ajax({
		type: "POST",
		url: window.siteUrl+"registration/get_state/"+ $('#txtCountry').val(),
		data: {id:$('#txtCountry').val()},
		success: function(data)
		{
			$('#txtState').append(data)
			
		}
		});
	});	

})
