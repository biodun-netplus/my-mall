<?php
include('includes/header.php');
var_dump($_SESSION);
?>

<?php

$transactions = mysql_query("SELECT * FROM order_details WHERE merchant_id = '{$provider_id}'
AND order_status = 'Approved' ORDER BY order_placed DESC");
$transaction_count = mysql_num_rows($transactions);

$all_sales = 0;
while($sales_data = mysql_fetch_object($transactions)) {
    $sales = $sales_data->product_price;
    $all_sales += $sales;
}
$total_sales = $all_sales;

//get values for product
$product = mysql_query("SELECT * FROM products WHERE pid = '".$provider_id."'");
$product_count = mysql_num_rows($product);

//get the messages
$messages = mysql_query("SELECT * FROM message WHERE pid = '{$provider_id}'");
$message_count = mysql_num_rows($messages);

?>

<div id="content">

    <?php include('includes/sidebar.php') ?>

    <div id="container">

        <h2 class="caps">Dashboard</h2>

        <div class="main">
            <div id="total_sales" class="dashboard_itm">
                <div class="di_ins_rgt">
                    <a href="merchant/orders.php">Total Sales</a>
                    <h3 class="white"><a href="merchant/orders.php">₦<?php echo number_format($total_sales) ?></a></h3>
                </div>
            </div>

            <div id="total_orders" class="dashboard_itm">
                <div class="di_ins_rgt">
                    <a href="merchant/orders.php">Total Orders</a>
                    <h3 class="white"><a href="merchant/orders.php"><?php echo $transaction_count ?></a></h3>
                </div>
            </div>

            <div id="product_count" class="dashboard_itm">
                <div class="di_ins_rgt">
                    <a href="merchant/products.php">Product Count</a>
                    <h3 class="white"><a href="merchant/products.php"><?php echo $product_count ?></a></h3>
                </div>
            </div>

            <div id="pending_payment" class="dashboard_itm">
                <div class="di_ins_rgt">
                    <a href="merchant/messages.php">Messages</a>
                    <h3 class="white"><a href="merchant/messages.php"><?php echo $message_count ?></a></h3>
                </div>
            </div>

            <br class="clearfix" />
        </div>

        <div class="main">
            <div class="box1" id="graph">
                <div class="box_title">Sales Analysis</div>
                <div class="box_ins" style="width:95%">
                    <canvas id="display_graph" height="400" width="600"></canvas>
                </div>
            </div>

            <div class="box1" id="recent_orders">
                <div class="box_title">Last 10 Orders</div>
                <div class="box_ins">
                    <table width="100%" border="1">
                        <tr>
                            <th width="25%" height="30">Customer Name</th>
                            <th width="40%">Product  Name</th>
                            <th width="15%">Price</th>
                            <th width="20%">Date</th>
                        </tr>

                        <?php
                        $transact = mysql_query("SELECT * FROM order_details WHERE merchant_id = '{$provider_id}'
AND order_status = 'Approved' ORDER BY order_placed DESC LIMIT 10");
                        while ($TransData = mysql_fetch_object($transact)) {
                            $customer_id = $TransData->customer_id;
                            $cust = mysql_query("SELECT * FROM customer_addresses WHERE customer_id =
                            '{$customer_id}'");
                            $customer_data = mysql_fetch_object($cust);

                            $product_id = $TransData->product_id;
                            $prod = mysql_query("SELECT * FROM products WHERE id ='{$product_id}'");
                            $product_data = mysql_fetch_object($prod);

                            $product_price = $TransData->product_price;
                        ?>
                        <tr>
                            <td><?php echo $customer_data->name ?></td>
                            <td><?php echo $product_data->product_name ?></td>
                            <td>₦<?php echo number_format($TransData->product_price) ?></td>
                            <td>
                                <?php
                                $date = date('M j, Y', strtotime($TransData->order_placed));
                                echo $date
                                ?>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <br class="clearfix" />

</div>

<?php
include('includes/footer.php')
?>

<script type="text/javascript">
    //var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
    var lineChartData = {
        labels : ["January","February","March","April","May","June","July"],
        datasets : [
            {
                label: "My First dataset",
                fillColor : "rgba(20,20,20,0.2)",
                strokeColor : "rgba(20,20,20,1)",
                pointColor : "rgba(20,20,20,1)",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data: [0, 0, 0, 0, 0, 0, 0]
            }
        ]

    }

    window.onload = function(){
        var ctx = document.getElementById("display_graph").getContext("2d");
        window.myLine = new Chart(ctx).Line(lineChartData, {
            responsive: true,
            scaleBeginAtZero : true,
            scaleShowGridLines : true,
            scaleGridLineColor : "rgba(0,0,0,.05)",
            scaleGridLineWidth : 1,
            scaleShowHorizontalLines: true,
            scaleShowVerticalLines: true

        });
    }

</script>

</body>
</html>