<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->load->model('cart_model');
		$this->load->model('user_model');
		$this->load->model('order_model');
		$this->load->model('product_model');

		
	}
	public function success(){
		
		unset($_SESSION['lastInsertedOrderId']);
		unset($_SESSION['cart']);
		unset($_SESSION['Check']);


		if(isset($_SESSION['orderMessage']) && $_SESSION['orderMessage'] != '')
		{
			$this->layout->view('sucess_interswitch');
		}
		else
		{
			$this->layout->view('sucess_netpluspay');
		}
			
		
		/*if($this->input->post('paymentType') && $this->input->post('paymentType'))
		{
			$this->success_pay_on_delivery();
		}
		else if($this->input->post('order_request') && $this->input->post('order_request'))
		{
			
			
		}
		else 
		{
			$this->fail_pay_on_delivery();
		}*/
		

	}
	public function failed(){
	
		$this->layout->view('sucess_interswitch');
		
		
	}

	public function pending(){
		
		$this->layout->view('sucess_interswitch');
		
	}

	public function canceled(){
			
		$this->layout->view('sucess_interswitch');
			
	}




	public function netpluspay_notify(){
		
		$post_data = "SUCCESS";

		echo $post_data;
		
		

	}
	public function netpluspay_success(){
		
		
		$this->courier();
		unset($_SESSION['lastInsertedOrderId']);
		$orderId 				= $_SESSION['Check'];
		$productdetail        	= "";
		$shippingDetail			= "";
		$orderDetail    = $this->order_model->single_order_detail(array('order_id' => $orderId));
		$productDetail	= $this->cart_model->get_all_items_cart('', $orderId);
		if($orderDetail && $orderDetail->shipping_add_id != '')
		{
			$shippingDetail	= $this->user_model->get_shipping_address(array('id'=>$orderDetail->shipping_add_id)); 
		} 

		$transactionreference 	= $_SESSION['Check'];
		$this->order_model->update_order(array('status' => 'Completed'), array('order_id'=> $orderId));
			//echo $this->db->last_query();
			$message = "";
			$message = '
			<!DOCTYPE html>
			<html>
			<head lang="en">
			    <meta charset="UTF-8">
			    <title></title>
			</head>
			<body style="background-color: #f3f3f3; padding: 0; margin: 0;font-family: \'Calibri\', Arial, sans-serif;">
			<div class="outer-div" style="margin: 0 auto; width:600px;background-color: #fff;">
			    <table width="100%">
			        <tr>
			            <td colspan="6">
			                <div style="background-color: #00587C;
			                        width:100%; height:100px; border-bottom: solid 3px #9AC434; padding:20px 0">
			                    <table>
			                        <tr>
			                            <td width="5%"></td>
			                            <td width="60%">
			                                <h1 style="color:#fff; font-size:36px; font-weight: normal">Congratulations!</h1>
			                            </td>
			                            <td width="30%">
			                                <img src="'.base_url().'assets/img/logo.png" />
			                            </td>
			                            <td width="5%"></td>
			                        </tr>
			                    </table>
			                </div>
			            </td>
			        </tr>

			        <tr>
			            <td colspan="6" style="padding:10px 20px;">
			                <p>
			                    Dear <strong> Customer</strong>,<br /><br />
			                    You have completed an order on Mymall. You can expect your product(s) delivered within 2-3 working days.
			                </p>
			            </td>
			        </tr>
			        <tr>
			            <td colspan="" style="padding:10px 20px;">
			                Transaction Refrence:
			            </td>

			            <td colspan="" style="padding:10px 20px;">MM'.
			               $transactionreference.'
			            </td>
			        </tr>

			        <tr>
			            <td colspan="6" style="padding:10px 20px;">
			               Delivery Information
			            </td>
			        </tr>

			        <tr>
			            <td colspan="6" style="padding:10px 20px;">
			                <table width="100%">
			                    <tr>
			                        <td width="40%" height="30">Address</td>
			                        <td width="60%">
			                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px; padding: 0 10px;">
			                                '.$shippingDetail->delivery_addres.'
			                            </div>
			                        </td>
			                      
			                    </tr>
			                    <tr>
			                        <td width="40%" height="30">Phone</td>
			                        <td width="60%">
			                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
			                            padding: 0 10px;">
			                                '.$shippingDetail->phone.'
			                            </div>
			                        </td>
			                        
			                    </tr>
		                        <tr>
			                        <td width="40%" height="30">Deliveryarea</td>
			                        <td width="60%">
			                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
			                            padding: 0 10px;">
			                                '.$shippingDetail->delivery_area.'
			                            </div>
			                        </td>
			                        
			                    </tr>
		                   		<tr>
			                        <td width="40%" height="30">Contact number for delivery</td>
			                        <td width="60%">
			                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
			                            padding: 0 10px;">
			                                '.$shippingDetail->contact_no_delivery.'
			                            </div>
			                        </td>
			                       
			                    </tr>
			                </table>
			            </td>
			        </tr>

			        <tr>
			            <td colspan="6" style="padding:10px 20px;">
			                <div style="border: solid 1px #ccc; background-color: #f3f3f3; padding: 15px;">
			                    <p><strong>Product(s) Information:</strong></p>
			                    <table width="100%" border="0" cellpadding="3" cellspacing="0" style="border-collapse: unset; font-size: 12px;">
								  <tr style="color: #fff;background-color: #ccc;">
									<td style="padding-left: 15px">ITEM</td>
									<td></td>
									<td><strong>ITEM PRICE</strong></td>
									<td><strong>QUANTITY</strong></td>
									<td><strong>TOTAL</strong></td>
									<td>&nbsp;</td>
								  </tr>';
						  
							  $productSize          = "";
						      $productColor         = "";
						      
							  $grandTotal 			= 0;
							  $merStore				= '';
							  if($productDetail)
							  {
							  $productImg 	= base_url().'assets/img/logo.png';
							  foreach($productDetail as $rows)
							  {


							  	if($rows->color != '' && $rows->size != '' )
							    {

							      $productdetail     = $this->product_model->relate_product_size_colour(array('products.id'=>$rows->product_id,'relation_color_size.colour_id'=>$rows->color,'relation_color_size.size_id'=>$rows->size));

							      //print_r($productdetail);
							      if($productdetail && $productdetail != '')
							      {
							      
							        foreach ($productdetail as $prodDeatil) 
							        {

							          if($prodDeatil->color && $prodDeatil->color != '')
							          {
							              $productColor   = $prodDeatil->color;

							          }
							          if($prodDeatil->size && $prodDeatil->size != '')
							          {
							              $productSize   = $prodDeatil->size;

							          }

							        }

							      }

							    }
							    else if($rows->size != '' )
							    {

							      $productdetail     = $this->product_model->relate_product_size(array('relation_color_size.size_id'=>$rows->size ,'products.id'=>$rows->product_id )); 
							      //print_r($productdetail);
							      
							      if($productdetail && $productdetail != '')
							      {
							      
							        foreach ($productdetail as $prodDeatil) 
							        {

							         if($prodDeatil->size && $prodDeatil->size != '')
							          {
							              $productSize   = $prodDeatil->size;

							          }

							        }

							      }

							    }
							    else if($rows->color  != '' )
							    {

							      $productdetail     = $this->product_model->relate_product_colour(array('relation_color_size.colour_id'=> $rows->color,'products.id'=>$rows->product_id )); 

							      if($productdetail && $productdetail != '')
							      {
							      
							        foreach ($productdetail as $prodDeatil) 
							        {

							          if($prodDeatil->color && $prodDeatil->color != '')
							          {
							              $productColor   = $prodDeatil->color;

							          }

							        }

							      }

							    }


							  $productImg  = base_url().'product_images/'.$rows->product_img;
							  $grandTotal = $grandTotal + ($rows->price * $rows->quantity);
							  // get  merchant store name
							  if($merStore != '')
							  $merStore   .= ',  ';
							  $merStore   .= $rows->first_name; 
							  $message .= '
							  <tr>
								<td style="padding:10px" valign="top"><img src="'.$productImg.'" width="78" height="67" border="none" /></td>
								<td width="47%" style="padding: 10px;"><p style="margin: 0;">'.$rows->product_name.'</p>';
								if($productColor != '')
					            {
					                 $message .=  "<br>Color: ".$productColor;
					            }
					            if($productSize != '')
					            {
					                 $message .=  "<br>Size: ".$productSize;
					            }
								$message .= '</td>
								<td width="12%"><p style="margin: 0; color: darkred"><strong>&#x20A6;'.number_format($rows->product_price).'</strong></p></td>
								<td style="padding:10px" width="10%">'.$rows->quantity.'</td>
								 <td style="padding:10px" width="12%"><p style="margin: 0; color: darkred"><strong>&#x20A6;'.number_format($rows->price * $rows->quantity).'</strong></p></td>
							  </tr>
								';
								}
								}
								$grandTotal =   $grandTotal + $orderDetail->delivery_amt;
								$message .= '
								<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td colspan="2"><strong>Delivery Amount:</strong></td>
								<td>&#x20A6; '.number_format($orderDetail->delivery_amt).'</td>
							  </tr>
								<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td colspan="2"><strong>Grand Total: </strong></td>
								<td>&#x20A6; '.number_format($grandTotal).'</td>
							  </tr>

							  </table>
			                </div>
			            </td>
			        </tr>

			        <tr>
			            <td colspan="6" style="padding:10px 20px;">
			                <p style="text-align: center">
			                    For more information, call 0818 778 2542, 0818 778 2542 or send an email to <a href="mailto:operator@mymall.com.ng" target="_top">operator@mymall.com.ng</a>
			                </p>
			            </td>
			        </tr>

			        <tr>
			            <td colspan="6" style="padding:10px 20px;">
			                <table width="100%">
			                    <tr>
			                        <td width="20%"><img src="'.base_url().'assets/img/ecobank.png" /></td>
			                        <td width="20%"></td>
			                        <td width="20%"></td>
			                        <td width="20%"><img src="'.base_url().'assets/img/fedex.png" height="25px" width="auto" /></td>
			                        <td width="20%"><img src="'.base_url().'assets/img/webmall.jpg" height="25px" width="auto" /></td>
			                    </tr>
			                </table>
			            </td>
			        </tr>

			        <tr>
			            <td colspan="6" style="padding:10px 20px; border-top: solid 1px #ccc;">
			                <table width="100%">
			                    <tr>
			                        <td width="60%">
			                            <p style="font-size:13px; color: #333;line-height: 30px">
			                                &copy; 2015, All rights reserved. Mymall
			                            </p>
			                        </td>

			                        <td width="40%">
			                            <img src="'.base_url().'assets/img/youtube.jpg" style="float:right; margin-left:5px;" />
			                            <img src="'.base_url().'assets/img/linkin.jpg" style="float:right; margin-left: 5px;" />
			                            <a target="_blank" href="https://twitter.com/MymallNg"><img src="'.base_url().'assets/img/twitter.jpg" style="float:right; margin-left: 5px;" /></a>
			                            <a target="_blank" href="https://www.facebook.com/MymallNg"><img src="'.base_url().'assets/img/facebook.jpg" style="float:right; margin-left: 5px;" /></a>
			                        </td>
			                    </tr>
			                </table>
			            </td>
			        </tr>
			    </table>
			</div>
			</body>
			</html>
		   ';

		    $_SESSION['orderMessage'] = $message;
			
			$message = ($message);
			
			$this->load->library('email');
			
			$config['mailtype'] = "html";
			
			$this->email->initialize($config);
			
			$this->email->from(AdminEmail, AdminEmailName);

			//$this->email->from("test@geeksperhour.com", "Mymall");
							
			$this->email->to($shippingDetail->email);
			//$this->email->to("parmeet@geeksperhour.com", "Mymall");

			$this->email->subject('Order on mymall - Online Store Solution');
			
			$this->email->message($message);
			
		//	$this->email->send();

			/******  mail send to merchant *******/ 

			$merchantMailArr 		= "";
			$productdetail        	= "";
			$proDetailToGetMer		= $this->cart_model->get_all_items_cart('', $orderId," u.id");
			if($proDetailToGetMer)
			{
				foreach($proDetailToGetMer as $rows)
				{

				  	$merchDeatil = $this->user_model->single_merchant_detail_by_product(array('products.id' =>$rows->product_id));
				  	//$merchDeatil->

					$message = "";
					$message = '
					<!DOCTYPE html>
					<html>
					<head lang="en">
					    <meta charset="UTF-8">
					    <title></title>
					</head>
					<body style="background-color: #f3f3f3; padding: 0; margin: 0;font-family: \'Calibri\', Arial, sans-serif;">
					<div class="outer-div" style="margin: 0 auto; width:600px;background-color: #fff;">
					    <table width="100%">
					        <tr>
					            <td colspan="6">
					                <div style="background-color: #00587C;
					                        width:100%; height:100px; border-bottom: solid 3px #9AC434; padding:20px 0">
					                    <table>
					                        <tr>
					                            <td width="5%"></td>
					                            <td width="60%">
					                                <h1 style="color:#fff; font-size:36px; font-weight: normal">Congratulations!</h1>
					                            </td>
					                            <td width="30%">
					                                <img src="'.base_url().'assets/img/logo.png" />
					                            </td>
					                            <td width="5%"></td>
					                        </tr>
					                    </table>
					                </div>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <p>
					                    Dear <strong> Merchant</strong>,<br /><br />
					                    New order placed on your store. Detail are listed below:
					                </p>
					            </td>
					        </tr>
					        <tr>
					            <td colspan="" style="padding:10px 20px;">
					                Transaction Refrence:
					            </td>

					            <td colspan="" style="padding:10px 20px;">MM'.
					               $transactionreference.'
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					               Delivery Information
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <table width="100%">
					                    <tr>
					                        <td width="40%" height="30">Address</td>
					                        <td width="60%">
					                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px; padding: 0 10px;">
					                                '.$shippingDetail->delivery_addres.'
					                            </div>
					                        </td>
					                      
					                    </tr>
					                    <tr>
					                        <td width="40%" height="30">Phone</td>
					                        <td width="60%">
					                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
					                            padding: 0 10px;">
					                                '.$shippingDetail->phone.'
					                            </div>
					                        </td>
					                        
					                    </tr>
				                        <tr>
					                        <td width="40%" height="30">Deliveryarea</td>
					                        <td width="60%">
					                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
					                            padding: 0 10px;">
					                                '.$shippingDetail->delivery_area.'
					                            </div>
					                        </td>
					                        
					                    </tr>
				                   		<tr>
					                        <td width="40%" height="30">Contact number for delivery</td>
					                        <td width="60%">
					                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
					                            padding: 0 10px;">
					                                '.$shippingDetail->contact_no_delivery.'
					                            </div>
					                        </td>
					                       
					                    </tr>
					                </table>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <div style="border: solid 1px #ccc; background-color: #f3f3f3; padding: 15px;">
					                    <p><strong>Product(s) Information:</strong></p>
					                    <table width="100%" border="0" cellpadding="3" cellspacing="0" style="border-collapse: unset; font-size: 12px;">
										  <tr style="color: #fff;background-color: #ccc;">
											<td style="padding-left: 15px">ITEM</td>
											<td></td>
											<td><strong>ITEM PRICE</strong></td>
											<td><strong>QUANTITY</strong></td>
											<td><strong>TOTAL</strong></td>
											<td>&nbsp;</td>
										  </tr>';
								  
									  $productSize          = "";
								      $productColor         = "";
									  $grandTotal 			= 0;
									  $merStore				= '';
									  if($productDetail)
									  {
									  $productImg 	= base_url().'assets/img/logo.png';
									  foreach($productDetail as $rows)
									  {

									  	if($rows->merchant_id == $merchDeatil->id){

										  	if($rows->color != '' && $rows->size != '' )
										    {

										      $productdetail     = $this->product_model->relate_product_size_colour(array('products.id'=>$rows->product_id,'relation_color_size.colour_id'=>$rows->color,'relation_color_size.size_id'=>$rows->size));

										      //print_r($productdetail);
										      if($productdetail && $productdetail != '')
										      {
										      
										        foreach ($productdetail as $prodDeatil) 
										        {

										          if($prodDeatil->color && $prodDeatil->color != '')
										          {
										              $productColor   = $prodDeatil->color;

										          }
										          if($prodDeatil->size && $prodDeatil->size != '')
										          {
										              $productSize   = $prodDeatil->size;

										          }

										        }

										      }

										    }
										    else if($rows->size != '' )
										    {

										      $productdetail     = $this->product_model->relate_product_size(array('relation_color_size.size_id'=>$rows->size ,'products.id'=>$rows->product_id )); 
										      //print_r($productdetail);
										      
										      if($productdetail && $productdetail != '')
										      {
										      
										        foreach ($productdetail as $prodDeatil) 
										        {

										         if($prodDeatil->size && $prodDeatil->size != '')
										          {
										              $productSize   = $prodDeatil->size;

										          }

										        }

										      }

										    }
										    else if($rows->color  != '' )
										    {

										      $productdetail     = $this->product_model->relate_product_colour(array('relation_color_size.colour_id'=> $rows->color,'products.id'=>$rows->product_id )); 

										      if($productdetail && $productdetail != '')
										      {
										      
										        foreach ($productdetail as $prodDeatil) 
										        {

										          if($prodDeatil->color && $prodDeatil->color != '')
										          {
										              $productColor   = $prodDeatil->color;

										          }

										        }

										      }

										    }

										  $productImg  = base_url().'product_images/'.$rows->product_img;
										  $grandTotal = $grandTotal + ($rows->price * $rows->quantity);
										  // get  merchant store name
										  if($merStore != '')
										  $merStore   .= ',  ';
										  $merStore   .= $rows->first_name; 
										  $message .= '
										  <tr>
											<td style="padding:10px" valign="top"><img src="'.$productImg.'" width="78" height="67" border="none" /></td>
											<td width="47%" style="padding: 10px;"><p style="margin: 0;">'.$rows->product_name.'</p>';
											if($productColor != '')
								            {
								                 $message .=  "<br>Color: ".$productColor;
								            }
								            if($productSize != '')
								            {
								                 $message .=  "<br>Size: ".$productSize;
								            }
											$message .= '</td>
											<td width="12%"><p style="margin: 0; color: darkred"><strong>&#x20A6;'.number_format($rows->product_price).'</strong></p></td>
											<td style="padding:10px" width="10%">'.$rows->quantity.'</td>
											 <td style="padding:10px" width="12%"><p style="margin: 0; color: darkred"><strong>&#x20A6;'.number_format($rows->price * $rows->quantity).'</strong></p></td>
										  </tr>
											';
										   }
										  }
										}
										//$grandTotal =   $grandTotal + $orderDetail->delivery_amt;
										$grandTotal =   $grandTotal;
										/*$message .= '
										<tr>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td colspan="2"><strong>Delivery Amount:</strong></td>
										<td>&#x20A6; '.number_format($orderDetail->delivery_amt).'</td>
									  </tr>'; */
									  $message .= '
										<tr>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td colspan="2"><strong>Grand Total: </strong></td>
										<td>&#x20A6; '.number_format($grandTotal).'</td>
									  </tr>

									  </table>
					                </div>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <p style="text-align: center">
					                    For more information, call 0818 778 2542, 0818 778 2542 or send an email to <a href="mailto:operator@mymall.com.ng" target="_top">operator@mymall.com.ng</a>
					                </p>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <table width="100%">
					                    <tr>
					                        <td width="20%"><img src="'.base_url().'assets/img/ecobank.png" /></td>
					                        <td width="20%"></td>
					                        <td width="20%"></td>
					                        <td width="20%"><img src="'.base_url().'assets/img/fedex.png" height="25px" width="auto" /></td>
					                        <td width="20%"><img src="'.base_url().'assets/img/webmall.jpg" height="25px" width="auto" /></td>
					                    </tr>
					                </table>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px; border-top: solid 1px #ccc;">
					                <table width="100%">
					                    <tr>
					                        <td width="60%">
					                            <p style="font-size:13px; color: #333;line-height: 30px">
					                                &copy; 2015, All rights reserved. Mymall
					                            </p>
					                        </td>

					                        <td width="40%">
					                            <img src="'.base_url().'assets/img/youtube.jpg" style="float:right; margin-left:5px;" />
					                            <img src="'.base_url().'assets/img/linkin.jpg" style="float:right; margin-left: 5px;" />
					                            <a target="_blank" href="https://twitter.com/MymallNg"><img src="'.base_url().'assets/img/twitter.jpg" style="float:right; margin-left: 5px;" /></a>
					                            <a target="_blank" href="https://www.facebook.com/MymallNg"><img src="'.base_url().'assets/img/facebook.jpg" style="float:right; margin-left: 5px;" /></a>
					                        </td>
					                    </tr>
					                </table>
					            </td>
					        </tr>
					    </table>
					</div>
					</body>
					</html>
				   ';
				    //$_SESSION['orderMessage'] = $message;
					
					$message = ($message);
					
					$this->load->library('email');
					
					$config['mailtype'] = "html";
					
					$this->email->initialize($config);
					
					$this->email->from(AdminEmail, AdminEmailName);

					//$this->email->from("test@geeksperhour.com", "Mymall");
									
					$this->email->to($merchDeatil->email);
					//$this->email->to("parmeet@geeksperhour.com", "Mymall");
		
					$this->email->subject('New order placed on your store');
					
					$this->email->message($message);
					
					$this->email->send();

				}
			}
	
			header("Location: ".base_url()."order/success");
			die();

	}

	public function netpluspay_cancle(){
		
		$this->order_model->update_order(array( 'status' => 'Canceled'), array('order_id'=> $_SESSION['Check']));
		$transactionreference 	= $_SESSION['Check'];

		unset($_SESSION['lastInsertedOrderId']);

		$_SESSION['Check'] = "55".date('Ymdhis');
		
		$user_id 				= '';
		$sessionId				= session_id();
		$order_id 				= $_SESSION['Check'];

		if(isset($_SESSION['userDetail']) && !empty($_SESSION['userDetail'])){
			$user_id  = $_SESSION['userDetail']->id;
		}
		
		if(isset($_SESSION['cart']))
		{
			
			for($i = 0; $i < count($_SESSION['cart']); $i ++)
			{
				// check product exist with active condtion 
				$productColor = $_SESSION['cart'][$i]['productColor'];
				$productSize  = $_SESSION['cart'][$i]['productSize'];
				$productId 	  = $_SESSION['cart'][$i]['productid'];
				$productPrice = $this->product_model->single_product_detail(array("products.isactive"=>"t","products.id"=>$productId));
				
				$data['cart_session']	= $sessionId;
				$data['date']			= date("Y:m:d H-i-s");
				$data['product_id']		= $productId;
				$data['price']			= $productPrice->product_price;
				$data['product_name']	= $productPrice->product_name;
				$data['quantity']		= $_SESSION['cart'][$i]['qty'];
				$data['user_id']		= $user_id;
				$data['color']			= $_SESSION['cart'][$i]['productColor'];
				$data['size']			= $_SESSION['cart'][$i]['productSize'];
				$data['order_id']		= $order_id;
				
				$cartDetail				= $this->cart_model->get_to_cart($productId,$sessionId,$user_id, $order_id,$productColor,$productSize);

				if(!$cartDetail){
					
					$lastInsertedId = $this->cart_model->insert_to_cart($data);
					$_SESSION['cart'][$i]['cartId']		= $lastInsertedId;
					
				}
			}
		}


		$message = '
		<!DOCTYPE html>
		<html>
		<head lang="en">
		    <meta charset="UTF-8">
		    <title></title>
		</head>
		<body style="background-color: #f3f3f3; padding: 0; margin: 0;font-family: \'Calibri\', Arial, sans-serif;">
		<div class="outer-div" style="margin: 0 auto; width:600px;background-color: #fff;">
		    <table width="100%">
		        <tr>
		            <td colspan="6">
		                <div style="background-color: #00587C;
		                        width:100%; height:100px; border-bottom: solid 3px #9AC434; padding:20px 0">
		                    <table>
		                        <tr>
		                            <td width="5%"></td>
		                            <td width="60%">
		                                <h1 class="responseDescription" style="color:#fff; font-size:36px; font-weight: normal">Canceled</h1>
		                            </td>
		                            <td width="30%">
		                                <img src="'.base_url().'assets/img/logo.png" />
		                            </td>
		                            <td width="5%"></td>
		                        </tr>
		                    </table>
		                </div>
		            </td>
		        </tr>

		        <tr>
		            <td colspan="6" style="padding:10px 20px;">
		                <p>
		                    Dear <strong> Customer</strong>,<br /><br /><strong style="color:red;">Your transaction has canceled.</strong>
		                    <br />Please try again!
		                </p>
		            </td>
		        </tr>

	        	<tr>
		            <td colspan="" style="padding:10px 20px;">
		                Transaction Refrence:
		            </td>

		            <td colspan="" style="padding:10px 20px;">MM'.
		               $transactionreference.'
		            </td>
		        </tr>

		        <tr>
		            <td colspan="6" style="padding:10px 20px;">
		               
		            </td>
		        </tr>

		        <tr>
		            <td colspan="6" style="padding:10px 20px;">
		                <p style="text-align: center">
		                    For more information, call 0818 778 2542, 0818 778 2542 or send an email to <a href="mailto:operator@mymall.com.ng" target="_top">operator@mymall.com.ng</a>
		                </p>
		            </td>
		        </tr>

		        <tr>
		            <td colspan="6" style="padding:10px 20px;">
		                <table width="100%">
		                    <tr>
		                        <td width="20%"><img src="'.base_url().'assets/img/ecobank.png" /></td>
		                        <td width="20%"></td>
		                        <td width="20%"></td>
		                        <td width="20%"><img src="'.base_url().'assets/img/fedex.png" height="25px" width="auto" /></td>
		                        <td width="20%"><img src="'.base_url().'assets/img/webmall.jpg" height="25px" width="auto" /></td>
		                    </tr>
		                </table>
		            </td>
		        </tr>

		        <tr>
		            <td colspan="6" style="padding:10px 20px; border-top: solid 1px #ccc;">
		                <table width="100%">
		                    <tr>
		                        <td width="60%">
		                            <p style="font-size:13px; color: #333;line-height: 30px">
		                                &copy; 2015, All rights reserved. Mymall
		                            </p>
		                        </td>

		                        <td width="40%">
		                            <img src="'.base_url().'assets/img/youtube.jpg" style="float:right; margin-left:5px;" />
		                            <img src="'.base_url().'assets/img/linkin.jpg" style="float:right; margin-left: 5px;" />
		                            <a target="_blank" href="https://twitter.com/MymallNg"><img src="'.base_url().'assets/img/twitter.jpg" style="float:right; margin-left: 5px;" /></a>
		                            <a target="_blank" href="https://www.facebook.com/MymallNg"><img src="'.base_url().'assets/img/facebook.jpg" style="float:right; margin-left: 5px;" /></a>
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		</div>
		</body>
		</html>
	   ';

	    $_SESSION['orderMessage'] = $message;
		
		$message = ($message);
		
		$this->load->library('email');
		
		$config['mailtype'] = "html";
		
		$this->email->initialize($config);
		
		$this->email->from(AdminEmail, AdminEmailName);

		//$this->email->from("test@geeksperhour.com", "Mymall");
						
		$this->email->to($shippingDetail->email);
		//$this->email->to("parmeet@geeksperhour.com", "Mymall");

		$this->email->subject('Order on mymall - Online Store Solution');
		
		$this->email->message($message);
		
		$this->email->send();

		header("Location: ".base_url()."order/canceled");
		
		die();

	}
	

	public function interswitchng_url()
	{

		//unset($_SESSION['cart']);
		//unset($_SESSION['Check']);
		if(isset($_POST))
		{

			$statusCode = 
			array(
				'00'=>'Approved by Financial Institution'
				,'01'=>'Refer to Financial Institution'
				,'02'=>'Refer to Financial Institution, Special Condition'
				,'03'=>'Invalid Merchant'
				,'04'=>'Pick-up card'
				,'05'=>'Do Not Honor'
				,'06'=>'Error'
				,'07'=>'Pick-Up Card, Special Condition'
				,'08'=>'Honor with Identification'
				,'09'=>'Request in Progress'
				,'10'=>'Approved by Financial Institution, Partial'
				,'11'=>'Approved by Financial Institution, VIP'
				,'12'=>'Invalid Transaction'
				,'13'=>'Invalid Amount'
				,'14'=>'The card number inputted is invalid, please re-try with a valid cardnumber'
				,'15'=>'No Such Financial Institution'
				,'16'=>'Approved by Financial Institution, Update Track 3'
				,'17'=>'Customer Cancellation'
				,'18'=>'Customer Dispute'
				,'19'=>'Re-enter Transaction'
				,'20'=>'Invalid Response from Financial Institution'
				,'21'=>'No Action Taken by Financial Institution'
				,'22'=>'Suspected Malfunction'
				,'23'=>'Unacceptable Transaction Fee'
				,'24'=>'File Update not Supported'
				//,'25'=>'Unable to Locate Record'
				,'26'=>'Duplicate Record'
				,'27'=>'File Update Field Edit Error'
				,'28'=>'File Update File Locked'
				,'29'=>'File Update Failed'
				,'30'=>'Format Error'
				,'31'=>'Bank Not Supported'
				,'32'=>'Completed Partially by Financial Institution'
				,'33'=>'Expired Card, Pick-Up'
				,'34'=>'Suspected Fraud, Pick-Up'
				,'35'=>'Contact Acquirer, Pick-Up'
				,'36'=>'Restricted Card, Pick-Up'
				,'37'=>'Call Acquirer Security, Pick-Up'
				,'38'=>'Incorrect security details provided. Pin tries exceeded'
				,'39'=>'No Credit Account'
				,'40'=>'Function not Supported'
				,'41'=>'Lost Card, Pick-Up'
				,'42'=>'No Universal Account'
				,'43'=>'Stolen Card, Pick-Up'
				,'44'=>'No Investment Account'
				,'51'=>'Insufficient Funds'
				,'52'=>'No Check Account'
				,'53'=>'No Savings Account'
				,'54'=>'Expired Card'
				,'55'=>'Incorrect security details provided'
				,'56'=>'ncorrect card details, please verify that the expiry date inputted is correct'
				,'57'=>'Your bank has prevented your card from carrying out this transaction, please
contact your bank'
				//,'58'=>'Transaction not permitted on Terminal'
				,'59'=>'Suspected Fraud'
				,'60'=>'Contact Acquirer'
				,'61'=>'Your bank has prevented your card from carrying out this transaction, please contact your bank'
				,'62'=>'Restricted Card'
				,'63'=>'Security Violation'
				,'64'=>'Original Amount Incorrect'
				,'65'=>'Exceeds withdrawal frequency'
				,'66'=>'Call Acquirer Security'
				,'67'=>'Hard Capture'
				,'68'=>'Response Received Too Late'
				,'75'=>'Incorrect security details provided. Pin tries exceeded'
				//,'76'=>'Reserved for Future Postilion Use'
				,'77'=>'Intervene, Bank Approval Required'
				,'78'=>'Intervene, Bank Approval Required for Partial Amount'
				,'90'=>'Cut-off in Progress'
				,'91'=>'Issuer or Switch Inoperative'
				,'92'=>'Routing Error'
				,'93'=>'Violation of law'
				,'94'=>'Duplicate Transaction'
				,'95'=>'Reconcile Error'
				,'96'=>'System Malfunction'
				,'98'=>'Exceeds Cash Limit'
				,'A0'=>'Unexpected error'
				,'A4'=>'Transaction not permitted to card holder, via channels'
				,'Z0'=>'Transaction Status Unconfirmed'
				,'Z1'=>'Transaction Error'
				,'Z2'=>'Bank account error'
				,'Z3'=>'Bank collections account error'
				,'Z4'=>'Interface Integration Error'
				,'Z5'=>'Duplicate Reference Error'
				,'Z6'=>'Incomplete Transaction'
				,'Z7'=>'Transaction Split Pre-processing Error'
				,'Z8'=>'Invalid Card Number, via channels'
				,'Z9'=>'Transaction not permitted to card holder, via channels'
				,'Z25'=>'This transaction does not exist on WebPAY'

				,'X00'=>'Account error, please contact your bank'
				,'Z3'=>'The amount requested is above the limit permitted by your bank, please contact your bank'
				,'X4'=>'The amount requested is too low'
				,'X5'=>'The amount requested is above the limit permitted by your bank, please contact your bank'
				
			);

			$orderId 					= $_SESSION['Check'];
			$status_get_url				= "https://webpay.interswitchng.com/paydirect/api/v1/gettransaction.xml";
			$productid 					= "5576";
			$transactionreference 		= $_POST['txnref'];
			$amount 					= $_SESSION['cartAmount'];
			$mac 						= '7E628F9302BA85744B213EB6EAD352744ABE39F00A95AE9AF2614055AE02AC9904AB91A6722BBBBE066E1A61417127304F79B0ACF308818441A9FCBA79202BF3';
			$stringToBehased 			= $productid . $transactionreference . $mac;
			$hash 						= hash('sha512', $stringToBehased);
			$headr 						= array();
			$headr[] 					= 'Content-type: application/json';
			$headr[] 					= 'hash:' . $hash;

			$postdata 					=  http_build_query(array("hash" => $hash), '', '&');

			$url = $status_get_url."?productid=".$productid."&transactionreference=".$transactionreference."&amount=".$amount."";
			$opts = array(
			    'http'=>array(
			        'method'=>"GET",
			        'header' => "hash: $hash\r\n" 
			    )
			);

			$context  = stream_context_create($opts);
			$response = file_get_contents($url, false, $context);
			//var_dump($response);

			$xml = simplexml_load_string($response);
			//echo $xml->ResponseCode;
			//print_r($xml);

			//die();

			if(array_key_exists("$xml->ResponseCode", $statusCode))
			{

				$productdetail  = "";
				$shippingDetail	= "";
				$orderDetail    = $this->order_model->single_order_detail(array('order_id' => $orderId));
				$productDetail	= $this->cart_model->get_all_items_cart('', $orderId);
				if($orderDetail && $orderDetail->shipping_add_id != '')
				{
					$shippingDetail	= $this->user_model->get_shipping_address(array('id'=>$orderDetail->shipping_add_id)); 
				} 

				if($xml->ResponseCode  == "00")
				{
					$this->courier();
					$this->order_model->update_order(array('status' => 'Completed', 'response_code' => $xml->ResponseCode, 'transaction_ref' => $transactionreference), array('order_id'=>$orderId));
					//echo $this->db->last_query();
					
					$message = "";
					$message = '
					<!DOCTYPE html>
					<html>
					<head lang="en">
					    <meta charset="UTF-8">
					    <title></title>
					</head>
					<body style="background-color: #f3f3f3; padding: 0; margin: 0;font-family: \'Calibri\', Arial, sans-serif;">
					<div class="outer-div" style="margin: 0 auto; width:600px;background-color: #fff;">
					    <table width="100%">
					        <tr>
					            <td colspan="6">
					                <div style="background-color: #00587C;
					                        width:100%; height:100px; border-bottom: solid 3px #9AC434; padding:20px 0">
					                    <table>
					                        <tr>
					                            <td width="5%"></td>
					                            <td width="60%">
					                                <h1 style="color:#fff; font-size:36px; font-weight: normal">Congratulations!</h1>
					                            </td>
					                            <td width="30%">
					                                <img src="'.base_url().'assets/img/logo.png" />
					                            </td>
					                            <td width="5%"></td>
					                        </tr>
					                    </table>
					                </div>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <p>
					                    Dear <strong> Customer</strong>,<br /><br />
					                    You have completed an order on Mymall. You can expect your product(s) delivered within 2-3 working days.
					                </p>
					            </td>
					        </tr>
					        <tr>
					            <td colspan="" style="padding:10px 20px;">
					                Transaction Refrence:
					            </td>

					            <td colspan="" style="padding:10px 20px;">MM'.
					               $transactionreference.'
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					               Delivery Information
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <table width="100%">
					                    <tr>
					                        <td width="40%" height="30">Address</td>
					                        <td width="60%">
					                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px; padding: 0 10px;">
					                                '.$shippingDetail->delivery_addres.'
					                            </div>
					                        </td>
					                      
					                    </tr>
					                    <tr>
					                        <td width="40%" height="30">Phone</td>
					                        <td width="60%">
					                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
					                            padding: 0 10px;">
					                                '.$shippingDetail->phone.'
					                            </div>
					                        </td>
					                        
					                    </tr>
				                        <tr>
					                        <td width="40%" height="30">Deliveryarea</td>
					                        <td width="60%">
					                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
					                            padding: 0 10px;">
					                                '.$shippingDetail->delivery_area.'
					                            </div>
					                        </td>
					                        
					                    </tr>
				                   		<tr>
					                        <td width="40%" height="30">Contact number for delivery</td>
					                        <td width="60%">
					                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
					                            padding: 0 10px;">
					                                '.$shippingDetail->contact_no_delivery.'
					                            </div>
					                        </td>
					                       
					                    </tr>
					                </table>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <div style="border: solid 1px #ccc; background-color: #f3f3f3; padding: 15px;">
					                    <p><strong>Product(s) Information:</strong></p>
					                    <table width="100%" border="0" cellpadding="3" cellspacing="0" style="border-collapse: unset; font-size: 12px;">
										  <tr style="color: #fff;background-color: #ccc;">
											<td style="padding-left: 15px">ITEM</td>
											<td></td>
											<td><strong>ITEM PRICE</strong></td>
											<td><strong>QUANTITY</strong></td>
											<td><strong>TOTAL</strong></td>
											<td>&nbsp;</td>
										  </tr>';
								  
									  $productSize          = "";
								      $productColor         = "";
								      
									  $grandTotal 			= 0;
									  $merStore				= '';
									  if($productDetail)
									  {
									  $productImg 	= base_url().'assets/img/logo.png';
									  foreach($productDetail as $rows)
									  {

									  	if($rows->color != '' && $rows->size != '' )
									    {

									      $productdetail     = $this->product_model->relate_product_size_colour(array('products.id'=>$rows->product_id,'relation_color_size.colour_id'=>$rows->color,'relation_color_size.size_id'=>$rows->size));

									      //print_r($productdetail);
									      if($productdetail && $productdetail != '')
									      {
									      
									        foreach ($productdetail as $prodDeatil) 
									        {

									          if($prodDeatil->qty > 0){

										          $newQty =  ($prodDeatil->qty - $rows->quantity);
										          // update product qty...
										          $this->product_model->update_product_size_colour(array('id' => $prodDeatil->id), array('qty' => $newQty));
									      	  }
									          if($prodDeatil->color && $prodDeatil->color != '')
									          {
									              $productColor   = $prodDeatil->color;

									          }
									          if($prodDeatil->size && $prodDeatil->size != '')
									          {
									              $productSize   = $prodDeatil->size;

									          }

									        }

									      }

									    }
									    else if($rows->size != '' )
									    {

									      $productdetail     = $this->product_model->relate_product_size(array('relation_color_size.size_id'=>$rows->size ,'products.id'=>$rows->product_id )); 
									      //print_r($productdetail);
									      
									      if($productdetail && $productdetail != '')
									      {
									      
									        foreach ($productdetail as $prodDeatil) 
									        {
									        	// update product qty...
									         if($prodDeatil->qty > 0){

										          $newQty =  ($prodDeatil->qty - $rows->quantity);
										          
										          $this->product_model->update_product_size_colour(array('id' => 	$prodDeatil->id), array('qty' => $newQty));
									      	  }
									         if($prodDeatil->size && $prodDeatil->size != '')
									          {
									              $productSize   = $prodDeatil->size;

									          }

									        }

									      }

									    }
									    else if($rows->color  != '' )
									    {

									      $productdetail     = $this->product_model->relate_product_colour(array('relation_color_size.colour_id'=> $rows->color,'products.id'=>$rows->product_id )); 

									      if($productdetail && $productdetail != '')
									      {
									      
									        foreach ($productdetail as $prodDeatil) 
									        {

									          // update product qty...
									          if($prodDeatil->qty > 0){

										        $newQty =  ($prodDeatil->qty - $rows->quantity);
										          
										        $this->product_model->update_product_size_colour(array('id' => 	$prodDeatil->id), array('qty' => $newQty));
									      	  }
									          if($prodDeatil->color && $prodDeatil->color != '')
									          {
									              $productColor   = $prodDeatil->color;

									          }

									        }

									      }

									    }

									    if($rows->color == '' && $rows->size == '' )
									    {

									      	$pDetail     = $this->product_model->single_product_detail(array('products.id'=>$rows->product_id )); 
									        
									        if($pDetail && $pDetail != "") 
									        {

									          // update product qty...
									          if($pDetail->product_quantity > 0){

										        $newQty =  ($pDetail->product_quantity - $rows->quantity); 
										          
										        $this->product_model->update_product(array('product_quantity' =>$newQty ), $pDetail->id );
									      	  }
									      	}

									    }

									  $productImg  = base_url().'product_images/'.$rows->product_img;
									  $grandTotal = $grandTotal + ($rows->price * $rows->quantity);
									  // get  merchant store name
									  if($merStore != '')
									  $merStore   .= ',  ';
									  $merStore   .= $rows->first_name; 
									  $message .= '
									  <tr>
										<td style="padding:10px" valign="top"><img src="'.$productImg.'" width="78" height="67" border="none" /></td>
										<td width="47%" style="padding: 10px;"><p style="margin: 0;">'.$rows->product_name.'</p>';
										if($productColor != '')
							            {
							                 $message .=  "<br>Color: ".$productColor;
							            }
							            if($productSize != '')
							            {
							                 $message .=  "<br>Size: ".$productSize;
							            }
										$message .= '</td>
										<td width="12%"><p style="margin: 0; color: darkred"><strong>&#x20A6;'.number_format($rows->product_price).'</strong></p></td>
										<td style="padding:10px" width="10%">'.$rows->quantity.'</td>
										 <td style="padding:10px" width="12%"><p style="margin: 0; color: darkred"><strong>&#x20A6;'.number_format($rows->price * $rows->quantity).'</strong></p></td>
									  </tr>
										';
										}
										}
										$grandTotal =   $grandTotal + $orderDetail->delivery_amt;
										$message .= '
										<tr>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td colspan="2"><strong>Delivery Amount:</strong></td>
										<td>&#x20A6; '.number_format($orderDetail->delivery_amt).'</td>
									  </tr>
										<tr>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td colspan="2"><strong>Grand Total: </strong></td>
										<td>&#x20A6; '.number_format($grandTotal).'</td>
									  </tr>

									  </table>
					                </div>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <p style="text-align: center">
					                    For more information, call 0818 778 2542, 0818 778 2542 or send an email to <a href="mailto:operator@mymall.com.ng" target="_top">operator@mymall.com.ng</a>
					                </p>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <table width="100%">
					                    <tr>
					                        <td width="20%"><img src="'.base_url().'assets/img/ecobank.png" /></td>
					                        <td width="20%"></td>
					                        <td width="20%"></td>
					                        <td width="20%"><img src="'.base_url().'assets/img/fedex.png" height="25px" width="auto" /></td>
					                        <td width="20%"><img src="'.base_url().'assets/img/webmall.jpg" height="25px" width="auto" /></td>
					                    </tr>
					                </table>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px; border-top: solid 1px #ccc;">
					                <table width="100%">
					                    <tr>
					                        <td width="60%">
					                            <p style="font-size:13px; color: #333;line-height: 30px">
					                                &copy; 2015, All rights reserved. Mymall
					                            </p>
					                        </td>

					                        <td width="40%">
					                            <img src="'.base_url().'assets/img/youtube.jpg" style="float:right; margin-left:5px;" />
					                            <img src="'.base_url().'assets/img/linkin.jpg" style="float:right; margin-left: 5px;" />
					                            <a target="_blank" href="https://twitter.com/MymallNg"><img src="'.base_url().'assets/img/twitter.jpg" style="float:right; margin-left: 5px;" /></a>
					                            <a target="_blank" href="https://www.facebook.com/MymallNg"><img src="'.base_url().'assets/img/facebook.jpg" style="float:right; margin-left: 5px;" /></a>
					                        </td>
					                    </tr>
					                </table>
					            </td>
					        </tr>
					    </table>
					</div>
					</body>
					</html>
				   ';

				    $_SESSION['orderMessage'] = $message;
					
					$message = ($message);
					
					$this->load->library('email');
					
					$config['mailtype'] = "html";
					
					$this->email->initialize($config);
					
					$this->email->from(AdminEmail, AdminEmailName);

					//$this->email->from("test@geeksperhour.com", "Mymall");
									
					$this->email->to($shippingDetail->email);
					//$this->email->to("parmeet@geeksperhour.com", "Mymall");
		
					$this->email->subject('Order on mymall - Online Store Solution');
					
					$this->email->message($message);
					
					$this->email->send();

					/******  mail send to merchant *******/ 

					$merchantMailArr 		= "";
					$productdetail        	= "";
					$proDetailToGetMer		= $this->cart_model->get_all_items_cart('', $orderId," u.id");
					if($proDetailToGetMer)
					{
						foreach($proDetailToGetMer as $rows)
						{

						  	$merchDeatil = $this->user_model->single_merchant_detail_by_product(array('products.id' =>$rows->product_id));
						  	//$merchDeatil->

							$message = "";
							$message = '
							<!DOCTYPE html>
							<html>
							<head lang="en">
							    <meta charset="UTF-8">
							    <title></title>
							</head>
							<body style="background-color: #f3f3f3; padding: 0; margin: 0;font-family: \'Calibri\', Arial, sans-serif;">
							<div class="outer-div" style="margin: 0 auto; width:600px;background-color: #fff;">
							    <table width="100%">
							        <tr>
							            <td colspan="6">
							                <div style="background-color: #00587C;
							                        width:100%; height:100px; border-bottom: solid 3px #9AC434; padding:20px 0">
							                    <table>
							                        <tr>
							                            <td width="5%"></td>
							                            <td width="60%">
							                                <h1 style="color:#fff; font-size:36px; font-weight: normal">Congratulations!</h1>
							                            </td>
							                            <td width="30%">
							                                <img src="'.base_url().'assets/img/logo.png" />
							                            </td>
							                            <td width="5%"></td>
							                        </tr>
							                    </table>
							                </div>
							            </td>
							        </tr>

							        <tr>
							            <td colspan="6" style="padding:10px 20px;">
							                <p>
							                    Dear <strong> Merchant</strong>,<br /><br />
							                    New order placed on your store. Detail are listed below:
							                </p>
							            </td>
							        </tr>
							        <tr>
							            <td colspan="" style="padding:10px 20px;">
							                Transaction Refrence:
							            </td>

							            <td colspan="" style="padding:10px 20px;">MM'.
							               $transactionreference.'
							            </td>
							        </tr>

							        <tr>
							            <td colspan="6" style="padding:10px 20px;">
							               Delivery Information
							            </td>
							        </tr>

							        <tr>
							            <td colspan="6" style="padding:10px 20px;">
							                <table width="100%">
							                    <tr>
							                        <td width="40%" height="30">Address</td>
							                        <td width="60%">
							                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px; padding: 0 10px;">
							                                '.$shippingDetail->delivery_addres.'
							                            </div>
							                        </td>
							                      
							                    </tr>
							                    <tr>
							                        <td width="40%" height="30">Phone</td>
							                        <td width="60%">
							                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
							                            padding: 0 10px;">
							                                '.$shippingDetail->phone.'
							                            </div>
							                        </td>
							                        
							                    </tr>
						                        <tr>
							                        <td width="40%" height="30">Deliveryarea</td>
							                        <td width="60%">
							                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
							                            padding: 0 10px;">
							                                '.$shippingDetail->delivery_area.'
							                            </div>
							                        </td>
							                        
							                    </tr>
						                   		<tr>
							                        <td width="40%" height="30">Contact number for delivery</td>
							                        <td width="60%">
							                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
							                            padding: 0 10px;">
							                                '.$shippingDetail->contact_no_delivery.'
							                            </div>
							                        </td>
							                       
							                    </tr>
							                </table>
							            </td>
							        </tr>

							        <tr>
							            <td colspan="6" style="padding:10px 20px;">
							                <div style="border: solid 1px #ccc; background-color: #f3f3f3; padding: 15px;">
							                    <p><strong>Product(s) Information:</strong></p>
							                    <table width="100%" border="0" cellpadding="3" cellspacing="0" style="border-collapse: unset; font-size: 12px;">
												  <tr style="color: #fff;background-color: #ccc;">
													<td style="padding-left: 15px">ITEM</td>
													<td></td>
													<td><strong>ITEM PRICE</strong></td>
													<td><strong>QUANTITY</strong></td>
													<td><strong>TOTAL</strong></td>
													<td>&nbsp;</td>
												  </tr>';
										  
											  $productSize          = "";
										      $productColor         = "";
											  $grandTotal 			= 0;
											  $merStore				= '';
											  if($productDetail)
											  {
											  $productImg 	= base_url().'assets/img/logo.png';
											  foreach($productDetail as $rows)
											  {

											  	if($rows->merchant_id == $merchDeatil->id){

												  	if($rows->color != '' && $rows->size != '' )
												    {

												      $productdetail     = $this->product_model->relate_product_size_colour(array('products.id'=>$rows->product_id,'relation_color_size.colour_id'=>$rows->color,'relation_color_size.size_id'=>$rows->size));

												      //print_r($productdetail);
												      if($productdetail && $productdetail != '')
												      {
												      
												        foreach ($productdetail as $prodDeatil) 
												        {

												          if($prodDeatil->color && $prodDeatil->color != '')
												          {
												              $productColor   = $prodDeatil->color;

												          }
												          if($prodDeatil->size && $prodDeatil->size != '')
												          {
												              $productSize   = $prodDeatil->size;

												          }

												        }

												      }

												    }
												    else if($rows->size != '' )
												    {

												      $productdetail     = $this->product_model->relate_product_size(array('relation_color_size.size_id'=>$rows->size ,'products.id'=>$rows->product_id )); 
												      //print_r($productdetail);
												      
												      if($productdetail && $productdetail != '')
												      {
												      
												        foreach ($productdetail as $prodDeatil) 
												        {

												         if($prodDeatil->size && $prodDeatil->size != '')
												          {
												              $productSize   = $prodDeatil->size;

												          }

												        }

												      }

												    }
												    else if($rows->color  != '' )
												    {

												      $productdetail     = $this->product_model->relate_product_colour(array('relation_color_size.colour_id'=> $rows->color,'products.id'=>$rows->product_id )); 

												      if($productdetail && $productdetail != '')
												      {
												      
												        foreach ($productdetail as $prodDeatil) 
												        {

												          if($prodDeatil->color && $prodDeatil->color != '')
												          {
												              $productColor   = $prodDeatil->color;

												          }

												        }

												      }

												    }

												  $productImg  = base_url().'product_images/'.$rows->product_img;
												  $grandTotal = $grandTotal + ($rows->price * $rows->quantity);
												  // get  merchant store name
												  if($merStore != '')
												  $merStore   .= ',  ';
												  $merStore   .= $rows->first_name; 
												  $message .= '
												  <tr>
													<td style="padding:10px" valign="top"><img src="'.$productImg.'" width="78" height="67" border="none" /></td>
													<td width="47%" style="padding: 10px;"><p style="margin: 0;">'.$rows->product_name.'</p>';
													if($productColor != '')
										            {
										                 $message .=  "<br>Color: ".$productColor;
										            }
										            if($productSize != '')
										            {
										                 $message .=  "<br>Size: ".$productSize;
										            }
													$message .= '</td>
													<td width="12%"><p style="margin: 0; color: darkred"><strong>&#x20A6;'.number_format($rows->product_price).'</strong></p></td>
													<td style="padding:10px" width="10%">'.$rows->quantity.'</td>
													 <td style="padding:10px" width="12%"><p style="margin: 0; color: darkred"><strong>&#x20A6;'.number_format($rows->price * $rows->quantity).'</strong></p></td>
												  </tr>
													';
												   }
												  }
												}
												//$grandTotal =   $grandTotal + $orderDetail->delivery_amt;
												$grandTotal =   $grandTotal;
												/*$message .= '
												<tr>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td colspan="2"><strong>Delivery Amount:</strong></td>
												<td>&#x20A6; '.number_format($orderDetail->delivery_amt).'</td>
											  </tr>'; */
											  $message .= '
												<tr>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td colspan="2"><strong>Grand Total: </strong></td>
												<td>&#x20A6; '.number_format($grandTotal).'</td>
											  </tr>

											  </table>
							                </div>
							            </td>
							        </tr>

							        <tr>
							            <td colspan="6" style="padding:10px 20px;">
							                <p style="text-align: center">
							                    For more information, call 0818 778 2542, 0818 778 2542 or send an email to <a href="mailto:operator@mymall.com.ng" target="_top">operator@mymall.com.ng</a>
							                </p>
							            </td>
							        </tr>

							        <tr>
							            <td colspan="6" style="padding:10px 20px;">
							                <table width="100%">
							                    <tr>
							                        <td width="20%"><img src="'.base_url().'assets/img/ecobank.png" /></td>
							                        <td width="20%"></td>
							                        <td width="20%"></td>
							                        <td width="20%"><img src="'.base_url().'assets/img/fedex.png" height="25px" width="auto" /></td>
							                        <td width="20%"><img src="'.base_url().'assets/img/webmall.jpg" height="25px" width="auto" /></td>
							                    </tr>
							                </table>
							            </td>
							        </tr>

							        <tr>
							            <td colspan="6" style="padding:10px 20px; border-top: solid 1px #ccc;">
							                <table width="100%">
							                    <tr>
							                        <td width="60%">
							                            <p style="font-size:13px; color: #333;line-height: 30px">
							                                &copy; 2015, All rights reserved. Mymall
							                            </p>
							                        </td>

							                        <td width="40%">
							                            <img src="'.base_url().'assets/img/youtube.jpg" style="float:right; margin-left:5px;" />
							                            <img src="'.base_url().'assets/img/linkin.jpg" style="float:right; margin-left: 5px;" />
							                            <a target="_blank" href="https://twitter.com/MymallNg"><img src="'.base_url().'assets/img/twitter.jpg" style="float:right; margin-left: 5px;" /></a>
							                            <a target="_blank" href="https://www.facebook.com/MymallNg"><img src="'.base_url().'assets/img/facebook.jpg" style="float:right; margin-left: 5px;" /></a>
							                        </td>
							                    </tr>
							                </table>
							            </td>
							        </tr>
							    </table>
							</div>
							</body>
							</html>
						   ';
						    //$_SESSION['orderMessage'] = $message;
							
							$message = ($message);
							
							$this->load->library('email');
							
							$config['mailtype'] = "html";
							
							$this->email->initialize($config);
							
							$this->email->from(AdminEmail, AdminEmailName);

							//$this->email->from("test@geeksperhour.com", "Mymall");
											
							$this->email->to($merchDeatil->email);
							//$this->email->to("parmeet@geeksperhour.com", "Mymall");
				
							$this->email->subject('New order placed on your store');
							
							$this->email->message($message);
							
							$this->email->send();

						}
					}

					
					header("Location: ".base_url()."order/success");
					die();

				}
				else
				{
					//* insert product  in cart *******/
					
					unset($_SESSION['lastInsertedOrderId']);

					$_SESSION['Check'] = "55".date('Ymdhis');
					
					$user_id 				= '';
					$sessionId				= session_id();
					$order_id 				= $_SESSION['Check'];

					if(isset($_SESSION['userDetail']) && !empty($_SESSION['userDetail'])){
						$user_id  = $_SESSION['userDetail']->id;
					}
					
					
					if(isset($_SESSION['cart']))
					{
						
						for($i = 0; $i < count($_SESSION['cart']); $i ++)
						{
							// check product exist with active condtion 
							$productColor = $_SESSION['cart'][$i]['productColor'];
							$productSize  = $_SESSION['cart'][$i]['productSize'];
							$productId 	  = $_SESSION['cart'][$i]['productid'];
							$productPrice = $this->product_model->single_product_detail(array("products.isactive"=>"t","products.id"=>$productId));
							
							$data['cart_session']	= $sessionId;
							$data['date']			= date("Y:m:d H-i-s");
							$data['product_id']		= $productId;
							$data['price']			= $productPrice->product_price;
							$data['product_name']	= $productPrice->product_name;
							$data['quantity']		= $_SESSION['cart'][$i]['qty'];
							$data['user_id']		= $user_id;
							$data['color']			= $_SESSION['cart'][$i]['productColor'];
							$data['size']			= $_SESSION['cart'][$i]['productSize'];
							$data['order_id']		= $order_id;
							
							$cartDetail				= $this->cart_model->get_to_cart($productId,$sessionId,$user_id, $order_id,$productColor,$productSize);

							if(!$cartDetail){
								
								$lastInsertedId = $this->cart_model->insert_to_cart($data);
								$_SESSION['cart'][$i]['cartId']		= $lastInsertedId;
								//echo $i. $_SESSION['Check']."<br>";
							}
						}
					}


					if($xml->ResponseCode  == "Z6")
					{
						$this->order_model->update_order(array( 'status' => 'Canceled', 'response_code' => $xml->ResponseCode, 'transaction_ref' => $transactionreference), array('order_id'=>$orderId));
					}
					else if($xml->ResponseCode  == "77" || $xml->ResponseCode  == "78")
					{
						$this->order_model->update_order(array( 'status' => 'Pending', 'response_code' => $xml->ResponseCode, 'transaction_ref' => $transactionreference), array('order_id'=>$orderId));
					} 
					else
					{
						$this->order_model->update_order(array( 'status' => 'Failed', 'response_code' => $xml->ResponseCode, 'transaction_ref' => $transactionreference), array('order_id'=>$orderId));
					}
					$message = "";
					$message = '
					<!DOCTYPE html>
					<html>
					<head lang="en">
					    <meta charset="UTF-8">
					    <title></title>
					</head>
					<body style="background-color: #f3f3f3; padding: 0; margin: 0;font-family: \'Calibri\', Arial, sans-serif;">
					<div class="outer-div" style="margin: 0 auto; width:600px;background-color: #fff;">
					    <table width="100%">
					        <tr>
					            <td colspan="6">
					                <div style="background-color: #00587C;
					                        width:100%; height:100px; border-bottom: solid 3px #9AC434; padding:20px 0">
					                    <table>
					                        <tr>
					                            <td width="5%"></td>
					                            <td width="60%">
					                                <h1 class="responseDescription" style="color:#fff; font-size:36px; font-weight: normal">'.$xml->ResponseDescription.'</h1>
					                            </td>
					                            <td width="30%">
					                                <img src="'.base_url().'assets/img/logo.png" />
					                            </td>
					                            <td width="5%"></td>
					                        </tr>
					                    </table>
					                </div>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <p>
					                    Dear <strong> Customer</strong>,<br /><br />';
					            if($xml->ResponseCode  == "77" || $xml->ResponseCode  == "78")
								{
									$message .= '<strong style="color:red;">Your transaction has pending.';
								}
								else
								{
									$message .= '<strong style="color:red;">Your transaction has failed.';
								}        
					            
					            $message .= 'Reason: '.$xml->ResponseDescription.' .</strong>
					                    <br />Please try again!
					                </p>
					            </td>
					        </tr>

				        	<tr>
					            <td colspan="" style="padding:10px 20px;">
					                Transaction Refrence:
					            </td>

					            <td colspan="" style="padding:10px 20px;">MM'.
					               $transactionreference.'
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					               
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <p style="text-align: center">
					                    For more information, call 0818 778 2542, 0818 778 2542 or send an email to <a href="mailto:operator@mymall.com.ng" target="_top">operator@mymall.com.ng</a>
					                </p>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px;">
					                <table width="100%">
					                    <tr>
					                        <td width="20%"><img src="'.base_url().'assets/img/ecobank.png" /></td>
					                        <td width="20%"></td>
					                        <td width="20%"></td>
					                        <td width="20%"><img src="'.base_url().'assets/img/fedex.png" height="25px" width="auto" /></td>
					                        <td width="20%"><img src="'.base_url().'assets/img/webmall.jpg" height="25px" width="auto" /></td>
					                    </tr>
					                </table>
					            </td>
					        </tr>

					        <tr>
					            <td colspan="6" style="padding:10px 20px; border-top: solid 1px #ccc;">
					                <table width="100%">
					                    <tr>
					                        <td width="60%">
					                            <p style="font-size:13px; color: #333;line-height: 30px">
					                                &copy; 2015, All rights reserved. Mymall
					                            </p>
					                        </td>

					                        <td width="40%">
					                            <img src="'.base_url().'assets/img/youtube.jpg" style="float:right; margin-left:5px;" />
					                            <img src="'.base_url().'assets/img/linkin.jpg" style="float:right; margin-left: 5px;" />
					                            <a target="_blank" href="https://twitter.com/MymallNg"><img src="'.base_url().'assets/img/twitter.jpg" style="float:right; margin-left: 5px;" /></a>
					                            <a target="_blank" href="https://www.facebook.com/MymallNg"><img src="'.base_url().'assets/img/facebook.jpg" style="float:right; margin-left: 5px;" /></a>
					                        </td>
					                    </tr>
					                </table>
					            </td>
					        </tr>
					    </table>
					</div>
					</body>
					</html>
				   ';

				    $_SESSION['orderMessage'] = $message;
					
					$message = ($message);
					
					$this->load->library('email');
					
					$config['mailtype'] = "html";
					
					$this->email->initialize($config);
					
					$this->email->from(AdminEmail, AdminEmailName);

					//$this->email->from("test@geeksperhour.com", "Mymall");
									
					$this->email->to($shippingDetail->email);
					//$this->email->to("parmeet@geeksperhour.com", "Mymall");
		
					$this->email->subject('Order on mymall - Online Store Solution');
					
					$this->email->message($message);
					
					$this->email->send();

					if($xml->ResponseCode  == "Z6")
					{
						header("Location: ".base_url()."order/canceled");
						die();
					}
					else if($xml->ResponseCode  == "77" || $xml->ResponseCode  == "78")
					{
						header("Location: ".base_url()."order/pending");
						die();
					}
					else
					{
						header("Location: ".base_url()."order/failed");
						die();
					}

				}


			}
			else
			{
				header("Location: ".base_url()."home");
				die();

			}


		}


		


	}
	


	public function success_pay_on_delivery(){
		
		//PAY ON DELIVERY
		if($this->input->post('paymentType') && $this->input->post('orderId') && $this->input->post('paymentType') == 'PAY_ON_DELIVERY' && $this->input->post('orderId') != '')
		{
			
			$orderId			= $this->input->post('orderId');
			/*$user_id			= '';
			if(isset($_SESSION['userDetail']) && !empty($_SESSION['userDetail'])){
				$userId  = $_SESSION['userDetail']->id;
			}*/
			
			if($this->order_model->update_order(array('payment_type' => 'Pay on Delivery'), array('order_id'=>$orderId, 'status' => 'Pending')))
			//$this->cart_model->update_cart_product(array('payment' => 'Pay on Delivery'), array('order_id'=>$orderId, 'payment_status' => 'Pending'));
			{
				
				$productDetail	= $this->cart_model->get_all_items_cart('', $orderId);
				$shippingDetail	= $this->user_model->single_shipping_detail(array('tranx_id'=>$orderId));
				$messg 		=	'';				
				$messg.='
				<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#F6F6F6">
				  <tr>
					<td height="248" colspan="17" valign="top" style="background-image:url('.base_url().'email-images/background_image.jpg); background-repeat:none; border-bottom:3px solid #034679; "><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<!--DWLayoutTable-->
						<tr>
						  <td width="21" height="18" ></td>
						  <td width="149" ></td>
						  <td width="33" ></td>
						  <td width="188" ></td>
						  <td width="209" ></td>
						</tr>
						<tr>
						  <td height="77" ></td>
						  <td valign="top" ><a href="#"><img src="'.base_url().'assets/img/logo.png" width="144" height="76" border="none" /></a></td>
						  <td ></td>
						  <td ></td>
						  <td ></td>
						</tr>
						<tr>
						  <td height="13" ></td>
						  <td ></td>
						  <td ></td>
						  <td ></td>
						  <td ></td>
						</tr>
						<tr>
						  <td height="45" colspan="5" valign="top" style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; color:#fff; font-weight:bolder; font-size:38px; text-shadow:#000; text-align:center;" >TRANSACTION COMPLETED</td>
						</tr>
						<tr>
						  <td height="22" colspan="5" valign="top" style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; color:#fff; font-weight:bolder; font-size:14px; text-shadow:#000; text-align:center;" >get more discount everytime you make a purchase on Ecomall</td>
						</tr>
						<tr>
						  <td height="13" ></td>
						  <td ></td>
						  <td ></td>
						  <td ></td>
						  <td ></td>
						</tr>
						<tr>
						  <td height="43" ></td>
						  <td ></td>
						  <td ></td>
						  <td valign="middle" style="background-color:#F7821B; border-bottom-left-radius:3px; border-bottom-right-radius:3px; border-top-left-radius:3px; border-top-right-radius:3px; font-family:arial rounded MT,Arial, Helvetica, sans-serif; color:#fff; font-weight:bolder; font-size:18px; text-align:center;" ><a href="<?php echo base_url();?>" style="text-decoration:none; color:#fff;">BUY MORE</a></td>
						  <td ></td>
						</tr>
						<tr>
						  <td height="14" ></td>
						  <td ></td>
						  <td ></td>
						  <td ></td>
						  <td ></td>
						</tr>
					  </table></td>
				  </tr>
				  <tr>
					<td width="18" height="16"></td>
					<td width="13"></td>
					<td width="29"></td>
					<td width="61"></td>
					<td width="21"></td>
					<td width="14"></td>
					<td width="42"></td>
					<td width="72"></td>
					<td width="14"></td>
					<td width="42"></td>
					<td width="85"></td>
					<td width="26"></td>
					<td width="6"></td>
					<td width="36"></td>
					<td width="55"></td>
					<td width="52"></td>
					<td width="14"></td>
				  </tr>
				  <tr>
					<td height="96"></td>
					<td></td>
					<td colspan="13" valign="top" style="font-family:script MT bold,Arial, Helvetica, sans-serif; color:#000; font-size:27px;">Dear Customer,<br />
					  <p style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; font-size:16px; color:#000; line-height:0.6cm;">You have completed an order on Ecomall. You can expect your product(s) delivered within 2-3 working days.</p></td>
					<td></td>
					<td></td>
				  </tr>
				  <tr>
					<td height="20">&nbsp;</td>
					<td>&nbsp;</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				  </tr>
				  <tr>
					<td height="">&nbsp;</td>
					<td colspan="15" valign="top" bgcolor="#034679"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
						  <td valign="middle" style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; font-size:16px; color:#fff; font-weight:bold; padding-left:20px" height="55px">Delivery Information</td>
						</tr>
						';
						if($shippingDetail)
						{
						
						$messg .= '
						<tr>
						  <td colspan="0" valign="top" bgcolor="#EFEFEF"><table width="100%" border="0" cellpadding="10" cellspacing="0" style="border-collapse: unset; margin-left: 20px;">
							  <tr>
							   <td width="40%"><strong>Address:</strong></td>
							   <td width="60%">'.$shippingDetail->address1.'</td>
							   </tr>
							  <tr>
							   <td><strong>TEL:</strong></td>
							   <td>'.$shippingDetail->phone.'</td>
							   </tr>
							  <tr>
							   <td><strong>Deliveryarea:</strong></td>
							   <td>'.$shippingDetail->shipping_place.'</td>
							   </tr>
							  <tr>
							   <td><strong>Contact number for delivery:</strong></td>
							   <td>'.$shippingDetail->contact_no_delivery.'</td>
							   </tr>
							  </table>
							  </td>
							  </tr>
							  ';
							  }
							  $messg .= '
							   </table>
							  </td>
							  </tr>';
							  
							  $messg .= '
							  <tr>
								<td height="">&nbsp;</td>
								<td colspan="15" valign="top" bgcolor="#034679"><table width="100%" border="0" cellpadding="0" cellspacing="0">
									<tr>
									  <td valign="middle" style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; font-size:16px; color:#fff; font-weight:bold; padding-left:20px" height="55px">Product(s) Information</td>
									</tr>
									<tr>
									  <td colspan="0" valign="top" bgcolor="#EFEFEF"><table width="100%" border="0" cellpadding="10" cellspacing="10" style="border-collapse: unset;">
									  <tr style="color: #fff;background-color: #ccc;">
										<td style="padding-left: 15px">ITEM</td>
										<td></td>
										<td>ITEM PRICE</td>
										<td>QUANTITY</td>
										<td>TOTAL</td>
										<td>&nbsp;</td>
									  </tr>';
							  
							  
							  $grandTotal 		= 0;
							  $merStore			= '';
							  if($productDetail)
							  {
							  $productImg 	= base_url().'assets/img/logo.png';
							  foreach($productDetail as $rows)
							  {
							  $productImg  = base_url().'product_images/'.$rows->product_img;
							  $grandTotal = $grandTotal + ($rows->price * $rows->quantity);
							  // get  merchant store name
							  if($merStore != '')
							  $merStore   .= ',  ';
							  $merStore   .= $rows->first_name; 
							  
							  
							  $messg .= '
							  <tr>
								<td style="padding:10px" valign="top"><img src="'.$productImg.'" width="78" height="67" border="none" /></td>
								<td width="47%" style="padding: 10px;"><p style="margin: 0;">'.$rows->product_name.'</p></td>
								<td width="12%"><p style="margin: 0; color: darkred">&#x20A6;'.number_format($rows->product_price).'</p></td>
								<td style="padding:10px" width="10%">'.$rows->quantity.'</td>
								 <td style="padding:10px" width="12%"><p style="margin: 0; color: darkred">&#x20A6;'.number_format($rows->price * $rows->quantity).'</p></td>
							  </tr>
						';
						}
						}
						$grandTotal =   $grandTotal + $shippingDetail->shipping;
						$messg .= '
					  </table></td></tr> 
					  </table>
						</td>
					 </tr>

					<td>&nbsp;</td>
				  </tr>
				  <tr>
					<td height="197" colspan="17" valign="top"  style="background-color:#E2E2E2; border-bottom:3px solid #fff; border-top:3px solid #fff;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
						  <td width="25" height="16"></td>
						  <td width="188"></td>
						  <td width="387"></td>
						</tr>
						<tr>
						  <td height="33"></td>
						  <td valign="middle"  style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; font-size:16px; color:#000; font-weight:bold;">Transaction ID:</td>
						  <td valign="middle" style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; font-size:16px; color:#000;">'. $shippingDetail->tranx_id.'</td>
						</tr>
						<tr>
						  <td height="35"></td>
						  <td valign="middle"  style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; font-size:16px; color:#000; font-weight:bold;">Payment Option:</td>
						  <td valign="middle" style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; font-size:16px; color:#000;">PAY ON DELIVERY</td>
						</tr>
						<tr>
						  <td height="40"></td>
						  <td valign="middle"  style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; font-size:16px; color:#000; font-weight:bold;">Total Amount:</td>
						  <td valign="middle" style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; font-size:16px; color:#000;">&#x20A6; '.number_format($grandTotal).'</td>
						</tr>
						<tr>
						  <td height="32"></td>
						  <td valign="middle"  style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; font-size:16px; color:#000; font-weight:bold;">Merchants Store: </td>
						  <td valign="middle" style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; font-size:16px; color:#000;">'.$merStore.'</td>
						</tr>
						<tr>
						  <td height="35"></td>
						  <td></td>
						  <td>&nbsp;</td>
						</tr>
					  </table></td>
				  </tr>
				  <tr>
					<td height="17"></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				  </tr>
				  <tr>
					<td height="17"></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				  </tr>
				  <tr>
					<td height="67" colspan="17" valign="top" bgcolor="#C8C8C8" style="border-bottom:1px solid #333; border-top:1px solid #333;"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
						  <td width="21" height="52">&nbsp;</td>
						  <td width="116" valign="middle" style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; font-size:12px; color:#000; line-height:0.7cm; font-weight:bold;"><a href="#" style="text-decoration:none; color:#F7821B;">ELECTRONICS</a></td>
						  <td width="15">&nbsp;</td>
						  <td width="107" valign="middle" style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; font-size:12px; color:#F7821B; line-height:0.7cm; font-weight:bold;"><a href="#" style="text-decoration:none; color:#F7821B;">FOOTWEAR</a></td>
						  <td width="13">&nbsp;</td>
						  <td width="235" valign="middle" style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; font-size:12px; color:#F7821B; line-height:0.7cm; font-weight:bold;"><a href="#" style="text-decoration:none; color:#F7821B;">JEWELRIES & WRISTWATCHES</a></td>
						  <td width="10">&nbsp;</td>
						  <td width="83" valign="middle" style="font-family:arial rounded MT,Arial, Helvetica, sans-serif; font-size:12px; color:#F7821B; line-height:0.7cm; font-weight:bold;"><a href="#" style="text-decoration:none; color:#F7821B;">MENS</a></td>
						</tr>
						<tr>
						  <td height="13"></td>
						  <td></td>
						  <td></td>
						  <td></td>
						  <td></td>
						  <td></td>
						  <td></td>
						  <td></td>
						</tr>
					  </table></td>
				  </tr>
				  <tr>
					<td height="40">&nbsp;</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				  </tr>
				  <tr>
					<td height="56" colspan="17" valign="top" bgcolor="#32313A"><table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
						  <td width="24" height="20">&nbsp;</td>
						  <td width="507">&nbsp;</td>
						  <td width="69">&nbsp;</td>
						</tr>
						<tr>
						  <td height="30"></td>
						  <td valign="top" style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#fff;">© 2015. Mymall. ALL RIGHTS RESERVED.</td>
						  <td></td>
						</tr>
						<tr>
						  <td height="15"></td>
						  <td></td>
						  <td></td>
						</tr>
					  </table></td>
				  </tr>
				</table>
				';				
				
				$data['html'] = $messg;
				if(isset($_SESSION['cart']) && !empty($_SESSION['cart']))
				{
				
					$msg  =( "<html>\n");
					
					$msg .=( "<head>\n");
										
					$msg .=( "</head>\n");
					
					$msg .=( "<body>\n");
					
					$msg .= ($messg);
					
					$msg .=( "</body>\r\n");
					
					$msg .=( "</html>\r\n");
					
					$this->load->library('email');
					
					$config['mailtype'] = "html";
					
					$this->email->initialize($config);
					
					$this->email->from(AdminEmail, AdminEmailName);
					
					$this->email->to($shippingDetail->email);
					
					$this->email->subject('Your Order is pending on Ecobank WebMall - Online Store Solution');
					
					$this->email->message($msg);
				
					$this->email->send();
					
					unset($_SESSION['cart']);
					unset($_SESSION['Check']);				
					
				}
				
				$this->layout->view('sucess_pay_on_delivery',$data);	
				
			}
			
		
		}
		else{
			header("Location: home");
			die();
		}
		
	} // end of function
	
	public function fail_pay_on_delivery()
	{
		
		echo 'transaction fail......';
		
	}
	
	public function courier()
	{
		
		$orderId 		= $_SESSION['Check'];
		$productDetail	=  $orderDetail   =  $shippingDetail = "";

		$orderDetail    = $this->order_model->single_order_detail(array('order_id' => $orderId));
		$cartDetail	= $this->cart_model->get_all_items_cart('', $orderId);
		if($orderDetail && $orderDetail->shipping_add_id != '')
		{
			$shippingDetail	= $this->user_model->get_shipping_address(array('id'=>$orderDetail->shipping_add_id)); 
		}
		

		if($cartDetail && !empty($cartDetail))
		{
			foreach ($cartDetail as $row) {
			//print_r($data);
				if($row->product_img != "")	
				$imagePath = base_url()."product_images/".$row->product_img;
				else
				$imagePath = base_url()."assets/img/logo.png";
					
				/********  get size, color and qty ***********/

				$productSize          = "";
				$productColor         = "";
				$productdetail        = "";

				if($row->color != '' && $row->size != '' )
				{

				    $productdetail     = $this->product_model->relate_product_size_colour(array('products.id'=>$row->product_id,'relation_color_size.colour_id'=>$row->color,'relation_color_size.size_id'=>$row->size));

				    //print_r($productdetail);
				    if($productdetail && $productdetail != '')
				    {

				      foreach ($productdetail as $prodDeatil) 
				      {

				          if($prodDeatil->color && $prodDeatil->color != '')
				          {
				              $productColor   = $prodDeatil->color;

				          }
				          if($prodDeatil->size && $prodDeatil->size != '')
				          {
				              $productSize   = $prodDeatil->size;

				          }

				      }

				    }

				}
				else if($row->size != '' )
				{

				  $productdetail     = $this->product_model->relate_product_size(array('relation_color_size.size_id'=>$row->size ,'products.id'=>$row->product_id )); 
				  //print_r($productdetail);
				  
				  if($productdetail && $productdetail != '')
				  {
				  
				    foreach ($productdetail as $prodDeatil) 
				    {

				     if($prodDeatil->size && $prodDeatil->size != '')
				      {
				          $productSize   = $prodDeatil->size;

				      }

				    }

				  }

				}
				else if($row->color  != '' )
				{

				  $productdetail     = $this->product_model->relate_product_colour(array('relation_color_size.colour_id'=> $row->color,'products.id'=>$row->product_id )); 

				  if($productdetail && $productdetail != '')
				  {
				  
				    foreach ($productdetail as $prodDeatil) 
				    {

				      if($prodDeatil->color && $prodDeatil->color != '')
				      {
				          $productColor   = $prodDeatil->color;

				      }

				    }

				  }

				}			
				//set POST variables
				// pickup means merchent
			 	$apiArray = array(	'transaction_id' 		=>	$orderId,
									'client_id' 			=>	"nj8kbrt",
									'courier_id' 			=>	"ksixga9",
									'pickup_address' 		=>	$row->merchant_add,
									'pickup_location' 		=>	$row->merchant_deliveryarea,
									'pickup_contactname' 	=>	$row->display_name,
									'pickup_contactnumber' 	=>	$row->merchant_phone,
									'pickup_contactemail' 	=>	$row->merchant_email,
									'delivery_address' 		=>	$shippingDetail->delivery_addres,
									'delivery_location' 	=>	$shippingDetail->delivery_area,
									'delivery_contactname' 	=>	$shippingDetail->email,
									'delivery_contactnumber'=>	$shippingDetail->phone,
									'delivery_contactemail' =>	$shippingDetail->email,
									'item_name' 			=>	$row->product_name,
									'item_size' 			=>	$productSize,
									'item_color' 			=>	$productColor,
									'item_quantity' 		=>	$row->quantity,
									'image_location' 		=>	$imagePath,
									'fragile' 				=>	0,
									'perishable' 			=>	0,
									'POD' 					=>	0,
									'delivery_cost' 		=>	$orderDetail->delivery_amt,
									'status' 				=> 	0
								); 

				//url-ify the data for the POST
				$url 			= "http://webmallnglab.com/courier_service/v1/delivery";
				$strData 		= json_encode($apiArray);
				//$result 		= sendPostData($url, $strData);

				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS,$strData);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
				$result = curl_exec($ch);
				curl_close($ch);  // Seems like good practice
				
				echo $result;
				$res 			= json_decode($result);
				$delivery_id 	= '';
				if(!empty($res->delivery_id))
				{
					$delivery_id = $res->delivery_id;
					$this->order_model->update_order(array('delivery_id' => $delivery_id),  array('order_id' => $orderId));

				}

			} // end of foreach..

		} // end of if...
		
	} // end of courier function...
	
	
}
