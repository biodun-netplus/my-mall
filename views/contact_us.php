<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="background-white">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <h3 class="page-header">Contact Us</h3>
        <form id="Contactus">
        <div style="clear: both; margin-bottom: 20px"></div>
        <input type="text" id="txtName" class="form-control contact-text" placeholder="Last Name, Other Names">
        <input type="text" id="txtEmail" class="form-control contact-text" placeholder="Email Address">
        <input type="text" id="numPnone" class="form-control contact-text" placeholder="Phone Number">
        <input type="text" id="txtComments" class="form-control contact-text" placeholder="Comments">
        <button style="height: 50px" type="button" name="login" class="btn bg-contact btn-block">Submit</button>
      </form>
      </div>
      <div class="col-lg-4">
        <div class="contactNo"><img src="<?php echo base_url();?>assets/img/Phone_number.png">&nbsp;Contact No:&nbsp;&nbsp;08024762146</div>
     </div>
    </div>
  </div>
</section>
