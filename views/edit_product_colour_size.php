<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('merchant-head'); 
?>

<section class="background-white">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header1 col-lg-10"> Edit Product Size And Colour</h3>
        <div class="col-lg-2" style="text-align: right; vertical-align: bottom; margin-top: 2em;"><a href="<?php echo base_url();?>relate-product-size-colour/<?php echo base64_encode($productDetail->product_id);?>">Back</a></div>
     </div>
    <div class="row">
      <div class="col-lg-12">
      <?php
  		if(isset($msg) && $msg != '')
  		{
  		?>
          <div class="alert alert-success">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
            <strong><?php echo $msg;?></strong> </div>
          <?php
              }
              else if(isset($error_msg) && $error_msg != '')
              {
              ?>
          <div class="alert alert-danger">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
            <strong><?php echo $error_msg;?></strong> </div>
          <?php
              }
              ?>
      </div>
    </div>
    <div class="border ">
      <form role="form" action="<?php echo base_url();?>edit-product-size-colour/<?php echo base64_encode($productDetail->id);?>" id="edit-product-size-colour" method="post" enctype="multipart/form-data">
        <div class="col-lg-6">
        <?php 
         if($productSize)
         {
		 ?>
          <div class="form-group">
            <label>Size</label>
            <select class="form-control validate[required]" name="txtSize" id="txtSize">
              <option value="">Size</option>
              <?php 
                foreach($productSize as $row)
                {
                  
               ?>
                      <option value="<?php echo $row->id; ?>" <?php if($productDetail->size_id == $row->id) {?> selected="selected" <?php } ?> ><?php echo $row->size; ?></option>
                      <?php
                }
               ?>
            </select>
          </div>
		<?php
		 }
		 if($productColour)
         {
		 ?>
          <div class="form-group">
            <label>Colour</label>
            <select class="form-control validate[required]" name="txtColour" id="txtColour">
              <option value="">Colour</option>
              <?php 
                foreach($productColour as $row)
                {
                  
               ?>
                      <option value="<?php echo $row->id; ?>" <?php if($productDetail->colour_id == $row->id) {?> selected="selected" <?php } ?> ><?php echo $row->color; ?></option>
                      <?php
                }
               ?>
            </select>
          </div>
          <?php
		  }
		  ?>
          <div class="form-group">
            <label>Quantity</label>
            <input type="text" value="<?php echo $productDetail->qty;?>" id="txtqty" name="txtqty" class="form-control validate[required,custom[number]]">
          </div>
        </div>
        <div class="col-lg-12">
          <button type="submit" class="btn btn-default" value="Submit" name="submit">Submit</button>
          <button type="reset" class="btn btn-default">Reset</button>
        </div>
      </form>
    </div>
  </div>
</section>
