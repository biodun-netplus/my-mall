<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('merchant-head'); 
?>

<section class="background-white">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header1">Banners</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <?php
		if(isset($_SESSION['message']) && $_SESSION['message'] != '')
		{
		?>
        <div class="alert alert-success">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
          <strong><?php echo $_SESSION['message']; unset($_SESSION['message']);?></strong> </div>
        <?php
            }
            else if(isset($_SESSION['errorMessage']) && $_SESSION['errorMessage'] != '')
            {
            ?>
        <div class="alert alert-danger">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
          <strong><?php echo $_SESSION['errorMessage']; unset($_SESSION['errorMessage']);?></strong> </div>
        <?php
            }
            ?>
      </div>
    </div>
    <div class="border1">
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
		  if($merchantBanner && $merchantBanner != '' && $merchantBanner->header_banner != '' )
		  {
			  $arr = explode(',',$merchantBanner->header_banner); 
			  $countRow = 0;
			  foreach($arr as $rows)
			  {
				   $countRow ++;
			 ?>
            <tr>
              <th scope="row"><?php echo $countRow;?></th>
              <td><?php
			  	  $parameter	= '';
				  if(strpos($rows, 'youtube.com/') === false )
				  {
					  
					  $parameter = $rows;
				  ?>
                <img src="<?php echo base_url().'banner_images/'.$rows ;?>" width="150" height="100">
                <?php
				  }
				  else
				  {
					  $youtube_thumb1 = explode('?v=',$rows);
					  $youtube_thumbnail = explode('&',$youtube_thumb1[1]);
					  $parameter = 'url_'.$youtube_thumbnail[0];
				  ?>
                <img src="http://img.youtube.com/vi/<?php echo $youtube_thumbnail[0];?>/2.jpg">
                <?php
				  }
				  ?></td>
              <td><a onclick="return confirm('Are you sure, you want to delete this record?')"  href="<?php echo base_url()?>banner/delete/<?php echo base64_encode($parameter);?>"><span aria-hidden="true" class="glyphicon glyphicon-remove"></span></a></td>
              </tr>
			  <?php
		 
			  }
		  }
		  else
		  {
		  ?>
          <tr>
          <td colspan="3"><div style="margin-bottom:10px" class="no-record text-center">No Record Found!</div></td>
          </tr>
          <?php
		  }
		  ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
