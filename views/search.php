<div id="search_title">
    <div class="container">
        <div class="result">
            <b><?php if($search && $search  != ''){ echo number_format(count($search));} else echo "0"; ?> Search Results for <?php if(isset($_GET['keyword'])){ echo $_GET['keyword'];}?>
        </b></div>

        <!--<div class="related">
            Related Search Results
        </div>-->
    </div>
</div>
<div id="main_content">
    <div class="container">


        <div class="search_cont">
          <div class="product_area_wrap">
          <?php
//              var_dump($search);
          if($search && $search  != '')
          {
              $searchImg = base_url().'assets/img/logo.png';
              foreach($search as $rows)
              {
                 if($rows->image != '')
                 $searchImg = base_url().'product_images/'.$rows->image;
              ?>
                <div class="product_wrap">
                  <a href="<?php echo base_url().'product/'.base64_encode($rows->id);?>">
                    <div class="image"><img src="<?php echo $searchImg;?>"></div>
                    <div class="details">
                        <table width="100%">
                            <tr>
                                <td colspan="2" style="color: #ffffff;height:42px;vertical-align: top;">
                            
                                <?php 
	                        if($rows->product_name != '')
	                        {
		                   if(strlen($rows->product_name) > 40)
		                   {
						   
		                      echo substr($rows->product_name, 0,  (36 - strlen($rows->product_name)))."...." ; 
		                   }
		                   else
		                   echo $rows->product_name;
	                        }
	                        ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #A8C738; font-size: 22px; font-weight: 600;">&#x20A6;<?php echo number_format($rows->product_price);?></td>
                                <td><input type="button" class="btn-block btn btn-cart" value="add to cart"></td>
                            </tr>
                        </table>
                    </div>
                    </a>
                </div>
              <?php
                }
              }
              else
              {

                echo '<div class="no-record text-center">No Record Found!</div>';
              }
              ?>
             <br class="clearfix" />
            </div>
            <br class="clearfix" />
        </div>
        <br class="clearfix" />
    </div>
</div>