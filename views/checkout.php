<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->load->model('product_model');
		$this->load->model('cart_model');
		
	}
	
	public function index(){
		$data   			= array();
		$userId				= '';
		$orderId			= '';	
		
		if(isset($_SESSION['cart'])  && $_SESSION['cart'])
		{
			if(!isset($_SESSION['Check'])) 
			{ 
				 $_SESSION['Check'] = "55".$this->generateOrder(10);
			}
			else
			{
				 $orderId = $_SESSION['Check']  ;
			}
			
			if(isset($_SESSION['userDetail']) && !empty($_SESSION['userDetail'])){
				$userId  = $_SESSION['userDetail']->id;
			}
			
			/*** update cart status ***/
			$this->cart_model->update_cart($userId, $orderId);
			
			$data['cartDetail']	= $this->cart_model->get_all_items_cart($userId, $orderId);
			
			$this->layout->view('cart',$data);	
		}
		else{
			header("Location: home");
			die();
		}
		
	}
	
	public function generateOrder($length)
	{
		$code = "";
		$possible = "012346789";
		$maxlength = strlen($possible);
		if ($length > $maxlength) {
		$length = $maxlength;
		}
		$i = 0; 
		while ($i < $length) { 
		$char = substr($possible, mt_rand(0, $maxlength-1), 1);
		if (!strstr($code, $char)) { 
		$code .= $char;
		$i++;
		}
		}
		return $code;
	}
	
}
