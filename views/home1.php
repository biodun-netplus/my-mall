<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Header -->
<header>

<!--    <div id="banner" class="slider-wrapper theme-default">-->
        <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height: 350px; overflow: hidden; visibility: hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                <div style="position:absolute;display:block;background:url('<?php echo base_url() ?>assets/new/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
            </div>
            <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1300px; height: 350px; overflow: hidden;">

                <div data-p="225.00" style="display: none;">
                    <img data-u="image" src="<?php echo base_url() ?>assets/new/img/banner1.png" />
                </div>
                <div data-p="225.00" style="display: none;">
                    <img data-u="image" src="<?php echo base_url() ?>assets/new/img/banner1.png" />
                </div>
                <div data-p="225.00" style="display: none;">
                    <img data-u="image" src="<?php echo base_url() ?>assets/new/img/banner1.png" />
                </div>
            </div>
            <!-- Bullet Navigator -->
            <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
                <!-- bullet navigator item prototype -->
                <div data-u="prototype" style="width:16px;height:16px;"></div>
            </div>
            <!-- Arrow Navigator -->
            <!-- <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
                       <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span> -->

        </div>
<!--       </div>-->

</header>

<div class="header-shadow"></div>

<!-- Portfolio Grid Section -->
<section id="portfolio">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2>Meet Our Merchants</h2>
      </div>
    </div>
  </div>
</section>

<section class="merchants">
    <div class="container">

        <div class="merchant_box_large">
            <a href="<?php echo base_url().$ourMerchant['slug'];?>">
            <div class="merchant_box_circle" style="background-image:url(<?php echo $ourMerchant['merchantImg'];?>)"></div>
            </a>

            <div class="merchant_box" style="width: 250px;">
                <table width="100%" style="color: #000000; margin-left:7px; ">
                  <tr>
                    <td style="height: 20px"></td>
                  </tr>
                  <tr>
                    <td style="font-size: 20px">
                        <?php echo $ourMerchant['displayName'];?> <br/>
                        <b><?php echo $ourMerchant['categoryName'];?></b>
                    </td>
                  </tr>
                  <tr>
                    <td style="height: 20px"></td>
                  </tr>
                  <tr>
                    <td>
                        <a href="<?php echo base_url().$ourMerchant['slug'];?>" style="color:#9AC434; text-decoration: none;">Visit Store</a>
                    </td>
                  </tr>
                </table>
            </div>

        </div>

        <?php
        if(isset($ourMerchant['product']) && !empty($ourMerchant['product']))
        {
            $productImg = base_url().'assets/img/logo.png';
            foreach($ourMerchant['product']  as $rows)
            {
                $productImg = base_url().'product_images/'.$rows->image;

        ?>
        <div class="merchant_box">
          <div class="inner">
              <a href="<?php echo base_url().'product/'.base64_encode($rows->id);?>">
                  <img src="<?php echo $productImg?>" style="width: 140px; height: 140px;">
              </a>
          </div>
        </div>
        <?php
            }
        }
        ?>

        <div class="merchant_box">
          <a href="<?php echo base_url().$ourMerchant['slug'];?>"  style="color: rgb(255, 255, 255);">
            <div class="count"><?php echo $ourMerchant['totalProduct'];?> <br/>
            items</div>
          </a>
        </div>

        <br class="clearfix" />
    </div>
</section>

<div class="line"></div>
<div class="line"></div>
<div class="line"></div>
<div class="line"></div>

<section class="browse">
  <ul class="nav nav-tabs nav-divider">
    <li><a data-toggle="tab" href="#sectionA">Shop by Merchants</a></li>
    <li class="active"><a data-toggle="tab" href="#sectionB">Shop by Products</a></li>
  </ul>
  <div class="tab-content">
    <div id="sectionA" class="tab-pane fade">
      <div class="container">
        <div class="owl-carousel-merchant">
          <div class="owl-carousel2">
            <?php
      			if($shopMerchants && $shopMerchants  != '')
      			{
      				$count_div = 1;
      				$total_merchant = count($shopMerchants);
      				$count_row =  1;
      				foreach($shopMerchants as $rows)
      				{
      					 $merchantLogo = '';
      					 if($rows->image != '' && file_exists('./banner_images/'.$rows->image))
      					 $merchantLogo = 'banner_images/'.$rows->image;
      					 else
      					 $merchantLogo = 'assets/img/newmerchant.jpg';

      				if($count_div == 1)
      				{
      		   ?>
            <div class="items">
              <div class="browse-box"> <a href="<?php echo base_url().$rows->slug;?>"><img src="<?php echo base_url().$merchantLogo;?>" width="200" height="200"></a> </div>
              <?php
			 // close (last) even div...
  			    if (($total_merchant % 2 == 1) && $total_merchant == $count_row) {
  				?>
              </div>
              <?php
  				}

				}// end of if...

			  if($count_div == 2)
			  {
			  ?>
            <div class="browse-box"> <a href="<?php echo base_url().$rows->slug;?>"><img src="<?php echo base_url().$merchantLogo;?>" width="200" height="200"></a> </div>
          </div>
          <?php
			    $count_div = 0;
			  } // end of if...
			  $count_div ++;
			  $count_row ++;
				}
			}
    		?>
        </div>
        <div class="customNavigation"> <a id="merchant-prev" class="prev">&gt;</a> <a id="merchant-next" class="next">&lt;</a> </div>
      </div>
    </div>
  </div>

  <div id="sectionB" class="tab-pane fade in active">
    <div class="container">
      <div class="container">
        <div class="owl-carousel-merchant">
          <div class="owl-carousel1">
            <?php
      			if($products && $products  != '')
      			{
      				$count_div = 1;
      				$total_products = count($products);
      				$count_row =  1;
      				foreach($products as $rows)
      				{
      					 $productsImg = '';
      					 if($rows->image != '')
      					 $productsImg = 'product_images/'.$rows->image;
      					 else
      					 $productsImg = 'assets/img/logo.png';

      				if($count_div == 1)
      				{
      		   ?>
            <div class="items">
              <div class="product-box">
                <a href="<?php echo base_url().'product/'.base64_encode($rows->id);?>">
                <div class="image"><img height="200" src="<?php echo base_url().$productsImg;?>"></div>
                <div class="details">
                  <table width="100%">
                    <tr>
                      <td colspan="2" style="color: #ffffff;height: 42px;vertical-align: top;">
                      <?php
						if($rows->product_name != '')
						{
						  if(strlen($rows->product_name) > 36)
						  {

						    echo substr($rows->product_name, 0,  (32 - strlen($rows->product_name)))."...." ;

						  }
						  else
						  echo $rows->product_name;
						}
						?>
                      </td>
                    </tr>
                    <tr>
                      <td style="color: #A8C738; font-size: 22px; font-weight: 600;">&#x20A6;<?php echo number_format($rows->product_price); ?></td>
                      <td><input type="button" class="btn-block btn btn-cart" value="add to cart"></td>
                    </tr>
                  </table>
                </div>
                </a>
              </div>
              <?php
      			 // close (last) even div...
      			    if (($total_products % 2 == 1) && $total_products == $count_row) {
      				?>
                  </div>
                  <?php
      				}

      				}// end of if...

      			  if($count_div == 2)
      			  {
      			  ?>
              <div class="product-box">
                  <a href="<?php echo base_url().'product/'.base64_encode($rows->id);?>">
                  <div class="image"><img height="200" src="<?php echo base_url().$productsImg;?>"></div>
                  <div class="details">
                    <table width="100%">
                      <tr>
                        <td colspan="2" style="color: #ffffff; height: 42px;vertical-align: top;">
                        <?php
						if($rows->product_name != '')
						{
						  if(strlen($rows->product_name) > 36)
						  {

						    echo substr($rows->product_name, 0,  (32 - strlen($rows->product_name)))."...." ;

						  }
						  else
						  echo $rows->product_name;
						}
						?>
                        </td>
                      </tr>
                      <tr>
                        <td style="color: #A8C738; font-size: 22px; font-weight: 600;">&#x20A6;
                          <?php  echo number_format($rows->product_price); ?></td>
                        <td><input type="button" class="btn-block btn btn-cart" value="add to cart"></td>
                      </tr>
                    </table>
                  </div>
              </div>

            </a>
          </div>
          <?php
			    $count_div = 0;
			  } // end of if...
			  $count_div ++;
			  $count_row ++;
				}
			}
    		?>
        </div>
        <div class="customNavigation">
            <a id="merchant-prev" class="prev">&gt;</a>
            <a id="merchant-next" class="next">&lt;</a>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>

<section class="stories">
  <div class="container"> <br/>

    <div style="text-align: center">
      <p style="color:#00587C"><b>Meet Our Merchants, hear their stories</b></p>
      <p>Discover merchants and products with reviews from around the marketplace</p>
    </div><br/>

    <div class="story-box-outer">
    <?php
    if($merchantProfile && $merchantProfile  != '')
    {

      foreach($merchantProfile as $rows)
      {

        if($rows->user_img != '' && file_exists('./banner_images/'.$rows->image))
        $merchantLogo = base_url().'provider_images/'.$rows->user_img;
        else
        $merchantLogo = base_url().'assets/img/newmerchant.jpg';
      ?>
     <div class="story-box">
        <div class="inner">
          <table width="100%">
            <tr>
              <td><div class="inner2"> <img width="180" height="180" src="<?php echo  $merchantLogo; ?>"> </div></td>
            </tr>
            <tr>
              <td><br/>
                <b>
                <?php
                if($rows->full_name != '')
                {
                  if(strlen($rows->full_name) > 24)
                  {
                    echo substr($rows->full_name, 0,  (22 - strlen($rows->full_name)))."..." ;
                  }
                  else
                    echo $rows->full_name;
                }
                ?></b><br/>
                <i>
                <?php
                if($rows->display_name != '')
                {
                  if(strlen($rows->display_name) > 29)
                  {
                    echo substr($rows->display_name, 0,  (26 - strlen($rows->display_name)))."..." ;
                  }
                  else
                    echo $rows->display_name;
                }
                ?></i></td>
            </tr>
            <tr>
              <td><br/>
                <?php
                if($rows->description != '')
                {
                  if(strlen($rows->description) > 90)
                  {

                    echo substr($rows->description, 0,  (87 - strlen($rows->description)))."...." ;

                  }
                  else
                  echo $rows->description;
                }
                ?> </td>
            </tr>
          </table>
        </div>
      </div>
      <?php
      }
    }
    ?>
    </div>

    <div class="extra_div">
      <a href="<?php echo base_url().'meet-our-merchants';?>">
          <button type="submit" class="btn bg-all-merchants ">See All Merchants <img src="<?php echo base_url()?>assets/img/arror-right.png"> </button>
      </a>
      <a href="<?php echo base_url().'registration';?>">
          <button type="submit" class="btn bg-register ">Become A Merchant <img src="<?php echo base_url()?>assets/img/arror-right.png"></button>
      </a>
        <br class="clearfix" />
    </div>

  </div>
</section>

<section class="tools">
  <div class="container">
    <div style="text-align: center">
      <p style="color:#00587C"><b>Banking Tools</b></p>
      <p>Connect with a like-minded business community to find knowledge, customers and opportunities to grow your
        business</p>
    </div>
    <div>
      <div class="tool-box">
        <table width=100% style="height: 170px">
          <tr style="vertical-align: top;">
            <td style="padding-right: 10px;"><p style="color:#00587C"><b>Ecobank Cards</b></p>
              <p>Ecobank Card account holders can operate their accounts through any of our globally
                recognised cards</p></td>
            <td><img src="<?php echo base_url()?>assets/img/cards.png"></td>
          <tr>
        </table>
        <a href="http://www.ecobank.com/cards.aspx" target="_blank"><img src="<?php echo base_url()?>assets/img/more-button.png"></a> </div>
      <div class="tool-box">
        <table width=100% style="height: 170px">
          <tr style="vertical-align: top;">
            <td style="padding-right: 10px;"><p style="color:#00587C"><b>Ecobank Loans</b></p>
              <p>Smile every mile... move ahead in life with an Ecobank Car or 'Moto'</p></td>
            <td><img src="<?php echo base_url()?>assets/img/loans.png"></td>
          <tr>
        </table>
        <a href="http://www.ecobank.com/loans.aspx" target="_blank"><img src="<?php echo base_url()?>assets/img/more-button.png"></a> </div>
      <div class="tool-box last">
        <table width=100% style="height: 170px">
          <tr style="vertical-align: top;">
            <td style="padding-right: 10px;"><p style="color:#00587C"><b>Online Account</b></p>
              <p>Apply online for your Ecobank Acc</p></td>
            <td><img src="<?php echo base_url()?>assets/img/online.png"></td>
          </tr>
        </table>
        <a href="http://www.ecobank.com/currentaccounts.aspx" target="_blank"><img src="<?php echo base_url()?>assets/img/more-button.png"></a> </div>
    </div>
  </div>
</section>
