<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>
<section class="background-white">

  <div class="container">

    <div class="outer-merchant">

      <?php 

		$merchantImg = base_url().'assets/img/newmerchant.jpg'; 

		if($merchantDetail->image && $merchantDetail->image != '')

		{

			$merchantImg = base_url().'banner_images/'.$merchantDetail->image;

		}

		?>

      <div class="col-xs-2 user_img"> <img width="176" height="180" src="<?php echo $merchantImg; ?>"> <a href="edit-profile"><span class="glyphicon glyphicon-pencil"></span> Edit</a> </div>

      <div class="col-sm-10  user_r_con">

        <h3><?php echo $merchantDetail->display_name; ?></h3>

      </div>

    </div>

    <div style="float:left;width: 100%;">

      <ul class="merchant_menu">

        <li class="b_width">&nbsp;</li>

        <li><a href="<?php echo base_url();?>profile">Profile</a></li>

        <li><a href="<?php echo base_url();?>edit-profile">Edit Profile</a></li>
        <li><a href="<?php echo base_url();?>change-password">Change Password</a></li>
        <li><a href="<?php echo base_url();?>product">Products</a></li>
        
        <li><a href="<?php echo base_url();?>description">Description</a></li>

        <li><a href="<?php echo base_url();?>banner">Banner</a></li>
        
        <li><a href="<?php echo base_url();?>add-banner">Add Banner</a></li>

        <li><a href="<?php echo base_url();?>transaction">Transaction</a></li>

        <div class="clearfix"></div>

      </ul>

    </div>

  </div>

</section>

