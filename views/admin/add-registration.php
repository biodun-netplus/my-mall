<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Page Heading -->
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header"> User Registration </h1>
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <?php
	if(isset($msg) && $msg != '')
	{
	?>
	<div class="alert alert-success"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
      <strong><?php echo $msg;?></strong>
    </div>
    <?php
	}
	else if(isset($error_msg) && $error_msg != '')
	{
	?>
    <div class="alert alert-danger"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
       <strong><?php echo $error_msg;?></strong>
    </div>
    <?php
	}
	?>
  </div>
</div>
<!-- /.row -->

<div class="row">
  <div class="col-lg-6">
    <form role="form" action="<?php echo base_url();?>admin/registration/add" id="add-user" method="post" enctype="multipart/form-data">
      <div class="form-group">
        <label>Name</label>
        <input class="form-control validate[required]" type="text" name="txtName" id="txtName"> 
      </div>
      <div class="form-group">
        <label>Display Name</label>
        <input class="form-control validate[required]" type="text" name="txtDisplayName" id="txtDisplayName"> 
      </div>
      <div class="form-group">
        <label>Email</label>
        <input class="form-control validate[required,custom[email]]" type="text" name="txtEmail" id="txtEmail"> 
      </div>
      <div class="form-group">
        <label>Password</label>
        <input class="form-control validate[required]" type="password" name="txtPassword" id="txtPassword"> 
      </div>
      <div class="form-group">
        <label>Phone</label>
        <input class="form-control validate[required,custom[phone]]" type="text" name="txtPhone" id="txtPhone"> 
      </div>
      <div class="form-group">
        <label>Address</label>
        <textarea rows="3" class="form-control validate[required]" name="txtAddress" id="txtAddress"></textarea> 
      </div>
      <div class="form-group">
        <label>Country</label>
        <select class="form-control validate[required]" name="txtCountry" id="txtCountry">
        <option value="">Country</option>
        <?php
		if($countries)
		{
			foreach($countries as $rows)
			{
				
		?>
        <option value="<?php echo $rows->id; ?>"><?php echo $rows->name; ?></option>
        <?php
			}
		}
		?>
        </select>
      </div>
      <div class="form-group">
        <label>State</label>
        <select class="form-control validate[required]" name="txtState" id="txtState">
        <option value="">State</option>
        </select>
      </div>
      <div class="form-group">
        <label>City</label>
        <input class="form-control validate[required]" type="text" name="txtCity" id="txtCity"> 
      </div>
      <div class="form-group">
        <label>Zip</label>
        <input class="form-control validate[required]" type="text" name="txtZip" id="txtZip"> 
      </div>
      <div class="form-group">
        <label>Category</label>
        <select class="form-control validate[required]" name="txtCategory" id="txtCategory">
        <option value="">Category</option>
		 <?php
         if($categories)
         {
            foreach($categories as $row)
            {
                
         ?>
         <option value="<?php echo $row->id; ?>"><?php echo $row->txtCategoryName; ?></option>
         <?php
            }
         }
		 ?>
        </select>
      </div>
      <!--<div class="form-group">
        <label>Sub Category</label>
        <select class="form-control validate[required]" name="txtSubCategory" id="txtSubCategory">
        <option value="1">test</option>
        </select>
      </div>-->
      <div class="form-group">
        <label>Logo</label>
        <input class="" type="file" name="txtImage" id="txtImage"> 
      </div>
      <div class="form-group">
        <label>Deliveryarea</label>
        <select class="form-control" name="txtDeliveryarea" id="txtDeliveryarea">
         <option value="">Deliveryarea</option>
		 <?php
         if($states_merchant)
         {
            foreach($states_merchant as $row)
            {
                
         ?>
         <option value="<?php echo $row->name; ?>"><?php echo $row->name; ?></option>
         <?php
            }
         }
		 ?>
        </select>
      </div>
      <div class="form-group">
        <label>Access Bank Account?</label>
        <div> <span>
          <input type="radio" value="Yes" id="Yes" class="bankaccounselect validate[required]" name="bankaccounselect">
          Yes</span> <span>
          <input type="radio" value="No" id="No" class="bankaccounselect validate[required]" name="bankaccounselect">
          No</span> </div>
        <p style="display:none;" id="BankShow">
          <input class="form-control validate[required]" type="text" name="txtAccountType" id="txtAccountType">
        </p>
      </div>
      <button type="submit" class="btn btn-default" value="Submit" name="submit">Submit</button>
      <button type="reset" class="btn btn-default">Reset</button>
    </form>
  </div>
</div>
<!-- /.row -->
<script type='text/javascript'>
//js validation code for add user form
$(document).ready(function(){

	$("#add-user").validationEngine({

		promptPosition : "topLeft",

		//ajaxFormValidationURL : "<?php echo base_url();?>ajax_validation.php"
	});
});
</script>
