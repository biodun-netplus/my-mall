<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" href="<?php echo base_url() ?>assets/new/css/reset.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/new/css/style.css">
<link rel="stylesheet" href="<?php echo base_url() ?>assets/new/css/responsive.css">
<script type="text/javascript" src="<?php echo base_url() ?>assets/new/js/jssor.slider.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<!-- Header -->
<header>

<!--    <div id="banner" class="slider-wrapper theme-default">-->
        <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height: 350px; overflow: hidden; visibility: hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                <div style="position:absolute;display:block;background:url('<?php echo base_url() ?>assets/new/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
            </div>
            <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1300px; height: 350px; overflow: hidden;">

                <div data-p="225.00" style="display: none;">
                    <img data-u="image" src="<?php echo base_url() ?>assets/new/img/banner1.png" />
                </div>
                <div data-p="225.00" style="display: none;">
                    <img data-u="image" src="<?php echo base_url() ?>assets/new/img/banner1.png" />
                </div>
                <div data-p="225.00" style="display: none;">
                    <img data-u="image" src="<?php echo base_url() ?>assets/new/img/banner1.png" />
                </div>
            </div>
            <!-- Bullet Navigator -->
            <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
                <!-- bullet navigator item prototype -->
                <div data-u="prototype" style="width:16px;height:16px;"></div>
            </div>
            <!-- Arrow Navigator -->
            <!-- <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
                       <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span> -->

        </div>
<!--       </div>-->

</header>
<div class="container">
<h2>Featured Merchant</h2>
<div class="disp1">
<div class="img">
    <?php
//    var_dump($featuredProduct);
    ?>
    <img style="width: 100%;" src="<?php echo base_url() ?>featured_merchant/<?php echo $featuredMerchant[0]->image;?>">
</div>

<div class="ptdis">
   <?php
      			if($featuredProduct && $featuredProduct  != '')
      			{
                      $count_div = 1;
                      $total_featured_products = count($featuredProduct);
//                      echo $total_featured_products;
                      $count_row =  1;
      				foreach($featuredProduct as $rows)
      				{
?>
                          <div class="product">
<!--                              <img src="--><?php //echo base_url() ?><!--assets/new/img/g.png">-->
                              <a href="<?php echo base_url() ?>product/<?php echo base64_encode($rows->id)?>">
                              <img src="<?php echo base_url() ?>product_images/<?php echo $rows->image?>" style="height: 200px">
                              <p><?php
                                  echo substr($rows->product_name,0,22);
                                  if(strlen($rows->product_name) > 22)
                                  {
                                      echo '...';
                                  }
                                      ?></p>
                              <div class="price">&#8358;<?php echo number_format($rows->product_price,0)?></div>
                              <div class="btn3"><p>Add To Cart</p></div></a>
                          </div><?php
                      }
                  }
                      ?>




</div>

<br class="clearfix">

<?php
$i = 0;
//    var_dump($category1);
foreach($category1 as $row)
{
    $subcat = $this->home_model->get_sub_categories($row->id);
//    var_dump($subcat);
    if($i%2== 0)
    {
        ?>
    <div class="wrap" style="float: left;">
        <div class="head">
            <h3 class="float_lft"><?php echo $row->category_name;?></h3>
<!--            <p class="float_rgt"><a href="">VIEW ALL</a></p>-->
        </div>

        <div class="info">
            <div class="imgtile" style="width: ">
                <img class="float_lft" style="padding: 10px;width:341px;height: 240px" src="<?php echo base_url() ?>banner_images/<?php echo $row->banner?>">
            </div>
        <?php
        $x = 0;
        foreach($subcat as $subcat_row)
        {
            if($x==0)
            {
                ?>
                <div class="tiles" style="padding: 10px; margin-top: 10px; width: 32%; float: left; text-align: center; border-bottom: none;">
                    <h3 style="margin: 0; padding: 0;"><?php echo $subcat_row->category_name?></h3>
                    <p style="font-size: 11px; color: grey;">Top Brands</p>
                    <img src="<?php echo base_url() ?>banner_images/<?php echo $subcat_row->banner?>">
                </div>
                <div style="padding: 10px; height: 230px;">
                <?php
            }
            if($x==1)
            {
                ?>
                <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none;">
                    <h3 style="margin: 0; padding: 0;"><?php echo $subcat_row->category_name?></h3>
                    <p style="font-size: 11px; color: grey;">Top Brands</p>
                    <img style="margin-top: 40px;" src="<?php echo base_url() ?>banner_images/<?php echo $subcat_row->banner?>">
                </div>
                <?php
            }
            if($x==2)
            {
                ?>
                <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none;">
                    <h3 style="margin: 0; padding: 0;"><?php echo $subcat_row->category_name?></h3>
                    <p style="font-size: 11px; color: grey;">Top Brands</p>
                    <img src="<?php echo base_url() ?>banner_images/<?php echo $subcat_row->banner?>">
                </div>
                <?php
            }
            if($x==3)
            {
                ?>
                <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center;">
                    <h3 style="margin: 0; padding: 0;"><?php echo $subcat_row->category_name?></h3>
                    <p style="font-size: 11px; color: grey;">Top Brands</p>
                    <img style="margin-top: 40px;" src="<?php echo base_url() ?>banner_images/<?php echo $subcat_row->banner?>">
                </div>
                <?php
            }

            $x++;
        }?>






            </div>

        </div>

    </div>
        <?php
    }
    else{
        ?>
    <div class="wrap" style="float: right;">
        <div class="head">
            <h3 class="float_lft"><?php echo $row->category_name;?></h3>
            <p class="float_rgt"><a href="">VIEW ALL</a></p>
        </div>

        <div class="info">
            <div class="imgtile">
                <img class="float_lft" style="padding: 10px;width:341px;height: 240px" src="<?php echo base_url() ?>banner_images/<?php echo $row->banner?>">
            </div>
            <?php
            $x = 0;
            foreach($subcat as $subcat_row)
            {
                if($x==0)
                {
                    ?>
                    <div class="tiles" style="padding: 10px; margin-top: 10px; width: 32%; float: left; text-align: center; border-bottom: none;">
                        <h3 style="margin: 0; padding: 0;"><?php echo $subcat_row->category_name?></h3>
                        <p style="font-size: 11px; color: grey;">Top Brands</p>
                        <img src="<?php echo base_url() ?>banner_images/<?php echo $subcat_row->banner?>">
                    </div>
                <div style="padding: 10px; height: 230px;">
                <?php
                }
                if($x==1)
                {
                    ?>
                    <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none;">
                        <h3 style="margin: 0; padding: 0;"><?php echo $subcat_row->category_name?></h3>
                        <p style="font-size: 11px; color: grey;">Top Brands</p>
                        <img style="margin-top: 40px;" src="<?php echo base_url() ?>banner_images/<?php echo $subcat_row->banner?>">
                    </div>
                    <?php
                }
                if($x==2)
                {
                    ?>
                    <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none;">
                        <h3 style="margin: 0; padding: 0;"><?php echo $subcat_row->category_name?></h3>
                        <p style="font-size: 11px; color: grey;">Top Brands</p>
                        <img src="<?php echo base_url() ?>banner_images/<?php echo $subcat_row->banner?>">
                    </div>
                    <?php
                }
                if($x==3)
                {
                    ?>
                    <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center;">
                        <h3 style="margin: 0; padding: 0;"><?php echo $subcat_row->category_name?></h3>
                        <p style="font-size: 11px; color: grey;">Top Brands</p>
                        <img style="margin-top: 40px;" src="<?php echo base_url() ?>banner_images/<?php echo $subcat_row->banner?>">
                    </div>
                    <?php
                }

                $x++;
            }?>
            </div>
        </div>

    </div>
        <?php
    }

//{
//    $get_no_of_sub = $this->home_model->get_no_of_sub($row->id);
//
//    if($get_no_of_sub > 3)
//        var_dump($get_no_of_sub);
    $i++;
}
?>







<br class="clearfix">

<div class="disp2">
    <div class="menu">
        <div class="head">
            <h3 class="float_lft">OTHER CATEGORY</h3>
        </div>
        <?php
//        var_dump($other_category);
//        echo $other_category[0]->id;
        $subcat_other = $this->home_model->get_sub_categories($other_category[0]->id);
        ?>
        <div class="menu_itm">
            <ul>
                <?php
                foreach($subcat_other as $sub)
                {
                    ?>
                    <a href=""><li><?php echo $sub->category_name;?></li></a>
                    <?php
                }
                ?>

            </ul>
        </div>

    </div>

    <img style="margin-left: 3px; vertical-align: top; padding: 5px;" src="<?php echo base_url() ?>assets/new/img/o1.png">
    <img style="vertical-align: top;  padding: 5px; border-right:1px solid #ddd;" src="<?php echo base_url() ?>assets/new/img/o2.png">
    <img style=" padding: 5px; " src="<?php echo base_url() ?>assets/new/img/o3.png">
    <img style="width: 41%; margin-left: 3px; vertical-align: top;  padding: 5px;" src="<?php echo base_url() ?>assets/new/img/p1.png">
    <img style="width: 33%; float: right;  padding: 5px;" src="<?php echo base_url() ?>assets/new/img/p2.png">

</div>
</div>
</div>

<br class="clearfix">
<?php
//var_dump($merchantProfile);
?>
<div class="mercht">
    <div class="contn">
        <h2>Meet our Merchants, hear their stories.</h2>
        <p>Discover merchants and products with reviews from around the market place</p>
<?php

        foreach($merchantProfile as $our)
        {
//            var_dump($our);
            if($our->user_img != '' )
                $merchantLogo = base_url().'provider_images/'.$our->user_img;
            else
                $merchantLogo = base_url().'assets/img/newmerchant.jpg';
         ?>
            <div class="box">
                <img src="<?php echo $merchantLogo;?>" style="width: 150px; height: 150px; border:1px solid grey;">
                <p style="text-align: left;"><b>Merchant Name</b></p>
                <p style="text-align: left;"><i><?php echo $our->display_name?></i></p>
            </div>
            <?php
        }
        ?>



        <br class="clearfix">

        <div class="button">
            <div class="btn2"><a href="<?php echo base_url().'meet-our-merchants';?>"><p>See All Merchant</p></a></div>

            <div class="btn1"><a href="<?php echo base_url().'registration';?>"><p>Become a Merchant</p></a></div>
        </div>


    </div>

    <br class="clearfix">


</div>