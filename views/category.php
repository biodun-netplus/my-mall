<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style type="text/css">
.section_nav { min-height: 52px; }
</style>

<div id="content">
  <div class="container">
    <div id="top_banner">

      <div class="main_banner">
        <div class="slider-wrapper theme-default">
          <div id="slider" class="nivoSlider"> <img src="<?php echo base_url();?>assets/img/banner1.jpg"/> </div>
        </div>
      </div>

      <div class="side_banner">
        <div class="side_banner_itm"><a href="<?php echo base_url();?>category/NjQ="><img src="<?php echo base_url();?>assets/img/small-banner1.jpg" /></a></div>
        <div class="side_banner_itm"><a href="<?php echo base_url();?>category/MzM="><img src="<?php echo base_url();?>assets/img/small-banner2.jpg" /></a></div>
          <br class="clearfix" />
      </div>

      <br class="clear" />
    </div>

    <div id="lower_banner">
      <div class="lower_banner_itm">
        <div class="inner"><a href="<?php echo base_url();?>category/NjQ="><img src="<?php echo base_url();?>assets/img/small-banner1.jpg" /></a></div>
      </div>
      <div class="lower_banner_itm">
        <div class="inner"><a href="<?php echo base_url();?>category/MzM="><img src="<?php echo base_url();?>assets/img/small-banner2.jpg" /></a></div>
      </div>
      <div class="lower_banner_itm">
        <div class="inner"><a href="<?php echo base_url();?>category/NjU="><img src="<?php echo base_url();?>assets/img/small-banner3.jpg" /></a></div>
      </div>
      <br class="clear" />
    </div>

    <div class="section">
      <?php
    
    if($categoryDetail)
    {
      
    $countI   = 0;
    $bannerImg  = base_url().'assets/img/ad-banner.jpg';
    foreach($categoryDetail as $rows)
    {
      if($rows['banner'] != '')
      $bannerImg  = base_url().'category_images/'.$rows['banner'];
      
      $countI ++;
      
    ?>
      <div class="section_title">
        <h3><?php echo $rows['category_name'];?></h3>
      </div>

      <div class="section_nav">
        <div class="nav-top">
          <div class="display-div" style="height: auto;">
            <div class="main_nav product_nav_wrap_silder">
              <div class="inner_nav"> 
                 <a href="#" id="prev<?php echo $countI;?>" class=""><div class="nav-top-control nav-icon-left left"></div>  </a> 
                 <a href="#" id="next<?php echo $countI;?>">  <div class="nav-top-control nav-icon-right right"></div> </a>
                <div class="slideshow" data-cycle-fx=carousel  data-cycle-timeout=0  data-cycle-slides="> div" data-cycle-next="#next<?php echo $countI;?>" data-cycle-prev="#prev<?php echo $countI;?>" >
                  <?php  
                   $subCategoryDetail   = $this->category_model->all_subcategory_list(array('parent_id' => $rows['id'] ,'status'=>'t')); 
                    if($subCategoryDetail  && !empty($subCategoryDetail))
                    {
                     
                    $countRows      = 0;
                    $groupCategory    = '';
                    
                    foreach($subCategoryDetail as $subCateRows)
                    {
            
                    ?>
                            <div>
                              <p><a href="javaScript:;"><?php echo $subCateRows->name;?></a></p>
                            </div>
                            <?php 
            
                      }
            
                    }

                  ?>
                </div>
                <div class="clear"></div>
              </div>
              <div class="clear"></div>
            </div>
          </div>
        </div>
      </div>  <!-- section_nav !--> 
      
      <div class="section_content">
        <div class="product_area_wrap">
          <?php 
     
      if($rows['productDetail'] && !empty($rows['productDetail']))
      {
        $index    = 0;
        $productImg   = base_url().'assets/img/logo.png';
        foreach($rows['productDetail'] as $productRow)
        {
          if($rows['productDetail'][$index]['image'] != '')
          $productImg  = base_url().'product_images/'.$rows['productDetail'][$index]['image'];
      ?>
          <div class="product_wrap"> <a href="<?php echo base_url().'product/'.base64_encode($rows['productDetail'][$index]['id']);?>">
            <div class="image"><img src="<?php echo $productImg;?>"></div>
            <div class="details">
              <table width="100%">
                <tr>
                  <td colspan="2" style="color: #fff; height: 42px; vertical-align: top; min-width: 210px; width: 100%; text-align:center;">
                      <?php echo $rows['productDetail'][$index]['product_name'];?>
                  </td>
                </tr>
                <tr>
                  <td style="color: #A8C738; font-size: 22px; font-weight: 600;">
                      &#x20A6;<?php echo number_format($rows['productDetail'][$index]['product_price']);?>
                  </td>
                  <td><input type="button" class="btn-block btn btn-cart" value="add to cart"></td>
                </tr>
              </table>
            </div>
            </a> </div>
          <?php
          $index ++;
        }
      
      }
      else
      {
        echo '<div class="no-record text-center" style="margin-bottom:10px">No Record Found!</div>';
      }
          
         ?>
          <br class="clear" />
        </div>
        <div class="sect_banner_ad">
          <div class="banner_ad_image"> <a href="<?php echo base_url();?>category/<?php echo base64_encode($rows['id']);?>"><img src="<?php echo $bannerImg;?>" /></a> </div>
          <div class="banner_ad_btn">Visit Merchant Store</div>
        </div>
        <br class="clear" />
        <div class="clear"></div>
      </div>
      <?php
    }
    }
  ?>
      <br class="clear" />
    </div>
  </div>
</div>
<div id="featured_merchant">
  <div class="container">
    <div class="feat_title">
      <h1 class="featured-merchant-heading">Featured Merchants</h1>
      <div class="more_btn"><a href="<?php echo base_url()?>meet-our-merchants">See All Merchants</a></div>
    </div>
    <div class="feat_cont">
      <div class="btm_slider_wrap">
        <div class="slider_inner">
          <div class="owl-carousel2">
            <?php
            if($merchant && $merchant  != '')
            {
              foreach($merchant as $rows)
              {
                 $merchantLogo = '';
                 if($rows->image != '')
                 $merchantLogo = 'banner_images/'.$rows->image;
                 else
                 $merchantLogo = 'assets/img/newmerchant.jpg';
              
            ?>
                  <div class="items">
                    <div class="slider_itm"> <a href="<?php echo base_url().$rows->slug;?>"><img src="<?php echo base_url().$merchantLogo;?>" width="180" height="178"></a> </div>
                  </div>
                  <?php 
              }
               
            }
          ?>
          </div>
          <div class="customNavigation"> <a class="prev left_control"></a> <a class="next right_control"></a> </div>
        </div>
      </div>
    </div>
  </div>
</div>
