<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="background-white">
  <div class="container">
    <div class="row">
      <div class="col-lg-12" style="text-align: center;">
       <!--<img src="<?php //echo base_url();?>assets/img/404-error-pages.png"> !-->
        <p style="font-size: 50px;color: #9bc535;"> 404 page not found </p>
        <p style="font-size: 20px;color: #1275b8;"> The page you are looking for does not exist. </p>
        <p style="font-size: 14px; color: #1275B8;"> Click <a href="<?php echo base_url();?>"> here </a> to continue shopping </p>
      </div>

      <div class="section_content" style="clear: both;">
		  <div class="product_area_wrap1" style="border-left: 1px solid #ddd;border-top: 1px solid #ddd;">
		    <?php 
		    if($productDetail && !empty($productDetail))
		        {
		        $productImg   = base_url().'assets/img/logo.png';
		        foreach($productDetail as $productRow)
		        {
		          if($productRow->image != '' )
		          $productImg  = base_url().'product_images/'.$productRow->image;
		      ?>
		    <div class="product_wrap product_wrap1"> <a href="<?php echo base_url().'product/'.base64_encode($productRow->id);?>">
		      <div class="image"><img src="<?php echo $productImg;?>"></div>
		      <div class="details">
		        <table>
		          <tr>
		            <td colspan="2" style="color: #fff; height: 42px; vertical-align: top; min-width: 210px; width: 100%;"><?php echo $productRow->product_name; ?></td>
		          </tr>
		          <tr>
		            <td style="color: #A8C738; font-size: 22px; font-weight: 600;">&#x20A6;<?php echo number_format($productRow->product_price);?></td>
		            <td><input type="button" class="btn-block btn btn-cart" value="add to cart"></td>
		          </tr>
		        </table>
		      </div>
		      </a> </div>
		    <?php
		        }
		      
		      }
		      else
		      {
		        echo '<div class="no-record text-center">No Record Found!</div>';
		      }
		         ?>
		    <br class="clear" />
		  </div>
		  <div class="clear"></div>
		</div>
    </div>
  </div>
</section> 
