<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
	<script type="text/javascript" src="js/jssor.slider.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
</head>
<body>

	<script>
        jssor_1_slider_init = function() {
            
            var jssor_1_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_1_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            //responsive code end
        };
    </script>

    		<div>

				<div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height: 350px; overflow: hidden; visibility: hidden;">
			        <!-- Loading Screen -->
			        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
			            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
			            <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
			        </div>
			        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1300px; height: 350px; overflow: hidden;">
			            
			            <div data-p="225.00" style="display: none;">
			                <img data-u="image" src="img/banner1.png" />
			            </div>
			            <div data-p="225.00" style="display: none;">
			                <img data-u="image" src="img/banner1.png" />
			            </div>
			             <div data-p="225.00" style="display: none;">
			                <img data-u="image" src="img/banner1.png" />
			            </div>
			        </div>
			        <!-- Bullet Navigator -->
			        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
			            <!-- bullet navigator item prototype -->
			            <div data-u="prototype" style="width:16px;height:16px;"></div>
			        </div>
			        <!-- Arrow Navigator -->
			        <!-- <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
			        <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span> -->
			        
			    </div>

<div class="container">
	<h2>Featured Merchant</h2>
	<div class="disp1">
		<div class="img">
			<img style="width: 100%;" src="img/fea.png">
		</div>

		<div class="ptdis">
			<div class="product">
				<img src="img/g.png">
				<p>Gold Dial Black Leather Strap</p>
				<div class="price">&#8358;2,600</div>
				<div class="btn3"><a href=""><p>Buy now</p></a></div>
			</div>

			<div class="product">
				<img src="img/g.png">
				<p>Gold Dial Black Leather Strap</p>
				<div class="price">&#8358;2,600</div>
				<div class="btn3"><a href=""><p>Buy now</p></a></div>
			</div>

			<div class="product">
				<img src="img/g.png">
				<p>Gold Dial Black Leather Strap</p>
				<div class="price">&#8358;2,600</div>
				<div class="btn3"><a href=""><p>Buy now</p></a></div>
			</div>

		</div>

		<br class="clearfix">

		<div class="view">
			<div class="wrap" style="float: left;">
				<div class="head">
					<h3 class="float_lft">MOBILE PHONES & TABLETS</h3>
					<p class="float_rgt"><a href="">VIEW ALL</a></p>
				</div>

				<div class="info">
						<div class="imgtile">
							<img class="float_lft" style="padding: 10px; width: 66%" src="img/1.png">
						</div>
						<div class="tiles" style="padding: 10px; margin-top: 10px; width: 32%; float: left; text-align: center; border-bottom: none;">
							<h3 style="margin: 0; padding: 0;">Smart Phones</h3>
							<p style="font-size: 11px; color: grey;">Top Brands</p>
							<img src="img/a1.png">
						</div>

					<div style="padding: 10px; height: 230px;">
						 <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none;">
							<h3 style="margin: 0; padding: 0;">Tablets</h3>
							<p style="font-size: 11px; color: grey;">Top Brands</p>
							<img style="margin-top: 40px;" src="img/b1.png">
						</div> 
						<div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none;">
							<h3 style="margin: 0; padding: 0;">Accessories</h3>
							<p style="font-size: 11px; color: grey;">Top Brands</p>
							<img src="img/b2.png">
						</div>
						<div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center;">
							<h3 style="margin: 0; padding: 0;">All</h3>
							<p style="font-size: 11px; color: grey;">Top Brands</p>
							<img style="margin-top: 40px;" src="img/b3.png">
						</div> 
					</div>

				</div>

			</div>

			<div class="wrap" style="float: right;">
				<div class="head">
					<h3 class="float_lft">ELECTRONICS</h3>
					<p class="float_rgt"><a href="">VIEW ALL</a></p>
				</div>

				<div class="info">
				<div class="imgtile">
					<img class="float_lft" style="padding: 10px; width: 66%" src="img/2.png">
				</div>
				<div class="tiles" style="padding: 10px; margin-top: 10px; width: 32%; float: left; text-align: center; border-bottom: none;">
						<h3 style="margin: 0; padding: 0;">Television</h3>
						<p style="font-size: 11px; color: grey;">Top Brands</p>
						<img style="margin-top: 30px;" src="img/a2.png">
				</div>

				<div style="padding: 10px; height: 230px;">
						 <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none; ">
							<h3 style="margin: 0; padding: 0;">DVD Players</h3>
							<p style="font-size: 11px; color: grey;">Top Brands</p>
							<img style="margin-top: 40px;" src="img/c1.png">
						</div> 
						<div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none; ">
							<h3 style="margin: 0; padding: 0;">Audio syatems</h3>
							<p style="font-size: 11px; color: grey;">Top Brands</p>
							<img style="margin-top: 10px;" src="img/c2.png">
						</div>
						<div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center;">
							<h3 style="margin: 0; padding: 0;">Game Consoles</h3>
							<p style="font-size: 11px; color: grey;">Top Brands</p>
							<img  src="img/c3.png">
						</div> 
					</div>
				</div>

			</div>
		</div>

		<div class="wrap" style="float: left;">
			<div class="head">
				<h3 class="float_lft">HOME & OFFICE APPLIANCE</h3>
				<p class="float_rgt"><a href="">VIEW ALL</a></p>
			</div>

			<div class="info">
			<div class="imgtile">
				<img class="float_lft" style="padding: 10px; width: 66%" src="img/3.png">
			</div>
			<div class="tiles" style="padding: 10px; margin-top: 10px; width: 32%; float: left; text-align: center; border-bottom: none;">
					<h3 style="margin: 0; padding: 0;">Small Appliances</h3>
					<p style="font-size: 11px; color: grey;">Top Brands</p>
					<img style="margin-top: 30px;" src="img/a3.png">
			</div>

			<div style="padding: 10px; height: 230px;">
					 <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none; ">
						<h3 style="margin: 0; padding: 0;">Large Appliances</h3>
						<p style="font-size: 11px; color: grey;">Top Brands</p>
						<img  src="img/d1.png">
					</div> 
					<div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none; ">
						<h3 style="margin: 0; padding: 0;">Kitchen & Dinning</h3>
						<p style="font-size: 11px; color: grey;">Top Brands</p>
						<img style="margin-top: 10px;" src="img/d2.png">
					</div>
					<div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center;">
						<h3 style="margin: 0; padding: 0;">House keeping</h3>
						<p style="font-size: 11px; color: grey;">Top Brands</p>
						<img  style="margin-top: 10px;" src="img/d3.png">
					</div> 
				</div>
			</div>

		</div>

		<div class="wrap" style="float: right;">
			<div class="head">
				<h3 class="float_lft">COMPUTER & ACCESSORIES</h3>
				<p class="float_rgt"><a href="">VIEW ALL</a></p>
			</div>

			<div class="info">
			<div class="imgtile">
				<img class="float_lft" style="padding: 10px; width: 66%" src="img/4.png">
			</div>
				<div class="tiles" style="padding: 10px; margin-top: 10px; width: 32%; float: left; text-align: center; border-bottom: none;">
					<h3 style="margin: 0; padding: 0;">Laptops</h3>
					<p style="font-size: 11px; color: grey;">Top Brands</p>
					<img style="margin-top: 30px;" src="img/a4.png">
				</div>

				<div style="padding: 10px; height: 230px;">
					 <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none; ">
						<h3 style="margin: 0; padding: 0;">Desktop & Monitors</h3>
						<p style="font-size: 11px; color: grey;">Top Brands</p>
						<img  src="img/e1.png">
					</div> 
					<div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none; ">
						<h3 style="margin: 0; padding: 0;">Printers & scanners</h3>
						<p style="font-size: 11px; color: grey;">Top Brands</p>
						<img style="margin-top: 25px;" src="img/e2.png">
					</div>
					<div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center;">
						<h3 style="margin: 0; padding: 0;">Accessories</h3>
						<p style="font-size: 11px; color: grey;">Top Brands</p>
						<img style="margin-top: 25px;" src="img/e3.png">
					</div> 
				</div>
			</div>

		</div>

		<div class="wrap" style="float: left;">
			<div class="head">
				<h3 class="float_lft">FASHION</h3>
				<p class="float_rgt"><a href="">VIEW ALL</a></p>
			</div>

			<div class="info">
			<div class="imgtile">
				<img class="float_lft" style="padding: 10px; width: 66%" src="img/5.png">
			</div>
			<div class="tiles" style="padding: 10px; margin-top: 10px; width: 32%; float: left; text-align: center; border-bottom: none;">
					<h3 style="margin: 0; padding: 0;">Women's Fashion</h3>
					<p style="font-size: 11px; color: grey;">Top Brands</p>
					<img src="img/a5.png">
			</div>

				<div style="padding: 10px; height: 230px;">
					 <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none; ">
						<h3 style="margin: 0; padding: 0;">Tablets</h3>
						<p style="font-size: 11px; color: grey;">Top Brands</p>
						<img  src="img/f1.png">
					</div> 
					<div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none; ">
						<h3 style="margin: 0; padding: 0;">Accessories</h3>
						<p style="font-size: 11px; color: grey;">Top Brands</p>
						<img style="margin-top: 30px;" src="img/f2.png">
					</div>
					<div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center;">
						<h3 style="margin: 0; padding: 0;">All</h3>
						<p style="font-size: 11px; color: grey;">Top Brands</p>
						<img style="margin-top: 30px;" src="img/f3.png">
					</div> 
				</div>
			</div>

		</div>

		<div class="wrap" style="float: right;">
			<div class="head">
				<h3 class="float_lft">BABY, KIDS & TOYS</h3>
				<p class="float_rgt"><a href="">VIEW ALL</a></p>
			</div>

			<div class="info">
			<div class="imgtile">
				<img class="float_lft" style="padding: 10px; width: 66%" src="img/6.png">
			</div>
			<div class="tiles" style="padding: 10px; margin-top: 10px; width: 32%; float: left; text-align: center; border-bottom: none;">
					<h3 style="margin: 0; padding: 0;">Diapering & daily care</h3>
					<p style="font-size: 11px; color: grey;">Top Brands</p>
					<img style="margin-top: 15px;" src="img/a6.png">
			</div>

			<div style="padding: 10px; height: 230px;">
					 <div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none; ">
						<h3 style="margin: 0; padding: 0;">Tablets</h3>
						<p style="font-size: 11px; color: grey;">Top Brands</p>
						<img  src="img/g1.png">
					</div> 
					<div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center; border-right: none; ">
						<h3 style="margin: 0; padding: 0;">Accessories</h3>
						<p style="font-size: 11px; color: grey;">Top Brands</p>
						<img src="img/g2.png">
					</div>
					<div class="tiles" style="padding: 10px; width: 33.3%; float: left; text-align: center;">
						<h3 style="margin: 0; padding: 0;">All</h3>
						<p style="font-size: 11px; color: grey;">Top Brands</p>
						<img style="margin-top: 20px;" src="img/g3.png">
					</div> 
				</div>
			</div>

		</div>

		<br class="clearfix">

		<div class="disp2">
			<div class="menu">
				<div class="head">
					<h3 class="float_lft">OTHERS</h3>
				</div>
				<div class="menu_itm">
					<ul>
						<a href=""><li>Sport & Outdoor</li></a>
						<a href=""><li>furniture & Decor</li></a>
						<a href=""><li>Beauty& Health</li></a>
						<a href=""><li>Services</li></a>
						<a href=""><li>Travel</li></a>
						<a href=""><li>Import & Export</li></a>
					</ul>
				</div>

			</div>

			<img style="margin-left: 3px; vertical-align: top; padding: 5px;" src="img/o1.png">
			<img style="vertical-align: top;  padding: 5px; border-right:1px solid #ddd;" src="img/o2.png">
			<img style=" padding: 5px; " src="img/o3.png">
			<img style="width: 41%; margin-left: 3px; vertical-align: top;  padding: 5px;" src="img/p1.png">
			<img style="width: 33%; float: right;  padding: 5px;" src="img/p2.png">

		</div>
	</div>
</div>

<br class="clearfix">

<div class="mercht">
	<div class="contn">
		<h2>Meet our Merchants, hear their stories.</h2>
		<p>Discover merchants and products with reviews from around the market place</p>

		<div class="box">
			<img src="" style="width: 150px; height: 150px; border:1px solid grey;">
			<p style="text-align: left;"><b>Merchant Name</b></p>
			<p style="text-align: left;"><i>Name</i></p>
		</div>

		<div class="box">
			<img src="" style="width: 150px; height: 150px; border:1px solid grey;">
			<p style="text-align: left;"><b>Merchant Name</b></p>
			<p style="text-align: left;"><i>Name</i></p>
		</div>

		<div class="box">
			<img src="" style="width: 150px; height: 150px; border:1px solid grey;">
			<p style="text-align: left;"><b>Merchant Name</b></p>
			<p style="text-align: left;"><i>Name</i></p>
		</div>

		<div class="box">
			<img src="" style="width: 150px; height: 150px; border:1px solid grey;">
			<p style="text-align: left;"><b>Merchant Name</b></p>
			<p style="text-align: left;"><i>Name</i></p>
		</div>

		<div class="box">
			<img src="" style="width: 150px; height: 150px; border:1px solid grey;">
			<p style="text-align: left;"><b>Merchant Name</b></p>
			<p style="text-align: left;"><i>Name</i></p>
		</div>

		<br class="clearfix">

		<div class="button">
			<div class="btn"><a href=""><p>See All Merchant</p></a></div>

			<div class="btn1"><a href=""><p>Become a Merchant</p></a></div>
		</div>


	</div>

	<br class="clearfix">

	
</div>









<script>
	jssor_1_slider_init();
</script>
</body>
</html>