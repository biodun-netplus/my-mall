<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="background-white">
  <div class="container"> 
    <!-- Page Heading -->
    <div class="row">
      <div class="col-lg-12">
        <h3 class="page-header"> Merchant Registration </h3>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <?php
    		if(isset($msg) && $msg != '')
    		{
    		?>
        <div class="alert alert-success">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
          <strong><?php echo $msg;?></strong> </div>
        <?php
            }
            else if(isset($error_msg) && $error_msg != '')
            {
            ?>
        <div class="alert alert-danger">
          <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
          <strong><?php echo $error_msg;?></strong> </div>
        <?php
            }
            ?>
      </div>
    </div>
    <!-- /.row -->
    
    <div class="row">
      <form role="form" action="<?php echo base_url();?>registration" id="add-user" method="post" enctype="multipart/form-data">
        <div class="col-lg-6">
          <div class="form-group">
            <label>Business Name</label>
            <input class="form-control validate[required,ajax[ajaxUserCall]]" type="text" name="first_name" id="first_name" value="<?php if($userArray && $userArray['first_name']) { echo $userArray['first_name'];  } ?>">
          </div>
          <div class="form-group">
            <label>Full Name</label>
            <input class="form-control validate[required]" type="text" name="txtFullName" id="txtFullName" value="<?php if($userArray && $userArray['txtFullName']) { echo $userArray['txtFullName'];  } ?>" />
          </div>
          <div class="form-group">
            <label>Email</label>
            <input class="form-control validate[required,custom[email],ajax[ajaxUserCall]]" type="text" name="email" id="email" value="<?php if($userArray && $userArray['email']) { echo $userArray['email'];  } ?>">
          </div>
          <div class="form-group">
            <label>Password</label>
            <input class="form-control validate[required]" type="password" name="txtPassword" id="txtPassword">
          </div>
          <div class="form-group">
            <label>Phone</label>
            <input class="form-control validate[required,funcCall[customPhone[txtPhone]]" type="text" name="txtPhone" id="txtPhone" value="<?php if($userArray && $userArray['txtPhone']) { echo $userArray['txtPhone'];  } ?>">
          </div>
          <div class="form-group">
            <label>Address</label>
            <textarea rows="3" class="form-control validate[required]" name="txtAddress" id="txtAddress"><?php   if($userArray && $userArray['txtAddress']) { echo $userArray['txtAddress'];  } ?></textarea>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="form-group">
            <label>State</label>
            <select class="form-control validate[required]" name="txtState" id="txtState">
              <option value="">State</option>
              <?php
              if($states)
              {
                foreach($states as $rows)
                {
                  
              ?>
                    <option value="<?php echo $rows->state_id; ?>" <?php if($userArray && $userArray['txtState'] && $userArray['txtState'] == $rows->state_id) {?> selected="selected" <?php } ?> ><?php echo $rows->name; ?></option>
                    <?php
                }
              }
              ?>
            </select>
          </div>
          <div class="form-group">
            <label>City</label>
            <input class="form-control validate[required]" type="text" name="txtCity" id="txtCity" value="<?php if($userArray && $userArray['txtCity']) { echo $userArray['txtCity'];  } ?>">
          </div>
          <?php /*
          <div class="form-group">
            <label>Zip</label>
            <input class="form-control validate[required]" type="text" name="txtZip" id="txtZip" value="<?php if($userArray && $userArray['txtZip']) { echo $userArray['txtZip'];  } ?>">
          </div> */ ?>
          <div class="form-group">
            <label>Category</label>
            <select class="form-control validate[required]" name="seleCategory" id="seleCategory">
              <option value="">Category</option>
              <?php
        			 if($categories)
        			 {
        				foreach($categories as $row)
        				{
        					
        			 ?>
                      <option value="<?php echo $row->id; ?>" <?php if($userArray && $userArray['seleCategory'] && $userArray['seleCategory'] == $row->id) {?> selected="selected" <?php } ?>><?php echo $row->category_name; ?></option>
                      <?php
        				}
        			 }
        			 ?>
            </select>
          </div>
          <div class="form-group" style=" margin-bottom: 33px;">
            <label>Logo</label>
            <input class="validate[funcCall[validateImage[txtImage]]" type="file" name="txtImage" id="txtImage">
          </div>
          <div class="form-group">
            <label>Merchant location</label>
            <select class="form-control" name="txtDeliveryarea" id="txtDeliveryarea">
              <option value="">Merchant location</option>
              <?php
        			 if($states_merchant)
        			 {
        				foreach($states_merchant as $row)
        				{
        					
        			 ?>
                      <option value="<?php echo $row->name; ?>"  <?php if($userArray && $userArray['txtDeliveryarea'] && $userArray['txtDeliveryarea'] == $row->name) {?> selected="selected" <?php } ?>><?php echo $row->name; ?></option>
                      <?php
        				}
        			 }
        			 ?>
            </select>
          </div>
          <div class="form-group">
            <label>Do you have an Ecobank account?</label>
            <div> <span>
              <input type="radio" value="Yes" id="Yes" class="bankaccounselect validate[required]" name="bankaccounselect">
              Yes</span> <span>
              <input type="radio" value="No" id="No" class="bankaccounselect validate[required]" name="bankaccounselect">
              No</span> </div>
            <p style="display:none;" id="BankShow">
              <input class="form-control validate[required]" type="text" name="txtAccountType" id="txtAccountType">
            </p>
          </div>
        </div>
        <div class="col-lg-12">
          <button type="submit" class="btn btn-default" value="Submit" name="submit">Submit</button>
          <button type="reset" class="btn btn-default">Reset</button>
        </div>
      </form>
    </div>
  </div>
  </div>
</section>
<!-- /.row --> 
<script type='text/javascript'>
//js validation code for add user form
$(document).ready(function(){

	$("#add-user").validationEngine({
		promptPosition : "topLeft",
		ajaxFormValidationURL : "<?php echo base_url();?>ajax_validation/checkuniqueness",

	});
});
</script> 
