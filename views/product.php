<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="merchant_display" style="margin-top: 0px">
  <div class="breadcrumb"> <a href="<?php echo base_url();?>"> Home </a> > <a href=""> All Categories </a> > <a href="<?php echo base_url().$merchantDetail->slug;?>"> <?php echo $merchantDetail->display_name;?> </a> </div>
</section>

<section class="merchant_display product-added-alert" style="margin-top: 0px; display:none;">
<div class="container">
<div class="alert-cart"></div>
</div>
</section>

<section class="merchant_display" style="margin-top: 10px; margin-bottom: 20px;">
  <div class="container">
    <div class="product_product_display">
      <?php
    $image1 = $image2 = $image3 = '';
    if($productDetail->image == '')
    {
      $image1 = base_url().'assets/img/_no_image.png';
    }
    else
    {
      $image1 = base_url().'product_images/'.$productDetail->image;
    }

    if($productDetail->image2 == '')
    {
      $image2 = $image1;
    }
    else
    {
      $image2 = base_url().'product_images/'.$productDetail->image2;
    }

    if($productDetail->image3 == '')
    {
      $image3 = $image1;
    }
    else
    {
      $image3 = base_url().'product_images/'.$productDetail->image3;
    }
  ?>
      <div class="merchant_box_content" id="product-tab" style="height: auto; width:auto;text-align: center">
        <div id="tabs-1"><img src="<?php echo $image1;?>"  style="height: 405px;" alt=""/></div>
        <div id="tabs-2"><img src="<?php echo $image2;?>"  style="height: 405px;" alt=""/></div>
        <div id="tabs-3"><img src="<?php echo $image3;?>"  style="height: 405px;" alt=""/></div>
        <div class="merchant_box_content" style="width: 400px; height: auto; border: 1px solid #ccc">
          <ul style="list-style: outside none none;">
            <li class="sub-boxes"><a href="#tabs-1"><img src="<?php echo $image1;?>"  style="width: 100%"  alt=""/></a></li>
            <li class="sub-boxes"><a href="#tabs-2"><img src="<?php echo $image2;?>"  style="width: 100%" alt=""/></a></li>
            <li class="sub-boxes"><a href="#tabs-3"><img src="<?php echo $image3;?>"  style="width: 100%" alt=""/></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="product_product_detail">
      <p><?php echo $productDetail->product_name;?></p>
      <!--<div class="star-rating"> <span class="fa fa-star-o" data-rating="1"></span> <span class="fa fa-star-o" data-rating="2"></span> <span class="fa fa-star-o" data-rating="3"></span> <span class="fa fa-star-o" data-rating="4"></span> <span class="fa fa-star-o" data-rating="5"></span>
        <input type="hidden" name="whatever" class="rating-value" value="3">
      </div>-->
      <div style="border-bottom:1px solid #8C8C8C; margin-bottom: 10px; margin-top: 10px; width: 100%; float: right;"></div>
      <form class="add-to-cart-form" style="clear: both;">
        <table width="100%">
          <tr>
            <td style="font-size: 12px; color:#666666;" width="50%">Price:</td>
            <td style="font-size: 12px; font-weight: 600;">&#x20A6;<?php echo number_format($productDetail->list_price);?></td>
          </tr>
          <tr style="margin-bottom: 20px">
            <td style="font-size: 12px; color:#666666;">Discount Price:</td>
            <td style="font-weight: 600; font-size: 20px;">&#x20A6;<?php echo number_format($productDetail->product_price);?></td>
          </tr>
          <?php
          $soldOutPro =  false;
          $proAttrDetail  = $this->product_model->get_product_size_colour_by_product(array('relation_color_size.product_id' => $productDetail->id, 'relation_color_size.qty >' => 0));  //echo "<!---"; print_r($proAttrDetail); echo "!-->";
          if(!$proAttrDetail && $proAttrDetail == ''){
            $soldOutPro =  true;
          ?>
          <tr style="margin-bottom: 20px">
            <td style="font-size: 12px; color:#666666;">Availability:</td>
            <td style="font-weight: 600; font-size: 20px; color:red;">Sold Out</td>
          </tr>
          <?php } ?>
          <tr style="height:10px;">
            <td></td>
            <td></td>
          </tr>
      	  <?php
      	  if($productDetail->product_detail != ""){ ?>
      	  <tr style="height:10px;">
            <td style="font-size: 18px;">Description</td>
            <td></td>
          </tr>
      	  <tr style="margin-bottom: 20px">
            <td colspan="2" style="font-size: 12px;"><div class="description-pro"><?php echo $productDetail->product_detail;?><div></td>
          </tr>
      	  <?php } ?>
          <tr style="height:10px; background-color: #ccc; ">
            <td></td>
            <td></td>
          </tr>
          <tr style="background-color: #ccc;">
            <td style="font-size: 12px; vertical-align: top; padding-left: 10px; padding-top: 10px; color:#666666;">Color:</td>
            <td> <?php
             if($productColor && $productColor != ''){?>
              <select name="productColor" id="productColor">
                <?php
                  echo '<option value="">Select</option>';
                foreach($productColor as $color){
                  ?>
                <option myval="<?php echo count($productColor);?>" value="<?php echo base64_encode($color->id);?>"> <?php echo $color->color;?></option>
                <?php
                }
                ?>
              </select>
              <?php
      }
      else
      echo '<span style="font-size: 12px; vertical-align: top; padding-top: 10px; padding-right: 10px; color:#666666;">Colour is not available for this product.</span>';
      ?></td>
          </tr>
          <tr style="background-color: #ccc;">
            <td style="font-size: 12px; vertical-align: top; padding-left: 10px; padding-top: 10px; color:#666666;">Size:</td>
            <td><?php
            if($productSize && $productSize != ''){?>
              <select name="productSize" id="productSize">
                <?php
                  var_dump($productSize);
                foreach($productSize as $size){
                  if(count($productSize) > 1)
                  {
                    echo '<option value="">Select</option>';
                  }
                ?>
                <option value="<?php echo base64_encode($size->id);?>"><?php echo $size->size;?></option>
                <?php
        }
        ?>
              </select>
              <?php
      }
      else
      echo '<span style="font-size: 12px; vertical-align: top; padding-top: 10px; padding-right: 10px; color:#666666;">Size is not available for this product.</span>';
      ?></td>
          </tr>
          <tr style="background-color: #ccc;">
            <td style="font-size: 12px; vertical-align: top; padding-left: 10px; padding-top: 10px; color:#666666;">Quantity:</td>
            <td>
            <?php
            if(!$soldOutPro){ // check product has sold out
            ?>
            <select name="productQty" id="productQty">
            <?php //echo "<!--"; print_r($productDetail); echo "!-->";
            for($i = 1 ; $i <= $productQty; $i ++){?>
              <option value="<?php echo $i;?>"><?php echo $i;?></option>
            <?php
            }
            ?>
            </select>
            <?php
            }
            else
            echo "0";
            ?>
            </td>
          </tr>
          <tr style="background-color: #ccc;">
            <td style="font-size: 12px; vertical-align: top; padding-top: 10px; padding-left: 10px; color:#666666;">Return Policy:</td>
            <td style="font-size: 12px; vertical-align: top; padding-top: 10px; padding-right: 10px; color:#666666;">Returns accepted if product not as described, buyer pays return shipping; or keep the product & agree refund with seller.</td>
          </tr>
          <tr style="background-color: #ccc;">
            <td style="font-size: 12px; vertical-align: top; padding-top: 10px; padding-left: 10px; color:#666666;">Merchant Guarantees:</td>
            <td style="font-size: 12px; vertical-align: top; padding-top: 10px; padding-right: 10px; color:#666666;">On-time Delivery</td>
          </tr>
          <tr style="height:10px; background-color: #ccc; ">
            <td></td>
            <td></td>
          </tr>
          <tr style="background-color: #ccc;">
            <td style="padding-left: 10px; padding-right: 5px;"><button style="height: 30px; padding:2px 18px;" type="button" class="btn bg-orange btn-block buy-now-btn">Buy Now</button></td>
            <td style="padding-left: 5px; padding-right: 10px;"><button style="height: 30px; padding:2px 18px;" type="button" class="btn bg-dark-orange btn-block add-to-cart-btn">Add to Cart</button>
              <input type="hidden" name="productId" id="productId" value="<?php echo base64_encode($productDetail->id);?>"  /></td>
          </tr>
          <tr style="height:10px; background-color: #ccc; ">
            <td></td>
            <td></td>
          </tr>
        </table>
      </form>
      <table style="margin-top: 20px;">
        <tr>
          <td style="font-size: 12px; vertical-align: top; padding-top: 10px; padding-right: 5px; color:#666666;" rowspan="2">Buyer's Protection:</td>
          <td style="font-size: 12px"><img src="<?php echo base_url().'assets/img/ticker.png';?>"> Full refund on all items within 7 days</td>
        </tr>
        <tr>
          <td style="font-size: 12px"><img src="<?php echo base_url().'assets/img/ticker.png';?>"> Free returns within 7 days</td>
        </tr>
      </table>
    </div>
    <div class="product_merchant_detail">
      <div class="merchant_box_large">
        <div class="merchant_box" style="width: 280px;">
          <?php
          $merchantImg = base_url().'assets/img/newmerchant.jpg';
          if($merchantDetail->image && $merchantDetail->image != '')
          {
          $merchantImg = base_url().'banner_images/'.$merchantDetail->image;
          }
          ?>
          <table width="100%" style="color: #000000; margin-left:7px; ">
            <tr>
              <td rowspan="3" style="width:40%;"><div class="merchant_box_picture_small" style="background-image:url(<?php echo $merchantImg;?>);"></div></td>
              <td style="font-size: 11px; color: #9BC534;"> Sold by </td>
            </tr>
            <tr>
              <td style="color: #00597D;"><b><?php echo $merchantDetail->display_name;?></b> <img style="float: right; margin-right: 20px;" src="<?php echo base_url().'assets/img/ticker.png';?>"></td>
            </tr>
            <?php
            $merCountry = '';
                  if($merchantDetail->country != '' && $merchantDetail->country == 'NG')
            $merCountry  = 'Nigeria';
                  if($merchantDetail->country != '' && $merchantDetail->country == 'US')
            $merCountry  = 'United States';
            ?>
            <tr>
              <td><?php echo $merchantDetail->category_name;?>, <?php echo $merCountry; ?></td>
            </tr>
            <tr>
              <td style="color: #666666"><?php if($relatedProducts && $relatedProducts != '') echo count($relatedProducts); else echo '0';?>
                items</td>

            </tr>
          </table>
        </div>
      </div>
      <div class="merchant_box_large" style="margin-bottom: 15px; height: 350px; overflow-y: auto;">
        <div style="border-bottom:1px solid #8C8C8C; margin-bottom: 20px; margin-top: 20px; width: 100%; float: right;"></div>
        <div style="clear: both;"></div>
        <?php
        if($merchantDetail && $merchantDetail->description != '')
        {
          echo  $merchantDetail->description;
        }
        ?>
      </div>
      <div class="merchant_box_large" style=" height: auto; width: 100%">
        <div style="width: 100px; margin: auto;"> <a style="height: 30px; padding:2px 18px;" href="<?php echo base_url().$merchantDetail->slug;?>"  class="btn bg-sub btn-block">Visit Store</a> </div>
        <div style="border-bottom:1px solid #8C8C8C; margin-bottom: 20px; margin-top: 20px; width: 100%; float: right;"></div>
      </div>
      <div class="merchant_box_large" style=" height: auto;">
        <p style="font-size: 15px; color: #9BC534; margin: 0;">Contact Merchant</p>
        <p style="font-size: 20px; margin: 0;"><a myval="ContactUs" class="clickme" href="javaScript:;"><img src="<?php echo base_url().'assets/img/message.png';?>" style="margin-right: 15px">Contact Now</a></p>
        <p style="font-size: 20px; margin: 0;"><a myval="PhoneNumber" class="clickme" href="javaScript:;"><img src="<?php echo base_url().'assets/img/message.png';?>" style="margin-right: 15px">Call Now</a></p>
      </div>
    </div>
    </div>
    <div class="container" style="position:relative">
    <!--=========================Merchant Contact Us Div==========================-->
    <div class="ContenerDivShow DropdownDiv col-md-3" id="ContactUs" style="display:none">
      <div>
        <div class="fill-details">
          <div class="fd-header"> Contact Us
            <div class="close closeDropdownDiv">X</div>
          </div>
          <div class="fd-body">
            <div class="row">
              <form role="form" method="post" id="merchantContact">
                <div class="col-md-12">
                  <label class="control-label">First Name</label>
                  <div class="form-group">
                    <input type="text" id="txtName"  required name="txtName" class="form-control">
                  </div>
                </div>
                <div class="col-md-12">
                  <label class="control-label">Email</label>
                  <div class="form-group">
                    <input type="text" name="txtEmail" id="txtEmail" class="form-control">
                  </div>
                </div>
                <div class="col-md-12">
                  <label class="control-label">Phone Number</label>
                  <div class="form-group">
                    <input type="text" id="txtPhone" name="txtPhone" class="form-control">
                  </div>
                </div>
                <div class="col-md-12">
                  <label class="control-label">Message</label>
                  <div class="form-group">
                    <textarea id="txtMessage" cols="3" role="3"  required name="txtMessage" class="form-control"></textarea>
                  </div>
                </div>
                <div class="btn-container col-md-12">
                  <div class="btn-inner">
                    <input type="hidden" id="txtMerchant" name="txtMerchant" value="<?php echo base64_encode($merchantDetail->id);?>">
                    <button class="btn bg-success btn-block" id="ContactUsBtu" type="button" name="SendMAil">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!--=========================Merchant Phone Number Div==========================-->
    <div class="ContenerDivShow DropdownDiv col-md-3" id="PhoneNumber" style="display:none">
      <div>
        <div class="fill-details">
          <div class="fd-header"> Phone Number
            <div class="close closeDropdownDiv">X</div>
          </div>
          <div class="fd-body">
            <div class="row">
              <div class="col-md-12 text-center"> <strong><?php echo $merchantDetail->phone; ?></strong> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="product_display">
  <div class="container" style="padding-bottom: 10px">
    <div style="float: left; color: #009AB1; font-size: 25px;">Featured Products from this merchant</div>
  </div>
  <div class="container">
    <div class="related-product">
      <?php if($relatedProducts && $relatedProducts != ''){ ?>
      <div class="customNavigation"> <a class="prev"><img src="<?php echo base_url()?>assets/img/greenarrow-left.png" style="cursor: pointer;padding-right: 6px;"></a> <a class="next"><img src="<?php echo base_url()?>assets/img/greenarrow-right.png" style="cursor: pointer;"></a> </div>
      <?php }
  else
    echo '<div class="no-record text-center">No Record Found!</div>';
  ?>
      <div class="owl-carousel-related-product">
        <?php
        if($relatedProducts && $relatedProducts  != '')
        {
            $count_div = 1;
            $total_products = count($relatedProducts);
            $count_row =  1;
            foreach($relatedProducts as $rows)
            {
               $merchantLogo = '';
               if($rows->image != '')
               $productsImg = 'product_images/'.$rows->image;
               else
               $productsImg = 'assets/img/logo.png';

              if($total_products < 8)
              {

                if($total_products < 5 ) // 4 items
                {
                ?>
                  <div class="items">
                    <div class="product-box"> <a href="<?php echo base_url().'product/'.base64_encode($rows->id);?>">
                      <div class="image"><img height="200" src="<?php echo base_url().$productsImg;?>"></div>
                      <div class="details">
                        <table width="100%">
                          <tr>
                            <td colspan="2" style="color: #ffffff;height: 42px;vertical-align: top;">
                            <?php
							if($rows->product_name != '')
							{
							  if(strlen($rows->product_name) > 36)
							  {

							    echo substr($rows->product_name, 0,  (32 - strlen($rows->product_name)))."...." ;

							  }
							  else
							  echo $rows->product_name;
							}
							?>
                            </td>
                          </tr>
                          <tr>
                            <td style="color: #A8C738; font-size: 22px; font-weight: 600;">&#x20A6;<?php echo number_format($rows->product_price); ?></td>
                            <td><input type="button" class="btn-block btn btn-cart" value="add to cart"></td>
                          </tr>
                        </table>
                      </div>
                      </a>
                    </div>
                  </div>
                <?php
                }
                if($total_products == 5 ) // 5 items
                {

                  if($count_row != 2)
                  {
                  ?>
                    <div class="items">
                   <?php
                  }
                   ?>
                    <div class="product-box"> <a href="<?php echo base_url().'product/'.base64_encode($rows->id);?>">
                      <div class="image"><img height="200" src="<?php echo base_url().$productsImg;?>"></div>
                      <div class="details">
                        <table width="100%">
                          <tr>
                            <td colspan="2" style="color: #ffffff;height: 42px;vertical-align: top;">
                            <?php
							if($rows->product_name != '')
							{
							  if(strlen($rows->product_name) > 36)
							  {

							    echo substr($rows->product_name, 0,  (32 - strlen($rows->product_name)))."...." ;

							  }
							  else
							  echo $rows->product_name;
							}
							?>
                            </td>
                          </tr>
                          <tr>
                            <td style="color: #A8C738; font-size: 22px; font-weight: 600;">&#x20A6;<?php echo number_format($rows->product_price); ?></td>
                            <td><input type="button" class="btn-block btn btn-cart" value="add to cart"></td>
                          </tr>
                        </table>
                      </div>
                      </a>
                    </div>
                    <?php
                    if($count_row != 1)
                    {
                    ?>
                    </div>
                    <?php
                    }
                    ?>
                <?php
                }

                if($total_products == 6 )// 6 items
                {

                  $flag = 0;
                  if($count_row == 2)
                  $flag = 1;

                  if($count_row == 4)
                  $flag = 1;

                  if($flag == 0)
                  {
                  ?>
                    <div class="items">
                 <?php
                  }
                   ?>
                    <div class="product-box"> <a href="<?php echo base_url().'product/'.base64_encode($rows->id);?>">
                      <div class="image"><img height="200" src="<?php echo base_url().$productsImg;?>"></div>
                      <div class="details">
                        <table width="100%">
                          <tr>
                            <td colspan="2" style="color: #ffffff;height: 42px;vertical-align: top;">
                            <?php
							if($rows->product_name != '')
							{
							  if(strlen($rows->product_name) > 36)
							  {

							    echo substr($rows->product_name, 0,  (32 - strlen($rows->product_name)))."...." ;

							  }
							  else
							  echo $rows->product_name;
							}
							?>
                            </td>
                          </tr>
                          <tr>
                            <td style="color: #A8C738; font-size: 22px; font-weight: 600;">&#x20A6;<?php echo number_format($rows->product_price); ?></td>
                            <td><input type="button" class="btn-block btn btn-cart" value="add to cart"></td>
                          </tr>
                        </table>
                      </div>
                      </a>
                    </div>
                    <?php

                  $flag1 = 0;
                  if($count_row == 1)
                  $flag1 = 1;

                  if($count_row == 3)
                  $flag1 = 1;

                  if($flag1 == 0)
                  {
                  ?>
                    </div>
                  <?php
                  }
                  ?>
                <?php
                }

                if($total_products == 7 )// 7 items
                {

                  $flag = 0;
                  if($count_row == 2)
                  $flag = 1;

                  if($count_row == 4)
                  $flag = 1;

                  if($count_row == 6)
                  $flag = 1;

                  if($flag == 0)
                  {
                  ?>
                    <div class="items">
                 <?php
                  }
                   ?>
                    <div class="product-box"> <a href="<?php echo base_url().'product/'.base64_encode($rows->id);?>">
                      <div class="image"><img height="200" src="<?php echo base_url().$productsImg;?>"></div>
                      <div class="details">
                        <table width="100%">
                          <tr>
                            <td colspan="2" style="color: #ffffff;height: 42px;vertical-align: top;">
                            <?php
							if($rows->product_name != '')
							{
							  if(strlen($rows->product_name) > 36)
							  {

							    echo substr($rows->product_name, 0,  (32 - strlen($rows->product_name)))."...." ;

							  }
							  else
							  echo $rows->product_name;
							}
							?>
                            </td>
                          </tr>
                          <tr>
                            <td style="color: #A8C738; font-size: 22px; font-weight: 600;">&#x20A6;<?php echo number_format($rows->product_price); ?></td>
                            <td><input type="button" class="btn-block btn btn-cart" value="add to cart"></td>
                          </tr>
                        </table>
                      </div>
                      </a>
                    </div>
                    <?php

                  $flag1 = 0;
                  if($count_row == 1)
                  $flag1 = 1;

                  if($count_row == 3)
                  $flag1 = 1;

                  if($count_row == 5)
                  $flag1 = 1;

                  if($flag1 == 0)
                  {
                  ?>
                    </div>
                  <?php
                  }
                  ?>
                <?php
                }

              }
              else
              {

                if($count_div == 1)
                {
                ?>
                <div class="items">
                <div class="product-box"> <a href="<?php echo base_url().'product/'.base64_encode($rows->id);?>">
                  <div class="image"><img height="200" src="<?php echo base_url().$productsImg;?>"></div>
                  <div class="details">
                   <table width="100%">
                  <tr>
                  <td colspan="2" style="color: #ffffff;height: 42px;vertical-align: top;">
                  <?php
					if($rows->product_name != '')
					{
					  if(strlen($rows->product_name) > 36)
					  {

					    echo substr($rows->product_name, 0,  (32 - strlen($rows->product_name)))."...." ;

					  }
					  else
					  echo $rows->product_name;
					}
					?>
                  </td>
                  </tr>
                  <tr>
                  <td style="color: #A8C738; font-size: 22px; font-weight: 600;">&#x20A6;<?php echo number_format($rows->product_price); ?></td>
                  <td><input type="button" class="btn-block btn btn-cart" value="add to cart"></td>
                  </tr>
                  </table>
                  </div>
                  </a> </div>
                <?php
                 // close (last) even div...
                  if (($total_products % 2 == 1) && $total_products == $count_row) {
                  ?>
                 </div>
                 <?php
                  }

                }// end of if...

                if($count_div == 2)
                {
                ?>
                <div class="product-box"> <a href="<?php echo base_url().'product/'.base64_encode($rows->id);?>">
                <div class="image"><img height="200" src="<?php echo base_url().$productsImg;?>"></div>
                <div class="details">
                  <table width="100%">
                  <tr>
                  <td colspan="2" style="color: #ffffff; height: 42px;vertical-align: top;">
                  <?php
					if($rows->product_name != '')
					{
					  if(strlen($rows->product_name) > 36)
					  {

					    echo substr($rows->product_name, 0,  (32 - strlen($rows->product_name)))."...." ;

					  }
					  else
					  echo $rows->product_name;
					}
					?></td>
                  </tr>
                  <tr>
                  <td style="color: #A8C738; font-size: 22px; font-weight: 600;">&#x20A6;
                  <?php  echo number_format($rows->product_price); ?></td>
                  <td><input type="button" class="btn-block btn btn-cart" value="add to cart"></td>
                  </tr>
                  </table>
                </div>
                </a> </div>
                </div>
                <?php
                $count_div = 0;
                } // end of if...


              } // end of else
              $count_div ++;
              $count_row ++;
            }
        }
   ?>
    </div>
  </div>
  </div>
</section>
