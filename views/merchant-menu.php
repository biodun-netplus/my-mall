<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="merchant_menu">
  <div class="container">
    <div class="display-div" style="height: auto;">
      <div class="user_profile">
        <div class="col-xs-2 user_img">
          <?php if($loginUserDetail->image != ''){?>
          <img src="<?php echo base_url()?>provider_images/<?php echo $loginUserDetail->image?>" width="176" height="180" />
          <?php } else { ?>
          <img src="<?php echo base_url()?>assets/img/no-user.png" class="img-rounded" width="80" />
          <?php } ?>
          <a href="edit-profile.php"><span class="glyphicon glyphicon-pencil"></span> Edit</a> </div>
        <div class="col-sm-10  user_r_con">
          <h3><?php echo $loginUserDetail->display_name?></h3>
        </div>
        <div class="clearfix"></div>
        <div  class="col-sm-12">
          <ul class="profile_link">
            <li class="b_width">&nbsp;</li>
            <li><a href="<?php echo base_url();?>profile.php">Profile</a></li>
            <li><a href="<?php echo base_url();?>edit-profile.php">Edit Profile</a></li>
            <li><a href="<?php echo base_url();?>product">Product</a></li>
            <li><a href="<?php echo base_url();?>add-banner.php">Banner</a></li>
            <li><a href="<?php echo base_url();?>merchant-transaction-record.php">Transaction</a></li>
            <div class="clearfix"></div>
          </ul>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</section>
