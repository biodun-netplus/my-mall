<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="merchant_display" style="margin-top:0px">
  <div class="breadcrumb"> <a href="<?php echo base_url();?>"> Home </a> > <a href=""> Payment </a> > <?php if($cartDetail  && $cartDetail != '') { ?> <a href="<?php echo ucwords(strtolower($cartDetail[0]->slug)); ?>"><?php echo ucwords(strtolower($cartDetail[0]->display_name)); ?></a> <?php } ?> </div>
</section> 

<section class="merchant_display update-qty-alert" style="margin-top: 0px; display:none;">
<div class="container"></div>
</section>

<section class="merchant_display" style="margin-top: 0px" >
  <div class="main_cart_con">
    <div class="main_cart_lft" >
      <?php
	 $amount  =  0;
	 if($cartDetail  && $cartDetail != '')
	 {
		 $groupMerchant	= ''; 

		 foreach($cartDetail as $rows)
		 {
			 
       $merchantImg   = base_url().'assets/img/newmerchant.jpg';
       $productImg  = base_url().'assets/img/logo.png';

       if($rows->merchant_img != '')
			 $merchantImg = base_url().'banner_images/'.$rows->merchant_img;
			 
			 if($rows->product_img != '')
			 $productImg  = base_url().'product_images/'.$rows->product_img;
			 
			 $amount  	  = ($rows->price * $rows->quantity) + $amount;
			 
	?>
      <div id="hide-row-<?php echo $rows->id;?>">
        <?php
		if($groupMerchant !=  $rows->merchant_id)
		{
			//$groupMerchant = $rows->merchant_img;
		?>
        <div style="padding: 5px"><img width="100" src="<?php echo $merchantImg;?>" style="padding-left: 10px;"></div>
        <?php
		}
		?>
        <table width="100%">
        <?php
		if($groupMerchant !=  $rows->merchant_id)
		{
			$groupMerchant = $rows->merchant_id;
		?>
        
          <tr style="color: #fff;background-color: #ccc;">
            <td style="padding-left: 15px">ITEM</td>
            <td></td>
            <td>ITEM PRICE</td>
            <td>QUANTITY</td>
            <td>TOTAL</td>
            <td>&nbsp;</td>
          </tr>
       <?php
		}

    /********  get size, color and qty ***********/

    $productQty           = 0;
    $productSize          = "";
    $productColor         = "";
    $productdetail        = "";

    if($rows->color != '' && $rows->size != '' )
    {

      $productdetail     = $this->product_model->relate_product_size_colour(array('products.id'=>$rows->product_id,'relation_color_size.colour_id'=>$rows->color,'relation_color_size.size_id'=>$rows->size));

      //echo "<!----";  //print_r($productdetail);  //echo "!-->";
      if($productdetail && $productdetail != '')
      {
      
        foreach ($productdetail as $prodDeatil) 
        {

          if($prodDeatil->color && $prodDeatil->color != '')
          {
              $productColor   = $prodDeatil->color;

          }
          if($prodDeatil->size && $prodDeatil->size != '')
          {
              $productSize   = $prodDeatil->size;

          }

        }

      }

    }
    else if($rows->size != '' )
    {

      $productdetail     = $this->product_model->relate_product_size(array('relation_color_size.size_id'=>$rows->size ,'products.id'=>$rows->product_id )); 
      //print_r($productdetail);
      
      if($productdetail && $productdetail != '')
      {
      
        foreach ($productdetail as $prodDeatil) 
        {

         if($prodDeatil->size && $prodDeatil->size != '')
          {
              $productSize   = $prodDeatil->size;

          }

        }

      }

    }
    else if($rows->color  != '' )
    {

      $productdetail     = $this->product_model->relate_product_colour(array('relation_color_size.colour_id'=> $rows->color,'products.id'=>$rows->product_id )); 

      if($productdetail && $productdetail != '')
      {
      
        foreach ($productdetail as $prodDeatil) 
        {

          if($prodDeatil->color && $prodDeatil->color != '')
          {
              $productColor   = $prodDeatil->color;

          }

        }

      }

    }
    else 
    {

      $productdetail     = $this->product_model->get_product_size_colour_by_product(array('relation_color_size.product_id'=>$rows->product_id));
      //echo "<!--"; print_r($productdetail); echo "!-->";

    }


		?>
          <tr>
            <td style="padding-left: 15px" width="19%"><img width="100" src="<?php echo $productImg;?>"></td>
            <td width="47%" style="padding: 0 5px;"><p style="margin: 0;"><?php echo $rows->product_name;?></p>
              <?php 
      			  if($rows->product_detail != '')
      			  {
      				  $countStrLen = strlen(strip_tags($rows->product_detail));
      				  echo substr(strip_tags($rows->product_detail),0,(40-$countStrLen));
      				  if($countStrLen > 40)
      				  echo '...';
      			  }
              if($productColor != '')
              {
                 echo "<br>Color: ".$productColor;
              }
              if($productSize != '')
              {
                 echo "<br>Size: ".$productSize;
              }
      			  ?>
            </td>
            <td width="12%"><p style="margin: 0; color: darkred">&#x20A6;<?php echo number_format($rows->product_price);?></p></td>
            <td width="10%">
              <select style="width: 60px;text-align: center; padding: 5px" id="new-qty<?php echo $rows->id;?>"> 
              <?php
	    				//echo "<!----"; print_r($productdetail); echo "!-->";
      				if($productdetail && $productdetail != '')
      				{
      			    $qtyFlag = 0;
      					foreach ($productdetail as $rowQty) {
                  # code...
                  if($qtyFlag == 0)
                  {
                    for($i = 1; $i <= $rowQty->qty; $i++ )
          					{
          					?>
          					<option value="<?php echo $i;?>" <?php if($i == $rows->quantity){ ?> selected="selected" <?php } ?>><?php echo $i;?></option>
          					<?php
          					}
                  }
                  $qtyFlag ++;
                }
      				}
              else
              {  
                  for($i = 1; $i <= $rows->product_quantity; $i++ )
                  {
                  ?>
                  <option value="<?php echo $i;?>" <?php if($i == $rows->quantity){ ?> selected="selected" <?php } ?>><?php echo $i;?></option>
                  <?php
                  }
                
              }
              ?>
              </select>
              <input type="hidden" id="prodSize-<?php echo $rows->id;?>" value="<?php echo base64_encode($rows->size);?>"  />
              <input type="hidden" id="prodColor-<?php echo $rows->id;?>" value="<?php echo base64_encode($rows->color);?>"  />
            <br /><a href="javascript:;" class="update-qty" myval="new-qty<?php echo $rows->id;?>" myattr="<?php echo $rows->product_id;?>">Update</a>
            </td>
            <td width="12%"><p style="margin: 0; color: darkred" id="new-cost<?php echo $rows->product_id;?>">&#x20A6;<?php echo number_format($rows->price * $rows->quantity);?></p></td>
            <td width="12%"><p style="margin: 0;"><a href="javascript:;" id="catId_<?php echo $rows->id;?>" class="remove-product"> <img src="<?php echo base_url().'assets/img/delete_cart.png'?>" style="float: right; padding-right: 20px;"></a></p></td>
          </tr>
        </table>
        <div style="border-bottom:1px solid #8C8C8C; margin-bottom: 10px; margin-top: 10px; width: 100%;"></div>
      </div>
      <?php
		 }
	 }
	 ?>
    </div>

    <div class="main_cart_rgt" >
      <p>CUSTOMER DETAILS</p>
      <div style="border-bottom:1px solid #8C8C8C; margin-bottom: 10px; margin-top: 10px; width: 100%;"></div>
      <div class="row1 move-to-shipping-address">
      <?php  
	  if(isset($_SESSION['customerShippingAddError']))
	  {
	  ?>
      <div role="alert" class="alert alert-danger">
          <?php  echo $_SESSION['customerShippingAddError'];?>
        </div>
      <?php
	  	unset($_SESSION['customerShippingAddError']);
	  }
	  if( isset($_POST['txtEmail']) && empty($shippingAdd) )
	  {
	  ?>
        <div role="alert" class="alert alert-danger">Oops we couldn't find delivery details for the email address
          <?php if(isset($_POST['txtEmail'])) echo $_POST['txtEmail'];?>
        </div>
        <?php 
	  }
	  else if(isset($_COOKIE['customerShippingEmail']) && $_COOKIE['customerShippingEmail'] != '')
	  {
	  ?>
        <p style="font-size: 14px; margin-bottom: 1px; color: #000000; text-align: left;">
          <?php if(isset($_COOKIE['customerShippingEmail'])) echo $_COOKIE['customerShippingEmail'];?>
        </p>
        <p style="font-size: 14px; margin-bottom: 1px; color: #000000; text-align: left;">
          <?php if(isset($_COOKIE['customerShippingPhone'])) echo $_COOKIE['customerShippingPhone'];?>
        </p>
        <p style="font-size: 14px; margin-bottom: 15px; color: #000000; text-align: left;"><a href="<?php echo base_url();?>cart/reset-detail">Change email</a></p>
        <p style="font-size: 14px; margin-bottom: 5px; color: #E74C3C; text-align: left;">Select delivery location </p>
        <?php 
		if($shippingAdd && $shippingAdd != '')
		{
			if(count($shippingAdd) == 1)
			{
			?>
          <div style="padding: 5px; background-color: #efefef; margin-bottom: 10px; height: auto; border: 1px solid #8c8c8c">
           <div class="form-group text-left">   
          <input name="radioShippingAdd" type="radio" value="<?php echo base64_encode($shippingAdd->id);?>" class="radioShippingAdd">
          <b><?php echo $shippingAdd->delivery_contact_name;?></b> <br />
          <?php echo $shippingAdd->delivery_addres;?>
          <br />
          <?php if($shippingAdd && $shippingAdd->delivery_area) echo $shippingAdd->delivery_area;?> 
          <br />
          <?php if($shippingAdd && $shippingAdd->contact_no_delivery) echo $shippingAdd->contact_no_delivery;?>
          </div>
        </div>            
            
      <?php	
			}
			else
			{
			foreach($shippingAdd as $shipAdd)
			{
		?>	
        
        <div style="padding: 5px; background-color: #efefef; margin-bottom: 10px; height: auto; border: 1px solid #8c8c8c">
           <div class="form-group text-left">   
          <input name="radioShippingAdd" type="radio" value="<?php echo base64_encode($shipAdd->id);?>" class="radioShippingAdd">
          <b><?php echo $shipAdd->delivery_contact_name;?></b> <br />
          <?php echo $shipAdd->delivery_addres;?>
          <br />
          <?php if($shipAdd && $shipAdd->delivery_area) echo $shipAdd->delivery_area;?> 
          <br />
          <?php if($shipAdd && $shipAdd->contact_no_delivery) echo $shipAdd->contact_no_delivery;?>
          </div>
        </div>
        <?php
			}
			}
		}
		?>
        
        <p style="font-size: 14px; margin-bottom: 5px; margin-top:10px; color: #E74C3C; text-align: left;"><a class="clickme" href="javascript:;" myval="customer-detail">Ship to new address?</a></p>
        <form role="form" action="<?php echo base_url();?>cart/new-address" id="customer-detail" method="post" name="customerDetail" style="display:none;">
          <div class="col-lg-121">
            <div class="form-group text-left">
              <label>Contact name for delivery</label>
              <input class="form-control validate[required]" type="text" id="txtDeliveryContactName" name="txtDeliveryContactName" />
            </div>
            <div class="form-group text-left">
              <label>Delivery Address</label>
              <textarea id="txtDeliveryAddress" name="txtDeliveryAddress" class="form-control validate[required]" rows="3"></textarea>
            </div>
            <div class="form-group text-left">
              <label>Deliveryarea</label>
              <select class="form-control validate[required]" name="selectDeliveryarea" id="selectDeliveryarea">
                <option value="">Deliveryarea</option>
                <?php
                 if($states_merchant)
                 {
                    foreach($states_merchant as $row)
                    {
                        
                 ?>
                <option value="<?php echo $row->name; ?>"><?php echo $row->name; ?></option>
                <?php
                    }
                 }
                 ?>
              </select>
            </div>
            <div class="form-group text-left">
              <label>Contact number for delivery</label>
              <input type="text" id="txtDeliveryContactNo" name="txtDeliveryContactNo" class="form-control validate[required,custom[phone]]">
            </div>
            <div>
              <button name="submit" value="Add Address" class="btn bg-orange" type="submit" style="padding:2px 18px;">Add Address</button>
            </div>
          </div>
        </form>
        <?php
	  }
	  if(!isset($_COOKIE['customerShippingEmail']))
	  {
	  ?>
        <form role="form" action="<?php echo base_url();?>cart/customer-detail" id="customer-detail" method="post" name="customerDetail">
          <div class="col-lg-121">
            <div class="form-group text-left">
              <label>Email</label>
              <input class="form-control validate[required]" type="text" name="txtEmail" id="txtEmail" value="">
            </div>
            <div class="form-group text-left">
              <div>
                <input type="radio" name="checkRetrieveDelivery" class="checkRetrieveDelivery " id="checkRetrieveDelivery" value="1">
                <label>Retrieve my delivery details</label>
              </div>
            </div>
            <div class="form-group text-left">
              <label>Contact name for delivery</label>
              <input class="form-control validate[required]" type="text" id="txtDeliveryContactName" name="txtDeliveryContactName" />
            </div>
            <div class="form-group text-left">
              <label>Phone</label>
              <input type="text" id="txtPhone" name="txtPhone" class="form-control validate[required,custom[phone]]" value="">
            </div>
            <div class="form-group text-left">
              <label>Delivery Address</label>
              <textarea id="txtDeliveryAddress" name="txtDeliveryAddress" class="form-control validate[required]" rows="3"></textarea>
            </div>
            <div class="form-group text-left">
              <label>Deliveryarea</label>
              <select class="form-control validate[required]" name="selectDeliveryarea" id="selectDeliveryarea">
                <option value="">Deliveryarea</option>
                <?php
                 if($states_merchant)
                 {
                    foreach($states_merchant as $row)
                    {
                        
                 ?>
                <option value="<?php echo $row->name; ?>"><?php echo $row->name; ?></option>
                <?php
                    }
                 }
                 ?>
              </select>
            </div>
            <div class="form-group text-left">
              <label>Contact number for delivery</label>
              <input type="text" id="txtDeliveryContactNo" name="txtDeliveryContactNo" class="form-control validate[required,custom[phone]]">
            </div>
            <div>
              <button name="submit" value="Proceed" class="btn bg-orange" type="submit" style="padding:2px 18px;">Proceed</button>
            </div>
          </div>
        </form>
        <?php
	  }
	  ?>
        <div style="border-bottom:1px solid #8C8C8C; margin-bottom: 10px; margin-top: 10px; width: 100%;"></div>
        <table width="100%">
          <?php 
          if(isset($_SESSION['Check']) && $_SESSION['Check'] != ""){?>
          <tr>
            <td style="text-align: left" width="50%">Transaction ID</td>
            <td style="text-align: right" width="30%"><?php echo $_SESSION['Check']; ?></td>
          </tr>
          <?php } ?>
          <tr>
            <td style="text-align: left" width="50%">Amount</td>
            <td style="text-align: right" width="30%" id="total-amt">&#x20A6;<?php echo number_format($amount);?></td>
          </tr>
          <tr>
            <td style="text-align: left" width="50%">Delivery</td>
            <td style="text-align: right" width="30%" id="delivery-amt">&#x20A6;0,00</td>
          </tr>
          <tr class="coupon-calc" <?php if($ordersDetail && $ordersDetail->redeemed_total == ""){ ?>style="display:none;"<?php } ?> >
            <td style="text-align: left; font-size: 16px; font-weight: 600;" width="50%">Coupon Value</td>
            <td style="text-align: right; font-size: 16px; font-weight: 600;" width="30%" id="coupon-total">-&#x20A6;<?php if($ordersDetail->redeemed_total > 0 )echo number_format($ordersDetail->redeemed_total);?></td>
          </tr>
        </table>
        <div style="border-bottom:1px solid #8C8C8C; margin-bottom: 10px; margin-top: 10px; width: 100%;"></div>
        <table width="100%">
          <tr>
            <td style="text-align: left; font-size: 16px; font-weight: 600;" width="50%">Total Amount</td>
            <td style="text-align: right; font-size: 16px; font-weight: 600;" width="30%" id="grand-total">&#x20A6;<?php echo number_format($amount);?></td>
          </tr>
        </table>
        <div style="margin-top: 5%; display:none;" id="proceed-to-checkout-div"></div>
      </div>
    </div>
  </div>
</section>

<section class="merchant_display" style="margin-top: 10px"  id="move-to-checkout">
  <div class="breadcrumb" style="font-size: 16px;"> CHOOSE YOUR PREFERRED PAYMENT OPTION
    <div style="border-bottom:1px solid #8C8C8C; margin-bottom: 10px; margin-top: 10px; width: 100%;"></div>
  </div>
</section>

<section class="merchant_display" style="margin-top: 10px; min-height: 120px;">
  <div class="breadcrumb">
    <div class="payment_box_holder" style="display:none;" >
      <div class="payment_box netpluspay">
        <input type="radio" name="radio-payment" class="radio-payment" alt="netpluspay" >
        &nbsp;<img src="<?php echo base_url().'assets/img/netpluspay.png'?>"> </div>
      INTERNET BANKING </div>

    <div class="payment_box_holder" style="display:none;">
      <div class="payment_box">
        <input type="radio" name="radio-payment" class="radio-payment" alt="ecobankcards" >
        &nbsp;<img src="<?php echo base_url().'assets/img/ecobankcards.png'?>"> </div>
      CARD </div>

    <div class="payment_box_holder">
      <div class="payment_box">
        <input type="radio" name="radio-payment" class="radio-payment" alt="interswitchng" >
        &nbsp;<img src="<?php echo base_url().'assets/img/interswitchng.png'?>" style="width: 120px; height: 50px; max-width: 100%;"> </div>
      Interswitchng </div>
      <div class="payment_box_holder">
          <div class="payment_box">
              <input type="radio" name="radio-payment" class="radio-payment" alt="netpluspay" >
              &nbsp;<img src="<?php echo base_url().'assets/img/netpluspay.png'?>" style="width: 120px; height: 50px; max-width: 100%;"> </div>
          NetplusPay </div>
      <div class="payment_box_holder">
      <div class="payment_box">
        <input type="radio" name="radio-payment" class="radio-payment" alt="pod" >
        &nbsp;<img src="<?php echo base_url().'assets/img/pod.png'?>" style="width: 120px; height: 50px; max-width: 100%;"> </div>
      Pay On Delivery </div>
      <div class="payment_box_holder">
          <div class="payment_box">
              <input type="radio" name="radio-payment" class="radio-payment" alt="bankdeposit" >
              &nbsp;<img src="<?php echo base_url().'assets/img/ecobank.png'?>" style="width: 120px; height: 50px; max-width: 100%;"> </div>
          Bank Deposit </div>

    <div class="payment_box_holder" style="display:none;">
      <div class="payment_box">
        <input type="radio" name="radio-payment" class="radio-payment"  alt="ecobank">
        &nbsp;<img src="<?php echo base_url().'assets/img/ecobank.png'?>"> </div>
      BOOK-ON-HOLD </div>

    <div class="payment_box_holder" style="display:none;">
      <div class="payment_box">
        <input type="radio" name="radio-payment" class="radio-payment"  alt="payOnDelivery">
        &nbsp;<img src="<?php echo base_url().'assets/img/ecobank.png'?>"> </div>
       PAY ON DELIVERY </div>

    <div class="payment_box_holder" style="float: right">
      <div class="button_box">
        
        <button class="btn bg-orange btn-block check-out" type="submit" style="height: 30px; padding:2px 18px;">CHECK OUT</button>
        
        <!-------------Access Api Implement-->
        <div id="ecobankcards" style="display:none;" class="PaymentDiv">
        </div>
        <!-------------netpluspay Api-->
        <div id="netpluspay" style="display:none;" class="PaymentDiv">
        </div>
         <!-------------interswitchng Api-->
        <div id="interswitchng" style="display:none;" class="PaymentDiv">
        </div>
          <div id="netpluspay" style="display:none;" class="PaymentDiv">
          </div>
          <div id="pod" style="display:none;" class="PaymentDiv">
        </div>
          <div id="bankdeposit" style="display:none;" class="PaymentDiv">
          </div>
        
        <div id="payOnDelivery" style="display:none;" class="PaymentDiv">
          <form method="POST" id="upay_form" name="upay_form" action="<?php echo base_url()?>order/success" target="_top">
            <input type="hidden" name="orderId" value="<?php if(isset($_SESSION['Check']) && $_SESSION['Check']!= '') echo $_SESSION['Check'];?>" >
            <input type="hidden" name="paymentType" value="PAY_ON_DELIVERY" />
            <input type="submit" class="btn bg-orange btn-block" name="submit" value="CHECK OUT" style="height: 30px; padding:2px 18px;">
          </form>
        </div>
        
        <?php
		 /* }*/
		  ?>
      </div>
    </div>

    <div class="payment_box_holder" style="float: right;">
      <div class="usecoupondiv">
        <span style="display: none;" id="cpCodeSpan">
          <p>USE COUPON</p>
          <input type="text" value="" id="CpCode" name="CpCode" placeholder="Enter Code" class="CpCode form-control">
          <input type="hidden" id="country" value="NG" name="country">
          <input type="hidden" id="amount" value="<?php //echo $amount; ?>" name="amount">
          <input type="hidden" id="tranx_id" value="<?php //if(isset($_SESSION['Check']) && $_SESSION['Check']!= '') echo $_SESSION['Check'];?>" name="tranx_id">
          <a id="RedeemCp" class="btnRedeem" href="javascript:;">Redeem</a> 
          <a id="CancelCp" class="btnCancel" href="javascript:;">Cancel</a> 
        </span>
        <div class="button_box coupon-btu-div" style="display:none;">
          <button style="height: 30px; padding:2px 18px;" type="submit" class="btn bg-primary bg-curve coupon-btu btn-block">USE COUPON</button>
        </div>
      </div>
    </div>
  </div> 
</section>
