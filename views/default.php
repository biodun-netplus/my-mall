<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>
<?=$title_for_layout?>
</title>
<!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
<link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url()?>assets/css/ecobank.css" rel="stylesheet">
<!-- Custom Fonts -->
<link href="<?php echo base_url()?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
<link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<?=$css_for_layout?>
<script src="<?php echo base_url()?>assets/js/jquery.js"></script>
</head>
<body id="page-top" class="index">
<div class="top-header">
  <div class="inner">
    <div class="div1"> <a href="<?php echo base_url()?>registration"> Open a store </a> </div>
    <div class="div3" style="z-index: 1000000"> <a href="javaScript:;" data-toggle="collapse" data-target="#loginDiv"> Login </a>
      <div id="loginDiv" class="collapsingForms collapse">
        <div class="form-group">
          <div class="alert alert-warning" id="loginError" style="display:none;"> Invalid username and password </div>
          <form name="loginform" method="post" id="loginform">
            <div class="form-group">
              <input type="text" class="form-control login-email" placeholder="Username" name="email">
            </div>
            <div class="form-group">
              <input type="password" class="form-control" name="password" placeholder="Password"  id="password">
            </div>
            <div class="form-group forgot-password-link"> <a href="#" class="no">Forgot password?</a> </div>
            <div class="form-group">
              <button type="button" id="Loginbutton" name="login" class="btn bg-success btn-block">Login </button>
            </div>
          </form>
        </div>
      </div>
      <a href="<?php echo base_url()?>registration"> Register </a> </div>
    <div class="div2"> <a href="#"> Shop </a> <a href="javaScript:;"> Meet Our Merchants </a> <a href="javaScript:;"> Ask An Expert </a> <a href="#"> Community </a> </div>
  </div>
</div>
<div class="actionbar">
  <div class="inner">
    <div class="logo"> <a class="" href="<?php echo base_url()?>"><img src="<?php echo base_url()?>assets/img/logo.png"/> </a> </div>
    <div class="cart">
      <table width="100%">
        <tr>
          <td>Shopping cart</td>
          <td rowspan="2"><a href="<?php echo base_url()?>cart">
            <div class="shopping_cart">
              <div class="cart_count">0</div>
            </div>
            </a></td>
        </tr>
        <tr>
          <td><h1>&#x20a6; 0.00</h1></td>
        </tr>
      </table>
    </div>
    <div class="search">
     <form action="<?php echo base_url().'search';?>" method="get">
      <table width="100%">
        <tr>
          <td>
            <div class="btn-group">
              <button type="button" class="btn bg-primary dropdown-toggle bg-icon" id="category-dropdown" data-toggle="dropdown" aria-expanded="false">
                <?php if(isset($_GET['category']) && ($_GET['category'])) echo $_GET['category'].' '; else echo'Categories' ?> <!--<span class="caret"></span>-->
              </button>
              <ul class="dropdown-menu" role="menu" id="category-menu">
              <?php
			  if($category && $category  != '')
			  {
				  foreach($category as $cat)
				  {
			  ?>
                <li <?php if(isset($_GET['category']) && ($_GET['category'] == $cat->category_name)){ echo 'class="active-li"'; } ?>><a href="javaScript:;" myval="<?php echo $cat->category_name ;?>"><?php echo $cat->category_name ;?></a></li>
              <?php
				  }
			  }
			  ?>
              </ul>
            </div>
          </td>
          <td><input type="text" style="border-color:#9BC535" class="form-control" placeholder="Browse by" name="keyword" value="<?php if(isset($_GET['keyword'])){ echo $_GET['keyword'];}?>"></td>
          <td><!--<button type="submit" name="login" class="btn bg-success bg-icon btn-block">All Categories </button>-->
          	<!-- Single button -->
            <div class="btn-group">
              <button type="button" class="btn dropdown-toggle bg-success bg-icon" id="merchant-dropdown" data-toggle="dropdown" aria-expanded="false"><?php if(isset($_GET['merchant']) && ($_GET['merchant'])) echo $_GET['merchant'].' '; else echo'&nbsp;&nbsp;&nbsp;Merchants &nbsp;&nbsp;&nbsp;' ?></button>
              <ul class="dropdown-menu" role="menu" id="merchant-menu">
              <?php
			  if($merchant && $merchant  != '')
			  {
				  foreach($merchant as $row)
				  {
			  ?>
                <li <?php if(isset($_GET['merchant']) && ($_GET['merchant'] == $row->display_name)){ echo 'class="active-li"'; } ?>><a href="javaScript:;" myval="<?php echo $row->display_name ;?>"><?php echo $row->display_name ;?></a></li>
              <?php
				  }
			  }
			  ?>
              </ul>
            </div>
          </td>
          <td style="text-align: right"><button type="submit" class="btn bg-success">Search</button></td>
        </tr>
      </table>
      <input type="hidden" value="<?php if(isset($_GET['category'])) echo $_GET['category']; ?>" name="category" id="category" />
      <input type="hidden" value="<?php if(isset($_GET['merchant'])) echo $_GET['merchant']; ?>" name="merchant" id="merchant" />
      </form>
    </div>
  </div>
</div>
<?=$content_for_layout?>
<!-- Footer -->
<footer class="text-center">
  <div class="footer-above">
    <div class="container">
      <h1>Experience freedom to shop from several online businesses in Nigeria.</h1>
      <h2>Receive goods nationwide in as little as 24 hours. </h2>
      <br/>
      <table width="100%">
        <tr>
          <td style="width: 20%"></td>
          <td><input type="text" class="form-control sub-text" placeholder="Subscribe to recieve updates on EcoMall"></td>
          <td><button style="height: 70px" type="submit" name="login" class="btn bg-sub btn-block">Subscribe</button></td>
          <td style="width: 20%"></td>
        </tr>
      </table>
    </div>
  </div>
  </div>
  <div class="footer-below">
    <div class="container">
      <table width="100%">
        <tr>
          <td>&copy; Ecomall 2015</td>
          <td><a href="javaScript:;">House Rules</a>&nbsp;|&nbsp;<a href="javaScript:;">Marketplace</a>&nbsp;|&nbsp;<a href="javaScript:;">Terms of use</a>&nbsp;|&nbsp;<a href="javaScript:;">Privacy</a>&nbsp;|&nbsp;<a href="javaScript:;">Cookies</a>&nbsp;|&nbsp;<a href="javaScript:;">FAQs</a>&nbsp;|&nbsp;<a href="javaScript:;">Contact Us</a>&nbsp;|&nbsp;<a href="javaScript:;">Registration</a></td>
          <td><a href="javaScript:;"><img src="<?php echo base_url()?>assets/img/facebook.jpg"></a> <a href="javaScript:;"><img src="<?php echo base_url()?>assets/img/twitter.jpg"></a> <a href="javaScript:;"><img src="<?php echo base_url()?>assets/img/gplus.jpg"></a> <a href="javaScript:;"><img src="<?php echo base_url()?>assets/img/linkin.jpg"></a> <a href="javaScript:;"><img src="<?php echo base_url()?>assets/img/blogspot.jpg"></a> <a href="javaScript:;"><img src="<?php echo base_url()?>assets/img/youtube.jpg"></a></td>
          <td></td>
        </tr>
      </table>
    </div>
  </div>
</footer>
<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-top page-scroll visible-xs visble-sm"> <a class="btn btn-primary" href="#page-top"> <i class="fa fa-chevron-up"></i> </a> </div>
 
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script> 
<script src="<?php echo base_url()?>assets/js/classie.js"></script> 
<script src="<?php echo base_url()?>assets/js/cbpAnimatedHeader.js"></script> 
<script src="<?php echo base_url()?>assets/js/jqBootstrapValidation.js"></script> 
<!--<script src="<?php //echo base_url()?>assets/js/contact_me.js"></script> -->
<script src="<?php echo base_url()?>assets/js/ecobank.js"></script> 
<!-- Custom JS --> 
<script src="<?php echo base_url()?>assets/js/script.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<?=$js_for_layout?>
</body>
</html>
