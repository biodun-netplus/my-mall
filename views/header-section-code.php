<header>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="carousel-header">
          <div class="owl-carousel-header">
          <?php  print_r($headerBanner);
          if($headerBanner && $headerBanner != ''){
              foreach ($headerBanner as $row) {
                $bannerImg    = base_url().'upload_home_slider_image/'.$row->path;
                ?>
                <div class="items"><img src="<?php echo $bannerImg;?>" alt="<?php echo $row->path;?>"></div>
                <?php
              }
           }
          ?>            
        </div>
       </div>
      </div>
    </div>
  </div>
</header>