<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('merchant-head');
?>

<section class="background-white">
  <div class="container">
    <div class="row">
      <div class="col-lg-10">
        <h3 class="page-header1">Product</h3>
      </div>
      <div class="col-lg-2" style="text-align: right; margin-top: 1em; font-size: 18px;">
        <a href="<?php echo base_url();?>add-product">Add Product</a>
      </div>
    </div>
    <div class="border1">
      <div class="productsDetail">
        <div style="clear: both;"></div>
        <div class="merchant-product">
        <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th width="" align="left" valign="top">Name.</th>
              <th width="100" align="left" valign="top">Product_price</th>
              <th width="" align="left" valign="top" style=" text-align:center">Status</th>
              <th width="150" align="left" valign="top">Image</th>
              <th width="150" align="left" valign="top">Qty.</th>
              <th align="left" width="13%">Action</th>
            </tr>
          </thead>
          <tbody> 

        <?php
        if($productsDetail && $productsDetail  != '')
        {
            $count_div = 1;
            $total_products = count($productsDetail);
            $count_row =  1; 
            foreach($productsDetail as $rows)
            {
                $merchantLogo = '';
                if($rows->image != '')
                $productsImg = 'product_images/'.$rows->image;
                else
                $productsImg = 'assets/img/logo.png';

                $status       = '';
                $declinedMsg  = '--';


                if($rows->isactive  == 'p')
                  $status = '<span style="color:grey;">Pending</span>';
                if($rows->isactive  == 't')
                  $status = '<span style="color:green;">Active</span>';
                if($rows->isactive  == 'f')
                  $status = '<span style="color:orange;">Inactive</span>';
                if($rows->isactive  == 'd')
                {
                  $status       = '<span style="color:red;">Declined</span>'.'<br/>';
              
                  $status       .= '<i style="color: red; line-height: 16px; position: relative;top: 5px;">'.$rows->declined_desc.'</i>';

                }

        ?>
            <tr>
              <td><?php echo $rows->product_name; ?></td>
              <td class="num-pallets">&#x20A6;<?php echo number_format($rows->product_price); ?></td>
              <td><?php echo $status; ?></td>
              <td align="center"><img src="<?php echo $productsImg; ?>" width="100" /></td>
              <td class="">
                <?php
                $proAttrDetail  = $this->product_model->get_product_size_colour_by_product(array('relation_color_size.product_id' => $rows->id, 'products.pid' => $_SESSION['userDetail']->id )); 
                if($proAttrDetail && $proAttrDetail != '')
                {
                ?>
                <table class="table table-bordered">
                  <tr bgcolor="#F3F3F3">
                    <th>Color</th>
                    <th>Size</th>
                    <th>Qty</th>
                  </tr>
                  <?php
                  foreach ($proAttrDetail as $value) {
                  ?>
                  <tr>
                    <td><?php $res = $this->product_model->get_single_product_color(array('product_color.id' => $value->colour_id));  if($res)  echo $res->color;
                     ?></td>
                    <td>
                     <?php $res = $this->product_model->get_single_product_size(array('product_size.id' => $value->size_id)); 
                       if($res) echo $res->size;
                     ?></td>
                    <td><?php echo $value->qty; ?></td>
                  </tr>
                  <?php
                  }
                  ?>
                </table>
                <?php
                }
                else
                {
                  echo '<div class="no-record text-center">No Record Found!</div>';
                }
                ?>
              </td>
              <td style="text-align:left;">
                <a title="Edit" href="<?php echo base_url()?>edit-product/<?php echo base64_encode($rows->id);?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"  style="margin-bottom: 5px;"></span></a>
                <a title="Delete" href="<?php echo base_url()?>delete-product/<?php echo base64_encode($rows->id);?>" onclick="return confirm('Are you sure, you want to delete this record?')"><span class="glyphicon glyphicon-remove" aria-hidden="true"  style="margin-bottom: 5px;"></span></a>
                <a title="Colour" href="<?php echo base_url()?>product-colour/<?php echo base64_encode($rows->id);?>"><span class="glyphicon" aria-hidden="true"  style="margin-bottom: 5px;"><img src="<?php echo base_url()."assets/img/color.png"; ?>" width="20" /></span></a>
                <a title="Size" href="<?php echo base_url()?>product-size/<?php echo base64_encode($rows->id);?>"><span class="glyphicon" aria-hidden="true"><img src="<?php echo base_url()."assets/img/size.png"; ?>" width="20" /></span></a>
                <?php

                //$checkProductColour    = $this->product_model->product_color(array('products.id' => $rows->id, 'products.pid' => $_SESSION['userDetail']->id));  
                //$checkProductSize      = $this->product_model->product_size(array('products.id' => $rows->id, 'products.pid' => $_SESSION['userDetail']->id)); 

                  //if($checkProductColour  || $checkProductSize)
                  //{

                ?>
                <a title="Relate size and colour" href="<?php echo base_url()?>relate-product-size-colour/<?php echo base64_encode($rows->id);?>"><span class="glyphicon cogs" aria-hidden="true"><img src="<?php echo base_url()."assets/img/color_size_.png"; ?>" width="20"></span></a>
                <?php
                  //}
                ?>

                </td>
            </tr>
          <?php	
			}
		}
		else
    	{
		?>
      <tr>
           <td colspan="12"><div style="margin-bottom:10px" class="no-record text-center">No Record Found!</div></td>
        </tr>
    <?php
    }
    ?>

    </tbody>
        </table>
      </div>
        </div>
      </div>
    </div>
  </div>
</section>
