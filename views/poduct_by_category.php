<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div id="content">
  <div class="container">
    <div class="section">
      <div class="section_title">
        <h3><?php echo $categoryDetail->category_name;?></h3>
      </div>
      <div class="section_nav">
        <div class="nav-top">
          <div class="display-div" style="height: auto;">
            <div class="main_nav product_nav_wrap_silder">
              <div class="inner_nav">
                <?php $countI = 0; ?>
                <a href="#" id="prev<?php echo $countI;?>" class="">
                <div class="nav-top-control nav-icon-left left"></div>
                </a> <a href="#" id="next<?php echo $countI;?>">
                <div class="nav-top-control nav-icon-right right"></div>
                </a>
                <div class="slideshow" data-cycle-fx=carousel  data-cycle-timeout=0  data-cycle-slides="> div" data-cycle-next="#next<?php echo $countI;?>" data-cycle-prev="#prev<?php echo $countI;?>" data-allow-wrap="false">
                  <?php
                    if($category && $category  != '')
                    {
                      foreach($category as $cate)
                      {
                    ?>
                  <div>
                    <p><a style="display:none;" href="javaScript:;" myval="subcate<?php echo $cate->id.$countI;?>" class="hoverme"><?php echo $cate->category_name;?></a></p>
                  </div>
                  <?php

                      }
                    }
                  ?>
                </div>
                <div class="clear"></div>
              </div>
              <div class="clear"></div>
            </div>
          </div>
        </div>
        <div class="nav-bottom" style="display:none">
          <?php
       if($subCategoryDetail  && !empty($subCategoryDetail))
       {

              $countRows        = 0;
              $groupCategory    = '';


              foreach($subCategoryDetail as $subCateRows)
              {
                  $html = '';
                    if($groupCategory != $subCateRows->parent_id){

                    $groupCategory  = $subCateRows->parent_id;

                    if($countRows > 0){

                      $html   .= ' </div><div class="clear"></div>'; // close slideshow
                      $html   .= ' </div><div class="clear"></div>'; // close inner_nav
                      $html   .= ' </div>'; // close main_nav
                    }
                    $html .= '<div id="subcate'.$subCateRows->parent_id.$countI.'"  class="main-nav hide-elements"';



                     if($subCateRows->parent_id != $catId){
                      $html   .= ' style="display:none;" >';
                    }

                    else{
                      $html   .= '>';
                    }

                    $html .= '<div class="inner_nav">';

                    $html .= '<a href="#" id="prevsub'.$countRows.'" class=""><div class="nav-top-control nav-icon-left left" style="background-color:#9BC534"></div></a> <a href="#" id="nextsub'.$countRows.'"><div class="nav-top-control nav-icon-right right"  style="background-color:#9BC534"></div></a>';

                    $html .= '<div class="slideshow" data-cycle-fx=carousel  data-cycle-timeout=0  data-cycle-slides="> div" data-cycle-next="#nextsub'.$countRows.'" data-cycle-prev="#prevsub'.$countRows.'"  data-allow-wrap="false">';

                    //$html   .= '<div id="'.$groupCategory.'" class="hoverme"';
                    $countRows ++;
                    }
           echo $html;
                ?>
          <div>
            <p class="subCateslider"><a myval="<?php echo base64_encode($subCateRows->id);?>" href="javaScript:;"><?php echo $subCateRows->category_name;?></a></p>
          </div>
          <?php

                }
              }
              ?>
        </div>
        <!--// close slideshow-->
      </div>
      <!-- close inner_nav-->
    </div>
    <!-- close main_nav -->
  </div>
</div>
<div class="section_content">
  <div class="product_area_wrap1">
    <?php
    if($productDetail && !empty($productDetail))
        {
        $productImg   = base_url().'assets/img/logo.png';
        foreach($productDetail as $productRow)
        {
          if($productRow->image != '' )
          $productImg  = base_url().'product_images/'.$productRow->image;
      ?>

    <div class="product_wrap product_wrap1"> <a href="<?php echo base_url().'product/'.base64_encode($productRow->id);?>">
      <div class="image"><img src="<?php echo $productImg;?>"></div>
      <div class="details">
        <table>
          <tr>
            <td colspan="2" style="color: #fff; height: 42px; vertical-align: top; min-width: 210px; width: 100%;">

             <?php
	     if($productRow->product_name != '')
	     {
		if(strlen($productRow->product_name) > 40)
		{

		    echo substr($productRow->product_name, 0,  (36 - strlen($productRow->product_name)))."...." ;
		}
		else
		echo $productRow->product_name;
	     }
	     ?>
            </td>
          </tr>
          <tr>
            <td style="color: #A8C738; font-size: 22px; font-weight: 600;">&#x20A6;<?php echo number_format($productRow->product_price);?></td>
            <td><input type="button" class="btn-block btn btn-cart" value="add to cart"></td>
          </tr>
        </table>
      </div>
      </a> </div>
    <?php
        }

      }
      else
      {
        echo '<div class="no-record text-center">No Record Found!</div>';
      }
         ?>
    <br class="clear" />
  </div>
  <div class="clear"></div>
</div>
<br class="clear" />
</div>
</div>
</div>
