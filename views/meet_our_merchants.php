<?php
defined('BASEPATH') OR exit('No direct script access allowed');
function startsWith2($haystack, $needle)
{
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
}

?>

<div id="main_content">
    <div class="container">

        <div class="dir_cont">
            <!---<h2>Meet Our Merchants</h2> !-->

            <ul class="tabs" data-persist="true">
                <li><a href="#atoe-tab">A-E</a></li>
                <li><a href="#ftoj-tab">F-J</a></li>
                <li><a href="#ktoo-tab">K-O</a></li>
                <li><a href="#ptot-tab">P-T</a></li>
                <li><a href="#utoz-tab">U-Z</a></li>
            </ul>

            <div class="tabcontents">
                <div class="tab-pane" id="atoe-tab">
                    <div class="tab-pane-cont">
                        <div class="tab-ins1">
                            <ul>
                            <?php

                            $categoryArray 		= array();
                            $i 							  = 1;
                            $merchantRegexp   = $this->home_model->get_merchants_with_active_products();
                            if($merchantRegexp && !empty($merchantRegexp))
                            {
                              $selectDataAEcount  	= count($merchantRegexp);
                                foreach ($merchantRegexp as $selectDataAE) {
                                    if (startsWith2(strtoupper($selectDataAE->first_name), 'A') || startsWith2(strtoupper($selectDataAE->first_name), 'B') || startsWith2(strtoupper($selectDataAE->first_name), 'C') || startsWith2(strtoupper($selectDataAE->first_name), 'D') || startsWith2(strtoupper($selectDataAE->first_name), 'E')) {
                                        $categoryArray[] = $selectDataAE;
                                    }
                                    $CattegoryCount = count($categoryArray);
                                }


                                $selectDataAE 			= "";
                              //$selectDataAEcount 	= round($CattegoryCount / 5); 
						  
                              foreach ($categoryArray as $selectDataAE){
          						$merchantImage		= base_url().'assets/img/newmerchant.jpg';

          							if($selectDataAE->image != '' && file_exists('./banner_images/'.$selectDataAE->image))
          							{
          								$merchantImage = base_url().'banner_images/'.$selectDataAE->image;
          							}
                              ?>

                              <li class="tab-ins">
                               <a href="<?php echo base_url() .$selectDataAE->slug;?>">
							                   <div class="tab-img">
                                  <img src="<?php echo $merchantImage;?>" />
                                  </div>
                                 <?php echo ucwords($selectDataAE->first_name);?>
                                </a>
                              </li>

                              <?php
                              }
                            } // end of if...
                            ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="ftoj-tab">
                   <div class="tab-pane-cont">
                      <div class="tab-ins1">
                         <ul>
                          <?php
                          $categoryArray      = array();
                          $i                  = 1;
                          $merchantRegexp   = $this->home_model->get_merchants_with_active_products();
                          if($merchantRegexp && !empty($merchantRegexp))
                          {
                              $selectDataAEcount  	= count($merchantRegexp);
                              foreach ($merchantRegexp as $selectDataAE) {
                                  if (startsWith2(strtoupper($selectDataAE->first_name), 'F') || startsWith2(strtoupper($selectDataAE->first_name), 'G') || startsWith2(strtoupper($selectDataAE->first_name), 'H') || startsWith2(strtoupper($selectDataAE->first_name), 'I') || startsWith2(strtoupper($selectDataAE->first_name), 'J')) {
                                      $categoryArray[] = $selectDataAE;
                                  }
                                  $CattegoryCount = count($categoryArray);
                              }


                              $selectDataAE = "";
                            $selectDataAEcount = round($CattegoryCount / 3); 
                            foreach ($categoryArray as $selectDataAE){
            					$merchantImage		= base_url().'assets/img/newmerchant.jpg';
            						if($selectDataAE->image != '' && file_exists('./banner_images/'.$selectDataAE->image)){
            							$merchantImage = base_url().'banner_images/'.$selectDataAE->image;
            						}
                            ?>

                            <li class="tab-ins">
                                <a href="<?php echo base_url() .$selectDataAE->slug;?>">
									                 <div class="tab-img">
                                      <img src="<?php echo $merchantImage;?>" />
                                   </div>
                                     <?php echo ucwords($selectDataAE->first_name);?>
                                </a>
                            </li>

                            <?php

                            //if ($i == $selectDataAEcount){

                            ?>

                            <!--</ul>   
                        </div>

                        <div class="tab-ins"><ul> !--><?php //$i = 0;

                           // }
                            //$i++;

                            } 
                          } // end of if...?>
                          </ul>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="ktoo-tab">
                    <div class="tab-pane-cont">
                        <div class="tab-ins1">
                            <ul>
                             <?php
                              $categoryArray    = array();
                              $i                = 1;
                             $merchantRegexp   = $this->home_model->get_merchants_with_active_products();
                             if($merchantRegexp && !empty($merchantRegexp))
                             {
                                 $selectDataAEcount  	= count($merchantRegexp);
                                 foreach ($merchantRegexp as $selectDataAE) {
                                     if (startsWith2(strtoupper($selectDataAE->first_name), 'K') || startsWith2(strtoupper($selectDataAE->first_name), 'L') || startsWith2(strtoupper($selectDataAE->first_name), 'M') || startsWith2(strtoupper($selectDataAE->first_name), 'N') || startsWith2(strtoupper($selectDataAE->first_name), 'O')) {
                                         $categoryArray[] = $selectDataAE;
                                     }
                                     $CattegoryCount = count($categoryArray);
                                 }

                                 $selectDataAE = "";
                                $selectDataAEcount = round($CattegoryCount / 3);
                                foreach ($categoryArray as $selectDataAE){
                					$merchantImage		= base_url().'assets/img/newmerchant.jpg';
                						if($selectDataAE->image != '' && file_exists('./banner_images/'.$selectDataAE->image))
                						{
                							$merchantImage = base_url().'banner_images/'.$selectDataAE->image;
                						}
                                                ?>
                								    <li class="tab-ins">
                						
                                      <a href="<?php echo base_url() .$selectDataAE->slug;?>">
									                     <div class="tab-img">
                                        <img src="<?php echo $merchantImage;?>" />
                                      </div>
                                     <?php echo ucwords($selectDataAE->first_name);?>
                                    </a>
                                  </li>
                                <?php

                                //if ($i == $selectDataAEcount){
                                ?>

                            <!--</ul>
                        </div>

                        <div class="tab-ins"> <ul> !--><?php //$i = 0;

                            //}
 
                            //$i++;

                              } 
                            } // end of if...?>
                          </ul>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="ptot-tab">
                    <div class="tab-pane-cont">
                        <div class="tab-ins1">
                            <ul>
                             <?php
                              $categoryArray = array();
                              $i = 1;
                             $merchantRegexp   = $this->home_model->get_merchants_with_active_products();
                             if($merchantRegexp && !empty($merchantRegexp))
                             {
                                 $selectDataAEcount  	= count($merchantRegexp);
                                 foreach ($merchantRegexp as $selectDataAE) {
                                     if (startsWith2(strtoupper($selectDataAE->first_name), 'P') || startsWith2(strtoupper($selectDataAE->first_name), 'Q') || startsWith2(strtoupper($selectDataAE->first_name), 'R') || startsWith2(strtoupper($selectDataAE->first_name), 'S') || startsWith2(strtoupper($selectDataAE->first_name), 'T')) {
                                         $categoryArray[] = $selectDataAE;
                                     }
                                     $CattegoryCount = count($categoryArray);
                                 }

                                 $selectDataAE = "";
                                $selectDataAEcount = round($CattegoryCount / 3);
                                foreach ($categoryArray as $selectDataAE){
              						$merchantImage		= base_url().'assets/img/newmerchant.jpg';
              						if($selectDataAE->image != '' && file_exists('./banner_images/'.$selectDataAE->image)){
              							$merchantImage = base_url().'banner_images/'.$selectDataAE->image;
              						}
                                ?>
                                  <li class="tab-ins"> 
                                   <a href="<?php echo base_url() .$selectDataAE->slug;?>">
									                   <div class="tab-img">
                                      <img src="<?php echo $merchantImage;?>" />
                                      </div>
                                     <?php echo ucwords($selectDataAE->first_name);?>
                                    </a>
                                  </li>
                                <?php

                                //if ($i == $selectDataAEcount){

                                ?>

                            <!--</ul>
                            </div>
                            <div class="tab-ins"> <ul> !--><?php //$i = 0;
                            //}
                            //$i++;

                              }
                            } // end of if... ?>
                          </ul>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="utoz-tab">
                    <div class="tab-pane-cont">
                        <div class="tab-ins1">
                           <ul>
                              <?php
                              $categoryArray = array();
                              $i = 1;
                              $merchantRegexp   = $this->home_model->get_merchants_with_active_products();
                              if($merchantRegexp && !empty($merchantRegexp))
                              {
                                  $selectDataAEcount  	= count($merchantRegexp);
                                  foreach ($merchantRegexp as $selectDataAE) {
                                      if (startsWith2(strtoupper($selectDataAE->first_name), 'U') || startsWith2(strtoupper($selectDataAE->first_name), 'V') || startsWith2(strtoupper($selectDataAE->first_name), 'W') || startsWith2(strtoupper($selectDataAE->first_name), 'X') || startsWith2(strtoupper($selectDataAE->first_name), 'Y') || startsWith2(strtoupper($selectDataAE->first_name), 'Z')) {
                                          $categoryArray[] = $selectDataAE;
                                      }
                                      $CattegoryCount = count($categoryArray);
                                  }


                                  $selectDataAE = "";

                                $selectDataAEcount = round($CattegoryCount / 3);

                                foreach ($categoryArray as $selectDataAE){
                			$merchantImage		= base_url().'assets/img/newmerchant.jpg';
                			if($selectDataAE->image != '' && file_exists('./banner_images/'.$selectDataAE->image)){
                				$merchantImage = base_url().'banner_images/'.$selectDataAE->image;
                			}

                                ?>
                               	 <li class="tab-ins">
                                   <a href="<?php echo base_url() .$selectDataAE->slug;?>">
									                   <div class="tab-img">
                                      <img src="<?php echo $merchantImage;?>" />
                                      </div>
                                     <?php echo ucwords($selectDataAE->first_name);?>
                                    </a>
                                  </li>
                                <?php
                                //if ($i == $selectDataAEcount){
                                ?>
                            <!--</ul> 
                            </div>
                            <div class="tab-ins"> <ul> !--><?php //$i = 0;
                            //}
                            //$i++;
                              }
                            }// end of if... ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
