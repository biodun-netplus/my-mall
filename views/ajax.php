<?php
include("includes/classes/functions.php");
$mainsiteUrl = 'http://' . $_SERVER['SERVER_NAME'] . '/access/';

/*==========================Searching By Category============================*/
if (isset($_REQUEST['CatId'])) {


    ?>
<script type="text/javascript">
        $(document).ready(function () {

            $(".owl-carousel").each(function () {
                var $this = $(this);
                $this.owlCarousel({
                    itemsCustom: [
                        [0, 1],
                        [450, 1],
                        [600, 2],
                        [700, 3],
                        [1000, 4]
                    ],
                    pagination: false
                });
                // Custom Navigation Events
                $this.parent().find(".next").click(function () {
                    $this.trigger('owl.next');
                });
                $this.parent().find(".prev").click(function () {
                    $this.trigger('owl.prev');
                });
            });

        })

    </script>
<?php
    $CateBanner = mysql_fetch_object(mysql_query("select * from category  where isactive = 't'  and id = '" . $_REQUEST['CatId'] . "'"));
    $new = new users;
    ?>

<div class="special-offer"><a href="#"><img
                src="<?php echo $mainsiteUrl . 'category_images/' . $CateBanner->banner;?>" alt=""/></a></div>
<div class="home-product-categories">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="home-merchant-slider">
          <div class="merchants owl-carousel SubCatCarousal">
            <?php
                            $SubCateQ = mysql_query("SELECT * FROM sub_category where parent_id='" . $_REQUEST['CatId'] . "' limit 10");
                            $new = new users;
                            while ($SubcatData = mysql_fetch_object($SubCateQ)) {
                                if (empty($SubcatData->image)) {

                                    $subCatImg = $mainsiteUrl . 'images/_no_image.png';
                                } else {
                                    $subCatImg = $mainsiteUrl . 'category_images/' . $SubcatData->image;

                                }

                                ?>
            <div class="imagebox"> <a href="javascript:;" class="product-title"><img src="<?php echo $subCatImg;?>"
                                                                                      style=" width:200px; height:150px"
                                                                                      alt=""/> </a><br/>
              <a href="javascript:;"
                                       class="product-title"><?php echo ucfirst($SubcatData->name); ?></a> </div>
            <?php } ?>
          </div>
          <div class="customNavigation"> <a id="merchants-prev" class="prev"></a> <a id="merchants-next" class="next"></a> </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="home-product-slider">
    <div class="products owl-carousel">
      <?php $PrdData = mysql_query("select products.*,u.first_name from products LEFT JOIN users AS u ON u.id = products.pid  where u.id!=''  AND products.category ='" . $_REQUEST['CatId'] . "' order by rand()");

                $main = new users;
                $currencyS = 'N';
                while ($AllPrdData = mysql_fetch_object($PrdData)) {
                    if (!empty($AllPrdData->first_name)) {
                        $currencyS = $main->getCurrency($AllPrdData->currency);
                        $fname = $main->FormatN($AllPrdData->first_name);
                        $pname = $main->FormatPr($AllPrdData->product_name);
                        if ($AllPrdData->image == '') {
                            $img = 'images/_no_image.png';
                        } else {
                            $img = "product_images/" . $AllPrdData->image;
                        }
                        if ($AllPrdData->list_price != '0.00' || $AllPrdData->list_price != '0' || $AllPrdData->list_price != '') {
                            $less = $AllPrdData->list_price - $AllPrdData->product_price;
                            $Divide = $less / $AllPrdData->list_price;
                            $Saving = round($Divide * 100, 2);
                        } else {
                            $Saving = 0;
                        }

                        ?>
      <div class="item"><img src="<?php echo $mainsiteUrl . $img;?>"
                                               style=" width:200px; height:200px" alt="">
        <div class="details">
          <h3><?php echo $currencyS . "&nbsp;" . $main->ShowPrice($AllPrdData->product_price); ?> <span>(<?php echo $Saving;?>% Off)</span></h3>
          <p><?php echo ucwords(strtolower($AllPrdData->product_name)); ?></p>
          <a href="<?php echo $mainsiteUrl . $fname . '/' . $pname . '/' . $AllPrdData->id?>"> Buy Now </a> </div>
      </div>
      <?php }
                }?>
    </div>
    <div class="customNavigation"><a id="products-prev" class="prev"></a> <a id="products-next"
                                                                                     class="next"></a></div>
  </div>
</div>
<?php }


/*==========================Delete Product From Cart============================*/
if (isset($_REQUEST['CartId'])) {
    $DeleteCartData = mysql_query("DELETE FROM cart where  id = '" . $_REQUEST['CartId'] . "' ");
}
/*==========================Search By Keyword============================*/
if (isset($_REQUEST['SearchVal'])) {
    $PrdData = mysql_query("select products.*,u.first_name from products LEFT JOIN users AS u ON u.id = products.pid  where products.product_name like '%" . $_REQUEST['SearchVal'] . "%'  order by rand()");
    if (mysql_num_rows($PrdData) > 0) {
        ?>
<div class="products owl-carousel">
  <?php
            $main = new users;
            $currencyS = 'N';
            while ($AllPrdData = mysql_fetch_object($PrdData)) {
                if (!empty($AllPrdData->first_name)) {
                    $currencyS = $main->getCurrency($AllPrdData->currency);
                    $fname = $main->FormatN($AllPrdData->first_name);
                    $pname = $main->FormatPr($AllPrdData->product_name);
                    if ($AllPrdData->image == '') {
                        $img = 'images/_no_image.png';
                    } else {
                        $img = "product_images/" . $AllPrdData->image;
                    }
                    if ($AllPrdData->list_price != '0.00' || $AllPrdData->list_price != '0' || $AllPrdData->list_price != '') {
                        $less = $AllPrdData->list_price - $AllPrdData->product_price;
                        $Divide = $less / $AllPrdData->list_price;
                        $Saving = round($Divide * 100, 2);
                    } else {
                        $Saving = 0;
                    }

                    ?>
  <div class="item"> <img src="<?php echo $mainsiteUrl . $img;?>" style="width:125px; height:125px" alt="">
    <div class="details prod-detail">
      <?php
                            $new_product_name = $AllPrdData->product_name;
                            $showStr = 16;
                            if (strlen($AllPrdData->product_name) > $showStr) {
                                $rest = $showStr - $AllPrdData->product_name;
                                $new_product_name = substr($AllPrdData->product_name, 0, $rest);
                                $new_product_name .= '...';
                            }
                            ?>
      <p><?php echo ucwords(strtolower($new_product_name)); ?></p>
      <h3><?php echo $currencyS . "&nbsp;" . number_format($main->ShowPrice($AllPrdData->product_price)); ?> <span>(<?php echo $Saving;?>% Off)</span></h3>
      <a href="<?php echo $mainsiteUrl . $fname . '/' . $pname . '/' . $AllPrdData->id?>">More
      Details</a> </div>
  </div>
  <?php
                }
            }?>
</div>
<div class="customNavigation"> <a id="products-prev" class="prev">&gt;</a> <a id="products-next" class="next">&lt;</a> </div>
<?php
    } else {
        ?>
<div class="no-record">No record found.</div>
<?php
    }
}

/*==========================Add product and category on Home page============================*/
if (isset($_REQUEST['countCate']) && $_REQUEST['countCate'] != '') {
    $countHr = 0;

    if (isset($_REQUEST['cateId']) && $_REQUEST['cateId'] != '') {
        $Category = mysql_query(" SELECT * FROM category WHERE isactive = 't' AND id = " . $_REQUEST['cateId'] . " ");
    } else {
        //" SELECT * FROM category WHERE isactive = 't' AND id NOT IN (".implode(',',$_SESSION['categoryIds']).") LIMIT ".$_REQUEST['countCate']." , 3 "
        $Category = mysql_query(" SELECT * FROM category WHERE isactive = 't' AND id NOT IN (" . implode(',', $_SESSION['categoryIds']) . ") LIMIT 3 ");

    }
    while ($CateRecord = mysql_fetch_object($Category)) {
        $_SESSION['categoryIds'][] = $CateRecord->id;
        if ($countHr < mysql_num_rows($Category)) { ?>
<hr class="horizontal-line col-md-11">
<?php }
        $countHr++;
        ?>
<div class="margin" id="cateId<?php echo $CateRecord->id;?>">
  <div class="col-md-3">
    <div class="category-name"><?php echo $CateRecord->category_name; ?></div>
    <div class="shop-sub-category-menu" id="sub-category-menu-<?php echo $CateRecord->id; ?>">
      <?php
                    /* category query*/
                    $SubCate = mysql_query("SELECT * FROM sub_category where parent_id='" . $CateRecord->id . "' LIMIT 0,8 ");
                    /* product query*/
                    $ProCate = mysql_query("select products.* from products where  products.isactive = 't' and products.product_type = 'N' and products.category  = '" . $CateRecord->id . "' limit 0, 4");
                    ?>
      <div><a id="all-<?php echo $CateRecord->id; ?>" class="searchBysubcat active-link"
                            href="javascript:;">All</a></div>
      <?php
                    while ($SubcatData = mysql_fetch_object($SubCate)) {
                        ?>
      <div><a id="<?php echo $SubcatData->id; ?>" class="searchBysubcat"
                                href="javascript:;"><?php echo $SubcatData->name; ?></a></div>
      <?php
                    }
                    ?>
    </div>
    <?php
                if (mysql_num_rows($ProCate) > 0) {
                    ?>
    <div class="load-more-sub-category" id="mainCate_<?php echo $CateRecord->id;?>"><a
                            href="<?php echo $mainsiteUrl ?>category.php?CatId=<?php echo $CateRecord->id; ?>"><img
                                src="<?php echo $mainsiteUrl ?>images/load_more.png" alt="Load More"/></a></div>
    <?php } ?>
  </div>
  <div class="col-md-9">
    <div class="home-product-categories" id="productCategorie_<?php echo $CateRecord->id;?>">
      <div class="row">
        <?php
                        if (mysql_num_rows($ProCate) > 0) {
                            ?>
        <!--  pagination -->
        <div class="paginationBycategory text-right">
          <?php
                                $ProCateCount = mysql_query("select products.* from products where  products.isactive = 't' and products.product_type = 'N' and products.category  = '" . $CateRecord->id . "' ");
                                ?>
          <a href="javascript:;" class="inactive-link">&#9668;</a> <a href="javascript:;" <?php if (mysql_num_rows($ProCateCount) > 6) { ?> onClick="pagination(<?php echo $CateRecord->id; ?>, <?php echo $CateRecord->id; ?>, 1, 'category');" <?php } else { ?> class="inactive-link" <?php } ?>> &#9658;</a> </div>
        <!-- End pagination -->
        <div class="home-merchant-slider">
          <div class="merchant-products">
            <?php
                                    while ($productData = mysql_fetch_object($ProCate)) {
                                        if (empty($productData->image)) {

                                            $productImg = $mainsiteUrl . 'images/_no_image.png';
                                        } else {
                                            $productImg = $mainsiteUrl . 'product_images/' . $productData->image;

                                        }

                                        $countQuertyData = mysql_num_rows(mysql_query("select * from users where id = '" . $productData->pid . "'"));

                                        if ($countQuertyData > 0) {
                                            $QuertyData = mysql_fetch_object(mysql_query("select * from users where id = '" . $productData->pid . "'"));
                                            $main = new users;
                                            $fname = $main->FormatN($QuertyData->first_name);
                                            $dname = $main->FormatN($QuertyData->display_name);
                                            $pname = $main->FormatPr($productData->product_name);
                                            $currencyS = $main->getCurrency($productData->currency);
                                        }

                                        ?>
            <div class="imagebox items col-md-3">
              <div class="merchant-prod"> <a href="<?php echo $mainsiteUrl . $fname . '/' . $pname . '/' . $productData->id?>">
                <div class="buyNow">Buy Now<span>&#8594;</span></div>
                </a>
                <div><img src="<?php echo $productImg;?>"  alt=""/></div>
                <h3><?php echo $currencyS . "&nbsp;" . number_format($main->ShowPrice($productData->product_price)); ?></h3>
                <?php
                                                    $new_product_name = $productData->product_name;
                                                    $showStr = 18;
                                                    if (strlen($productData->product_name) > $showStr) {
                                                        $rest = $showStr - $productData->product_name;
                                                        $new_product_name = substr($productData->product_name, 0, $rest);
                                                        $new_product_name .= '...';
                                                    }
                                                    ?>
                <a href="<?php echo $mainsiteUrl . $fname . '/' . $pname . '/' . $productData->id?>">
                <div class="product-title"> <?php echo ucwords(strtolower($new_product_name));?> </div>
                </a>
                <div class="customer-name"> <a href="<?php echo $mainsiteUrl . $fname  ?>"> <?php echo $QuertyData->display_name; ?> </a> </div>
              </div>
            </div>
            <?php } ?>
          </div>
          <br/>
          <br/>
          <a href="<?php echo $mainsiteUrl; ?>category.php?CatId=<?php echo $CateRecord->id ?>"> <i style="font-size: 24px">Load more ...</i> </a> <br/>
          <br/>
        </div>
        <?php
                        } else { ?>
        <div class="no-record">No record found.</div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<?php
    }
}

/*==========================Add category on Home page============================*/
if (isset($_REQUEST['countCategories']) && $_REQUEST['countCategories'] != '') {

    $CateQ = mysql_query("select * from category  where isactive = 't' limit " . $_REQUEST['countCategories'] . ", 5");
    if (mysql_num_rows($CateQ) > 0) {
        while ($CatData = mysql_fetch_object($CateQ)) { ?>
<div><a id="<?php echo $CatData->id;?>" href="javascript:;"
                    class="searchBycat"><?php echo $CatData->category_name;?></a></div>
<?php
        }
    }
}


/*==========================get category id from sub category id on Home page=====================*/
if (isset($_REQUEST['sub_categ_id']) && $_REQUEST['sub_categ_id'] != '') {

    $ProCate = mysql_query("select parent_id  from sub_category where status = 't' and id  = '" . $_REQUEST['sub_categ_id'] . "' ");

    $productData = mysql_fetch_object($ProCate);
    echo $productData->parent_id;
}


/*==========================Add product on Home page============================*/
if (isset($_REQUEST['sub_category_id']) && $_REQUEST['sub_category_id'] != '') {

    $page = 0;
    $per_page = 4;
    if (isset($_REQUEST['page']) && $_REQUEST['page'] != '') {
        $page = $_REQUEST['page'];
    }
    $start = $page * $per_page;

    if (isset($_REQUEST['all_product']) && $_REQUEST['all_product'] != '') {
        $query = "select products.* from products where  products.isactive = 't' and products.product_type = 'N' and products.category  = '" . $_REQUEST['sub_category_id'] . "' Limit " . $start . ", " . $per_page . " ";
    } else if (isset($_REQUEST['page']) && isset($_REQUEST['page']) && $_REQUEST['page'] != '' && $_REQUEST['type'] == 'category') {
        $query = "select products.* from products where  products.isactive = 't' and products.product_type = 'N' and products.category  = '" . $_REQUEST['sub_category_id'] . "' Limit " . $start . ", " . $per_page . " ";
    } else {
        $query = "select products.* from products where  products.isactive = 't' and products.product_type = 'N' and products.sub_category  = '" . $_REQUEST['sub_category_id'] . "' Limit " . $start . ", " . $per_page . "  ";
    }
    $ProCate = mysql_query($query);
    if (mysql_num_rows($ProCate) > 0) {
        ?>
<div class="row"> 
  <!--  pagination -->
  <div class="paginationBycategory text-right">
    <?php
                $ProCateCount = mysql_query("select products.* from products where  products.isactive = 't' and products.product_type = 'N' and products." . $_REQUEST['type'] . "  = '" . $_REQUEST['sub_category_id'] . "' ");
                $ProCateCountRes = mysql_fetch_object($ProCateCount);
                ?>
    <a href="javascript:;" <?php if ($start < mysql_num_rows($ProCateCount) && $page > 0) { ?> onClick="pagination(<?php echo $_REQUEST['sub_category_id'] . ',' . $ProCateCountRes->category . ',' . ($page - 1) . ',\'' . $_REQUEST['type'] . '\''; ?> );" <?php } else { ?> class="inactive-link" <?php } ?>> &#9668;</a> <a href="javascript:;" <?php if ($start < mysql_num_rows($ProCateCount) && (($page + 1) * $per_page) < mysql_num_rows($ProCateCount)) { ?> onClick="pagination(<?php echo $_REQUEST['sub_category_id'] . ',' . $ProCateCountRes->category . ',' . ($page + 1) . ',\'' . $_REQUEST['type'] . '\''; ?>);" <?php } else { ?> class="inactive-link" <?php } ?>> &#9658;</a> </div>
  <!-- End pagination -->
  <div class="home-merchant-slider">
    <div class="merchant-products">
      <?php
                    while ($productData = mysql_fetch_object($ProCate)) {
                        if (empty($productData->image)) {

                            $productImg = $mainsiteUrl . 'images/_no_image.png';
                        } else {
                            $productImg = $mainsiteUrl . 'product_images/' . $productData->image;

                        }
                        $countQuertyData = mysql_num_rows(mysql_query("select * from users where id = '" . $productData->pid . "'"));

                        if ($countQuertyData > 0) {
                            $QuertyData = mysql_fetch_object(mysql_query("select * from users where id = '" . $productData->pid . "'"));
                            $main = new users;
                            $fname = $main->FormatN($QuertyData->first_name);
                            $dname = $main->FormatN($QuertyData->display_name);
                            $pname = $main->FormatPr($productData->product_name);
                            $currencyS = $main->getCurrency($productData->currency);
                        }

                        ?>
      <div class="imagebox items col-md-3">
        <div class="merchant-prod"> <a href="<?php echo $mainsiteUrl . $fname . '/' . $pname . '/' . $productData->id?>">
          <div class="buyNow">Buy Now<span>&#8594;</span></div>
          </a>
          <div><img src="<?php echo $productImg;?>" alt=""/> </div>
          <?php
                                    $new_product_name = $productData->product_name;
                                    $showStr = 18;
                                    if (strlen($productData->product_name) > $showStr) {
                                        $rest = $showStr - $productData->product_name;
                                        $new_product_name = substr($productData->product_name, 0, $rest);
                                        $new_product_name .= '...';
                                    }
                                    ?>
          <h3><?php echo $currencyS . "&nbsp;" . number_format($main->ShowPrice($productData->product_price)); ?></h3>
          <a href="<?php echo $mainsiteUrl . $fname . '/' . $pname . '/' . $productData->id?>">
          <div class="product-title"> <?php echo ucwords(strtolower($new_product_name)); ?> </div>
          </a>
          <div class="customer-name"> <a href="<?php echo $mainsiteUrl . $fname  ?>"> <?php echo strtolower($QuertyData->display_name);?> </a> </div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
  <br />
  <br />
  <a href="<?php echo $mainsiteUrl; ?>category.php?CatId=<?php echo $id_file ?>"> <i style="font-size: 24px">Load more ...</i> </a> </div>
<?php
    }
}

/*==========================Add sub category on Home page============================*/
/*if(isset($_REQUEST['countSubCateg']) && $_REQUEST['countSubCateg'] != ''){ 
  
	$SubCate = mysql_query("SELECT * FROM sub_category where parent_id='".$_REQUEST['category_id']."' LIMIT ".$_REQUEST['countSubCateg'].",5" );
	while($SubcatData = mysql_fetch_object($SubCate)){?>
    
      <div><a id="<?php echo $SubcatData->id; ?>" class="searchBysubcat" href="javascript:;"><?php echo $SubcatData->name; ?></a></div>
   
    <?php
    }
}*/

/*==========================Merchant's Contact us detail ============================*/
if (isset($_REQUEST['mercahntContantDetail']) && $_REQUEST['mercahntContantDetail'] != '') {

    if (isset($_REQUEST['pid']) && $_REQUEST['pid'] != '') {
        $result = mysql_query("select * from users where id='" . base64_decode($_REQUEST['pid']) . "'");
        if (mysql_num_rows($result) > 0) {
            $merchantInfo = mysql_fetch_object($result);
            $to = $merchantInfo->email;
            $email_id = date('Ymhdis') . $merchantInfo->id;
            //$to = 'parmeet@geeksperhr.com';
            $subject = 'Contact Us Form Access Bank ' . $_REQUEST['name'] . ' Email id: ' . $email_id;
            $message .= "New request received\n";
            $message .= "Email id :" . $email_id  . "\n";
            $message .= "Name :" . $_REQUEST['name'] . "\n";
//			$message .= "Contact :".$_REQUEST['phone']."\n";
//			$message .= "Email :".$_REQUEST['email']."\n";
            $message .= "message :" . $_REQUEST['message'] . "\n";
            $headers = "From: info@access.com\r\n";


            $messageFinal = htmlspecialchars($message);
            $email = htmlspecialchars($_REQUEST['email']);
            $tell = htmlspecialchars($_REQUEST['phone']);
            $created_at = date('Y-m-d h:i:s');
            $merchants = $merchantInfo->display_name;


            $sql = "INSERT INTO emails (email_id ,merchant,`message`,`email`,tell,created_at)VALUES ( '" . $email_id . "','" . $merchants . "','" . $messageFinal . "','" . $email . "','" . $tell . "','" . $created_at . "')";
            if (mysql_query($sql)) {
                if (mail($to, $subject, $message, $headers)) {
                    echo '<div class="alert alert-success successMessage" role="alert">Mail has been sent successfully.</div>';
                }
            }else{echo '<div class="alert alert-danger successMessage" role="alert">Mail has not sent. Please try again!</div>';}

        } else {
            echo '<div class="alert alert-danger successMessage" role="alert">Mail has not sent. Please try again!</div>';
        }
    }
}

/*==========================Merchant's Contact us detail ============================*/
if (isset($_REQUEST['mercahntRaing']) && $_REQUEST['mercahntRaing'] != '') {

    if (isset($_REQUEST['pid']) && $_REQUEST['pid'] != '') {

        $checkExistingMerchantRating = mysql_query("SELECT count(`id`) As MerchantRating FROM `rating` WHERE `mer_id` = '" . base64_decode($_REQUEST['pid']) . "'");
        if (mysql_num_rows($checkExistingMerchantRating) > 0) {
            echo '<div class="alert alert-info successMessage" role="alert">You have already posted rating.</div>';
        } else {
            $sql = "INSERT INTO rating (`user_id` ,`mer_id` ,`rating`)VALUES ( '" . $_SESSION['provider_id'] . "', '" . base64_decode($_REQUEST['pid']) . "', '" . $_REQUEST['rating'] . "')";
            if (mysql_query($sql)) {

                echo '<div class="alert alert-success successMessage" role="alert">Rating has been successfully posted.</div>';
            }
        }

    } else {
        echo '<div class="alert alert-danger successMessage" role="alert">Rating has not posted. Please try again!</div>';
    }

}


/*==========================Merchant's Review ============================*/
if (isset($_REQUEST['mercahntRaing_review']) && $_REQUEST['mercahntRaing_review'] != '') {

    if (isset($_REQUEST['pid']) && $_REQUEST['pid'] != '') {


        $checkExistingMerchantRating = mysql_query("SELECT count(`id`) As MerchantRating FROM `rating` WHERE `mer_id` = '" . base64_decode($_REQUEST['pid']) . "'");

        $rating_array = mysql_fetch_array($checkExistingMerchantRating);


        //echo $rating_array['MerchantRating'];

        //echo mysql_num_rows($checkExistingMerchantRating);

        if ($rating_array['MerchantRating'] != 0) {
            echo '<div class="alert alert-info successMessage" role="alert">You have already posted rating.</div>';
        } else {
            $sql = "INSERT INTO rating (`user_id` ,`mer_id` ,`rating`, `comments`)VALUES ( '" . $_SESSION['provider_id'] . "', '" . base64_decode($_REQUEST['pid']) . "', '" . $_REQUEST['rating'] . "', '" . $_REQUEST['comments'] . "')";
            if (mysql_query($sql)) {

                echo '<div class="alert alert-success successMessage" role="alert">Rating has been successfully posted.</div>';
            }
        }

    } else {
        echo '<div class="alert alert-danger successMessage" role="alert">Rating has not posted. Please try again!</div>';
    }

}


/*========================== Filter product on category page============================*/
if (isset($_REQUEST['filterProduct']) && $_REQUEST['filterProduct'] != '') {

    $priceArray = array('between 0 and 10000', 'between 10000 and 50000', 'between 50000 and 100000', '> 100000', '> 0');


    $searchByBrandCon = '';
    $searchByOSCon = '';
    $searchByPriceCon = '';
    $searchByColorCon = '';
    $whereCon = '';
    if (isset($_REQUEST['searchByBrand']) && !empty($_REQUEST['searchByBrand'])) {
        $searchByBrandCon = implode("','", $_REQUEST['searchByBrand']);
        $searchByBrandCon = "'" . $searchByBrandCon . "'";
        $searchByBrandCon = 'brand IN (' . $searchByBrandCon . ')';
        $whereCon .= $searchByBrandCon;
    }
    if (isset($_REQUEST['searchByOS']) && !empty($_REQUEST['searchByOS'])) {
        $searchByOSCon = implode(",", $_REQUEST['searchByOS']);
        $searchByOSCon = 'sub_category IN (' . $searchByOSCon . ')';
        if ($whereCon && $whereCon != '') {
            $whereCon .= ' AND ';
        }
        $whereCon .= $searchByOSCon;
    }

    if (isset($_REQUEST['searchByPrice']) && $_REQUEST['searchByPrice'] != '') {

        if ($_REQUEST['searchByPrice'] > 3)
            $searchByPriceCon = $priceArray[4];
        else
            $searchByPriceCon = $priceArray[$_REQUEST['searchByPrice']];

        $searchByPriceCon = 'product_price ' . $searchByPriceCon;
        if ($whereCon && $whereCon != '') {
            $whereCon .= ' AND ';
        }
        $whereCon .= $searchByPriceCon;
    }
    if (isset($_REQUEST['searchByColor']) && !empty($_REQUEST['searchByColor'])) {
        $searchByColorCon = implode(",", $_REQUEST['searchByColor']);
        $searchByColorCon = ' id IN (SELECT pid FROM product_color WHERE id IN (' . $searchByColorCon . ' ))';

        if ($whereCon && $whereCon != '') {
            $whereCon .= ' AND ';
        }
        $whereCon .= $searchByColorCon;
    }
    if (isset($_REQUEST['searchByCategory']) && $_REQUEST['searchByCategory'] != '') {
        if ($whereCon && $whereCon != '') {
            $whereCon .= ' AND ';
        }
        $whereCon .= 'category = ' . $_REQUEST['searchByCategory'];
    }
    if (isset($_REQUEST['merchantId']) && $_REQUEST['merchantId'] != '') {
        if ($whereCon && $whereCon != '') {
            $whereCon .= ' AND ';
        }
        $whereCon .= 'pid = ' . $_REQUEST['merchantId'];
    }
    $result = mysql_query(" SELECT * FROM `products` WHERE " . $whereCon);

    if (mysql_num_rows($result) > 0) { ?>
<div class="merchant-products" id="category-products">
  <?php
            while ($productData = mysql_fetch_object($result)) {
                if (empty($productData->image)) {

                    $productImg = $mainsiteUrl . 'images/_no_image.png';
                } else {
                    $productImg = $mainsiteUrl . 'product_images/' . $productData->image;
                }

                $countQuertyData = mysql_num_rows(mysql_query("select * from users where id = '" . $productData->pid . "'"));

                if ($countQuertyData > 0) {
                    $QuertyData = mysql_fetch_object(mysql_query("select * from users where id = '" . $productData->pid . "'"));
                    $main = new users;
                    $fname = $main->FormatN($QuertyData->first_name);
                    $dname = $main->FormatN($QuertyData->display_name);
                    $pname = $main->FormatPr($productData->product_name);
                    $currencyS = $main->getCurrency($productData->currency);
                }

                ?>
  <div class="imagebox items col-md-3"> <a href="<?php echo $mainsiteUrl . $fname . '/' . $pname . '/' . $productData->id?>">
    <div class="merchant-prod">
      <div class="buyNow">Buy Now<span>&#8594;</span></div>
      <div><img src="<?php echo $productImg;?>" style="width:100px; height:100px" alt=""/></div>
      <h3><?php echo $currencyS . "&nbsp;" . number_format($main->ShowPrice($productData->product_price)); ?></h3>
      <?php
                            $new_product_name = $productData->product_name;
                            $showStr = 18;
                            if (strlen($productData->product_name) > $showStr) {
                                $rest = $showStr - $productData->product_name;
                                $new_product_name = substr($productData->product_name, 0, $rest);
                                $new_product_name .= '...';
                            }
                            ?>
      <div class="product-title"><?php echo ucwords(strtolower($new_product_name)); ?></div>
      <div class="customer-name"> <a href="<?php echo $mainsiteUrl . $fname  ?>"> <?php echo $QuertyData->display_name;?> </a> </div>
    </div>
    </a> </div>
  <?php
            }?>
</div>
<?php
    } else { ?>
<div class="no-record text-center">No record found.</div>
<?php }
}

/*========================== Filter by category category page============================*/
if (isset($_REQUEST['filterProductByCategory']) && $_REQUEST['filterProductByCategory'] != '') {
    $CategoryId = $_REQUEST['CategoryId']
    ?>
<div class="col-md-3">
  <div class="product-filter no_mobile">
    <div class="filter-by">Filter by</div>
    <hr class="col-md-9 horiz-line"/>
    <h3 class="category-title"><strong>Brand</strong></h3>
    <div class="product-brand">
      <?php
                $ProductQ = mysql_query("select count(`id`) as count_id, `brand`,id from products  where isactive = 't' and category = '" . $catId . "' group by `brand` ");
                while ($ProductData = mysql_fetch_object($ProductQ)) { ?>
      <div>
        <input type="checkbox" class="searchByBrand filterProduct"
                               value="<?php echo $ProductData->brand;?>" name="searchByBrand"/>
        <?php echo ucfirst($ProductData->brand);?> (<?php echo $ProductData->count_id;?>) </div>
      <?php }?>
    </div>
  </div>
  <div class="product-filter no_mobile">
    <hr class="col-md-9 horiz-line"/>
    <h3 class="category-title"><strong>Operating System</strong></h3>
    <div class="product-brand">
      <?php
                $SubCateQ = mysql_query("SELECT sub_category.id, sub_category.name, products.`sub_category`,  products.`category` ,count(products.`id`) as count_id FROM `products` JOIN sub_category ON sub_category.id = products.`sub_category` where products.`category`= '" . $catId . "' group by `sub_category` ");
                while ($SubCatData = mysql_fetch_object($SubCateQ)) { ?>
      <div>
        <input type="checkbox" class="searchByOS filterProduct" value="<?php echo $SubCatData->id;?>"
                               name="searchByOS"/>
        <?php echo ucfirst($SubCatData->name);?> (<?php echo $SubCatData->count_id;?>) </div>
      <?php }?>
    </div>
  </div>
  <div class="product-filter no_mobile">
    <hr class="col-md-9 horiz-line"/>
    <h3 class="category-title"><strong>Price &#x20a6;</strong></h3>
    <div class="product-price">
      <?php
                $ProductPriceQ = mysql_query(" select count(`id`) as count_id from products  where isactive = 't' and category = '" . $catId . "' and product_price between 0 and 10000 ");
                $ProductPrice = mysql_fetch_object($ProductPriceQ); ?>
      <div>
        <input type="radio" class="searchByPrice filterProduct" value="0" name="searchByPrice"/>
        0 - 10,000(<?php echo $ProductPrice->count_id;?>) </div>
      <?php
                $ProductPriceQ = mysql_query(" select count(`id`) as count_id from products  where isactive = 't' and category = '" . $catId . "' and product_price between 10000 and 50000 ");
                $ProductPrice = mysql_fetch_object($ProductPriceQ); ?>
      <div>
        <input type="radio" class="searchByPrice filterProduct" value="1" name="searchByPrice"/>
        10,000 - 50,000(<?php echo $ProductPrice->count_id;?>) </div>
      <?php
                $ProductPriceQ = mysql_query(" select count(`id`) as count_id from products  where isactive = 't' and category = '" . $catId . "' and product_price between 50000 and 100000 ");
                $ProductPrice = mysql_fetch_object($ProductPriceQ); ?>
      <div>
        <input type="radio" class="searchByPrice filterProduct" value="2" name="searchByPrice"/>
        50,000 - 100,000(<?php echo $ProductPrice->count_id;?>) </div>
      <?php
                $ProductPriceQ = mysql_query(" select count(`id`) as count_id from products  where isactive = 't' and category = '" . $catId . "' and product_price > 100000 ");
                $ProductPrice = mysql_fetch_object($ProductPriceQ); ?>
      <div>
        <input type="radio" class="searchByPrice filterProduct" value="3" name="searchByPrice"/>
        > 100,000(<?php echo $ProductPrice->count_id;?>) </div>
    </div>
  </div>
  <div class="product-filter no_mobile">
    <hr class="col-md-9 horiz-line"/>
    <h3 class="category-title"><strong>Colour</strong></h3>
    <div class="color-box">
      <?php
                $ProductColorQ = mysql_query(" SELECT product_color.* FROM `product_color` INNER JOIN products ON products.id = product_color.pid WHERE products.category = '" . $catId . "' GROUP BY product_color.color ");
                while ($ProductColor = mysql_fetch_object($ProductColorQ)) {
                    ?>
      <div myColor="<?php echo $ProductColor->id; ?>" class="products-color filterProduct"
                         style="background-color:<?php echo $ProductColor->color;?>">&nbsp;</div>
      <?php } ?>
    </div>
  </div>
  <div class="product-filter">
    <hr class="col-md-9 horiz-line"/>
    <h3 class="category-title"><strong>Filter By Merchant</strong></h3>
    <div class="merchant-box">
      <?php
                $merchantQ = mysql_query(" SELECT * FROM `users` WHERE isactive = 't' ");?>
      <select id="merchant-id" class="form-control">
        <option value="">Select Merchant</option>
        <?php
                    while ($merchantDetail = mysql_fetch_object($merchantQ)) {
                        ?>
        <option
                            value="<?php echo $merchantDetail->id;?>"><?php echo $merchantDetail->display_name;?></option>
        <?php } ?>
      </select>
    </div>
  </div>
  <div class="no_mobile">
    <?php
            $categoryBanner = mysql_query(" SELECT banner FROM `category` WHERE id = '" . $catId . "' ");
            $ProductColor = mysql_fetch_object($categoryBanner);
            if ($ProductColor->banner != '') {
                ?>
    <img src="<?php echo $mainsiteUrl ?>category_images/<?php echo $ProductColor->banner;?>"
                     alt="<?php echo $ProductColor->banner;?>"/>
    <?php } else { ?>
    <img src="<?php echo $mainsiteUrl ?>images/this-is-lumia.png" alt="This is lumia"/>
    <?php } ?>
  </div>
</div>
<!--Start Product With Category-->
<div class="col-md-9">
  <div class="row">
    <?php
            $ProCate = mysql_query("select products.* from products where  products.isactive = 't' and products.product_type = 'N' and products.category  = '" . $catId . "' ");
            if (mysql_num_rows($ProCate) > 0) {
                ?>
    <div class="merchant-products" id="category-products">
      <?php
                    while ($productData = mysql_fetch_object($ProCate)) {
                        if (empty($productData->image)) {

                            $productImg = $mainsiteUrl . 'images/_no_image.png';
                        } else {
                            $productImg = $mainsiteUrl . 'product_images/' . $productData->image;
                        }

                        $countQuertyData = mysql_num_rows(mysql_query("select * from users where id = '" . $productData->pid . "'"));

                        if ($countQuertyData > 0) {
                            $QuertyData = mysql_fetch_object(mysql_query("select * from users where id = '" . $productData->pid . "'"));
                            $main = new users;
                            $fname = $main->FormatN($QuertyData->first_name);
                            $pname = $main->FormatPr($productData->product_name);
                            $currencyS = $main->getCurrency($productData->currency);
                        }

                        ?>
      <div class="imagebox items col-md-3"> <a href="<?php echo $mainsiteUrl . $fname . '/' . $pname . '/' . $productData->id?>">
        <div class="merchant-prod">
          <div class="buyNow">Buy Now<span>&#8594;</span></div>
          <div><img src="<?php echo $productImg;?>" style="width:100px; height:100px" alt=""/> </div>
          <h3><?php echo $currencyS . "&nbsp;" . number_format($main->ShowPrice($productData->product_price)); ?></h3>
          <?php
                                    $new_product_name = $productData->product_name;
                                    $showStr = 18;
                                    if (strlen($productData->product_name) > $showStr) {
                                        $rest = $showStr - $productData->product_name;
                                        $new_product_name = substr($productData->product_name, 0, $rest);
                                        $new_product_name .= '...';
                                    }
                                    ?>
          <div
                                        class="product-title"><?php echo ucwords(strtolower($new_product_name)); ?></div>
        </div>
        </a> </div>
      <?php
                    }?>
    </div>
    <?php
            } else { ?>
    <div class="no-record text-center">No record found.</div>
    <?php } ?>
  </div>
</div>
<!--End Product With Category-->
<?php
}

