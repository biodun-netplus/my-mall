<?php
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */


if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{      
	 $FedEx=array();
	 $FedEx['cat_id']=$_REQUEST['category'];	
	 $FedEx['origin']=$_REQUEST['origin'];
	 $FedEx['avg_weight']=$_REQUEST['avg_weight'];
	 $FedEx['destination']=$_REQUEST['destination'];
	 $FedEx['type']=$_REQUEST['type'];
	 $FedEx['shipping']=$_REQUEST['shipping'];
	 $FedEx['created']=date("Y-m-d H:i:s");
	 $FedEx['modified']=date("Y-m-d H:i:s");
	 $FedEx_Obj= new users;
	 $FedEx_Res= $FedEx_Obj->update($FedEx,"fedex",$_POST['id']);
	 if($FedEx_Res)
	 {
	    $_SESSION['msg'] = 'FedEx NG Shipping Updated Successfully';
		header("location:fedex.php");
	 }
}
	$Result =mysql_fetch_object(mysql_query("select * from fedex where id='".$_REQUEST['id']."'"));
	$query4 = mysql_query("select * from category where isactive='t'");
	$query3 = mysql_query("select * from sh_cities order by name ASC");

 ?>
<script type="text/javascript" src="js/fedex.js"></script>
<div class="clear"></div>
 <!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content" style="">


<div >
<div id="page-heading"><h1>Add FedEx NG Shipping</h1></div>
<div style="float:right; padding-right:20px;"><input type="button" class="buttons btn btn-info" value="Back" onclick="goBack()" /></div>
</div>


<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
				
				<!--  start message-red -->
				
				<!--  end message-red -->
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td align="left" style="padding-left:110px;">
	
	
		<!--  start step-holder -->
		<!--  end step-holder -->
        <!-- start id-form -->
        <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" id="FedExForm">
		<table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
        <tr>
			<th valign="top">Category:</th>
			<td>
            	<select id="category" name="category" class="inp-select form-control" >
            		<option value="">Category</option>
                    <?php while($data4 = mysql_fetch_object($query4)){ ?>  
                    <option <?php if($Result->cat_id==$data4->id){ ?> selected="selected" <?php } ?> value="<?php echo $data4->id ?>"><?php echo $data4->category_name; ?></option> 
					<?php } ?>
            	</select>
            </td>
			<td><label class="error"  id="category_error"></label></td>
			</tr>
        <tr>
			<th valign="top">Average Weight</th>
			<td>
            	<select id="avg_weight" name="avg_weight" class="inp-select form-control" >
                  	<option value="">Average Weight</option>
                    <option <?php if($Result->avg_weight =='0.1'){ ?> selected="selected" <?php } ?>  value="0.1">0.1 KG</option>  
                    <option <?php if($Result->avg_weight =='0.2'){ ?> selected="selected" <?php } ?>  value="0.2">0.2 KG</option> 
                    <option <?php if($Result->avg_weight =='0.3'){ ?> selected="selected" <?php } ?>  value="0.3">0.3 KG</option> 
                    <option <?php if($Result->avg_weight =='0.4'){ ?> selected="selected" <?php } ?>  value="0.4">0.4 KG</option> 
                    <option <?php if($Result->avg_weight =='0.5'){ ?> selected="selected" <?php } ?>  value="0.5">0.5 KG</option> 
                    <option <?php if($Result->avg_weight =='1'){ ?> selected="selected" <?php } ?>  value="1">1 KG</option> 
                    <option <?php if($Result->avg_weight =='1.5'){ ?> selected="selected" <?php } ?>  value="1.5">1.5 KG</option> 
                    <option <?php if($Result->avg_weight =='2'){ ?> selected="selected" <?php } ?>  value="2">2 KG</option>
                    <option <?php if($Result->avg_weight =='2.5'){ ?> selected="selected" <?php } ?>  value="2.5">2.5 KG</option>
                    <option <?php if($Result->avg_weight =='3'){ ?> selected="selected" <?php } ?>  value="3">3 KG</option>
                    <option <?php if($Result->avg_weight =='3.5'){ ?> selected="selected" <?php } ?>  value="3.5">3.5 KG</option>
                    <option <?php if($Result->avg_weight =='4'){ ?> selected="selected" <?php } ?>  value="4">4 KG</option>
                    <option <?php if($Result->avg_weight =='4.5'){ ?> selected="selected" <?php } ?>  value="4.5">4.5 KG</option>
                    <option <?php if($Result->avg_weight =='5'){ ?> selected="selected" <?php } ?>  value="1">5 KG</option>
            	</select>
            </td>
			<td><label class="error"  id="avg_weight_error"></label></td>
			</tr>
        <tr>
			<th valign="top">Origin:</th>
			<td>
            	<select id="origin" name="origin" class="inp-select form-control" >
                    <option selected="selected" value="lagos">Lagos</option>
            	</select>
            </td>
			<td><label class="error"  id="origin_error"></label></td>
			</tr>
        <tr>
			<th valign="top">Destination:</th>
			<td>
            	<select id="destination" name="destination" class="inp-select form-control" >
                   	<option value="">Destination</option>
                   <?php while($data3 = mysql_fetch_object($query3)){ ?>  
                    <option <?php if($Result->destination ==$data3->id){ ?> selected="selected" <?php } ?>   value="<?php echo $data3->id; ?>"><?php echo $data3->name; ?></option> 
					<?php } ?>
            	</select>
            </td>
			<td><label class="error"  id="destination_error"></label></td>
			</tr>
		<tr>
			<th valign="top">Shipping:</th>
			<td><input type="text" class="form-control" id="shipping" name="shipping" value="<?php echo $Result->shipping; ?>" /></td>
			<td><label class="error"  id="shipping_error"></label><input type="hidden" name="id" value="<?php echo $Result->id; ?>" /></td>
		</tr>
		<tr>
			<th valign="top">Destination Type:</th>
			<td>
            	<select id="type" name="type" class="inp-select form-control" >
                   	<option value="">Destination Type</option>
                    <option <?php if($Result->type =='INTRA AREA'){ ?> selected="selected" <?php } ?>  value="INTRA AREA">INTRA AREA</option>
                    <option <?php if($Result->type =='INTER AREA'){ ?> selected="selected" <?php } ?>  value="INTER AREA">INTER AREA</option>
                    <option <?php if($Result->type =='NATION WIDE'){ ?> selected="selected" <?php } ?>  value="NATION WIDE">NATION WIDE</option>
                    <option <?php if($Result->type =='DIRECT'){ ?> selected="selected" <?php } ?>  value="DIRECT">DIRECT</option>
            	</select>
            </td>
			<td><label class="error"  id="type_error"></label></td>
			</tr>
	<tr>
		<th>&nbsp;</th>
		<td valign="top">
			<input type="submit" id="FedEx_Submit" value="submit" name="submit" class="form-submit btn btn-default" />
			<input type="reset" value="Reset" class="form-reset btn btn-default"  />		</td>
		</tr>
	</table>
   		 </form>
	<!-- end id-form  -->	</td>
	
</tr>
<tr>
<td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
	
	<div class="clear"></div>
 

</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>

<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>