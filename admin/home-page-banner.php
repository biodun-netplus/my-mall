<?php 
include("header.php");
/* Home page banner management */
/* Author @Vijay Verma */
/* Date : 29-may-2015 */

if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{  
  if(isset($_FILES['image']['name']) && $_FILES['image']['name']!=''){ 
    
    
    $name     =  $_FILES['image']['name'];
    $tname    = $_FILES['image']['tmp_name'];
    $ext      = pathinfo($name, PATHINFO_EXTENSION);
    $path     = "../upload_home_slider_image/".time().'.'.$ext;
    $newName  =  time().'.'.$ext;
    //$path = $_FILES['image']['name'];
    move_uploaded_file($tname,$path);

    if(isset($_POST['old_image']) && $_POST['old_image'] != '')
    {
      if(file_exists("../upload_home_slider_image/".$_POST['old_image']))
      unlink("../upload_home_slider_image/".$_POST['old_image']);

    }


  }
  else {
    $path = $_POST['old_image'];
  }

   $sql = "UPDATE `home-page-slider-image` SET `path`='".$newName."',`description`='".$_POST['description']."'  WHERE `id`='1'"; 
     mysql_query($sql);
   header('Location:home-page-banner.php');
  
}

$query = "SELECT * FROM `home-page-slider-image` WHERE id = 1 ";
$result = mysql_query($query) or die(mysql_error());
$num_rows = mysql_num_rows($result);
?>

<script type="text/javascript" src="js/validation.js"></script>
 <div class="clear"></div>
 
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">

<div>
  <div id="page-heading"><h1>Home Page Banner</h1></div> 
</div>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
  <th rowspan="3" class="sized"></th>
  <th class="topleft"></th>
  <td id="tbl-border-top">&nbsp;</td>
  <th class="topright"></th>
  <th rowspan="3" class="sized"></th>
</tr>
<tr>
  <td id="tbl-border-left"></td>
  <td>
  <!--  start content-table-inner -->
  <div id="content-table-inner">
  <!--  start message-red -->
   <!--  end message-green -->
  
  <table border="0" width="100%" cellpadding="0" cellspacing="0">
  <tr valign="top">
  <td align="">
  
    <!--  start step-holder -->
    <!--  end step-holder -->
        <!-- start id-form -->
    <form action="" name="form1" method="post" enctype="multipart/form-data">
    <table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
      <?php while($row = mysql_fetch_array($result)){ ?>
         <tr>
            <th valign="top">Description:</th>
              <td><textarea style="height: 160px; width: 370px;" name="description" class="text-area"><?php echo $row['description']; ?></textarea></td>
          </tr>
          <tr>
          <?php if($row['path']!=''){ ?>
          <tr>
            <th valign="top">Banner: </th>
            <td><img src="../upload_home_slider_image/<?php echo $row['path']; ?>" style="width: 370px;border: 1px solid gray;padding: 5px;border-radius: 5px;" /></td>
          </tr>
          <?php } // end of if ?>
          <tr>
            <th valign="top">Upload: </th>
            <td><input type="file" id="image" name="image" />
                <span id="error_show" style="color:red"></span>
                <input type="hidden" id="id" name="id" value="<?php echo $row['id']; ?>" />
                <input type="hidden" id="old_image" name="old_image" value="<?php echo $row['path']; ?>" />
            </td>
          </tr>
       <tr>
        <?php } // end of while loop ?>
    <th>&nbsp;</th>
    <td valign="top">
      <input type="submit" name="submit" value="submit" class="form-submit btn btn-default" onclick="return validation();" />
    </td>
    </tr>
  </table>
       </form>
  <!-- end id-form  --> </td>
  
</tr>
<tr>
<td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
  
  <div class="clear"></div>
 

</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
  <th class="sized bottomleft"></th>
  <td id="tbl-border-bottom">&nbsp;</td>
  <th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
<?php include("footer.php")?>

<script type="text/javascript">
function validation(){   

var fup = document.getElementById('image');
var fileName = fup.value; 
var ext = fileName.substring(fileName.lastIndexOf('.') +  1);
  
  if(ext == "gif" || ext == "GIF" || ext == "jpg" || ext == "JPG" || ext == "jpeg" || ext == "JPEG" || ext == "png" || ext == "PNG" || ext == "bmp" || ext == "BMP" || fileName=='')
  {
    document.getElementById("error_show").innerHTML="";
  }else{
    
    document.getElementById("error_show").innerHTML="Upload only jpg,jpeg,gif,png,bmp image.";
    fup.focus();
    return false;
  }
   
return true;  
}
</script>

<style type="text/css">
.text-area{
  border: 1px solid gray;
  color: #393939;
  height: 60px;
  padding: 0px 0px 0px 6px;
  border-radius: 7px;
  width: 188px;
}
</style>
    