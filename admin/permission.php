<?php 
include("header.php");
/* permission layout */
/* Author @Parmeet */
/* Date : 24-04-2015 */
?>
<style type="text/css">
.alert-danger{
    background-color: #f65b4b;
    border-color: #e74c3c;
    color: #ffffff;
	font-size: 15px;
  	width: 50%;
  	margin: 0 auto;
	margin-top: 7%;
}
.alert {
    border: 1px solid transparent;
    border-radius: 4px;
    margin-bottom: 21px;
    padding: 15px;
}
</style>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer" style="min-height:460px;"> 
  <!-- start content -->
  <div id="content">
    <div class="alert alert-danger" role="alert"> <strong>Warning! </strong>You don't have enough permission to access this area.</div>
  </div>
  <!-- end content --> 
</div>
<!-- end content-outer -->
<?php include("footer.php")?>
