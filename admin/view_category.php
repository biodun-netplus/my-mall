<?php 
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */
	
	?>
     <script type="text/javascript">
function goBack()
  {
  window.history.back()
  }
</script>

  
 <div class="clear"></div>
 
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content" style="">

<div>
<div id="page-heading"><h1>Category Details</h1></div>
<div style="float:right; padding-right:20px;"><input type="button" class="buttons btn btn-info" value="Back" onclick="goBack()" /></div>
</div>


<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"></th>
    
</tr>
<tr>
	<td id="tbl-border-left"> </td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
		
			<!--  start table-content  -->
			<div id="table-content">
            <?php 
			//echo "aaaaaaaaaaaaaaaaaaaaaaa";
			//print_r($_REQUEST['view_id']);
			$main= new users;
			$xx= $main->viewprofile($_REQUEST['view_id'],"category");
			//print_r($xx);
			?>
			<table width="100%" border="0" cellpadding="0" cellspacing="0"  id="id-form">
			<tr>
			<th width="150" align="left" valign="top">Category name:</th>
			<td align="left" valign="top"><?php echo ucfirst($xx->category_name);?></td>
			</tr>
			<tr>
			<th align="left" valign="top">Description:</th>
			<td align="left" valign="top"><?php echo ucfirst($xx->message);?></td>
			</tr>
			<tr>
			<th align="left" valign="top">Image:</th>
			<td align="left" valign="top"><img src="../category_images/<?php echo $xx->image;?>" height="200" width="300" /></td>
			</tr>
		
			
			</table>
		
			</div>
			
<div class="clear"></div>
		 
		</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
    

<?php include("footer.php")?>