<?php 
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */


if(isset($_REQUEST['BrandNameSubmit']) && $_REQUEST['BrandNameSubmit']=='submit')
{  
	$User = array(); 
	$main = new users;  
	$User['type'] = 'brand';	
	$User['name'] = strtolower($_REQUEST['BrandName']);
	$xx= $main->ADD($User,"color_or_brands");
	if($xx)
	{
		
		header("location:color-brands.php");
	}
}
if(isset($_REQUEST['ColorNameSubmit']) && $_REQUEST['ColorNameSubmit']=='submit')
{  
	$User = array(); 
	$main = new users;  
	$User['type'] = 'color';	
	$User['name'] = strtolower($_REQUEST['ColorName']);
	$xx= $main->ADD($User,"color_or_brands");
	if($xx)
	{
		
		header("location:color-brands.php");
	}
}
$SqlQuery = mysql_query("SELECT * FROM category WHERE isactive = 't'");

 ?>
 <script type="text/javascript">
 	$(document).ready(function()
	{
		$("#BrandName").blur(function()
		{
			if($("#BrandName").val()=='')
			{
				$("#BrandNameError").text('Enter Brand Name!')
				return false;
			}
			else
			{
				$("#BrandNameError").text('');
				return false;
			}
		})
		
		$("#BrandNameSubmit").click(function()
		{
			if($("#BrandName").val()=='')
			{
				$("#BrandNameError").text('Enter Brand Name!')
				return false;
			}
			else
			{
				return true;
			}
		})
		$("#ColorName").blur(function()
		{
			if($("#ColorName").val()=='')
			{
				$("#ColorNameError").text('Enter Color Name!')
				return false;
			}
			else
			{
				$("#ColorNameError").text('');
				return false;
			}
		})
		
		$("#ColorNameSubmit").click(function()
		{
			if($("#ColorName").val()=='')
			{
				$("#ColorNameError").text('Enter Color Name!')
				return false;
			}
			else
			{
				return true;
			}
		})
	})
 
 </script>

 <div class="clear"></div>
 
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content" style="">

<div >
<?php if($_REQUEST['hint']=='brand'){ ?><div id="page-heading"><h1>Add Brands</h1></div> <?php }else{ ?>
<div id="page-heading"><h1>Add Color</h1></div>
<?php } ?>
<div style="float:right; padding-right:20px;"><input type="button" class="buttons btn btn-info" value="Back" onclick="history.back();" /></div>
</div>


<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
				
				<!--  start message-red -->
				
				<!--  end message-red -->
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td>
    <?php if($_REQUEST['hint']=='brand'){ ?>
        <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" enctype="multipart/form-data">
	<table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
		<tr>
			<th valign="top">Brand Name:</th>
			<td><input type="text" class="form-control" id="BrandName" name="BrandName" value="" /></td>
			<td width="301"><label class="error"  id="BrandNameError"></label></td>
		</tr>
		
	<tr>
	
		<th>&nbsp;</th>
		<td valign="top">
			<input type="submit" name="BrandNameSubmit" id="BrandNameSubmit" value="submit" class="form-submit btn btn-default" />
			<input type="reset" value="Reset" class="form-reset btn btn-default"  />		</td>
		</tr>
	</table>
   		 </form>
         <?php }else
		 { ?>
         <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" enctype="multipart/form-data">
	<table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
		<tr>
			<th valign="top">Color Name:</th>
			<td><input type="text" class="form-control" id="ColorName" name="ColorName" value="" /></td>
			<td width="301"><label class="error"  id="ColorNameError"></label></td>
		</tr>
		
	<tr>
	
		<th>&nbsp;</th>
		<td valign="top">
			<input type="submit" name="ColorNameSubmit" id="ColorNameSubmit" value="submit" class="form-submit btn btn-default" />
			<input type="reset" value="Reset" class="form-reset btn btn-default"  />		</td>
		</tr>
	</table>
   		 </form>
         <?php } ?>
	<!-- end id-form  -->	</td>
	
</tr>
<tr>
<td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
	
	<div class="clear"></div>
 

</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
    <?php include("footer.php")?>