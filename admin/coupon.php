<?php 
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */
	
	?>
 <div class="clear"></div>
 
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<script type="text/javascript">
$().ready(function(){
$('#delete').click(function(){
	var del=[];
	$("input[type='checkbox']:checked").each(function(){
	del.push($(this).val());
	$(this).parent("td").parent('tr').addClass('RowHighlight');
}); 
if(del=='')
{
	alert("Please select at least one item.");
}
else
	if(confirm("Are you sure want to delete the selected items")){
  		 $.ajax({
    		type:'post',
   		 	url: "deletesignups.php?DelBrand="+del,
     		success: function()
   			{
    		$(".RowHighlight").remove();	
     		}
 		 });
	  }
})
$(".DelCoupon").click(function(){
	var id=$(this).attr("alt")
	$(this).parent("td").parent('tr').addClass('RowHighlight');
	if(confirm("Are you sure want to delete this items ?")){
		$.ajax({
			type: 'POST',
			url: "delete_category.php?DelCoupon="+id,
			success: function()
			{
    			$(".RowHighlight").remove();
     		}
		});
	}
	else
	{
		return false;
	}
	})
})
function hilite(elem)
{
	elem.style.background = '#FFC';
}

function lowlite(elem)
{
	elem.style.background = '';
}

</script>

 <div class="row bottom-margin">
  <div id="page-heading">
    <h1>Coupon for Reward</h1>
  </div>
  <?php
	$query = "SELECT COUNT(*) FROM reward_coupon";
	$result = mysql_query($query) or die(mysql_error());
	$num_rows = mysql_fetch_row($result);
	$pages = new Paginator;
	$pages->items_total = $num_rows[0];
	$pages->mid_range = 9; // Number of pages to display. Must be odd and > 3
	$pages->default_ipp = '10'; 
	$pages->paginate();

	//echo $pages->display_pages();
	echo "<span class=\"\">".$pages->display_jump_menu().$pages->display_items_per_page()."</span>";

	 $query = "SELECT * FROM reward_coupon order by id desc $pages->limit";
	//echo $query;
	$result = mysql_query($query) or die(mysql_error());

	$countCondition = mysql_num_rows($result);
	?>
  <div class="add-button col-lg-5"> <a href="create_coupon.php"><input type="button" class="buttons btn btn-info" value="Create Coupon"/></a>
    </a> </div>
  <div id="paging-table" class="col-lg-6"><?php echo $pages->display_pages(); ?></div>
</div>
          
<?php if(isset($_SESSION['msg']) && $_SESSION['msg']!='')  {?>
                                
	<div id="message-success" align="center">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td class="green-left"><?php echo $_SESSION['msg'];  $_SESSION['msg'] ='';?><a href=""></a></td>
			<td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif"   alt="" /></a></td>
		</tr>
	</table>
	</div>          
<?php } ?>
	<!--  start content-table-inner -->
<div id="content-table-inner">

	<!--  start table-content  -->
<form id="mainform" action="" class="table-responsive">

		<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="table table-bordered table-hover table-striped">
		<tr>
			<th width="13%" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Serial No.</a>	</th>
            <th width="13%" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Coupon Code</a>	</th>
            <th width="13%" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Email Address</a>	</th>
            <th width="13%" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Coupon Value</a>	</th>
            <th width="13%" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Start Date</a></th>
            <th width="13%" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Expire Date</a></th>
            <th width="13%" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Session</a></th>
            <th width="13%" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Country</a></th>
            <th width="13%" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Status </a></th>
		    <th  class="table-header-repeat line-left"><a class="arrow_none">Action</a></th>
		</tr>
        <?php 
		if($countCondition > 0){
			if(!isset($_REQUEST['page']) || $_REQUEST['page']=='1')
			{
				$SrNo = 1;
			}
			else
			{
				$SrNo = ($_REQUEST['page']*10)-9;
			}
		
		while($data = mysql_fetch_object($result)){ 
		
		?>

		<tr>
			<?php /*?><td width="20"><input  type="checkbox" name="checkbox" value="<?php echo $data->id;?>"/></td><?php */?>
			<td align="left" style="text-align:center;"><?php echo $SrNo ?></td>
			<td align="left" style="text-align:center;"><?php echo $data->code; ?></td>
			<td align="left" style="text-align:center;"><?php echo $data->email; ?></td>
            <td align="left" style="text-align:center;"><?php if($data->country=='US' && $data->type=='V'){ echo "$ ";}elseif($data->country=='NG' && $data->type=='V'){echo "&#8358; ";} echo number_format($data->value);if($data->type=='P'){ echo "%";} ?></td>
            <td align="left" style="text-align:center;"><?php echo $data->start_date; ?></td>
             <td align="left" style="text-align:center;"><?php echo $data->expire_date; ?></td>
              <td align="left" style="text-align:center;"><?php if($data->session=='S'){ echo "One Time";}else{echo "Multiple";}?></td>
              <td align="left" style="text-align:center;"><?php if($data->country=='US'){ echo "United State";}else{echo "Nigeria";}?></td>
               <td align="left" style="text-align:center;"><?php echo $data->status; ?></td>
		  	<td>
                <a href="javascript:;" class="DelCoupon" alt="<?php echo $data->id;?>" title="Click To Delete Sub Category">
                    <img src="images/table/action_delete.gif"/>
                </a>
               <!-- <a href="edit_color-brands.php?edit_id=<?php //echo $data->id;?>&hint=<?php //echo $data->type; ?>" title="Click To Edit Sub Category"> 
                    <img src="images/table/action_edit.gif"/>
                </a>-->
            </td>
		</tr>
        <?php $SrNo++;
		}
		}
		else{ ?>
        
		<tr>
        	<td colspan="8" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>
        </tr>
		<?php }?>
		</table>
		<!--  end product-table................................... --> 
		</form>
	
	  <div class="clear"></div>
 
</div>
<!--  end content-table-inner  -->

<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
<?php include("footer.php") ?>