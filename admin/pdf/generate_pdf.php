<?php
require_once('../dompdf-master/dompdf_config.inc.php');
require_once("../includes/classes/db_connect.php");

$fedexDelivery			= 0;
$fidelityAmount 		= 0;
$netplusAmount 			= 0;
$html               = '';
$query2 = "SELECT * FROM `order`  
    INNER JOIN cart ON (`order`.order_id = cart.order_id)
    INNER JOIN products ON (cart.product_id = products.id)
    INNER JOIN users ON (products.pid = users.id) 
    where `order`.`created` < '" . htmlspecialchars($_REQUEST['endDate'], ENT_QUOTES)." 00:00:00' AND `order`.`created` > '" . htmlspecialchars($_REQUEST['startDate'], ENT_QUOTES) . " 00:00:00' AND `order`.payment_type = '" . htmlspecialchars($_REQUEST['type'], ENT_QUOTES) ."' AND `order`.status = 'Completed' GROUP BY `order`.transaction_ref";
//echo $countTotal ;
$html .=
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Settlement pdf</title>
  </head>
  <style>@page { margin: 15px 25px 15px 25px; } body { overflow: auto; padding: 0px; margin: 0px;} body{  font-family:Arial, Helvetica, sans-serif; font-size:13px; }</style>
  <body>
    <div>
      ';
     $html .=' <div style="overflow-y: auto;overflow-x: auto; margin-top:10px;">
        <table width="100%">

          <tr >
          <th style="text-align: left;"><b>Transaction Date</b></th>
            <th style="text-align: left;"><b>Transaction Ref</b></th>

            <th style="text-align: left;"><b>Actual Settlement</b></th>
            <th style="text-align: left;"><b>interswitch 1.5% on fedex</b></th>
            <th style="text-align: left;"><b>Fedex</b></th>
            <th style="text-align: left;"><b>Fee</b></th>
            <th style="text-align: left;"><b>Net To Be Settled To Merchant</b></th>
            <th style="text-align: left;"><b>Netplus</b></th>
            <th style="text-align: left;"><b>Ecobank</b></th>
            <th style="text-align: left;"><b>Account No </b></th>
            <th style="text-align: left;"><b>Name </b></th>
          </tr>';
$result = mysql_query( $query2) or die(mysql_error());
while ($data = mysql_fetch_object($result)) {
//           var_dump($data);
    $sum_total = $data->total_ammount;
    $transaction_date = $data->created;
    $transaction_ref = $data->transaction_ref;
    $account_no = $data->account_type;
    $merchant_name = $data->first_name;

    $interswitch_amount = ((1.5/100)*$sum_total);


    $fedex_amount = $data->delivery_amt;
    $actual_settlement = $sum_total - $interswitch_amount;
    $settledAmount  = ($actual_settlement-$fedex_amount);
    $merchant_amount  = ((97/100)*$settledAmount);
    $shared_amount = ((3/100)*$settledAmount);
    $eco_amount  = ((70/100)*$shared_amount);
    $netplus_amount  = ((30/100)*$shared_amount);



//    var_dump($me);


              $html .='

              <tr>
                <td style="text-align: left;">'.$transaction_date.'</td>
                <td style="text-align: left;">'.$transaction_ref.'</td>

                <td style="text-align: left;">'.$actual_settlement.'</td>
                <td style="text-align: left;">'.$interswitch_amount.'</td>
                <td style="text-align: left;">'.$fedex_amount.'</td>
                <td style="text-align: left;">'.$shared_amount.'</td>
                <td style="text-align: left;">'.$merchant_amount.'</td>
                <td style="text-align: left;">'.$netplus_amount.'</td>
                <td style="text-align: left;">'.$eco_amount.'</td>
                <td style="text-align: left;">'.$account_no.'</td>
                <td style="text-align: left;">'.$merchant_name.'</td>
              </tr>';
}
        $html .='  
        </table>
      </div>
    </div>

  </body></html>'; 

  def("DOMPDF_ENABLE_REMOTE", false);
  // see: http://getcomposer.org/doc/00-intro.md

  // disable DOMPDF's internal autoloader if you are using Composer
  def('DOMPDF_ENABLE_AUTOLOAD', false);
 //echo $html; 

  $dompdf = new DOMPDF();
  $dompdf->load_html($html);
  $dompdf->render();
  $dompdf->stream("settlement_". str_replace(' ','_',$_REQUEST['type']) ."_from_" . $_REQUEST['startDate'] . "_to_". $_REQUEST['endDate'] .".pdf");
  
  // The next call will store the entire PDF as a string in $pdf
  // $pdf = $dompdf->output();


die();

