<?php 
include("ship_header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */
$main =new users();
$result = $main->viewprofile($_SESSION['FedExId'],'admin');
if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{    
 	$password1 = $result->password;
	if($password1==$_REQUEST['oldpassword'])
	{
		$filds=array('password','email_id ');
		$value=array($_REQUEST['newpassword'],$_REQUEST['Email']);
		$conditions=array_combine($filds,$value);	
		$id=$_SESSION['FedExId'];
	 	$main->Changepassword($conditions,'admin',$id);
		header("Location:FedEx_settings.php?msg=c_password");	
	}
	else
	{
		$show ="Old password did  not match";
	}
}
 ?>
 <script type="text/javascript">
  $(document).ready(function(){
	  $('#submit').click(function(){
		  	var a = $("#Email").val();
			var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
			if(a=='')
			{
				alert("Please enter email");
				$('#Email').focus();
				return false;
			}
			else if(!filter.test(a)){
				alert("Please enter valid email");
				$('#Email').focus();
				return false;
			}
		  	var oldpassword=$('#oldpassword').val()
			if(oldpassword=='')
			{
				alert("please enter the old password");
				$('#oldpassword').focus();
				return false;
			}
			var newpassword=$('#newpassword').val();
			if(newpassword=='')
			{
				alert("Please enter the new password");
				$('#newpassword').focus();
				return false;
			}
			var confirmPassword=$('#confirmPassword').val();
			if(confirmPassword=='')
			{
				alert("Please enter the confirm password");
				$('#confirmPassword').focus();
				return false;
			}
		  
		  var newpassword=$('#newpassword').val();
		  var confirmPassword=$('#confirmPassword').val();
		  if(newpassword!==confirmPassword)
		  {
			  alert("New Password And Confirm password did not match please enter the same password")
			  $('#newpassword').focus();
			  return false;
		  }
		  return true;
	
		  })
	  })
</script>

 <div class="clear"></div>
 
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content" style="">
<div>
<div id="page-heading"><h1>Update FedEx Admin Password</h1></div>
<div style="float:right; padding-right:20px;"><a href="settings.php"><input type="button" class="buttons btn btn-info" value="Back" /></a></div>
</div>


<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
				
				<!--  start message-red -->
				
				<!--  end message-green -->
		
		 
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td align="center">
	
	
		<!--  start step-holder -->
		<!--  end step-holder -->
        <!-- start id-form -->
        <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post">
		<table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
		
		<?php if(isset($show)){ ?><tr>
        <td>&nbsp;</td><td style="color:#F00"> <?php echo  $show;?></td></tr><?php  } ?>
		<tr>
			<th valign="top">Email:</th>
			<td><input type="text" class="form-control" value="<?php echo $result->email_id; ?>" id="Email" name="Email" autocomplete="off" /></td>
			</tr>	
		<tr>
			<th valign="top">Old password:</th>
			<td><input type="password" class="form-control" id="oldpassword" name="oldpassword" autocomplete="off" /></td>
			</tr>
            <tr>
			<th valign="top">New password :</th>
			<td><input type="password" class="form-control" id="newpassword" name="newpassword" autocomplete="off" /></td>
			</tr>
			<tr>
			<th valign="top">Confirm password:</th>
			<td><input type="password" class="form-control" id="confirmPassword" name="confirmPassword"  autocomplete="off" /></td>
			</tr>
			
		
	<tr>
	
		<th>&nbsp;</th>
		<td valign="top">
			<input type="submit" name="submit" id="submit" value="submit" class="form-submit btn btn-default" />
			<input type="reset" value="Reset" class="form-reset btn btn-default"  />		</td>
		</tr>
	</table>
   		 </form>
	<!-- end id-form  -->	</td>
	
</tr>
<tr>
<td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
	
	<div class="clear"></div>
 

</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
    <?php include("footer.php")?>