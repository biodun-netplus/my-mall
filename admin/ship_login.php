<?php 
ob_start();
include("includes/classes/functions.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>WebmallNG : Admin</title>
<link rel="stylesheet" href="css/screen.css" type="text/css" media="screen" title="default" />
<!--  jquery core -->
<script src="js/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>

<!-- Custom jquery scripts -->
<script src="js/jquery/custom_jquery.js" type="text/javascript"></script>

<!-- MUST BE THE LAST SCRIPT IN <HEAD></HEAD></HEAD> png fix -->
<script src="js/jquery/jquery.pngFix.pack.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
$(document).pngFix( );
});
</script>
<?php 


if(isset($_REQUEST['submit']) && $_REQUEST['submit'] =="submit-login")
{
    $username= $_REQUEST['username'];
    $password= $_REQUEST['password'];
	$main= new users;
	$data = $main->fedex_login($username,$password);
	echo $data;
}

?>
</head>
<body id="login-bg"> 
 
<!-- Start: login-holder -->
<div id="login-holder">

	<!-- start logo -->
	<div id="logo-login" >
		<a href="index.php"><img src="images/shared/fedex_logo.png" alt="" /></a>
	</div>
	<!-- end logo -->
	
	<div class="clear"></div>
	
	<!--  start loginbox ................................................................................. -->
	<div id="loginbox">
    <?php if(isset($data))
	{?>
	<div id="forgotbox-text"><?php echo $data;?></div>
	<?php }?>
	<!--  start login-inner -->
	<div id="login-inner">
		<form action="<?php $_SERVER['PHP_SELF'];?>" method="post" name="form1">
        <table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<th>Username</th>
			<td><input type="text" name="username" value=""  class="login-inp" /></td>
		</tr>
		<tr>
			<th>Password</th>
			<td><input type="password" value="" name="password"  class="login-inp" /></td>
		</tr>
		<tr>
			<th></th>
			<td valign="top"><input type="checkbox" class="checkbox-size" id="login-check" /><label for="login-check">Remember me</label></td>
		</tr>
		<tr>
			<th></th>
			<td><input type="submit" value="submit-login" name="submit" class="submit-login"  /></td>
		</tr>
		</table></form>
	</div>
 	<!--  end login-inner -->
	<div class="clear"></div>
	<a href="" class="forgot-pwd">Forgot Password?</a>
 </div>
 <!--  end loginbox -->
 
	<!--  start forgotbox ................................................................................... -->
	<div id="forgotbox">
		<div id="forgotbox-text">Please enter your email and we'll send your password.</div>
       <?php 
	   if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='Send Password')
	   {
		$main= new users();
		$result1= $main->view('admin');
		//print_r($result1);
		$U_name= $result1[0]->username;
		$U_email= $result1[0]->email_id;
		$U_password= $result1[0]->password;
		 //print_r($_REQUEST['email_id']);
		 if($_REQUEST['email_id']==$U_email);
		 {
			 $to=$U_email;
			 $subject= "Forget PassWord Detalies" ;
			 $message= "username"."=".$U_name."And"."password"."=".$U_password;
			 mail($to, $subject, $message );
			 
		 }
	   }
	   ?>
		<!--  start forgot-inner -->
		<div id="forgot-inner">
        <form name="form1" action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<th>Email address:</th>
			<td><input type="text" value="" name="email_id"   class="login-inp" /></td>
		</tr>
		<tr>
			<th> </th>
			<td><input type="submit" name="submit" class="submit-login" value="Send Password"  /></td>
		</tr>
		</table>
        </form>
		</div>
		<!--  end forgot-inner -->
		<div class="clear"></div>
		<a href="" class="back-login">Back to login</a>
	</div>
	<!--  end forgotbox -->

</div>
<!-- End: login-holder -->
</body>
</html>