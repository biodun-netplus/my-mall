<?php 
include("header.php");	
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */

	?>
 <div class="clear"></div>

<script type="text/javascript">
function hilite(elem)
{
	elem.style.background = '#FFC';
}

function lowlite(elem)
{
	elem.style.background = '';
}
</script> 
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">
<script type="text/javascript">
function changeStatus(id, status)
{
		
		$.ajax({
			type: 'POST',
			url: "delete_category.php?proid="+id+"&status="+status,
			success: function()
			{
    		window.location.reload()
     		}
		});
	}

$().ready(function()
{
	
	$('#delete').click(function()
	{
		var del=[];
		$("input[type='checkbox']:checked").each(function()
		{
			del.push($(this).val());
			$(this).parent("td").parent('tr').addClass('RowHighlight');
		}); 
		if(del=="")
	 {
		 alert("Please select at least one item.")
	 }
	 else
		if(confirm("Are you sure want to delete the selected item(s)?"))
		{
			$.ajax({
				type:'post',
				url: "delete_category.php?Mprodel_id="+del,
				success: function()
				{
					$(".RowHighlight").remove();	
				}
			});
		}
	})
	$(".delete").click(function()
	{
		var pdel_id=$(this).attr("alt")
		$(this).parent("td").parent('tr').addClass('RowHighlight');
		$.ajax({
			type: 'POST',
			url: "delete_category.php?pdel_id="+pdel_id,
			success: function()
			{
    			$(".RowHighlight").remove();
     		}
		});
	})
})

</script>

<div>
<div id="page-heading" ><h1>Gift Registry</h1> </div>
<div id="" style="float:right;padding-right:20px;">
              <a href="add_gift_registry.php"> <input type="button" class="buttons btn btn-info" value="Add Gift Registry"/></a> 
				</div>

</div>
<?php
$query = "SELECT * FROM gift_registry_user order by id desc";
$result = mysql_query($query) or die(mysql_error());
$countCondition = mysql_num_rows($result); 
 ?>
 <?php if(isset($_SESSION['msg']) && $_SESSION['msg']!='')  {?>                                
                                   <div id="message-success" style="padding-left: 0; padding-right: 81px; width: 70.3%;" align="center">
									<table border="0" width="100%" cellpadding="0" cellspacing="0">
													<tr>
														<td class="green-left"><?php echo $_SESSION['msg'];  $_SESSION['msg'] ='';?><a href=""></a></td>
														<td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif"   alt="" /></a></td>
													</tr>
											</table>
                               </div>          
                			<?php } ?>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
		
			<!--  start table-content  -->
			<div id="table-content">
			
				
				<!--  start message-green -->
				
				<!--  end message-green -->
				<!--  start product-table ..................................................................................... -->
				<form id="mainform" action="" class="table-responsive">

				<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="table table-bordered table-hover table-striped">
				<tr>
					<th class="table-header-check"><a id="toggle-all" ></a> </th>
					<th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">First Name</a>	</th>
                    <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Last Name</a>	</th>
					<th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Email</a></th>
                    <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Phone</a></th>
                    <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Event Type</a></th>
                    <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Delivery Address</a></th>
                    <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Event Location</a></th>
				    <th width="200" class="table-header-repeat line-left"><a a class="arrow_none">Action</a></th>
				</tr>
                <?php
				if($countCondition > 0){
				while($data = mysql_fetch_object($result)){ 
				
				$CateData = mysql_fetch_object(mysql_query("select * from gift_registry_user where 1=1"));
				?>
				<tr>
					<td width="20"><input  type="checkbox" name="checkbox" value="<?php echo $data->id;?>"/></td>
					<td><?php echo $data->first_name; ?></td>
                    <td><?php echo $data->last_name; ?></td>
					<td><?php echo $data->email; ?></td>
                    <td><?php echo $data->phone; ?></td>
                    <td><?php echo $data->event_type; ?></td>
                    <td><?php echo $data->delivery_address; ?></td>
                    <td><?php echo $data->event_location; ?></td>
				  	<td><a href="edit_gift_registry.php?edit_id=<?php echo $data->id;?>" title="Click to Edit provider"><img src="images/table/action_edit.gif"/></a></td>
				</tr>
                <?php }}else{ ?>
                <tr>
                	<td colspan="8" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>
                </tr>
                <?php } ?>
				</table>
				<!--  end product-table................................... --> 
				</form>
			</div>
			<!--  end content-table  -->
		<!--  start actions-box ............................................... -->
			
			
			<!--  start paging..................................................... -->
			
			<!--  end paging................ -->
			
			<div class="clear"></div>
		 
		</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>