<?php 
include("header.php");
/* Home page sidebar management */
/* Author @Parmeet */
/* Date : 11-03-2015 */
?>

<!-- start content-outer -->

<div id="content-outer"> 
  <!-- start content -->
  <div id="content">
    <div class="row bottom-margin">
      <div id="page-heading">
        <h1>Home Page Sidebar Images</h1>
      </div>
      <div class="add-button col-lg-5"> <a href="add-home-page-sidebar-image.php">
        <input type="button" class="buttons btn btn-info" value="Add Sidebar Images"/>
        </a> </div>
    </div>
    <?php
/** delete sidebar query **/

if(isset($_GET['id']) && $_GET['id'] != ''){

	$res = mysql_query("SELECT * FROM `home-page-sidebar` WHERE `id` = '".$_GET['id']."' ") or die(mysql_error());
	$record = mysql_fetch_object($res);
	$imagePath = '../upload_home_sidebar_images/'.$record->path;

	if (file_exists($imagePath)) {
		unlink($imagePath);
		mysql_query("DELETE FROM `home-page-sidebar` WHERE `id` = '".$_GET['id']."' ");
	}
}

$query = "SELECT * FROM `home-page-sidebar`";
//echo $query;
$result = mysql_query($query) or die(mysql_error());

$num_rows = mysql_num_rows($result);
?>
    <?php if(isset($_SESSION['msg']) && $_SESSION['msg']!='')  {?>
    <div id="message-success" style="padding-left: 0; padding-right: 81px; width: 602px;" align="center">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td class="green-left"><?php echo $_SESSION['msg'];  $_SESSION['msg'] ='';?><a href=""></a></td>
          <td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif"   alt="" /></a></td>
        </tr>
      </table>
    </div>
    <?php } ?>
    <form id="mainform" action="" class="table-responsive">
      <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="table table-bordered table-hover table-striped">
        <tr>
          <th width="70%" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Sidebar Image </a></th>
          <th class="table-header-repeat line-left"><a class="arrow_none">Action</a></th>
        </tr>
        <?php 
				if($num_rows > 0){
				while($data = mysql_fetch_object($result)){ ?>
        <tr>
          <td><img src="../upload_home_sidebar_images/<?php echo $data->path;?>" height="100" width="400" /></td>
          <td  align="center" style="padding-left:43px;" class="status"><a href="edit-home-page-sidebar-image.php?id=<?php echo $data->id;?>"class="icon-2 statusChange" title="Edit" ><img src="images/table/action_edit.gif"></a><a href="home-page-sidebar-image.php?id=<?php echo $data->id;?>"class="icon-2 statusChange" title="Delete" onclick="return confirm('Are you sure you want to delete this record?');" ><img src="images/table/action_delete.gif"></a></td>
        </tr>
        <?php }}
				else{ ?>
        <tr>
          <td colspan="6" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>
        </tr>
        <?php }?>
      </table>
      <!--  end product-table................................... -->
    </form>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php") ?>
