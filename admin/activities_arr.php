<?php

$activitiesArr = array('Role Management' => array('List' => 'listRole', 'Add'=> 'addRole', 'Edit'=> 'editRole', 'Delete'=> 'deleteRole'),

					 'Verify User' => array('Create' => 'verifyUserCreate', 'Update'=> 'verifyUserUpdate', 'Delete'=> 'verifyUserDelete'),

					 'Users' => array('List' => 'listUser', 'Add'=> 'addUser', 'Edit'=> 'editUser', 'Delete'=> 'deleteUser'),
					
					 'Audit Trail' => array('List' => 'listAuditTrail',),
					 

					 'Merchant' => array('List' => 'listMerchant', 'View' => 'viewMerchant', 'Add'=> 'addMerchant', 'Edit'=> 'editMerchant', 'Delete'=> 'deleteMerchant', 'Manage Status' => 'manageStatusMerchant', 'Banner'=> 'addBanner', 'Aboutus'=> 'aboutus', 'View Meta'=> 'viewMeta','Add and Edit Meta'=> 'addEditMeta',  'Send mail to merchant'=> 'sendMailToMerchant', 'Export list of merchant'=> 'exportMerchant'),
					 
					  'Catergory' => array('List'=> 'listCatergory', 'View'=> 'viewCatergory', 'Add'=> 'addCatergory', 'Edit'=> 'editCatergory', 'Delete'=> 'deleteCatergory', 'View Sub Category'=> 'viewSubCatergory','Add Sub Category'=> 'addSubCatergory', 'Edit Sub Category'=> 'editSubCatergory', 'Delete Sub Category'=> 'deleteSubCatergory' , 'View Size '=> 'viewCatergorySize', 'Add Size'=> 'addCatergorySize', 'Edit Size'=> 'editCatergorySize', 'Delete Size'=> 'deleteCatergorySize'),
					   
					  'Products' => array('List'=> 'listProduct', 'View'=> 'viewProduct', 'Add'=> 'addProduct', 'Edit'=> 'editProduct', 'Delete'=> 'deleteProduct', 'Manage Status'=> 'manageStatusProduct',),
					  
					 // 'Deals' => array('List'=> 'listDeal', /*'View'=> 'viewDeal',*/ 'Add'=> 'addDeal', 'Edit'=> 'editDeal'/*, 'Delete'=> 'deleteDeal'*/),
					  
					  'Customers' => array('List'=> 'listBuyer',/*'View'=> 'viewBuyer', 'Add'=> 'addBuyer',*/ 'Edit'=> 'editBuyer', 'Delete'=> 'deleteBuyer', 'Export list of Customer' => 'exportBuyer'),
					 
					  'Transactions Record' => array('View'=> 'viewTransactionsRecord'),
					  
					 // 'Export Transation' => array('Export'=> 'exportTransactionsRecord'),
					 
					  /*'Brands' => array('List'=> 'listBrand', 'Add'=> 'addBrand', 'Edit'=> 'editBrand', 'Delete'=> 'deleteBrand'),
					  
					  'Colors' => array('List'=> 'listColor', 'Add'=> 'addColor', 'Edit'=> 'editColor', 'Delete'=> 'deleteColor'),*/
					  
					  //'Color And Brands' => array('List'=> 'listColorBrand', 'Add Brand'=> 'addBrand', 'Add Color'=> 'addColor', 'Edit'=> 'editColorBrand', 'Delete'=> 'deleteColorBrand'),
					 
					  'Coupon' => array('List'=> 'listcoupon', 'Add'=> 'addCoupon', 'Delete'=> 'deleteCoupon'),

					  'Home page slider image' => array('list'=> 'listHomePageSliderImage', 'Add'=> 'addHomePageSliderImage', 'Edit'=> 'editHomePageSliderImage', 'Delete'=> 'deleteHomePageSliderImage'),
					  
					  //'Home page sidebar image' => array('list'=> 'listHomePagesidebarImage', 'Add'=> 'addHomePagesidebarImage', 'Edit'=> 'editHomePagesidebarImage', 'Delete'=> 'deleteHomePagesidebarImage'),
					   
					  'Settlement' => array('View'=> 'viewsettlement',),
					   
					  //'Sponsor' => array('List'=> 'listSponsor', 'Add'=> 'addSponsor', 'Edit'=> 'editSponsor', 'Delete'=> 'deleteSponsor'),
					  
					  'FedEx NG Shipping' => array('List'=> 'listFedExNGShipping', 'Add'=> 'addFedExNGShipping', 'Edit'=> 'editFedExNGShipping', 'Delete'=> 'deleteFedExNGShipping'),
					  
					  'Merchant FedEx NG Shipping' => array('List'=> 'listMerchantFedExNGShipping', 'Add'=> 'addMerchantFedExNGShipping',/* 'Edit'=> 'editMerchantFedExNGShipping', 'Delete'=> 'deleteMerchantFedExNGShipping'*/),
					  
					  'Merchant FedEx NG Cast' => array('List'=> 'listMerchantFedExNGCast', 'Add'=> 'addMerchantFedExNGCast'/*, 'Edit'=> 'editMerchantFedExNGCast', 'Delete'=> 'deleteMerchantFedExNGCast'*/),
					  
					  'Meta Tags' => array('List'=> 'listMetaTags', 'Add'=> 'addMetaTags', 'Edit'=> 'editMetaTags'/*, 'Delete'=> 'deleteMetaTags'*/),

					   'Page' => array('List'=> 'listPage', 'Add'=> 'addPage', 'Edit'=> 'editPage'),
					  
					  ); 
