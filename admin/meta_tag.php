<?php 
include("header.php");	
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */

?>
<script type="text/javascript">
function hilite(elem)
{
	elem.style.background = '#FFC';
}

function lowlite(elem)
{
	elem.style.background = '';
}
</script>
<!-- start content-outer -->

<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 
    <script type="text/javascript">
function changeStatus(id, status)
{
		
		$.ajax({
			type: 'POST',
			url: "delete_category.php?proid="+id+"&status="+status,
			success: function()
			{
    		window.location.reload()
     		}
		});
	}

$().ready(function()
{
	
	$('#delete').click(function()
	{
		var del=[];
		$("input[type='checkbox']:checked").each(function()
		{
			del.push($(this).val());
			$(this).parent("td").parent('tr').addClass('RowHighlight');
		}); 
		if(del=="")
	 {
		 alert("Please select at least one item.")
	 }
	 else
		if(confirm("Are you sure want to delete the selected item(s)?"))
		{
			$.ajax({
				type:'post',
				url: "delete_category.php?Mprodel_id="+del,
				success: function()
				{
					$(".RowHighlight").remove();	
				}
			});
		}
	})
	$(".delete").click(function()
	{
		if(confirm("Are you sure want to delete this item?"))
    {
      var meta_id=$(this).attr("alt")
  		$(this).parent("td").parent('tr').addClass('RowHighlight');
  		$.ajax({
  			type: 'POST',
  			url: "delete_category.php?meta_id="+meta_id,
  			success: function()
  			{
      			$(".RowHighlight").remove();
       		}
  		});
    }
	})
})

</script>
    <div class="row bottom-margin">
      <div id="page-heading" >
        <h1>Meta Tags</h1>
      </div>
      <div class="add-button col-lg-5"> <a href="add_meta_tag.php">
        <input type="button" class="buttons btn btn-info" value="Add Meta Tag"/>
        </a> </div>
    </div>
    <?php
$query = "SELECT * FROM meta_tag";
$result = mysql_query($query) or die(mysql_error());
$countCondition = mysql_num_rows($result); 
 ?>
    <?php if(isset($_SESSION['msg']) && $_SESSION['msg']!='')  {?>
    <div id="message-success" style="padding-left: 0; padding-right: 81px; width: 70.3%;" align="center">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td class="green-left"><?php echo $_SESSION['msg'];  $_SESSION['msg'] ='';?><a href=""></a></td>
          <td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif"   alt="" /></a></td>
        </tr>
      </table>
    </div>
    <?php } ?>
    <form id="mainform" action="" class="table-responsive">
      <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="table table-bordered table-hover table-striped">
        <tr>
          <th class="table-header-check"><a id="toggle-all" ></a> </th>
          <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Title</a> </th>
          <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Meta Keywords</a> </th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Meta Description </a></th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Meta Page </a></th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Category</a></th>
          <th width="200" class="table-header-repeat line-left"><a a class="arrow_none">Action</a></th>
        </tr>
        <?php
		if($countCondition > 0){
			while($data = mysql_fetch_object($result)){ 
				
				$CateData = mysql_fetch_object(mysql_query("select * from category where id = '".$data->category."'"));
				?>
        <tr>
          <td width="20"><input  type="checkbox" name="checkbox" value="<?php echo $data->id;?>"/></td>
          <td><?php echo $data->title; ?></td>
          <td><?php echo $data->keyword; ?></td>
          <td><?php echo $data->description ?></td>
          <td><?php echo $data->page ?></td>
          <td><?php if($CateData->category_name==''){ echo "Index Page";}else{echo $CateData->category_name;} ?></td>
          <td><a href="edit_meta_tag.php?edit_id=<?php echo $data->id;?>" title="Click to Edit provider"><img src="images/table/action_edit.gif"/></a><a title="Click to Delete" alt="<?php echo $data->id ?>" class="delete" href="javascript:;"> <img src="images/table/action_delete.gif"></a></td>
        </tr>
        <?php }}else{ ?>
        <tr>
          <td colspan="4" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>
        </tr>
        <?php } ?>
      </table>
      <!--  end product-table................................... -->
    </form>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>