<?php
include("header.php");
include("activities_arr.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */

if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{    
	 $role 		= '';  
     if(isset($_REQUEST['permission']) && $_REQUEST['permission'] != '')
	 {
	 	$role = implode('|',$_REQUEST['permission']);
		
	 }
	 
	 $User									= array();
	 $User['title']							= $_REQUEST['title'];
	 $User['description']					= $_REQUEST['description'];
	 $User['role']							= $role;
	 $main									= new users;
	 
	 $insert= $main->ADD($User,"role");
	 if($insert)
	 {
		header("location:role.php");
	 }
}
 ?>
<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content" style="">
    <div >
      <div id="page-heading" >
        <h1>Add Role</h1>
      </div>
      <div style="float:right; padding-right:20px;">
        <input type="button" class="buttons btn btn-info" value="Back" onclick="window.history.back()" />
      </div>
    </div>
    <div class="row">
      <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" id="UserForm">
        <div class="col-lg-5">
          <div class="form-group">
            <label>Title: </label>
            <input type="text" class="form-control" id="title" name="title" value="" />
          </div>
          <div class="form-group">
            <label>Description: </label>
            <textarea name="description" id="description" class="form-control"></textarea>
          </div>
          <div class="form-group">
            <input type="submit" id="RoleSubmit" value="submit" name="submit" class="form-submit btn btn-default" />
            <input type="reset" value="Reset" class="form-reset btn btn-default"  />
          </div>
        </div>
        <div class="col-lg-6">
          <h2>Permission</h2>
          <div class="table-responsive" style="overflow-y: auto; height: 352px;">
            <table class="table table-bordered table-hover">
              <tbody>
                <?php  
			  foreach($activitiesArr as $key => $value){
			   
			   foreach($activitiesArr[$key] as $key1 => $value1){
			   ?>
                <tr>
                  <td><input type="checkbox" name="permission[]" value="<?php echo $value1; ?>" /></td>
                  <td><?php echo $key1; ?></td>
                  <td><?php echo $key; ?></td>
                </tr>
                <?php
			   }
			  }
			  ?>
              </tbody>
            </table>
          </div>
        </div>
      </form>
      <!-- end id-form  --> 
    </div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>