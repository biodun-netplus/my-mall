<?php 
include("header.php"); 
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */
 

if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{ 
	$Product				= array();
	$date					= date("Y-m-d H:i:s");
	$main					= new users;
	$destination 			        = '../product_images/';
	
	if($_FILES['image']['tmp_name']!='')
	{
			/// unlink already image
			$alreadyimage = $main->viewprofile($_REQUEST['id'],'products');
			if($alreadyimage->image!='' && file_exists($destination.$alreadyimage->image) && $_FILES['image']['name'] == $alreadyimage->image)
			{
				 unlink($destination.$alreadyimage->image);
			}
			
			if(isset($_POST['old_image']) && $_POST['old_image'] != '')
			{
				//if(file_exists($destination.$_POST['old_image']))
				//unlink($destination.$_POST['old_image']);
				
			}
			
			/// uploade process			
			$file = $_FILES['image'];
			$upload = new Upload; 
			$result = $upload->uploadImage($file, $destination, null, array('type' => 'resizecrop', 'size' => array('400', '400'), 'output' => 'jpg')); 
			$Product['image']=$result;
	}
	
	if($_FILES['image2']['tmp_name']!='')
	{
		/// unlink old image
		if(isset($_POST['old_image2']) && $_POST['old_image2'] != '')
		{
			//if(file_exists($destination.$_POST['old_image2']))
			//unlink($destination.$_POST['old_image2']);
			
		}
		
		/// uploade process			
		$file = $_FILES['image2'];
		$upload = new Upload; 
		$result = $upload->uploadImage($file, $destination, null, array('type' => 'resizecrop', 'size' => array('400', '400'), 'output' => 'jpg')); 
		$Product['image2']=$result;
	}
	
	if($_FILES['image3']['tmp_name']!='')
	{
		/// unlink old image
		if(isset($_POST['old_image3']) && $_POST['old_image3'] != '')
		{
			//if(file_exists($destination.$_POST['old_image3']))
			//unlink($destination.$_POST['old_image3']);
			
		}
		
		/// uploade process			
		$file 						= $_FILES['image3'];
		$upload 					= new Upload; 
		$result 					= $upload->uploadImage($file, $destination, null, array('type' => 'resizecrop', 'size' => array('400', '400'), 'output' => 'jpg')); 
		$Product['image3']	=$result;
	}
		
	$Product['product_name'] 		= $_REQUEST['name'];	
	$Product['category'] 			= $_REQUEST['category'];
	$Product['sub_category']		= $_REQUEST['provider_subcategory'];
	$Product['sub_sub_category']		= $_REQUEST['provider_sub_subcategory'];
	$Product['product_price'] 		= $_REQUEST['price'];
	$Product['product_quantity'] = $_REQUEST['quantity'];
	$Product['product_detail'] 	= $_REQUEST['detail'];
    $Product['pid']=$_POST['pid'];
	$Product['currency'] 			= $_REQUEST['currency'];
	if(isset($_REQUEST['isactiveProduct']) && $_REQUEST['isactiveProduct'] == 'd')
	{
		$Product['isactive'] 			= 'd';
		$Product['declined_desc'] = $_REQUEST['declined_desc'];
	}
	else if(isset($_REQUEST['isactiveProduct']) &&  $_REQUEST['isactiveProduct'] == 't')
	{
		$Product['isactive'] =  't';
	}
	 
	$id=$_REQUEST['id'];
	
	$xx= $main->update($Product,'products',$id);
	if($xx)
	{
		$_SESSION['msg']='Product detail updateded successfully.';
	header("Location:product.php");
	}
}
 ?>
<?php 

if(isset($_REQUEST['edit_id']))
{
	$edit= new users;
	$edit_data= $edit->viewprofile($_REQUEST['edit_id'],"products"); //print_r($edit_data);
}
?>
<script type="text/javascript">
function goBack()
  {
  window.history.back()
  }
 $(document).ready(function () { 
	$("input:radio[name=isactiveProduct]").click(function() {
    	if($(this).val() == 'd'){
			$('#declined_descShow').show();
			//$('#declined_desc').val('');
		}
		else{
			$('#declined_descShow').hide();
			$('#declined_desc').val('');
		}
	});	
});	
</script>
 <div class="clear"></div>
 
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content" style="">

<div>
<div id="page-heading"><h1>Edit Product</h1></div>
<div style="float:right; padding-right:20px;">Search &nbsp;: &nbsp;&nbsp;<input type="button" value="Back" class="buttons" onclick="goBack()" /></div>
</div>


<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"></th>
</tr>
<tr> 
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
				
				<!--  start message-red -->
				
				<!--  end message-green -->
		
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top"> 
	<td align="left">
	
		<!--  start step-holder -->
		<!--  end step-holder -->
        <!-- start id-form -->
        <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" enctype="multipart/form-data" id="edit-product">
		<table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
		<tr>
			<th valign="top">Product name:</th>
			<td><input type="text" class="form-control" name="name" value="<?php echo $edit_data->product_name;?>" /></td>
			</tr>
            <input type="hidden" value="<?php echo $edit_data->id;?>" name="id"/>
			
            <tr>
			<th valign="top">Category:</th>
            <td>
			<select id="provider_categorya" class="form-control" name="category" onchange="javascript:getSub(this.value,'provider_categorya','provider_subcategory')">
				<option value="">Category</option>
                <?php
//                var_dump($edit_data);
				$main= new users;
				$xx= $main->viewCat();
//                var_dump($xx);
				foreach($xx as $data){
//                    var_dump($xx);?>
				<option <?php if($edit_data->category==$data->id){ ?> selected="selected" <?php } ?> value="<?php echo $data->id ?>"><?php echo $data->category_name ?></option> <?php }?>
			</select>
            </td>
           
			</tr>
			<tr>
				<th valign="top">Sub Category:</th>
				<td>
					<select  class="form-control" id="provider_subcategory" name="provider_subcategory" onchange="javascript:getSub(this.value,'provider_subcategory','provider_sub_subcategory')">
					<option value="">Sub Category</option>
                        <?php
//                                        echo $edit_data->category;
                        if($edit_data->category != 0)
                        {
                            $xx= $main->view_isactive_cat('category',$edit_data->category,'t');
                            foreach($xx as $data){ ?>
                                <option  <?php if($edit_data->sub_category==$data->id){ ?> selected="selected" <?php } ?> value="<?php echo $data->id ?>" ><?php echo $data->category_name; ?></option>

                                <?php }
                        }
                        ?>
					</select>

			</tr>
			<tr>
				<th valign="top">Sub Sub Category:</th>
				<td>
					<select  class="form-control" id="provider_sub_subcategory" name="provider_sub_subcategory">
					<option value="">Sub Sub Category</option>
                        <?php
//                                        echo $edit_data->category;
                        if($edit_data->sub_category != 0)
                        {
                            $xx= $main->view_isactive_cat('category',$edit_data->sub_category,'t');
                            foreach($xx as $data){ ?>
                                <option  <?php if($edit_data->sub_sub_category==$data->id){ ?> selected="selected" <?php } ?> value="<?php echo $data->id ?>" ><?php echo $data->category_name; ?></option>

                                <?php }
                        }
                        ?>
					</select>
				</td><td><label class="error"  id="subcategory"></label></td>
			</tr>
        <tr>
            <th valign="top">Provider:</th>
            <td>
                <select  class="form-control" id="provider_name" name="pid">
                    <option value="">provider_name</option>
                    <?php

                    $sel=mysql_query("select * from users where user_type='p' and isactive='t'");
                    while($data1=mysql_fetch_assoc($sel))

                    { ?>
                        <option <?php if($edit_data->pid==$data1['id']){ ?> selected="selected" <?php } ?> value="<?php echo $data1['id']; ?>"><?php echo $data1['first_name']; ?></option> <?php }?>
                </select>
            </td><td><label class="error"  id="categorya"></label></td>

        </tr>
			<tr>
			<th valign="top">Product price:</th>
			<td><input type="text" class="form-control" name="price" value="<?php echo $edit_data->product_price;?>" /></td>
			</tr>
            <tr>
                <th valign="top">Currency:</th>
                <td>
                    <select  class="form-control" id="currency" name="currency">
                        <option <?php if($edit_data->currency=='Naira'){ ?> selected="selected" <?php } ?> value="Naira">Naira</option>
                        <option <?php if($edit_data->currency=='Doller'){ ?> selected="selected" <?php } ?> value="Doller">Us Dollar</option>
                        <option <?php if($edit_data->currency=='Euro'){ ?> selected="selected" <?php } ?> value="Euro">Euro</option>
                        <option <?php if($edit_data->currency=='Pounds'){ ?> selected="selected" <?php } ?> value="Pounds">Pounds</option>
                    </select>
                </td><td><label class="error"  id="categorya"></label></td>

            </tr>
			<tr>
				<th valign="top">Product Quantity:</th>
				<td>
				<div class="table-responsive">
				<?php
            $proAttrDetailSql  = mysql_query("SELECT relation_color_size.*
            FROM `relation_color_size` 
            INNER JOIN products ON (products.id =  relation_color_size.product_id) 
            where `relation_color_size`.`product_id` = '".$edit_data->id."' ");  

            if(mysql_num_rows($proAttrDetailSql) > 0)
            { 
            ?>
              <table cellspacing="0" cellpadding="0" border="0" width="100%" class="table table-bordered table-hover table-striped table-cell">
                <tr bgcolor="#F3F3F3">
                  <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Color</a></th>
                  <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Size</a></th>
                  <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Qty</a></th>
                </tr>
                <?php
                 while($value = mysql_fetch_object($proAttrDetailSql)) {
                  
                  ?>
                  <tr>
                    <td><?php $res = mysql_query("SELECT product_color.*
                    FROM `product_color` INNER JOIN products ON (products.id = product_color.pid) 
                    where product_color.id = '".$value->colour_id."' ");  
                    if(mysql_num_rows($res) > 0)
                    {
                      $recored = mysql_fetch_object($res);
                      echo $recored->color;
                    }  
                    ?></td>
                    <td>
                     <?php 
                     $res1 = mysql_query("SELECT product_size.*
                    FROM `product_size` INNER JOIN products ON (products.id = product_size.pid) 
                    where product_size.id = '".$value->size_id ."' ");  
                    if(mysql_num_rows($res1) > 0)
                    {
                      $recored1 = mysql_fetch_object($res1);
                      echo $recored1->size;
                    }
                    ?></td>
                    <td><?php echo $value->qty; ?></td>
                  </tr>
                  <?php
                  }
                  ?>
                </table>
                <?php
                }
                else
                {
                  echo '<div style="color:#F00; font-size:14px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found!</div>';
                }
                ?>
                <?php /*
				<input type="text" class="form-control" name="quantity" value="<?php echo $edit_data->product_quantity;?>" />*/?>
				</div>
				</td>
			</tr>
			
			<tr>
			<th valign="top">Product detail:</th>
			
			<td><textarea rows="2" cols="20" id="product_detail" class="form-control form-textarea" name="detail" ><?php echo $edit_data->product_detail;?></textarea></td>
			
			</tr>
            <tr>
			<th valign="top">Image1: </th>
			<td>
			<?php if($edit_data->image != ''){ ?> 	
			<img src="../product_images/<?php echo $edit_data->image;?>" style="width: 400px;height: 400px"/><br />
			<?php } ?>
            <input type="hidden" id="old_image" name="old_image" value="<?php echo $edit_data->image;?>" />
            <input type="file" id="image" name="image" /></td>
			</tr>
            <tr>
			<th valign="top">Image2: </th>
			<td>
			<?php if($edit_data->image2 != ''){ ?> 	
			<img src="../product_images/<?php echo $edit_data->image2;?>" /><br />
			<?php } ?>
            <input type="hidden" id="old_image2" name="old_image2" value="<?php echo $edit_data->image2;?>" />
            <input type="file" id="image2" name="image2" /></td>
			</tr>
            <tr>
			<th valign="top">Image3: </th>
			<td>
			<?php if($edit_data->image3 != ''){ ?> 	
			<img src="../product_images/<?php echo $edit_data->image3;?>" /><br />
			<?php } ?>
            <input type="hidden" id="old_image3" name="old_image3" value="<?php echo $edit_data->image3;?>" />
            <input type="file" id="image3" name="image3" /></td>
			</tr>
            <?php   
			if($edit_data->isactive == 'p'){ ?>
            <tr>
			<th valign="top">Status: </th>
			<td>
            <div> 
              <span>
                 <input type="radio" name="isactiveProduct" value="t" />  Activate</span> 
             <span>
               <input type="radio" name="isactiveProduct" value="d" />  Decline</span> 
              </div>
              <p id="declined_descShow" style="display:none;">
              <textarea rows="2" cols="20" id="declined_desc" class="form-control form-textarea" name="declined_desc" ></textarea>
            </p>
            </td>
			</tr>
            <?php } ?>
	<tr>
		<th>&nbsp;</th>
		<td valign="top">
        
			<input type="submit" value="submit" name="submit" class="form-submit btn btn-default" />
					</td>
		</tr>
	</table>
   		 </form>
	<!-- end id-form  -->	</td>
	
</tr>
<tr>
<td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
	
	<div class="clear"></div>
 

</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
<link rel="stylesheet" type="text/css"  href="../assets/css/editor.css" />
<script type="text/javascript" src="../assets/js/editor.js"></script>
<script type='text/javascript'> $('#product_detail').Editor(); $('form#edit-product').submit(function(){ $('#product_detail').val($('.Editor-editor').html()); }); $('.Editor-editor').html($('#product_detail').val());
/*=== get subcategory from category ============================================*/
<!--	$("body").on("click","div#provider_categorya_container ul li",function (){ -->
<!--		$('.loader').show();-->
<!--		$.ajax({-->
<!--			 url: "delete_category.php",-->
<!--			 data: {'catId':$("#provider_categorya").val(),  'getSubCat': 'getSubCat' },-->
<!--			 success: function(Data){-->
<!--				 $('.loader').hide();-->
<!--				 $('#provider_subcategory').find('option').remove().end().append(Data);-->
<!---->
<!---->
<!--			}-->
<!--		});-->
<!--		return false;-->
<!---->
//<!--	})
function getSub(id,sending_div,receiving_div)
{

    $.ajax({
        type: 'POST',
        url: 'delete_category.php',
        data: {'catId':$("#"+sending_div).val(),  'getSubCat': 'getSubCat' },
        beforeSend:function()
        {
            $('#'+receiving_div).html("<div class='col-md-9' ><img src='images/small_loader.gif'></div>");
        },
        success: function (data) {
            $('#'+receiving_div).html(data);
        }
    });
}
</script>
<?php include("footer.php")?>

