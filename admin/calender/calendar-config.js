﻿$().ready(function() {
    $('.Gen_date,.Exp_date').datepicker({
        numberOfMonths: 1,
        showButtonPanel: true,
        beforeShow: customRange,
        dateFormat: 'mm-dd-yy',
		duration: '',
		showTime: false,
		constrainInput: false,
        showOn: 'both',
        buttonImage: 'images/calendar_icon.png',
        buttonImageOnly: true,
        buttonText: 'Show Date Picker'
    });
	
	$('.datefrom,.dateto123').datepicker({

        numberOfMonths: 1,

        showButtonPanel: true,

       // beforeShow: customRangeNew,

        dateFormat: 'yy-mm-dd',

		duration: '',

		showTime: false,

		constrainInput: false,

        showOn: 'both',

        buttonImage: 'images/calendar_icon.png',

        buttonImageOnly: true,

        buttonText: 'Show Date Picker'

    });

	
});
function customRange(a) {
    var b = new Date();
    var c = new Date(b.getFullYear(), b.getMonth(), b.getDate());
    if (a.id == 'DropoffDate') {
        if ($('.Gen_date').datepicker('getDate') != null) {
            c = $('.Gen_date').datepicker('getDate');
        }
    }
    return {
        minDate: c
    }
}