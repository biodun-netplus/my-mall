<?php 
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 8-4-2015 */
?>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 
 
<?php
// delete user....
$msg = '';
if(isset($_GET['id']) && $_GET['id'] != ''){

  /*$User['isactive']    = 't';
  $main                = new users;
  $update= $main->update($User,"admin",base64_decode($_GET['id']));
  if($update)
  $msg = "User has deleted successfully.";*/
  mysql_query("Delete FROM admin WHERE id = '".base64_decode($_GET['id'])."'");
  $msg = "User has deleted successfully.";

}
// end of delete

$query = "SELECT COUNT(*) FROM admin WHERE isactive IN('p','d','pd') ";
$result = mysql_query($query) or die(mysql_error());
$num_rows = mysql_fetch_row($result);

$pages = new Paginator;
$pages->items_total = $num_rows[0];
$pages->mid_range = 4; // Number of pages to display. Must be odd and > 3
$pages->paginate();
$query = "SELECT a.*, b.username AS created_username from admin a LEFT JOIN admin b ON b.id = a.created_by WHERE a.`isactive` IN('p','d','pd') order by id desc $pages->limit";
$result = mysql_query($query) or die(mysql_error());
$countCondition = mysql_num_rows($result);
 ?>
    <div class="row bottom-margin">
      <div id="page-heading" >
        <h1>User</h1>
      </div>
      <div id="paging-table" class="col-lg-6"><?php echo $pages->display_pages(); ?></div>
    </div>
    <?php if(isset($msg) && $msg!='')  {?>
      <div class="alert alert-success">
        <?php echo $msg;?>
      </div>
    <?php } ?>
    <form id="mainform" action="" class="table-responsive">
      <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="table table-bordered table-hover table-striped">
        <tr>
          <th class="table-header-check"><a id="toggle-all" ></a> </th>
          <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Username</a></th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Email </a></th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Create By </a></th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Role </a></th>
          <th width="225" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Action</a></th>
        </tr>
        <?php
    if($countCondition > 0){
      while($data = mysql_fetch_object($result)){ 
       
       // getting role name
       $qryRole = mysql_query("SELECT * FROM `role` WHERE id='".$data->role."'"); 
       $roleResult = mysql_fetch_object($qryRole);  
    ?>
        <tr>
          <td width="20"><input  type="checkbox" name="checkbox" value="<?php echo $data->id;?>"/></td>
          <td><?php echo $data->username; ?></td>
          <td><?php echo $data->email_id; ?></td>
          <td><?php echo $data->created_username; ?></td>
          <td><?php echo $roleResult->title; ?></td>
          <td>
          <?php if($_SESSION['id']==$data->created_by){
               echo "No Permission";
          }
           else{ ?>
                
                 <select alt="<?php echo $data->id;?>" onchange="javascript:changeStatus('<?php echo $data->id;?>',this.value);">
            <option value="" selected="selected">Select</option>
            
            <?php if($data->isactive=='p'){ ?>
            <option value="t">Approve Create</option>
            <?php }  ?>
            <?php if($data->isactive=='d'){ ?>
            <option value="d">Decline Create</option>
            <?php } ?> 
            <?php if($data->isactive=='pd'){ ?>
            <option value="d">Approve Delete</option>
            <option value="t">Decline Delete</option>
            <?php }   
            else { ?>
            <option value="d">Decline Create</option>
            <?php } ?>           

            </select>

           <?php } ?>
            
            <!--<a style="text-align:left; margin-left: 10px;" href="verify_users.php?id=<?php //echo base64_encode($data->id);?>" onclick="return confirm('Are you sure, you want to delete this user?')"  alt="<?php echo $data->id;?>" title="Click to Delete user"> <img src="images/table/action_delete.gif"/></a>-->
          </td>
        </tr>
        <?php }}else{ ?>
        <tr>
          <td colspan="10" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>
        </tr>
        <?php } ?>
      </table>
      <!--  end product-table................................... -->
    </form>
    <div id="actions-box"> <a href="" class="action-slider"></a>
      <div id="actions-box-slider"> <a href="javascript:;" name="delete" id="delete" class="action-delete" >Delete</a> </div>
      <div class="clear"></div>
    </div>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<script type="text/javascript">
function changeStatus(id, status)
{
    $.ajax({
      type: 'POST',
      url: "delete_category.php?sUserId="+id+"&status="+status,
      success: function()
      {
         window.location.reload()
        }
    });
  }
</script>
<?php include("footer.php")?>