<?php 
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */
?>

<!-- start content-outer -->

<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 
    <script type="text/javascript">
function changeStatus(id, status)
{
		$.ajax({
			type: 'POST',
			url: "delete_category.php?prdid="+id+"&status="+status,
			success: function()
			{
    		window.location.reload()
     		}
		});
	}

$().ready(function(){
	$('#delete').click(function(){
		//alert("akhil");
		var del=[];
		
		$("input[type='checkbox']:checked").each(function(){
         del.push($(this).val());
		 $(this).parent("td").parent('tr').addClass('RowHighlight');
       }); 
	 //alert(del);
	 if(del=="")
	 {
		 alert("Please select at least one item.")
	 }
	 else
	   if(confirm("Are you sure want to delete the selected item(s)?")){
  		 $.ajax({
    		type:'post',
   		 	url: "delete_category.php?Mproductdel_id="+del,
     		success: function()
   			{
    		$(".RowHighlight").remove();	
     		}
 		 });
	   }
		
		
		
		})
	
	$(".delete").click(function(){
		var product_del_id=$(this).attr("alt")
		//alert(product_del_id);
		$(this).parent("td").parent('tr').addClass('RowHighlight');
			$.ajax({
			type: 'POST',
			url: "delete_category.php?product_del_id="+product_del_id,
			success: function()
			{
    			$(".RowHighlight").remove();

     		}
		});
	})
})

</script>
    <?php
//$query = "SELECT c.category_name,u.* FROM users AS u INNER JOIN category AS c ON u.category=c.id where user_type='p'";

$query = "SELECT COUNT(*) FROM `products`  ";
$result = mysql_query($query) or die(mysql_error());
$num_rows = mysql_fetch_row($result);

$pages = new Paginator;
$pages->items_total = $num_rows[0];
$pages->mid_range = 4; // Number of pages to display. Must be odd and > 3
//$pages->default_ipp = '10'; 
$pages->paginate();

//echo $pages->display_pages();
//echo "<span class=\"\">".$pages->display_jump_menu().$pages->display_items_per_page()."</span>";

$query = "SELECT products.*,category.category_name,users.first_name FROM `products` left join category on products.category=category.id  left join users on  products.pid=users.id order by products.id desc $pages->limit";
//echo $query;
$result = mysql_query($query) or die(mysql_error());
$countCondition = mysql_num_rows($result);
 ?>
    <div class="row bottom-margin">
      <div id="page-heading">
        <h1>Products</h1>
      </div>
      <div>
        <div class="add-button col-lg-5"> <a href="add_product.php">
          <input type="button" class="buttons btn btn-info" value="Add New Product"/>
          </a> </div>
        <div id="paging-table" class="col-lg-6"><?php echo $pages->display_pages(); ?></div>
      </div>
    </div>
    <?php if(isset($_SESSION['msg']) && $_SESSION['msg']!='')  {?>
    <div id="message-success" >
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td class="green-left"><?php echo $_SESSION['msg'];  $_SESSION['msg'] ='';?><a href=""></a></td>
          <td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif"   alt="" /></a></td>
        </tr>
      </table>
    </div>
    <?php } ?>
    <form id="mainform" action="" class="table-responsive">
      <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="table table-bordered table-hover table-striped">
        <tr>
          <th class="table-header-check"><a id="toggle-all" ></a> </th>
          <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Product name</a></th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Category </a></th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Provider </a></th>
          <th  class="table-header-repeat line-left "><a class="arrow_none"> Price </a></th>
          <th  class="table-header-repeat line-left "><a class="arrow_none">Quantity </a></th>
          <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Short Description </a></th>
          <th  class="table-header-repeat line-left "><a class="arrow_none">Viewed</a></th>
          <th  class="table-header-repeat line-left minwidth-1" style="min-width:145px;"><a class="arrow_none">Image</a> </th>
          <th  class="table-header-repeat line-left "><a class="arrow_none">Status</a> </th>
          <th width="125" class="table-header-repeat line-left"><a class="arrow_none">Action</a></th>
        </tr>
        <?php
			   if($countCondition > 0){
				while($data = mysql_fetch_object($result)){ 
				
					//$query1 = mysql_query("select * from category where id = '".$data->category."'");
					//$cat = mysql_fetch_object($query1);
					//$query12 = mysql_query("select * from users where id = '".$data->pid."'");
					//$pro= mysql_fetch_object($query12);
				?>
        <tr>
          <td width="20"><input  type="checkbox" name="checkbox" value="<?php echo $data->id;?>"/></td>
          <td><?php echo ucfirst($data->product_name); ?></td>
          <td><?php  echo $data->category_name;  ?></td>
          <td ><?php 
					if($data->pid==0)
						echo "Admin";
					else	
					echo $data->first_name;
					
					
					?></td>
          <td >$<?php echo number_format($data->product_price, 2, '.', ''); ?></td>
          <td ><?php echo $data->product_quantity?></td>
          <td style="text-align:justify"><?php echo ucfirst(substr($data->product_detail,0,100));?></td>
          <td style="text-align:center;"><?php echo $data->viewed;?></td>
          <td><img src="../product_images/<?php echo $data->image;?>" height="100" width="100" /></td>
          <td  align="center" style="padding-left:20px;" class="status"><?php if($data->isactive=='t'){ ?>
            <a href="javascript:changeStatus('<?php echo $data->id;?>','f');" class="icon-5 statusChange" id="t" alt="<?php echo $data->id;?>" title="Click to De-Activate" ></a>
            <?php } else {?>
            <a href="javascript:changeStatus('<?php echo $data->id;?>','t');" class="icon-2 statusChange" id="f" title="Click to Activate"  ></a>
            <?php } ?></td>
          <td><a href="view_product.php?view_id=<?php echo $data->id;?>" title="Click to view Product"><img src="images/table/Gnome-Logviewer-32.png" width="24" height="24" alt="" /> </a><a href="javascript:;" class="delete" alt="<?php echo $data->id;?>" title="Click to Delete Product"> <img src="images/table/action_delete.gif"/></a><a href="edit_product.php?edit_id=<?php echo $data->id;?>" title="Click to Edit Product"> <img src="images/table/action_edit.gif"/></a></td>
        </tr>
        <?php }}else{ ?>
        <tr>
          <td colspan="10" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>
        </tr>
        <?php } ?>
      </table>
      <!--  end product-table................................... -->
    </form>
    <div id="actions-box"> <a href="" class="action-slider"></a>
      <div id="actions-box-slider"> <a href="javascript:;" name="delete" id="delete" class="action-delete" >Delete</a> </div>
      <div class="clear"></div>
    </div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>