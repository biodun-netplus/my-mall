<?php 
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */
?>
<script type="text/javascript">
function goBack()
  {
  window.history.back()
  }
</script>
<style type="text/css">
#id-form td, #id-form th {
    display: inline-block;
}
#content-table-inner table#id-form {
    width: 100%;
}
</style>
  
 <div class="clear"></div>
 
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content" style="">

<div>
<div id="page-heading" style="float:none"><h1>Products Details</h1></div>
<div style="float:right; padding-right:20px;"><input type="button" value="Back" class="buttons" onclick="goBack()" /></div>
</div>


<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"></th>
    
</tr>
<tr>
	<td id="tbl-border-left"> </td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
		
			<!--  start table-content  -->
			<div id="table-content" align="left">
            <?php 
			$main= new users;
			$xx= $main->viewprofile($_REQUEST['view_id'],"products");
			?>
			<table width="100%" border="0" cellpadding="0" cellspacing="0"  id="id-form">
			<tr>
			<th width="150" align="left" valign="top">Product Name:</th>
			<td align="left" valign="top"><?php echo $xx->product_name;?></td>
			</tr>
            <tr>
			<th width="150" align="left" valign="top">Category:</th>
			<td align="left" valign="top"><?php echo $xx->category;?></td>
			</tr>
			
            <tr>
			<th width="150" align="left" valign="top">Product Price:</th>
			<td align="left" valign="top"><?php echo $xx->product_price;?></td>
			</tr>
            <tr>
			<th width="150" align="left" valign="top">Product Quantity:</th>
			<td align="left" valign="top"><?php echo $xx->product_quantity;?></td>
			</tr>
            <tr>
			<th width="150" align="left" valign="top">Product Detail:</th>
			<td align="left" valign="top"><?php echo $xx->product_detail;?></td>
			</tr>
            	<tr>
			<th width="150" align="left" valign="top">Product Image:</th>
			<td align="left" valign="top"><img src="../product_images/<?php echo $xx->image;?>"  /></td>
			</tr>	
			</table>
		
			</div>
			
<div class="clear"></div>
		 
		</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
    

<?php include("footer.php")?>