<?php
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */


if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{      
    $destination = '../product_images/';
	$file = basename($_FILES['images']);
	$upload = new Upload; 
	$result = $upload->uploadImage($file, $destination); 
     $date=date("Y-m-d H:i:s");
	 $User=array();
	
	 $User['deal_name']=$_REQUEST['deal_name'];	
	 $User['images']=$result;
	  $User['offer']=$_REQUEST['offer'];
	   $User['content']=$_REQUEST['content'];
	    $User['url']=$_REQUEST['url'];
		
	 $main= new users;
	 
	 $insert= $main->ADD($User,"deal");
	 if($insert)
	 {
		header("location:deal.php");
	 }
}
 ?>


<div class="clear"></div>

<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content" style="">
    <div >
      <div id="page-heading" >
        <h1>Add deals</h1>
      </div>
      <div style="float:right; padding-right:20px;">
        <input type="button" class="buttons btn btn-info" value="Back" onclick="goBack()" />
      </div>
    </div>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
      <tr>
        <th rowspan="3" class="sized"></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><!--  start content-table-inner -->
          
          <div id="content-table-inner"> 
            
            <!--  start message-red --> 
            
            <!--  end message-red -->
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
              <tr valign="top">
                <td align="center"><!--  start step-holder --> 
                  <!--  end step-holder --> 
                  <!-- start id-form -->
                  
                  <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" id="ProviderForm" enctype="multipart/form-data">
                    <table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
                      <tr>
                        <th valign="top">Deal Name:</th>
                        
                        <td> <input type="text" class="form-control" name="deal_name" value="" required="required" />
                        </td>
                      </tr>
                      <tr>
                        <th valign="top">Upload Image:</th>
                        <td><input type="file" name="images"  id="image" value="" required="required" ></td>
                        <td><label class="error"  id="imagea"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Offer</th>
                        <td><input type="text" class="form-control" id="provider_pass" name="offer" value="" required="required" /></td>
                        <td><label class="error"  id="password"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Content:</th>
                        <td><textarea name="content" id="message" class="form-control form-textarea" cols="" rows=""></textarea></td>
                        <td><label class="error"  id="password"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Url:</th>
                        <td><input type="text" class="form-control" id="provider_pass" name="url" value=""  required="required" /></td>
                        <td><label class="error"  id="password"></label></td>
                      </tr>
                      
                      <tr>
                        <th>&nbsp;</th>
                        <td valign="top"><input type="submit" id="ProviderSubmit" value="submit" name="submit" class="form-submit btn btn-default" />
                          <input type="reset" value="Reset" class="form-reset btn btn-default"  /></td>
                      </tr>
                    </table>
                  </form>
                  
                  <!-- end id-form  --></td>
              </tr>
              <tr>
                <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                <td></td>
              </tr>
            </table>
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>