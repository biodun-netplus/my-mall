<?php 
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */
?>

<!-- start content-outer -->

<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 
    <script type="text/javascript">
function changeStatus(id, status)
{
	$.ajax({
		type: 'POST',
		url: "delete_category.php?Scatid="+id+"&Sstatus="+status,
		success: function()
		{
			window.location.reload()	
		}
	});
}
$().ready(function(){
$('#delete').click(function(){
	var del=[];
	$("input[type='checkbox']:checked").each(function(){
	del.push($(this).val());
	$(this).parent("td").parent('tr').addClass('RowHighlight');
}); 
if(del=='')
{
	alert("Please select at least one item.");
}
else
	if(confirm("Are you sure want to delete the selected items")){
  		 $.ajax({
    		type:'post',
   		 	url: "delete_category.php?DelBrand="+del,
     		success: function()
   			{
    		$(".RowHighlight").remove();	
     		}
 		 });
	  }
})
$(".delete").click(function(){
	var id=$(this).attr("alt")
	$(this).parent("td").parent('tr').addClass('RowHighlight');
	if(confirm("Are you sure want to delete this items")){
		$.ajax({
			type: 'POST',
			url: "delete_category.php?DelColor="+id,
			success: function()
			{
    			$(".RowHighlight").remove();
     		}
		});
	}
	else
	{
		return false;
	}
	})
})
function hilite(elem)
{
	elem.style.background = '#FFC';
}

function lowlite(elem)
{
	elem.style.background = '';
}

</script>
    <?php
$query = "SELECT COUNT(*) FROM color_or_brands";
$result = mysql_query($query) or die(mysql_error());
$num_rows = mysql_fetch_row($result);
$pages = new Paginator;
$pages->items_total = $num_rows[0];
$pages->mid_range = 9; // Number of pages to display. Must be odd and > 3
$pages->default_ipp = '10'; 
$pages->paginate();

//echo $pages->display_pages();
echo "<span class=\"\">".$pages->display_jump_menu().$pages->display_items_per_page()."</span>";

$query = "SELECT * FROM color_or_brands $pages->limit";
//echo $query;
$result = mysql_query($query) or die(mysql_error());

$countCondition = mysql_num_rows($result);

 ?>
    <div class="row bottom-margin">
      <div id="page-heading">
        <h1>Brands And Colors</h1>
      </div>
      <div class="add-button col-lg-5"> <a href="add-color-brands.php?hint=brand">
        <input type="button" class="buttons btn btn-info" value="Add Brand"/>
        </a> <a href="add-color-brands.php?hint=color">
        <input type="button" class="buttons btn btn-info" value="Add Color"/>
        </a> </div>
      <div id="paging-table" class="col-lg-6"><?php echo $pages->display_pages(); ?></div>
    </div>
    <?php if(isset($_SESSION['msg']) && $_SESSION['msg']!='')  {?>
    <div id="message-success" style="padding-left: 0; padding-right: 81px; width: 602px;" align="center">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td class="green-left"><?php echo $_SESSION['msg'];  $_SESSION['msg'] ='';?><a href=""></a></td>
          <td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif"   alt="" /></a></td>
        </tr>
      </table>
    </div>
    <?php } ?>
    <form id="mainform" action="" class="table-responsive">
      <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="table table-bordered table-hover table-striped">
        <tr>
          <th width="8%" class="table-header-check"><a id="toggle-all" ></a> </th>
          <th width="12%" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Name</a> </th>
          <th width="8%" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Type </a></th>
          <th  class="table-header-repeat line-left"><a class="arrow_none">Action</a></th>
        </tr>
        <?php 
				if($countCondition > 0){
				while($data = mysql_fetch_object($result)){ ?>
        <tr>
          <td width="20"><input  type="checkbox" name="checkbox" value="<?php echo $data->id;?>"/></td>
          <td><?php echo ucfirst($data->name); ?></td>
          <td><?php echo ucfirst($data->type); ?></td>
          <td><a href="javascript:;" class="delete" alt="<?php echo $data->id;?>" title="Click To Delete Sub Category"> <img src="images/table/action_delete.gif"/> </a> <a href="edit_color-brands.php?edit_id=<?php echo $data->id;?>&hint=<?php echo $data->type; ?>" title="Click To Edit Sub Category"> <img src="images/table/action_edit.gif"/> </a></td>
        </tr>
        <?php }}
				else{ ?>
        <tr>
          <td colspan="6" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>
        </tr>
        <?php }?>
      </table>
      <!--  end product-table................................... -->
    </form>
    
    <!--  start actions-box ............................................... -->
    <div id="actions-box"> <a href="" class="action-slider"></a>
      <div id="actions-box-slider"> <a href="javascript:;" name="delete" id="delete" class="action-delete" >Delete</a> </div>
      <div class="clear"></div>
    </div>
    <!-- end actions-box........... --> 
    
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php") ?>