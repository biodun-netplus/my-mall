<?php 
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */
?>

<!-- start content-outer -->

<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 
  <script type="text/javascript">
function changeStatus(id, status)
{
    $.ajax({
      type: 'POST',
      url: "delete_category.php?prdid="+id+"&status="+status,
      success: function()
      {
        window.location.reload()
        }
    });
  }

$().ready(function(){
  $('#delete').click(function(){
    //alert("akhil");
    var del=[];
    
    $("input[type='checkbox']:checked").each(function(){
         del.push($(this).val());
     $(this).parent("td").parent('tr').addClass('RowHighlight');
       }); 
   //alert(del);
   if(del=="")
   {
     alert("Please select at least one item.")
   }
   else
     if(confirm("Are you sure want to delete the selected item(s)?")){
       $.ajax({
        type:'post',
        url: "delete_category.php?Mproductdel_id="+del,
        success: function()
        {
        $(".RowHighlight").remove();  
        }
     });
     }
    
    })
  
  $(".delete").click(function(){
    if(confirm("Are you sure, you want to delete this item(s)?")){
    var product_del_id=$(this).attr("alt")
    //alert(product_del_id);
    $(this).parent("td").parent('tr').addClass('RowHighlight');
      $.ajax({
      type: 'POST',
      url: "delete_category.php?product_del_id="+product_del_id,
      success: function()
      {
          $(".RowHighlight").remove();

        }
      });
    }
  })

  $(".decline-popup").click(function(){
    var product_id=$(this).attr("alt");
    $("#productId").val(product_id);
   })

  $(".submit-popup").click(function(){
        
      var productId = $('#productId').val();
      $.ajax({
      type: 'POST',
      url: "delete_category.php?product_decline=product_decline",
      data: {declineDescription : $('#declineDescription').val(),productId : $('#productId').val()},
      success: function(res)
      {
          
          $(".modal-content .modal-body").prepend(res);
          $('#productId').val('');
          $('#declineDescription').val('')
          setTimeout(function(){
              $('#alert-remove').remove();
          },30000);
          $("#row-"+productId).remove();
        }
    });
  })

})

</script>
<?php
//$query = "SELECT c.category_name,u.* FROM users AS u INNER JOIN category AS c ON u.category=c.id where user_type='p'";
$where = '';
if(isset($_REQUEST['status']) && $_REQUEST['status'] == 'Inactivate')
{
  $where  = " WHERE products.isactive  = 'f'";
}

if(isset($_REQUEST['status']) && $_REQUEST['status'] == 'Declined')
{
  $where  = " WHERE products.isactive  = 'd'";
}

if(isset($_REQUEST['keyword']))
{
  $query = "SELECT COUNT(*) FROM `products` WHERE product_name like '%".$_REQUEST['keyword']."%'  ";
}
else
{
  $query = "SELECT COUNT(*) FROM `products`  ".$where ; 
}

$result = mysql_query($query) or die(mysql_error());
$num_rows = mysql_fetch_row($result);

$pages = new Paginator;
$pages->items_total = $num_rows[0];
$pages->mid_range = 4; // Number of pages to display. Must be odd and > 3
//$pages->default_ipp = '10'; 
$pages->paginate();

//echo $pages->display_pages();
//echo "<span class=\"\">".$pages->display_jump_menu().$pages->display_items_per_page()."</span>";
if(isset($_REQUEST['keyword']))
{
  $query = "SELECT products.*,category.category_name,users.first_name FROM `products` left join category on products.category=category.id  left join users on  products.pid=users.id WHERE product_name like '%".$_REQUEST['keyword']."%' order by products.id desc $pages->limit";
}
else
{
  $query = "SELECT products.*,category.category_name,users.first_name FROM `products` left join category on products.category=category.id  left join users on  products.pid=users.id ".$where ."order by products.id desc $pages->limit";
}
//echo $query;

$result = mysql_query($query) or die(mysql_error());
$countCondition = mysql_num_rows($result);
 ?>
    <div class="row bottom-margin">
      <div id="page-heading">
        <h1>Products</h1>
      </div>
      <div>
      <div class="bottom-margin" style="width: 100%; overflow-y: auto;">
        <form method="post"  action="product.php" >
          <div class="col-lg-4">
            <input type="text" name="keyword" class="form-control" value="" required="required" />
          </div>
          <div class="col-lg-2">
            <input type="submit" class="buttons" value="Search"/>
          </div>
        </form>
      </div>
        <div class="add-button col-lg-6"> <a href="add_product.php">
          <input type="button" class="buttons btn btn-info" value="Add New Product"/>
          </a> <a href="product.php?status=Inactivate">
          <input type="button" class="buttons btn btn-info" value="Inactivate Products"/>
          </a> <a href="product.php?status=Declined">
          <input type="button" class="buttons btn btn-info" value="Declined Products"/>
          </a></div>
        <div id="paging-table" class="col-lg-6"><?php echo $pages->display_pages(); ?></div>
      </div>
    </div>
    <?php if(isset($_SESSION['msg']) && $_SESSION['msg']!='')  {?>
    <div id="message-success" align="center">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td class="green-left"><?php echo $_SESSION['msg'];  $_SESSION['msg'] ='';?><a href=""></a></td>
          <td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif" alt="" /></a></td>
        </tr>
      </table>
    </div>
    <?php } ?>
    <form id="mainform" action="" class="table-responsive">
      <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="table table-bordered table-hover table-striped">
        <tr>
          <th class="table-header-check"><a id="toggle-all" ></a> </th>
          <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Product name</a></th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Category </a></th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Sub Category </a></th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Sub Sub Category </a></th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Provider </a></th>
          <th  class="table-header-repeat line-left "><a class="arrow_none"> Price </a></th>
          <th  class="table-header-repeat line-left "><a class="arrow_none">Quantity </a></th>
          <!--<th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Short Description </a></th> !-->
          <th  class="table-header-repeat line-left "><a class="arrow_none">Viewed</a></th>
          <th  class="table-header-repeat line-left minwidth-1" style="min-width:145px;"><a class="arrow_none">Image</a> </th>
          <th  class="table-header-repeat line-left "><a class="arrow_none">Status</a> </th>
          <th width="125" class="table-header-repeat line-left"><a class="arrow_none">Action</a></th>
        </tr>
        <?php
         if($countCondition > 0){
        while($data = mysql_fetch_object($result)){ 
        
          $sub_cat = mysql_query("select category_name from category where id = '".$data->sub_category."'");
          $cat = mysql_fetch_object($sub_cat);
            $sub_sub_cat = mysql_query("select category_name from category where id = '".$data->sub_sub_category."'");
            $sub_cat = mysql_fetch_object($sub_sub_cat);
          //$query12 = mysql_query("select * from users where id = '".$data->pid."'");
          //$pro= mysql_fetch_object($query12);
//          print_r($data);
        ?>
        <tr id="row-<?php echo $data->id;?>">
          <td width="20"><input  type="checkbox" name="checkbox" value="<?php echo $data->id;?>"/></td>
          <td><?php echo ucfirst($data->product_name); ?></td>
          <td><?php  echo $data->category_name;  ?></td>
          <td><?php  echo $cat->category_name;  ?></td>
          <td><?php  echo $sub_cat->category_name;  ?></td>
          <td ><?php
          if($data->pid==0)
            echo "Admin";
          else  
          echo $data->first_name;
          ?></td>
          <td >&#x20A6;<?php echo number_format($data->product_price, 2, '.', ''); ?></td>
          <td>
          <?php //echo $data->product_quantity; ?>
            <div class="table-responsive">
            <?php
            $proAttrDetailSql  = mysql_query("SELECT relation_color_size.*
            FROM `relation_color_size` 
            INNER JOIN products ON (products.id =  relation_color_size.product_id) 
            where `relation_color_size`.`product_id` = '".$data->id."' ");  

            if(mysql_num_rows($proAttrDetailSql) > 0)
            { 
            ?>
              <table cellspacing="0" cellpadding="0" border="0" width="100%" class="table table-bordered table-hover table-striped table-cell">
                <tr bgcolor="#F3F3F3">
                  <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Color</a></th>
                  <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Size</a></th>
                  <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Qty</a></th>
                </tr>
                <?php
                 while($value = mysql_fetch_object($proAttrDetailSql)) {
                  
                  ?>
                  <tr>
                    <td><?php $res = mysql_query("SELECT product_color.*
                    FROM `product_color` INNER JOIN products ON (products.id = product_color.pid) 
                    where product_color.id = '".$value->colour_id."' ");  
                    if(mysql_num_rows($res) > 0)
                    {
                      $recored = mysql_fetch_object($res);
                      echo $recored->color;
                    }  
                    ?></td>
                    <td>
                     <?php 
                     $res1 = mysql_query("SELECT product_size.*
                    FROM `product_size` INNER JOIN products ON (products.id = product_size.pid) 
                    where product_size.id = '".$value->size_id ."' ");  
                    if(mysql_num_rows($res1) > 0)
                    {
                      $recored1 = mysql_fetch_object($res1);
                      echo $recored1->size;
                    }
                    ?></td>
                    <td><?php echo $value->qty; ?></td>
                  </tr>
                  <?php
                  }
                  ?>
                </table>
                <?php
                }
                else
                {
                  echo '<div style="color:#F00; font-size:14px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found!</div>';
                }
                ?>
            </div>
          </td>
          <!--<td style="text-align:justify"><?php echo ucfirst(substr($data->product_detail,0,100));?></td> !-->
          <td style="text-align:center;"><?php echo $data->viewed;?></td>
          <td><img src="../product_images/<?php echo $data->image;?>" height="100" width="100" /></td>
          <td  align="center" style="padding-left:20px;" class="status">
          <?php if($data->isactive=='p'){ ?>
            <span title="Pending" >Pending</span>
            <?php }
        else if($data->isactive=='t'){ ?>
            <select alt="<?php echo $data->id;?>" onchange="javascript:changeStatus('<?php echo $data->id;?>','f');">
            <option value="t" selected="selected">Activate</option>
            <option value="f">Inactivate</option>
            </select>
            <?php }
            else if($data->isactive=='f'){ ?>
             <select  alt="<?php echo $data->id;?>" onchange="javascript:changeStatus('<?php echo $data->id;?>','t');">
            <option value="f" selected="selected">Inactivate</option>
            <option value="t">Activate</option>
            </select>
            <?php }
            else if($data->isactive=='d'){   ?>
            <span title="Declined">Declined</span><br/>
            <i style="color: red;"><?php echo $data->declined_desc; ?></i>
            <?php
             }
           ?></td>
          <td><a href="view_product.php?view_id=<?php echo $data->id;?>" title="Click to view Product"><img src="images/table/Gnome-Logviewer-32.png" width="24" height="24" alt="" /> </a> <a href="javascript:;" class="delete" alt="<?php echo $data->id;?>" title="Click to Delete Product"> <img src="images/table/action_delete.gif"/></a><a href="edit_product.php?edit_id=<?php echo $data->id;?>" title="Click to Edit Product"> <img src="images/table/action_edit.gif"/></a></td>
        </tr>
        <?php }}else{ ?>
        <tr>
          <td colspan="10" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>
        </tr>
        <?php } ?>
      </table>
      <!--  end product-table................................... -->
    </form>
    <div id="actions-box"> <a href="" class="action-slider"></a>
      <div id="actions-box-slider"> <a href="javascript:;" name="delete" id="delete" class="action-delete" >Delete</a> </div>
      <div class="clear"></div>
    </div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="images/table/action_delete.gif"></button>
            <h4 class="modal-title" id="myModalLabel">Decline Product</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label>Description: </label>
                    <textarea name="declineDescription" id="declineDescription" class="form-control"></textarea>
                    <input type="hidden" name="productId" id="productId" value="">
              </div>  
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary submit-popup">Submit</button>
      </div>
    </div>
  </div>
</div>
<?php include("footer.php")?>