<?php
include("header.php");
/* Settlement */
/* Author @Mayowa Anibaba */
/* Date : 06-05-2015 */
 
?>

<script src="js/jquery/custom_jquery.js" type="text/javascript"></script>

<!-- MUST BE THE LAST SCRIPT IN <HEAD></HEAD></HEAD> png fix -->
<script type="text/javascript" src="calender/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="calender/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="calender/timepicker.mod.js"></script>
<script type="text/javascript" src="calender/calendar-config.js"></script>
<link href="calender/jquery-ui-sunny.css" rel="stylesheet" type="text/css" />
<script src="js/jquery/jquery.pngFix.pack.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/datePicker.css" type="text/css" />

<script type="text/javascript">
$(document).ready(function(){
	$('#submit').click(function(){
		var DateFrom = $('#startDate').val();
		var DateTo = $('#endDate').val();

		if(DateFrom==""){
			$('#startDate').attr('placeholder','Select Date');}
		if(DateTo==""){
			$('#endDate').attr('placeholder','Select Date');}
		})

$(document).pngFix( );
</script>
	
 <div class="clear"></div>

 <?php include 'settlement/settlement_calculation.php'; ?>

<!-- start content-outer -->
<div id="content-outer" style="padding-top: 20px">
<!-- start content -->
    <div id="page-heading"><h1>Settlement</h1></div>
    <div id="" style="border: 1px solid #ccc; border-radius: 5px;  padding: 20px; margin: 15px">

        <div style="width: 40%; float: left;">
            <form action="settlement.php" method="post">
                 <table>
                    <tr style="height: 35px">
                        <td>Start date:&nbsp;</td>
                        <td><input required type="text" class="datefrom" id="startDate" value="<?php if (isset($_REQUEST['startDate'])) {
    echo $_REQUEST['startDate'];
} ?>" name="startDate" style="height: 25px; vertical-align: top; margin-right: 5px;"/></td>
                    </tr>
                    <tr style="height: 35px">
                        <td>End date:&nbsp;</td>
                        <td><input required type="text" class="dateto123" id="endDate" value="<?php if (isset($_REQUEST['endDate'])) {
    echo $_REQUEST['endDate'];
} ?>" name="endDate" style="height: 25px; vertical-align: top;  margin-right: 5px;"/></td>
                    </tr>
                    <tr style="height: 35px">
                        <td>Payment method:&nbsp;</td>
                        <td>
                            <select name="type" style="height: 35px; width: 200px;">
                            <?php if (isset($_REQUEST['type'])) { ?>
                                <option value="<?php echo $_REQUEST['type'] ?>"><?php echo $_REQUEST['type'] ?></option>
                            <?php } ?>
                                <option value="Card">Card</option>
                                <option value="Pay on Delivery">Pay on Delivery</option>
                                <option value="Bank Deposit">Bank Deposit</option>
                                <option value="Internet Banking">Internet Banking</option>
                                <option value="Interswitchng">Interswitchng</option>
                            </select>
                        </td>
                    </tr>
                     <tr style="height: 35px;">
                        <td></td>
                        <td><input type="submit" id="submit"></td>
                    </tr>
                 </table>
            </form>
        </div>


        <div style="width: 40%; float: left; padding: 10px; border-left: 1px solid #ccc;">
        <table>
            <tr>
                <td style="font-size: 16px; font-weight: 600; width: 250px">Transaction count:</td>
                <td><?php echo number_format($countTotal);?></td>
            </tr>
            <tr>
                <td style="font-size: 16px; font-weight: 600;">Total Amount:</td>
                <td>₦ <?php echo number_format($total_amount,2);?></td>
            </tr>
            <tr>
                <td style="font-size: 16px; font-weight: 600;">Settled amount:</td>
                <td>₦ <?php echo number_format($settledAmount,2);?></td>
            </tr>
            <tr>
                <td style="font-size: 16px; font-weight: 600;">Amount Due to merchants:</td>
                <td>₦ <?php echo number_format($merchantAmount,2);?></td>
            </tr>
            <tr>
                <td style="font-size: 16px; font-weight: 600;">Fedex (delivery):</td>
                <td>₦ <?php echo number_format($fedexDelivery,2);?></td>
            </tr>
            <tr>
                <td style="font-size: 16px; font-weight: 600;">Ecobank Income:</td>
                <td>₦ <?php echo number_format($fidelityAmount,2);?></td>
            </tr>
            <tr>
                <td style="font-size: 16px; font-weight: 600;">Netplus Income:</td>
                <td>₦ <?php echo number_format($netplusAmount,2);?></td>
            </tr>
            <?php if($countTotal>0){?>
            <tr>
                <td colspan="2" style="font-size: 16px; font-weight: 600;"><a href="csv/generate_csv.php?endDate=<?php echo $endDate;?>&startDate=<?php echo $startDate;?>&type=<?php echo $type;?>">Export to CSV</a></td>
            </tr>
            <tr>
                <td colspan="2" style="font-size: 16px; font-weight: 600;"><a href="pdf/generate_pdf.php?endDate=<?php echo $endDate;?>&startDate=<?php echo $startDate;?>&type=<?php echo $type;?>">Export to PDF</a></td>
            </tr>
            <?php } ?>
        </table>
        </div>
        <div style="clear: both;">
    </div>
</div>

<?php if (isset($_SESSION['msg']) && $_SESSION['msg'] != '') { ?>

    <div id="message-success" >
        <table border="0" width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td class="green-left"><?php echo $_SESSION['msg'];
                    $_SESSION['msg'] = ''; ?><a href=""></a></td>
                <td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif" alt=""/></a>
                </td>
            </tr>
        </table>
    </div>
<?php } ?>

<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
			<!--  start table-content  -->
			<div id="table-content">
			
				
				<!--  start message-green -->
               
				<!--  end message-green -->
				<!--  start product-table ..................................................................................... -->
				<form id="mainform" action="">
				<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="table table-bordered table-hover table-striped">
				<tr>
                    <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Transaction Id</a></th>
                    <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">product Detail </a></th>
                    <th  class="table-header-repeat line-left "><a class="arrow_none">Date of Transaction</a></th>
                    <th  class="table-header-repeat line-left "><a class="arrow_none">Payment Method</a></th>
                    <th  class="table-header-repeat line-left "><a class="arrow_none">Merchant Name </a></th>
                    <th  class="table-header-repeat line-left "><a class="arrow_none">Delivery</a></th>
                    <th  class="table-header-repeat line-left "><a class="arrow_none">Cost</a></th>
                    <th  class="table-header-repeat line-left "><a class="arrow_none">Settled</a></th>
                    <th  class="table-header-repeat line-left "><a class="arrow_none">Due</a></th>
				</tr>
               <?php
if ($countCondition > 0) {
    $i = 0;
    $newMerchant = false;
    $first = true;
    $merchant = '';
    $merchantAccount = '';
    $total = 0;
    while ($data = mysql_fetch_object($result)) {

        if($first){
            $first = false;
            $merchant = $data->first_name;
            $merchantAccount =  $data->account_type;
            $total += $data->total_ammount;
        } else {
            if($merchant==$data->first_name){
                $newMerchant = false;
                $total += $data->total_ammount;
                $merchant = $data->first_name;
                $merchantAccount =  $data->account_type;
            }else{
                $newMerchant = true;
            }

        }

        ?>
        <?php if($newMerchant) {
            $settled = $total * $paymentSharePercentage;
            $final = $total * $splitSharePercentage;

            $prod[$num]['first_name'] = $merchant;
            $prod[$num]['account_type'] = '\''.$merchantAccount;
            //$prod[$num]['price'] = number_format($final,2);
            $prod[$num]['total_ammount'] = number_format($final,2);

            $num++;

            ?>
            <tr style="background-color: #DDDDDD;">
                <td colspan="6" style="font-size: 20px"><?php echo $merchant ?> ac.<?php echo $merchantAccount ?></td>
                <td>₦<?php echo number_format($total, 2); ?></td>
                <td>₦<?php echo number_format($settled, 2); ?></td>
                <td style="font-size: 18px">₦<?php echo number_format($final , 2); ?></td>
            </tr>
        <?php $merchant = $data->first_name; $merchantAccount =  $data->account_type; //$total = $data->price; 
        $total = $data->total_ammount; } ?>


        <tr>
            <style>
                .success {
                    font-weight: bold;
                    font-size: 18px;
                    color: #49AE44;
                    padding-right: 9px;
                }

                .pending {
                    font-weight: bold;
                    font-size: 18px;
                    color: #C94214;
                    padding-right: 9px;
                }

                .requested {
                    font-weight: bold;
                    font-size: 18px;
                    color: #B99339;
                    padding-right: 9px;
                }

            </style>
            <td>
                <div class="demo" id="fading-tooltip_<?php echo $i; ?>"
                     style="cursor:pointer;"><?php echo $data->order_id; ?></div>


            </td>
            <td><?php echo $data->product_name; ?></td>
            <td><?php echo $data->date; ?></td>
            <td><?php echo $data->payment;?></td>
            <td><?php echo $data->first_name; ?></td>
            <td>₦<?php echo number_format($data->delivery_amt, 2); ?></td>
            <td>₦<?php echo number_format($data->total_ammount, 2); ?></td>
            <td>₦<?php echo number_format(($data->total_ammount * $paymentSharePercentage), 2); ?></td>
            <td>₦<?php echo number_format((($data->total_ammount * $paymentSharePercentage)*$splitSharePercentage), 2); ?></td>
        </tr>

        <?php
        if($i == $countCondition-1){
            $settled = $total * $paymentSharePercentage;
            //$final = $total * $splitSharePercentage;
            $final   = (($total * $paymentSharePercentage)*$splitSharePercentage);

            $prod[$num]['first_name'] = $merchant;
            $prod[$num]['account_type'] = '\''.$merchantAccount;
            $prod[$num]['price'] = number_format($final,2);
            //$prod[$num]['total_ammount'] = number_format($final,2);
            $num++;
            ?>
            <tr style="background-color: #DDDDDD;">
                <td colspan="6" style="font-size: 20px"><?php echo $merchant ?> ac.<?php echo $merchantAccount ?></td>
                <td>₦<?php echo number_format($total, 2); ?></td>
                <td>₦<?php echo number_format($settled, 2); ?></td>
                <td style="font-size: 18px">₦<?php echo number_format($final , 2); ?></td>
            </tr>

            <?php } ?>

        <?php
        $i++;
    }
} else { ?>
    <tr>
        <td colspan="10" align="center">
            <div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found !
            </div>
        </td>
    </tr>
<?php
} ?>
				</table>
				<!--  end product-table................................... --> 
				</form>
			</div>
			<!--  end content-table  -->
		
			<!--  start actions-box ............................................... 
			<div id="actions-box">
				<a href="" class="action-slider"></a>
					<div id="actions-box-slider">
					<a href="javascript:;" name="delete" id="delete" class="action-delete" >Delete</a>
				</div>
				<div class="clear"></div>
			</div>-->
			
			<!--  start paging..................................................... -->
			<table border="0" cellpadding="0" cellspacing="0" id="paging-table">
			<tr>
			<td>
				<?php echo $pages->display_pages(); ?>
			</td>
			
			</tr>
			</table>
			<!--  end paging................ -->
			
			<div class="clear"></div>
		 
		</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
<?php
$_SESSION['settlement'] = $prod;
include("footer.php")
?>