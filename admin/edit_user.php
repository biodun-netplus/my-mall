<?php
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */

$id  = base64_decode($_GET['id']);
if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{    
	 $role_id 				    =  implode('|',$_REQUEST['role_id'] );
	 $User					      = array();
	 $username 				    = strtolower(str_replace(array( '\'', '"', ',' , '(' , ')', ';' ,':' ,'.','&',"'", '<', '>',' ' ),'',$_REQUEST['user_name']));
	 $User['username']		= $username;	
	 $User['email_id']		= $_REQUEST['user_email'];
	 $User['contact']		  = $_REQUEST['user_contact'];
	 $User['address']		  = $_REQUEST['user_address'];
   $User['role']        = $role_id;
   //$User['isactive']    = 'p';

	 if(isset($_REQUEST['user_pass']) && $_REQUEST['user_pass'] !='')
	 $User['password']		= md5($_REQUEST['user_pass']);
	 $main					      = new users;
	 
	 $update= $main->update($User,"admin",$id);
	 if($update)
	 {
		header("location:view_users.php");
	 }
}
$Query = mysql_query("SELECT * FROM admin where id = '".$id."'");
$Data = mysql_fetch_object($Query);
?>

<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content" style="">
    <div >
      <div id="page-heading" >
        <h1>Edit User</h1>
      </div>
      <div style="float:right; padding-right:20px;">
        <input type="button" class="buttons btn btn-info" value="Back" onclick="window.history.back()" />
      </div>
    </div>
    <div class="row">
      <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" id="UserForm">
        <div class="col-lg-5">
          <div class="form-group">
            <label>Name: </label>
            <input type="text" class="form-control" id="user_name" name="user_name" value="<?php echo $Data->username;?>" />
            <label class="error"  id="name_error"></label>
          </div>
          <div class="form-group">
            <label> Email: </label>
            <input type="text" class="form-control" id="user_email" name="user_email" value="<?php echo $Data->email_id;?>" />
            <label class="error"  id="email"></label>
          </div>
          <div class="form-group">
            <label>Password: </label>
            <input type="password" class="form-control" id="user_pass" name="user_pass" value="" />
            <label class="error"  id="password"></label>
          </div>
          <div class="form-group">
            <label>Contact No: </label>
            <input type="text" class="form-control" id="user_contact" name="user_contact" value="<?php echo $Data->contact;?>" />
            <label class="error"  id="contact"></label>
          </div>
          <div class="form-group">
            <label>Address: </label>
            <textarea class="form-control" id="user_address" name="user_address"><?php echo $Data->address;?></textarea>
            <label class="error"  id="address"></label>
          </div>
          <div class="form-group">
            <input type="submit" id="UserSubmit" value="submit" name="submit" class="form-submit btn btn-default" />
            <input type="reset" value="Reset" class="form-reset btn btn-default"  />
          </div>
        </div>
        <div class="col-lg-6">
          <h2>Roles</h2>
		  <?php         
          $Query = mysql_query("SELECT * FROM role");
          
          ?> 
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <tbody>
               <?php 
			   if(mysql_num_rows($Query) > 0){

          $checkId  = explode("|", $Data->role);
          
			   while($Data1  = mysql_fetch_object($Query)){
			   ?>
                <tr>
                  <td><input type="checkbox" name="role_id[]" value="<?php echo $Data1->id;?>" <?php if(in_array($Data1->id, $checkId)){?> checked="checked" <?php }?> /></td>
                  <td><?php echo $Data1->title;?></td>
                  <td><?php echo $Data1->description;?></td>
                </tr>
              <?php
			   }
			   }
			   else{
				  echo  '<td colspan="5" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>';
				   
			   }
			   ?>
              </tbody>
            </table>
          </div>
        </div>
      </form>
      
      <!-- end id-form  --> 
    </div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>