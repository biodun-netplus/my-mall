<?php 
include("ship_header.php"); 

$query = "SELECT cart.*,spa.* from cart left join shipping_address as spa ON spa.tranx_id = cart.order_id  WHERE cart.Ad = '1' and (cart.payment_status = 'Complete' || cart.payment_status = 'Requested') and spa.country = 'NG' and spa.shipping !='' GROUP BY cart.order_id";
$resultForPaging = mysql_query($query) or die(mysql_error());
$num_rows = mysql_num_rows($resultForPaging);
$pages = new Paginator;
$pages->items_total = $num_rows;
$pages->mid_range = 4; // Number of pages to display. Must be odd and > 3
$pages->default_ipp = '20'; 
$pages->paginate();
$query1 = "SELECT cart.*,spa.* from cart left join shipping_address as spa ON spa.tranx_id = cart.order_id  WHERE cart.Ad = '1' and (cart.payment_status = 'Complete' || cart.payment_status = 'Requested') and spa.country = 'NG' and spa.shipping !='' GROUP BY cart.order_id ORDER BY cart.id DESC $pages->limit";
$result = mysql_query($query1) or die(mysql_error());
$countCondition = mysql_num_rows($result);
?>
<style>
.success {
	font-weight:bold;
	font-size:18px;
	color:#49AE44;
	padding-right:9px;
}
.pending {
	font-weight:bold;
	font-size:18px;
	color:#C94214;
	padding-right:9px;
}
.requested {
	font-weight:bold;
	font-size:18px;
	color:#B99339;
	padding-right:9px;
}
.inp-select {
	background: url("images/forms/form_inp_sm.gif") no-repeat scroll 0 0 transparent;
	width:100px;
	margin-right:2px;
	margin-top:15px;
}
</style>
<script type="text/javascript" src="/../js/wtooltip.js"></script>
 
<script type="text/javascript">

$(function()
{
	$(".StatusUpdate").change(function()
	{
		if(this.value=='Picked Up')
		{
			$("#oid").val(this.id);
			$.fancybox({
            'width': '40%',
            'height': '40%',
            'autoScale': true,
            'transitionIn': 'fade',
            'transitionOut': 'fade',
            'type': 'inline',
            'href': '#inline'
        });
		}
		else
		{
			$.ajax({
			  type:'post',
			  url: 'delete_category.php',
			  data :{SendFedExMail:10,OrderId:this.id,Value:this.value},
			  success: function(data) 
			  {
				  alert("Email Notification Successfully Sent.")
				  window.location.reload();
			  }
			});
		}
	})
	$(".modalbox").fancybox();
	$("#send").on("click", function(){
		if(validateNumDays())
		{
			$("#send").replaceWith("<em>sending...</em>");
			$.ajax({
				type: 'POST',
				url: 'delete_category.php',
				data :{SendFedExMail:10,OrderId:$("#oid").val(),Value:'Picked Up',days:$("#days").val()},
				success: function(data) 
				{
						$("#contact").fadeOut("fast", function()
						{
							$(this).before("<p><strong>Email Notification Successfully Sent.</strong></p>");
							setTimeout("$.fancybox.close()", 3000);
							setTimeout("window.location.reload()", 3100);
						})
				}
			});
			return false;
		}
		else
		{
			return false;
		}
	})
		
})
function validateNumDays()
{
	if($("#days").val()=='')
	{
		$("#days").addClass('error');
		return false
	}
	else if(isNaN($("#days").val()))
	{
		$("#days").addClass('error');
		return false
	}
	else
	{
		$("#days").removeClass('error');
		return true
	}
}
</script>
 <link rel="stylesheet" type="text/css" media="all" href="fancybox/style.css">
  <link rel="stylesheet" type="text/css" media="all" href="fancybox/jquery.fancybox.css">
 
  <script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.0.6"></script>

<a class="modalbox" style="display:none;" href="#inline">click to open</a>
<div id="inline">
	<h2>Enter number of days to receive the package.</h2>
	<form id="contact" name="contact" action="#" method="post">
		<input type="text" id="days" name="days" class="txt">
        <input type="hidden" id="oid" name="oid" class="txt">
		<button id="send">Submit</button>
	</form>
</div>
<div class="clear"></div>
<div id="content-outer">
  <div id="content">
    <div>
      <div id="page-heading">
        <h1>FedEx Transactions Record</h1>
      </div>
    </div>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
      <tr>
        <th rowspan="3" class="sized"></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><div id="content-table-inner">
            <div id="table-content">
              <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
                <tr>
                  <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Unique Id</a></th>
                  <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Transaction Id</a></th>
                  <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">User Details</a></th>
                  <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Shipping Price</a></th>
                  <th  class="table-header-repeat line-left "><a class="arrow_none">View Product Details</a></th>
                  <th  class="table-header-repeat line-left "><a class="arrow_none">Date of Transaction</a></th>
                  <th  class="table-header-repeat line-left "><a class="arrow_none">Transaction Type</a></th>
                  <th  class="table-header-repeat line-left "><a class="arrow_none">Transaction Status</a></th>
                </tr>
                <?php
										if($countCondition > 0)
										{
											$i = 0;
										while($data = mysql_fetch_object($result))
										{ 
										
										if($data->user_id!='')
										{
											$ConsumerDetail = mysql_fetch_object(mysql_query("SELECT u.*,st.name AS StateName,cn.name AS CountryName,cp.points 
											FROM users AS u LEFT JOIN states AS st ON st.state_id = u.state LEFT JOIN countries AS cn ON cn.id  = u.country 
											left join coupon AS cp ON cp.user_id = u.id WHERE u.id = '".$data->user_id."'"));
											$email = $ConsumerDetail->email;
											$name = $ConsumerDetail->first_name;
											$Address = $ConsumerDetail->address." , ".$ConsumerDetail->city." , ".$ConsumerDetail->StateName." , ".$ConsumerDetail->CountryName;
											$company = $ConsumerDetail->company;
										}
										else
										{
											$ConsumerDetail = mysql_fetch_object(mysql_query("SELECT * FROM shipping_address WHERE tranx_id = '".$data->order_id."'"));
											$email = $ConsumerDetail->email;
											$name = $ConsumerDetail->first_name;
											if($ConsumerDetail->country=='US'){ $cn = "United State"; }elseif($ConsumerDetail->country=='NG'){ $cn = "Nigeria"; }else{ $cn = '';}
											$Address = $ConsumerDetail->address1." , ".$ConsumerDetail->city." , ".$cn;
											$company = '';
										}
										
										$MerchantDetail = mysql_fetch_object(mysql_query("SELECT p.pid,u.first_name FROM products AS p LEFT JOIN users 
										AS u ON u.id = p.pid WHERE p.id = '".$data->product_id."'"));
										?>
                <tr>
                  <td><?php echo "WM".$data->id; ?></td>
                  <td><div class="demo" id="fading-tooltip_<?php echo $i; ?>" style="cursor:pointer;">
                      <?php  echo $data->order_id; ?>
                    </div>
                    <script type="text/javascript" language="javascript">
									$("#fading-tooltip_<?php echo $i; ?>").wTooltip({ 
										 content: '<div style="display:inline-block; width:350px; word-wrap: break-word;">Transaction Id : <?php echo $data->order_id; ?> <br>Reference Code : <?php echo $data->trns_code; ?><br><span>Transaction Status :</span><span style="width:330px; word-wrap: break-word; color:#F66; display:block;"> <strong><?php echo $data->trns_status; ?></strong></span></div>', 
										 fadeIn: 500, 
										 fadeOut: "slow",
										 offsetY: 15, 
										 offsetX: 0  
									}); 
									</script></td>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>Name</td>
                        <td><?php  echo ucfirst($name); ?></td>
                      </tr>
                      <tr>
                        <td>Address</td>
                        <td><?php echo $Address; ?></td>
                      </tr>
                      <tr>
                        <td>Phone</td>
                        <td><?php  echo $ConsumerDetail->phone; ?></td>
                      </tr>
                    </table></td>
                  <?php /*?><td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>Name</td>
                        <td><?php  echo $data->product_name; ?></td>
                      </tr>
                      <tr>
                        <td>Quantity</td>
                        <td><?php echo $data->quantity;?></td>
                      </tr>
                      <tr>
                        <td>Price</td>
                        <td><?php  echo $data->price; ?></td>
                      </tr>
                    </table></td><?php */?>
                  <td><?php  echo "&#8358; ".$data->shipping; ?></td>
                  <td><a href="FedEx_Detail.php?id=<?php echo $data->order_id; ?>" id="<?php echo $data->shipping; ?>" class="Details">View Details</a></td>
                  <td><?php  echo $data->date; ?></td>
                  <td><?php  if($data->payment=="W"){ echo "WebPay"; }elseif($data->payment=="B"){ echo "Bank Deposit";}else{ echo $data->payment; } ?></td>
                  <td><div class="demo <?php if($data->payment_status=='Complete'){ echo 'success';}elseif($data->payment_status=='Requested'){ echo 'requested';}
						else{ echo 'pending';} ?>" id="fading-tooltip1_<?php echo $i; ?>" style="cursor:pointer;">
                      <?php if($data->payment_status=='Complete' || $data->payment_status=='Requested'){ echo $data->payment_status; }
						else{ echo "Canceled";} ?>
                    </div>
                    <script type="text/javascript" language="javascript">
							$("#fading-tooltip1_<?php echo $i; ?>").wTooltip({ 
								content: '<div style="display:inline-block; width:350px; word-wrap: break-word;">Transaction Id : <?php echo $data->order_id; ?><br>Reference Code : <?php echo $data->trns_code; ?><br><span>Transaction Status :</span><span style="width:330px; word-wrap: break-word; color:#F66; display:block;"> <strong><?php echo $data->trns_status; ?></strong></span></div>', 
								fadeIn: 500, 
								fadeOut: "slow",
								offsetY: 15, 
								offsetX: -350  
							}); 
						</script>
                       <?php if($data->payment_status=='Complete' || $data->payment_status=='Requested')
					   { ?>
                     <select id="<?php echo $data->order_id.'NP'.$Address.'NP'.$email; ?>" name="StatusUpdate" class="inp-select StatusUpdate" >
                      <option value=""></option>
                      <option <?php if($data->FedEx_Status=='Picked Up'){?>  selected="selected" <?php } ?> value="Picked Up">Picked Up</option>
                      <option <?php if($data->FedEx_Status=='Delivered'){?>  selected="selected" <?php } ?> value="Delivered">Delivered</option>
                    </select>
                    <?php if($data->FedEx_Status=='Picked Up'){ echo 'Delivered in <strong>'.$data->days.'</strong> days'; }?>
                    <?php } ?>
                    </td>
                </tr>
                <?php 
                                    $i++; }
                                    }
                                    else
                                    { 	?>
                <tr>
                  <td colspan="10" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>
                </tr>
                <?php 
                                    } 	?>
              </table>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" id="paging-table">
              <tr>
                <td><?php echo $pages->display_pages(); ?></td>
              </tr>
            </table>
            <!--  end paging................ -->
            
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>
