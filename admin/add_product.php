<?php
ob_start(); //error_reporting(E_ALL);
include("header.php");

if(isset($_REQUEST['submit']))
{
	
	//print_r($_REQUEST); 
	$destination = '../product_images/';
	$file = $_FILES['image'];
	$upload = new Upload; 
	$result = $upload->uploadImage($file, $destination); 

	$date=date("Y-m-d H:i:s");
	$Product=array();
	$Product['product_name']=$_POST['product_name'];	
	$Product['category']=$_POST['provider_category'];
	$Product['sub_category']=$_POST['provider_subcategory'];
	$Product['sub_sub_category']=$_POST['provider_sub_subcategory'];
	$Product['pid']=$_POST['pid'];
	$Product['product_price']=$_POST['product_price'];
	$Product['product_quantity']=$_POST['product_quantity'];
	$Product['product_detail']=$_POST['product_detail'];
	$Product['currency']=$_POST['currency'];
	$Product['image']=$result; 
	$main = new users;
	$success= $main->product_details($Product,"products"); //die();
	if($success)
	{
		header('location:product.php');
	}
}
 ?>

<script type="text/javascript" src="js/validation.js"></script>

 
 <div class="clear"></div>
 
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content" style="">

<div>
<div id="page-heading" ><h1>Add Products</h1></div>
<div style="float:right; padding-right:20px;"><input type="button" class="buttons btn btn-info" value="Back" onclick="goBack()" /></div>
</div>


	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
		<tr>
			<th rowspan="3" class="sized"></th>
			<th class="topleft"></th>
			<td id="tbl-border-top">&nbsp;</td>
			<th class="topright"></th>
			<th rowspan="3" class="sized"></th>
		</tr>
		<tr>
			<td id="tbl-border-left"></td>
			<td>
	<!--  start content-table-inner -->
				<div id="content-table-inner">
				
				<!--  start message-red -->
				
				<!--  end message-red -->
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
					<tr valign="top">
						<td>
							<form action="add_product.php" name="form1" method="post" enctype="multipart/form-data" id="add-product">
							<table border="0" cellpadding="0" cellspacing="0"  id="id-form" width="100%">
								<tr>
									<th width="33%" valign="top" >Product Name:</th>
									<td width="33%"><input type="text" class="form-control" id="product_name" name="product_name" value="" /></td>
									<td width="33%"><label class="error"  id="namea"></label></td>
			
								</tr>
								<tr>
									<th valign="top">Category:</th>
									<td>
										<select  class="form-control" id="provider_categorya" name="provider_category" onchange="javascript:getSub(this.value,'provider_categorya','provider_subcategory')">
											<option value="">Ctagory</option>
											<?php 
											$main= new users;
											$xx= $main->view_isactive_cat("category",0,'t');
											foreach($xx as $data){ ?>
											<option value="<?php echo $data->id ?>"><?php echo $data->category_name ?></option> <?php }?>
										</select>
									</td><td><label class="error"  id="categorya"></label></td>
								</tr>
								<tr>
									<th valign="top">Sub Category:</th>
									<td>
										<select  class="form-control" id="provider_subcategory" name="provider_subcategory" onchange="javascript:getSub(this.value,'provider_subcategory','provider_sub_subcategory')">
											<option value="">Sub Category</option>
										</select>
									</td><td><label class="error"  id="subcategory"></label></td>
								</tr>

                                <tr>
                                    <th valign="top">Sub Sub Category:</th>
                                    <td>
                                        <select  class="form-control" id="provider_sub_subcategory" name="provider_sub_subcategory">
                                            <option value="">Sub Sub Category</option>
                                        </select>
                                    </td><td><label class="error"  id="sub_subcategory"></label></td>
                                </tr>

                                <tr>
									<th valign="top">Provider:</th>
									<td>
										<select  required class="form-control" id="provider_name" name="pid">
											<option value="">provider_name</option>
											<?php 
										
											$sel=mysql_query("select * from users where user_type='p' and isactive='t'");
											while($data1=mysql_fetch_assoc($sel))
											
											{ ?>
											<option value="<?php echo $data1['id']; ?>"><?php echo $data1['first_name']; ?></option> <?php }?>
										</select>
									</td><td><label class="error"  id="categorya"></label></td>
           
								</tr>
                                
								<tr>
									<th valign="top">Product Price :</th>
									<td><input type="text" class="form-control" id="product_price" name="product_price" value="" /></td>
									<td><label class="error"  id="price"></label></td>
								</tr>
                                <tr>
									<th valign="top">Currency:</th>
									<td>
										<select  class="styledselect_pages" id="currency" name="currency">
											<option value="Naira">Naira</option>
                                            <option value="Doller">Us Dollar</option>
                                            <option value="Euro">Euro</option>
                                            <option value="Pounds">Pounds</option>
										</select>
									</td><td><label class="error"  id="cccccc"></label></td>
           
								</tr>
								<tr>
									<th valign="top">Product Quantity:</th>
									<td><input type="text" class="form-control" id="product_quantity" name="product_quantity" value="" /></td>
									<td><label class="error"  id="quantity"></label></td>
								</tr>			
								<tr>
									<th valign="top">Message : </th>
										<td><textarea rows="" cols="" class="form-control form-textarea" name="product_detail" id="product_detail"></textarea></td>
                                        <td><label class="error"  id="detail"></label></td>
										</tr>
			
								<tr>
									<th valign="top">Upload Image:</th>
									<td>
									<input type="file" name="image"  id="image" value=""></td>
									<td><label class="error"  id="imagea"></label></td>


	
								</tr>
								<tr>
									<th>&nbsp;</th>
									<td valign="top">
										<input type="submit" id="Product_submit" value="submit" name="submit" class="form-submit btn btn-default" />
										<input type="reset" value="Reset" class="form-reset btn btn-default"  />		</td>
								</tr>
						</td>	
					</tr>
				</table>
							</form>
	<!-- end id-form  --></td>
	
</tr>
<tr>
<td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
	
			<div class="clear"></div>
 

</div>
<!--  end content-table-inner  -->
			</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>

<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
<link rel="stylesheet" type="text/css"  href="../assets/css/editor.css" />
<script type="text/javascript" src="../assets/js/editor.js"></script>
<script type='text/javascript'> $('#product_detail').Editor(); $('form#add-product').submit(function(){ $('#product_detail').val($('.Editor-editor').html()); }); $( ".Editor-editor" ).keyup(function() {   $('#product_detail').val($('.Editor-editor').html()); }); $( ".Editor-editor" ).keydown(function() {   $('#product_detail').val($('.Editor-editor').html()); });
/*=== get subcategory from category ============================================*/
//	$("body").on("click","div#provider_categorya_container ul li",function (){
//
//		$('.loader').show();
//		$.ajax({
//			 url: "delete_category.php",
//			 data: {'catId':$("#provider_categorya").val(),  'getSubCat': 'getSubCat' },
//			 success: function(Data){
//				 $('.loader').hide();
//				 $('#provider_subcategory').find('option').remove().end().append(Data);
//
//
//			}
//		});
//		return false;
//
//	})
function getSub(id,sending_div,receiving_div)
{

    $.ajax({
        type: 'POST',
        url: 'delete_category.php',
        data: {'catId':$("#"+sending_div).val(),  'getSubCat': 'getSubCat' },
        beforeSend:function()
        {
            $('#'+receiving_div).html("<div class='col-md-9' ><img src='images/small_loader.gif'></div>");
        },
        success: function (data) {
            $('#'+receiving_div).html(data);
        }
    });
}
</script>
<?php include("footer.php")?>

