<?php 
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */
//error_reporting(E_ALL);

if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{       
  
  $User=array();
  $date=date("Y-m-d H:i:s");
  $main= new users;
  if($_FILES['image']['tmp_name']!='')
  {
    $destination = '../category_images/';
    $alreadyimage = $main->viewprofile($_REQUEST['id'],'category');
    if($alreadyimage->image!='' && file_exists($destination.$alreadyimage->image))
    {
      unlink($destination.$alreadyimage->image);
    }
    $file = $_FILES['image'];
    $upload = new Upload; 
    $result = $upload->uploadImage($file, $destination, null, null); 
    $User['image']=$result;
  }
  if($_FILES['banner']['tmp_name']!='')
  {
    $destination = '../category_images/';
    $alreadyimage = $main->viewprofile($_REQUEST['id'],'category');
    if($alreadyimage->banner!='' && file_exists($destination.$alreadyimage->banner))
    {
      unlink($destination.$alreadyimage->banner);
    }
    $file = $_FILES['banner'];
    $upload = new Upload; 
    $result = $upload->uploadImage($file, $destination, null, null); 
    $User['banner']=$result;
  }
  if($_FILES['banner1']['tmp_name']!='')
  {
    $destination = '../category_images/';
    $alreadyimage = $main->viewprofile($_REQUEST['id'],'category');
    if($alreadyimage->vert_banner!='' && file_exists($destination.$alreadyimage->vert_banner))
    {
      unlink($destination.$alreadyimage->vert_banner);
    }
    $file = $_FILES['banner1'];
    $upload = new Upload; 
    $result = $upload->uploadImage($file, $destination, null, null); 
    $User['vert_banner']=$result;
  }

  //print_r($_REQUEST); die(); 

  $User['category_name']=$_REQUEST['category_name'];  
  $User['message']=$_REQUEST['message'];
  $User['created']=$date;
  $User['parent_id'] = $_REQUEST['parent_cat'];
  $User['weight'] = $_REQUEST['weight'];
  $User['unit'] = $_REQUEST['unit'];
  $id=$_REQUEST['id'];
  $xx= $main->update($User,"category",$id);    
  if($xx)
  {
    $_SESSION['msg']='Category detail updateded successfully.';
    header("location:catergory.php");
  }
}
if(isset($_REQUEST['edit_id']))
{
  $edit= new users;
  $edit_data= $edit->viewprofile($_REQUEST['edit_id'],"category");
}
?>
<script type="text/javascript" src="js/validation.js"></script>

<div class="clear"></div>

<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content">
    <div>
      <div id="page-heading" >
        <h1>EditCategory</h1>
      </div>
      <div style="float:right; padding-right:20px;">
        <input type="button" class="buttons btn btn-info" value="Back" onclick="goBack()" />
      </div>
    </div>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
      <tr>
        <th rowspan="3" class="sized"></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><!--  start content-table-inner -->
          
          <div id="content-table-inner"> 
            
            <!--  start message-red --> 
            
            <!--  end message-green -->
            
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
              <tr valign="top">
                <td align="center"><!--  start step-holder --> 
                  <!--  end step-holder --> 
                  <!-- start id-form -->
                  
                  <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" enctype="multipart/form-data">
                    <table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
                       <tr>
                        <th valign="top">Parent category:</th>
                        <td><select class="form-control" id="parent_cat" name="parent_cat">
                        <option value="">Select Category</option>
                        <?php
                        $main = new users; 
                        echo $main->get_categories(0,$edit_data->parent_id);
                        ?>
                        </select>
                        </td>
                        <td width="301"><label class="error" id="parent_cate"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Category name:</th>
                        <td><input type="text" class="form-control" name="category_name" value="<?php  echo $edit_data->category_name;?>" /></td>
                      </tr>
                      <tr>
                        <input type="hidden" name="id" value="<?php echo $edit_data->id;?>"/>
                      <tr>
                        <th valign="top">Message : </th>
                        <td><textarea rows="" cols="" class="form-control form-textarea" name="message"><?php echo $edit_data->message;?></textarea></td>
                      </tr>
                      <tr>
                        <th valign="top">Category Image: </th>
                        <td><img src="../category_images/<?php echo $edit_data->image;?>" height="100" width="100" /><br />
                          <input type="file" id="image" name="image" /></td>
                      </tr>
                      <tr>
                        <th valign="top">Category Banner 1: </th>
                        <td><img src="../category_images/<?php echo $edit_data->banner;?>" height="100" width="100" /><br />
                          <input type="file" id="banner" name="banner" /></td>
                      </tr>
                      <tr>
                        <th valign="top">Category Banner 2: </th>
                        <td><img src="../category_images/<?php echo $edit_data->vert_banner;?>" height="100" width="100" /><br />
                          <input type="file" id="banner1" name="banner1" /></td>
                      </tr>
                      <tr>
                        <th valign="top">Weight: </th>
                        <td>
                          <input type="text" value="<?php echo $edit_data->weight;?>" id="weight" name="weight" /></td>
                      </tr>
                      <tr>
                        <th valign="top">Unit: </th>
                        <td>
                          <select name="unit" id="unit" class="form-control">
                            <option value="Kg" <?php if($edit_data->unit == 'Kg'){ ?> Selected <?php } ?>>Kg</option>
                            <option value="Gm" <?php if($edit_data->unit == 'Gm'){ ?> Selected <?php } ?>>Gm</option>
                          </select>
                         </td>
                      </tr>
                      <tr>
                        <th>&nbsp;</th>
                        <td valign="top"><input type="submit" name="submit" value="submit" class="form-submit btn btn-default" /></td>
                      </tr>
                    </table>
                  </form>
                  
                  <!-- end id-form  --></td>
              </tr>
              <tr>
                <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                <td></td>
              </tr>
            </table>
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>
