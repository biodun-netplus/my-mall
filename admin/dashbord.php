<?php 
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */
include("header.php");
?>

<div class="row">
  <div class="col-lg-12">
    <div id="page-heading">
      <h1>Dashboard</h1>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-3 col-md-6">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xs-3"> <i class="fa fa-cube fa-5x"></i> </div>
          <div class="col-xs-9 text-right">
            <div class="huge"><?php echo $total_product->total_product ;?></div>
            <div>Total Active Products</div>
          </div>
        </div>
      </div>
      <a href="product.php">
      <div class="panel-footer"> <span class="pull-left">View Details</span> <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
        <div class="clearfix"></div>
      </div>
      </a> </div>
  </div>
  <div class="col-lg-3 col-md-6">
    <div class="panel panel-green">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xs-3"> <i class="fa fa-user fa-5x"></i> </div>
          <div class="col-xs-9 text-right">
            <div class="huge"><?php echo $total_merchant->total_merchant ;?></div>
            <div>Total Merchants</div>
          </div>
        </div>
      </div>
      <a href="provider.php">
      <div class="panel-footer"> <span class="pull-left">View Details</span> <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
        <div class="clearfix"></div>
      </div>
      </a> </div>
  </div>
  <div class="col-lg-3 col-md-6">
    <div class="panel panel-yellow">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xs-3"> <i class="fa fa-user fa-5x"></i> </div>
          <div class="col-xs-9 text-right">
            <div class="huge"><?php echo $total_approved_merchant->total_approved_merchant ;?></div>
            <div>Total Active Mercha..</div>
          </div>
        </div>
      </div>
      <a href="provider.php">
      <div class="panel-footer"> <span class="pull-left">View Details</span> <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
        <div class="clearfix"></div>
      </div>
      </a> </div>
  </div>
  <div class="col-lg-3 col-md-6">
    <div class="panel panel-red">
      <div class="panel-heading">
        <div class="row">
          <div class="col-xs-3"> <i class="fa fa-user fa-5x"></i> </div>
          <div class="col-xs-9 text-right">
            <div class="huge"><?php echo $total_customer->total_customer ;?></div>
            <div>Total Customers</div>
          </div>
        </div>
      </div>
      <a href="product.php">
      <div class="panel-footer"> <span class="pull-left">View Details</span> <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
        <div class="clearfix"></div>
      </div>
      </a> </div>
  </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o fa-fw"></i> Transactions Chart</h3>
            </div>
            <div class="panel-body">
                <div id="morris-area-chart"></div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
<?php /*
<div class="row">
  <div class="col-lg-8">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Transactions Panel</h3>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-bordered table-hover table-striped">
            <thead>
              <tr>
                <th>Order #</th>
                <th>Order Date</th>
                <th>Order Time</th>
                <th>Amount (USD)</th>
              </tr>
            </thead>
            <tbody>
      <?php 
      $result = mysql_query("SELECT * FROM `order` ORDER BY id DESC LIMIT 10 ") or die(mysql_error());
      if(mysql_num_rows($result) > 0){
        while($records = mysql_fetch_object($result)){
      ?>
              <tr>
                <td><?php echo $records->order_id; ?> </td>
                <td><?php echo date("d-m-Y", strtotime($records->created)); ?></td>
                <td><?php echo date("H:i", strtotime($records->created)); ?></td>
                <td><?php echo $records->total_ammount; ?></td>
              </tr>
           <?php
        }
      }
      else{?>
              <tr>
                <td colspan="5"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>
              </tr>
            <?php
      }
      ?>
            </tbody>
          </table>
        </div>
        <div class="text-right"> <a href="users.php">View All Transactions <i class="fa fa-arrow-circle-right"></i></a> </div>
      </div>
    </div>
  </div>
</div>
*/ ?>
<!-- Morris Charts style -->
<link href="<?php echo $site_url; ?>css/themes/plugins/morris.css" rel="stylesheet">
<!-- Morris Charts JavaScript -->
<script src="<?php echo $site_url; ?>js/themes/plugins/morris/raphael.min.js"></script>
<script src="<?php echo $site_url; ?>js/themes/plugins/morris/morris.min.js"></script>
<?php 
$mons = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Aug", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec");
$startDate    = $nextDate   = 0;
$cardArr      = $internetBankingArr  = $netpluspayArr   = $cashOnDeliveryArr  = array();
$getStartDate = $getNextDate  = "";

//$res = mysql_query("SELECT * FROM `cart`  where date >= now()-interval 3 month ORDER BY date, payment");

$res = mysql_query("SELECT cart.*, `order`.status,  `order`.total_ammount  FROM `cart` 
INNER JOIN `order` ON (`order`.order_id = cart.order_id)
where cart.date >= now()-interval 3 month AND `order`.status = 'Completed' 
GROUP BY `order`.transaction_ref
ORDER BY cart.date, cart.payment");
if(mysql_num_rows($res) > 0)
{
  
  $currentDate  = 1;
  $falg       = 0;
  $data       = array();
  $index      = 0;
  $rowIndex     = 0;
  while ($row = mysql_fetch_object($res))
  {
    $currentDate  = date("Y-m-d",strtotime($row->date));
    if($currentDate > $nextDate)
    {
      
      if($nextDate > 0 )
      {
        //$data[$index]['period']     = date("M Y",strtotime($startDate))." - ".date("M Y",strtotime($nextDate));

        
        //$data[$index]['period']       = date("Y",strtotime($startDate)). " Q" .array_search( date("M",strtotime($startDate)),$mons);

        $data[$index]['period']       = date("Y-m",strtotime($startDate));


        if(array_sum($cardArr) > 0)
        $data[$index]['card']       = array_sum($cardArr);
        else
        $data[$index]['card']       = null;

        if(array_sum($internetBankingArr) > 0)  
        $data[$index]['internetBanking']  = array_sum($internetBankingArr);
        else
        $data[$index]['internetBanking']  = null;

        if(array_sum($cashOnDeliveryArr) > 0) 
        $data[$index]['cashOnDelivery']   = array_sum($cashOnDeliveryArr);
        else
        $data[$index]['cashOnDelivery']   = null;
        
        if(array_sum($netpluspayArr) > 0) 
        $data[$index]['netpluspay']     = array_sum($netpluspayArr);
        else
        $data[$index]['netpluspay']     = null;

        if(array_sum($interswitchngArr) > 0) 
        $data[$index]['interswitchng']     = array_sum($interswitchngArr);
        else
        $data[$index]['interswitchng']     = null;


        $index ++;
        $cardArr    = $internetBankingArr  = $netpluspayArr   = $cashOnDeliveryArr  = $interswitchngArr = array();
      
      }
      
      $startDate    = date("Y-m-d",strtotime($row->date));
      //$date       = strtotime(date("Y-m-d", strtotime($row->date)) . " +2 month");
      //$nextDate     = date("Y-m-d",$date);
      //$nextDate   = date("Y-m-t", strtotime($nextDate));
      $nextDate   = date("Y-m-t", strtotime($row->date));


    }


    $getDate  = date("Y-m-d",strtotime($row->date));
    if($nextDate >= $getDate)
    {
      if($row->payment == "Card" )
      {
        $cardArr[] = $row->total_ammount;   

      }
      if($row->payment == "Internet Banking" || $row->payment == "Bank Deposit" )
      {
        $internetBankingArr[] = $row->total_ammount;  

      }
      if($row->payment == "Pay On Delivery" )
      {
        $cashOnDeliveryArr[] = $row->total_ammount;   

      }
      if($row->payment == "Netpluspay" )
      {
        $netpluspayArr[] = $row->total_ammount;   

      }
      if($row->payment == "interswitchng" )
      {
        $interswitchngArr[] = $row->total_ammount;   

      }


    }
    
    $rowIndex ++;
    if(mysql_num_rows($res) == $rowIndex)
    {

      /*if(date("M Y",strtotime($startDate)) == date("M Y"))
      {

        $data[$index]['period']     = date("M Y",strtotime($startDate));
      }
      else
      {
        $data[$index]['period']     = date("M Y",strtotime($startDate))." - ".date("M Y");
      
      } */

      //$data[$index]['period']       = date("Y",strtotime($startDate)). " Q" .array_search( date("M",strtotime($startDate)),$mons);

      $data[$index]['period']       = date("Y-m",strtotime($startDate));
      
      if(array_sum($cardArr) > 0)
      $data[$index]['card']       = array_sum($cardArr);
      else
      $data[$index]['card']       = null;

      if(array_sum($internetBankingArr) > 0)  
      $data[$index]['internetBanking']  = array_sum($internetBankingArr);
      else
      $data[$index]['internetBanking']  = null;

      if(array_sum($cashOnDeliveryArr) > 0) 
      $data[$index]['cashOnDelivery']   = array_sum($cashOnDeliveryArr);
      else
      $data[$index]['cashOnDelivery']   = null;
      
      if(array_sum($netpluspayArr) > 0) 
      $data[$index]['netpluspay']     = array_sum($netpluspayArr);
      else
      $data[$index]['netpluspay']     = null;

      if(array_sum($interswitchngArr) > 0) 
      $data[$index]['interswitchng']     = array_sum($interswitchngArr);
      else
      $data[$index]['interswitchng']     = null;

    }
    
    
  } 
  //print_r($data);
  
}

?>
<script type="text/javascript">
$(function() {

    // Area Chart
    Morris.Area({
        element: 'morris-area-chart',
        data: <?php echo json_encode($data); ?>,
        xkey: 'period',
        ykeys: ['card', 'internetBanking', 'cashOnDelivery', 'netpluspay','interswitchng'],
        labels: ['Card', 'Internet Banking', 'Cash On Delivery', 'Netpluspay', 'Interswitchng'],
        //pointSize: 2,
        hideHover: 'auto',
        resize: true,
        parseTime:false
    });  

});
</script>
<?php include("footer.php")?>

<?php //SELECT payment, price, date FROM `cart` WHERE payment ='Netpluspay' AND date BETWEEN '2015-04-14 00:00:01' AND '2015-06-14 00:00:59'
//SELECT SUM(price) FROM `cart` WHERE payment ='Netpluspay' AND date BETWEEN '2014-12-01 00:00:01' AND '2015-02-28 00:00:59'
?>