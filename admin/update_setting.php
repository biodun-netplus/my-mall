<?php 
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */
//error_reporting(E_ALL);

$main 	= new users();
$id 	= $_SESSION['id'];  
$result = $main->viewprofile($id ,"admin");  
if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{    
	//print_r($_REQUEST);die;
	
 	$password1	=	$result->password;
	if($password1==md5(mysql_real_escape_string($_REQUEST['oldpassword'])))
	{
		if(strlen($_REQUEST['newpassword']) < 7)
		{
			$show = "Password must be grater than 7 characters!";
		}
		else if( !preg_match("#[a-z]+#", $_REQUEST['newpassword']) ) {
			$show = "Password must include at least one letter!";
		}
		else if( !preg_match("#[A-Z]+#", $_REQUEST['newpassword']) ) {
			$show = "Password must include at least one CAPS!";
		}
		else if( !preg_match("#\W+#", $_REQUEST['newpassword']) ) {
			$show = "Password must include at least one symbol!";
		}
		else{
			$data['password'] 		= md5(mysql_real_escape_string($_REQUEST['newpassword']));
			$data['email_id'] 		= mysql_real_escape_string($_REQUEST['Email']);
		 	$main->Changepassword($data,'admin',$id);
		 	
			$to =$_REQUEST['Email'];
			//$to =$_REQUEST['Email'].',wolef77@yahoo.com';
			$subject = "Password Successfully changed";
			$headers = 'From:  info@mymall.com.ng' . "\r\n" .
			    	   'Reply-To:  info@mymall.com.ng' . "\r\n" .
			    	   'X-Mailer: PHP/' . phpversion();
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			$message1 .='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
						<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
</head>
<body style="background-color: #f3f3f3; padding: 0; margin: 0;font-family: Calibri, Arial, sans-serif;">
<div style="margin: 0 auto; width:600px;background-color: #fff;">
    <table width="100%">
        <tr>
            <td colspan="6">
                <div style="background-color: #00587C;
                        width:100%; height:100px; border-bottom: solid 3px #9AC434; padding:20px 0">
                    <table>
                        <tr>
                            <td width="5%"></td>
                            <td width="60%">
                                <h1 style="color:#fff; font-size:36px; font-weight: normal">Congratulations!</h1>
                            </td>
                            <td width="30%">
                                <img src="http://www.mymall.com.ng/assets/img/logo.png" />
                            </td>
                            <td width="5%"></td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>

        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <p>
                    Dear <strong>'.$result->username.'</strong>,<br /><br />
                    Thank you for registering your business with Mymall, your store details are listed below.
                </p>
            </td>
        </tr>

        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <table width="100%">
                    <tr>
                        <td width="20%" height="30">Username</td>
                        <td width="60%">
                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
                            padding: 0 10px;">
                                '.$result->email_id.'
                           </div>
                        </td>
                        <td width="20%"></td>
                    </tr>
                    <tr>
                        <td width="20%" height="30">Password</td>
                        <td width="60%">
                            <div style="background-color:#00587C; color: #fff; height:30px; line-height:30px;
                            padding: 0 10px;">
                                '.$_REQUEST['newpassword'].'
                           </div>
                        </td>
                        <td width="20%"></td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <div style="border: solid 1px #ccc; background-color: #f3f3f3; padding: 15px;">
                    <p>What happens next...</p>
                    <p>Your store is now open and can be visible to all customers at
                        <strong>http://www.mymall.com.ng/'.$result->username.'</strong></p>
                    <ol>
                        <li>Customize your store</li>
                        <li>Upload more products</li>
                        <li>Start Selling</li>
                    </ol>
                </div>
            </td>
        </tr>

        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <div style="background-color:#9AC434; height:40px; width:45%; text-align:center; color:#fff;
                line-height: 40px; float: left">LOGIN TO YOUR ACCOUNT</div>

                <div style="background-color:#eec400; height:40px; width:45%; text-align:center; color:#fff;
                line-height: 40px; float: right">HOW IT WORKS</div>
            </td>
        </tr>

        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <p style="text-align: center">
                    For more information, call 0818 778 2542, 0818 778 2542 or send an email to info@mymall.com.ng
                </p>
            </td>
        </tr>

        <tr>
            <td colspan="6" style="padding:10px 20px;">
                <table width="100%">
                    <tr>
                        <td width="20%"><img src="http://www.mymall.com.ng/assets/img/ecobank.png" /></td>
                        <td width="20%"></td>
                        <td width="20%"></td>
                        <td width="20%"><img src="http://www.mymall.com.ng/assets/img/fedex.png" height="25px" width="auto" /></td>
                        <td width="20%"><img src="http://www.mymall.com.ng/assets/img/webmall.jpg" height="25px" width="auto" /></td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td colspan="6" style="padding:10px 20px; border-top: solid 1px #ccc;">
                <table width="100%">
                    <tr>
                        <td width="60%">
                            <p style="font-size:13px; color: #333;line-height: 30px">
                                &copy; 2015, All rights reserved. Mymall
                            </p>
                        </td>

                        <td width="40%">
                            <img src="http://www.mymall.com.ng/assets/img/youtube.jpg" style="float:right; margin-left:5px;" />
                            <img src="http://www.mymall.com.ng/assets/img/linkin.jpg" style="float:right; margin-left: 5px;" />
                            <img src="http://www.mymall.com.ng/assets/img/twitter.jpg" style="float:right; margin-left: 5px;" />
                            <img src="http://www.mymall.com.ng/assets/img/facebook.jpg" style="float:right; margin-left: 5px;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>


</body>
</html>';
			mail($to, $subject, $message1, $headers);
			
			header("Location:settings.php?msg=c_password");	
		}

	}
	else
	{
		$show ="Old password did  not match";
	}
}
 ?>
 <script type="text/javascript">
  $(document).ready(function(){
	  $('#submit').click(function(){
		  	var a = $("#Email").val();
			var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
			if(a=='')
			{
				alert("Please enter email");
				$('#Email').focus();
				return false;
			}
			else if(!filter.test(a)){
				alert("Please enter valid email");
				$('#Email').focus();
				return false;
			}
		  	var oldpassword=$('#oldpassword').val()
			if(oldpassword=='')
			{
				alert("please enter the old password");
				$('#oldpassword').focus();
				return false;
			}
			var newpassword=$('#newpassword').val();
			if(newpassword=='')
			{
				alert("Please enter the new password");
				$('#newpassword').focus();
				return false;
			}
			var confirmPassword=$('#confirmPassword').val();
			if(confirmPassword=='')
			{
				alert("Please enter the confirm password");
				$('#confirmPassword').focus();
				return false;
			}
		  
		  var newpassword=$('#newpassword').val();
		  var confirmPassword=$('#confirmPassword').val();
		  if(newpassword!==confirmPassword)
		  {
			  alert("New Password And Confirm password did not match please enter the same password")
			  $('#newpassword').focus();
			  return false;
		  }
		  return true;
	
		  })
	  })
</script>

 <div class="clear"></div>
 
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content" style="">
<div>
<div id="page-heading"><h1>Update Admin Password</h1></div>
<div style="float:right; padding-right:20px;"><a href="settings.php"><input type="button" class="buttons btn btn-info" value="Back" /></a></div>
</div>


<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
		
				<!--  start message-red -->
				<!--  end message-green -->
		 
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td align="left">
	
	
		<!--  start step-holder -->
		<!--  end step-holder -->
        <!-- start id-form -->
        <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post">
		<table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
		
		<?php if(isset($show)){ ?><tr>
        <td>&nbsp;</td><td style="color:#F00"> <?php echo  $show;?></td></tr><?php  } ?>
		<tr>
			<th valign="top">Email:</th>
			<td><input type="text" class="form-control" value="<?php echo $result->email_id; ?>" id="Email" name="Email" autocomplete="off" /></td>
			</tr>	
		<tr>
			<th valign="top">Old password:</th>
			<td><input type="password" class="form-control" id="oldpassword" name="oldpassword" autocomplete="off" /></td>
			</tr>
            <tr>
			<th valign="top">New password :</th>
			<td><input type="password" class="form-control" id="newpassword" name="newpassword" autocomplete="off" /></td>
			</tr>
			<tr>
			<th valign="top">Confirm password:</th>
			<td><input type="password" class="form-control" id="confirmPassword" name="confirmPassword"  autocomplete="off" /></td>
			</tr>
	<tr>
	
		<th>&nbsp;</th>
		<td valign="top">
			<input type="submit" name="submit" id="submit" value="submit" class="form-submit btn btn-default" />
			<input type="reset" value="Reset" class="form-reset btn btn-default"  />		</td>
		</tr>
	</table>
   		 </form>
	<!-- end id-form  -->	</td>
	
</tr>
<tr>
<td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
	
	<div class="clear"></div>
 

</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
    <?php include("footer.php")?>