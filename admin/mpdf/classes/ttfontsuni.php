<?php

/*******************************************************************************
* TTFontFile class                                                             *
*                                                                              *
* Version:  1.04		                                                       *
* Date:     2010-08-20                                                         *
* Author:   Ian Back <ianb@bpm1.com>                                           *
* License:  LGPL                                                               *
* Copyright (c) Ian Back, 2010                                                 *
* This class is based on The ReportLab Open Source PDF library                 *
* written in Python - http://www.reportlab.com/software/opensource/            *
* together with ideas from the OpenOffice source code and others.              * 
* This header must be retained in any redistribution or                        *
* modification of the file.                                                    *
*                                                                              *
*******************************************************************************/

// Define the value used in the "head" table of a created TTF file
// 0x74727565 "true" for Mac
// 0x00010000 for Windows
// Either seems to work for a font embedded in a PDF file
// when read by Adobe Reader on a Windows PC(!)
define("_TTF_MAC_HEADER", false);

// Recalculate correct metadata/profiles when making subset fonts (not SIP/SMP)
// e.g. xMin, xMax, maxNContours
if (!defined('_RECALC_PROFILE')) define("_RECALC_PROFILE", false);

// TrueType Font Glyph operators
define("GF_WORDS",(1 << 0));
define("GF_SCALE",(1 << 3));
define("GF_MORE",(1 << 5));
define("GF_XYSCALE",(1 << 6));
define("GF_TWOBYTWO",(1 << 7));



class TTFontFile {

var $panose;	// mPDF 5.0
var $maxUni;
var $sFamilyClass;
var $sFamilySubClass;
var $sipset;
var $smpset;
var $_pos;
var $numTables;
var $searchRange;
var $entrySelector;
var $rangeShift;
var $tables;
var $otables;
var $filename;
var $fh;
var $glyphPos;
var $charToGlyph;
var $ascent;
var $descent;
var $name;
var $familyName;
var $styleName;
var $fullName;
var $uniqueFontID;
var $unitsPerEm;
var $bbox;
var $capHeight;
var $stemV;
var $italicAngle;
var $flags;
var $underlinePosition;
var $underlineThickness;
var $charWidths;
var $defaultWidth;
var $maxStrLenRead;
var $numTTCFonts;
var $TTCFonts;
var $maxUniChar;

	function TTFontFile() {
		$this->maxStrLenRead = 200000;	// Maximum size of glyf table to read in as string (otherwise reads each glyph from file)
	}


	function getMetrics($file, $TTCfontID=0, $debug=false, $BMPonly=false) {	// mPDF 5.0
		$this->filename = $file;
		$this->fh = fopen($file,'rb') or die('Can\'t open file ' . $file);
		$this->_pos = 0;
		$this->charWidths = '';
		$this->glyphPos = array();
		$this->charToGlyph = array();
		$this->tables = array();
		$this->otables = array();
		$this->ascent = 0;
		$this->descent = 0;
		$this->numTTCFonts = 0;
		$this->TTCFonts = array();
		$this->version = $version = $this->read_ulong();
		$this->panose = array();	// mPDF 5.0
		// mPDF 5.0.05
		if ($version==0x4F54544F) 
			die("Postscript outlines are not supported");
		if ($version==0x74746366 && !$TTCfontID) 
			die("ERROR - You must define the TTCfontID for a TrueType Collection in config_fonts.php (". $file.")");
		if (!in_array($version, array(0x00010000,0x74727565)) && !$TTCfontID)
			die("Not a TrueType font: version=".$version);
		if ($TTCfontID > 0) {
			$this->version = $version = $this->read_ulong();	// TTC Header version now
			if (!in_array($version, array(0x00010000,0x00020000)))
				die("ERROR - Error parsing TrueType Collection: version=".$version." - " . $file);
			$this->numTTCFonts = $this->read_ulong();
			for ($i=1; $i<=$this->numTTCFonts; $i++) {
	      	      $this->TTCFonts[$i]['offset'] = $this->read_ulong();
			}
			$this->seek($this->TTCFonts[$TTCfontID]['offset']);
			$this->version = $version = $this->read_ulong();	// TTFont version again now
		}
		$this->readTableDirectory($debug);
		$this->extractInfo($debug, $BMPonly); 	// mPDF 5.0.03 Added SMP
		fclose($this->fh);
	}


	function readTableDirectory($debug=false) {
	    $this->numTables = $this->read_ushort();
            $this->searchRange = $this->read_ushort();
            $this->entrySelector = $this->read_ushort();
            $this->rangeShift = $this->read_ushort();
            $this->tables = array();	
            for ($i=0;$i<$this->numTables;$i++) {
                $record = array();
                $record['tag'] = $this->read_tag();
                $record['checksum'] = array($this->read_ushort(),$this->read_ushort());
                $record['offset'] = $this->read_ulong();
                $record['length'] = $this->read_ulong();
                $this->tables[$record['tag']] = $record;
		}
		if ($debug) $this->checksumTables();
	}

	function checksumTables() {
		// Check the checksums for all tables
		foreach($this->tables AS $t) {
		  if ($t['length'] > 0 && $t['length'] < $this->maxStrLenRead) {	// 1.02
            	$table = $this->get_chunk($t['offset'], $t['length']);
            	$checksum = $this->calcChecksum($table);
            	if ($t['tag'] == 'head') {
				$up = unpack('n*', substr($table,8,4));
				$adjustment[0] = $up[1];
				$adjustment[1] = $up[2];
            		$checksum = $this->sub32($checksum, $adjustment);
			}
            	$xchecksum = $t['checksum'];
            	if ($xchecksum != $checksum) 
            	    die(sprintf('TTF file "%s": invalid checksum %s table: %s (expected %s)', $this->filename,dechex($checksum[0]).dechex($checksum[1]),$t['tag'],dechex($xchecksum[0]).dechex($xchecksum[1])));
		  }
		}
	}

	function sub32($x, $y) {
		$xlo = $x[1];
		$xhi = $x[0];
		$ylo = $y[1];
		$yhi = $y[0];
		if ($ylo > $xlo) { $xlo += 1 << 16; $yhi += 1; }
		$reslo = $xlo-$ylo;
		if ($yhi > $xhi) { $xhi += 1 << 16;  }
		$reshi = $xhi-$yhi;
		$reshi = $reshi & 0xFFFF;
		return array($reshi, $reslo);
	}

	function calcChecksum($data)  {
		if (strlen($data) % 4) { $data .= str_repeat("\0",(4-(strlen($data) % 4))); }
		$hi=0x0000;
		$lo=0x0000;
		for($i=0;$i<strlen($data);$i+=4) {
			$hi += (ord($data[$i])<<8) + ord($data[$i+1]);
			$lo += (ord($data[$i+2])<<8) + ord($data[$i+3]);
			$hi += $lo >> 16;
			$lo = $lo & 0xFFFF;
			$hi = $hi & 0xFFFF;
		}
		return array($hi, $lo);
	}

	function get_table_pos($tag) {
		$offset = $this->tables[$tag]['offset'];
		$length = $this->tables[$tag]['length'];
		return array($offset, $length);
	}

	function seek($pos) {
		$this->_pos = $pos;
		fseek($this->fh,$this->_pos);
	}

	function skip($delta) {
		$this->_pos = $this->_pos + $delta;
		fseek($this->fh,$this->_pos);
	}

	function seek_table($tag, $offset_in_table = 0) {
		$tpos = $this->get_table_pos($tag);
		$this->_pos = $tpos[0] + $offset_in_table;
		fseek($this->fh, $this->_pos);
		return $this->_pos;
	}

	function read_tag() {
		$this->_pos += 4;
		return fread($this->fh,4);
	}

	function read_short() {
		$this->_pos += 2;
		$s = fread($this->fh,2);
		$a = (ord($s[0])<<8) + ord($s[1]);
		if ($a & (1 << 15) ) { 
			$a = ($a - (1 << 16)); 
		}
		return $a;
	}

	// mPDF 5.0
	function unpack_short($s) {
		$a = (ord($s[0])<<8) + ord($s[1]);
		if ($a & (1 << 15) ) { 
			$a = ($a - (1 << 16)); 
		}
		return $a;
	}

	function read_ushort() {
		$this->_pos += 2;
		$s = fread($this->fh,2);
		return (ord($s[0])<<8) + ord($s[1]);
	}

	function read_ulong() {
		$this->_pos += 4;
		$s = fread($this->fh,4);
		// if large uInt32 as an integer, PHP converts it to -ve
		return (ord($s[0])*16777216) + (ord($s[1])<<16) + (ord($s[2])<<8) + ord($s[3]); // 	16777216  = 1<<24
	}

	function get_ushort($pos) {
		fseek($this->fh,$pos);
		$s = fread($this->fh,2);
		return (ord($s[0])<<8) + ord($s[1]);
	}

	function get_ulong($pos) {
		fseek($this->fh,$pos);
		$s = fread($this->fh,4);
		// iF large uInt32 as an integer, PHP converts it to -ve
		return (ord($s[0])*16777216) + (ord($s[1])<<16) + (ord($s[2])<<8) + ord($s[3]); // 	16777216  = 1<<24
	}

	function pack_short($val) {
		if ($val<0) { 
			$val = abs($val);
			$val = ~$val;
			$val += 1;
		}
		return pack("n",$val); 
	}

	function splice($stream, $offset, $value) {
		return substr($stream,0,$offset) . $value . substr($stream,$offset+strlen($value));
	}

	function _set_ushort($stream, $offset, $value) {
		$up = pack("n", $value);
		return $this->splice($stream, $offset, $up);
	}

	// mPDF 5.0
	function _set_short($stream, $offset, $val) {
		if ($val<0) { 
			$val = abs($val);
			$val = ~$val;
			$val += 1;
		}
		$up = pack("n",$val); 
		return $this->splice($stream, $offset, $up);
	}

	function get_chunk($pos, $length) {
		fseek($this->fh,$pos);
		if ($length <1) { return ''; }	// 1.02
		return (fread($this->fh,$length));
	}

	function get_table($tag) {
		list($pos, $length) = $this->get_table_pos($tag);
		if ($length == 0) { return ''; }	// 1.03
		fseek($this->fh,$pos);
		return (fread($this->fh,$length));
	}

	function add($tag, $data) {
		if ($tag == 'head') {
			$data = $this->splice($data, 8, "\0\0\0\0");
		}
		$this->otables[$tag] = $data;
	}



/////////////////////////////////////////////////////////////////////////////////////////
	function getCTG($file, $TTCfontID=0, $debug=false) {
		$this->filename = $file;
		$this->fh = fopen($file,'rb') or die('Can\'t open file ' . $file);
		$this->_pos = 0;
		$this->charWidths = '';
		$this->glyphPos = array();
		$this->charToGlyph = array();
		$this->tables = array();
		$this->numTTCFonts = 0;
		$this->TTCFonts = array();
		$this->skip(4);
		if ($TTCfontID > 0) {
			$this->version = $version = $this->read_ulong();	// TTC Header version now
			if (!in_array($version, array(0x00010000,0x00020000)))
				die("ERROR - Error parsing TrueType Collection: version=".$version." - " . $file);
			$this->numTTCFonts = $this->read_ulong();
			for ($i=1; $i<=$this->numTTCFonts; $i++) {
	      	      $this->TTCFonts[$i]['offset'] = $this->read_ulong();
			}
			$this->seek($this->TTCFonts[$TTCfontID]['offset']);
			$this->version = $version = $this->read_ulong();	// TTFont version again now
		}
		$this->readTableDirectory($debug);


		// cmap - Character to glyph index mapping table
		$cmap_offset = $this->seek_table("cmap");
		$this->skip(2);
		$cmapTableCount = $this->read_ushort();
		$unicode_cmap_offset = 0;
		for ($i=0;$i<$cmapTableCount;$i++) {
			$platformID = $this->read_ushort();
			$encodingID = $this->read_ushort();
			$offset = $this->read_ulong();
			$save_pos = $this->_pos;
			if ($platformID == 3 && $encodingID == 1) { // Microsoft, Unicode
				$format = $this->get_ushort($cmap_offset + $offset);
				if ($format == 4) {
					$unicode_cmap_offset = $cmap_offset + $offset;
					break;
				}
			}
			else if ($platformID == 0) { // Unicode -- assume all encodings are compatible
				$format = $this->get_ushort($cmap_offset + $offset);
				if ($format == 4) {
					$unicode_cmap_offset = $cmap_offset + $offset;
					break;
				}
			}
			$this->seek($save_pos );
		}

		$glyphToChar = array();
		$charToGlyph = array();
		$this->getCMAP4($unicode_cmap_offset, $glyphToChar, $charToGlyph );

		fclose($this->fh);
		return ($charToGlyph);
	}

/////////////////////////////////////////////////////////////////////////////////////////
	function getTTCFonts($file) {
		$this->filename = $file;
		$this->fh = fopen($file,'rb');
		if (!$this->fh) { return ('ERROR - Can\'t open file ' . $file); }
		$this->numTTCFonts = 0;
		$this->TTCFonts = array();
		$this->version = $version = $this->read_ulong();
		if ($version==0x74746366) {
			$this->version = $version = $this->read_ulong();	// TTC Header version now
			if (!in_array($version, array(0x00010000,0x00020000)))
				return("ERROR - Error parsing TrueType Collection: version=".$version." - " . $file);
		}
		else {
			return("ERROR - Not a TrueType Collection: version=".$version." - " . $file);
		}
		$this->numTTCFonts = $this->read_ulong();
		for ($i=1; $i<=$this->numTTCFonts; $i++) {
	            $this->TTCFonts[$i]['offset'] = $this->read_ulong();
		}
	}


	// Used to get font information from files in directory for mPDF config
	function extractCoreInfo($file, $TTCfontID=0) {
		$this->filename = $file;
		$this->fh = fopen($file,'rb');
		if (!$this->fh) { return ('ERROR - Can\'t open file ' . $file); }
		$this->_pos = 0;
		$this->charWidths = '';
		$this->glyphPos = array();
		$this->charToGlyph = array();
		$this->tables = array();
		$this->otables = array();
		$this->ascent = 0;
		$this->descent = 0;
		$this->numTTCFonts = 0;
		$this->TTCFonts = array();
		$this->version = $version = $this->read_ulong();
		$this->panose = array();	// mPDF 5.0
		if ($version==0x4F54544F) 
			return("ERROR - NOT ADDED as Postscript outlines are not supported - " . $file);
		if ($version==0x74746366) {
			if ($TTCfontID > 0) {
				$this->version = $version = $this->read_ulong();	// TTC Header version now
				if (!in_array($version, array(0x00010000,0x00020000)))
					return("ERROR - NOT ADDED as Error parsing TrueType Collection: version=".$version." - " . $file);
			}
			else return("ERROR - Error parsing TrueType Collection - " . $file);
			$this->numTTCFonts = $this->read_ulong();
			for ($i=1; $i<=$this->numTTCFonts; $i++) {
	      	      $this->TTCFonts[$i]['offset'] = $this->read_ulong();
			}
			$this->seek($this->TTCFonts[$TTCfontID]['offset']);
			$this->version = $version = $this->read_ulong();	// TTFont version again now
			$this->readTableDirectory(false);
		}
		else {
			if (!in_array($version, array(0x00010000,0x74727565)))
				return("ERROR - NOT ADDED as Not a TrueType font: version=".$version." - " . $file);
			$this->readTableDirectory(false);
		}

/* Included for testing...
		$cmap_offset = $this->seek_table("cmap");
		$this->skip(2);
		$cmapTableCount = $this->read_ushort();
		$unicode_cmap_offset = 0;
		for ($i=0;$i<$cmapTableCount;$i++) {
			$x[$i]['platformId'] = $this->read_ushort();
			$x[$i]['encodingId'] = $this->read_ushort();
			$x[$i]['offset'] = $this->read_ulong();
			$save_pos = $this->_pos;
			$x[$i]['format'] = $this->get_ushort($cmap_offset + $x[$i]['offset'] );
			$this->seek($save_pos );
		}
		print_r($x); exit;
*/
		///////////////////////////////////
		// name - Naming table
		///////////////////////////////////
			$name_offset = $this->seek_table("name");
			$format = $this->read_ushort();
			if ($format != 0)
				return("ERROR - NOT ADDED as Unknown name table format ".$format." - " . $file);
			$numRecords = $this->read_ushort();
			$string_data_offset = $name_offset + $this->read_ushort();
			$names = array(1=>'',2=>'',3=>'',4=>'',6=>'');
			$K = array_keys($names);
			$nameCount = count($names);
			for ($i=0;$i<$numRecords; $i++) {
				$platformId = $this->read_ushort();
				$encodingId = $this->read_ushort();
				$languageId = $this->read_ushort();
				$nameId = $this->read_ushort();
				$length = $this->read_ushort();
				$offset = $this->read_ushort();
				if (!in_array($nameId,$K)) continue;
				$N = '';
				if ($platformId == 3 && $encodingId == 1 && $languageId == 0x409) { // Microsoft, Unicode, US English, PS Name
					$opos = $this->_pos;
					$this->seek($string_data_offset + $offset);
					if ($length % 2 != 0)
						$length += 1;
					$length /= 2;
					$N = '';
					while ($length > 0) {
						$char = $this->read_ushort();
						$N .= (chr($char));
						$length -= 1;
					}
					$this->_pos = $opos;
					$this->seek($opos);
				}
				else if ($platformId == 1 && $encodingId == 0 && $languageId == 0) { // Macintosh, Roman, English, PS Name
					$opos = $this->_pos;
					$N = $this->get_chunk($string_data_offset + $offset, $length);
					$this->_pos = $opos;
					$this->seek($opos);
				}
				if ($N && $names[$nameId]=='') {
					$names[$nameId] = $N;
					$nameCount -= 1;
					if ($nameCount==0)