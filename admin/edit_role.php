<?php
include("header.php");
include("activities_arr.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */

$id  = base64_decode($_GET['id']);

if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{    
	  $User                 = array();
    $role 		            = '';  
    $userrole             = '';

    if(isset($_REQUEST['permission']) && $_REQUEST['permission'] != '')
	  {
	 	  $role = implode('|',$_REQUEST['permission']);

      if(isset($_REQUEST['old_role']) && $_REQUEST['old_role'] != '')
      {
        
        $new_element_arr  =  array_diff($_REQUEST['permission'], explode('|', $_REQUEST['old_role']));
        $role1            = implode('|',$new_element_arr);
        echo $role1;
        if(isset($_REQUEST['role_update_by_user']) && $_REQUEST['role_update_by_user'] != '')
        {
          
          $user_arry        = explode('|', $_REQUEST['role_update_by_user'].'|'.$role1);  print_r($user_arry) ;

          for ($i=0; $i < count($user_arry) ; $i++) 
          {

            if(in_array($user_arry[$i], $_REQUEST['permission']))
            {
              $userrole .= $user_arry[$i]."|";
            }
              
          }
          $userrole = substr($userrole , 0, -1); 
          $User['role_update_by_user']  = $userrole;
          /*$user_role        = array_intersect($user_arry, $_REQUEST['permission']);  
          $userrole         = implode('|',$user_role);
          $User['role_update_by_user']  = $userrole;*/
        }

      }

    }
		

	 
	 $User['title']							   = $_REQUEST['title'];
	 $User['description']					 = $_REQUEST['description'];
	 $User['role']							   = $role;
	 $main									       = new users;
	 
	 $insert= $main->update($User,"role",$id); 
	 if($insert)
	 {
		header("location:role.php");
	 }
}

$Query = mysql_query("SELECT * FROM role where id = '".$id."'");
$Data = mysql_fetch_object($Query);
 ?>
<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content" style="">
    <div >
      <div id="page-heading" >
        <h1>Edit Role</h1>
      </div>
      <div style="float:right; padding-right:20px;">
        <input type="button" class="buttons btn btn-info" value="Back" onclick="window.history.back()" />
      </div>
    </div>
    <div class="row">
      <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" id="UserForm">
        <div class="col-lg-5">
          <div class="form-group">
            <label>Title: </label>
            <input type="text" class="form-control" id="title" name="title" value="<?php echo $Data->title;?>" />
          </div>
          <div class="form-group">
            <label>Description: </label>
            <textarea name="description" id="description" class="form-control"><?php echo $Data->description;?></textarea>
            <input type="hidden" name="role_update_by_user" value="<?php echo $Data->role_update_by_user;?>" />
            <input type="hidden" name="old_role" value="<?php echo $Data->role;?>" />
          </div>
          <div class="form-group">
            <input type="submit" id="RoleSubmit" value="submit" name="submit" class="form-submit btn btn-default" />
            <input type="reset" value="Reset" class="form-reset btn btn-default"  />
          </div>
        </div>
        <div class="col-lg-6">
          <h2>Permission</h2>
          <div class="table-responsive" style="overflow-y: auto; height: 352px;">
            <table class="table table-bordered table-hover">
              <tbody>
                <?php
        			  $activPermis =  explode('|', $Data->role);	 
        				
        			  foreach($activitiesArr as $key => $value){
        			   
        			   foreach($activitiesArr[$key] as $key1 => $value1){ 
        			   ?>
                        <tr>
                          <td><input type="checkbox" name="permission[]" value="<?php echo $value1; ?>"  <?php if(in_array($value1,$activPermis)){ echo 'checked="checked"'; } ?>  /></td>
                          <td><?php echo $key1; ?></td>
                          <td><?php echo $key; ?></td>
                        </tr>
                        <?php
        			   }
        			  }
        			  ?>
              </tbody>
            </table>
          </div>
        </div>
      </form>
      <!-- end id-form  --> 
    </div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>