<?php
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */


if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{   $User=array();
	 $User['username']=$_REQUEST['provider_name'];	
	 $User['email_id']=$_REQUEST['provider_email'];
	 $User['password']=$_REQUEST['provider_pass'];
	 $User['contact']=$_REQUEST['contact'];
	 $User['address']=$_REQUEST['address'];
	 $User['admin_type']=$_REQUEST['admin_type'];
	 $main= new users;
	 $insert= $main->ADD($User,"admin");
	 if($insert)
	 {
		header("location:sub-admin.php");
	 }
}
 ?>


<script type="text/javascript" src="js/validation.js"></script>

 
 <div class="clear"></div>
 
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content" style="">


<div >
<div id="page-heading" ><h1>Add Sub Admin</h1></div>
<div style="float:right; padding-right:20px;"><input type="button" class="buttons btn btn-info" value="Back" onclick="goBack()" /></div>
</div>


<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
				
				<!--  start message-red -->
				
				<!--  end message-red -->
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td align="center">
	
	
		<!--  start step-holder -->
		<!--  end step-holder -->
        <!-- start id-form -->
        <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" id="ProviderForm">
		<table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
		<tr>
			<th valign="top">User Name:</th>
			<td><input type="text" class="form-control" id="name" name="provider_name" value="" /></td>
			<td><label class="error"  id="name_error"></label></td>
		</tr>
			<tr>
			<th valign="top"> Email:</th>
			<td><input type="text" class="form-control" id="provider_email" name="provider_email" value="" /></td>
			<td width="310"><label class="error"  id="email"></label></td>
			
			</tr>
			<tr>
			<th valign="top">Password:</th>
			<td><input type="password" class="form-control" id="provider_pass" name="provider_pass" value="" /></td>
			<td><label class="error"  id="password"></label></td>
			</tr>
             
			<tr>
			<th valign="top">Address:</th>
			<td><input type="text" class="form-control" id="provider_address" name="address" value="" /></td>
		<td><label class="error"  id="address"></label></td>
		</tr>
			<tr>
			<th valign="top">Contact:</th>
			<td><input type="text" class="form-control" id="provider_contact" name="contact" value="" /></td>
			<td><label class="error"  id="contact"></label>
			</tr>
            
            <tr>
			<th valign="top">Admin Type:</th>
			<td><select class="inp-select form-control" name="admin_type" id="">
            		<option value="Admin">Admin</option>
                      
                    <option value="Settling officer">Settling officer</option> 
					  
                    <option value="Merchant Manager">Merchant Manager</option> 
					            	</select>
            </td>
			<td><label id="country_error" class="error"></label></td>
			</tr>
	<tr>
    
		<th>&nbsp;</th>
		<td valign="top">
			<input type="submit" id="AdminSubmit" value="submit" name="submit" class="form-submit btn btn-default" />
			<input type="reset" value="Reset" class="form-reset btn btn-default"  />		</td>
		</tr>
	</table>
   		 </form>
	<!-- end id-form  -->	</td>
	
</tr>
<tr>
<td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
	
	<div class="clear"></div>
 

</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>

<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>