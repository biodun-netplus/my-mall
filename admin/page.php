<?php 
include("header.php");
/* Page management */
/* Author @Parmeet Arora */
/* Date : 25-08-2015 */
?>
<!-- start content-outer -->

<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 

<?php
if(isset($_REQUEST['keyword']))
{
  $query = "SELECT COUNT(*) FROM `miscellaneous` WHERE title like '%".$_REQUEST['keyword']."%'  ";
}
else
{
  $query = "SELECT COUNT(*) FROM `miscellaneous`  " ; 
}

$result = mysql_query($query) or die(mysql_error());
$num_rows = mysql_fetch_row($result);
$pages = new Paginator;
$pages->items_total = $num_rows[0];
$pages->mid_range = 4; // Number of pages to display. Must be odd and > 3
//$pages->default_ipp = '10'; 
$pages->paginate();

if(isset($_REQUEST['keyword']))
{
  $query = "SELECT * FROM `miscellaneous` WHERE title like '%".$_REQUEST['keyword']."%'  ";
}
else
{
  $query = "SELECT * FROM `miscellaneous` WHERE 1 $pages->limit";
}
//echo $query;

$result = mysql_query($query) or die(mysql_error());
$countCondition = mysql_num_rows($result);
 ?>
    <div class="row bottom-margin">
      <div id="page-heading">
        <h1>Page</h1>
      </div>
      <div>
      <div class="bottom-margin" style="width: 100%; overflow-y: auto;">
        <form method="post"  action="page.php" >
          <div class="col-lg-4">
            <input type="text" name="keyword" class="form-control" value="" required="required" />
          </div>
          <div class="col-lg-2">
            <input type="submit" class="buttons" value="Search"/>
          </div>
        </form>
      </div>
        <div class="add-button col-lg-6"> <a href="add_page.php">
          <input type="button" class="buttons btn btn-info" value="Add New Page"/>
          </a>
        </div>
        <div id="paging-table" class="col-lg-6"><?php echo $pages->display_pages(); ?></div>
      </div>
    </div>
    <?php if(isset($_SESSION['msg']) && $_SESSION['msg']!='')  {?>
    <div id="message-success" align="center">
      <table border="0" width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td class="green-left"><?php echo $_SESSION['msg'];  $_SESSION['msg'] ='';?><a href=""></a></td>
          <td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif" alt="" /></a></td>
        </tr>
      </table>
    </div>
    <?php } ?>
    <form id="mainform" action="" class="table-responsive">
      <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="table table-bordered table-hover table-striped">
        <tr>
          <th class="table-header-check"><a id="toggle-all" ></a> </th>
          <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Title</a></th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Description </a></th>
          <th width="125" class="table-header-repeat line-left"><a class="arrow_none">Action</a></th>
        </tr>
        <?php
        if($countCondition > 0){
        while($data = mysql_fetch_object($result)){ 
          $detail   = $data->detail;
          
          if(strlen($data->detail) > 50)
          {
            $detail = substr($data->detail, 0, (50-strlen($data->detail)));
            $detail .= "...";
            
          }
        ?>
        <tr id="row-<?php echo $data->id;?>">
          <td width="20"><input  type="checkbox" name="checkbox" value="<?php echo $data->id;?>"/></td>
          <td><?php echo ucfirst($data->title); ?></td>
          <td><?php echo $detail;  ?></td>
          <td><a href="edit_page.php?edit_id=<?php echo $data->id;?>" title="Click to Edit page"> <img src="images/table/action_edit.gif"/></a></td>
        </tr>
        <?php }}else{ ?>
        <tr>
          <td colspan="10" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>
        </tr>
        <?php } ?>
      </table>
      <!--  end product-table................................... -->
    </form>
    <div id="actions-box"> <a href="" class="action-slider"></a>
      <div id="actions-box-slider"> <a href="javascript:;" name="delete" id="delete" class="action-delete" >Delete</a> </div>
      <div class="clear"></div>
    </div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="images/table/action_delete.gif"></button>
            <h4 class="modal-title" id="myModalLabel">Decline Product</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                  <label>Description: </label>
                    <textarea name="declineDescription" id="declineDescription" class="form-control"></textarea>
                    <input type="hidden" name="productId" id="productId" value="">
              </div>  
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary submit-popup">Submit</button>
      </div>
    </div>
  </div>
</div>
<?php include("footer.php")?>