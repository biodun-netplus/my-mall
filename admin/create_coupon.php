<?php
include("header.php");
require_once("includes/classes/upload.php");
/* Create coupon */
/* Author @Parmeet Arora */
/* Date : 22-07-2015 */


if(isset($_POST['submit']))
{

    $obj                      = new users;

    $GeneratedCode            = $obj->GeneratePass(8);     

    $GenCodeDate              = date("Y-m-d H:i:s");

    $GenCode                  = array();

    $GenCode['code']          = $GeneratedCode;  

    $GenCode['status']        = 'Unused';

    $GenCode['start_date']    = $_POST['Gen_date'];

    $GenCode['expire_date']   = $_POST['Exp_date'];

      $GenCode['type']        = htmlspecialchars(mysql_real_escape_string($_POST['Type']), ENT_QUOTES);
      $GenCode['session']     = htmlspecialchars(mysql_real_escape_string($_POST['Session']), ENT_QUOTES);
    $GenCode['value']         = htmlspecialchars(mysql_real_escape_string($_POST['Value']), ENT_QUOTES);
    $GenCode['country']       = htmlspecialchars(mysql_real_escape_string($_POST['Country']), ENT_QUOTES);
     $GenCode['email']       = strtolower(htmlspecialchars(mysql_real_escape_string($_POST['Email']), ENT_QUOTES));
    $GenCode['created']       = $GenCodeDate;
    $value = number_format($GenCode['value']);

    
    if(empty($GenCode['email'])){
         $Add                      = $obj->ADD($GenCode,"reward_coupon");

    }else{
      $GenCode['staff_name']       = htmlspecialchars(mysql_real_escape_string($_POST['Name']), ENT_QUOTES);
       $GenCode['years']       = htmlspecialchars(mysql_real_escape_string($_POST['Years']), ENT_QUOTES);
       $staff_n = strtoupper($GenCode['staff_name']);
      $to = $GenCode['email'];
      $subject = "Mymall coupon";
      $txt = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
</head>


<body>
<b style="font-size:30px;">Congratulations,</b>
<br>
<p>Your mymall coupon has been created.</p>

<h3>HOW TO APPLY COUPON ON mymall.com.ng</h3>
<ol>
<li>Copy coupon code from the gift card below</li>
<li>Click on the use coupon button after you must have filled in your details and selected the items you wish to purchase</li>
<li>Ensure the value of the item(s) you wish to purchase is same value or greater than the coupon value</li>
<li>Paste the coupon code in the provided field and click on redeem</li>
<li>Click on the coupon radio button at the left side of the screen then click on the checkout button</li>
</ol>

<p>COUPON CAN ALSO BE USED OFFLINE BY VISITING MERCHANT STORES</p>

<p>for more enquiry contact 08099990660/01-2265509, folakemi@mymall.com.ng or mymall@ecobank.com.</p>
<table align="center"  style="border-radius: 20px;
  
  background: #1e5799; /* Old browsers */
  background: -moz-linear-gradient(45deg,  #1e5799 0%, #2989d8 50%, #207cca 79%, #7db9e8 100%); /* FF3.6-15 */
  background: -webkit-linear-gradient(45deg,  #1e5799 0%,#2989d8 50%,#207cca 79%,#7db9e8 100%); /* Chrome10-25,Safari5.1-6 */
  background: linear-gradient(45deg,  #1e5799 0%,#2989d8 50%,#207cca 79%,#7db9e8 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=#1e5799, endColorstr=#7db9e8,GradientType=1 ); /* IE6-9 fallback on horizontal gradient */" width="619" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr>
    <td width="15" height="21">&nbsp;</td>
    <td width="181">&nbsp;</td>
    <td width="37">&nbsp;</td>
    <td width="4">&nbsp;</td>
    <td width="183">&nbsp;</td>
    <td width="20">&nbsp;</td>
    <td width="13">&nbsp;</td>
    <td width="137">&nbsp;</td>
    <td width="29">&nbsp;</td>
  </tr>
  <tr>
    <td height="48"></td>
    <td rowspan="2" valign="top"><img src="http://www.webmallnglab.com/mymall/assets/img/maillogo.png" width="181" height="65" border="0" /></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="2" valign="center"><p style="color:#FFFFFF; margin:0; padding:0; text-align:center; font-family:Geneva, Arial, Helvetica, sans-serif; font-size:30px">&#8358;'.$value.'</p></td>
    <td></td>
  </tr>
  <tr>
    <td height="17"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr> 
  <tr>
    <td height="6"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="2"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td colspan="3" rowspan="3" valign="top"><p style="color:#FFFFFF; font-family:Geneva, Arial, Helvetica, sans-serif; font-size:18px">VALIDITY: 90DAYS</p></td>
    <td></td>
  </tr>
  <tr>
    <td height="23"></td>
    <td colspan="2" valign="center"><p style="color:#FFFFFF; font-family:Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0; font-size:19px">Coupon code:<br>'.$GenCode['code'].'</p></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="3"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="10"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="44"></td>
    <td colspan="3" valign="top"><p style="font-family:Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;  color:#FFFFFF; font-size:16px; line-height:0.6cm;">Beneficiary:<br/>
      '.$staff_n.'
    </p></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
  </tr>
  <tr>
    <td height="30"></td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="44"></td>
    <td colspan="6" valign="top"><p style="font-family:Geneva, Arial, Helvetica, sans-serif; color:#FFFFFF; margin:0; padding:0; font-size:14px; line-height:0.6cm;">This Gift Card can be used for purchases at <a style="color:#E3F45C; text-decoration:none" href="http://www.mymall.com.ng">mymall.com.ng</a></p></td>
    <td rowspan="1" valign="top"><img src="http://www.webmallnglab.com/mymall/assets/img/mail_logo.png" width="120" height="55" border="0" /></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td height="17"></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>
</body>
</html>
';
     $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= "From: coupons@mymall.com.ng \r\n";
    $headers .= 'Cc: mymall@ecobank.com ' . "\r\n";
    $headers .= 'Bcc: unwana.ekanem@netplusadvisory.com ' . "\r\n";
    $headers .= 'Bcc: wole@netplusadvisory.com' . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";

       mail($to,$subject,$txt,$headers);
      // if(mail($to,$subject,$txt,$headers)){
      //   echo "sent";
      // }else{
      //   echo "nope";
      // }

        $Add                      = $obj->ADD($GenCode,"reward_coupon");
       
    }
   
   



    if($Add)
    {
      // $mail = $obj->send_mail();
       
    
         $_SESSION['msg'] = "Coupon code successfully generated";

      header("location:coupon.php");
   
     

    }

}

?>
<script src="js/jquery/custom_jquery.js" type="text/javascript"></script>

<!-- MUST BE THE LAST SCRIPT IN <HEAD></HEAD></HEAD> png fix -->
<script type="text/javascript" src="calender/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="calender/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="calender/timepicker.mod.js"></script>
<link href="calender/jquery-ui-sunny.css" rel="stylesheet" type="text/css" />
<script src="js/jquery/jquery.pngFix.pack.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/datePicker.css" type="text/css" />
<script type="text/javascript">
$(document).ready(function(){
    $('.datefrom,.dateto123').datepicker({
      numberOfMonths: 1,
      showButtonPanel: true,
      // beforeShow: customRangeNew,
      dateFormat: 'yy-mm-dd',
      duration: '',
      showTime: false,
      constrainInput: false,
      showOn: 'both',
      buttonImage: 'images/calendar_icon.png',
      buttonImageOnly: true,
      buttonText: 'Show Date Picker'
    });

});

</script>
<style type="text/css">
.ui-datepicker-trigger{
  float: right;
  margin-right: 5px;
  margin-top: -5.8%;
  position: relative;
}  
</style>
<script type="text/javascript" src="js/validation.js"></script>

<div class="clear"></div>

<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content" style="">
    <div >
      <div id="page-heading" >
        <h1>Generate Coupon</h1>
      </div>
      <div style="float:right; padding-right:20px;">
        <input type="button" class="buttons btn btn-info" value="Back" onclick="goBack()" />
      </div>
    </div>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
      <tr>
        <th rowspan="3" class="sized"></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><!--  start content-table-inner -->
          
          <div id="content-table-inner"> 
            
            <!--  start message-red --> 
            
            <!--  end message-red -->
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
              <tr valign="top">
                <td align="left"><!--  start step-holder --> 
                  <!--  end step-holder --> 
                  <!-- start id-form -->
                  
                  <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" id="ProviderForm" enctype="multipart/form-data">
                    <table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
                      <tr>
                        <th valign="top">Generate Date:</th>
                        <td><input type="text" class="form-control datefrom" id="Gen_date" name="Gen_date" value="" /></td>
                        <td width="301"><label class="error"  id="Gen_date_error"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Expire Date : </th>
                        <td><input type="text" class="form-control dateto123" id="DropoffDate" name="Exp_date" value="" /></td>
                        <td><label class="error"  id="Exp_date_error"></label></td>
                      </tr>
                      <tr>
                       <th valign="top">Value Type : </th>
                        <td>
                          <select class="inp-select form-control" id="Type" name="Type" >
                            <option value="">Select Type</option>
                            <option value="V">Value</option>
                            <option value="P">Percentage</option>
                          </select>
                        </td>
                        <td><label class="error"  id="Type_error"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Coupon Value : </th>
                        <td><input type="text" class="form-control" id="Value" name="Value" value="" /></td>
                        <td><label class="error"  id="Value_error"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Country : </th>
                        <td>
                          <select class="inp-select form-control" id="Country" name="Country" >
                            <?php 
                            $query = mysql_query("select * from countries");
                            while($data = mysql_fetch_object($query)){ ?>
                            <option value="<?php echo $data->id ?>"><?php echo $data->name; ?></option>
                            <?php } ?>
                          </select>
                        </td>
                        <td><label class="error"  id="Country_error"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Coupon Session : </th>
                        <td>
                          <select class="inp-select form-control" id="Session" name="Session" >
                            <option value="">Select Session</option>
                            <option value="S">One Time</option>
                            <option value="M">Multiple</option>
                          </select>
                          </td>
                        <td><label class="error"  id="Session_error"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Recipient Name : </th>
                        <td><input type="text" class="form-control" id="Value" name="Name" value="" /></td>
                        <td><label class="error"  id="Value_error"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Email Address : </th>
                        <td><input type="email" class="form-control" id="Value" name="Email" value="" /></td>
                        <td><label class="error"  id="Value_error"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Number of Service Years : </th>
                        <td><input type="text" class="form-control" id="Value" name="Years" value="" /></td>
                        <td><label class="error"  id="Value_error"></label></td>
                      </tr>
                      <tr>
                        <th>&nbsp;</th>
                        <td valign="top"><input type="submit" id="couponSubmit" value="submit" name="submit" class="form-submit btn btn-default" />
                          <input type="reset" value="Reset" class="form-reset btn btn-default"  /></td>
                      </tr>
                    </table>
                  </form>
                  
                  <!-- end id-form  --></td>
              </tr>
              <tr>
                <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                <td></td>
              </tr>
            </table>
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>

<?php include("footer.php")?>
