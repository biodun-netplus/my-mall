<?php
/**
 * Created by PhpStorm.
 * User: May
 * Date: 08/05/2015
 * Time: 11:07
 */

class csv {
    static function array2csv($fedexDelivery,$fidelityAmount,$netplusAmount)
    {

        $prod = $_SESSION['settlement'] ;

        ob_start();
        $df = fopen("php://output", 'w');
        fputcsv($df, array('Ecobank','',number_format($fidelityAmount,2)));
        fputcsv($df, array('Fedex','',number_format($fedexDelivery,2)));
        fputcsv($df, array('Netplus','',number_format($netplusAmount,2)));
        fputcsv($df, array('','',''));
        fputcsv($df, array('','',''));

        fputcsv($df, array('Beneficiary','Account Number','Amount'));
        foreach ($prod as $product) {
            fputcsv($df, $product);
        }
        fclose($df);
        return ob_get_clean();
    }


    static function download_send_headers($filename) {
        // disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

        // force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

        // disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }

}