<?php
include("../includes/classes/functions.php");


header( 'Content-Type: text/csv' );
$file_name = "settlement_". str_replace(' ','_',$_REQUEST['type']) ."_from_" . $_REQUEST['startDate'] . "_to_". $_REQUEST['endDate'] .".csv";
        header( 'Content-Disposition: attachment;filename='.$file_name );

$query = "SELECT * FROM `order`
    INNER JOIN cart ON (`order`.order_id = cart.order_id)
    INNER JOIN products ON (cart.product_id = products.id)
    INNER JOIN users ON (products.pid = users.id)
    where `order`.`created` < '" . htmlspecialchars($_REQUEST['endDate']
    , ENT_QUOTES)." 00:00:00' AND `order`.`created` > '" . htmlspecialchars($_REQUEST['startDate'], ENT_QUOTES)
    . " 00:00:00' AND `order`.payment_type = '"
    . htmlspecialchars($_REQUEST['type'], ENT_QUOTES)
    ."' AND `order`.status = 'Completed'";
//echo $query;

$paymentSharePercentage = 0.985;
$splitSharePercentage = 0.97;
$me = array();
$result = mysql_query( $query) or die(mysql_error());
while ($data = mysql_fetch_object($result)) {
//           var_dump($data);
    $sum_total = $data->total_ammount;
    $transaction_date = $data->created;
    $transaction_ref = $data->transaction_ref;
    $account_no = $data->account_type;
    $merchant_name = $data->first_name;

    $interswitch_amount = ((1.5/100)*$sum_total);


    $fedex_amount = $data->delivery_amt;
    $actual_settlement = $sum_total - $interswitch_amount;
    $settledAmount  = ($actual_settlement-$fedex_amount);
    $merchant_amount  = ((97/100)*$settledAmount);
    $shared_amount = ((3/100)*$settledAmount);
    $eco_amount  = ((70/100)*$shared_amount);
    $netplus_amount  = ((30/100)*$shared_amount);


    $me [] = ['Transaction Date'=>$transaction_date,
        'Transaction Ref'=>$transaction_ref,
        'Merchant Account No' => $account_no,
        'My Mall Transaction' => $ecoAmount,
        'Actual Settlement' => $actual_settlement,
        'interswitch 1.5% on fedex' => $interswitch_amount,
        'Fedex ' => $fedex_amount,
        'Fee ' => $shared_amount,
        'Net To Be Settled To Merchant' => $merchant_amount,
        'Netplus ' => $netplus_amount,
        'Ecobank ' => $eco_amount,
        'Account No ' => $account_no,
        'Name' => $merchant_name];
//    var_dump($me);
}

$i = 0;
foreach($me as $key=>$value)
{

    if($i == 0)
    {
        echocsv( array_keys( $value ) );
    }



    echocsv( $value );
    $row = mysql_fetch_assoc( $result );



    $i++;
}



if ( $row )
{
    echocsv( array_keys( $row ) );
}
while ( $row )
{
    echocsv( $row );
    $row = mysql_fetch_assoc( $result );
}
function echocsv( $fields )
{
    $separator = '';
    foreach ( $fields as $field )
    {
        if ( preg_match( '/\\r|\\n|,|"/', $field ) )
        {
            $field = '"' . str_replace( '"', '""', $field ) . '"';
        }
        echo $separator . $field;
        $separator = ',';
    }
    echo "\r\n";
}

