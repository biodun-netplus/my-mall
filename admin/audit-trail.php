<?php 
include("header.php");  
/* User management */
/* Author @parmeet  arora */
/* Date : 9-06-2015 */
//error_reporting(E_ALL);
?>
<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 
  <?php
  if(isset($_REQUEST['keyword'])){
    
    $query = "SELECT COUNT(*) FROM `audit_trail` AS aut WHERE activity like '%".$_REQUEST['keyword']."%' ";
    
  }
  else{
    $query = "SELECT COUNT(*) FROM `audit_trail`";
    
  }
  $result = mysql_query($query) or die(mysql_error());
  $num_rows = mysql_fetch_row($result);
  
  $pages = new Paginator;
  $pages->items_total = $num_rows[0];
  $pages->mid_range = 4; // Number of pages to display. Must be odd and > 3
  $pages->default_ipp = '20'; 
  $pages->paginate();
  if(isset($_REQUEST['keyword'])){
    
    
    $query = "SELECT a.username , aut.* FROM `audit_trail` AS aut LEFT JOIN `admin` AS a ON aut.user_id = a.id  WHERE aut.activity like '%".$_REQUEST['keyword']."%' order by aut.id DESC $pages->limit";
    
  }
  else{
    $query = "SELECT a.username , aut.* FROM `audit_trail` AS aut LEFT JOIN `admin` AS a ON aut.user_id = a.id order by aut.id DESC $pages->limit";
    
  }

  
  $result = mysql_query($query) or die(mysql_error());
  $countCondition = mysql_num_rows($result);
  ?>
    <div class="row bottom-margin">
      <div id="page-heading" >
        <h1>Addit Trail</h1>
      </div>
      <div class="col-lg-61 bottom-margin">
        <form method="post"  action="audit-trail.php" >
          <div class="col-lg-4">
            <input type="text" name="keyword" class="form-control" value="" required="required" />
          </div>
          <div class="col-lg-2">
            <input type="submit" class="buttons" value="Search"/>
          </div>
        </form>
      </div>
      <div id="paging-table" class="col-lg-6"><?php echo $pages->display_pages(); ?></div>
    </div>
    
    <form id="mainform" action="" class="table-responsive">
      <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="table table-bordered table-hover table-striped">
        <tr>
          <th class="table-header-check"><a id="toggle-all" ></a> </th>
          <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">User Name</a> </th>
          <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Activity</a> </th>
          <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">IP</a> </th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Date Time </a></th>
          <th  class="table-header-repeat line-left ">Action</th>
          </tr>
        <?php
    if($countCondition > 0){
      $row_i = 0;
      while($data = mysql_fetch_object($result)){ 
    ?>
        <tr>
          <td width="20"><input  type="checkbox" name="checkbox" value="<?php echo $data->id;?>"/></td>
          <td><?php echo ucfirst($data->username); ?></td>
          <td><?php echo $data->activity; ?></td>
          <td><?php echo $data->ip; ?></td>
          <td><?php echo $data->datetime ?></td>
          <td><?php if(strpos($data->activity, 'Update') !== false){ ?><a href="javascript:showDetails('id<?php echo $row_i ;?>')" title="View Details"><img width="24" height="24" alt="" src="images/table/Gnome-Logviewer-32.png"></a> <?php } ?> </td>
        </tr>
        <?php if(strpos($data->activity, 'Update') !== false){ ?>
        <tr id="id<?php echo $row_i ;?>" class="transDetails" style="display: none; background-color:#ddd;">
          <td colspan="8" style="text-align: left; vertical-align: top;"> <table class="table table-bordered table-hover table-striped">
        	<?php
          $tableName  = "";
        	/** role **/
    			if($data->activity == 'Update Role' && $data->field_id > 0){
            $tableName       = "role";
          ?>
          <tbody>
            <tr>
              <th>Role</th>
              <th>Title</th>
              <th>Description</th>
            </tr>
          </tbody>
            <?php
            $result1 = mysql_query("SELECT * FROM `".$tableName."` WHERE id = (SELECT id FROM `".$tableName."_log` WHERE log_id = ".$data->field_id.") ");
            $record  = mysql_fetch_object($result1);
            echo "<tr  style='background-color: #f0ad4e;'>"; 
            echo  "<td>".$record->role."</td>";
            echo  "<td>".$record->title."</td>";
            echo  "<td>".$record->description."</td>";
            echo "</tr>"; 
            $result2 = mysql_query("SELECT * FROM `".$tableName."_log` WHERE log_id = ".$data->field_id);
            if(mysql_num_rows($result2) > 0)
            {
              $record1  = mysql_fetch_object($result2);
              echo "<tr>"; 
              echo  "<td>".$record1->role."</td>";
              echo  "<td>".$record1->title."</td>";
              echo  "<td>".$record1->description."</td>";
              echo "</tr>"; 
            }
            
    			}
    			
    			/** admin user **/
    			if($data->activity == 'Update Sub admin' ){
    				$tableName				= "admin";
          ?>
          <tbody>
            <tr>
              <th>Username</th>
              <th>Email</th>
              <th>Contact</th>
              <th>Address</th>
              <th>Role</th>
            </tr>
          </tbody>
            <?php
            $result1 = mysql_query("SELECT * FROM `".$tableName."` WHERE id = (SELECT id FROM `".$tableName."_log` WHERE log_id = ".$data->field_id.") ");
            $record  = mysql_fetch_object($result1);
            echo "<tr  style='background-color: #f0ad4e;'>"; 
            echo  "<td>".$record->username."</td>";
            echo  "<td>".$record->email_id."</td>";
            echo  "<td>".$record->contact."</td>";
            echo  "<td>".$record->address."</td>";

            if($record->role != "")
            {
              $result3 =  mysql_query("SELECT * FROM `role` WHERE `id` = '".$record->role."' ");
              $record2 = mysql_fetch_object($result3);
              echo  "<td>".$record2->title."</td>";
            }
            else
            {
              echo  "<td>".$record->role."</td>";
            }
            echo "</tr>"; 
            $result2 = mysql_query("SELECT * FROM `".$tableName."_log` WHERE log_id = ".$data->field_id);
            if(mysql_num_rows($result2) > 0)
            {
              $record1  = mysql_fetch_object($result2);
              echo "<tr>"; 
              echo  "<td>".$record1->username."</td>";
              echo  "<td>".$record1->email_id."</td>";
              echo  "<td>".$record1->contact."</td>";
              echo  "<td>".$record1->address."</td>";
              if($record1->role != "")
              {
              $result3 =  mysql_query("SELECT * FROM `role` WHERE `id` = '".$record1->role."' ");
              $record2 = mysql_fetch_object($result3);
              echo  "<td>".$record2->title."</td>";
              }
              else
              {
                echo  "<td>".$record1->role."</td>";
              }
              echo "</tr>"; 
            }

    			}
    			
    			/** category **/
    			if($data->activity == 'Update Category' ){
    				$tableName				= "category";
          ?>
          <tbody>
            <tr>
              <th>Category</th>
              <th>Message</th>
              <th>Image</th>
              <th>Banner Image</th>
            </tr>
          </tbody>
            <?php
            $result1 = mysql_query("SELECT * FROM `".$tableName."` WHERE id = (SELECT id FROM `".$tableName."_log` WHERE log_id = ".$data->field_id.") ");
            $record  = mysql_fetch_object($result1);
            echo "<tr  style='background-color: #f0ad4e;'>"; 
            echo  "<td>".$record->category_name."</td>";
            echo  "<td>".$record->message."</td>";
            echo  "<td>";
            if($record->image != "")
            {
            echo "<img src='../category_images/".$record->image."' width='100' height='100' />";
            }
            echo "</td>";
            echo  "<td>";
            if($record->banner != "")
            {
            echo "<img src='../category_images/".$record->banner."' width='100' height='100' />";
            }
            echo "</td>";
            
            echo "</tr>"; 
            $result2 = mysql_query("SELECT * FROM `".$tableName."_log` WHERE log_id = ".$data->field_id);
            if(mysql_num_rows($result2) > 0)
            {
              $record1  = mysql_fetch_object($result2);
              echo "<tr>"; 
              echo  "<td>".$record1->category_name."</td>";
              echo  "<td>".$record1->message."</td>";
              echo  "<td>";
              if($record1->image != "")
              {
              echo "<img src='../category_images/".$record1->image."' width='100' height='100' />";
              }
              echo "</td>";
              echo  "<td>";
              if($record1->banner != "")
              {
              echo "<img src='../category_images/".$record1->banner."' width='100' height='100' />";
              }
              echo "</td>";
              echo "</tr>"; 
            }
    			}
    			
    			/** merchant **/
    			if($data->activity == 'Update Merchant' ){
    				$tableName				= "users";
            ?>
          <tbody>
            <tr>
              <th>First Name</th>
              <th>Full Name</th>
              <th>Display Name</th>
              <th>Email</th>
              <th>Category</th>
              <th>Phone</th>
              <th>Address</th>
              <th>City</th>
              <th>State</th>
              <th>Zip</th>
              <th>Payment Type</th>
              <th>Bank Name</th>
              <th>Account Type</th>
              <th>Logo</th>
              <th>Img</th>
            </tr>
          </tbody>
            <?php
            $result1 = mysql_query("SELECT * FROM `".$tableName."` WHERE id = (SELECT id FROM `".$tableName."_log` WHERE log_id = ".$data->field_id.") ");
            $record  = mysql_fetch_object($result1);
            echo "<tr  style='background-color: #f0ad4e;'>"; 
            echo  "<td>".$record->first_name."</td>";
            echo  "<td>".$record->full_name."</td>";
            echo  "<td>".$record->display_name."</td>";
            echo  "<td>".$record->email."</td>";
            echo  "<td>".$record->category."</td>";
            echo  "<td>".$record->phone."</td>";
            echo  "<td>".$record->address."</td>";
            echo  "<td>".$record->city."</td>";
            echo  "<td>".$record->state."</td>";
            echo  "<td>".$record->zip."</td>";
            echo  "<td>".$record->payment_type."</td>";
            echo  "<td>".$record->bank_name."</td>";
            echo  "<td>".$record->account_type."</td>";
            echo  "<td>";
            if($record->image != ""){
            echo  "<img src='../banner_images/".$record->image."' width='100' height='100' />";
            }
            echo  "</td>";
            echo  "<td>";
            if($record->user_img != ""){
            echo  "<img src='../provider_images/".$record->user_img."' width='100' height='100' />";
            }
            echo "</td>";
            echo "</tr>"; 
            $result2 = mysql_query("SELECT * FROM `".$tableName."_log` WHERE log_id = ".$data->field_id);
            if(mysql_num_rows($result2) > 0)
            {
              $record1  = mysql_fetch_object($result2);
              echo "<tr>"; 
              echo  "<td>".$record1->first_name."</td>";
              echo  "<td>".$record1->full_name."</td>";
              echo  "<td>".$record1->display_name."</td>";
              echo  "<td>".$record1->email."</td>";
              echo  "<td>".$record1->category."</td>";
              echo  "<td>".$record1->phone."</td>";
              echo  "<td>".$record1->address."</td>";
              echo  "<td>".$record1->city."</td>";
              echo  "<td>".$record1->state."</td>";
              echo  "<td>".$record1->zip."</td>";
              echo  "<td>".$record1->payment_type."</td>";
              echo  "<td>".$record1->bank_name."</td>";
              echo  "<td>".$record1->account_type."</td>";
              echo  "<td>";
              if($record1->image != ""){
              echo  "<img src='../banner_images/".$record1->image."' width='100' height='100' />";
              }
              echo  "</td>";
              echo  "<td>";
              if($record1->user_img != ""){
              echo  "<img src='../provider_images/".$record1->user_img."' width='100' height='100' />";
              }
              echo "</td>";
              echo "</tr>"; 
            }
    			}

    			/** products **/
    			if($data->activity == 'Update Product' ){
    				$tableName				= "products";
          ?>
          <tbody>
            <tr>
              <th>Name</th>
              <th>Category</th>
              <th>Product Price</th>
              <th>List Price</th>
              <th>Qty</th>
              <th>Detail</th>
              <th>Image1</th>
              <th>Image2</th>
              <th>Image3</th>
              <th>Stock Shipping</th>
              <th>Stock Pickup</th>
            </tr>
          </tbody>
            <?php
            $result1 = mysql_query("SELECT * FROM `".$tableName."` WHERE id = (SELECT id FROM `".$tableName."_log` WHERE log_id = ".$data->field_id.") ");
            $record  = mysql_fetch_object($result1);
            echo "<tr  style='background-color: #f0ad4e;'>"; 
            echo  "<td>".$record->product_name."</td>";
            echo  "<td>".$record->category."</td>";
            echo  "<td>".$record->product_price."</td>";
            echo  "<td>".$record->list_price."</td>";
            echo  "<td>".$record->product_quantity."</td>";
            echo  "<td>".$record->product_detail."</td>";
            echo  "<td>";
            if($record->image != ""){
            echo "<img src='../product_images/".$record->image."' width='100' height='100' />";
            }
            echo "</td>";
            echo "<td>";
            if($record->image1 != ""){
            echo "<img src='../product_images/".$record->image1."' width='100' height='100' />";
            }
            echo "</td>";
            echo "<td>";
            if($record->image2 != ""){
            echo "<img src='../product_images/".$record->image2."' width='100' height='100' />";
            }
            echo "</td>";            
            echo  "<td>".$record->stock_shipping."</td>";
            echo  "<td>".$record->stock_pickup."</td>";
            echo "</tr>"; 
            $result2 = mysql_query("SELECT * FROM `".$tableName."_log` WHERE log_id = ".$data->field_id);
            if(mysql_num_rows($result2) > 0)
            {
              $record1  = mysql_fetch_object($result2);
              echo "<tr>"; 
              echo  "<td>".$record1->product_name."</td>";
              echo  "<td>".$record1->category."</td>";
              echo  "<td>".$record1->product_price."</td>";
              echo  "<td>".$record1->list_price."</td>";
              echo  "<td>".$record1->product_quantity."</td>";
              echo  "<td>".$record1->product_detail."</td>";
              echo  "<td>";
              if($record1->image != ""){
              echo "<img src='../product_images/".$record1->image."' width='100' height='100' />";
              }
              echo "</td>";
              echo "<td>";
              if($record1->image1 != ""){
              echo "<img src='../product_images/".$record1->image1."' width='100' height='100' />";
              }
              echo "</td>";
              echo "<td>";
              if($record1->image2 != ""){
              echo "<img src='../product_images/".$record1->image2."' width='100' height='100' />";
              }
              echo "</td>";            
              echo  "<td>".$record1->stock_shipping."</td>";
              echo  "<td>".$record1->stock_pickup."</td>";
              echo "</tr>";
            }
    			}
    			
    			/** deal **/
    			if($data->activity == 'Update Deal'){
    				// log table does not exist in DB for for it
    			}

    			/** color_or_brands **/
    			if($data->activity == 'Update Color Or Brands'){
    				// log table does not exist in DB for for it
    			}

    			/** home-page-slider **/
    			if($data->activity == 'Update Home Page Slider' ){
    				$tableName				= "home-page-slider";
          ?>
          <tbody>
            <tr>
              <th>Image</th>
              <th>Link</th>
            </tr>
          </tbody>
          <?php
            $result1 = mysql_query("SELECT * FROM `".$tableName."` WHERE id = (SELECT id FROM `".$tableName."_log` WHERE log_id = ".$data->field_id.") ");
            $record  = mysql_fetch_object($result1);
            echo "<tr  style='background-color: #f0ad4e;'>"; 
            echo  "<td>";
            if($record->path != ""){
            echo "<img src='../upload_home_slider_image/".$record->path."' width='100' height='100' />";
            }
            echo "</td>";
            echo  "<td>".$record->link."</td>";
            echo "</tr>"; 
            $result2 = mysql_query("SELECT * FROM `".$tableName."_log` WHERE log_id = ".$data->field_id);
            if(mysql_num_rows($result2) > 0)
            {
              $record1  = mysql_fetch_object($result2);
              echo "<tr>"; 
              echo  "<td>".$record1->link."</td>";
              echo  "<td>";
              if($record1->path != ""){
              echo "<img src='../upload_home_slider_image/".$record1->path."' width='100' height='100' />";
              }
              echo "</td>";
              echo "</tr>";
            }
    			}

    			/** home-page-slider **/
    			if($data->activity == 'Update Home Page Sidebar'){
    				// log table does not exist in DB for for it
    			}

    			/** sponsor **/
    			if($data->activity == 'Update Sponsor'){
    				// log table does not exist in DB for for it
    			}

    			/** fedex **/
    			if($data->activity == 'Update Fedex'){
    				$tableName				= "fedex";
    			}

    			/** merchant_fedex_location **/
    			if($data->activity == 'Update Merchant Fedex Location'){
    				// log table does not exist in DB for for it
    			}

    			/** merchant class cast **/
    			if($data->activity == 'Update Merchant Class Cast'){
    				// log table does not exist in DB for for it
    			}

    			/** meta tag **/
    			if($data->activity == 'Update Meta Tag' ){
    				$tableName				= "meta_tag";
          ?>
          <tbody>
            <tr>
              <th>Category</th>
              <th>Title</th>
              <th>Keyword</th>
              <th>Page</th>
              <th>Description</th>
            </tr>
          </tbody>
          <?php
            $result1 = mysql_query("SELECT * FROM `".$tableName."` WHERE id = (SELECT id FROM `".$tableName."_log` WHERE log_id = ".$data->field_id.") ");
            $record  = mysql_fetch_object($result1);
            echo "<tr  style='background-color: #f0ad4e;'>"; 
            echo  "<td>";
            if($record->category != "")
            {
              $result4 = mysql_query("SELECT * FROM category WHERE id = '".$record->category."'");
              $record3 = mysql_fetch_object($result4);
              echo $record3->category_name;
            }
            else
            echo  $record->category; 

            echo "</td>";
            echo  "<td>".$record->title."</td>";
            echo  "<td>".$record->keyword."</td>";
            echo  "<td>".$record->page."</td>";
            echo  "<td>".$record->description."</td>";
            echo "</tr>"; 
            $result2 = mysql_query("SELECT * FROM `".$tableName."_log` WHERE log_id = ".$data->field_id);
            if(mysql_num_rows($result2) > 0)
            {
              $record1  = mysql_fetch_object($result2);
              echo "<tr>"; 
              echo "<td>";
              if($record1->category != "")
              {
                $result4 = mysql_query("SELECT * FROM category WHERE id = '".$record1->category."'");
                $record3 = mysql_fetch_object($result4);
                echo $record3->category_name;
              }
              else
              echo  $record1->category; 

              echo "</td>";
              echo  "<td>".$record1->title."</td>";
              echo  "<td>".$record1->keyword."</td>";
              echo  "<td>".$record1->page."</td>";
              echo  "<td>".$record1->description."</td>";
              echo "</tr>";
            }

    			}
        ?>
          </table>
        	</td>
        </tr>
        <?php
        	} 
        	$row_i ++;
    	}

    }
    else{ ?>
        <tr>
          <td colspan="7" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>
        </tr>
        <?php } ?>
      </table>
      <!--  end product-table................................... -->
    </form>
    <!--  start actions-box ............................................... -->
    <div id="actions-box"> <a href="" class="action-slider"></a>
      <div id="actions-box-slider"> <a href="javascript:;" name="delete" id="delete" class="action-delete" >Delete</a> </div>
      <div class="clear"></div>
    </div>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<script>
function showDetails(id){
	if(document.getElementById(id).style.display == 'table-row'){
		document.getElementById(id).style.display = 'none';
	} else {
		var elements = document.getElementsByClassName('transDetails');
		for(var i = 0, length = elements.length; i < length; i++) {
			elements[i].style.display = 'none';
		}

		document.getElementById(id).style.display = 'table-row';
	}

}
</script>
<?php include("footer.php")?>