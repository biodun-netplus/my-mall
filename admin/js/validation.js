function goBack()
  {
  window.history.back()
  }
 
$(document).ready(function(){
	$("#CompanySubmit").click(function()
	{
		if($("#CompanyName").val()=='')
		{
			//$("#CompanyName").fadeIn('');
			$("#CompanyError").fadeIn('slow');
			$("#CompanyError").text('Enter Company Name');
			return false
		}
		else
		{
			$("#CompanyError").fadeOut('fast');
			return true;
		}
	})
	
	$("#buyer_email").blur(function()
	{ 
		validateDuplicate('b');
	})
	$("#provider_email").blur(function()
	{ 
		validateDuplicate('p');
	})
	$("#name").blur(validateFirstName);
	$("#name").blur(function()
	{ 
		validateDuplicateName('p');
	})
	
	$("#name").keyup(validateFirstName);
	
	$("#provider_email").blur(validateEmail);
	$("#provider_email").keyup(validateEmail);
	
	$("#provider_pass").blur(validatePassword);
	$("#provider_pass").keyup(validatePassword);
	
	$("#provider_contact").blur(validateContact);
	$("#provider_contact").keyup(validateContact);
	
	$("#provider_address").blur(validateAddress);
	$("#provider_address").keyup(validateAddress);
	
	$("#provider_city").change(validateCity);
	
	$("#provider_city").blur(validateCity);
	$("#provider_city").keyup(validateCity);
	
	$("#country").change(validateCountry);
	
	
	$("#provider_state").change(validateState);
	
	
	$("#provider_zip").blur(validateZip);
	$("#provider_zip").keyup(validateZip);
	
	$("div.selectbox-wrapper2 ul li").click(validateProv_Category);
	$("div.selectbox-wrapper2 ul li").click(validateCategorya);
	
	$("#provider_category").blur(validateCategory);
	$("#provider_category").keyup(validateCategory);
	
	$("#provider_categorya").blur(validateCategorya);
	$("#provider_categorya").keyup(validateCategorya);
	
	$("#message").blur(validateDescription);
	$("#message").keyup(validateDescription);
	
	$("#image").change(validateImage);
	
	
	$("#product_price").blur(validatePrice);
	$("#product_price").keyup(validatePrice);
	
	$("#product_name").blur(validateProductName);
	$("#product_name").keyup(validateProductName);
	
	$("#product_quantity").blur(validateQuantity);
	$("#product_quantity").keyup(validateQuantity);
	
	$("#product_detail").blur(validateDetail);
	$("#product_detail").keyup(validateDetail);
	
		$("#Product_submit").click(function()
	{
		if(validateDetail() & validateQuantity() & validateProductName() & validatePrice() & validateCategorya() & validateImage())
		{ 
			return true;
		}
		else 
		{ 
			return false; 
		}
	})
	
	
	$("#BuyerSubmit").click(function()
	{
		if(validateFirstName() & validateEmail() & validatePassword() & validateContact() & validateAddress() & validateCity() & validateState() &validateCountry() & validateZip())
		{ 
			return true;
		}
		else 
		{ 
			return false; 
		}
	})
	$("#Categorysubmit").click(function()
	{
		
		if(validateCategory() & validateDescription() & validateImage())
		{ 
			return true;
		}
		else 
		{ 
			return false; 
		}
	})
	$("#ProviderSubmit").click(function()
	{
		if(validateFirstName() & validateEmail() & validatePassword() & validateContact() & validateAddress() &  validateCountry() & validateState() & validateCity() & validateZip() & validateCategory() & validateDeliveryarea())
		{ 
			return true;
		}
		else 
		{ 
			return false; 
		}
	})


	$("#couponSubmit").click(function()

	{
		if(StartDate() & EndDate() & ValueType() & Value() & Country() & Session())

		{

			return true;

		}

		return false;

	})

	$("#Gen_date").change(StartDate);

	$("#DropoffDate").change(EndDate);

	$("#Gen_date").blur(StartDate);

	$("#DropoffDate").blur(EndDate);

	$("#Type").change(ValueType);

	$("#Value").blur(Value);

	$("#Value").keyup(Value);

	$("#Country").change(Country);

	$("#Session").change(Session);



})

	// check coupon's field 
	function StartDate(){if($("#Gen_date").val()==''){$("#Gen_date_error").text('Select Start Date.');return false;}	else{$("#Gen_date_error").text('');return true;}}

	function EndDate(){	if($("#DropoffDate").val()=='')	{$("#Exp_date_error").text('Select End Date.');return false;}else{$("#Exp_date_error").text('');return true;}}

	function ValueType(){if($("#Type").val()=='')	{$("#Type_error").text('Select Value Type.');return false;}else{$("#Type_error").text('');return true;}}

	function Country(){if($("#Country").val()=='')	{$("#Country_error").text('Select Country.');return false;}else{$("#Country_error").text('');return true;}}

	function Session(){if($("#Session").val()=='')	{$("#Session_error").text('Select Session.');return false;}else{$("#Session_error").text('');return true;}}

	function Value()
	{
		if($("#Value").val()=='')	

		{

			$("#Value_error").text('Enter Coupon Value.');

			return false;

		}
		else if(isNaN($("#Value").val()))
		{

			$("#Value_error").text('Enter Only Numeric Value.');

			$("#Value").val('');

			return false;

		}
		else

		{
			$("#Value_error").text('');return true;

		}

	}



	function validateFirstName()
	{
		if($("#name").val()=='')
		{
			$("#name_error").text("This field is required.");
			$('#name_error').fadeIn('slow');
			return false;
		}
		else
		{
			$('#name_error').fadeOut('slow');
			$("#name_error").text("");
			return true;
		}
	}
	function validateEmail()
	{
		
		if($("#provider_email").val()){
			var a = $("#provider_email").val();
		}else
		{
			var a = $("#buyer_email").val();
			}
  		var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
 		if(filter.test(a))
		{
			$("#email").text("");
   			$("#email").fadeOut("slow");
  			return true;
 		}
 		else
		{	
			$("#email").text("Valid e-mail please, you will need it to log in!");
			$("#email").fadeIn("slow");
   			return false;
  		} 
	 }
	 function validateDuplicate(hint){
		 	if(hint=='p'){
		 		a = $("#provider_email").val();
			}
			else
			{
				a = $("#buyer_email").val();
			}
			$.post('checkduplicate.php?email='+a+"&hint="+hint, function(Retttt)
			{
				if($.trim(Retttt)=='t')
				{
  					var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
 					if(filter.test(a))
					{
						$("#email").text('');
						$("#email").fadeOut("slow");
					}
					else
					{
						$("#email").text("Valid e-mail please, you will need it to log in!");
						$("#email").fadeIn("slow");
					}
				}
				else
				{
					if(hint=='p'){
		 				$("#provider_email").val('');
					}
					else
					{
						$("#buyer_email").val('');
					}
					$("#email").text(a+" already registered !");
					$("#email").fadeIn("slow");
				}
			});
		}
		
		function validateDuplicateName(hint){
	 	if(hint=='p'){
	 		a = $("#name").val();
		}
		$.post('checkduplicate.php?firstName='+a+"&hint="+hint, function(Retttt)
		{
			if($.trim(Retttt)=='f')
			{
					$("#name_error").text(a + " already registered. please enter another!");
					$("#name_error").fadeIn("slow");
					return false;
				
			}
			else
			{
					$("#name_error").text("");
					$("#name_error").fadeOut("slow");
					return true;
			}
		});
	}

		
		
		
	 function  validatePassword()
	 {
		if($("#provider_pass").val()=='')
		{
			$("#password").text("This field is required.");
			$('#password').fadeIn('slow');
			return false;
		}
		else
		{
			$('#password').fadeOut('slow');
			$("#password").text("");
			return true;
		}
	 
	 }
	  function  validateContact()
	 {
		if($("#provider_contact").val()=='' || isNaN($("#provider_contact").val()))
		{
			$("#contact").text("Please provide valid contact detail.");
			$('#contact').fadeIn('slow');
			return false;
		}
		else
		{
			$('#contact').fadeOut('slow');
			$("#contact").text("");
			return true;
		}
	 
	 }
	 
	  function  validateAddress()
	 {
		if($("#provider_address").val()=='')
		{
			$("#address").text("This field is required.");
			$('#address').fadeIn('slow');
			return false;
		}
		else
		{
			$('#address').fadeOut('slow');
			$("#address").text("");
			return true;
		}
	 
	 }
	  function  validateCity()
	 {
		if($("#provider_city").val()=='')
		{
			$("#city").text("Please select the city.");
			$('#city').fadeIn('slow');
			return false;
		}
		else
		{
			$('#city').fadeOut('slow');
			$("#city").text("");
			return true;
		}
	 
	 }
	  
	  function  validateState()
	 {
		if($("#provider_state").val()=='')
		{
			$("#state").text("Please select the state.");
			$('#state').fadeIn('slow');
			return false;
		}
		else
		{
			$('#state').fadeOut('slow');
			$("#state").text("");
			return true;
		}
	 
	 }
	 
	  function validateCountry()
	{
		if($("#country").val()=='' )
		{
			$("#country_error").text("Please select the country");
			$('#country_error').fadeIn('slow');
			return false;
		}
		else
		{
			$('#country_error').fadeOut('slow');
			$("#country_error").text("");
			return true;
		}
	 
	 }
	 
	  function  validateZip()
	 {
		if($("#provider_zip").val()=='' || isNaN($("#provider_zip").val()))
		{
			$("#zip").text("Please provide valid zipcode");
			$('#zip').fadeIn('slow');
			return false;
		}
		else
		{
			$('#zip').fadeOut('slow');
			$("#zip").text("");
			return true;
		}
	 
	 }
	 
	 function validateCategory()
	{
		if($("#provider_category").val()=='' )
		{
			$("#category").text("Please select the category");
			$('#category').fadeIn('slow');
			return false;
		}
		else
		{
			$('#category').fadeOut('slow');
			$("#category").text("");
			return true;
		}
	 
	 }
	 
	 
	 function validateSubCategory()
	{
		if($("#provider_subcategory").val()=='' )
		{
			$("#subcategory").text("Please select the sub category");
			$('#subcategory').fadeIn('slow');
			return false;
		}
		else
		{
			$('#subcategory').fadeOut('slow');
			$("#subcategory").text("");
			return true;
		}
	 
	 }
	 	
		 
	 function validateDeliveryarea()
	{
		if($("#provider_deliveryarea").val()=='' )
		{
			$("#deliveryarea").text("Please select the merchant pickup area");
			$('#deliveryarea').fadeIn('slow');
			return false;
		}
		else
		{
			$('#deliveryarea').fadeOut('slow');
			$("#deliveryarea").text("");
			return true;
		}
	 
	 }

	 
	  function validateProv_Category()
	{
		if($("#prov_category").val()=='' )
		{
			$("#provd_category").text("Please select the category");
			$('#provd_category').fadeIn('slow');
			return false;
		}
		else
		{
			$('#provd_category').fadeOut('slow');
			$("#provd_category").text("");
			return true;
		}
	 
	 }
	 
	  function validateCategorya()
	{
		if($("#provider_categorya").val()=='' )
		{
			$("#categorya").text("Please select the category");
			$('#categorya').fadeIn('slow');
			return false;
		}
		else
		{
			$('#categorya').fadeOut('slow');
			$("#categorya").text("");
			return true;
		}
	 
	 }
	 
	 
	  function validateDescription()
	{
		if($("#message").val()=='' )
		{
			$("#detail").text("Please enter the details");
			$('#detail').fadeIn('slow');
			return false;
		}
		else
		{
			$('#detail').fadeOut('slow');
			$("#detail").text("");
			return true;
		}
	 
	 }
	 
	 
	 
	  function validateImage()
	{
		if($("#image").val()=='' )
		{
			$("#imagea").text("Please upload the image");
			$('#imagea').fadeIn('slow');
			return false;
		}
		else
		{
			$('#imagea').fadeOut('slow');
			$("#imagea").text("");
			return true;
		}
	 
	 }
	 
	 
	  function validateProductName()
	{
		if($("#product_name").val()=='' )
		{
			$("#namea").text("Please enter the name of product");
			$('#namea').fadeIn('slow');
			return false;
		}
		else
		{
			$('#namea').fadeOut('slow');
			$("#namea").text("");
			return true;
		}
	 
	 }
	 
	 
	  function validatePrice()
	{
		if($("#product_price").val()=='' || isNaN($("#product_price").val()))
		{
			$("#price").text("Please enter only numeric price");
			$('#price').fadeIn('slow');
			return false;
		}
		else
		{
			$('#price').fadeOut('slow');
			$("#price").text("");
			return true;
		}
	 
	 }
	 
	 
	  function validateQuantity()
	{
		if($("#product_quantity").val()=='' || isNaN($("#product_quantity").val()))
		{
			$("#quantity").text("Please enter only numeric quantity");
			$('#quantity').fadeIn('slow');
			return false;
		}
		else
		{
			$('#quantity').fadeOut('slow');
			$("#quantity").text("");
			return true;
		}
	 
	 }
	 
	   function validateDetail()
	{
		if($("#product_detail").val()=='' )
		{
			$("#detail").text("Please enter the details");
			$('#detail').fadeIn('slow');
			return false;
		}
		else
		{
			$('#detail').fadeOut('slow');
			$("#detail").text("");
			return true;
		}
	 
	 }
	 
	
		
	


 
 