function goBack()
{
	window.history.back()
}
 
$(document).ready(function(){
	$("#category").change(FedExCategory);
	
	$("#avg_weight").change(FedExWeight);
	
	$("#destination").change(FedExDestination);
	
	$("#shipping").blur(FedExShipping);
	$("#shipping").keyup(FedExShipping);
	
	$("#type").change(FedExType);
	
	$("#FedEx_Submit").click(function()
	{
		if(FedExCategory() & FedExWeight() & FedExDestination() & FedExShipping() & FedExType())
		{ 
			return true;
		}
		else 
		{ 
			return false; 
		}
	})
})
function FedExCategory()
{
	if($("#category").val()=='')
	{
		$("#category_error").text("Please select category.");
		$('#category_error').fadeIn('slow');
		return false;
	}
	else
	{
		$('#category_error').fadeOut('slow');
		$("#category_error").text("");
		return true;
	}
}
function FedExWeight()
{
	if($("#avg_weight").val()=='')
	{
		$("#avg_weight_error").text("Please select weight.");
		$('#avg_weight_error').fadeIn('slow');
		return false;
	}
	else
	{
		$('#avg_weight_error').fadeOut('slow');
		$("#avg_weight_error").text("");
		return true;
	}
}
function FedExDestination()
{
	if($("#destination").val()=='')
	{
		$("#destination_error").text("Please select destination.");
		$('#destination_error').fadeIn('slow');
		return false;
	}
	else
	{
		$('#destination_error').fadeOut('slow');
		$("#destination_error").text("");
		return true;
	}
}
function FedExShipping()
{
	if($("#shipping").val()=='')
	{
		$("#shipping_error").text("Please enter shipping price.");
		$('#shipping_error').fadeIn('slow');
		return false;
	}
	if(isNaN($("#shipping").val()))
	{
		$("#shipping").val('');
		$("#shipping_error").text("Please enter valid shipping price.");
		$('#shipping_error').fadeIn('slow');
		return false;
	}
	else
	{
		$('#shipping_error').fadeOut('slow');
		$("#shipping_error").text("");
		return true;
	}
}
function FedExType()
{
	if($("#type").val()=='')
	{
		$("#type_error").text("Please select destination type.");
		$('#type_error').fadeIn('slow');
		return false;
	}
	else
	{
		$('#type_error').fadeOut('slow');
		$("#type_error").text("");
		return true;
	}
}	  