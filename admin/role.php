<?php 
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 8-4-2015 */
?>

<div class="clear"></div>

<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 

<?php
// delete role....
$msg = '';
if(isset($_GET['id']) && $_GET['id'] != ''){
	mysql_query("Delete FROM role WHERE id = '".base64_decode($_GET['id'])."'");
	$msg = "Role has deleted successfully.";
}
// end of delete

$query = "SELECT COUNT(*) FROM role  ";
$result = mysql_query($query) or die(mysql_error());
$num_rows = mysql_fetch_row($result);

$pages = new Paginator;
$pages->items_total = $num_rows[0];
$pages->mid_range = 4; // Number of pages to display. Must be odd and > 3
$pages->paginate();
$query = "SELECT * from role  order by id desc $pages->limit";
$result = mysql_query($query) or die(mysql_error());
$countCondition = mysql_num_rows($result);
?>
    <div class="row bottom-margin">
      <div id="page-heading" >
        <h1>Role</h1>
      </div>
      <div class="add-button col-lg-5"> <a href="add_role.php">
        <input type="button" class="buttons btn btn-info" value="Add New Role"/>
        </a> </div>
      <div id="paging-table" class="col-lg-6"><?php echo $pages->display_pages(); ?></div>
    </div>
    <?php if(isset($msg) && $msg!='')  {?>
      <div class="alert alert-success">
        <?php echo $msg;?>
      </div>
    <?php } ?>
    <form id="mainform" action="" class="table-responsive">
      <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="table table-bordered table-hover table-striped">
        <tr>
          <th class="table-header-check"><a id="toggle-all" ></a> </th>
          <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Title</a></th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Description </a></th>
          <th width="125" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Action</a></th>
        </tr>
        <?php
		if($countCondition > 0){
			while($data = mysql_fetch_object($result)){ 
					
		?>
        <tr>
          <td width="20"><input  type="checkbox" name="checkbox" value="<?php echo $data->id;?>"/></td>
          <td><?php echo ucfirst($data->title); ?></td>
          <td><?php echo $data->description; ?></td>
          <td><a onclick="return confirm('Are you sure, you want to delete this role?')" href="role.php?id=<?php echo base64_encode($data->id);?>" class="" alt="<?php echo $data->id;?>" title="Click to Delete Role"> <img src="images/table/action_delete.gif"/></a>
            <a href="edit_role.php?id=<?php echo base64_encode($data->id);?>" title="Click to Edit Role"> <img src="images/table/action_edit.gif"/></a></td>
        </tr>
        <?php }}else{ ?>
        <tr>
          <td colspan="10" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>
        </tr>
        <?php } ?>
      </table>
      <!--  end product-table................................... -->
    </form>
    <div id="actions-box"> <a href="" class="action-slider"></a>
      <div id="actions-box-slider"> <a href="javascript:;" name="delete" id="delete" class="action-delete" >Delete</a> </div>
      <div class="clear"></div>
    </div>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>