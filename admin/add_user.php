<?php
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */


if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{    
	 $role_id 				     =  implode('|',$_REQUEST['role_id'] );
	 
	 $User					      = array();
	 $username 				    = strtolower(str_replace(array( '\'', '"', ',' , '(' , ')', ';' ,':' ,'.','&',"'", '<', '>',' ' ),'',$_REQUEST['user_name']));
	 $User['username']		= $username;	
	 $User['email_id']		= $_REQUEST['user_email'];
	 $User['password']		= md5($_REQUEST['user_pass']);
	 $User['contact']		  = $_REQUEST['user_contact'];
	 $User['address']		  = $_REQUEST['user_address'];
	 $User['admin_type']	= 1;
	 $User['role']			  = $role_id;
   $User['created_by']  = $_SESSION['id'];

	 $main					      = new users;
	
	 $insert= $main->ADD($User,"admin"); 
	 if($insert)
	 {
		header("location:view_users.php");
	 }
}
 ?>

<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content" style="">
    <div >
      <div id="page-heading" >
        <h1>Add User</h1>
      </div>
      <div style="float:right; padding-right:20px;">
        <input type="button" class="buttons btn btn-info" value="Back" onclick="window.history.back()" />
      </div>
    </div>
    <div class="row">
      <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" id="UserForm">
        <div class="col-lg-5">
          <div class="form-group">
            <label>Name: </label>
            <input type="text" class="form-control" id="user_name" name="user_name" value="" />
            <label class="error"  id="name_error"></label>
          </div>
          <div class="form-group">
            <label> Email: </label>
            <input type="text" class="form-control" id="user_email" name="user_email" value="" />
            <label class="error"  id="email"></label>
          </div>
          <div class="form-group">
            <label>Password: </label>
            <input type="password" class="form-control" id="user_pass" name="user_pass" value="" />
            <label class="error"  id="password"></label>
          </div>
          <div class="form-group">
            <label>Contact No: </label>
            <input type="text" class="form-control" id="user_contact" name="user_contact" value="" />
            <label class="error"  id="contact"></label>
          </div>
          <div class="form-group">
            <label>Address: </label>
            <textarea class="form-control" id="user_address" name="user_address"></textarea>
            <label class="error"  id="address"></label>
          </div>
          <div class="form-group">
            <input type="submit" id="UserSubmit" value="submit" name="submit" class="form-submit btn btn-default" />
            <input type="reset" value="Reset" class="form-reset btn btn-default"  />
          </div>
        </div>
        <div class="col-lg-6">
          <h2>Roles</h2>
		  <?php         
          $Query = mysql_query("SELECT * FROM role");
          
          ?> 
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <tbody>
               <?php 
			   if(mysql_num_rows($Query) > 0){
			   while($Data  = mysql_fetch_object($Query)){
			   ?>
                <tr>
                  <td><input type="checkbox" name="role_id[]" value="<?php echo $Data->id;?>" /></td>
                  <td><?php echo $Data->title;?></td>
                  <td><?php echo $Data->description;?></td>
                </tr>
              <?php
			   }
			   }
			   else{
				  echo  '<td colspan="5" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>';
				   
			   }
			   ?>
              </tbody>
            </table>
          </div>
        </div>
      </form>
      
      <!-- end id-form  --> 
    </div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>