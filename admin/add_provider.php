<?php
include("header.php");
require_once("includes/classes/upload.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */


if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{      
    $result	= ''; 
	if(isset($_FILES['provider_logo']['name']) && $_FILES['provider_logo']['name'] != '')
	{
		$destination = '../banner_images/';
		$file = $_FILES['provider_logo'];
		$upload = new Upload; 
		$result = $upload->uploadImage($file, $destination); 
	}
	 $provider='p';
     $date=date("Y-m-d H:i:s");
	 $User=array();
	 $merchantName =strtolower(str_replace(array( '\'', '"', ',' , '(' , ')', ';' ,':' ,'.','&',"'", '<', '>',' ' ),'-',$_REQUEST['name']));
	 $User['first_name']			= $_REQUEST['name'];	
	 $User['display_name']	= $_REQUEST['display_name'];	
	 $User['slug']					= $merchantName;	
	 $User['email']				= $_REQUEST['provider_email'];
	 $User['password']			= md5($_REQUEST['provider_pass']);
	 $User['category']			= $_REQUEST['provider_category'];
	 $User['phone']				= $_REQUEST['provider_contact'];
	 $User['address']				    = $_REQUEST['provider_address'];
   $User['state']					     = $_REQUEST['provider_state'];
	 $User['country']				    = $_REQUEST['Country'];
	 $User['zip']					     = $_REQUEST['provider_zip'];
	 $User['city']					     =$_REQUEST['provider_city'];
   $User['pickup_state']		   = $_REQUEST['states'];
   $User['pickup_lga']       = $_REQUEST['provider_lga'];
	 $User['isactive']				   = 't';
	 $User['createdBy']			     = $_REQUEST['createdBy'];
	 $User['created']				     = $date;
	 $User['user_type']			    = $provider;
   $User['account_type']      = $_REQUEST['txtAccountType'];
   $User['image']	=	$result; 

	 $main= new users;
	 $insert= $main->ADD($User,"users");
	 if($insert)
	 {
$to = $_POST['email'];
$subject = "Registration Successful on WebmallNG";
$headers = 'From: WebmallNG' . "\r\n" .
    'Reply-To: info@WebmallNG.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers .= "MIME-Version: 1.0\r\n";
$headers .= 'Cc: wolef77@yahoo.com' . "\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
$message .='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Customer Successfully Completed Transaction</title>
</head>

<body style="margin:0;padding:0;background:#efefef;">
<table align="center" width="600" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif;font-size:12px;border:1px solid #ccc;">
  <!--DWLayoutTable-->
  <tr>
    <td colspan="3" valign="top" style="border-bottom:5px solid #bb9765;">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="17" height="96"></td>
          <td width="536" valign="middle"><a href="#"><img src="https://www.webmallng.com/images/mailwebmalllogo.png" alt="logo" border="0" style="-webkit-user-select: none" /></a></td>
          <td width="17"></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td height="20" colspan="3" valign="top" bgcolor="#fff" style="border-bottom:5px solid #bb9765;">
      <table width="100%" background="http://www.umall.com.ng/mall/images/shadow.png" style="background-repeat:no-repeat;" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td style="padding:10px 0;" valign="top">
            <table width="98%" border="0" cellspacing="0" cellpadding="10" style="color:#333333; font-size:14px">
              <tr>
                <td valign="top"><img src="https://www.webmallng.com/images/dm.png" alt="" style="margin-bottom:10px;" /><br />
               You have successfully created a store on Webmallng, Below are login detail below..</td>
              </tr>
              <tr>
                <td valign="top">
                	<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding:15px;background:#d6e6e6;font-size:16px;border:1px solid #97c5c5;">
                    <tr>
                      <td height="24">Username : </td>
                      <td><strong>: '.$User['email'].'</strong></td>
                    </tr>
                    <tr>
                      <td height="24">Password : </td>
                      <td><strong>'.$User['password'].' </strong></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
                <td >
               You Store is now open and can be visible to all coustomers at <a href="">www.webmallng.com/'.$merchantName.' </a> login using your detailes to complete your store setup including LOGO uploads , product images , price and more </td>
              </tr> 
              <tr>
                <td> </td>
              </tr>
              <tr>
                <td>For Inquiries Please call us on 0703.948.1991 or send an email to info@webmallng.com </td>
              </tr>
      </table>
    </td>
  </tr>
    <tr>
    <td></td>
    <td valign="top" style="padding:20px 0">
    	<table width="100%" border="0" cellpadding="0" cellspacing="0" >
          
        <tr>
          <td align="left"><a href="#"><img src="https://www.webmallng.com/images/loginlogo.png" alt="" /></a></td>          
          <td align="right"><a href="#"><img src="http://www.webmallnglab.com/first-city-mall/images/fb.png" alt="" border="0" style="margin: 0 10px 0 0;" /></a><a href="#"><img src="http://www.webmallnglab.com/first-city-mall/images/tt.png" alt="" border="0" style="margin: 0 10px 0 0;" /></a></td>
        </tr> 
      </table>

    </td>
    <td></td>
  </tr>
</table>
</body>
</html>';



		mail($to, $subject, $message, $headers);
		header("location:provider.php");
	 }
}
 ?>
<script type="text/javascript">
$(document).ready(function(){
  $("#states").change(function()
	{

    var id = this.value;
    
		
		if(id!=''){
			$.ajax({
				url: "lga.php",
				type: "POST",
				data: {'id':id,'hint':'lga'},
				success: function(Data){
					
					$("#ajax_lga").html(Data);
				}
			});
		}
  })
 
	
	$("#country").change(function()
	{
		var id = this.value;
		
		if(id!=''){
			$.ajax({
				url: "state.php",
				type: "POST",
				data: {'id':id,'hint':'state'},
				success: function(Data){
					
					$("#ajax_state").html(Data);
				}
			});
		}
	})
	$("#provider_state").live('change',function()
	{
		var id = this.value;
		
		if(id!=''){
			$.ajax({
				url: "state.php",
				type: "POST",
				data: {'id':id,'hint':'city'},
				success: function(Data){
					
					$("#ajax_city").html(Data);
				}
			});
		}
  })
  
  
	
	$("input:radio[name=bankaccounselect]").click(function() {
    	if($(this).val() == 'Yes'){
			$('#BankShow').show();
			$('#txtAccountType').val('');
		}
		else{
			$('#BankShow').hide();
			$('#txtAccountType').val('no');
		}
	});	
	/*=== get subcategory from category ============================================*/
	$("#provider_category").change(function (){
		$('.loader').show();
		$.ajax({
			 url: "delete_category.php",
			 data: {'catId':$("#provider_category").val(),  'getSubCat': 'getSubCat' },
			 success: function(Data){
				 $('.loader').hide();
				 $('#provider_subcategory').append(Data);
			}
		});
		return false;

	})

})
</script>
<script type="text/javascript" src="js/validation.js"></script>

<div class="clear"></div>

<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content" style="">
    <div >
      <div id="page-heading" >
        <h1>Add Merchant</h1>
      </div>
      <div style="float:right; padding-right:20px;">
        <input type="button" class="buttons btn btn-info" value="Back" onclick="goBack()" />
      </div>
    </div>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
      <tr>
        <th rowspan="3" class="sized"></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><!--  start content-table-inner -->
          
          <div id="content-table-inner"> 
            
            <!--  start message-red --> 
            
            <!--  end message-red -->
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
              <tr valign="top">
                <td align="left"><!--  start step-holder --> 
                  <!--  end step-holder --> 
                  <!-- start id-form -->
                  
                  <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" id="ProviderForm" enctype="multipart/form-data">
                    <table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
                      <tr>
                        <th valign="top">Name:</th>
                        <td><input type="text" class="form-control" id="name" name="name" value="" /></td>
                        <td><label class="error"  id="name_error"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Display name 	:</th>
                        <td><input type="text" class="form-control" id="display_name" name="display_name" value="" /></td>
                        <td><label class="error"  id="display_name_error"></label></td>
                      </tr>
                      <tr>
                        <th valign="top"> Email:</th>
                        <td><input type="text" class="form-control" id="provider_email" name="provider_email" value="" /></td>
                        <td width="310"><label class="error"  id="email"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Password:</th>
                        <td><input type="password" class="form-control" id="provider_pass" name="provider_pass" value="" /></td>
                        <td><label class="error"  id="password"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Contact No:</th>
                        <td><input type="text" class="form-control" id="provider_contact" name="provider_contact" value="" /></td>
                        <td><label class="error"  id="contact"></label>
                      </tr>
                      <tr>
                        <th valign="top">Address:</th>
                        <td><input type="text" class="form-control" id="provider_address" name="provider_address" value="" /></td>
                        <td><label class="error"  id="address"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Country:</th>
                        <td><select id="country" name="Country" class="inp-select form-control" >
                            <option value="">select country</option>
                            <?php 
						              	$query = mysql_query("select * from countries");
							              while($data = mysql_fetch_object($query)){ ?>
                            <option value="<?php echo $data->id ?>"><?php echo $data->name; ?></option>
                            <?php } ?>
                          </select></td>
                        <td><label class="error"  id="country_error"></label></td>
                      </tr>
                      
                      <tr>
                        <th valign="top">State:</th>
                        <td><span id="ajax_state">
                          <select id="provider_state" name="provider_state" class="inp-select form-control" >
                            <option value="">select state</option>
                          </select>
                          </span></td>
                        <td><label class="error"  id="state"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">City:</th>
                        <td><input type="text" class="form-control" id="provider_city" name="provider_city" value="" /></td>
                        <td><label class="error"  id="city"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Zip Code:</th>
                        <td><input type="text" class="form-control" id="provider_zip" name="provider_zip" value="" /></td>
                        <td><label class="error"  id="zip"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Category:</th>
                        <td><?php $deleQ = mysql_query("select * from category where isactive='t' order by category_name ASC");?>
                          <select  name="provider_category" class="inp-select form-control"  id="provider_category">
                            <option value=""> Category</option>
                            <?php while($row = mysql_fetch_object($deleQ)){ ?>
                            <option value="<?php echo $row->id; ?>"><?php echo $row->category_name; ?></option>
                            <?php } ?>
                          </select></td>
                        <td><label class="error"  id="category"></label></td>
                      </tr>
                      <tr>
                      <tr>
                        <th valign="top">Logo:</th>
                        <td><input type="file" name="provider_logo" id="provider_logo"  /></td>
                        <td></td>
                      </tr>
                      <!-- <tr>
                        <th valign="top"> Merchant Pickup Area:</th>
                        <td><?php $deleQ = mysql_query("select * from states_merchant where country_id='NG' order by name ASC");?>
                          <select  name="provider_deliveryarea" class="inp-select form-control"  id="provider_deliveryarea">
                            <option value=""> Merchant Pickup Area</option>
                            <?php while($DeleveryData = mysql_fetch_object($deleQ)){ ?>
                            <option value="<?php echo $DeleveryData->name; ?>"><?php echo $DeleveryData->name; ?></option>
                            <?php } ?>
                          </select></td>
                        <td><label class="error"  id="deliveryarea"></label></td>
                      </tr> -->
                      <tr>
                        <th valign="top">Merchant Pickup State:</th>
                        <td><select id="states" name="states" class="inp-select form-control" >
                            <option value="">select state</option>
                            <?php 
						              	$query = mysql_query("select * from f_states");
							              while($data = mysql_fetch_object($query)){ ?>
                            <option value="<?php echo $data->id ?>"><?php echo $data->name; ?></option>
                            <?php } ?>
                          </select></td>
                        <td><label class="error"  id="state_error"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">LGA:</th>
                        <td><span id="ajax_lga">
                          <select id="provider_lga" name="provider_lga" class="inp-select form-control" >
                            <option value="">select lga</option>
                          </select>
                          </span></td>
                        <td><label class="error"  id="lga"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Do you have an Ecobank account?:</th>
                        <td><div> <span>
                            <input type="radio" name="bankaccounselect" class="bankaccounselect" id="Yes" value="Yes">
                            Yes</span> <span>
                            <input type="radio" name="bankaccounselect" class="bankaccounselect" id="No" value="No">
                            No</span> </div>
                          <p id="BankShow" style="display:none;">
                            <input type="text" id="txtAccountType" name="txtAccountType" class="form-control">
                          </p></td>
                        <td></td>
                      </tr>
                      <tr>
                        <th valign="top">Created By:</th>
                        <td><select  name="createdBy" class="inp-select form-control" >
                            <option value="WebMallNG Team">WebMallNG Team</option>
                            <option value="Ecobank Team">Ecobank Team</option>
                          </select></td>
                        <td></td>
                      </tr>
                      <tr>
                        <th>&nbsp;</th>
                        <td valign="top"><input type="submit" id="ProviderSubmit" value="submit" name="submit" class="form-submit btn btn-default" />
                          <input type="reset" value="Reset" class="form-reset btn btn-default"  /></td>
                      </tr>
                    </table>
                  </form>
                  
                  <!-- end id-form  --></td>
              </tr>
              <tr>
                <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                <td></td>
              </tr>
            </table>
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>
