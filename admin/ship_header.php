<?php 
ob_start();
include("includes/classes/functions.php");
include("../includes/classes/upload.php");
include('paginator.class.php'); 
if(!isset($_SESSION['FedExId']))
{
	echo"<script>window.location.href='FedEx_login.php'</script>";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>WebmallNG: Admin</title>
<link rel="stylesheet" href="css/screen.css" type="text/css" media="screen" title="default" />
<!--[if IE]>
<link rel="stylesheet" media="all" type="text/css" href="css/pro_dropline_ie.css" />
<![endif]-->

<!--  jquery core -->
 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

<!--  checkbox styling script -->

</head>
<body>
<!-- Start: page-top-outer -->
<div id="page-top-outer"> 
  
  <!-- Start: page-top -->
  <div id="page-top"> 
    
    <!-- start logo -->
    <div > <a href=""><img src="images/shared/fedex_logo.png" alt="" /></a> </div>
    <!-- end logo --> 
    
    <!--  start top-search --> 
    
    <!--  end top-search -->
    <div class="clear"></div>
  </div>
  <!-- End: page-top --> 
  
</div>
<!-- End: page-top-outer -->

<div class="clear">&nbsp;</div>

<!--  start nav-outer-repeat................................................................................................. START -->
<div class="nav-outer-repeat"> 
  <!--  start nav-outer -->
  <div class="nav-outer"> 
    
    <!-- start nav-right -->
    <div id="nav-right">
      <div class="nav-divider">&nbsp;</div>
      <div class="showhide-account"></div>
      <a href="FedEx_settings.php"><img src="images/shared/nav/nav_myaccount.gif" width="93" height="14" alt=""/></a>
      <div class="nav-divider">&nbsp;</div>
      <a href="logout.php?logout" id="logout"><img src="images/shared/nav/nav_logout.gif" width="64" height="14" alt="" title="log out" /></a>
      <div class="clear">&nbsp;</div>
      
      <!--  start account-content --> 
      
      <!--  end account-content --> 
      
    </div>
    <!-- end nav-right --> 
    <style>
    .nav .current a b { display: block; padding: 0 32px 0 13px;}
	.nav .select a{ padding:0 24px;}
    </style>
    <!--  start nav -->
    <div class="nav">
      <div class="table">
        <?php  $dashboard = end(explode('/',$_SERVER['SCRIPT_FILENAME']))=='FedEx_dashboard.php'; ?>
        <ul class="<?php if($dashboard){  ?>current<?php }else{ ?>select<?php } ?>">
          <li><a href="FedEx_dashboard.php"><b>Dashboard</b></a></li>
        </ul>
        <div class="nav-divider">&nbsp;</div>
        <?php
			$FedEx_record = end(explode('/',$_SERVER['SCRIPT_FILENAME']))=='FedEx_record.php' || end(explode('/',$_SERVER['SCRIPT_FILENAME']))=='FedEx_Detail.php';
			?>
        <ul  class="<?php if($FedEx_record){  ?>current<?php }else{ ?>select<?php } ?>">
          <li><a href="FedEx_record.php"><b>FedEx Record</b><!--[if IE 7]><!--></a><!--<![endif]--> 
          </li>
        </ul>
           <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <!--  start nav --> 
    
  </div>
  <div class="clear"></div>
  <!--  start nav-outer --> 
</div>
