<?php 
include("header.php");
/* Page management */
/* Author @Parmeet Arora */
/* Date : 25-08-2015 */
if(isset($_REQUEST['edit_id']))
{
	if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
	{ 
		$page					= array();
		$main					= new users;
		$page['title'] 			= $_REQUEST['title'];
		$page['detail'] 		= $_REQUEST['detail'];
		$id 					= $_REQUEST['edit_id'];
		
		$xx= $main->update($page,'miscellaneous',$id);
		if($xx)
		{
			$_SESSION['msg']='Page detail updated successfully.';
			header("Location:page.php");
		}
	}

	$edit 		= new users;
	$edit_data	= $edit->viewprofile($_REQUEST['edit_id'],"miscellaneous"); //
}
else
{
	header("Location:page.php");
}
?>

<div class="clear"></div>
 
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content" style="">

<div>
<div id="page-heading"><h1>Edit Page</h1></div>
<div style="float:right; padding-right:20px;">Search &nbsp;: &nbsp;&nbsp;<input type="button" value="Back" class="buttons" onclick="goBack()" /></div>
</div>


<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"></th>
</tr>
<tr> 
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
				
				<!--  start message-red -->
				
				<!--  end message-green -->
		
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top"> 
	<td align="center">
	
		<!--  start step-holder -->
		<!--  end step-holder -->
        <!-- start id-form -->
        <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" enctype="multipart/form-data" id="edit-page">
		<table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
		<tr>
			<th valign="top">Title:</th>
			<td><input type="text" class="form-control" name="title" value="<?php echo $edit_data->title;?>" /></td>
		</tr>
            <input type="hidden" value="<?php echo $edit_data->id;?>" name="id"/>
		<tr>
			<th valign="top">Description:</th>
			<td><textarea rows="2" cols="20" id="detail" class="form-control form-textarea" name="detail" ><?php echo $edit_data->detail;?></textarea></td>
		</tr>
		<tr>
			<th>&nbsp;</th>
			<td valign="top">
				<input type="submit" value="submit" name="submit" class="form-submit btn btn-default" />
			</td>
		</tr>
	</table>
   		 </form>
	<!-- end id-form  -->	</td>
	
</tr>
<tr>
<td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
	
	<div class="clear"></div>
 

</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->
<div class="clear">&nbsp;</div>
<link rel="stylesheet" type="text/css"  href="../assets/css/editor.css" />
<script type="text/javascript" src="../assets/js/editor.js"></script>
<script type='text/javascript'> $('#detail').Editor(); $('form#edit-page').submit(function(){ $('#detail').val($('.Editor-editor').html()); }); $('.Editor-editor').html($('#detail').val());
</script>
<?php include("footer.php")?>

