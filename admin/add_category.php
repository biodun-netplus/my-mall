<?php 
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */
//error_reporting(E_ALL);

//this function add by vijay to resolve number issue in category page url
function FormateString($name){
    return strtolower(str_replace(array( '\'', '"', ',' , ';' , '(' , ')' ,':' ,'.','&',"'", '<', '>',' ' ),'',stripslashes($name)));
}

if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{     
  
  $destination = '../category_images/';
  $file = $_FILES['image'];
  $upload = new Upload; 
  $result = $upload->uploadImage($file, $destination);
  //$BannerDestination = '../provider_images/';
  $bannerfile = $_FILES['banner'];
  //$BannerUpload = new Upload; 
  $banner = $upload->uploadImage($bannerfile, $destination, null, null); 
  // vertical banner upload 
  $bannerfile1 = $_FILES['banner2'];
  $banner1 = $upload->uploadImage($bannerfile1, $destination, null, null); 
    $date=date("Y-m-d H:i:s");
  $User=array();
  $User['category_name']=$_REQUEST['category_name'];  
  $User['message']=$_REQUEST['message'];
  $User['parent_id'] = $_REQUEST['parent_cat'];
  $User['image']=$result;
  $User['banner']=$banner;
  $User['vert_banner']=$banner1;
  $User['created']=$date;
  $User['weight'] = $_REQUEST['weight'];
  $User['unit'] = $_REQUEST['unit'];
  //change by vijay
  $User['cname']=FormateString($_REQUEST['category_name']); 
     
    $main= new users;
    $xx= $main->ADD($User,"category");
    if($xx)
    {
     header("location:catergory.php");
    }
}
 ?>
<script type="text/javascript" src="js/validation.js"></script>

<div class="clear"></div>

<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content" style="">
    <div >
      <div id="page-heading">
        <h1>Add Category</h1>
      </div>
      <div style="float:right; padding-right:20px;">
        <input type="button" class="buttons btn btn-info" value="Back" onclick="goBack()" />
      </div>
    </div>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
      <tr>
        <th rowspan="3" class="sized"></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><!--  start content-table-inner -->
          
          <div id="content-table-inner"> 
            
            <!--  start message-red --> 
            
            <!--  end message-red -->
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
              <tr valign="top">
                <td><!--  start step-holder --> 
                  <!--  end step-holder --> 
                  <!-- start id-form -->
                  
                  <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" enctype="multipart/form-data">
                    <table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
                      <tr>
                        <th valign="top">Parent category:</th>
                        <td><select class="form-control" id="parent_cat" name="parent_cat">
                        <option value="">Select Category</option>
                        <?php
                        $main= new users; 
                        echo $main->get_categories();
                        ?>
                        </select>
                        </td>
                        <td width="301"><label class="error" id="parent_cate"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Category name:</th>
                        <td><input type="text" class="form-control" id="provider_category" name="category_name" value="" /></td>
                        <td width="301"><label class="error"  id="category"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Description : </th>
                        <td><textarea rows="" cols="" class="form-control form-textarea" id="message" name="message"></textarea></td>
                        <td><label class="error"  id="detail"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Category Image: </th>
                        <td><input type="file" id="image" name="image" value=""/></td>
                        <td><label class="error"  id="imagea"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Category Banner 1: </th>
                        <td><input type="file" id="banner" name="banner" value=""/></td>
                        <td><label class="error"  id="imagea"></label></td>
                      </tr>
                      <tr>
                        <th valign="top">Category Banner 2: </th>
                        <td><input type="file" id="banner2" name="banner2" value=""/></td>
                        <td></td>
                      </tr>
                      <tr>
                        <th valign="top">Weight: </th>
                        <td><input type="text" id="weight" name="weight" value=""/></td>
                        <td></td>
                      </tr>
                      <tr>
                        <th valign="top">Unit: </th>
                        <td>
                          <select name="unit" id="unit" class="form-control">
                             <option value="Kg">Kg</option>
                            <option value="Gm">Gm</option>
                          </select>
                         </td>
                        <td></td>
                      </tr>
                      <tr>
                        <th>&nbsp;</th>
                        <td valign="top"><input type="submit" name="submit" id="Categorysubmit" value="submit" class="form-submit btn btn-default" />
                          <input type="reset" value="Reset" class="form-reset btn btn-default"  /></td>
                      </tr>
                    </table>
                  </form>
                  
                  <!-- end id-form  --></td>
              </tr>
              <tr>
                <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                <td></td>
              </tr>
            </table>
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>
