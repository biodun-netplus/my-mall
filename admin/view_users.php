<?php 
include("header.php");
error_reporting(0); 
/* User management */
/* Author @Damodar Prasad */
/* Date : 8-4-2015 */
?>
<script type="text/javascript">
function changeStatus(id, status)
{
    $.ajax({
      type: 'POST',
      url: "delete_category.php?sUserId="+id+"&status="+status,
      success: function()
      {
        window.location.reload()
        }
    });
  }
$().ready(function(){
  
  $(".resetPassword").click(function(){
    var user_id=$(this).attr("alt")
    //$(this).parent("td").parent('tr').addClass('RowHighlight');
      $.ajax({
      type: 'POST',
      url: "delete_category.php?user_id="+user_id+"&reset_password=reset_password",
      success: function(data)
      {
          $('#mainform').prepend(data);
        setTimeout(function(){
          $('#mainform .alert').remove();
     },10000)

        }
    });
  })
})

</script>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 

<?php
// delete user....
$msg = '';
if(isset($_GET['id']) && $_GET['id'] != ''){

  /*mysql_query("Delete FROM admin WHERE id = '".base64_decode($_GET['id'])."'");
  $msg = "User has deleted successfully.";*/
  
  $User['isactive']    = 'pd'; // delete pending
  $main                = new users;

  $updatedata= $main->update($User,"admin",base64_decode($_GET['id']));

//    if($updatedata)
//    $msg = "User has deleted successfully.";
 }
// end of delete

$query = "SELECT *  FROM `admin` a WHERE a.`isactive` NOT IN ('a')";
$result = mysql_query($query) or die(mysql_error());
$num_rows = mysql_fetch_row($result);

$pages = new Paginator;
$pages->items_total = $num_rows[0];
$pages->mid_range = 4; // Number of pages to display. Must be odd and > 3
$pages->paginate();
$query = "SELECT a.*, b.username AS created_username from admin a LEFT JOIN admin b ON b.id = a.created_by WHERE a.`isactive` NOT IN ('a') order by id desc $pages->limit";
$result = mysql_query($query) or die(mysql_error());
$countCondition = mysql_num_rows($result);
 ?>
    <div class="row bottom-margin">
      <div id="page-heading" >
        <h1>User</h1>
      </div>
      <div class="add-button col-lg-5"> <a href="add_user.php">
        <input type="button" class="buttons btn btn-info" value="Add New User"/>
        </a> </div>
      <div id="paging-table" class="col-lg-6"><?php echo $pages->display_pages(); ?></div>
    </div>
    <?php if(isset($msg) && $msg!='')  {?>
      <div class="alert alert-success">
        <?php echo $msg;?>
      </div>
    <?php } ?>
    <form id="mainform" action="" class="table-responsive">
      <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="table table-bordered table-hover table-striped">
        <tr>
          <th class="table-header-check"><a id="toggle-all" ></a> </th>
          <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Username</a></th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Email </a></th>
          <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Created By</a></th>
          <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Status </a></th>
          <th width="125" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Action</a></th>
        </tr>
        <?php
    if($countCondition > 0){
      while($data = mysql_fetch_object($result)){ 
                      
    ?>
        <tr>
          <td width="20"><input  type="checkbox" name="checkbox" value="<?php echo $data->id;?>"/></td>
          <td><?php echo $data->username; ?></td>
          <td><?php echo $data->email_id; ?></td>
          <td><?php echo $data->created_username; ?></td>
          <td  align="center" style="padding-left:20px;" class="status">
       
          <?php if($data->isactive=='p'){ ?>
          Pending Create
          <?php } ?>
          
          <?php if($data->isactive=='pd'){ ?>
                 Pending Delete
            <?php } ?>

          <?php if($data->isactive=='t'){ ?>
            <select alt="<?php echo $data->id;?>" onchange="javascript:changeStatus('<?php echo $data->id;?>','f');">
            <option value="t" selected="selected">Activate</option>
            <option value="f">Inactivate</option>
            </select>
            <?php }
            else if($data->isactive=='f'){ ?>
             <select  alt="<?php echo $data->id;?>" onchange="javascript:changeStatus('<?php echo $data->id;?>','t');">
            <option value="f" selected="selected">Inactivate</option>
            <option value="t">Activate</option>
            </select>
            <?php }?></td>
          <td><a class="resetPassword" href="javaScript:;"  alt="<?php echo $data->id;?>" title="Click to reset password"> <img src="images/table/change-password-icon.png"/></a>
          <a href="view_users.php?id=<?php echo base64_encode($data->id);?>" onclick="return confirm('Are you sure, you want to delete this user?')"  alt="<?php echo $data->id;?>" title="Click to Delete user"> <img src="images/table/action_delete.gif"/></a>
            <a href="edit_user.php?id=<?php echo base64_encode($data->id);?>" title="Click to Edit user"> <img src="images/table/action_edit.gif"/></a></td>
        </tr>
        <?php }}else{ ?>
        <tr>
          <td colspan="10" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>
        </tr>
        <?php } ?>
      </table>
      <!--  end product-table................................... -->
    </form>
    <div id="actions-box"> <a href="" class="action-slider"></a>
      <div id="actions-box-slider"> <a href="javascript:;" name="delete" id="delete" class="action-delete" >Delete</a> </div>
      <div class="clear"></div>
    </div>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>