<?php
include("header.php");
include("activities_arr.php");
/* role management */
/* Author @parmeet */
/* Date : 2-6-2015 */

$id  = base64_decode($_GET['id']);

if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{    
	 $role 		= '';  
     if(isset($_REQUEST['permission']) && $_REQUEST['permission'] != '')
	 {
	 	$role = implode('|',$_REQUEST['permission']);
		
	 }
	 $User												= array();
	 $User['role_update_by_user']				= $role;
	 $main												= new users;
	 
	 $update= $main->update($User,"role",$id);
	 if($update)
	 {
		header("location:manage_role.php?id=".base64_encode($id));
	 }
}

$Query = mysql_query("SELECT * FROM role where id = '".$id."'");
$Data = mysql_fetch_object($Query);
 ?>
<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content" style="">
    <div >
      <div id="page-heading" >
        <h1>Update Role</h1>
      </div>
      <div style="float:right; padding-right:20px;">
        <input type="button" class="buttons btn btn-info" value="Back" onclick="window.history.back()" />
      </div>
    </div>
    <div class="row">
      <form action="<?php echo $_SERVER['PHP_SELF'];?>?id=<?php echo base64_encode($id) ;?>" name="form1" method="post" id="UserForm">
        <div class="col-lg-6">
          <div class="table-responsive" style="overflow-y: auto; height: 352px;">
            <table class="table table-bordered table-hover">
              <tbody>
                <?php
			  
			  $activPermis 	=  explode('|', $Data->role);	
			  $activPermis1 	=  explode('|', $Data->role_update_by_user);	
			  foreach($activitiesArr as $key => $value){
			   
			   foreach($activitiesArr[$key] as $key1 => $value1){ 
			   	
				if(!in_array($value1,$activPermis)) continue;
				
			   ?>
                <tr>
                  <td><input type="checkbox" name="permission[]" value="<?php echo $value1; ?>"  <?php if(in_array($value1,$activPermis1)){ echo 'checked="checked"'; } ?>  /></td>
                  <td><?php echo $key1; ?></td>
                  <td><?php echo $key; ?></td>
                </tr>
                <?php
			   }
			  }
			  ?>
              </tbody>
            </table>
          </div>
           <div class="form-group" style="margin-top: 15px;">
            <input type="submit" id="RoleSubmit" value="submit" name="submit" class="form-submit btn btn-default" />
            <input type="reset" value="Reset" class="form-reset btn btn-default"  />
          </div>
        </div>
      </form>
      <!-- end id-form  --> 
    </div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>