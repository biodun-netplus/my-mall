<?php 
include_once('db_connect.php'); 
//error_reporting(E_ALL);
class users {
	var  $sql;
	var  $query;
	var  $result;
	var  $row;

	function dbconnect(){
		 
		$dsn = 'mysql:dbname=' . DB_DATABASE . ';host=' . DB_HOSTNAME;

		//initialize the connection to the database
		$conn = new PDO($dsn, DB_USERNAME, DB_PASSWORD);	
		return $conn;
	}


	//================Start----LoginUSer===============================================//
	function FormatPr($name)
	{
		return strtolower(str_replace(array( '\'','"',',','/','_', ';','(' , ')',':','.','&',"'", '<', '>',' ' ),'-',stripslashes($name)));
	}
	function FormatN($name)
	{
		return strtolower(str_replace(array( '\'', '"', ',' , ';' , '(' , ')' ,':' ,'.','&',"'", '<', '>',' ' ),'',stripslashes($name)));
	}
	function login($username,$password) 
	{
		
		$this->sql= "SELECT * FROM admin WHERE username='".$username."' AND password='" .$password."'" ;
		$this->result= mysql_query($this->sql) or die("There might be some error in login function". mysql_error());
		$num = mysql_num_rows($this->result);
       
		if($num > 0)
		{
			$userinfo =  mysql_fetch_assoc($this->result);
			// check active, in-active user
			if($userinfo['isactive'] == "p" || $userinfo['isactive'] == "f")
			{
				//echo "User has In-actived, please try later!";
				return  $error="User has In-actived, please try later!";
			}
			else
			{
				$_SESSION['id'] = $userinfo['id'];
				$firstLoginInfo = $userinfo['first_time_login'];
				$_SESSION['admin_type'] = $userinfo['admin_type'];
				if(isset($_SESSION['id']))
				{
			   		// audit trail 
					$activity			= "Login";
					$user_id			= $_SESSION['id'];
					$obj				= new users;
					
					// check user for first login time
					$dataValue = array();
                    if($firstLoginInfo==0){
                    	
                       $dataValue['lock_user'] 	= 0;
                       $dataValue['first_time_login'] = 1;

                       $this->update($dataValue,"admin",$user_id);
         		  	   $obj->addAuditTrail("",$activity);
                    	
                       header("location:update_setting.php");
                    }
                    else { 
                    	
                    	$this->update(array('lock_user' => 0 ),"admin",$user_id);
         		  	    $obj->addAuditTrail("",$activity);
                        
                        header("location:dashbord.php");
                    }
								
				}
			}
		}
		else
		{
			// count wrong attempt for password and locked after 3 times
			$this->sql= "SELECT * FROM admin WHERE username='".$username."'" ;
			$this->result= mysql_query($this->sql) or die("There might be some error in login function". mysql_error());
			$num = mysql_num_rows($this->result);
			if($num > 0)
			{
				$lock_time  = 0 ;
				$userinfo =  mysql_fetch_assoc($this->result);
				if($userinfo['password'] != $password )
				{
					
					$data = array();
					if($userinfo['lock_user']  > 2)
					{
						$data['isactive'] 	= "f";
                        $data['lock_user'] 	= $userinfo['lock_user'] +1;
						$lock_time = $lock_time + 1;
					}
					else
					{
						$data['lock_user'] = $userinfo['lock_user'] +1;
					}

					$this->update($data,"admin",$userinfo['id']);
					if($lock_time > 0) 
					 	{ return  $error="User account has been deactivated"; }
					 
				}
				
			}
			
			echo "User name  and Password did not match";
			return  $error="User name  and Password did not match";
		}
	}
	function fedex_login($username,$password)
	{
		$this->sql= "SELECT * FROM admin WHERE username='".$username."' AND password='" .$password."'" ;
		$this->result= mysql_query($this->sql) or die("There might be some error in login function". mysql_error());
		$num= mysql_num_rows($this->result);
		if($num > 0)
		{
			$userinfo =  mysql_fetch_assoc($this->result);
			$_SESSION['FedExId'] = $userinfo['id'];
			if(isset($_SESSION['FedExId']))
			{
		   		header("location:FedEx_dashboard.php");
			}
		}
		else
		{
			echo "User name  and Password did not match";
			return  $error="User name  and Password did not match please Click Forget password";
		}
	}
	
	function mail_attachment($mailto, $from_mail,$subject, $message)
	{
		$header = 'MIME-Version: 1.0' . "\r\n";
		$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$header .= "From: <".$from_mail.">\r\n";
		$header .= "MIME-Version: 1.0\r\n";
		$header .= $message."\r\n\r\n";
		
		if (mail($mailto, $subject, "", $header)) {
		   return "mail send ... OK"; 
		} else {
		   return ''; //echo "mail send ... ERROR!";
    	}
	}
	
	function ShowPrice($Value)
	{
		//return number_format(round($Value),2, '.', '');
		return round($Value);
	}
	
//================End----LoginUSer=======================================================//
//================Start---ADD===============================================//
	function ADD($Users,$table_name)
	{
		
		$this->sql= "INSERT INTO $table_name SET ";
		foreach($Users as $keys=> $values)
		{
			$this->sql .= $keys ."= '".mysql_real_escape_string($values)."',";
		}	
		$this->sql = substr($this->sql,0,-1);
		$this->query = mysql_query($this->sql) or die("There is some error". mysql_error());	
		if($this->query )
		{
			$this->addAuditTrail($table_name);
		}
		return $this->query;
	}
//================End----ADD==================================================//
	
	
	//=========--ADD===============================//
	
	function addAuditTrail($tableNames,$activity){
		
		if($tableNames != '')
		{
			
			/** role **/
			if($tableNames == 'role'){
				$activity				= "Add Role";
			}
			
			/** admin user **/
			if($tableNames == 'admin'){
				$activity				= "Add Sub admin";
			}
			
			/** category **/
			if($tableNames == 'category'){
				$activity				= "Add category";
			}
			if($tableNames == 'sub_category'){
				$activity				= "Add sub category";
			}
			if($tableNames == 'cat_size'){
				$activity				= "Add sub size";
			}
			/*if($tableNames == 'cat_size'){
				$data['activity']			= "Add sub size";
			}*/
			
			/** merchant **/
			if($tableNames == 'users'){
				$activity				= "Add merchant";
			}

			/** products **/
			if($tableNames == 'products'){
				$activity				= "Add Product";
			}
			
			/** deal **/
			if($tableNames == 'deal'){
				$activity				= "Add Deal";
			}

			/** color_or_brands **/
			if($tableNames == 'color_or_brands'){
				$activity				= "Add Color Or Brands";
			}

			/** home-page-slider **/
			if($tableNames == 'home-page-slider'){
				$activity				= "Add Home Page Slider";
			}

			/** home-page-slider **/
			if($tableNames == 'home-page-sidebar'){
				$activity				= "Add Home Page Sidebar";
			}

			/** sponsor **/
			if($tableNames == 'sponsor'){
				$activity				= "Add Sponsor";
			}

			/** fedex **/
			if($tableNames == 'fedex'){
				$activity				= "Add Fedex";
			}

			/** merchant_fedex_location **/
			if($tableNames == 'merchant_fedex_location'){
				$activity				= "Add Merchant Fedex Location";
			}

			/** merchant class cast **/
			if($tableNames == 'merchant_class_cast'){
				$activity				= "Add Merchant Class Cast";
			}

			/** meta tag **/
			if($tableNames == 'meta_tag'){
				$activity				= "Add Meta Tag";
			}

			
		}
		$user_id				= $_SESSION['id'];
		$ip_address 			= $this->get_client_ip();

		mysql_query("INSERT INTO  audit_trail (`user_id` , `ip`, `activity`,field_id) VALUES ('".$user_id."', '".$ip_address."',  '".$activity."', '0')") or die("There is some error". mysql_error());
		
	
	}
	
	//=========--end of function ===========================//
	
	//=========--updateAuditTrail ===============================//
	
	function updateAuditTrail($tableNames,$activity, $field_id = 0){
		
		if($tableNames != '')
		{
			
			/** role **/
			if($tableNames == 'role'){
				$activity				= "Update Role";
			}
			
			/** admin user **/
			if($tableNames == 'admin'){
				$activity				= "Update Sub admin";
			}
			
			/** category **/
			if($tableNames == 'category'){
				$activity				= "Update Category";
			}
			if($tableNames == 'sub_category'){
				$activity				= "Update Sub Category";
			}
			if($tableNames == 'cat_size'){
				$activity				= "Update sub size";
			}
			
			/** merchant **/
			if($tableNames == 'users'){
				$activity				= "Update Merchant";
			}

			/** products **/
			if($tableNames == 'products'){
				$activity				= "Update Product";
			}
			
			/** deal **/
			if($tableNames == 'deal'){
				$activity				= "Update Deal";
			}

			/** color_or_brands **/
			if($tableNames == 'color_or_brands'){
				$activity				= "Update Color Or Brands";
			}

			/** home-page-slider **/
			if($tableNames == 'home-page-slider'){
				$activity				= "Update Home Page Slider";
			}

			/** home-page-slider **/
			if($tableNames == 'home-page-sidebar'){
				$activity				= "Update Home Page Sidebar";
			}

			/** sponsor **/
			if($tableNames == 'sponsor'){
				$activity				= "Update Sponsor";
			}

			/** fedex **/
			if($tableNames == 'fedex'){
				$activity				= "Update Fedex";
			}

			/** merchant_fedex_location **/
			if($tableNames == 'merchant_fedex_location'){
				$activity				= "Update Merchant Fedex Location";
			}

			/** merchant class cast **/
			if($tableNames == 'merchant_class_cast'){
				$activity				= "Update Merchant Class Cast";
			}

			/** meta tag **/
			if($tableNames == 'meta_tag'){
				$activity				= "Update Meta Tag";

			}

			
		}


		$user_id				= $_SESSION['id'];
		$ip_address 			= $this->get_client_ip();


		mysql_query("INSERT INTO  audit_trail (`user_id` , `ip`, `activity`, `field_id`) VALUES ('".$user_id."', '".$ip_address."', '".$activity."', '".$field_id."')") or die("There is some error". mysql_error());	
		
	}
	
	//=========--end of function ===========================//

//================Start----View All row===============================================//
	
	function view($table_name)
	{
		$this->sql="SELECT * FROM $table_name";
		$this->query=mysql_query($this->sql) or die ("There might be some error in viewprofile function". mysql_error());
		while($this->result = mysql_fetch_object($this->query))
		{
			$result[] = $this->result;
		}
		return $result;
	}
    function view_isactive_cat($table_name,$parent_id,$status)
    {
        $this->sql="SELECT * FROM $table_name WHERE parent_id = '$parent_id' AND isactive='$status'";
        $this->query=mysql_query($this->sql) or die ("There might be some error in viewprofile function". mysql_error());
        while($this->result = mysql_fetch_object($this->query))
        {
            $result[] = $this->result;
        }
        return $result;
    }
    function viewCat()
    {
        $this->sql="SELECT * FROM category WHERE parent_id=0 and isactive='t'";
        $this->query=mysql_query($this->sql) or die ("There might be some error in viewprofile function". mysql_error());
        while($this->result = mysql_fetch_object($this->query))
        {
            $result[] = $this->result;
        }
        return $result;
    }
	function Count($table_name)
	{
		$this->sql="SELECT * FROM $table_name";
		$this->query=mysql_query($this->sql) or die ("There might be some error in viewprofile function". mysql_error());
		$num= mysql_num_rows($this->result);
		return $num;
	}
//================Start----View All row===============================================//
	
	function viewcondition($table_name,$condition)
	{
		$this->sql="SELECT * FROM $table_name";
		$this->query=mysql_query($this->sql) or die ("There might be some error in viewprofile function". mysql_error());
		while($this->result = mysql_fetch_object($this->query))
		{
			$result[] = $this->result;
		}
		return $result;
	}
	function Countcondition($table_name)
	{
		$this->sql="SELECT * FROM $table_name";
		$this->query=mysql_query($this->sql) or die ("There might be some error in viewprofile function". mysql_error());
		$num= mysql_num_rows($this->result);
		return $num;
	}
//================End----All row================================================//
	 function GeneratePass($length){
			    $code = "";
			    $possible = "012346789ABCDEFGHIGKLMNOPQRSTUVWXYZ";
    			$maxlength = strlen($possible);
   				if ($length > $maxlength) {
				$length = $maxlength;
				}
    		$i = 0; 
   		 while ($i < $length) { 
      		$char = substr($possible, mt_rand(0, $maxlength-1), 1);
		      if (!strstr($code, $char)) { 
       			 $code .= $char;
      		  $i++;
     		 }
   		 }
    return $code;
  }
	function ViewSate($table_name,$id)
	{
		$this->sql="SELECT * FROM $table_name where country_id = '".$id."'";
		$this->query=mysql_query($this->sql) or die ("There might be some error in viewprofile function". mysql_error());
		while($this->result = mysql_fetch_object($this->query))
		{
			$result[] = $this->result;
		}
		return $result;
	}

	function ViewLga($table_name,$id)
	{

		$this->sql="SELECT * FROM $table_name where state_id = '".$id."'";
		$this->query=mysql_query($this->sql) or die ("There might be some error in viewprofile function". mysql_error());
		while($this->result = mysql_fetch_object($this->query))
		{
			$result[] = $this->result;
		}
		return $result;
	}
	function ViewCity($table_name,$id=NULL)
	{
		$this->sql="SELECT * FROM $table_name where state_code = '".$id."'";
		$this->query=mysql_query($this->sql) or die ("There might be some error in viewprofile function". mysql_error());
		while($this->result = mysql_fetch_object($this->query))
		{
			$result[] = $this->result;
		}
		return $result;
	}
//================Start----ViewProfile===============================================//
	
	function viewprofile($id,$table_name)
	{
		$this->sql="SELECT * FROM $table_name where id = '".$id."' ";

		$this->query=mysql_query($this->sql) or die ("There might be some error in viewprofile function". mysql_error());
		$this->result = mysql_fetch_object($this->query);
		return $this->result;
	}
	function viewisactive($status,$table_name)
	{
		$this->sql="SELECT * FROM $table_name where isactive = '".$status."' ";
		$this->query=mysql_query($this->sql) or die ("There might be some error in viewprofile function". mysql_error());
		while($this->result = mysql_fetch_object($this->query))
		{
			$result[]=$this->result;
		}
		return $result;
	}
//================End----ViewProfile================================================//
//================Start----deleterow===============================================//
	
	function delete($id,$table_name)
	{
		$sel= mysql_query("select * from $table_name where id= '".$id."'");
		$data= mysql_fetch_object($sel);
		$img= ($data->image);
		unlink('../product_images/'.$img);
		unlink('../category_images/'.$img);
		$this->sql="DELETE FROM $table_name where id = '".$id."' ";
		$this->query=mysql_query($this->sql) or die ("There might be some error in delete function". mysql_error());
		return $this->query;
	}
	function deleteMultiple($id,$table_name)
	{
	  	$this->sql="DELETE FROM $table_name where id IN (".$id.") ";  
		$this->query=mysql_query($this->sql) or die ("There might be some error in delete function". mysql_error());
		return $this->query;
	}
	
	function deleteSingleRow($id,$table_name)
	{
	  	$this->sql="DELETE FROM $table_name where  id = '".$id."' ";  
		$this->query=mysql_query($this->sql) or die ("There might be some error in delete function". mysql_error());
		return $this->query;
	}

       function activeUserDel($table_name,$id)
	{
                $this->sql="DELETE FROM $table_name WHERE id = '".$id."' AND isactive = 't' OR isactive = 'f' ";  
		$this->query=mysql_query($this->sql) or die ("There might be some error in delete function". mysql_error());
		return $this->query;
	}
//================End----deleterow===============================================//
	
//================Start----Update===============================================//
	function update($arrRecords,$table_name,$id)
	{
		 	
		$field = "";
		$where = "";

		$record = $this->viewprofile($id,$table_name);

	 	$this->sql= "UPDATE $table_name SET ";

        foreach($arrRecords as $keys => $values)
		{
			
			if($record->$keys  != mysql_real_escape_string($values)){
				$field .= "`".$keys ."`,";
			}
			$this->sql .= $keys ." = '".mysql_real_escape_string($values)."',";
		}
		$this->sql= substr($this->sql,0,-1);
		$this->sql .=" WHERE id=". $id ;
		$where .=" WHERE id=". $id ;
//		echo $this->sql;die;
		// 
		$field = substr($field,0,-1);
		$tableName = str_replace('`','',$table_name)."_log"; 
		//echo "INSERT INTO  `".$tableName."` (`id`, ".$field.") SELECT  `id`, ".$field." FROM ".$table_name. $where; 
		mysql_query("INSERT INTO  `".$tableName."` (`id`, ".$field.") SELECT  `id`, ".$field." FROM ".$table_name. $where);  
		$field_id = mysql_insert_id();

		$this->query=mysql_query($this->sql);
		
		if($this->query)
		{
//			$this->updateAuditTrail($table_name,"",$field_id);
		}
		
		return $this->query;
	}

	function statusupdate($table_name,$id,$row)
	{
                $this->sql= "UPDATE $table_name SET  isactive='".$row."' where  id='".$id."'"; 
		$this->query=mysql_query($this->sql);
		return $this->query;
	}
    function update11($table_name,$key,$data,$parameter,$value)
    {
        $this->sql= "UPDATE $table_name SET  ".$key."='".$data."' where  ".$parameter."='".$value."'";
        echo $this->sql;
        $this->query=mysql_query($this->sql);
        return $this->query;
    }
	function statusupdate11($table_name,$id,$row)
	{
		$this->sql= "UPDATE $table_name SET  status='".$row."' where  id='".$id."'"; 
		$this->query=mysql_query($this->sql);
		return $this->query;
	}
//================End----Update===============================================//
	
//================Start----View All row===============================================//
	
	function viewwhere($row,$table_name)
	{
		$table2="category";
		$this->sql="SELECT c.category_name,u.* FROM $table_name AS u INNER JOIN $table2 AS c ON u.category=c.id where user_type='".$row."'";
		$this->query=mysql_query($this->sql) or die ("There might be some error in viewprofile function". mysql_error());
		while($this->result = mysql_fetch_object($this->query)){
		$result[] = $this->result;
		}
		return $result;
	}
	//==================Product detail============================================//
	function product_details($Product,$table_name)
	{
		$this->sql= "INSERT INTO $table_name SET ";
		foreach($Product as $keys=> $values)
		{
			$this->sql .= $keys ."= '".$values."',";
		}		
		$this->sql = substr($this->sql,0,-1);
		$this->query = mysql_query($this->sql) or die("There is some error". mysql_error());	
		return $this->query;
	}
	//==============================Provider details=================================//
		function select($table_name,$table_name2)
		{
			$result=array();
		 	$this->sql="SELECT c.category_name,p.* FROM $table_name as p INNER JOIN $table_name2 as c ON p.category = c.id";
			$this->query=mysql_query($this->sql) or die ("There might be some error in viewprofile function". mysql_error());
			while($this->result = mysql_fetch_object($this->query))
			{
				$result[] = $this->result;
			}
		 	return $result;
		}
	//===============Buyer Detail=============================================//
	
	function select_buyer($type,$table_name){
		$result=array();
		$this->sql="SELECT * FROM $table_name where user_type='".$type."'";
		$this->query=mysql_query($this->sql) or die ("There might be some error in viewprofile function". mysql_error());
		while($this->result = mysql_fetch_object($this->query)){
		 
		$result[] = $this->result;
		}
		return $result;
	}
		
//================End----All row================================================//
	
//================Start----Change password===============================================//

		
	
	function Changepassword($arrRecords,$table_name,$id)
	{
		$this->sql= "UPDATE $table_name SET ";
		foreach($arrRecords as $keys => $values){
			$this->sql .= $keys ."= '".$values."',";
			}
		$this->sql= substr($this->sql,0,-1);
		$this->sql .=" WHERE id=". $id  ;
		$this->query=mysql_query($this->sql);
		return $this->query;
	}
	
	function RelateProduct()
	{
		
		
	}
	function SelectAll($table=NULL)
	{
		$sesson = session_id();
		$ReturnArray=array();
		//echo "SELECT * FROM $table where user_id = '".$_SESSION['user_id']."' and cart_session='".$sesson."' and order_id IS NULL ";
		$this->sql = mysql_query("SELECT * FROM $table where user_id = '".$_SESSION['user_id']."' and cart_session='".$sesson."' and order_id IS NULL ");
		while($this->query = mysql_fetch_object($this->sql))
		{
			$ReturnArray[] = $this->query;
		}
		return $ReturnArray;
	}
	function SelectAll2($table,$OrderID)
	{
		$sesson = session_id();
		$ReturnArray=array();
		$this->sql = mysql_query("SELECT * FROM $table where user_id = '".$_SESSION['user_id']."' and cart_session='".$sesson."' and order_id ='".$OrderID."' ");
		while($this->query = mysql_fetch_object($this->sql))
		{
			$ReturnArray[] = $this->query;
		}
		return $ReturnArray;
	}
//================End----Update===============================================//

//================Search function===============================================//

	function Search($tp,$type)
	{
		$this->sql="SELECT cn.name as countryName, u.*,s.name as stateName, cat.category_name as CatName, c.name as cityName FROM `users` AS u INNER JOIN `cities` AS c ON u.city = c.id INNER JOIN `states` AS s ON u.state = s.id INNER JOIN `countries`AS cn ON u.country = cn.id LEFT JOIN category AS cat ON cat.id = u.category  where u.first_name like '%$type%' && user_type = '".$tp."'";
		$this->query=mysql_query($this->sql) or die ("There might be some error in viewprofile function". mysql_error());
		while($this->result = mysql_fetch_object($this->query)){
		 
		$result[] = $this->result;
		}
		return $result;
	}
	
	function getValues($query=NULL)
	{
		$sesson = session_id();
		$ReturnArray=array();
		//echo "SELECT * FROM $table where user_id = '".$_SESSION['user_id']."' and cart_session='".$sesson."' and order_id IS NULL ";
		$this->sql = mysql_query($query);
		while($this->query = mysql_fetch_object($this->sql))
		{
			$ReturnArray[] = $this->query;
		}
		return $ReturnArray;
	}
	
	//// for currency type
	function getCurrency($currency)
	{
		switch($currency)
		{
			case 'Naira':
			return "&#8358;";
			break;
			
			case 'Doller':
			return "$";
			break;
			
			case 'Euro':
			return "&euro;";
			break;
			
			case 'Pounds':
			return "&pound;";
			break;
					
			default:
			return "$";
			break;
		}
			
	}
//================End----Update===============================================//
	
	//================  Get categories ===============================//
	function get_categories($parent = 0, $selected = "")
	{
	    $html  = "";
	    $getParent = "";
	    $query = mysql_query("SELECT * FROM `category` WHERE `parent_id` = '$parent' ");
	    
	    while($row = mysql_fetch_assoc($query))
	    {
	       
	         
	        $current_id = $row['id'];

	        if($row['parent_id'] > 0 )
	        {
	          
	          $getParent = (array_reverse(explode(">", $this->get_sub_level($row['parent_id']))));
	          $getParent = implode(" > ", $getParent);
	          $getParent = substr($getParent, 2)." > ";
	        }

	        $html .= '<option value="'.$row['id'].'"';

	        if($selected ==  $row['id'])
	        {
	        	$html .= ' selected="selected" ';
	    	}

	        $html .= ' >' . $getParent. $row['category_name'];

	        $has_sub = NULL;
	        $has_sub = mysql_num_rows(mysql_query("SELECT COUNT(`parent_id`) FROM `category` WHERE `parent_id` = '$current_id'"));

	        if($has_sub)
	        {
	            
	            $html .= $this->get_categories($current_id, $selected); 

	        }
	        $html .= '</option>';
	         
	    }

	    return $html;
	} 

	function get_sub_level($parent = 0)
	{
	    $html  = "";
	    $getParent = "";
	    $query = mysql_query("SELECT * FROM `category` WHERE `id` = '$parent' ");
	    while($row = mysql_fetch_assoc($query))
	    {
	       
	        $current_id = $row['parent_id'];
	        $html      .= $row['category_name'];

	        $has_sub = NULL;
	        $has_sub = mysql_num_rows(mysql_query("SELECT COUNT(`id`) FROM `category` WHERE `id` = '$current_id' AND parent_id > 0 "));
	       
	        //die();

	        if($has_sub)
	        {
	            
	            $html .= ">". $this->get_sub_level($current_id); 

	        }
	    }

	    return $html;
	} 

    function buildTree($items) {

	    $childs = array();

	    foreach($items as &$item)
	    { 
	    	$childs[$item['parent_id']][] = &$item;
		}
	    unset($item);

	    foreach($items as &$item) {
	    	if (isset($childs[$item['id']])){
	            $item['childs'] = $childs[$item['id']];
	    	}
		}
		//$item['childs'] = "Neeraj";
		//print_r($childs[0]);
		$ocean_item = array();
		$final = $childs[0];
		$cat_name = '';
		$j = 0;
		for($i = 0; $i < count($final); $i++ ){
			$ocean_item[$j] = $final[$i];
			//$ocean_item[$j]['set_name'] = $final[$i]['category_name'];
			$cat_name = $final[$i]['category_name'];
			if(isset($final[$i]['childs'])){
				$j++;
				$final1 = $final[$i]['childs'];
				for($i1 = 0; $i1 < count($final1); $i1++ ){
					$ocean_item[$j] = $final1[$i1];
					$cat_name .= " > ".$final1[$i1]['category_name'];
					//$ocean_item[$j]['set_name'] = $cat_name;
					if(isset($final1[$i1]['childs'])){
						$j++;
						$final2 = $final1[$i1]['childs'];
						for( $i2 = 0; $i2 < count($final2); $i2++ ){
							$ocean_item[$j] = $final2[$i2];
							$cat_name .= " > ".$final2[$i2]['category_name'];
							//$ocean_item[$j]['set_name'] = $cat_name;
							if(isset($final2[$i2]['childs'])){
								$j++;
								$final3 = $final2[$i2]['childs'];
								for($i3 = 0; $i3 < count($final3); $i3++ ){
									$ocean_item[$j] = $final3[$i3];
									$cat_name .= " > ".$final3[$i3]['category_name'];
									//$ocean_item[$j]['set_name'] = $cat_name;
									if(isset($final3[$i3]['childs'])){
										$j++;
										$final4 = $final3[$i3]['childs'];
										for($i4 = 0; $i4 < count($final4); $i4++ ){
											$ocean_item[$j] = $final4[$i4];
											$cat_name .= " > ".$final4[$i4]['category_name'];
											//$ocean_item[$j]['set_name'] = $cat_name;
											if(isset($final4[$i4]['childs'])){
												$j++;
												$final5 = $final4[$i4]['childs'];
												for($i5 = 0; $i5 < count($final5); $i5++ ){
													$ocean_item[$j] = $final5[$i5];
													$cat_name .= " > ".$final5[$i5]['category_name'];
													//$ocean_item[$j]['set_name'] = $cat_name;
													if(isset($final5[$i5]['childs'])){
														$j++;
														$final6 = $final5[$i5]['childs'];
														for($i6 = 0; $i6 < count($final6); $i6++ ){
															$ocean_item[$j] = $final6[$i6];
															$cat_name .= " > ".$final6[$i6]['category_name'];
															//$ocean_item[$j]['set_name'] = $cat_name;
															
														}
													}
													$j++;
													
												}
											}
											$j++;
											
										}
									}
									$j++;
									
								}
							}
							$j++;
						}
					}
					$j++;
				}
			}
			$j++;
		}

		//print_r($ocean_item);
	    return $ocean_item;
	}

	function get_cat_list(){
		$categories = array();
		$sql = "SELECT c1.* FROM `category` c1 ";
		//$sql .= " AND c1.parent_id = 0";
		
//        echo $sql;
		$res = mysql_query($sql);
		
		if( mysql_num_rows($res) > 0 ){
			$i = 0;
			$items = array();
		//	print_r(mysql_fetch_array($res));
			while( $row = mysql_fetch_assoc($res) )
    		{
    			$categories[$i]['id'] 		 = $row['id'];
    			$categories[$i]['name'] 	 = $row['category_name'];
    			$categories[$i]['image'] 	 = $row['image'];
    			$categories[$i]['banner'] 	 = $row['banner'];
    			$categories[$i]['parent_id'] = 0;
				$items[] 					 = $row;
			}

			$ocean = $this->buildTree($items); //print_r($ocean);
//            var_dump($ocean);
			return $ocean;
			
		} else {
			return FALSE;
		}
	}

    // Function to get the client IP address
	function get_client_ip() {
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';

	    return $ipaddress;
	}
}

function sql_injection($field){
	return mysql_real_escape_string($field);
}