<?php 
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */

	// Database connection class
	class DatabaseConnectClass
	{

		public $database_hostname;
		public $database_username;
		public $database_password;
		public $database_name;

		// Create a connection to the database server
		public function databaseConnection($objDatabaseConnect)
		{
			$this->_database_connection =
			mysql_connect($this->database_hostname,
						   $this->database_username,
						   $this->database_password)
			or trigger_error(mysql_error(),E_USER_ERROR);
			return $this->_database_connection;
		}

		// Select the database and open it
		public function databaseConnectionSelect()
		{
			$this->_database_connection_select =
			mysql_select_db($this->database_name,
							$this->_database_connection);
			return $this->_database_connection_select;
		}

		// Call all the database connection objects
		public function databaseConnectionProcess($objDatabaseConnect)
		{
			$objDatabaseConnect->databaseConnection($objDatabaseConnect);
			$objDatabaseConnect->databaseConnectionSelect($objDatabaseConnect);
		}

		// Build main object method
		public function databaseConnectionMain($objDatabaseConnect)
		{
			$objDatabaseConnect->databaseConnectionProcess($objDatabaseConnect);
		}

	}
	?>