<?php
ob_start();

include("includes/classes/functions.php");
include("includes/classes/upload.php");
include('paginator.class.php');
include('check_permission.php');

if (!isset($_SESSION['id'])) {
    echo "<script>window.location.href='index.php'</script>";
}
//$site_url = 'https://www.mymall.com.ng/admin/';
//$siteUrl  = 'https://www.mymall.com.ng/';
$site_url = 'http://localhost/mymall/admin/';
$siteUrl  = 'http://localhost/mymall/';
//$site_url = 'http://www.webmallnglab.com/ecomall/admin/';
//
//$siteUrl  =  'http://www.webmallnglab.com/ecomall/';;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MyMall - Online Store Solution: Admin</title>
    <link rel="shortcut icon" href="http://localhost/mymall/assets/img/favicon.png" type="image/x-icon" />
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo $site_url; ?>css/themes/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo $site_url; ?>css/themes/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo $site_url; ?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css">

<!--     HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="js/themes/jquery.js"></script>-->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!--    <script src="plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>-->
    <script src="plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery -->

    <!-- Bootstrap Core JavaScript -->
    <script src="js/themes/bootstrap.min.js"></script>
    <?php
	$styleopen			= '<style type="text/css">';
	$styleClose			= '</style>';
	$styles				= "";
    
	if($request_url == 'provider.php' && !in_array('deleteMerchant', $UserRole)){
		$styles	.= '.delete { display: none; } ';
	}
	if($request_url == 'provider.php' && !in_array('manageStatusMerchant', $UserRole)){
		$styles	.= '.icon-2.statusChange {  cursor: default;  pointer-events: none; } ';
	}
	if($request_url == 'provider.php' &&  !in_array('viewMerchant', $UserRole)){
		$styles	.= 'a[title="Click to view Provider information"] { display: none; } ';
	}
	if($request_url == 'provider.php' &&  !in_array('editMerchant', $UserRole)){
		$styles	.= 'a[title="Click to Edit provider"] { display: none; } ';
	}
	if($request_url == 'provider.php' &&  !in_array('sendMailToMerchant', $UserRole)){
		$styles	.= 'a[title="Click to send mail from provider"] { display: none; } ';
	}
	if($request_url == 'provider.php' &&  !in_array('aboutus', $UserRole)){
		$styles	.= 'input[value="About Us"] { display: none; } ';
	}
	if($request_url == 'provider.php' &&  !in_array('addBanner', $UserRole)){
		$styles	.= 'input[value="Shop banner\'s"] { display: none; } ';
	}
	if($request_url == 'provider.php' &&  !in_array('viewMeta', $UserRole)){
		$styles	.= 'input[value="Add Meta And Title"] { display: none; } ';
	}
	if($request_url == 'provider.php' && !in_array('manageStatusMerchant', $UserRole)){
		$styles	.= '.icon-5.statusChange {  cursor: default;  pointer-events: none; } ';
	}

	/*******  category  ********/
	if($request_url == 'catergory.php' && !in_array('deleteCatergory', $UserRole)){
		$styles	.= '.delete { display: none; }';
	}
	if($request_url == 'catergory.php' && !in_array('deleteCatergory', $UserRole)){
		$styles	.= 'a[title="Click to view category"] { display: none; } ';
	}
	if($request_url == 'catergory.php' && !in_array('addCatergory', $UserRole)){
		$styles	.= 'input[value="Add New Category"]{ display: none; } ';
	}
	if($request_url == 'catergory.php' && !in_array('editCatergory', $UserRole)){
		$styles	.= 'a[title="Click to edit category"] { display: none; }  ';
	}
	if($request_url == 'catergory.php' && !in_array('addSubCatergory', $UserRole)){
		$styles	.= 'a[title="Click to Add Sub-Category"] { display: none; }  ';
	}
	if($request_url == 'catergory.php' && !in_array('viewSubCatergory', $UserRole)){
		$styles	.= 'a[title="Click to Views Sub-Category"] { display: none; }  ';
	}
	if($request_url == 'catergory.php' && !in_array('addCatergorySize', $UserRole)){
		$styles	.= 'a[title="Click to Add Size "] { display: none; }  ';
	}
	if($request_url == 'catergory.php' && !in_array('viewCatergorySize', $UserRole)){
		$styles	.= 'a[title="Click to View Size "] { display: none; }  ';
	}
	
	if($request_url == 'view_sub_cat.php' && !in_array('deleteSubCatergory', $UserRole)){
		$styles	.= '.delete { display: none; }';
	}
	if($request_url == 'view_sub_cat.php' && !in_array('editSubCatergory', $UserRole)){
		$styles	.= 'a[title="Click To Edit Sub Category"] { display: none; }  ';
	}
	if($request_url == 'view_sub_cat.php' && !in_array('addSubCatergory', $UserRole)){
		$styles	.= 'input[value="Add Sub Category"] { display: none; }  ';
	}

	if($request_url == 'view_subcat_size.php' && !in_array('deleteCatergorySize', $UserRole)){
		$styles	.= '.delete { display: none; }';
	}
	if($request_url == 'view_subcat_size.php' && !in_array('editCatergorySize', $UserRole)){
		$styles	.= 'a[title="Click To Edit Sub Category"] { display: none; }  ';
	}
	if($request_url == 'view_subcat_size.php' && !in_array('addCatergorySize', $UserRole)){
		$styles	.= 'input[value="Add Categories Sizes"] { display: none; }  ';
	}
	
	/*******  product  ********/
    if($request_url == 'product.php' && !in_array('deleteProduct', $UserRole)){
		$styles	.= '.delete { display: none; }';
	}
	if($request_url == 'product.php' && !in_array('manageStatusProduct', $UserRole)){
		$styles	.= '.status select {  cursor: default;  pointer-events: none; } ';
	} 
	if($request_url == 'product.php' && !in_array('editProduct', $UserRole)){
		$styles	.= 'a[title="Click to Edit Product"] { display: none; }  ';
	}
	if($request_url == 'product.php' && !in_array('viewProduct', $UserRole)){
		$styles	.= 'a[title="Click to view Product"] { display: none; }  ';
	}
	if($request_url == 'product.php' && !in_array('addProduct', $UserRole)){
		$styles	.= 'input[value="Add New Product"] { display: none; }  ';
	}
	
	/*******  Buyer  ********/
	
	if($request_url == 'buyer.php' && !in_array('exportBuyer', $UserRole)){
		$styles	.= 'input[value="Export"] { display: none; }  ';
	}
	if($request_url == 'buyer.php' && !in_array('deleteBuyer', $UserRole)){
		$styles	.= '.delete { display: none; }';
	}
	
	/*******  deal  ********/
	
	if($request_url == 'deal.php' && !in_array('addBuyer', $UserRole)){
		$styles	.= 'input[value="Add New Deals"] { display: none; }  ';
	}
	if($request_url == 'deal.php' && !in_array('editBuyer', $UserRole)){
		$styles	.= 'a[title="Click to Edit Product"] { display: none; }  ';   
	}
	
	/*******  color-brands  ********/
	/*if($request_url == 'color-brands.php' && !in_array('deleteColorBrand', $UserRole)){
		$styles	.= '.delete { display: none; }';
	}
	if($request_url == 'color-brands.php' && !in_array('editColorBrand', $UserRole)){
		$styles	.= 'a[title="Click To Edit Sub Category"] { display: none; }  ';   
	}
	if($request_url == 'color-brands.php' && !in_array('addColor', $UserRole)){
		$styles	.= 'input[value="Add Brand"] { display: none; }  ';   
	}
	if($request_url == 'color-brands.php' && !in_array('addBrand', $UserRole)){
		$styles	.= 'input[value="Add Color"] { display: none; }  ';   
	} */
	
	/*********   home-page-slider ***************/
	if($request_url == 'home-page-slider-image.php' && !in_array('deleteHomePageSliderImage', $UserRole) ){
		$styles	.= 'a[title="Delete"]{ display: none; } .status{ color: #fff;}';
	}
	if($request_url == 'home-page-slider-image.php' && !in_array('editHomePageSliderImage', $UserRole) ){
		$styles	.= 'a[title="Edit"]{ display: none; } .status{ color: #fff;}';
	}
	if($request_url == 'home-page-slider-image.php' && !in_array('addHomePageSliderImage', $UserRole) ){
		$styles	.= 'input[value="Add Slider Images"]{ display: none; }';
	}
	
	/*********   home-page-sidebar ***************/
	if($request_url == 'home-page-sidebar-image.php' && !in_array('addHomePagesidebarImage', $UserRole) ){
		$styles	.= 'input[value="Add Sidebar Images"]{ display: none; }';
	}
	if($request_url == 'home-page-sidebar-image.php' && !in_array('editHomePagesidebarImage', $UserRole) ){
		$styles	.= 'a[title="Edit"]{ display: none; }';
	}
	if($request_url == 'home-page-sidebar-image.php' && !in_array('deleteHomePagesidebarImages', $UserRole) ){
		$styles	.= 'a[title="Delete"]{ display: none; }';
	}
	
	/*********   fedex ***************/
	if($request_url == 'fedex.php' &&  !in_array('deleteFedExNGShipping', $UserRole)){
		$styles	.= '.delete { display: none; }';
	}
	if($request_url == 'fedex.php' &&  !in_array('addFedExNGShipping', $UserRole)){
		$styles	.= 'input[value="Add FedEx NG Shipping"]{ display: none; }'; 
	}
	if($request_url == 'fedex.php' &&  !in_array('editFedExNGShipping', $UserRole)){
		$styles	.= 'a[title="Click To Edit FedEx NG"]{ display: none; }'; 
	}
	
	if($request_url == 'm_location_list.php' && !in_array('addMerchantFedExNGShipping', $UserRole)){
		$styles	.= 'input[value="Add Merchant FedEx NG Shipping Area"]{ display: none; }'; 
	}
	if($request_url == 'm_shipcast_list.php' && !in_array('addMerchantFedExNGCast', $UserRole)){
		$styles	.= 'input[value="Add Merchant FedEx NG Cast"]{ display: none; }'; 
	}

	/*********   Role ***************/
	if($request_url == 'role.php' &&  !in_array('deleteRole', $UserRole) ){
		$styles	.= 'a[title="Click to Delete Role"]{ display: none; }';
	}
	if($request_url == 'role.php' &&  !in_array('addRole', $UserRole) ){
		$styles	.= 'input[value="Add New Role"]{ display: none; }'; 
	}
	if($request_url == 'role.php' &&  !in_array('editRole', $UserRole) ){
		$styles	.= 'a[title="Click to Edit Role"]{ display: none; }';
	}
	
	/*********   User ***************/
	if($request_url == 'view_users.php' &&  !in_array('deleteUser', $UserRole)){
		$styles	.= 'a[title="Click to Delete user"]{ display: none; }';
	}
	if($request_url == 'view_users.php' &&  !in_array('addUser', $UserRole)){
		$styles	.= 'input[value="Add New User"]{ display: none; }'; 
	}
	if($request_url == 'view_users.php' &&  !in_array('editUser', $UserRole)){
		$styles	.= 'a[title="Click to Edit user"]{ display: none; }';
	}
	
	/*********   Sponsor ***************/
	/*if($request_url == 'sponsor.php' &&  !in_array('deleteSponsor', $UserRole) ){
		$styles	.= 'a[title="Delete"]{ display: none; }';
	}
	if($request_url == 'sponsor.php' &&  !in_array('addSponsor', $UserRole) ){
		$styles	.= 'input[value="Add Sponsor"]{ display: none; }'; 
	}
	if($request_url == 'sponsor.php' &&  !in_array('editSponsor', $UserRole) ){
		$styles	.= 'a[title="Edit"]{ display: none; }';
	} */
	
	/*********   Meta ***************/
	if($request_url == 'add_meta_tag.php' &&  !in_array('addMetaTags', $UserRole) ){
		$styles	.= 'input[value="Click to Edit provider"]{ display: none; }'; 
	}
	if($request_url == 'meta_tag.php' &&  !in_array('editMetaTags', $UserRole) ){
		$styles	.= 'a[title="Click to Edit provider"]{ display: none; }';
	}
	
	/*********   Coupon ***************/
	if($request_url == 'create_coupon.php' &&  !in_array('addCoupon', $UserRole) ){
		$styles	.= 'input[value="Create Coupon"]{ display: none; }'; 
	}
	if($request_url == 'coupon.php' &&  !in_array('deleteCoupon', $UserRole) ){
		$styles	.= '.DelCoupon{ display: none; }';
	}
	// if($request_url == 'couponrecord.php' &&  !in_array('addCoupon', $UserRole) ){
	// 	$styles	.= '.DelCoupon{ display: none; }';
	// }

	/*********   page ***************/
	if($request_url == 'page.php' &&  !in_array('addPage', $UserRole) ){
		$styles	.= 'input[value="Add New Page"]{ display: none; }'; 
	}
	if($request_url == 'page.php' &&  !in_array('editPage', $UserRole) ){
		$styles	.= 'a[title="Click to Edit page"]{ display: none; }';
	}
	if($styles != '')
	echo $styleopen. $styles. $styleClose;
	
	
	/*** count all merchant ***/
	$total_merchant = mysql_fetch_object(mysql_query("SELECT count(`id`) AS total_merchant FROM `users` ")) or die(mysql_error());

	/*** count unapproved merchant ***/
	$total_unapproved_merchant = mysql_fetch_object(mysql_query("SELECT count(`id`) AS total_unapproved_merchant FROM `users` WHERE   `isactive` = CONVERT( _utf8 'p' USING latin1 ) COLLATE latin1_swedish_ci  ")) or die(mysql_error());

    /*** count approved merchant ***/
    $total_approved_merchant = mysql_fetch_object(mysql_query("SELECT count(`id`) AS total_approved_merchant FROM `users` WHERE   `isactive` = CONVERT( _utf8 't' USING latin1 ) COLLATE latin1_swedish_ci  ")) or die(mysql_error());

    /*** count approved products  ***/
	$total_product = mysql_fetch_object(mysql_query("SELECT count(`id`) AS total_product FROM `products` WHERE isactive = 't' ")) or die(mysql_error());

	/*** count unapproved products  ***/
	$total_unapproved_product = mysql_fetch_object(mysql_query("SELECT count(`id`) AS total_unapproved_product FROM `products` WHERE isactive = 'p' ")) or die(mysql_error());

	/*** count all orders  ***/
	$total_order = mysql_fetch_object(mysql_query("SELECT count(`id`) As total_order FROM `order`  ")) or die(mysql_error());

	/*** count all orders  ***/
	$total_customer = mysql_fetch_object(mysql_query("SELECT count( DISTINCT `email`) AS total_customer FROM `shipping_addres_list` WHERE 1 ")) or die(mysql_error());
	?>
</head>

<body> 
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><span
                    class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span></button>
            <a class="navbar-brand" href="<?php echo $site_url; ?>dashbord.php"><img src="../assets/img/admin-logo.png" style="width: 125px;"></a></div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
        	<?php
        	$flagNotify1 = $flagNotify2 = 1; 
        	if (isset($_SESSION['admin_type']) && $_SESSION['admin_type'] > 0) 
        	{
        		
        		if(!in_array('listMerchant', $UserRole)  || !in_array('editMerchant', $UserRole))
        		{
        			$flagNotify1 = 0;
        		}
        		

        		if(!in_array('listProduct', $UserRole) || !in_array('editProduct', $UserRole))
        		{
        			$flagNotify2 = 0;
        		}



        	}


            if($total_unapproved_product->total_unapproved_product > 0 && $flagNotify2 == 1 )
            {
            ?>
        	<li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="fa fa-cube"></i><span class="badge badge-red"><?php echo $total_unapproved_product->total_unapproved_product;?></span></a>
                <ul class="dropdown-menu alert-dropdown">
                    <?php
                    $resu = mysql_query("SELECT id,product_name, image, Add_Date FROM `products` WHERE isactive = 'p' ") or die(mysql_error());

                    if(mysql_num_rows($resu) > 0)
                    {
                    	while ($rows = mysql_fetch_object($resu)) {
                    		$productImg 	= "../assets/img/admin-logo.png";
                    		if($rows->image != "")
                    		$productImg 	= "../product_images/".$rows->image ;
                    ?>
                    <li class="alert-items">
                        <a href="<?php echo $site_url; ?>edit_product.php?edit_id=<?php echo $rows->id;?>">
                        <img src="<?php echo $productImg;?>" width="36" height="36" /><span><?php echo $rows->product_name;?></span>
                        <div><?php echo $rows->Add_Date;?></div>
                        </a>
                    </li>
                    <?php
                		}
                	}
                	?>
                </ul>
            </li>
            <?php
            }   
            if($total_unapproved_merchant->total_unapproved_merchant > 0 && $flagNotify1 == 1)
            {
            ?>
        	<li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="fa fa-user"></i><span class="badge badge-yellow"><?php echo $total_unapproved_merchant->total_unapproved_merchant;?></span></a>
                <ul class="dropdown-menu alert-dropdown">
                    <?php
                    $resu = mysql_query("SELECT id, display_name, image, created FROM `users` WHERE isactive = 'p' ") or die(mysql_error());

                    if(mysql_num_rows($resu) > 0)
                    {
                    	while ($rows = mysql_fetch_object($resu)) {
                    		$merchantImg 	= "../assets/img/admin-logo.png";
                    		if($rows->image != "")
                    		$merchantImg 	= "../banner_images/".$rows->image ;
                    ?>
                    <a href="<?php echo $site_url; ?>edit_provider.php?edit_id=<?php echo $rows->id;?>">
                    <li class="alert-items">
                        <img src="<?php echo $merchantImg;?>" width="36" height="36" /><span><?php echo $rows->display_name;?></span>
                        <div><?php echo $rows->created;?></div>
                        </a>
                    </li>
                    <?php
                		}
                	}
                	?>
                </ul>
            </li>
            <?php
            }   
            ?>
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php if($resultAdmin->username != ''){   echo ucwords($resultAdmin->username) ; } ?> 
                     <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="settings.php"><i class="fa fa-fw fa-user"></i> My Account</a></li>
                    <li><a href="logout.php" id="logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a></li>
                </ul>
            </li>
            
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <?php
                $dashboard = end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'dashbord.php';
                
                $category = end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'catergory.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_category.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_category.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'view_category.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_sub_cat.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'view_sub_cat.php';

                $provider = end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'provider.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_provider.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_provider.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'view_provider.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'meta.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'e_meta.php';

                $buyer = end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'buyer.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_buyer.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_buyer.php';

                $product = end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'product.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_product.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_product.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'view_product.php';

                $company = end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'company.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_company.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_company.php';

                $record = end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'users.php';
                 
                $couponrecord = end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'couponrecord.php';

                $color = end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'color-brands.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add-color-brands.php';

                $singupDeals = end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'signupdeals.php';
                ?>
                <li class="<?php if ($dashboard) { ?>active<?php } else { ?>select<?php } ?>"><a href="dashbord.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a></li>
                <?php
                if (isset($_SESSION['admin_type']) && $_SESSION['admin_type'] > 0) {

                    if (in_array('listRole', $UserRole) || in_array('addRole', $UserRole) || in_array('editRole', $UserRole) || in_array('deleteRole', $UserRole) || in_array('listUser', $UserRole) || in_array('addUser', $UserRole) || in_array('editUser', $UserRole) || in_array('deleteUser', $UserRole) || in_array('listAuditTrail', $UserRole) || in_array('verifyUserCreate', $UserRole) || in_array('verifyUserUpdate', $UserRole) ) { ?>
                        <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'view_users.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_user.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_user.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'role.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_role.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_role.php'|| end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'audit-trail.php') { ?>active<?php } else { ?>select<?php } ?>">
                            <a href="javascript:;" data-toggle="collapse" data-target="#pages-dropbown1"><i  class="fa fa-fw fa-arrows-v"></i> Administrator <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="pages-dropbown1" class="collapse">
                                <?php if (in_array('listRole', $UserRole) || in_array('addRole', $UserRole) || in_array('editRole', $UserRole) || in_array('deleteRole', $UserRole)) { ?>
                                    <li><a href="role.php"><i class="fa fa-key"></i> Role</a></li>
                                <?php
                                }
                                if (in_array('listUser', $UserRole) || in_array('addUser', $UserRole) || in_array('editUser', $UserRole) || in_array('deleteUser', $UserRole)) { ?>
                                    <li><a href="view_users.php"><i class="fa fa-user"></i> Users</a></li>
                                <?php
                                }
								if (in_array('listAuditTrail', $UserRole)) { ?>
                                    <li><a href="audit-trail.php"><i class="fa fa-paw"></i>
 Audit Trail</a></li>
                                <?php
                                }
                                if (in_array('verifyUserCreate', $UserRole) || in_array('verifyUserUpdate', $UserRole) || in_array('verifyUserDelete', $UserRole) || in_array('deleteUser', $UserRole)) { ?>
                                    <li><a href="verify_users.php"><i class="fa fa-user"></i>
 Verify Users</a></li>
                                <?php
                                }

                                ?>
                            </ul>
                        </li>
                    <?php
                    }
                    if (in_array('listCatergory', $UserRole) ||  in_array('viewCatergory', $UserRole) || in_array('addCatergory', $UserRole) || in_array('editCatergory', $UserRole) || in_array('deleteCatergory', $UserRole) || in_array('viewSubCatergory', $UserRole) || in_array('addSubCatergory', $UserRole) || in_array('editSubCatergory', $UserRole) || in_array('deleteSubCatergory', $UserRole) || in_array('viewCatergorySize', $UserRole) || in_array('addCatergorySize', $UserRole) || in_array('editCatergorySize', $UserRole) || in_array('deleteCatergorySize', $UserRole)) { ?>
                        <li class="<?php if ($category) { ?>active<?php } else { ?>select<?php } ?>"><a href="catergory.php"><i class="fa fa-sitemap"></i>Catergory</a></li>
                    <?php }
                    ?>

                    <?php

                    if (in_array('listMerchant', $UserRole) ||  in_array('viewMerchant', $UserRole) || in_array('addMerchant', $UserRole) || in_array('editMerchant', $UserRole) || in_array('deleteMerchant', $UserRole) || in_array('addBanner', $UserRole) || in_array('Aboutus', $UserRole) || in_array('viewMeta', $UserRole) || in_array('addMeta', $UserRole) || in_array('editMeta', $UserRole) || in_array('deleteMeta', $UserRole) || in_array('exportMerchant', $UserRole) || in_array('manageStatusMerchant', $UserRole)) { ?>
                        <li class="<?php if ($provider) { ?>active<?php } else { ?>select<?php } ?>"><a href="provider.php"><i class="fa fa-user"></i> Merchants</a></li>

                    <?php }
                    if (in_array('listBuyer', $UserRole) || in_array('addBuyer', $UserRole) || in_array('editBuyer', $UserRole) /*|| in_array('deleteBuyer', $UserRole) || in_array('exportBuyer', $UserRole)*/) { ?>
                        <li class="<?php if ($buyer) { ?>active<?php } else { ?>select<?php } ?>"><a href="buyer.php"><i class="fa fa-user"></i> Customers</a>
                        </li>

                    <?php }
                    if (in_array('listProduct', $UserRole) || in_array('viewProduct', $UserRole) || in_array('addProduct', $UserRole) || in_array('editProduct', $UserRole) || in_array('deleteProduct', $UserRole) || in_array('manageStatusProduct', $UserRole)) { ?> 
                        <li class="<?php if ($product) { ?>active<?php } else { ?>select<?php } ?>"><a href="product.php"><i class="fa fa-cube"></i>
 Products</a></li>
                    <?php }
                    /*if (in_array('listDeal', $UserRole) || in_array('addDeal', $UserRole) || in_array('editDeal', $UserRole) || in_array('deleteDeal', $UserRole)) { ?>

                       <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'deal.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit-deal.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add-deal.php') { ?>active<?php  } else { ?>select<?php } ?>">
                            <a href="deal.php">Deal</a></li>
                    <?php  } */
                    if (in_array('viewTransactionsRecord', $UserRole)) { ?>
                        <li class="<?php if ($record) { ?>active<?php } else { ?>select<?php } ?>"><a href="users.php"><i class="fa fa-money fa-fw"></i> Transaction Records</a></li>
                    <?php }
                    if (in_array('listcoupon', $UserRole)) { ?>
                        <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'create_coupon.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'coupon.php') { ?>active<?php  } else { ?>select<?php } ?>"><a href="coupon.php"><i class="fa fa-shopping-cart"></i> Coupon</a></li>
                    <?php }
                     if (in_array('listcoupon', $UserRole)) { ?>
                        <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'couponrecord.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'couponrecord.php') { ?>active<?php  } else { ?>select<?php } ?>"><a href="couponrecord.php"><i class="fa fa-money fa-fw"></i> Coupon record</a></li>
                    <?php }


                    /*if (in_array('exportTransactionsRecord', $UserRole)) { ?>
                        <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'export-transation.php') { ?>active<?php } else { ?>select<?php } ?>">
                            <a href="export-transation.php">Export transaction</a></li>
                    <?php } 
                    if (in_array('listColorBrand', $UserRole) || in_array('addBrand', $UserRole) || in_array('addColor', $UserRole) || in_array('editColorBrand', $UserRole) || in_array('deleteColorBrand', $UserRole) ) { ?>
                        <li class="<?php if ($color) { ?>active<?php } else { ?>select<?php } ?>"><a
                                href="color-brands.php"><i class="fa fa-cog"></i>
 Color And Brands</a></li>
                    <?php }*/

                    if (in_array('listHomePageSliderImage', $UserRole) || in_array('addHomePageSliderImage', $UserRole) || in_array('editHomePageSliderImage', $UserRole) || in_array('deleteHomePageSliderImage', $UserRole) ) { ?>
                        <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'home-page-slider-image.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit-home-page-slider-image.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add-home-page-slider-image.php')   { ?>active<?php } else { ?>select<?php } ?>"><a href="home-page-slider-image.php"><i class="fa fa-file-image-o">  Home page Slider Image</i></a></li>
                    <?php
                    }
                    /*
                    if (in_array('listSponsor', $UserRole) || in_array('addSponsor', $UserRole) || in_array('editSponsor', $UserRole) || in_array('deleteSponsor', $UserRole)) { ?>

                        <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'sponsor.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_sponsor.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_sponsor.php') { ?>active<?php } else { ?>select<?php } ?>">
                            <a href="sponsor.php"><i class="fa fa-users"></i>
 Sponsor</a></li>
                    <?php
                    } */
                    if (in_array('viewsettlement', $UserRole) ) { ?>
                       <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'settlement.php' ) { ?>active<?php } else { ?>select<?php } ?>">
                        <a href="settlement.php"><i class="fa fa-adjust"></i>
 Settlement</a></li>
                    <?php
                    }

                    if (in_array('listFedExNGShipping', $UserRole) || in_array('addFedExNGShipping', $UserRole) || in_array('editFedExNGShipping', $UserRole) || in_array('deleteFedExNGShipping', $UserRole) || in_array('listMerchantFedExNGShipping', $UserRole) || in_array('addMerchantFedExNGShipping', $UserRole) /*|| in_array('editMerchantFedExNGShipping', $UserRole) || in_array('deleteMerchantFedExNGShipping', $UserRole)*/ || in_array('listMerchantFedExNGCast', $UserRole) || in_array('addMerchantFedExNGCast', $UserRole) /*|| in_array('editMerchantFedExNGCast', $UserRole) || in_array('deleteMerchantFedExNGCast', $UserRole)*/
                    ) { ?>
                        <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'fedex.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_fedex.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_fedex.php') { ?>active<?php } else { ?>select<?php } ?>">
                            <a href="fedex.php"><i class="fa fa-truck"></i>
 FedEx NG</a></li>
                    <?php
                    }
                    if (in_array('listMetaTags', $UserRole) || in_array('addMetaTags', $UserRole) || in_array('editMetaTags', $UserRole)/* || in_array('deleteMetaTags', $UserRole)*/) { ?>
                        <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'meta_tag.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_meta_tag.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_meta_tag.php') { ?>active<?php } else { ?>select<?php } ?>">
                            <a href="meta_tag.php"><i class="fa fa-info"></i>
 Meta Tag</a></li>
                    <?php
                    }
                    if (in_array('listPage', $UserRole) || in_array('addPage', $UserRole) || in_array('editPage', $UserRole)) { ?>
                    <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'page.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_page.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_page.php') { ?>active<?php } else { ?>select<?php } ?>">
                        <a href="page.php"><i class="fa fa-file-text"></i>
 Page</a></li>
 					<?php
 					}
                } else {
                    ?>
                    <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'view_users.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_user.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_user.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'role.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_role.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_role.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'audit-trail.php') { ?>active<?php } else { ?>select<?php } ?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#administrator-dropbown"><i class="fa fa-fw fa-arrows-v"></i> Administrator <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="administrator-dropbown" class="collapse">
                            <li><a href="role.php"><i class="fa fa-key"></i>
 Role</a></li>
                            <li><a href="view_users.php"><i class="fa fa-user"></i> Users</a></li>
                             <li><a href="audit-trail.php"><i class="fa fa-paw"></i>
 Audit Trail</a></li>
  							 <li><a href="verify_users.php"><i class="fa fa-user"></i> Verify Users</a></li>
                        </ul>
                    </li>
                    <li class="<?php if ($category) { ?>active<?php } else { ?>select<?php } ?>"><a href="catergory.php"><i class="fa fa-sitemap"></i>
 Catergory</a></li>

                    <li class="<?php if ($provider) { ?>active<?php } else { ?>select<?php } ?>"><a href="provider.php"><i class="fa fa-user"></i> Merchants</a> </li>

                    <li class="<?php if ($buyer) { ?>active<?php } else { ?>select<?php } ?>"><a  href="buyer.php"><i class="fa fa-user"></i> Customers</a></li>

                    <li class="<?php if ($product) { ?>active<?php } else { ?>select<?php } ?>"><a href="product.php"><i class="fa fa-cube"></i>
 Products</a> </li>

                    <!--<li class="<?php // if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'deal.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit-deal.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add-deal.php') { ?>active<?php  //} else { ?>select<?php //} ?>">
                        <a href="deal.php">Deal</a></li> !-->

                    <li class="<?php if ($record) { ?>active<?php } else { ?>select<?php } ?>"><a href="users.php"><i class="fa fa-money fa-fw"></i> Transaction Records</a></li>
                     <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'create_coupon.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'coupon.php') { ?>active<?php  } else { ?>select<?php } ?>"><a href="coupon.php"><i class="fa fa-shopping-cart"></i> Coupon</a></li>

                      <li class="<?php if ($couponrecord) { ?>active<?php } else { ?>select<?php } ?>"><a href="couponrecord.php"><i class="fa fa-money fa-fw"></i> Coupon Records</a></li>

                    <!--<li class="<?php // if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'export-transation.php') { ?>active<?php // } else { ?>select<?php //} ?>">
                        <a href="export-transation.php">Export transaction</a></li> !-->
                    <?php /*
                    <li class="<?php if ($color) { ?>active<?php } else { ?>select<?php } ?>"><a
                            href="color-brands.php"><i class="fa fa-cog"></i>
 Color And Brands</a></li> */ ?>

                    </li> 
                    <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'home-page-slider-image.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit-home-page-slider-image.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add-home-page-slider-image.php')   { ?>active<?php } else { ?>select<?php } ?>">
                            <a href="home-page-slider-image.php"><i class="fa fa-file-image-o"></i>  Home page Slider Image</a></li>

                    <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'settlement.php' ) { ?>active<?php } else { ?>select<?php } ?>">
                        <a href="settlement.php"><i class="fa fa-adjust"></i>
 Settlement</a></li>
 					<?php /*
                    <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'sponsor.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_sponsor.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_sponsor.php') { ?>active<?php } else { ?>select<?php } ?>">
                        <a href="sponsor.php"><i class="fa fa-users"></i>
 Sponsor</a></li> */ ?>

                    <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'fedex.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_fedex.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_fedex.php') { ?>active<?php } else { ?>select<?php } ?>">
                        <a href="fedex.php"><i class="fa fa-truck"></i>
 FedEx NG</a></li>

                    <li class="<?php if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'meta_tag.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_meta_tag.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_meta_tag.php') { ?>active<?php } else { ?>select<?php } ?>">
                        <a href="meta_tag.php"><i class="fa fa-info"></i>
 Meta Tag</a></li>
 					<!--<li class="<?php /*if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'page.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit_page.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add_page.php') { */?>active<?php /*} else { */?>select<?php /*} */?>">
                        <a href="page.php"><i class="fa fa-file-text"></i>
 Page</a></li>
                    <?php /*if (isset($_SESSION['id']) && $_SESSION['id'] == 1) { */?>
                        <li class="<?php /*if (end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'sub-admin.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'edit-sub-admin.php' || end(explode('/', $_SERVER['SCRIPT_FILENAME'])) == 'add-sub-admin.php') { */?>active<?php /*} else { */?>select<?php /*} */?>">
                            <a href="sub-admin.php">Sub-Admin</a></li>-->
                    <?php
//                    }

                }
                ?>
                <li class="<?php if ($category) { ?>active<?php } else { ?>select<?php } ?>"><a href="featured_merchants.php"><i class="fa fa-sitemap"></i>Featured Merchant</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <div id="page-wrapper">
        <div class="container-fluid">
