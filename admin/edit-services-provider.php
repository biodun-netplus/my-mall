<?php
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */


if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{      
     $provider='sp';
     $date=date("Y-m-d H:i:s");
	 $User=array();
	 $merchantName =strtolower(str_replace(array( '\'', '"', ',' , '(' , ')', ';' ,':' ,'.','&',"'", '<', '>',' ' ),'',$_REQUEST['provider_name']));
	 $User['first_name']=$_REQUEST['provider_name'];	
	 $User['email']=$_REQUEST['provider_email'];
	 $User['password']=$_REQUEST['provider_pass'];
	 $User['phone']=$_REQUEST['provider_contact'];
	 $User['address']=$_REQUEST['provider_address'];
	 $User['deliveryarea']=$_REQUEST['deliveryarea'];
	 $User['state']=$_REQUEST['provider_state'];
	 $User['country']=$_REQUEST['Country'];
	 $User['created']=$date;
	 $User['user_type']=$provider;
	 $update = mysql_query("update users set  first_name='".$_REQUEST['provider_name']."',email='".$_REQUEST['provider_email']."' where id='".$_REQUEST['services_pid']."'");
	 if($update)
	 {
		 		header("location:services-provider.php");

	 }
}
$ServicesProviderInfo = mysql_fetch_object(mysql_query("select * from users where id='".$_REQUEST['id']."'"));
// print_r($ServicesProviderInfo);
 ?>
<script type="text/javascript">
$(document).ready(function(){
	
	$("#country").change(function()
	{
		var id = this.value;
		
		if(id!=''){
			$.ajax({
				url: "state.php",
				type: "POST",
				data: {'id':id,'hint':'state'},
				success: function(Data){
					
					$("#ajax_state").html(Data);
				}
			});
		}
	})
	$("#provider_state").live('change',function()
	{
		var id = this.value;
		
		if(id!=''){
			$.ajax({
				url: "state.php",
				type: "POST",
				data: {'id':id,'hint':'city'},
				success: function(Data){
					
					$("#ajax_city").html(Data);
				}
			});
		}
	})	
})
</script>

<script type="text/javascript" src="js/validation.js"></script>

 
 <div class="clear"></div>
 
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content" style="">


<div >
<div id="page-heading" ><h1>Add Merchant</h1></div>
<div style="float:right; padding-right:20px;"><input type="button" class="buttons btn btn-info" value="Back" onclick="goBack()" /></div>
</div>


<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
				
				<!--  start message-red -->
				
				<!--  end message-red -->
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td align="center">
	
	
		<!--  start step-holder -->
		<!--  end step-holder -->
        <!-- start id-form -->
        <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post" id="ProviderForm">
		<table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
		<tr>
			<th valign="top">Name:</th>
			<td><input type="text" class="form-control" id="name" name="provider_name" value="<?php echo $ServicesProviderInfo->first_name;?>" /></td>
			<td><label class="error"  id="name_error"></label></td>
		</tr>
			<tr>
			<th valign="top"> Email:</th>
			<td><input type="text" class="form-control" id="provider_email" name="provider_email" value="<?php echo $ServicesProviderInfo->email;?>" /></td>
			<td width="310"><label class="error"  id="email"></label></td>
			
			</tr>
			<input type="hidden" name="services_pid" value="<?php echo $_REQUEST['id']?>" />
             
			
			<tr>
			<th valign="top">Contact No:</th>
			<td><input type="text" class="form-control" id="provider_contact" name="provider_contact" value="<?php echo $ServicesProviderInfo->phone;?>" /></td>
			<td><label class="error"  id="contact"></label>
			</tr>
			<tr>
			<th valign="top">Address:</th>
			<td><input type="text" class="form-control" id="provider_address" name="provider_address" value="<?php echo $ServicesProviderInfo->address;?>" /></td>
		<td><label class="error"  id="address"></label></td>
		</tr>
			
			<tr>
			<th valign="top">Country:</th>
			<td><select id="country" name="Country" class="inp-select form-control" >
            		<option value="">select country</option>
                    <?php 
					$query = mysql_query("select * from countries");
					while($data = mysql_fetch_object($query)){ ?>  
                    <option <?php if($ServicesProviderInfo->country==$data->id){ ?> selected="selected" <?php } ?> value="<?php echo $data->id ?>"><?php echo $data->name; ?></option> 
					<?php } ?>
            	</select>
            </td>
			<td><label class="error"  id="country_error"></label></td>
			</tr>
			<tr>
			<th valign="top">State:</th>
			<td>
            	<span id="ajax_state">
                
                <select id="provider_state" name="provider_state" class="inp-select form-control" >
                	<option value="">select state</option>
                </select>
                </span>
            </td>
			<td><label class="error"  id="state"></label></td>
			</tr>
            
         
        
	<tr>
		<th>&nbsp;</th>
		<td valign="top">
			<input type="submit" id="ProviderSubmit" value="submit" name="submit" class="form-submit btn btn-default" />
			<input type="reset" value="Reset" class="form-reset btn btn-default"  />		</td>
		</tr>
	</table>
   		 </form>
	<!-- end id-form  -->	</td>
	
</tr>
<tr>
<td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
	
	<div class="clear"></div>
 

</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>

<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>