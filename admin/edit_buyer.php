<?php 
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */

if(isset($_REQUEST['submit']) && $_REQUEST['submit']=='submit')
{     
	 $date=date("Y-m-d H:i:s");
	 $provider='p';
	 $User=array();
	 $User['first_name']=$_REQUEST['buyer_name'];
	 $User['email']=$_REQUEST['email'];
	 $User['phone']=$_REQUEST['contact'];
	 $User['address']=$_REQUEST['address'];
	 $User['city']=$_REQUEST['city'];
	 $User['state']=$_REQUEST['state'];
	 $User['zip']=$_REQUEST['zipcode'];
	 $id=$_REQUEST['id'];
	 $main= new users;
	 $xx= $main->update($User,'users',$id);
	 if($xx)
	 {
		 header("Location:buyer.php");
	 }
}
if(isset($_REQUEST['edit_id']))
{
	//echo "SELECT cn.name as countryName, u.*,s.name as stateName, c.name as cityName FROM `users` AS u INNER JOIN `cities` AS c ON u.city = c.id INNER JOIN `states` AS s ON u.state = s.id INNER JOIN `countries`AS cn ON u.country = cn.id where u.id = '".$_REQUEST['edit_id']."'";
	
$query = mysql_query("select * from countries");
$query1 = mysql_query("select * from states");
$query2 = mysql_query("select * from cities");

	
$JoinQuery = mysql_query("SELECT cn.name as countryName, u.*,s.name as stateName, c.name as cityName FROM `users` AS u INNER JOIN `cities` AS c ON u.city = c.id INNER JOIN `states` AS s ON u.state = s.id INNER JOIN `countries`AS cn ON u.country = cn.id where u.id = '".$_REQUEST['edit_id']."'");
$JoinData = mysql_fetch_object($JoinQuery);
//print_r($JoinData);
}?>
 <div class="clear"></div>
 <script type="text/javascript">
$(document).ready(function(){
	
	$("#country").change(function()
	{
		var id = this.value;
		if(id!=''){
			$.ajax({
				url: "state.php",
				type: "POST",
				data: {'id':id,'hint':'state'},
				success: function(Data){
					
					$("#ajax_state").html(Data);
				}
			});
		}
	})
	$("#provider_state").live('change',function()
	{
		var id = this.value;
		if(id!=''){
			$.ajax({
				url: "state.php",
				type: "POST",
				data: {'id':id,'hint':'city'},
				success: function(Data){
					
					$("#ajax_city").html(Data);
				}
			});
		}
	})	
})
</script>
<script type="text/javascript" src="js/validation.js"></script>
<!-- start content-outer -->
<div id="content-outer">
<!-- start content -->
<div id="content">


<div id="page-heading"><h1>Edit Buyer</h1></div>


<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
<tr>
	<th rowspan="3" class="sized"></th>
	<th class="topleft"></th>
	<td id="tbl-border-top">&nbsp;</td>
	<th class="topright"></th>
	<th rowspan="3" class="sized"></th>
</tr>
<tr>
	<td id="tbl-border-left"></td>
	<td>
	<!--  start content-table-inner -->
	<div id="content-table-inner">
				
				<!--  start message-red -->
				
				<!--  end message-green -->
		
		 
	
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
	<tr valign="top">
	<td align="center">
	
	
		<!--  start step-holder -->
		<!--  end step-holder -->
        <!-- start id-form -->
        <form action="<?php $_SERVER['PHP_SELF'];?>" name="form1" method="post">
		<table border="0" cellpadding="8" cellspacing="0"  id="id-form" width="">
		<tr>
			<th valign="top">Buyer name:</th>
			<td><input type="text" class="form-control" name="buyer_name" value="<?php echo $JoinData->first_name;?>" /></td>
			</tr>
            <input type="hidden" value="<?php echo $JoinData->id;?>" name="id"/>
			
            
			<tr>
			<tr>
			<th valign="top">Email</th>
			<td><input type="text" class="form-control" name="email" value="<?php echo $JoinData->email;?>" /></td>
			</tr>
			<tr>
			<th valign="top">Contact</th>
			<td><input type="text" class="form-control" name="contact" value="<?php echo $JoinData->phone;?>" /></td>
			</tr>
			
			<tr>
			<th valign="top">Address</th>
			
			<td><input type="text" class="form-control" name="address" value="<?php echo $JoinData->address;?>" /></td>
			</tr>
			<tr>
			<th valign="top">Country:</th>
			<td><select id="country" name="Country" class="inp-select form-control" >
            		<option value="">select country</option>
                    <?php while($data = mysql_fetch_object($query)){ ?>  
                    <option <?php if($JoinData->countryName==$data->name){ ?> selected="selected" <?php } ?> value="<?php echo $data->id ?>"><?php echo $data->name; ?></option> 
					<?php } ?>
            	</select>
            </td>
			<td><label class="error"  id="country_error"></label></td>
			</tr>
			<tr>
			<th valign="top">State:</th>
			<td>
            	<span id="ajax_state"><select id="provider_state" name="provider_state" class="inp-select form-control" >
                	<option value="">select states</option>
                    <?php while($data1 = mysql_fetch_object($query1)){ ?>  
                    <option <?php if($JoinData->stateName==$data1->name){ ?> selected="selected" <?php } ?> value="<?php echo $data1->id ?>"><?php echo $data1->name; ?></option> 
					<?php } ?>
                </select>
                </span>
            </td>
			<td><label class="error"  id="state"></label></td>
			</tr>
            <tr>
			<th valign="top">City:</th>
			<td><span id="ajax_city"><select id="provider_city" name="provider_city" class="inp-select form-control" >
                	<option value="">select city</option>
                    <?php while($data2 = mysql_fetch_object($query2)){ ?>  
                    <option <?php if($JoinData->cityName==$data2->name){ ?> selected="selected" <?php } ?> value="<?php echo $data2->id ?>"><?php echo $data2->name; ?></option> 
					<?php } ?>
                </select>
                </span></td>
			<td><label class="error"  id="city"></label></td>
			</tr>
            <tr>
			<th valign="top">Zipcode</th>
			
			<td><input type="text" class="form-control" name="zipcode" value="<?php echo $JoinData->zip;?>" /></td>
			
			</tr>
			
	<tr>
		<th>&nbsp;</th>
		<td valign="top">
			<input type="submit" value="submit" name="submit" class="form-submit btn btn-default" />
			<input type="reset" value="Reset" class="form-reset btn btn-default"  />		</td>
		</tr>
	</table>
   		 </form>
	<!-- end id-form  -->	</td>
	
</tr>
<tr>
<td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
<td></td>
</tr>
</table>
	
	<div class="clear"></div>
 

</div>
<!--  end content-table-inner  -->
</td>
<td id="tbl-border-right"></td>
</tr>
<tr>
	<th class="sized bottomleft"></th>
	<td id="tbl-border-bottom">&nbsp;</td>
	<th class="sized bottomright"></th>
</tr>
</table>
<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

 

<div class="clear">&nbsp;</div>
    <?php include("footer.php")?>