<?php 
include("header.php");
/* User management */
/* Author @Damodar Prasad */
/* Date : 16-12-2011 */

//error_reporting(E_ALL);
  
  ?>
<style type="text/css">
#id-form td, #id-form th {
  display: inline-block;
}
</style>

<div class="clear"></div>

<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content" style="">
    <div>
      <div id="page-heading" style="float:none">
        <h1>Admin Details</h1>
      </div>
      <div style="float:right; padding-right:20px;"><a href="update_setting.php">
        <input type="button" class="buttons btn btn-info" value="Change Password"  />
        </a></div>
    </div>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
      <tr>
        <th rowspan="3" class="sized"></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><!--  start content-table-inner -->
          
          <div id="content-table-inner"> 
            
            <!--  start table-content  -->
            <div id="table-content">
              <?php if(isset($_REQUEST['msg'])){ ?>
              <div id="message-green">
                <table border="0" width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td class="green-left">Password Sucessfully Change. <a href=""></a></td>
                    <td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif"   alt="" /></a></td>
                  </tr>
                </table>
              </div>
              <?php  } ?>
              <?php 
              $admin_id=$_SESSION['id'];
              $main= new users;
              $xx= $main->viewprofile($admin_id,"admin");
              ?>
              <table width="100%" border="0" cellpadding="0" cellspacing="0"  id="id-form">
                <tr>
                  <th width="250" align="left" valign="top">User name:</th>
                  <td align="left" valign="top"><?php echo ucfirst($xx->username);?></td>
                </tr>
                <tr>
                  <th  width="250" align="left" valign="top">Email:</th>
                  <td align="left" valign="top"><?php echo $xx->email_id;?></td>
                </tr>
              </table>
            </div>
            <div class="clear"></div>
          </div>
         <?php
         if (isset($_SESSION['admin_type']) && $_SESSION['admin_type'] < 0) 
         {
         ?>
        <h1>Manage Role</h1>
       <?php 
       $result1 = mysql_fetch_object(mysql_query("SELECT admin .* FROM admin WHERE admin.id = '".$_SESSION['id']."'"));
       
       //echo $result1->role;
       
       if($result1->role != ''){  
       
       $role_str  = '';
       $roles   = explode("|", $result1->role);  
       $str_roles='';
       for($i = 0; $i < count($roles); $i++)
       {
         $str_roles   .= "'".$roles[$i]."',";
         //$str_roles .= $roles[$i].",";
         
       }

       $str_roles = substr($str_roles, 0 ,-1); 

        $query = "SELECT role .* FROM role WHERE role.id IN ( ". $str_roles." )";
        
        $result = mysql_query($query) or die(mysql_error());
        $countCondition = mysql_num_rows($result);
        ?>          
      <form id="mainform" action="" class="table-responsive">
        <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" class="table table-bordered table-hover table-striped">
          <tr>
            <th class="table-header-check"><a id="toggle-all" ></a> </th>
            <th  class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Title</a></th>
            <th class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Description </a></th>
            <th width="125" class="table-header-repeat line-left minwidth-1"><a class="arrow_none">Action</a></th>
          </tr>
          <?php
        if($countCondition > 0){
          while($data = mysql_fetch_object($result)){ 
            
        ?>
          <tr>
            <td width="20"><input  type="checkbox" name="checkbox" value="<?php echo $data->id;?>"/></td>
            <td><?php echo ucfirst($data->title); ?></td>
            <td><?php echo $data->description; ?></td>
            <td><a href="manage_role.php?id=<?php echo base64_encode($data->id);?>" title="Click to Edit Role"> <img src="images/table/action_edit.gif"/></a></td>
          </tr>
          <?php }}else{ ?>
          <tr>
            <td colspan="10" align="center"><div style="color:#F00; font-size:15px; font-weight:bold; text-align:center; padding:15px 0 15px 0;">No Record Found ! </div></td>
          </tr>
          <?php } ?>
        </table>
        <!--  end product-table................................... -->
      </form>
      <?php
      }
    }
      ?>
          
          
          <!--  end content-table-inner  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>
<?php include("footer.php")?>
