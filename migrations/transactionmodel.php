<?php

	class Transactionmodel extends CI_Model{


        private $query = array();

    public function __construct()
    {	
        parent::__construct();  // Call the Model constructor
    }

    public function view_transactions($limit='', $start=0, $total_row = false){
		if(!$total_row)
		{
			if($limit != '') {
                if ($_SESSION['parish_id'] != null)
                {
                    $p_id = $_SESSION['parish_id'];
                    $query = $this->db->query("SELECT transaction.full_name,transaction.email,transaction.donation_id,transaction.church_id,transaction.parish_id,transaction.updated_at,transaction.status,transaction.trans_ref,transaction.transaction_id,transaction.total_amount,transaction.id,transaction.narration,churches.name as church_name,churches.convenience_fee as convenience_fee, parishes.name as parish_name,donation.description as donation_description FROM transaction left join donation on (transaction.donation_id = donation.donation_id) left join churches on (transaction.church_id = churches.id) left join parishes on (transaction.parish_id = parishes.parish_id) where transaction.parish_id = $p_id ORDER BY transaction.id desc LIMIT " . $start . "," . $limit);

                }
                else {
                    $query = $this->db->query("SELECT transaction.full_name,transaction.email,transaction.donation_id,transaction.church_id,transaction.parish_id,transaction.updated_at,transaction.status,transaction.trans_ref,transaction.transaction_id,transaction.total_amount,transaction.id,transaction.narration,churches.name as church_name,churches.convenience_fee as convenience_fee,parishes.name as parish_name,donation.description as donation_description FROM transaction left join donation on (transaction.donation_id = donation.donation_id) left join churches on (transaction.church_id = churches.id) left join parishes on (transaction.parish_id = parishes.parish_id) ORDER BY transaction.id desc LIMIT " . $start . "," . $limit);

                }
				// echo  $this->db->last_query();
				// die;
				if($query->num_rows() > 0){
					return $query->result();
				}else{
					return FALSE;
				}
			}
			else
			{

				$query = $this->db->query ("SELECT transaction.full_name,transaction.email,transaction.donation_id,transaction.church_id,transaction.parish_id,transaction.updated_at,transaction.status,transaction.trans_ref,transaction.transaction_id,transaction.total_amount,transaction.id,transaction.narration,FROM transaction ORDER BY id desc ");


			// echo "string"; $this->db->last_query();
			// die;
				if($query->num_rows() > 0){
					return $query->result();
				}else{
					return FALSE;
				}
			}
		}
		else{
			$query = $this->db->get('transaction');
			return $query->num_rows();
		}
	}

	public function search($donation){
		$query = $this->db->query("SELECT * FROM transaction WHERE donation_id=".$donation."");
		if ($query->num_rows() > 0) {
			# code...
			return $query->result();
		}else{
			return FALSE;
		}
	}
        public function search_by_date_range($start_date ='',$end_date='',$param = '', $param1 = ''){
           if ($_SESSION['parish_id'] != null){
                $pid = $_SESSION['parish_id'];
             $this->db->select('transaction.transaction_id,transaction.full_name,transaction.email,transaction.total_amount,transaction.narration,transaction.trans_ref,transaction.updated_at,transaction.status,transaction.id,churches.name as
 church_name,churches.convenience_fee as convenience_fee,parishes.name as parish_name,donation.description as
 donation_description');
            $this->db->from('transaction');
            $this->db->join('donation', 'transaction.donation_id = donation.donation_id','left');
            $this->db->join('churches', 'transaction.church_id = churches.id','left');
            $this->db->join('parishes', 'transaction.parish_id = parishes.parish_id','left');
            $this->db->where('transaction.parish_id ='.$pid);
//            $this->db->join('','left');
            if($start_date != '' && $end_date != ''){
                $this->db->where('updated_at BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
            }
            if($param != '')
            {
                $this->db->where($param);
            }
            if($param1 != ''){
                $this->db->where($param1);
            }
            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                # code...
                return $query->result();
            }else{
                return FALSE;
            }
           }else{

              //$this->db->select("SELECT transaction.full_name,transaction.email,transaction.donation_id,transaction.church_id,transaction.parish_id,transaction.updated_at,transaction.status,transaction.trans_ref,transaction.transaction_id,transaction.total_amount,transaction.id,transaction.narration,churches.name as church_name,churches.convenience_fee as convenience_fee,parishes.name as parish_name,donation.description as donation_description FROM transaction left join donation on (transaction.donation_id = donation.donation_id) left join churches on (transaction.church_id = churches.id) left join parishes on (transaction.parish_id = parishes.parish_id) ORDER BY transaction.id desc LIMIT " . $start=0 . "," . $limit='');

               $this->db->select('transaction.id,transaction.transaction_id,transaction.full_name,transaction.email,transaction.total_amount,transaction.narration,transaction.trans_ref,transaction.updated_at,transaction.status,transaction.id,
               churches.name as
                 church_name,churches.convenience_fee as convenience_fee,parishes.name as parish_name,donation.description as
                 donation_description');
               $this->db->from('transaction');
               $this->db->join('donation', 'transaction.donation_id = donation.donation_id','left');
               $this->db->join('churches', 'transaction.church_id = churches.id','left');
               $this->db->join('parishes', 'transaction.parish_id = parishes.parish_id','left');
               $this->db->order_by('transaction.id', 'desc');
//            $this->db->join('','left');
               if($start_date != '' && $end_date != ''){
                   $this->db->where('updated_at BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
               }
               if($param != '')
               {
                   $this->db->where($param);
               }if($param1 != '')
               {
                   $this->db->where($param1);
               }
               $query = $this->db->get();

               if ($query->num_rows() > 0) {
                   # code...
                   return $query->result();
               }else{
                   return FALSE;
               }
           }

        }


       function build_query($q){
         $this->query[] = $q;
       }

        public function search_query(){
            $this->db->select('transaction.transaction_id,transaction.full_name,transaction.email,transaction.total_amount,transaction.narration,transaction.trans_ref,transaction.updated_at,transaction.status,transaction.id,churches.name as
 church_name,churches.convenience_fee as convenience_fee,parishes.name as parish_name,donation.description as
 donation_description');
            $this->db->from('transaction');
            $this->db->join('donation', 'transaction.donation_id = donation.donation_id','left');
            $this->db->join('churches', 'transaction.church_id = churches.id','left');
            $this->db->join('parishes', 'transaction.parish_id = parishes.parish_id','left');

            if (count($this->query) > 0){
                $r = implode(' and ',$this->query);
                //echo $r;
                $this->db->where($r);
            }

            $query = $this->db->get();

            if ($query->num_rows() > 0){
               return $query->result();
            }else{
               return array();
            }


          //$this->db->where('updated_at BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');

        }




        public function total_transaction(){
            if($_SESSION['parish_id'] == null){
                $query = $this->db->query("SELECT count(id) AS total FROM transaction");
            }else{
                $pid = $_SESSION['parish_id'];
                $query = $this->db->query("SELECT count(id) AS total FROM transaction WHERE parish_id = $pid");
            }
            return $query->result();
        }

        public function total_amount(){
            if($_SESSION['parish_id'] != null) {
                $pid = $_SESSION['parish_id'];
                $query = $this->db->query("SELECT sum(total_amount) AS total FROM transaction WHERE status = '1' AND parish_id = $pid");

            }
            else{
                $query = $this->db->query("SELECT sum(total_amount) AS total FROM transaction WHERE status = '1'");
            }
            return $query->result();
        }

        function congregation_member(){
            if($_SESSION['parish_id'] != null){
                $pid = $_SESSION['parish_id'];
                $query = $this->db->query("SELECT * FROM transaction WHERE parish_id = $pid GROUP By email");
            }else{
                $query = $this->db->query("SELECT * FROM transaction GROUP By email");
            }
            return $query->result();
        }
	}

?>